package za.co.enerweb.ebr.gui.vaadin.time_machine;

import static org.mockito.Mockito.mock;

import java.util.List;

import junit.framework.Assert;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.BeforeClass;
import org.junit.Test;

import za.co.enerweb.toolbox.vaadin.messaging.MessageBus;
import za.co.enerweb.toolbox.vaadin.messaging.MessageBusFactory;

import com.vaadin.Application;

public class TestRenderTimeMachineState {

    @BeforeClass
    public static void globalSetup() {
        MessageBusFactory.setTestMode(true);
    }

    MessageBus bus = MessageBusFactory.getTestInstance();

    @Test
    public void testgetTimeRangeIncOneDayOverTwoDays(){
        DateTime start = new DateTime(2006, 1, 1, 0, 0, 0, 0);
        DateTime end = new DateTime(2006, 1, 2, 0, 0, 0, 0);


        Interval timeWindow = new Interval(start, end);


        RenderTimeMachineState state = new RenderTimeMachineState();


        List<DateTime> dates = state.getTimeRange(timeWindow, TimeMachineStateContext.ONE_DAY);

        Assert.assertEquals(2, dates.size());



    }
    @Test
    public void testgetTimeRangeIncOneDayOver20Days(){
        DateTime start = new DateTime(2006, 1, 1, 0, 0, 0, 0);
        DateTime end = new DateTime(2006, 1, 30, 0, 0, 0, 0);


        Interval timeWindow = new Interval(start, end);


        RenderTimeMachineState state = new RenderTimeMachineState();


        List<DateTime> dates = state.getTimeRange(timeWindow, TimeMachineStateContext.ONE_DAY);

        Assert.assertEquals(30, dates.size());



    }

    @Test
    public void testgetTimeRangeIncOneDayOver1Year(){
        DateTime start = new DateTime(2006, 1, 1, 0, 0, 0, 0);
        DateTime end = new DateTime(2007, 1, 1, 0, 0, 0, 0);


        Interval timeWindow = new Interval(start, end);


        RenderTimeMachineState state = new RenderTimeMachineState();


        List<DateTime> dates = state.getTimeRange(timeWindow, TimeMachineStateContext.ONE_DAY);

        Assert.assertEquals(366, dates.size());



    }

    @Test
    public void testgetTimeRangeIncTwoDayOverTwoDays(){
        DateTime start = new DateTime(2006, 1, 1, 0, 0, 0, 0);
        DateTime end = new DateTime(2006, 1, 2, 0, 0, 0, 0);


        Interval timeWindow = new Interval(start, end);


        RenderTimeMachineState state = new RenderTimeMachineState();


        List<DateTime> dates = state.getTimeRange(timeWindow, TimeMachineStateContext.TWO_DAY);

        Assert.assertEquals(1, dates.size());



    }



    @Test
    public void testStateTimeRangeIncOneDayOverTwoDays(){
        DateTime start = new DateTime(2006, 1, 1, 0, 0, 0, 0);
        DateTime end = new DateTime(2006, 1, 2, 0, 0, 0, 0);
        Application app =
            mock(Application.class);

        Interval timeWindow = new Interval(start, end);


        TimeMachineStateContext ctx = new TimeMachineStateContext(
            app, timeWindow, TimeMachineStateContext.ONE_DAY);


        RenderTimeMachineState state = new RenderTimeMachineState();

        state.doWork(ctx, timeWindow, TimeMachineStateContext.ONE_DAY);

        ctx.getCurrentState();


        if (!(ctx.getCurrentState() instanceof EffectiveTimeChangeState)){
            Assert.fail("Should be in EffectiveTimeChangeState");
        }

        EffectiveTimeChangeState timeChangeState = (EffectiveTimeChangeState) ctx.getCurrentState();

        Assert.assertEquals(2, timeChangeState.getRenderedCache().size());



    }
}
