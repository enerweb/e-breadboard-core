package za.co.enerweb.ebr.gui.vaadin.time_machine;

import static org.mockito.Mockito.mock;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import za.co.enerweb.toolbox.vaadin.messaging.MessageBusFactory;

import com.vaadin.Application;


public class TestTimeMachineStateContext {

    @BeforeClass
    public static void globalSetup() {
        MessageBusFactory.setTestMode(true);
    }

    @Test
    public void test() {

        Application app =
            mock(Application.class);

        DateTime start = new DateTime(2006, 1, 1, 0, 0, 0, 0);
        DateTime end = new DateTime(2006, 1, 2, 0, 0, 0, 0);


        Interval timeWindow = new Interval(start, end);


        TimeMachineStateContext ctx = new TimeMachineStateContext(
            app, timeWindow, TimeMachineStateContext.ONE_DAY);


        ctx.doWork();//render


        if (!(ctx.getCurrentState() instanceof EffectiveTimeChangeState)){
            Assert.fail("Should be in EffectiveTimeChangeState");
        }

        EffectiveTimeChangeState timeChangeState = (EffectiveTimeChangeState) ctx.getCurrentState();

        Assert.assertEquals(2, timeChangeState.getRenderedCache().size());



        //
        ctx.doWork();//effective date change


        ctx.close();



    }

}
