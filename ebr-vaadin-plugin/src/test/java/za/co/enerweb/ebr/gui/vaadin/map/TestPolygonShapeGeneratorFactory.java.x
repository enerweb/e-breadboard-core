package za.co.enerweb.ebr.gui.vaadin.map;

import junit.framework.Assert;

import org.junit.Test;
import org.vaadin.vol.Area;
import org.vaadin.vol.Point;

public class TestPolygonShapeGeneratorFactory {

    @Test
    public void testTopLeft() {

        Area triangle =
            PolygonShapeGeneratorFactory.generateTrangle(TrangleType.TOP_LEFT,
                new Point(30, 20), 0.1);

        Point[] pointsInTriangle = triangle.getPoints();

        Assert.assertEquals(3, pointsInTriangle.length);

        Assert.assertEquals(30d, pointsInTriangle[0].getLon());
        Assert.assertEquals(20d, pointsInTriangle[0].getLat());
        //
        Assert.assertEquals(30d - 0.1, pointsInTriangle[1].getLon());
        Assert.assertEquals(20d, pointsInTriangle[1].getLat());
        //
        Assert.assertEquals(30d, pointsInTriangle[2].getLon());
        Assert.assertEquals(20d - 0.1, pointsInTriangle[2].getLat());

    }

    @Test
    public void testTopRight() {

        Area triangle =
            PolygonShapeGeneratorFactory.generateTrangle(TrangleType.TOP_RIGHT,
                new Point(30, 20), 0.1);

        Point[] pointsInTriangle = triangle.getPoints();

        Assert.assertEquals(3, pointsInTriangle.length);

        Assert.assertEquals(30d, pointsInTriangle[0].getLon());
        Assert.assertEquals(20d, pointsInTriangle[0].getLat());
        //
        Assert.assertEquals(30d - 0.1, pointsInTriangle[1].getLon());
        Assert.assertEquals(20d, pointsInTriangle[1].getLat());
        //
        Assert.assertEquals(30d, pointsInTriangle[2].getLon());
        Assert.assertEquals(20d + 0.1, pointsInTriangle[2].getLat());

    }

    @Test
    public void testBottomLeft() {

        Area triangle =
            PolygonShapeGeneratorFactory.generateTrangle(TrangleType.BOTTOM_LEFT,
                new Point(30, 20), 0.1);

        Point[] pointsInTriangle = triangle.getPoints();

        Assert.assertEquals(3, pointsInTriangle.length);

        Assert.assertEquals(30d, pointsInTriangle[0].getLon());
        Assert.assertEquals(20d, pointsInTriangle[0].getLat());
        //
        Assert.assertEquals(30d - 0.1, pointsInTriangle[1].getLon());
        Assert.assertEquals(20d, pointsInTriangle[1].getLat());
        //
        Assert.assertEquals(30d, pointsInTriangle[2].getLon());
        Assert.assertEquals(20d + 0.1, pointsInTriangle[2].getLat());

    }

    @Test
    public void testBottomRight() {

        Area triangle =
            PolygonShapeGeneratorFactory.generateTrangle(TrangleType.BOTTOM_RIGHT,
                new Point(30, 20), 0.1);

        Point[] pointsInTriangle = triangle.getPoints();

        Assert.assertEquals(3, pointsInTriangle.length);

        Assert.assertEquals(30d, pointsInTriangle[0].getLon());
        Assert.assertEquals(20d, pointsInTriangle[0].getLat());
        //
        Assert.assertEquals(30d + 0.1, pointsInTriangle[1].getLon());
        Assert.assertEquals(20d, pointsInTriangle[1].getLat());
        //
        Assert.assertEquals(30d, pointsInTriangle[2].getLon());
        Assert.assertEquals(20d + 0.1, pointsInTriangle[2].getLat());

    }

}
