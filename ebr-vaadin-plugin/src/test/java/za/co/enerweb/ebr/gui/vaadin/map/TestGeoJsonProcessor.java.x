package za.co.enerweb.ebr.gui.vaadin.map;

import static org.mockito.Mockito.mock;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import junit.framework.Assert;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.junit.Test;
import org.vaadin.vol.Area;
import org.vaadin.vol.Attributes;
import org.vaadin.vol.PolyLine;
import org.vaadin.vol.Vector;

import za.co.enerweb.toolbox.io.ResourceUtils;

public class TestGeoJsonProcessor {
    protected File getTestPointGeoJsonFile() throws IOException {
        return ResourceUtils.getResourceAsFile("mts_test.geojson");
    }
    protected File getTestPolygonGeoJsonFile() throws IOException {
        return ResourceUtils.getResourceAsFile("provinces_3_loadzones.geojson");
    }
    protected File getTestReducedPolygonGeoJsonFile() throws IOException {
        return ResourceUtils.getResourceAsFile("provinces_3_loadzones_reduced.geojson");
    }

    @Test
    public void testPointGeoJson() throws JSONException, IOException {

        GeoJsonListener listener = mock(GeoJsonListener.class);

        GeoJsonProcessor proc = new GeoJsonProcessor();
        proc.loadVector(getTestPointGeoJsonFile(), listener);

    }
//    @Test
    public void testPoligonGeoJson() throws JSONException, IOException {

        final List<Area> polygon = new ArrayList<Area>();

        GeoJsonListener listener = new GeoJsonListener() {

            @Override
            public void addPolygon(String featureId,String name, Area pointsInPolygon, String caption,
                String description, String colour) {
                polygon.add(pointsInPolygon);
            }

            @Override
            public Vector addMarker(String category, Vector vector, String caption,
                String description, String colour) {
                return null;
            }

            @Override
            public Vector addMarker(String category, Vector vector, String caption) {
                return null;
            }

            @Override
            public void addMarker(String category, PolyLine pLine, String colour,
                String caption) {
            }
        };

        GeoJsonProcessor proc = new GeoJsonProcessor();
        proc.loadVector(getTestPolygonGeoJsonFile(), listener);

        Assert.assertEquals(3,polygon.size());


    }
    @Test
    public void splitPoligonGeoJson() throws JSONException, IOException {


        HashMap<Area, StringBuffer> areaPoints = GeoJsonAdfProcessor.generateAdfInserts(getTestReducedPolygonGeoJsonFile(), "I_LOAD_PAR", "load_id");

        Collection<StringBuffer> polygons = areaPoints.values();

        int polyCount =1;
        for( Area area : areaPoints.keySet()){

            Attributes attr = area.getAttributes();

            Set<String> attrKeys = attr.keySet();

            Object prov = attr.getKeyValueMap().get("PROVNAME");

            if (prov == null){
                prov = "UNKNOWN";
            }

            StringBuffer polygonBuffer = areaPoints.get(area);
            System.out.println(polygonBuffer);
            System.out.println("---------------------------");

            FileUtils.writeStringToFile(new File("/tmp/p_inserts"+prov.toString()),polygonBuffer.toString());

        }

        Assert.assertEquals(3, polygons.size());


    }


}
