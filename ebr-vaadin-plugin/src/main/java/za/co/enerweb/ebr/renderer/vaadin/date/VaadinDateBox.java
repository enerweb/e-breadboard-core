package za.co.enerweb.ebr.renderer.vaadin.date;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import za.co.enerweb.ebr.VaadinPlugin;
import za.co.enerweb.ebr.annotations.EnvVarRenderer;
import za.co.enerweb.ebr.renderer.RendererMode;
import za.co.enerweb.ebr.renderer.vaadin.AbstractVaadinRenderer;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@EnvVarRenderer(id = VaadinDateBox.ID,
    technology = VaadinPlugin.TECHNOLOGY_NAME,
    supportedTypes = {"java.util.Date"},
    rankPerType = {80},
    supports = RendererMode.EDIT)
public class VaadinDateBox extends AbstractVaadinRenderer
                           implements ValueChangeListener {

    public static final String ID = "datebox";

    // create a list of date formats to try and convert string to date
    public static final List<String> sdfFormatList = new ArrayList<String>();
    static {
        sdfFormatList.add("yyyy-MM-dd HH:mm:ss");
        sdfFormatList.add("yyyy-MM-dd HH:mm");
        sdfFormatList.add("yyyy-MM-dd HH");
        sdfFormatList.add("yyyy-MM-dd");
    }

    private DateField field;
    private boolean inline;

    public VaadinDateBox() {

    }

    @Override
    public Component getComponent() {
        installAuxProperties();

        if (isInline()) {
            field = new InlineDateField();
        } else {
            field = new DateField();
        }

        field.setResolution(DateField.RESOLUTION_DAY);
        field.setDateFormat("yyyy-MM-dd");

        VerticalLayout layout = new VerticalLayout();
        layout.addComponent(field);

        // add a dummy label so the datefield doesn't get stretched
        layout.addComponent(new Label(""));

        Serializable obj = getValue();

        if (obj != null) {

            if (obj instanceof String) {
                Date date = tryConvert((String) obj);
                if (date != null) {
                    field.setValue(date);
                }
            } else if (obj instanceof Date) {
                field.setValue((Date) obj);
            } else if (obj instanceof GregorianCalendar) {
                field.setValue(((GregorianCalendar) obj).getTime());
            } else if (obj instanceof java.sql.Timestamp) {
                field.setValue(new Date(((java.sql.Timestamp) obj).getTime()));
            }
        }

        field.addListener(this);

        return layout;
    }

    protected void installAuxProperties() {
        setInline(getVariableProperty("inline", null) != null);
    }

    public boolean isInline() {
        return inline;
    }

    public void setInline(final boolean inline) {
        this.inline = inline;
    }

    /**
     * Try and convert a string to a date
     */
    protected Date tryConvert(final String dateStr) {
        for (String fmt : sdfFormatList) {
            SimpleDateFormat sdf = new SimpleDateFormat(fmt);

            try {
                return sdf.parse(dateStr);
            } catch (ParseException ignore) {
                // ignore, date not parseble by fmt. try next
            }
        }

        return null;
    }

    @Override
    public void valueChange(final ValueChangeEvent event) {
        Date date = (Date) field.getValue();

        setValue(date);
    }

}
