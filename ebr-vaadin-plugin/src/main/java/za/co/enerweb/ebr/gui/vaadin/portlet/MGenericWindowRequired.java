package za.co.enerweb.ebr.gui.vaadin.portlet;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import za.co.enerweb.toolbox.vaadin.messaging.Message;

@Data
@AllArgsConstructor
public class MGenericWindowRequired<Node> extends Message{


    private String windowRequired;
    private List<Node> selectedNodesMessage;
}
