package za.co.enerweb.ebr.gui.vaadin.components;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.exceptions.CouldNotFindTaskException;
import za.co.enerweb.ebr.gui.vaadin.EnvVarUtils;
import za.co.enerweb.ebr.gui.vaadin.IMainApplication;
import za.co.enerweb.ebr.gui.vaadin.JobProgressComponent;
import za.co.enerweb.ebr.gui.vaadin.JobProgressListener;
import za.co.enerweb.ebr.task.ITaskExecutorManager;
import za.co.enerweb.ebr.task.InputMetadata;
import za.co.enerweb.ebr.task.OutputMetadata;
import za.co.enerweb.ebr.task.RunMode;
import za.co.enerweb.ebr.task.TaskExecutionMetadata;
import za.co.enerweb.ebr.task.TaskMetadata;
import za.co.enerweb.toolbox.vaadin.Heading;

import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;

/**
 * A base vaadin component that can execute tasks.
 * It shows a progressbar inline.
 *
 * By default this sets it's size to fully expand.
 * Users can set the size to undefined if they want it to expand or shrink
 * according to the content (by calling setSizeUndefined() on this).
 * Doing that by default can be very unintuitive, because with html content
 * for example it shrinks down to 0px.
 */
@NoArgsConstructor
@Slf4j
public class TaskComponent extends VerticalLayout
    implements JobProgressListener {
    private static final long serialVersionUID = 1L;

    @Setter
    private String taskId;
    @Setter
    private IInputParameterProvider inputParameterProvider;

    private IMainApplication mainApplication;
    @Getter @Setter
    private TaskExecutionMetadata taskExecutionMetadata;

    @Getter @Setter
    private TaskMetadata taskMetadata;

    @Getter
    private transient EnvVarUtils envVarUtils;
    private Component currentRenderer;

    {
        setSizeFull();
//        setMargin(false);
//        setSpacing(false);
    }

    public TaskComponent(String taskId,
        IInputParameterProvider inputParameterProvider) {
        this.taskId = taskId;
        this.inputParameterProvider = inputParameterProvider;
    }

    public TaskComponent(TaskExecutionMetadata taskExecutionMetadata) {
        this.taskExecutionMetadata = taskExecutionMetadata;
        taskMetadata = taskExecutionMetadata.getTaskMetadata();
        taskId = taskMetadata.getId();
    }

    /**
     * Can only be done if this has been attached already.
     */
    public void init() {
        removeAllComponents();
        if (taskId == null) {
            throw new IllegalStateException("taskId is required");
        }

        ITaskExecutorManager taskExecutorManager =
            EbrFactory.getTaskExecutorManager();
        // mainApplication = (IMainApplication) getApplication();
        envVarUtils = null;
        try {
            if (taskMetadata == null) {
                taskMetadata = EbrFactory.getTaskRegistry().getTaskMetadata(
                    null, taskId);
            }
            if (taskExecutionMetadata == null) {
                taskExecutionMetadata =
                    taskExecutorManager.initTaskExecution(taskMetadata,
                        mainApplication.getSessionId(), null);
            }
        } catch (CouldNotFindTaskException e) {
            addComponent(new Heading(
                e.getLocalizedMessage()));
        } catch (Exception e) {
            log.error("Error executing task "+e.getMessage(),e);
            addComponent(new Heading("Could not instantiate task: "
                + e.getLocalizedMessage()));
            // log.debug("Could not instantiate task", e);
        }
        envVarUtils = new EnvVarUtils(getSession());
    }

    private boolean initialized() {
        return envVarUtils != null && taskExecutionMetadata != null
            && taskMetadata != null;
    }

    public void showOutputParameter(String parameterName) {
        if (!initialized()) {
            return;
        }

        Application app = getApplication();
        if (app != null) {
            // this gets called from events, so make sure we are in sync
            synchronized (app) {
                removeAllComponents();
                // save current renderer so that
                // derived classes can have access to it
                OutputMetadata outputMetadata = taskMetadata.getOutputMetadata(
                    parameterName);
                if (outputMetadata == null) {
                    throw new IllegalStateException(
                        "There is no parameter called: " + parameterName
                            + " in task: " + taskId + " "
                            + taskMetadata.getTitle());
                }
                currentRenderer = envVarUtils.getJobRendererComponent(
                    taskExecutionMetadata,
                    outputMetadata, null,
                    false);
                addComponent(currentRenderer);
            }
        }
    }

    /**
     * Brings up an elegant progress indicator while the
     * task is running.
     */
    public void runTaskAndShowProgress() {
        if (!initialized()) {
            return;
        }
        runTask();

        JobProgressComponent jpc = new JobProgressComponent();
        // tasks running as a frontend should not take too long
        jpc.setJobPollIntervalMilliseconds(200);
        jpc.setShowTimeInfo(false);
//        jpc.setOnlyShowDoneStatus(true);
        jpc.addJobProgressListener(this);
        VerticalLayout vl = new VerticalLayout();
        vl.addComponent(new Heading(taskExecutionMetadata.getTaskMetadata().getTitle()));
        vl.addComponent(jpc);
        addComponent(vl);
        setExpandRatio(vl, 1f);
        jpc.setTaskExecutionMetadata(taskExecutionMetadata);
        jpc.start();
    }

    public void runTask() {
        if (!initialized()) {
            return;
        }
        setInputParameters();

        ITaskExecutorManager taskExecutorManager =
            EbrFactory.getTaskExecutorManager();
        try {
            taskExecutorManager.run(taskExecutionMetadata.getJobId(),
                RunMode.INTERACTIVE);
        } catch (Exception e) {
            addComponent(new Heading("Could not execute task: "
                + e.getLocalizedMessage()));
        }
    }

    private void setInputParameters() {
        if (inputParameterProvider == null) {
            throw new IllegalStateException(
                "inputParameterProvider is required");
        }
        for (InputMetadata inputMetadata : taskMetadata.getInputMetadata()) {
            Serializable value = inputParameterProvider.getParameterValue(
                inputMetadata.getName(),
                taskMetadata, this);

            if (value != null) {
                new EnvVarUtils(getApplication()).setValue(
                    taskExecutionMetadata.getInputIEnvironmentId(),
                    inputMetadata, value);
            }
        }
    }

    @Override
    public void contentUpdated() {
    }

    @Override
    public void finalizingAterTaskDone(TaskExecutionMetadata taskExecutionMetadata) {
        this.taskExecutionMetadata = taskExecutionMetadata;
        // maybe we should rather send a task complete message
        // so that others can register listeners on
        // the renderer if they want to
        // source component, job metadata, renderer component
    }

    public Component getRenderer() {
        return currentRenderer;
    }

}
