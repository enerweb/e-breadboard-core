package za.co.enerweb.ebr.renderer.vaadin.dataresource;

import lombok.Getter;
import za.co.enerweb.ebr.server.HomeFile;

import com.vaadin.server.Resource;
import com.vaadin.server.StreamResource;

/**
 * for showing HomeFiles
 */
public class HomeFileLink extends AbstractLink {

    private static final long serialVersionUID = 1L;
    @Getter
    private HomeFile homeFile;

    public HomeFileLink(HomeFile homeFile) {
        this(homeFile, null);
    }

    public HomeFileLink(HomeFile homeFile, String caption) {
        super(caption);
        this.homeFile = homeFile;
        if (homeFile != null) {
            setDescription(homeFile.getAbsolutePath());
        }
    }

    public String createCaption() {
        if (homeFile != null) {
            return homeFile.getName();
        } else {
            return "null";
        }
    }

    public Resource getResource() {
        if (homeFile != null) {
            StreamResource streamResource = new StreamResource(
                new HomeFileStreamSource(homeFile),
                homeFile.getName());
            streamResource.setCacheTime(0);
            return streamResource;
        }
        return null;
    }

}
