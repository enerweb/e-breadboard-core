package za.co.enerweb.ebr.renderer.vaadin.string;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import za.co.enerweb.ebr.VaadinPlugin;
import za.co.enerweb.ebr.annotations.EnvVarRenderer;
import za.co.enerweb.ebr.env.EnvVarMetadata;
import za.co.enerweb.ebr.renderer.RendererMode;
import za.co.enerweb.ebr.renderer.vaadin.AbstractVaadinRenderer;

import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.AbstractSelect.Filtering;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;

@EnvVarRenderer(id = SuggestBox.ID,
        technology = VaadinPlugin.TECHNOLOGY_NAME,
        supportedTypes = { "java.lang.String" },
        rankPerType = { 0 },
        supports = RendererMode.EDIT)
public class SuggestBox extends AbstractVaadinRenderer implements
        ValueChangeListener {

    private static final long serialVersionUID = 1L;

    public static String AUX_OPTIONS_PROP = "options";

    public static final String ID = "suggestbox";
    private static final String PROP_NAME = "name";
    private static final String PROP_ID = "id";

    private ComboBox cmbSuggest;

    private IndexedContainer container;

    @Override
    public Component getComponent() {
        EnvVarMetadata metaData = getVariableMetaData();
        container = new IndexedContainer();
        container.addContainerProperty(PROP_ID, String.class, null);
        container.addContainerProperty(PROP_NAME, String.class, null);

        Serializable data = getVariableProperty(AUX_OPTIONS_PROP,
                new ArrayList<String>());
        // populate the combo box
        populateContainer(data);
        // create the combo box ui component
        cmbSuggest = new ComboBox("", container);
        cmbSuggest.setSizeFull();
        cmbSuggest.setImmediate(true);
        cmbSuggest.setFilteringMode(Filtering.FILTERINGMODE_CONTAINS);
        // cmbSuggest.setWidth(350, Sizeable.UNITS_PIXELS);
        cmbSuggest.setInputPrompt(metaData.getDescription());
        cmbSuggest.setItemCaptionPropertyId(PROP_NAME);
        cmbSuggest
                .setItemCaptionMode(AbstractSelect.ITEM_CAPTION_MODE_PROPERTY);
        cmbSuggest.setNullSelectionAllowed(false);
        cmbSuggest.addListener(this);
        // select current value in the combobox
        Serializable value = getValue();
        if (value != null) {
            cmbSuggest.setValue(value);
        }
        return cmbSuggest;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    protected void populateContainer(Serializable data) {
        if (data instanceof List) {
            // support lists where the value = display
            for (Object o : (List) data) {
                String value = o.toString();
                Item item = container.addItem(value);
                item.getItemProperty(PROP_ID).setValue(value);
                item.getItemProperty(PROP_NAME).setValue(value);
            }
            container.sort(new Object[] { "name" }, new boolean[] { true });
        } else if (data instanceof Map) {
            Map map = (Map) data;
            Iterator<Map.Entry> iterator = map.entrySet().iterator();
            while (iterator.hasNext()) {
                Entry entry = iterator.next();
                Object key = entry.getKey();
                Object value = entry.getValue();
                Item item = container.addItem(value);
                item.getItemProperty(PROP_ID).setValue(key);
                item.getItemProperty(PROP_NAME).setValue(value.toString());
            }
        }
    }

    @Override
    public void valueChange(final ValueChangeEvent e) {
        setValue((Serializable) e.getProperty().getValue());
    }

}
