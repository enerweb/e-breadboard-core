package za.co.enerweb.ebr.renderer.vaadin.string;

import java.io.Serializable;

import za.co.enerweb.ebr.VaadinPlugin;
import za.co.enerweb.ebr.annotations.EnvVarRenderer;
import za.co.enerweb.ebr.renderer.RendererMode;
import za.co.enerweb.ebr.renderer.vaadin.AbstractVaadinRenderer;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.TextField;

@SuppressWarnings("serial")
@EnvVarRenderer(id = TextBox.ID,
    technology = VaadinPlugin.TECHNOLOGY_NAME,
    supportedTypes = {
        "java.lang.Double",
        "java.lang.Integer",
        "java.lang.String"},
    rankPerType = {50, 50, 50},
    supports = RendererMode.EDIT)
public class TextBox extends AbstractVaadinRenderer
    implements ValueChangeListener {
    public static final String ID = "textbox";
    private TextField field = new TextField();

    /*
     * (non-Javadoc)
     * @see za.co.enerweb.ebr.renderer.VaadinRenderer#getComponent()
     */
    @Override
    public Component getComponent() {
        Serializable value = getValue();
        if (value != null) {
            field.setValue((String) value);
        }
        // field.setWidth("10em");
        field.setWidth("99.9%");
        field.addListener(this);
        return field;
    }

    /*
     * (non-Javadoc)
     * @see com.vaadin.data.Property.ValueChangeListener#valueChange(
     * com.vaadin.data.Property.ValueChangeEvent)
     */
    @Override
    public void valueChange(final ValueChangeEvent event) {
        setValue((String) field.getValue());
    }

}
