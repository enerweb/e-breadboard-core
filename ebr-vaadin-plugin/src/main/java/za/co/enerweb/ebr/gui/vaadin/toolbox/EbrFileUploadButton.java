package za.co.enerweb.ebr.gui.vaadin.toolbox;

import java.io.File;
import java.io.IOException;

import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.toolbox.vaadin.FileUploadButton;

/**
 * Simplified vaadin upload button.
 * (Creates a Ebr temp file)
 */
public class EbrFileUploadButton extends FileUploadButton {

    public EbrFileUploadButton(FileReceiver fileReceiver) {
        super(fileReceiver);
    }

    public EbrFileUploadButton(String caption, FileReceiver fileReceiver) {
        super(caption, fileReceiver);
        setMargin(false);
    }

    protected File createTempFile(final String filename) throws IOException {
        return EbrFactory.getHomeService().getTempFile(filename).getFile();
    }
}
