package za.co.enerweb.ebr.gui.vaadin.menu;

import java.io.Serializable;

/**
 * We are using an abstract factory class because maybe we would like
 * to inject more context, then we don't want to change all implementations.
 */
@SuppressWarnings("serial")
public abstract class AbstractMenuFactory implements Serializable,
Comparable<AbstractMenuFactory> {

    public static String MAIN_MENU_NAME = "main";
    public static final String TOOLS_MENU_NAME = "Tools";
    public static final String CONFIG_MENU_NAME = "Config";

    /**
     * @param menuName you should check if this is "main" if you want
     *        to add stuff to the main menu. Maybe in future there may be other
     *        menus, who knows?
     * @param menuContainer
     */
    public void addToMenu(final String menuName,
        final MenuItemContainer menuContainer){
        if (menuName.equals(MAIN_MENU_NAME)) {
            addToMainMenu(menuContainer);
        }
    }

    /**
     * you only need to override this if you only want to add stuff to the
     * main menu
     * @param menuContainer
     */
    public void addToMainMenu(final MenuItemContainer menuContainer) {
    }

    /**
     * You can influence the order in which the Menu Factories are are
     * processed, in order to force the rank of sub menus.
     * Lowest rank is executed first.
     * @return
     */
    public int getRank() {
        return 1000;
    }

    public final int compareTo(AbstractMenuFactory o) {
        int ret = getRank() - o.getRank();
        if (ret == 0) {
            // we need a consistent tie breaker if for some reason
            // the ranks are equal
            ret = getClass().getName().compareToIgnoreCase(
                o.getClass().getName());
        }
        return ret;
    }
}
