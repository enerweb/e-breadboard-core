package za.co.enerweb.ebr.renderer.vaadin.jquery;

import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.VaadinPlugin;
import za.co.enerweb.ebr.VaadinUrlService;
import za.co.enerweb.ebr.annotations.EnvVarRenderer;
import za.co.enerweb.ebr.renderer.RendererMode;
import za.co.enerweb.ebr.renderer.vaadin.AbstractVaadinRenderer;
import za.co.enerweb.ebr.renderer.vaadin.jquery.InteractiveMap.Artifact;
import za.co.enerweb.ebr.renderer.vaadin.jquery.InteractiveMap.MapItem;
import za.co.enerweb.ebr.renderer.vaadin.jquery.InteractiveMap.MapItemType;
import za.co.enerweb.ebr.renderer.vaadin.jquery.InteractiveMap.Region;
import za.co.enerweb.vaadin.jquery.JQueryListener;

import com.vaadin.Application;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;

/*
 * TODO: You should define a custom type with a converter see:
 *   za.co.enerweb.ebr.renderer.vaadin.dataresource.DocumentRenderer and
 *   za.co.enerweb.ebr.datatype.File  and maybe
 *   za.co.enerweb.r.examples.file_output.R
 * The user should not have to choose the renderer, he should
 * only need to specify the type. If we ever write a better renderer
 * for something we can just give the new one a higher rank
 * and it will automatically be used from then on, unless
 * the user hardcodes which renderer to use.
 * Be more explicit in the supported types: String and list.
 */
@EnvVarRenderer(id = InteractiveMapRenderer.ID,
        technology = VaadinPlugin.TECHNOLOGY_NAME,
        supportedTypes = { "java.io.Serializable" },
        rankPerType = { 1 }, supports = RendererMode.EDIT_AND_VIEW)
@Slf4j
public class InteractiveMapRenderer extends AbstractVaadinRenderer implements
        JQueryListener {
    private static final long serialVersionUID = 1L;
    public static final String ID = "interactivemap";
    public static String AUX_PROP_MAPID = "mapid";

    public static String PARM_IMAGE = "image";
    public static String PARM_READ_ONLY = "readonly";
    public static String PARM_SELECTED = "selected";
    private InteractiveMap map;

    private List<Region> regions = new ArrayList<Region>();
    private List<Artifact> artifacts = new ArrayList<Artifact>();
    private Map<String, MapItem> lookup = new HashMap<String, MapItem>();
    // use when the renderer is used as an input
    private String selectedItem;

    @Override
    public Component getComponent() {
        return initMapComponent();
    }

    @SuppressWarnings("unchecked")
    protected AbstractComponent initMapComponent() {
        // the map id which will be used to create a JS object
        // representing the gwt widget
        String mapid = getVariableProperty(AUX_PROP_MAPID, null);
        String jsMapObj = "document." + mapid;
        Serializable value = getValue();
        if (value instanceof List) {
            List<Object> data = (List<Object>) value;
            if (data.size() != 2) {
                return new Label(
                        "Found invalid output parameter. "+
                        "Expect a list with 2 values: first value config data" +
                        " and the second one with values");
            }

            // collect all the map artifacts & regions
            populateItems((List<Map<String, Object>>)data.get(1));
            // get the template variables
            Map<String, Object> parms = (Map<String, Object>) data.get(0);
            String html = createTemplate(mapid, parms, regions);
            // replace template variables
            if (html == null) {
                return new Label("There was an error creating the template. "
                    + "Check the log file for details");
            }
            // make the map readonly
            String jsReadOnly = "";
            if (isMapReadOnly(parms)) {
                jsReadOnly = jsMapObj + ".makereadonly();";
            }
            // create the map and add artifacts
            String itemKey = getSelectedItemParameter(parms);
            String jsSelectItem = createItemSelectJavaScript(jsMapObj, itemKey);
            map = new InteractiveMap(mapid, html);
            map.setJsInitCode("init_" + mapid + "(" + jsMapObj + ");");
            map.setCustomMapInitCode("$(function() {" + jsSelectItem
                    + jsReadOnly + "});");
            //map.addJQueryListener(this);
        } else if (value instanceof String){
            String selected = (String) value;
            boolean selectedEmpty = selected == null || selected.length() == 0;
            // use the map as an input parameter
            // collect the regions and artifacts
            Serializable items = getVariableProperty("items",
                    new ArrayList<Map<String, Object>>());
            populateItems((List<Map<String, Object>>) items);
            // get the selected item and create javascript to select that item
            String jsSelectItem = createItemSelectJavaScript(jsMapObj,
                selected);
            // collect the parameters, but from the auxiliary properties
            Map<String, Object> parms = createParmsFromAuxProperties();
            String html = createTemplate(mapid, parms, regions);
            // replace template variables
            if (html == null) {
                return new Label("There was an error creating the template. "
                    + "Check the log file for details");
            }
            // make the map readonly if no value is selected
            String jsReadOnly = "";
            if (!selectedEmpty) {
                jsReadOnly = jsMapObj + ".makereadonly();";
            }
            map = new InteractiveMap(mapid, html);
            map.setJsInitCode("init_" + mapid + "(" + jsMapObj + ");");
            map.setCustomMapInitCode("$(function() {" + jsSelectItem
                + jsReadOnly + "});");
            // only add a listener if a value hasn't been selected
            if (selectedEmpty) {
                map.addJQueryListener(this);
            }
        }

        for (Artifact artifact : artifacts) {
            map.add(artifact);
        }
        return map;
    }

    protected String createItemSelectJavaScript(String jsMapObj,
        String itemKey) {
        if (itemKey != null && lookup.containsKey(itemKey)) {
            MapItem mi = lookup.get(itemKey);
            if (mi instanceof Region)
                return jsMapObj + ".hilightarea('" + itemKey + "');\n";
            else if (mi instanceof Artifact)
                return  "setTimeout(function() {" + jsMapObj
                        + ".show('" + itemKey + "');}, 200);\n";
        }
        return "";
    }

    protected void populateItems(List<Map<String, Object>> rawValues) {
        artifacts.clear();
        regions.clear();
        lookup.clear();
        for (Map<String, Object> m : rawValues) {
            MapItem mapItem = InteractiveMap.createMapItem(m);
            lookup.put(mapItem.getKey(), mapItem);
            if (mapItem.getType() == MapItemType.ARTIFACT)
                artifacts.add((Artifact) mapItem);
            else if (mapItem.getType() == MapItemType.REGION)
                regions.add((Region) mapItem);
        }
    }

    protected Map<String, Object> createParmsFromAuxProperties() {
        Map<String, Object> parms = new HashMap<String, Object>();
        parms.put(PARM_IMAGE, getVariableProperty(PARM_IMAGE, null));
        parms.put(PARM_READ_ONLY, getVariableProperty(PARM_READ_ONLY, false));

        return parms;
    }

    protected String createTemplate(String mapid, Map<String, Object> parms,
            List<Region> regions) {
        String image = (String) parms.get(PARM_IMAGE);
        if (image != null) {
            image = makeWebContentPath(image);
            int[] size = readImageSize(image);
            StringBuilder builder = new StringBuilder();
            builder.append("<div id=\"");
            builder.append(mapid);
            builder.append("_wrapper\" class=\"mapwrapper\">\n");
            builder.append("<img id=\"" + mapid + "img\" class=\"map\" src=\"");
            builder.append(image);
            builder.append("\"");
            if (size != null) {
                // set image size if it's available
                builder.append(" width=\"" + size[0] + "\"");
                builder.append(" height=\"" + size[1] + "\"");
            }
            builder.append(" usemap=\"#" + mapid + "_imagemap\"/>\n");
            builder.append("<map name=\"" + mapid + "_imagemap\">\n");
            for (Region region : regions) {
                builder.append("<area href=\"#\" class=\"");
                builder.append(region.getKey());
                builder.append("\" shape=\"poly\" title=\"");
                builder.append(region.getCaption());
                builder.append("\" coords=\"");
                builder.append(region.getCoords());
                builder.append("\" />\n");
            }
            builder.append("</map>\n");
            builder.append("</div>");
            return builder.toString();
        } else {
            warnMissingParm(PARM_IMAGE);
            return null;
        }
    }

    protected boolean isMapReadOnly(Map<String, Object> parms) {
        if (parms.containsKey(PARM_READ_ONLY))
            return (Boolean) parms.get(PARM_READ_ONLY);
        return true;
    }

    protected String getSelectedItemParameter(Map<String, Object> parms) {
        if (parms.containsKey(PARM_SELECTED))
            return (String) parms.get(PARM_SELECTED);
        return null;
    }

    private int[] readImageSize(String imageUrl) {
        try {
            Application app = getApplication();
            VaadinUrlService vus = VaadinUrlService.getInstance(app);
            String contextPath = vus.getContextPath();
            String path = app.getURL().toExternalForm();
            path = path.replace(contextPath + "/", "");
            // Read image from a URL
            URL url = new URL(path + imageUrl);
            BufferedImage image = ImageIO.read(url);
            int width = image.getWidth();
            int height = image.getHeight();
            return new int[] { width, height };
        } catch (Throwable e) {
            log.error("Couldn't determine image size from url "
                + imageUrl, e);
            return null;
        }
    }

    private void warnMissingParm(String name) {
        log.warn("Couldn't find " + name
            + " parameter. html contents may be incorrect.");
    }

    private String makeWebContentPath(String path) {
        VaadinUrlService vus = VaadinUrlService.getInstance(getApplication());
        String wcp = vus.getWebContentRelUrlPath();
        if (!path.startsWith("/")) {
            path = wcp + "/" + path;
        }

        return path;
    }

    public InteractiveMap getMap() {
        return map;
    }

    @Override
    public void onEventOccurred(String event, Object value) {
        if (event.equals("area_click")) {
            selectedItem = (String) value;

            if (map != null) {
                map.execThis("hilightarea('" + selectedItem + "');");
            }
        } else if (event.equals("artifact_click")) {
            selectedItem = (String) value;
            if (map != null) {
                map.execThis("unhilightarea();");
                map.execThis("show('" + selectedItem + "');");
            }
        } else {
            // N/A event
            return;
        }
        setValue(selectedItem);
    }

    public String getSelectedItem() {
        return selectedItem;
    }
}
