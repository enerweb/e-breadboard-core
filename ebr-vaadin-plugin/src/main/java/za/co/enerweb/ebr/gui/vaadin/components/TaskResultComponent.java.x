package za.co.enerweb.ebr.gui.vaadin.components;

import lombok.NoArgsConstructor;
import lombok.Setter;
import za.co.enerweb.ebr.task.TaskExecutionMetadata;
import za.co.enerweb.ebr.task.TaskExecutionStatus;

/**
 * Executes the task and display one result.
 * For more complicated requirements clone this.
 */
@NoArgsConstructor
public class TaskResultComponent extends TaskComponent {
    private static final long serialVersionUID = 1L;
    @Setter
    private String outputParameterName;
    private boolean populated = false;

    public TaskResultComponent(String taskId,
        IInputParameterProvider inputParameterProvider,
        String outputParameterName) {
        super(taskId, inputParameterProvider);
        this.outputParameterName = outputParameterName;
    }

    public void attach() {
        super.attach();
        if (!populated) { // by default only run once
            init();
            populated = true;
            runTaskAndShowProgress();
        }
    }

    @Override
    public void finalizingAterTaskDone(TaskExecutionMetadata taskExecutionMetadata) {
        super.finalizingAterTaskDone(taskExecutionMetadata);
        if (outputParameterName == null) {
            throw new IllegalStateException(
                "outputParameterName is required");
        }
        if (taskExecutionMetadata.getStatus().equals(TaskExecutionStatus.SUCCESS)) {
            showOutputParameter(outputParameterName);
        }

    }
}
