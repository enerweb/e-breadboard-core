package za.co.enerweb.ebr.gui.vaadin;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.exceptions.UnknownTaskExecutionException;
import za.co.enerweb.ebr.task.TaskExecutionMetadata;
import za.co.enerweb.ebr.task.ITaskExecutorManager;
import za.co.enerweb.ebr.task.TaskExecutionStatus;
import za.co.enerweb.toolbox.vaadin.Heading;

import com.google.common.collect.ImmutableList;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.ProgressIndicator;
import com.vaadin.ui.themes.BaseTheme;

/**
 * Fixme - show the times when this is clicked
 */
@Slf4j
public class JobProgressComponent extends GridLayout
    implements Runnable, ClickListener {
    private static final String STATUS_CAPTION = "Status:";

    private static final long serialVersionUID = 1L;

    private static final int MILLIS_TO_WAIT_BEFORE_STARTING_PROGRESS = 1000;
    private static final int INITIAL_POLL_INTERVAL_MILLIS = 10;
    private static final int INITIAL_WAIT_COUNT =
        MILLIS_TO_WAIT_BEFORE_STARTING_PROGRESS
            / INITIAL_POLL_INTERVAL_MILLIS;

    // XXX: this should probably be configurable
    private static final int JOB_POLL_INTERVAL_MILLIS = 1100;
    private static final long JOB_POLL_TIMEOUT_MILLIS = 1000 * 60 * 60 * 12;

    @Getter
    @Setter
    private int jobPollIntervalMilliseconds = JOB_POLL_INTERVAL_MILLIS;

    private transient ITaskExecutorManager jobManager;

    @Getter
    @Setter
    private TaskExecutionMetadata taskExecutionMetadata;
    private Heading statusCaption = new Heading();
    private Button detailLink = new Button("(Detail)");

    private Heading statusHeading = new Heading();
    private Heading startTimeHeading = new Heading();
    private Heading stopTimeHeading = new Heading();
    private Heading durationHeading = new Heading();
    private Heading messageHeading = new Heading();
    private ProgressIndicator progressIndicator;
    private transient Thread jobPollerThread = null;
    private List<JobProgressListener> progressListeners =
        new ArrayList<JobProgressListener>();

    @Getter
    @Setter
    private boolean showTimeInfo = true;
    @Getter
    @Setter
    private boolean onlyShowDoneStatus = false;

    @Getter
    @Setter
    private boolean statusIsLink = true;

    private int indecatorRow = 1;
    private int messageRow;

    public JobProgressComponent() {
        populate();
        detailLink.setStyleName(BaseTheme.BUTTON_LINK);
        detailLink.addStyleName("v-label");
        detailLink.addListener(this); // react to clicks
        setWidth("280px");
    }

    public void addJobProgressListener(JobProgressListener listener) {
        progressListeners.add(listener);
    }

    public void populate() {
        // setSizeFull();
        setSpacing(true);
        setMargin(true);

        progressIndicator = new ProgressIndicator();
        progressIndicator.setVisible(false);
        progressIndicator.setPollingInterval(jobPollIntervalMilliseconds);
        progressIndicator.setSizeFull();

    }

    public void reset() {
        stop();
        // removeAllComponents();
    }

    public void stop() {
        // the results does not appear if you do the following ?!
//        if (jobPollerThread != null) {
//            jobPollerThread.interrupt();
//            jobPollerThread = null;
//        }
    }

    public void detach() {
        super.detach();
        stop();
    }

    protected void updateContent() {
        synchronized (getApplicationMutex()) {
            if (taskExecutionMetadata.getStatus().equals(TaskExecutionStatus.INITIALISED)) {
                statusCaption.setContent("");
                detailLink.setCaption("");
                statusHeading.setContent("");
            } else if (taskExecutionMetadata.getStatus().isDone()
                || !onlyShowDoneStatus) {
                detailLink.setCaption("(Detail)");
                statusCaption.setContent(STATUS_CAPTION);
                statusHeading.setContent(taskExecutionMetadata.getStatus().toString());
            }
            if (showTimeInfo) {
                startTimeHeading.setContent(taskExecutionMetadata.getStartTimeString());
                stopTimeHeading.setContent(taskExecutionMetadata.getStopTimeString());
                durationHeading.setContent(taskExecutionMetadata.getDurationString());
            }
            messageHeading.setContent(taskExecutionMetadata.getMessage());
            if (progressIndicator != null) {
                progressIndicator.setValue(taskExecutionMetadata.getProgress());
            }

            // needed in order for the window to recalculate its size
//            setSizeFull();
            // requestRepaint();
        }
        for (JobProgressListener listener : progressListeners) {
            listener.contentUpdated();
        }
    }

    public Object getApplicationMutex() {
        Object mutex = getApplication();
        if (mutex == null) {
            // when running from tests the application is null
            mutex = "not running in application";
        }
        return mutex;
    }

    public synchronized void start() {
        final String jobId = taskExecutionMetadata.getJobId();
        for (int i = 0; i < INITIAL_WAIT_COUNT
            && taskExecutionMetadata.getStatus().isBusy(); i++) {
            try {
                Thread.sleep(INITIAL_POLL_INTERVAL_MILLIS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            updateTaskExecutionMetadata();
        }
        if (taskExecutionMetadata.getStatus().isBusy()) {
            jobPollerThread = new Thread(this, "JobPoller: " + jobId);
            jobPollerThread.setDaemon(true);
            jobPollerThread
                .setPriority(Thread.currentThread().getPriority() - 1);
            activateProgressIndicator();
            jobPollerThread.start();
        } else {
            fireFinalizingAterTaskDone();
        }
        update();
    }

    public void update() {
        if (taskExecutionMetadata == null) {
            return;
        }
        log.debug(taskExecutionMetadata.getStatus().toString());
        removeAllComponents();
        setColumns(3);
        setRows(6);
        setColumnExpandRatio(0, 1f);
        setColumnExpandRatio(1, 1f);
        setColumnExpandRatio(2, 0f);
        indecatorRow = 1;
        messageRow = 0;
        addComponent(statusCaption, 0, 0);
        if (showTimeInfo) {
            addComponent(new Heading("Start Time:"), 0, 1);
            addComponent(new Heading("Stop Time:"), 0, 2);
            addComponent(new Heading("Duration:"), 0, 3);
            indecatorRow = 4;
        }
        messageRow = indecatorRow + 1;

        addComponent(statusHeading, 1, 0);
        if (statusIsLink) {
            addComponent(detailLink, 2, 0);
        }
        this.setComponentAlignment(statusHeading, Alignment.TOP_LEFT);

        if (showTimeInfo) {
            addComponent(startTimeHeading, 1, 1);
            addComponent(stopTimeHeading, 1, 2);
            addComponent(durationHeading, 1, 3);
        } else {
            // remove excess spacing at the bottom
            for (int i = messageRow + 1; i < getRows(); i++) {
                removeRow(i);
            }
        }

        addComponent(messageHeading, 0, messageRow, 2, messageRow);

        addComponent(progressIndicator, 0, indecatorRow, 2, indecatorRow);

//        if (jobPollerThread != null) {
//            activateProgressIndicator();
//        } else {
//            deactivateProgressIndicator();
//        }

        final String jobId = taskExecutionMetadata.getJobId();

        // Iterator<Component> i = getComponentIterator();
        // while(i.hasNext()) {
        // Component c = i.next();
        // c.addStyleName("ebr-env-var-heading-cell");
        // c.addStyleName("ebr-env-var-grid");
        // }
        updateContent();
    }

    public ITaskExecutorManager getTaskExecutorManager() {
        if (jobManager == null) {
            jobManager = EbrFactory.getTaskExecutorManager();
        }
        return jobManager;
    }

    @Override
    public void run() {

        long pollingMillis = 0;
        // updateTaskExecutionMetadata();
        // updateContent();

        // XXX: when nobody references this job from the frontend
        // we should probably terminate this thread. how?
        while (taskExecutionMetadata.getStatus().isBusy() &&
            pollingMillis < JOB_POLL_TIMEOUT_MILLIS) {
            try {
                pollingMillis +=
                    jobPollIntervalMilliseconds;
                Thread.sleep(jobPollIntervalMilliseconds);
            } catch (InterruptedException e) {
                log.info("Job status poller got interupted.", e);
                break;
            }
            updateTaskExecutionMetadata();
            updateContent();
        }

        if (pollingMillis >= JOB_POLL_TIMEOUT_MILLIS) {
            log.info("Stopped polling job {} after {} ms.",
                taskExecutionMetadata.getJobId(), pollingMillis);
        }

        // make sure we have the latest status
        // waitAWhile();
        // updateTaskExecutionMetadata(taskExecutionMetadata.getJobId());
        // updateContent();

        // give dialog a chance to update before updating the
        // renderers.
        // waitAWhile();
        synchronized (getApplicationMutex()) {
            if (progressIndicator != null) {
                progressIndicator.setIndeterminate(true);
            }
            messageHeading.setContent("Loading results.");
//            requestRepaint();
        }
        // waitAWhile();
        fireFinalizingAterTaskDone();
        updateContent();
//        waitAWhile();
        synchronized (getApplicationMutex()) {
            requestRepaint();
            deactivateProgressIndicator();
        }
    }

    public void fireFinalizingAterTaskDone() {
        synchronized (getApplicationMutex()) {
            for (JobProgressListener listener : ImmutableList
                .copyOf(progressListeners)) {
                log.debug("finalizingAterTaskDone: " + listener);
                listener.finalizingAterTaskDone(taskExecutionMetadata);
            }
        }
    }

    public void updateTaskExecutionMetadata() {
        try {
            taskExecutionMetadata = updateTaskExecutionMetadata(taskExecutionMetadata.getJobId());
        } catch (UnknownJobException e1) {
            taskExecutionMetadata.setStatus(TaskExecutionStatus.FAIL);
            taskExecutionMetadata.setMessage("Unknown job: "
                + e1.getLocalizedMessage());
        } catch (Exception e) {
            taskExecutionMetadata.setStatus(TaskExecutionStatus.FAIL);
            taskExecutionMetadata.setMessage("Could not run job: "
                + e.getLocalizedMessage());
        }
    }

    private void waitAWhile() {
        try {
            Thread.sleep(jobPollIntervalMilliseconds / 2);
        } catch (InterruptedException e) {
            log.info("Job status poller got interupted.");
        }
    }

    /**
     * for testing
     */
    public synchronized void join() {
        if (jobPollerThread != null) {
            try {
                jobPollerThread.join();
            } catch (InterruptedException e) {
                log.error("Interupted while waiting for poller", e);
            }
        }
    }

    private TaskExecutionMetadata updateTaskExecutionMetadata(final String jobId)
        throws UnknownJobException {
        taskExecutionMetadata = getTaskExecutorManager().getTaskExecutionMetadata(jobId);
        return taskExecutionMetadata;
    }

    public void activateProgressIndicator() {
        if (jobPollerThread != null && progressIndicator != null) {
            // log.debug("activateProgressIndicator");
            progressIndicator.setVisible(true);
            progressIndicator.setEnabled(true);
        }
    }

    public void deactivateProgressIndicator() {
        if (progressIndicator != null) {
            // log.debug("deactivateProgressIndicator");
            progressIndicator.setEnabled(false);
            progressIndicator.setVisible(false);
            if (getRows() > indecatorRow) {
                this.removeRow(indecatorRow);
            }
            // removeComponent(progressIndicator);
        }
    }

    private JobProgressDetailDialog jobProgressDialog;

    @Override
    public void buttonClick(ClickEvent event) {
        jobProgressDialog = new JobProgressDetailDialog(getTaskExecutionMetadata());
        this.addJobProgressListener(jobProgressDialog);
        jobProgressDialog.show(this);
        jobProgressDialog.populate();
        // jpd.start();
    }
}
