package za.co.enerweb.ebr.gui.vaadin.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import za.co.enerweb.toolbox.vaadin.messaging.Message;

/**
 * This gets fired if the e-breadboard is told to reload (normally reloads the
 * tasks etc.)
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class MEbrReload extends Message {
}
