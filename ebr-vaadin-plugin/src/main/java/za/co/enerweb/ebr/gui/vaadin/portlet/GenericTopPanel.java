
package za.co.enerweb.ebr.gui.vaadin.portlet;

import java.util.ArrayList;

import lombok.extern.slf4j.Slf4j;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeButton;

@Slf4j
public class GenericTopPanel extends HorizontalLayout {

    private boolean detach = false;
    private boolean maximized = false;
    final ArrayList<IPortalActionListener> portalListeners =
        new ArrayList<IPortalActionListener>();

    private static final String DETACHED = "Offline";
    private static final String ATTACHED = "Online";


    public GenericTopPanel() {
        addAttached();


    }

    private void addAttached() {
        final NativeButton detachButton = new NativeButton(ATTACHED);


        addComponent(detachButton);


        detachButton.addListener(
            new ClickListener() {

                @Override
                public void buttonClick(ClickEvent event) {
                    log.info("detach pressed");
                    if (!detach) {
                        detachButton.setCaption(DETACHED);
                        detach = !detach;

                    }
                    else {
                        detachButton.setCaption(ATTACHED);
                        detach = !detach;
                    }
                    for (IPortalActionListener listener : portalListeners) {
                        listener.detachMessageBus(detach);
                    }


                }
            });

    }




    public void addComponent(Component comp) {
        super.addComponent(comp);
        setExpandRatio(comp, 1.0f);

    }

    public void addPortalActionListener(IPortalActionListener listener) {
        portalListeners.add(listener);
    }

    public boolean isOnline() {
        return !detach;
    }

    public boolean isOffline() {
        return detach;
    }
}
