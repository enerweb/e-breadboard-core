package za.co.enerweb.ebr.command;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.toolbox.config.ResourceBundleUtils;

@Deprecated
@Slf4j
public class CommandRegistryService {
    private static final List<Command> commandList = new ArrayList<Command>();

    static {
        // register the shutdown command
        // maybe find another place to register this
        try {
            ResourceBundleUtils rbu = new ResourceBundleUtils("ebr");
            int shutdownPort = rbu.getIntWithDefault("shutdown_port", 0);
            String shutdownCmd = rbu.getStringWithDefault("shutdown_command",
                "");
            if (shutdownPort > 0) {
                Command cmd = new Command("Shutdown",
                    za.co.enerweb.ebr.command.ShutdownCommand.class);
                CommandRegistryService.registerCommand(cmd);
                cmd.setParmeter("port", shutdownPort);
                cmd.setParmeter("cmd", shutdownCmd);
            }
        } catch (Exception e) {
            log.debug("Could not add shutdown command.", e);
        }
    }

    public static void registerCommand(final Command cmd) {
            commandList.add(cmd);
    }

    public static List<Command> GetCommandList() {
        return commandList;
    }

}
