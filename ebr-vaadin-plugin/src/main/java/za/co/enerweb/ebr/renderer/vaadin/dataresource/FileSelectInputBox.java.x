package za.co.enerweb.ebr.renderer.vaadin.dataresource;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.VaadinPlugin;
import za.co.enerweb.ebr.annotations.EnvVarRenderer;
import za.co.enerweb.ebr.datatype.dataresource.FileDataResource;
import za.co.enerweb.ebr.server.HomeFile;
import za.co.enerweb.ebr.renderer.RendererMode;
import za.co.enerweb.ebr.renderer.vaadin.AbstractVaadinRenderer;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ProgressIndicator;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@EnvVarRenderer(id = FileSelectInputBox.ID,
    technology = VaadinPlugin.TECHNOLOGY_NAME,
    supportedTypes = {
                //XXX: "java.io.File",
        "za.co.enerweb.ebr.datatype.dataresource.FileDataResource"
        },
    rankPerType = {//50,
        50},
    supports = RendererMode.EDIT)
@Slf4j
public class FileSelectInputBox extends AbstractVaadinRenderer {
    private static final long serialVersionUID = 6588820669021176504L;

    public static final String ID = "file-input";
    private ProgressIndicator pi = new ProgressIndicator();
    private UploadReceiver receiver = new UploadReceiver();

    private Upload upload = new Upload(null, receiver);

    @Override
    public Component getComponent() {
        final HorizontalLayout layout = new HorizontalLayout();

        // create a modal popup window
        final Window winProgress = new Window("Upload progress");
        winProgress.setWidth("300px");
        winProgress.setWidth("50%");
        winProgress.setModal(true);

        VerticalLayout progressLayout =
            (VerticalLayout) winProgress.getContent();
        progressLayout.addComponent(pi);

        upload.setImmediate(true);
        upload.setButtonCaption("Upload New File");
        // The description is shown when you mouse-over the parameter name
        // upload.setCaption(getVariableMetaData().getDescription());
        Serializable value = getValue();
        if (value instanceof FileDataResource){
            FileDataResource fdr = (FileDataResource) value;
            try {
                upload.setCaption("Current file: "
                    + FilenameUtils.getName(fdr.getAbsolutePath()));
            } catch (Exception e1) {
                log.warn("Could not obtain the current file.", e1);
            }
        }
        layout.addComponent(upload);

        final Button cancelProcessing = new Button("Cancel");
        cancelProcessing.addListener(new Button.ClickListener() {
            private static final long serialVersionUID = 7418983909624218306L;

            public void buttonClick(final ClickEvent event) {
                upload.interruptUpload();
            }
        });
        cancelProcessing.setStyleName("small");

        progressLayout.addComponent(cancelProcessing);

        upload.addListener(new Upload.StartedListener() {
            private static final long serialVersionUID = 7418983909624218306L;

            public void uploadStarted(final StartedEvent event) {
                // This method gets called immediately after upload is started
                upload.setEnabled(false);

                Window win = getApplication().getMainWindow();
                win.addWindow(winProgress);

                pi.setValue(0f);
                pi.setPollingInterval(500);
            }
        });

        upload.addListener(new Upload.ProgressListener() {
            private static final long serialVersionUID = 7418983909624218306L;

            public void updateProgress(final long readBytes,
                final long contentLength) {
                pi.setValue(new Float(readBytes / (float) contentLength));
            }
        });

        upload.addListener(new Upload.SucceededListener() {
            private static final long serialVersionUID = 7418983909624218306L;

            public void uploadSucceeded(final SucceededEvent e) {
                upload.setEnabled(true);
                upload.setCaption("You have successfully uploaded file "
                    + e.getFilename());

                HomeFile tempFile = receiver.getTempFile();
                try {
                    // move it into the environment dir
                    File environmentDir = EbrFactory
                        .getEnvironmentManager()
                        .getEnvironmentDir(getEnvId());
                    File envFile = new File(environmentDir, tempFile.getName());
                    FileUtils.moveFile(tempFile.getFile(), envFile);
                    FileDataResource file = new FileDataResource(getEnvId(),
                        envFile.getAbsolutePath());
                    file.addToEnvironmentIfNeeded(getEnvId());
                    setValue(file);
                } catch (IOException e1) {
                    log.error("Could not load file into then environment "
                        + "direrctory", e1);
                    notifyUploadFailed(e.getFilename());
                }
            }
        });

        upload.addListener(new Upload.FailedListener() {
            private static final long serialVersionUID = 7418983909624218306L;

            public void uploadFailed(final FailedEvent e) {
                notifyUploadFailed(e.getFilename());
            }

        });

        upload.addListener(new Upload.FinishedListener() {
            private static final long serialVersionUID = 7418983909624218306L;

            public void uploadFinished(final FinishedEvent e) {
                // close popup window
                ((Window) winProgress.getParent()).removeWindow(winProgress);
            }
        });

        return layout;
    }

    private void notifyUploadFailed(String filename) {
        upload.setEnabled(true);
        upload.setCaption("Uploading of file "
            + filename + " was NOT SUCCESSFUL.");
        setValue(null);
    }
}
