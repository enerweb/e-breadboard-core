package za.co.enerweb.ebr.renderer.vaadin.jquery;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import za.co.enerweb.ebr.VaadinUrlService;
import za.co.enerweb.vaadin.jquery.JQueryComponent;
import za.co.enerweb.vaadin.jquery.JQueryListener;

public class InteractiveMap extends JQueryComponent implements JQueryListener {
    private static final long serialVersionUID = 1L;

    public static final String EVENT_INIT = "initialized";
    public static final String PARM_OBJ_KEY = "key";
    public static final String PARM_OBJ_CAPTION = "caption";
    public static final String PARM_OBJ_X = "x";
    public static final String PARM_OBJ_Y = "y";
    public static final String PARM_OBJ_IMAGE = "image";
    public static final String PARM_OBJ_TYPE = "type";
    public static final String PARM_OBJ_COORDS = "coords";
    @Getter
    @Setter
    private List<MapItem> items = new ArrayList<MapItem>();
    @Getter
    @Setter
    private String customMapInitCode;

    public enum MapItemType {
        REGION, ARTIFACT;

        public String toString() {
            return name().toLowerCase();
        }
    }

    public static MapItem createMapItem(Map<String, Object> map) {
        String key = (String) map.get(PARM_OBJ_KEY);
        String caption = (String) map.get(PARM_OBJ_CAPTION);
        String typeName = (String) map.get(PARM_OBJ_TYPE);
        MapItemType type = MapItemType.valueOf(typeName.toUpperCase());
        if (type == MapItemType.REGION) {
            String coords = (String) map.get(PARM_OBJ_COORDS);
            return new Region(key, caption, coords);
        } else if (type == MapItemType.ARTIFACT) {
            String image = (String) map.get(PARM_OBJ_IMAGE);
            int x = ((Double) map.get(PARM_OBJ_X)).intValue();
            int y = ((Double) map.get(PARM_OBJ_Y)).intValue();
            return new Artifact(key, caption, x, y, image);
        } else {
            return null;
        }
    }

    @NoArgsConstructor
    @AllArgsConstructor
    static abstract class MapItem implements Serializable {
        private static final long serialVersionUID = 1L;
        @Getter
        @Setter
        private String key;
        @Getter
        @Setter
        private String caption;
        @Getter
        @Setter
        private MapItemType type;
    }

    @NoArgsConstructor
    static class Region extends MapItem {
        private static final long serialVersionUID = 1L;
        @Getter
        @Setter
        private String coords;

        public Region(String key, String caption, String coords) {
            super(key, caption, MapItemType.REGION);
            this.coords = coords;
        }
    }

    @NoArgsConstructor
    static class Artifact extends MapItem {
        private static final long serialVersionUID = 1L;
        @Getter
        @Setter
        private int x, y;
        @Getter
        @Setter
        private String image;

        public Artifact(String key, String caption, int x, int y, String image) {
            super(key, caption, MapItemType.ARTIFACT);
            this.x = x;
            this.y = y;
            this.image = image;
        }
    }

    public InteractiveMap() {
        this(null, "");
    }

    public InteractiveMap(String id, String html) {
        super(id, html);
        addJQueryListener(this);
    }

    @Override
    public void init() {
        for (MapItem mi : items) {
            if (mi.getType() == MapItemType.ARTIFACT) {
                Artifact a = (Artifact) mi;
                String path = makeWebContentPath(a.getImage());
                execThis("create" + a.getType() + "('" + mi.key + "', '"
                        + mi.caption + "', '" + path + "', " + a.getX() + ", "
                        + a.getY() + ");");
            }
        }
        // execute custom client side code to customize the map
        if (getCustomMapInitCode() != null) {
            exec(getCustomMapInitCode());
        }
        requestRepaint();
    }

    private String makeWebContentPath(String path) {
        if (!path.startsWith("/")) {
            path = VaadinUrlService.getInstance(getApplication())
                .getWebContentRelUrlPath(path);
        }
        return path;
    }

    public void add(MapItem mi) {
        items.add(mi);
    }

    @Override
    public void onEventOccurred(String event, Object value) {
        if (event.equals(EVENT_INIT)) {
            init();
        }
    }

}
