package za.co.enerweb.ebr.gui.vaadin.messaging;

import lombok.NoArgsConstructor;

import com.vaadin.ui.Component;

/**
 * You should extend this class for any nontrivial messages
 * @deprecated use za.co.enerweb.toolbox.vaadin.messaging.Message
 */
@NoArgsConstructor
public class Message extends za.co.enerweb.toolbox.vaadin.messaging.Message {

    /**
    *
    * @deprecated rather set the source when firing
    */
   @Deprecated
   public Message(Component source) {
       setSource(source);
   }

    public Message(Component source, String messageId) {
        super(source, messageId);
    }

}
