package za.co.enerweb.ebr.renderer.vaadin.dataresource;

import za.co.enerweb.ebr.server.HomeFile;

import com.vaadin.server.StreamResource;
import com.vaadin.ui.Window;
import com.vaadin.util.FileTypeResolver;

/**
 * Make it easy to download home files.
 * see downloadInNewWindow()
 */
public class HomeFileStreamResource extends StreamResource {

    public HomeFileStreamResource(HomeFile homeFile, String filename) {
        super(new HomeFileStreamSource(homeFile), filename,
            application);

        setCacheTime(0);
        setFilename(filename);
        setMIMEType(FileTypeResolver.getMIMEType(
            filename));
        // streamResource.getStream().setParameter(
        // "Content-Disposition",
        // "attachment; filename="
        // + URLEncoder.encode(filename,"utf-8"));
        getStream().setParameter(
            "Content-Disposition", "attachment; filename=\""
                + filename + "\"");
    }

    public static void downloadInNewWindow(HomeFile homeFile, String filename) {
        Window win = new Window(filename);
        application.getMainWindow().addWindow(win);
        win.open(new HomeFileStreamResource(homeFile, filename,
            application)
            );
    }
}
