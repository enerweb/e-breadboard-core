package za.co.enerweb.ebr.gui.vaadin.menu;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.toolbox.vaadin.NotificationUtil;

/**
 * A very simple menu item that can run any runnable and
 * show a messeage weather it was successful or not
 */
@SuppressWarnings("serial")
@Slf4j
@Data
public class RunnableMenuItem extends AbstractMenuItem {

    private final Runnable runnable;

    public RunnableMenuItem(final int rank, final String caption,
        Runnable runnable) {
        this(caption, rank, caption, runnable);
    }

    public RunnableMenuItem(String id, final int rank, final String caption,
        Runnable runnable) {
        super(id, rank, caption);
        this.runnable = runnable;
    }

    public void menuSelected() {
        NotificationUtil notificationUtil =
            new NotificationUtil();
        try {
            runnable.run();
            notificationUtil.note("Successuflly executed: " + getCaption());
        } catch (Exception e) {
            notificationUtil.error("Could not execute: " + getCaption(), e);
        }
    }

}
