package za.co.enerweb.ebr.gui.vaadin.menu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

import com.vaadin.ui.Notification;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true, of = { })
@Slf4j
public class MenuItemContainer extends AbstractMenuItem {

    private List<AbstractMenuItem> menuItems =
        new ArrayList<AbstractMenuItem>();

    public MenuItemContainer(final int rank, final String caption) {
        // the caption should be unique enough for our purposes..
        super(caption, rank, caption);
    }

    public MenuItemContainer(String id, final int rank, final String caption) {
        super(id, rank, caption);
    }

    public void addMenuItem(final AbstractMenuItem menuItem) {
        List<String> context = menuItem.getContext();
        context.addAll(getContext());
        context.add(getCaption());
        menuItems.add(menuItem);
    }

    public void sort() {
        Collections.sort(menuItems);
    }

    @Override
    public void menuSelected() {
        Notification.show("Not implemented");
    }

    /**
     * Recursively select all menu items with the specified id.
     * @param id
     */
    public void menuSelected(String id) {
        if (!_menuSelected(id)) {
            log.warn("Could not find menu item with id: " + id);
        }
    }

    public boolean _menuSelected(String id) {
        if (id != null) {
            for (AbstractMenuItem menuItem : menuItems) {
                if (menuItem instanceof MenuItemContainer) {
                    MenuItemContainer subContainer =
                        (MenuItemContainer) menuItem;
                    // subContainer.setApplication(getApplication());
                    if (subContainer._menuSelected(id)) {
                        return true;
                    }
                } else if (menuItem.getId() != null
                    && menuItem.getId().startsWith(id)) {
                    // menuItem.setApplication(getApplication());
                    menuItem.menuSelected();
                    return true;
                }
            }
        }
        return false;
    }

    public MenuItemContainer findOrAddSubMenu(String caption) {
        return findOrAddSubMenu(-1, caption);
    }

    public MenuItemContainer findOrAddSubMenu(int rank, String caption) {
        AbstractMenuItem menuItem = findSubMenu(caption);
        MenuItemContainer ret;
        if (menuItem == null) {
            if (rank == -1) {
                rank = 500;
            }
            ret = new MenuItemContainer(rank, caption);
            addMenuItem(ret);
        } else if (menuItem instanceof MenuItemContainer) {
            ret = (MenuItemContainer) menuItem;
            if (rank != -1) {
                ret.setRank(rank); // can set the rank out of sequence
            }
        } else {
            // menu with that caption is not an Item Container
            // create another one with a different id
            if (rank == -1) {
                rank = 500;
            }
            int i = 0;
            while (null != findSubMenuById(caption + ++i)) {
                log.debug("found menu with id: " + caption + i);
            }
            ret = new MenuItemContainer(caption + i, rank, caption);
            addMenuItem(ret);
        }
        return ret;
    }

    private AbstractMenuItem findSubMenu(String caption) {
        for (AbstractMenuItem menuItem : menuItems) {
            if (menuItem.getCaption().equals(caption)) {
                return menuItem;
            }
        }
        return null;
    }

    private AbstractMenuItem findSubMenuById(String id) {
        for (AbstractMenuItem menuItem : menuItems) {
            if (menuItem.getId().equals(id)) {
                return menuItem;
            }
        }
        return null;
    }
}
