package za.co.enerweb.ebr.gui.vaadin.menu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.toolbox.vaadin.messaging.MessageBusFactory;

import com.vaadin.ui.Component;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@Slf4j
@Data
public abstract class AbstractMenuItem implements Comparable<AbstractMenuItem>,
Serializable {

    // Used for looking up this item by others that want to navigate to
    // trigger the selection of this menu item.
    private String id;
    private String caption;
    private List<String> context = new ArrayList<String>();

    public AbstractMenuItem(String id, final int rank, final String caption) {
        log.info("Adding menu item: id={} caption={}",
            id, caption);
        this.id = id;
        this.rank = rank;
        this.caption = caption;
    }

    /**
     * how this item ranks compared to its siblings
     * (the order in which it must be displayed, from low to high)
     * Assign numbers between 100 and 1000 leaving at least a gap of 10
     * between items, so that others may have space to add new items
     * without changing your code.
     */
    private int rank;

    public abstract void menuSelected();

    public int compareTo(final AbstractMenuItem o) {
        int ret = getRank() - o.getRank();
        if (ret == 0) {
            // we need a consistent tie breaker if for some reason
            // the ranks are equal
            ret = getCaption().compareToIgnoreCase(o.getCaption());
        }
        return ret;
    }

    protected void addToCurrentMessageBus(Component component) {
        MessageBusFactory.registerComponent(UI.getCurrent(), component);
    }
}
