package za.co.enerweb.ebr.renderer.vaadin;

import java.io.Serializable;

import za.co.enerweb.ebr.env.EnvVarMetadata;

import com.vaadin.ui.Component;

public interface VaadinRenderer extends Serializable {
    public Component getComponent(String envId, EnvVarMetadata envVarMetadata);
}
