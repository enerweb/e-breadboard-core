package za.co.enerweb.ebr.gui.vaadin.components;

import java.io.Serializable;

import za.co.enerweb.ebr.task.TaskMetadata;

public interface IInputParameterProvider {

    /**
     * @param name
     * @param taskMetadata
     * @param taskComponent
     * @return
     */
    Serializable getParameterValue(String parameterName,
        TaskMetadata taskMetadata,
        TaskComponent taskComponent);

}
