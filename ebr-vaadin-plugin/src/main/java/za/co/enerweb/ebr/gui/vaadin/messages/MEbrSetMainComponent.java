package za.co.enerweb.ebr.gui.vaadin.messages;

import java.util.Arrays;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import za.co.enerweb.toolbox.vaadin.messaging.Message;

import com.vaadin.ui.Component;

/**
 * This gets fired if the e-breadboard is told to reload (normally reloads the
 * tasks etc.)
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class MEbrSetMainComponent extends Message {

    private Component component;
    private List<String> breadCrumbStrings;

    public MEbrSetMainComponent(
        Component component, List<String> breadCrumbStrings) {
        super();
        super.setSynchronous(true);
        this.component = component;
        this.breadCrumbStrings = breadCrumbStrings;
    }

    public MEbrSetMainComponent(final Component component,
        final String... breadCrumbStrings) {
        this(component, Arrays.asList(breadCrumbStrings));
    }
}
