package za.co.enerweb.ebr.gui.vaadin;

import za.co.enerweb.ebr.task.TaskExecutionMetadata;

public interface JobProgressListener {

    void contentUpdated();

    void finalizingAterTaskDone(TaskExecutionMetadata taskExecutionMetadata);
}
