package za.co.enerweb.ebr;

import com.google.common.base.Joiner;
import com.vaadin.server.VaadinServlet;

public class VaadinUrlService {
    public static final String EBR = "home";
    public static final String WEB_CONTENT = "web-content";
    public static final String JAVASCRIPT = "javascript";
    public static final String STYLESHEETS = "stylesheets";
    public static final String VAADIN = VaadinPlugin.TECHNOLOGY_NAME;

    private static final Joiner PATH_JOINER = Joiner.on("/");

    // private static Map<Application, VaadinUrlService> instances
    // = new HashMap<Application, VaadinUrlService>();
    //
    // private String contextPath;
    //
    // private VaadinUrlService(Application application) {
    // this.application = application;
    // WebApplicationContext context = (WebApplicationContext)
    // application.getContext();
    // contextPath = context.getHttpSession().getServletContext()
    // .getContextPath();
    // // log.debug("*** contextPath = " + contextPath);
    // }

    public static String getContextPath() {
        return VaadinServlet.getCurrent().getServletContext().getContextPath();
        // return contextPath;
    }

    /**
     * @param relPath list of subdirs and file name
     *        (if you join them yourself use / or joinPaths())
     */
    public static String getWebContentRelUrlPath(String... relPath) {
        return joinPaths(getRelUrlPath(WEB_CONTENT),
            joinPaths(relPath));
    }

    private static String getRelUrlPath(String homeRelPath) {
        return joinPaths(getContextPath(), EBR, homeRelPath);
    }

    // public static synchronized VaadinUrlService getInstance() {
    // VaadinUrlService instance = instances.get(application);
    // if (instance == null) {
    // instance = new VaadinUrlService(application);
    // instances.put(application, instance);
    // }
    // return instance;
    // }

    public static String joinPaths(final String... paths) {
        return PATH_JOINER.join(paths);
    }
}
