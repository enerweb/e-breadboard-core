package za.co.enerweb.ebr.renderer.vaadin.html;

import za.co.enerweb.ebr.VaadinPlugin;
import za.co.enerweb.ebr.annotations.EnvVarRenderer;
import za.co.enerweb.ebr.datatype.html.HtmlTar;
import za.co.enerweb.ebr.renderer.RendererMode;
import za.co.enerweb.ebr.renderer.vaadin.AbstractVaadinRenderer;

import com.vaadin.server.ExternalResource;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Label;

/**
 * This only seems to work if the url used ends with a trailing slash eg.:
 * http://localhost:17080/ebr-vaadin-web/
 */
@EnvVarRenderer(id = HtmlTarRenderer.ID,
    technology = VaadinPlugin.TECHNOLOGY_NAME,
    supportedTypes = {"za.co.enerweb.ebr.datatype.html.HtmlTar"},
    rankPerType = {70},
    supports = RendererMode.VIEW)
public class HtmlTarRenderer extends AbstractVaadinRenderer {
    private static final long serialVersionUID = 1L;

    public static final String ID = "html";

    private String width = "100%";
    private String height = "500px";

    public HtmlTarRenderer() {

    }

    protected Label createHtmlLabel() {
        Label richText = new Label();
        richText.setContentMode(Label.CONTENT_XHTML);
        return richText;
    }

    @Override
    public Component getComponent() {
        HtmlTar tar = getValue();

        if (tar != null) {
            // get optional properties set for this renderer
            // and set the members appropriately
            installProperties();

            String mainFile = tar.getMainFile();
            String mainPath =
                "htmltar/" + getEnvId() + "/" + getVarName() + "/"
                   + tar.getResourceId() + "/" + mainFile;

            Embedded e = new Embedded("", new ExternalResource(mainPath));
            e.setWidth(getWidth());
            e.setHeight(getHeight());
            e.setType(Embedded.TYPE_BROWSER);

            return e;
        } else {

            Label lblError = createHtmlLabel();
            // lblError.setValue("<p style=\"color: #FF0000\">Null Value</p>");

            return lblError;
        }
    }

    public String getHeight() {
        return height;
    }

    public String getWidth() {
        return width;
    }

    protected void installProperties() {
        setWidth(getVariableProperty("width", getWidth()));
        setHeight(getVariableProperty("height", getHeight()));
    }

    public void setHeight(final String height) {
        this.height = height;
    }

    public void setWidth(final String width) {
        this.width = width;
    }
}
