package za.co.enerweb.ebr.gui.vaadin;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import lombok.AllArgsConstructor;
import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.VaadinPlugin;
import za.co.enerweb.ebr.datatype.IDataTypeRegistryService;
import za.co.enerweb.ebr.env.EnvVarMetadata;
import za.co.enerweb.ebr.env.IEnvironmentManager;
import za.co.enerweb.ebr.exceptions.InvalidMetadataException;
import za.co.enerweb.ebr.exceptions.ServiceException;
import za.co.enerweb.ebr.exceptions.UnsupportedEnvVarRendererException;
import za.co.enerweb.ebr.renderer.EnvVarRendererRegistryService;
import za.co.enerweb.ebr.renderer.vaadin.VaadinRenderer;
import za.co.enerweb.ebr.session.SessionMetadata;
import za.co.enerweb.ebr.task.InputMetadata;
import za.co.enerweb.ebr.task.OutputMetadata;
import za.co.enerweb.ebr.task.ParameterEnvironment;
import za.co.enerweb.ebr.task.ParameterMetadata;
import za.co.enerweb.ebr.task.TaskExecutionMetadata;
import za.co.enerweb.toolbox.vaadin.NotificationUtil;

import com.vaadin.ui.Component;
import com.vaadin.ui.Label;

@AllArgsConstructor
public class EnvVarUtils {
    protected static final List<String> supportedTechnologies =
        Collections.singletonList(VaadinPlugin.TECHNOLOGY_NAME);

    public Component getJobRendererComponent(
        final TaskExecutionMetadata taskExecutionMetadata,
        final EnvVarMetadata envVarMetadata,
        // XXX: rather take a defaultProvider or future or something
        // so that the default only needs to be calculated if needed
        final Serializable defaultValue,
        final boolean editable) {
        String envId;
        String variableName = envVarMetadata.getName();
        if (envVarMetadata instanceof ParameterMetadata
            && ((ParameterMetadata) envVarMetadata).getEnvironment().equals(
                ParameterEnvironment.JOB)) {
            if (envVarMetadata instanceof InputMetadata) {
                envId = taskExecutionMetadata.getInputIEnvironmentId();
            } else if (envVarMetadata instanceof OutputMetadata) {
                envId = taskExecutionMetadata.getOutputIEnvironmentId();
            } else {
                throw new InvalidMetadataException(
                    "Unsupported parameter type:"
                        + envVarMetadata.getClass().getName());
            }
        } else {
            // if it is not a job variable, assume that it is in the session
            envId = getSessionEnvironmentId(envVarMetadata);
        }
        IEnvironmentManager envManager =
            EbrFactory.getEnvironmentManager();
        // XXX: this should not be necessary, because the job should do this
        // on construction
        if (defaultValue != null &&
            !envManager.hasVariable(envId, variableName)) {
            // does not exist; set default
            envManager.setVariable(envId, envVarMetadata, defaultValue);
        }
        return getRendererComponent(envId, envVarMetadata, editable);
    }

    public Component getSessionRendererComponent(
        final EnvVarMetadata envVarMetadata, final boolean editable) {
        String envId = getSessionEnvironmentId(envVarMetadata);
        return getRendererComponent(envId, envVarMetadata, editable);
    }

    private String getSessionEnvironmentId(
        final EnvVarMetadata envVarMetadata) {
        SessionMetadata session = getSession();
        String envId = session.getEnvironmentId(envVarMetadata.getNamespace());
        if (envId == null) {
            // create env if it does not exist
            session = EbrFactory.getSessionManager().ensureNamespace(
                    session.getId(), envVarMetadata.getNamespace());
            envId = session.getEnvironmentId(envVarMetadata.getNamespace());
        }
        return envId;
    }

    public Component getRendererComponent(final String envId,
        final EnvVarMetadata envVarMetadata, final boolean editable) {
        try {
            VaadinRenderer renderer = lookupRenderer(envVarMetadata, editable);
            try {
                Component component =
                    renderer.getComponent(envId, envVarMetadata
                        );
                // component.addStyleName(AbstractTaskLayout.EBR_ENV_VAR_VALUE);
                return component;
            } catch (Exception e) {
                getNotificationUtil().warning(
                    "Can't use variable renderer for '"
                        + envVarMetadata.getCaption() + "'.", e);
            }
        } catch (Exception e) {
            getNotificationUtil().warning(
                "Can't lookup variable renderer for '"
                    + envVarMetadata.getCaption() + "'.", e);
        }
        Label label = new Label("(no renderer)");
        // label.addStyleName(AbstractTaskLayout.EBR_ENV_VAR_VALUE);
        return label;
    }

    public VaadinRenderer lookupRenderer(final EnvVarMetadata envVarMetadata,
        final boolean editable) throws UnsupportedEnvVarRendererException {
        Object renderer = EnvVarRendererRegistryService.getRenderer(
            supportedTechnologies, envVarMetadata, editable);
        if (renderer instanceof VaadinRenderer) {
            return (VaadinRenderer) renderer;
        } else {
            throw new ServiceException("Renderer of wrong type: "
                + (renderer != null ? renderer.getClass() : null));
        }
    }

    private NotificationUtil getNotificationUtil() {
        return new NotificationUtil();
    }

    private SessionMetadata getSession() {
        return EbrFactory.getSessionManager().getSession(
            new EbrVaadinUtils().getSessionId());
    }

    /*
     * this can actually move to a non-vaadin util..
     */
    public void setValue(String envId, EnvVarMetadata envVarMetadata,
        java.io.Serializable value) {
        if (value != null) {
            String varName = envVarMetadata.getName();
            // Convert stuff here to so that each renderer does not need
            // to implement it.
            IDataTypeRegistryService dataTypeRegistryService =
                EbrFactory.getDataTypeRegistryService();
            String valueType = dataTypeRegistryService.lookupType(null,
                envVarMetadata.getTypeName());
            // only do conversion if necessary
            if (!valueType.equals(value.getClass().getName())) {
                value = dataTypeRegistryService
                    .lookupConverter(null, envVarMetadata.getTypeName())
                    .fromSimpleStructure(envId, varName, value);
            }
            EbrFactory.getEnvironmentManager()
                .setVariableValue(envId, varName, value);
        }
    }
}
