package za.co.enerweb.ebr.gui.vaadin.portlet;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import za.co.enerweb.toolbox.vaadin.messaging.Message;

@Data
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
/**
 * Fire this message and get a MSelectedNodes back
 */
@NoArgsConstructor
public class MGenericGetSelectedNodes<Node> extends Message{

    private List<Node> responseCurrentlySelected;



}
