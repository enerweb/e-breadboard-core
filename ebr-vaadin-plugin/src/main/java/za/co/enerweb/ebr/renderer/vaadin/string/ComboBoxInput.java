package za.co.enerweb.ebr.renderer.vaadin.string;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import za.co.enerweb.ebr.VaadinPlugin;
import za.co.enerweb.ebr.annotations.EnvVarRenderer;
import za.co.enerweb.ebr.env.EnvVarMetadata;
import za.co.enerweb.ebr.renderer.RendererMode;
import za.co.enerweb.ebr.renderer.vaadin.AbstractVaadinRenderer;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.ListSelect;

/*
 * FIXME: maybe in stead of the renderer taking the options parameter we should
 * make a type that includes the serializable value and the query..
 * Then the user can specify the query in the default parameter.
 * Also non-task things can specify a complete object to render.
 */
@EnvVarRenderer(id = ComboBoxInput.ID,
    technology = VaadinPlugin.TECHNOLOGY_NAME,
    supportedTypes = {"java.lang.Integer", "java.lang.String"},
    rankPerType = {0, 0},
    supports = RendererMode.EDIT)
public class ComboBoxInput extends AbstractVaadinRenderer implements
    ValueChangeListener {

    private static final long serialVersionUID = -900487327777309995L;

    public static String AUX_OPTIONS_PROP = "options";

    public static final String ID = "combobox";

    private ListSelect cmbOptions;

    @Override
    @SuppressWarnings("rawtypes")
    public Component getComponent() {
        EnvVarMetadata metaData = getVariableMetaData();

        cmbOptions = new ListSelect(metaData.getDescription());

        Serializable data =
            getVariableProperty(AUX_OPTIONS_PROP, new ArrayList<String>());

        // populate the combo box
        if (data instanceof List) {

            // support lists where the value = display
            for (Object o : (List) data) {
                cmbOptions.addItem(o);
            }

        } else if (data instanceof Map) {
            // support for maps where the real value is hidden
            Map map = (Map) data;

            for (Object value : map.keySet()) {
                Object caption = map.get(value);

                cmbOptions.addItem(value);
                cmbOptions.setItemCaption(value, caption.toString());
            }
        }

        cmbOptions.setValue(getValue());

        cmbOptions.setRows(1);
        cmbOptions.setNullSelectionAllowed(false);
        cmbOptions.setImmediate(true);
        cmbOptions.addListener(this);

        return cmbOptions;
    }

    @Override
    public void valueChange(final ValueChangeEvent event) {
        setValue((Serializable) cmbOptions.getValue());
    }

}
