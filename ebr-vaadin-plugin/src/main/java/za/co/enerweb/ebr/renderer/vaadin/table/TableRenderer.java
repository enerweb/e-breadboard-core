package za.co.enerweb.ebr.renderer.vaadin.table;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.VaadinPlugin;
import za.co.enerweb.ebr.annotations.EnvVarRenderer;
import za.co.enerweb.ebr.env.EnvVarMetadata;
import za.co.enerweb.ebr.renderer.RendererMode;
import za.co.enerweb.ebr.renderer.vaadin.AbstractVaadinRenderer;

import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;

//XXX: don't support object, rather define a custom type
@EnvVarRenderer(id = TableRenderer.ID,
        technology = VaadinPlugin.TECHNOLOGY_NAME,
        supportedTypes = { "java.lang.String", "java.lang.Object" },
        rankPerType = { 0, 0 },
        supports = RendererMode.EDIT_AND_VIEW)
@Slf4j
public class TableRenderer extends AbstractVaadinRenderer
        implements Table.ValueChangeListener {
    private static final long serialVersionUID = 1L;
    public static final String ID = "table";
    private static String AUX_PROP_DATAFRAME = "dataframe";
    private static String AUX_PROP_LISTDATA = "list";
    private static String AUX_PROP_HEADERS = "headers";
    private static String AUX_PROP_MULTI = "multi";
    private static String AUX_PROP_HEIGHT = "height";
    private Table table;
    private IndexedContainer container;

    @SuppressWarnings("unchecked")
    @Override
    public Component getComponent() {
        EnvVarMetadata metaData = getVariableMetaData();
        // create the table model
        container = new IndexedContainer();

        boolean rendererOnly = false;
        Serializable tmp = null;
        if ((tmp = getVariableProperty(AUX_PROP_DATAFRAME, null)) != null) {
            // get the data from from the AUX_PROP_DATAFRAME property
            populateTableDataFrame((Map<String, Object>) tmp);
        } else if ((tmp = getVariableProperty(AUX_PROP_LISTDATA, null))
                != null) {
            // get the data map from the AUX_PROP_DATAMAP property
            populateTableLists((Map<String, Object>) tmp);
        } else {
            // this is just an output renderer
            rendererOnly = true;
            tmp = getValue();
            if (tmp == null) {
                log.debug("No data to show in the table.");
                return new Label();
            }
            populateTableDataFrame((Map<String, Object>) tmp);
        }

        // create the table
        table = new Table(metaData.getDescription(), container);
        table.setImmediate(true);
        if (!rendererOnly) {
            table.setSelectable(true);
            table.setMultiSelect(isMultiSelect());
        }
        table.setHeight(getVariableProperty(AUX_PROP_HEIGHT, "200px"));
        table.addListener(this);
        // select the current value(s) in the table
        Serializable value = getValue();
        if (value != null) {
            if (value.getClass().isArray()) {
                int length = Array.getLength(value);
                for (int i = 0; i < length; i++) {
                    table.select(Array.get(value, i));
                }
            } else if (value instanceof List) {
                List<Object> list = (List<Object>) value;
                for (Object v : list) {
                    table.select(v);
                }
            } else {
                table.select(value);
            }
        }

        return table;
    }

    @SuppressWarnings("unchecked")
    protected String[] getHeaders() {
        Serializable tmp = getVariableProperty(AUX_PROP_HEADERS,
                new ArrayList<String>());
        List<String> headers = (List<String>) tmp;
        return headers.toArray(new String[0]);
    }

    protected boolean isMultiSelect() {
        Serializable tmp = getVariableProperty(AUX_PROP_MULTI, Boolean.FALSE);
        if (tmp instanceof Double) {
            Double v = (Double) tmp;
            return v != null && v.intValue() != 0;
        } else if (tmp instanceof Boolean) {
            Boolean v = (Boolean) tmp;
            return v != null && v.booleanValue();
        } else {
            return false;
        }
    }

    protected void populateTableDataFrame(Map<String, Object> dframe) {
        // get get the headers
        String[] keys = dframe.keySet().toArray(new String[0]);
        String[] headers = getHeaders();
        if (headers == null || headers.length == 0) {
            headers = keys;
        }
        // warn about incorrect number of headers given
        if (headers.length != keys.length) {
            log.error("Incorrect number of headers given! Expected "
                    + keys.length + " but got " + headers.length);
        }
        // create the container columns
        for (String header : headers) {
            container.addContainerProperty(header, String.class, null);
        }
        // cache the arrays for performance
        Map<String, Object[]> arrayCache = new HashMap<String, Object[]>();
        // get the number of rows in the dataframe
        int size = toArray(dframe.values().iterator().next()).length;
        // populate the container row by row
        for (int row = 0; row < size; row++) {
            Item item = container.addItem(row);
            for (int col = 0; col < headers.length; col++) {
                try {
                    // cache the array values
                    String key = keys[col];
                    Object[] values = null;
                    if ((values = arrayCache.get(key)) == null) {
                        values = toArray(dframe.get(key));
                        arrayCache.put(key, values);
                    }
                    // get the row value and set it for the column
                    item.getItemProperty(headers[col]).setValue(values[row]);
                } catch (IndexOutOfBoundsException e) {
                    // columns and given headers are not the same
                    // we are warning user near the top of the func
                }
            }
        }
    }

    protected void populateTableLists(Map<String, Object> data) {
        // get get the headers
        String[] headers = getHeaders();
        // create the container columns
        for (String header : headers) {
            container.addContainerProperty(header, String.class, null);
        }
        // populate the container
        Iterator<Entry<String, Object>> iterator = data.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<String, Object> entry = iterator.next();
            String id = entry.getKey();
            Object[] values = toArray(entry.getValue());
            Item item = container.addItem(id);
            for (int i = 0; i < headers.length; i++) {
                try {
                    item.getItemProperty(headers[i]).setValue(values[i]);
                } catch (IndexOutOfBoundsException e) {
                    // ignore for now
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    protected Object[] toArray(Object rawList) {
        if (rawList.getClass().isArray()) {
            // convert Object to Object[]
            Object[] array = new Object[Array.getLength(rawList)];
            for (int i = 0; i < array.length; i++) {
                array[i] = Array.get(rawList, i);
            }
            return array;
        } else if (rawList instanceof List) {
            List<Object> list = (List<Object>) rawList;
            Object[] result = new Object[list.size()];
            int i = 0;
            for (Object o : list) {
                result[i++] = o;
            }
            return result;
        } else {
            return new String[0];
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void valueChange(ValueChangeEvent e) {
        Object tmp = e.getProperty().getValue();
        if (tmp instanceof Set) {
            // multi select
            Set<Object> set = (Set<Object>) tmp;
            if (null == set || set.size() == 0) {
                setValue(new Object[0]);
            } else {
                setValue(new ArrayList<Object>(set));
            }
        } else if (tmp instanceof Object) {
            // single select
            Serializable value = (Serializable) tmp;
            setValue(value);
        }
    }


}
