package za.co.enerweb.ebr.gui.vaadin.components;

import java.io.Serializable;
import java.util.Collections;

import lombok.NoArgsConstructor;
import lombok.Setter;
import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.VaadinPlugin;
import za.co.enerweb.ebr.env.EnvVarMetadata;
import za.co.enerweb.ebr.exceptions.TaskExecutionException;
import za.co.enerweb.ebr.exceptions.TaskInstantiationException;
import za.co.enerweb.ebr.exceptions.UnknownTaskExecutionException;
import za.co.enerweb.ebr.gui.vaadin.IJobController;
import za.co.enerweb.ebr.gui.vaadin.IMainApplication;
import za.co.enerweb.ebr.gui.vaadin.ITaskView;
import za.co.enerweb.ebr.gui.vaadin.messages.MEbrReload;
import za.co.enerweb.ebr.task.TaskExecutionMetadata;
import za.co.enerweb.ebr.layout.vaadin.IVaadinTaskLayout;
import za.co.enerweb.ebr.session.SessionManagerFactory;
import za.co.enerweb.ebr.session.SessionMetadata;
import za.co.enerweb.ebr.task.InputMetadata;
import za.co.enerweb.ebr.task.TaskMetadata;
import za.co.enerweb.toolbox.vaadin.messaging.Message;
import za.co.enerweb.toolbox.vaadin.messaging.MessageListener;

import com.vaadin.ui.Component;

/**
 * Uses the task layout to display the task.
 */
@NoArgsConstructor
public class TaskLayoutComponent extends TaskComponent
    implements IJobController, MessageListener {
    private static final long serialVersionUID = 1L;
    private boolean populated = false;
    private IVaadinTaskLayout taskLayout;
    private ITaskView view;
    @Setter
    private boolean runOnInit = false;

    public TaskLayoutComponent(String taskId,
        IInputParameterProvider inputParameterProvider,
        String outputParameterName) {
        super(taskId, inputParameterProvider);
    }

    public void attach() {
        super.attach();
        if (!populated) { // only populate once
            init();
            populated = true;
            populateTask();
        }
    }

    private void populateTask() {
        addTaskLayout();

        if (isRunOnInit()) {
            runTaskAndShowProgress();
        }
    }

    protected void addTaskLayout() {
        TaskMetadata taskMetadata = getTaskMetadata();
        TaskExecutionMetadata taskExecutionMetadata = getTaskExecutionMetadata();
        if (taskMetadata == null || taskExecutionMetadata == null) {
            return;
        }
        String layout = taskMetadata.getLayout();
        taskLayout =
            (IVaadinTaskLayout)
            EbrFactory.getTaskLayoutRegistry().getLayout(
            Collections.singletonList(VaadinPlugin.TECHNOLOGY_NAME.toString()),
            layout);
        taskLayout.init(this, taskExecutionMetadata);
        taskLayout.updateStatus();
        addComponent(taskLayout.getLayoutComponent());
    }

    private boolean isRunOnInit() {
        return runOnInit;
    }

    @Override
    public void finalizingAterTaskDone(TaskExecutionMetadata taskExecutionMetadata) {
        super.finalizingAterTaskDone(taskExecutionMetadata);
        synchronized(getApplication()) {
            removeAllComponents();
            addTaskLayout();
        }
    }

    @Override
    public void reset() {
        setTaskExecutionMetadata(null);
        init();
        addTaskLayout();
    }

    @Override
    public IVaadinTaskLayout getLayout() {
        return taskLayout;
    }

    @Override
    public SessionMetadata getSession() {
        return SessionManagerFactory.getInstance().getSession(
            ((IMainApplication) getApplication()).getSessionId());
    }

    @Override
    public Component getJobRendererComponent(EnvVarMetadata envVarMetadata,
        boolean editable) {
        Serializable defaultValue = null;
        if (envVarMetadata instanceof InputMetadata) {
            defaultValue = ((InputMetadata) envVarMetadata).getDefaultValue();
        }
        return getEnvVarUtils().getJobRendererComponent(
            getTaskExecutionMetadata(), envVarMetadata, defaultValue, editable);
    }

    @Override
    public void init(ITaskView view, String taskId)
        throws TaskInstantiationException {
    }

    @Override
    public void run() throws TaskExecutionException, UnknownJobException {
        runTask();
    }

    @Override
    public void processTaskExecutionMetadata(TaskExecutionMetadata taskExecutionMetadata) {
        finalizingAterTaskDone(taskExecutionMetadata);
    }

    @Override
    public void onMessage(Message message) {
        if (message instanceof MEbrReload) {
            reset();
        }
    }
}
