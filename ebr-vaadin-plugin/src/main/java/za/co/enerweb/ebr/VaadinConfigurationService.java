package za.co.enerweb.ebr;

import java.util.Collections;
import java.util.List;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.server.ConfSection;
import za.co.enerweb.ebr.server.SystemConfiguration;
import za.co.enerweb.toolbox.config.ResourceBundleUtils;

@Slf4j
public class VaadinConfigurationService {
    private static final String THEME = "ebr";

    private String theme = THEME;
    @Getter
    private String startMenuItemId = null;
    @Getter
    private List<String> hideMenuItemIds;

    private VaadinConfigurationService() {
        try {
            ResourceBundleUtils rbu =
                new ResourceBundleUtils(
                    SystemConfiguration.EBR_BRANDING_PROPERTIES);
            theme = rbu.getStringWithDefault("vaadin.theme", theme);
        } catch (Exception e) {
        }

        try {
            parseConfIni();
        } catch (Exception e) {
        }
        if (hideMenuItemIds == null) {
            hideMenuItemIds = Collections.emptyList();
        }
    }

    public void parseConfIni() {
        ConfSection vaadinSection =
            EbrFactory.getConfigurationService().getSection(
                "frontend/vaadin");
        if (vaadinSection == null) {
            return;
        }
        try {
            // Section vaadinSection = ini.get("frontend/vaadin");
            // if (vaadinSection != null) {
                startMenuItemId = vaadinSection.get(
                    "startAtMenuItemId");
                hideMenuItemIds = vaadinSection.getAll(
                    "hideMenuItemId");
            // }
        } catch (Exception e) {
            log.warn("Could not parse config file.", e);
        }
    }

    public String getTheme() {
        return theme;
    }

    public String getCustomCss() {
        return VaadinPlugin.getCustomCss();
    }

    // XXX: move this to the plugin factory
    public static synchronized VaadinConfigurationService getInstance() {
        return new VaadinConfigurationService();
    }

    public static void reload() {
        EbrFactory.getConfigurationService().reload();
    }
}
