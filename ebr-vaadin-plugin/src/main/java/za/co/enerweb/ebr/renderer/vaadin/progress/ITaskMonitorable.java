package za.co.enerweb.ebr.renderer.vaadin.progress;

import java.io.Serializable;

@Deprecated
public interface ITaskMonitorable extends Serializable {

    public void done();

    public void done(String msg);

    public void reset();

    public void start();

    public void update(double progress, String msg);

    public void update(int progress, String msg);

}
