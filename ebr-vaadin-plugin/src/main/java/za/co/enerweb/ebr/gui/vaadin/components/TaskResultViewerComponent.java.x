package za.co.enerweb.ebr.gui.vaadin.components;

import lombok.NoArgsConstructor;
import lombok.Setter;
import za.co.enerweb.ebr.task.TaskExecutionMetadata;

/**
 * Display one result of a task that already ran.
 */
@NoArgsConstructor
public class TaskResultViewerComponent extends TaskComponent {
    private static final long serialVersionUID = 1L;
    @Setter
    private String outputParameterName;
    private boolean populated = false;

    public TaskResultViewerComponent(TaskExecutionMetadata taskExecutionMetadata,
        String outputParameterName) {
        super(taskExecutionMetadata);
        this.outputParameterName = outputParameterName;
    }

    public void attach() {
        super.attach();
        if (!populated) { // by default only run once
            init();
            populated = true;
            showOutputParameter(outputParameterName);
        }
    }
}
