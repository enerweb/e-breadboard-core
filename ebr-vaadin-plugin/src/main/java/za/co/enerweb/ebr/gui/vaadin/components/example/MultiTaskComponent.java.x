package za.co.enerweb.ebr.gui.vaadin.components.example;

import za.co.enerweb.ebr.gui.vaadin.components.IInputParameterProvider;
import za.co.enerweb.ebr.gui.vaadin.components.TaskResultComponent;

import com.vaadin.ui.VerticalLayout;

public class MultiTaskComponent extends VerticalLayout {
    private static final long serialVersionUID = 1L;

    public MultiTaskComponent() {

    }

    public TaskResultComponent addTask(String taskId,
            IInputParameterProvider inputParameterProvider,
            String outputParameterName) {
        TaskResultComponent tc = new TaskResultComponent(taskId,
            inputParameterProvider, outputParameterName);
        addComponent(tc);
        return tc;
    }
}
