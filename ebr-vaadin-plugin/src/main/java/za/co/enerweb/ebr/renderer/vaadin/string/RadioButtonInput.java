package za.co.enerweb.ebr.renderer.vaadin.string;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import za.co.enerweb.ebr.VaadinPlugin;
import za.co.enerweb.ebr.annotations.EnvVarRenderer;
import za.co.enerweb.ebr.env.EnvVarMetadata;
import za.co.enerweb.ebr.renderer.RendererMode;
import za.co.enerweb.ebr.renderer.vaadin.AbstractVaadinRenderer;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.OptionGroup;

@EnvVarRenderer(id = RadioButtonInput.ID,
    technology = VaadinPlugin.TECHNOLOGY_NAME,
    supportedTypes = {"java.lang.Integer", "java.lang.String"},
    rankPerType = {0, 0},
    supports = RendererMode.EDIT)
public class RadioButtonInput extends AbstractVaadinRenderer implements
        ValueChangeListener {

    private static final long serialVersionUID = -900487327777309995L;

    public static final String AUX_OPTIONS_PROP = "options";

    public static final String AUX_ORIENTATION_PROP = "orientation";
    public static final String ORIENTATION_VERTICAL = "vertical";
    public static final String ORIENTATION_HORIZONTAL = "horizontal";

    public static final String ID = "radio-buttons";

    private OptionGroup radOptions;

    @Override
    @SuppressWarnings("rawtypes")
    public Component getComponent() {
        EnvVarMetadata metaData = getVariableMetaData();

        radOptions = new OptionGroup(metaData.getDescription());

        Serializable data =
            getVariableProperty(AUX_OPTIONS_PROP, new ArrayList<String>());

        String orientation =
            getVariableProperty(AUX_ORIENTATION_PROP, ORIENTATION_VERTICAL);
        if (ORIENTATION_HORIZONTAL.equals(orientation)) {
            radOptions.addStyleName("ebr-horizontal-optiongroup");
        }

        // populate the combo box
        if (data instanceof List) {

            // support lists where the value = display
            for (Object o : (List) data) {
                radOptions.addItem(o);
            }

        } else if (data instanceof Map) {
            // support for maps where the real value is hidden
            Map map = (Map) data;

            for (Object value : map.keySet()) {
                Object caption = map.get(value);

                radOptions.addItem(value);
                radOptions.setItemCaption(value, caption.toString());
            }
        }

        radOptions.select(getValue());

        radOptions.setNullSelectionAllowed(false);
        radOptions.setImmediate(true);
        radOptions.addListener(this);

        return radOptions;
    }

    @Override
    public void valueChange(final ValueChangeEvent event) {
        setValue((Serializable) radOptions.getValue());
    }

}
