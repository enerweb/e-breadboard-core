package za.co.enerweb.ebr.gui.vaadin;

import com.vaadin.Application;

public interface ITaskView {
    public void error(String msg, Exception e);

    public Application getApplication();

    /**
     * @param taskId
     */
    public void resetTask(String taskId);

    public void setTaskId(String taskId);

}
