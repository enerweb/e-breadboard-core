package za.co.enerweb.ebr.gui.vaadin.time_machine.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import za.co.enerweb.toolbox.vaadin.messaging.Message;

@Data
@AllArgsConstructor
public class MClearRenderCacheTimeRange extends Message{
    //private List<Date> dateToRender;
    //private MRenderTimeRange originalRenderRequest;
}
