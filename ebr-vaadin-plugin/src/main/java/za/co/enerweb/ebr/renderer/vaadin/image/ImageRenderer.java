package za.co.enerweb.ebr.renderer.vaadin.image;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.VaadinPlugin;
import za.co.enerweb.ebr.annotations.EnvVarRenderer;
import za.co.enerweb.ebr.datatype.image.Image;
import za.co.enerweb.ebr.renderer.RendererMode;
import za.co.enerweb.ebr.renderer.vaadin.AbstractVaadinRenderer;
import za.co.enerweb.toolbox.validation.ValidationException;

import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;

@EnvVarRenderer(id = ImageRenderer.ID,
    technology = VaadinPlugin.TECHNOLOGY_NAME,
    supportedTypes = {"za.co.enerweb.ebr.datatype.image.Image"},
    rankPerType = {90},
    supports = RendererMode.VIEW)
@Slf4j
public class ImageRenderer extends AbstractVaadinRenderer {
    private final static class ImageStreamSource implements StreamSource,
        Serializable {
        private static final long serialVersionUID = 1L;
        private final Image value;

        /**
         * @param value
         */
        private ImageStreamSource(final Image value) {
            this.value = value;
        }

        @Override
        public InputStream getStream() {
            try {
                return value.getInputStream();
            } catch (IOException e) {
                log.error("Could not render document.",
                    e);
                return null;
            } catch (ValidationException e) {
                log.debug("Invalid dataresource", e);
                return null;
            }
        }
    }

    private static final long serialVersionUID = 1L;
    public static final String ID = "image";

    protected Label createHtmlLabel() {
        Label richText = new Label();
        richText.setContentMode(Label.CONTENT_XHTML);
        richText.setWidth("100%");

        return richText;
    }

    @Override
    public Component getComponent() {
        final Image value = getValue();

        if (value != null) {

            try {
                HorizontalLayout layout = new HorizontalLayout();

                StreamResource streamResource = new StreamResource(
                    new ImageStreamSource(value), value.getResourceMetadata()
                        .getFileName());

                // Instruct browser not to cache the image.
                streamResource.setCacheTime(0);

                Embedded img = new Embedded("", streamResource);
                img.setType(Embedded.TYPE_IMAGE);
                layout.addComponent(img);

                // add a label that can be stretched,
                // thus preventing image from being stretched
                Label lbl = new Label("");
                lbl.setWidth("100%");
                layout.addComponent(lbl);

                return layout;
            } catch (ValidationException e) {
                log.debug("Invalid dataresource: "
                    + e.getLocalizedMessage());
                return new Label();
            }

        } else {

            // default display for null values. maybe it should be empty?
            Label lblEntry = createHtmlLabel();
            // lblEntry.setValue("<p style=\"color: #FF0000\">Null Value</p>");

            return lblEntry;
        }

    }

}
