package za.co.enerweb.ebr.gui.vaadin.windows;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.server.Registrar;

import com.vaadin.Application;
import com.vaadin.ui.Window;

@Singleton
@Slf4j
public class WindowService implements IWindowService {

    private Map<String, IWindowProvider> providers =
        new HashMap<String, IWindowProvider>();

    @PostConstruct
    private void setup() {
        Set<Class<? extends IWindowProvider>> types =
            Registrar.getReflections().getSubTypesOf(IWindowProvider.class);
        for (Class<? extends IWindowProvider> type : types) {
            try {
                IWindowProvider provider = (IWindowProvider) type.newInstance();
                providers.put(provider.getName(), provider);
            } catch (Exception e) {
                log.warn("Could not add IWindowProvider: " + type, e);
            }
        }
    }

    // public boolean hasWindow(String windowName) {
    // return providers.containsKey(windowName);
    // }

    @Override
    public Window getWindow(Application application, Window window,
        String windowName) {
        IWindowProvider provider = providers.get(windowName);
        if (provider == null) {
            // log.debug("Unknown window: " + windowName);
            return window;
        }
        return provider.getWindow(application, window, windowName);
    }

}
