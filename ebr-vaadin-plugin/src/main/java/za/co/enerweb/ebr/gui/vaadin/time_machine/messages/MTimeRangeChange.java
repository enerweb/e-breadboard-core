package za.co.enerweb.ebr.gui.vaadin.time_machine.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import org.joda.time.DateTime;

import za.co.enerweb.toolbox.vaadin.messaging.Message;

/**
 * This is fired when some component changes the broad timerange
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class MTimeRangeChange extends Message {
    private DateTime startDate;
    private DateTime endDate;
}
