package za.co.enerweb.ebr.gui.vaadin.messaging;

/**
 * @deprecated use za.co.enerweb.toolbox.vaadin.messaging.MessageListener
 */
@Deprecated
public interface MessageListener {

    void onMessage(Message message);
}
