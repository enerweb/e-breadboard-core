package za.co.enerweb.ebr.renderer.vaadin.table;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.renderer.vaadin.AbstractVaadinRenderer;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;

/*
 * FIXME: the dataframe parsing should happen in a converter in the R plugin?
 * or there should be a generic timeseries type and the R converter must
 * be able to convert it.
 */
@Slf4j
public abstract class BaseDataFrameRenderer extends AbstractVaadinRenderer {
    private static final long serialVersionUID = 1L;
    public static final String ID = "table";
    public static String AUX_PROP_DATAFRAME = "dataframe";
    public static String AUX_PROP_LISTDATA = "list";
    public static String AUX_PROP_HEADERS = "headers";
    public static String AUX_PROP_MULTI = "multi";
    public static String AUX_PROP_HEIGHT = "height";
    public static String AUX_PROP_WIDTH = "width";

    private SimpleDateFormat sdfyyyymmdd = new SimpleDateFormat("yyyy-MM-dd");
    private Calendar dateConverterCal = new GregorianCalendar();

    protected Map<String, Object> parameters;
    protected Map<String, Object> dataFrame;

    protected String[] getHeaders() {
        return getHeaders(null);
    }

    @SuppressWarnings("unchecked")
    protected boolean initializeDataFrame() {
        Serializable value = getValue();
        if (value != null) {
            // XXX: maybe rather throw exceptions with good messages so
            // the user can see what is wrong on the frontend

            // it is an output renderer
            Collection<Object> list = null;
            if (value instanceof Collection){
                list = (Collection<Object>) value;
            } else {
                log.error("Received unsupported type: "
                    + value.getClass());
                return false;
            }
            // XXX: Do we really have to require options?
            if (list.size() != 2) {
                log.error("Expected a list with 2 values: "
                        + "one for the parameters "
                        + "and one for the dataframe");
                return false;
            }
            // get the options
            Iterator<Object> iterator = list.iterator();
            try {
                parameters = (Map<String, Object>)iterator.next();
            } catch (ClassCastException e) {
                log.error("Couldn't cast the first value to a map "
                    + "containing the parameters!", e);
                return false;
            }
            // get the dataframe
            try {
                dataFrame = (Map<String, Object>)iterator.next();
            } catch (ClassCastException e) {
                log.error("Couldn't cast the second value to a dataframe!",
                    e);
                return false;
            }
        } else {
            // this is an input renderer/viewer
            // get the dataframe from the renderer properties
            dataFrame = (Map<String, Object>)getVariableProperty(
                AUX_PROP_DATAFRAME, null);
        }
        if (dataFrame == null) {
            log.error("dataframe wasn't initialized");
            return false;
        }
        return dataFrame != null;
    }

    @SuppressWarnings("unchecked")
    protected String[] getHeaders(String[] defaults) {
        Serializable tmp = getVariableProperty(AUX_PROP_HEADERS, null);
        if (tmp != null) {
            List<String> headers = (List<String>) tmp;
            return headers.toArray(new String[0]);
        } else {
            return defaults;
        }
    }

    protected boolean isMultiSelect() {
        Serializable tmp = getVariableProperty(AUX_PROP_MULTI, Boolean.FALSE);
        if (tmp instanceof Double) {
            Double v = (Double) tmp;
            return v != null && v.intValue() != 0;
        } else if (tmp instanceof Boolean) {
            Boolean v = (Boolean) tmp;
            return v != null && v.booleanValue();
        } else {
            return false;
        }
    }

    protected IndexedContainer createIndexedContainer() {
        return new IndexedContainer();
    }

    protected IndexedContainer fromTableDataFrame(Map<String, Object> dframe) {
        IndexedContainer container = createIndexedContainer();
        // get get the headers
        String[] dataframeKeys = dframe.keySet().toArray(new String[0]);
        String[] headers = getHeaders(dataframeKeys);
        // warn about incorrect number of headers given
        if (headers.length != dataframeKeys.length) {
            log.error("Incorrect number of headers given! Expected "
                    + dataframeKeys.length + " but got " + headers.length);
        }
        // cache the arrays for performance
        Map<String, Object[]> arrayCache = new HashMap<String, Object[]>();
        // get the number of rows in the dataframe
        int size = toArray(dframe.values().iterator().next()).length;
        // populate the container row by row
        for (int row = 0; row < size; row++) {
            Item item = null;;
            for (int col = 0; col < headers.length; col++) {
                try {
                    // cache the array values
                    String header = headers[col];
                    Object[] values = null;
                    if ((values = arrayCache.get(header)) == null) {
                        // cache the array
                        values = toArray(dframe.get(dataframeKeys[col]));
                        arrayCache.put(header, values);
                        // add the container index
                        Object first = convertObject(col, 0, values[0]);
                        addContainerProperty(container, col, header,
                            first.getClass());
                    }
                    // create new row
                    if (item == null) {
                        Object rowid = getRowIndexValue(row, values);
                        item = container.addItem(rowid);
                    }
                    // get the row value and set it for the column
                    try {
                        Object value = convertObject(col, row, values[row]);
                        item.getItemProperty(header).setValue(value);
                    } catch (NullPointerException e) {
                        log.error("Error adding row " + row);
                    }
                } catch (IndexOutOfBoundsException e) {
                    // columns and given headers are not the same
                    // we are warning user near the top of the func
                }
            }
        }
        return container;
    }

    // make this class extendible
    protected void addContainerProperty(IndexedContainer container, int col,
            String name, Class<? extends Object> clazz) {
        container.addContainerProperty(name, clazz, null);
    }

    // make this class extendible
    protected Object getRowIndexValue(int row, Object[] values) {
        return row;
    }

    // make this class extendible
    protected Object convertObject(int column, int row, Object value) {
        return value;
    }

    @SuppressWarnings("unchecked")
    protected Object[] toArray(Object rawList) {
        if (rawList.getClass().isArray()) {
            // convert Object to Object[]
            Object[] array = new Object[Array.getLength(rawList)];
            for (int i = 0; i < array.length; i++) {
                array[i] = Array.get(rawList, i);
            }
            return array;
        } else if (rawList instanceof List) {
            List<Object> list = (List<Object>) rawList;
            Object[] result = new Object[list.size()];
            int i = 0;
            for (Object o : list) {
                result[i++] = o;
            }
            return result;
        } else {
            return new String[0];
        }
    }

    protected Object getParameter(String name) {
        if (parameters.containsKey(name)) {
            return parameters.get(name);
        }
        return null;
    }

    protected Date toDate(Object value) {
        if (value instanceof Double) {
            return convertDate((Double) value);
        } else if (value instanceof Integer) {
            return convertDate((Integer) value);
        } else if (value instanceof String) {
            return convertDate((String) value);
        } else {
            log.error("Couldn't convert " + value + " to date");
            return null;
        }
    }

    private void resetCalendar() {
        try {
            dateConverterCal.setTimeInMillis(sdfyyyymmdd.parse("1970-01-01")
                .getTime());
        } catch (ParseException e) {
            log.error("Date conversion from epoch error", e);
        }
    }

    protected Date convertDate(double d) {
        return convertDate((int)d);
    }

    protected Date convertDate(int d) {
        resetCalendar();
        dateConverterCal.add(Calendar.DAY_OF_MONTH, (int)d);
        return dateConverterCal.getTime();
    }

    protected Date convertDate(String date) {
        try {
            return sdfyyyymmdd.parse(date);
        } catch (ParseException e) {
            log.error("Date conversion from " + date + " failed", e);
            return null;
        }
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    public Map<String, Object> getDataFrame() {
        return dataFrame;
    }
}
