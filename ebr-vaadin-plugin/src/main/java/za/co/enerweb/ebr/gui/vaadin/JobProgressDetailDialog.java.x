package za.co.enerweb.ebr.gui.vaadin;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.task.TaskExecutionMetadata;
import za.co.enerweb.toolbox.vaadin.InfoDialog;

import com.vaadin.ui.Component;

@Slf4j
public class JobProgressDetailDialog extends InfoDialog
implements JobProgressListener {
    private static final long serialVersionUID = 1L;

    private JobProgressComponent progressComponent;

    public JobProgressDetailDialog(final TaskExecutionMetadata taskExecutionMetadata) {
        progressComponent = new JobProgressComponent();
        progressComponent.setSizeFull();
        progressComponent.setTaskExecutionMetadata(taskExecutionMetadata);
        progressComponent.setStatusIsLink(false);
        setName(taskExecutionMetadata.getJobId());
        setWidth("500px");
        setHeight("400px");
    }

    @Override
    public Component getInfoDialogContent() {
        setModal(true);
        // progressComponent.addJobProgressListener(this);
        progressComponent.setSizeFull();
        return progressComponent;
    }

    public void populate() {
        synchronized (progressComponent.getApplicationMutex()) {
            progressComponent.update();
            center();
        }
    }

    @Override
    public void contentUpdated() {
        synchronized (progressComponent.getApplicationMutex()) {
            progressComponent.updateContent();
            center();
        }
    }

    @Override
    public void finalizingAterTaskDone(TaskExecutionMetadata taskExecutionMetadata) {
        // synchronized (progressComponent.getApplicationMutex()) {
        // log.debug("Updating renderers");
        // }
    }

    public void start() {
        setCaption(progressComponent.getTaskExecutionMetadata().getTaskMetadata()
            .getTitle());
        progressComponent.start();
    }

    public void join() {
        progressComponent.join();
    }
}
