package za.co.enerweb.ebr.gui.vaadin.messaging;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.toolbox.vaadin.messaging.Message;
import za.co.enerweb.toolbox.vaadin.messaging.MessageListener;

import com.vaadin.ui.Component;

/**
 * All components added to this that implements {@link MessageListener} will get
 * onMessage events.
 * This follows the publish + subscribe model = observer pattern.
 * @deprecated use za.co.enerweb.toolbox.vaadin.messaging.MessageBus
 */
@Deprecated
@NoArgsConstructor
@Slf4j
public class MessageBus extends za.co.enerweb.toolbox.vaadin.messaging.
    MessageBus {

    private static final long serialVersionUID = 1L;

    public MessageBus(Component component) {
        super(component);
    }

    /**
     * Call this to send a message to all of its listening components.
     * @param message
     */
    @Deprecated
    public void fireMessage(Message message) {
        if (message.getSource() == null) {
            throw new IllegalArgumentException(
                "Message source may not be null");
        }
        fireMessageOnComponents(getComponents().iterator(), message);
    }

    /**
     * Overrides this method so it can still call the old MessageListeners
     */
    protected void fireMessageOnComponent(Component targetComponent,
        Message message) {
        if (targetComponent instanceof za.co.enerweb.ebr.gui.vaadin.messaging
            .MessageListener &&
            !targetComponent.equals(message.getSource())) {
            // maybe this should happen in a background thread
            // sleeping a while between messages
            za.co.enerweb.ebr.gui.vaadin.messaging.MessageListener listener =
                (za.co.enerweb.ebr.gui.vaadin.messaging.MessageListener)
                targetComponent;
            // ScreenMessageQueue.queueMessage(listener, screenMessage);

            if (message instanceof za.co.enerweb.ebr.gui.vaadin.messaging
                .Message) {
                za.co.enerweb.ebr.gui.vaadin.messaging.Message oldStyleMessage =
                    (za.co.enerweb.ebr.gui.vaadin.messaging.Message) message;
                try {
                    log.debug("Sending "
                        + message + "\n to " + listener);
                    listener.onMessage(oldStyleMessage);
                } catch (Exception e) {
                    log.error("An error occured while sending "
                        + message + " to " + listener, e);
                }
            } else {
                log.error("You trying to send new-style-messages[1] on an " +
                    "old-style-messagebus[2] which listerners can only" +
                    "handle the old type messages.\n" +
                    "I'd suggest you upgrade your message bus to [3].\n" +
                    "[1] za.co.enerweb.toolbox.vaadin.messaging.Message\n" +
                    "[2] za.co.enerweb.ebr.gui.vaadin.messaging.MessageBus\n" +
                    "[3] za.co.enerweb.toolbox.vaadin.messaging.MessageBus"
                    );
            }
        }
        super.fireMessageOnComponent(targetComponent, message);
    }
}
