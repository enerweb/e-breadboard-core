package za.co.enerweb.ebr.gui.vaadin.time_machine.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

import org.joda.time.DateTime;
import org.joda.time.Period;

import za.co.enerweb.toolbox.vaadin.messaging.Message;


/**
 * This is the initial message to request components to render.
 * Component can take as long as they wish
 */

@Data
@EqualsAndHashCode(of="uniqueId", callSuper=false)
@AllArgsConstructor
@Slf4j
public class MRenderTimeRange extends Message{

    private String uniqueId;// this is a non business Id, it will make the hashCode unique

    private DateTime date;
    private Period periodToInc;
    private boolean lastRender;
    private boolean firstRender;

}
