package za.co.enerweb.ebr.gui.vaadin.portlet;

import com.vaadin.ui.HorizontalLayout;


public interface IPortalTopPanel {

    HorizontalLayout generateTopPanel();

}
