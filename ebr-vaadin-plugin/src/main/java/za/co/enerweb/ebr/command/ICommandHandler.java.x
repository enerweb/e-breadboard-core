package za.co.enerweb.ebr.command;

import java.io.Serializable;

import com.vaadin.Application;

@Deprecated
public interface ICommandHandler extends Serializable {
    public void setParmeter(String name, Object value);

    public boolean execute(Application context);
}
