package za.co.enerweb.ebr.command;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

@Deprecated
@Slf4j
public class Command implements Serializable {
    private static final long serialVersionUID = 1L;
    protected Map<String, Object> parameters = new HashMap<String, Object>();
    protected String commandName;
    protected Class<? extends ICommandHandler> handler;

    public Command() {

    }

    public Command(final String cmdName,
        final Class<? extends ICommandHandler> handler) {
        commandName = cmdName;
        this.handler = handler;
    }

    public ICommandHandler instantiateHandler() {
        if (handler != null) {
            try {
                ICommandHandler ch = handler.newInstance();
                for (String pName : parameters.keySet()) {
                    ch.setParmeter(pName, parameters.get(pName));
                }
                return ch;
            } catch (Throwable e) {
                log.debug("Could not instantiate handler.", e);
            }
        }
        return null;
    }

    public Object getParameter(final String name) {
        if (parameters.containsKey(name)) {
            return parameters.get(name);
        }
        return null;
    }

    public int getParameterAsInt(final String name, final int def) {
        Object v = getParameter(name);
        if (v != null) {
            if (v instanceof Integer) {
                return (Integer) v;
            }
            if (v instanceof String) {
                return Integer.parseInt((String) v);
            }
        }
        return def;
    }

    public String getParameterAsStr(final String name, final String def) {
        Object v = getParameter(name);
        if (v != null) {
            return (String) v;
        }
        return def;
    }

    public void setParmeter(final String name, final Object value) {
        parameters.put(name, value);
    }

    public String getCommandName() {
        return commandName;
    }
}
