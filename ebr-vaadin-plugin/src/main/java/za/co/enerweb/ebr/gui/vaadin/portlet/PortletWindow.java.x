package za.co.enerweb.ebr.gui.vaadin.portlet;

import static lombok.AccessLevel.PROTECTED;
import lombok.Getter;
import za.co.enerweb.toolbox.vaadin.messaging.MessageBusFactory;

import com.vaadin.data.Item;
import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeButton;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.Reindeer;
import com.vaadin.ui.themes.Runo;

public class PortletWindow extends ToolbarWindow {

    private static final long serialVersionUID = 1L;

    private float lastWidth;
    private float lastHeight;
    private int lastX;
    private int lastY;

    private AnimatorProxy ap = new AnimatorProxy();

    protected HorizontalLayout tools = new HorizontalLayout();

    @Getter(value = PROTECTED)
    private VerticalLayout portletLayout = new VerticalLayout();

    public PortletWindow(String name, boolean addCloseButton, Resource icon) {
        super(name);
        portletLayout.setSizeFull();
        setContent(portletLayout);

        this.setIcon(icon);

        center();
        // setWidth("400px");
        // setHeight("300px");

        this.setSizeUndefined();
        init(addCloseButton);
        this.setClosable(false);
    }

    public void init(boolean addCloseButton) {

        // this.setContent(buildContent());

        tools.addStyleName(Runo.LAYOUT_DARKER);
        tools.setSpacing(true);

        NativeButton closeButton =
            new NativeButton(null, new Button.ClickListener() {
                public void buttonClick(ClickEvent event) {

                    PortletWindow.this.close();
                    Window parent = PortletWindow.this.getParent();
                    if (parent != null) {
                        parent.removeWindow(PortletWindow.this);
                    }

                }
            });
        ThemeResource close = new ThemeResource("../runo/icons/32/cancel.png");

        if (close != null) {
            closeButton.setIcon(close);
            closeButton.addStyleName(Runo.BUTTON_LINK);
        }

        closeButton.setDescription("Close");
        // b.setIcon(new ClassResource("minimize.png", this.getApplication()));

        // b.addStyleName(Reindeer.BUTTON_LINK);
        //

        NativeButton maximizeButton =
            new NativeButton(null, new Button.ClickListener() {
                public void buttonClick(ClickEvent event) {
                    if (PortletWindow.this.getWidth() != 100) {
                        lastWidth = PortletWindow.this.getWidth();
                        lastHeight = PortletWindow.this.getHeight();
                        lastX = PortletWindow.this.getPositionX();
                        lastY = PortletWindow.this.getPositionY();
                        PortletWindow.this.setSizeFull();
                        PortletWindow.this.center();
                    } else {

                        PortletWindow.this.setWidth(lastWidth,
                            Window.UNITS_PIXELS);
                        PortletWindow.this.setHeight(lastHeight,
                            Window.UNITS_PIXELS);
                        PortletWindow.this.setPositionX(lastX);
                        PortletWindow.this.setPositionY(lastY);
                    }
                    event.getButton().focus();
                }
            });
        maximizeButton.setDescription("Maximize");

        ThemeResource max = new ThemeResource("../runo/icons/32/arrow-up.png");

        if (max != null) {
            maximizeButton.setIcon(max);
            maximizeButton.addStyleName(Runo.BUTTON_LINK);
        }
        //
        //
        tools.addComponent(maximizeButton);
        if (addCloseButton) {
            tools.addComponent(closeButton);
        }
        this.setToolbar(tools);

    }

    private ComponentContainer buildContent() {
        TabSheet tabs = new TabSheet();
        tabs.setSizeFull();
        // tabs.addStyleName(Reindeer.TABSHEET_SMALL);

        CssLayout wrap = new CssLayout();
        wrap.setMargin(true);
        wrap.setWidth("100%");
        wrap.addComponent(new Label(
            "<p><strong>The size of the window should be specified, " +
                "otherwise it will overflow the content when the user " +
                "resizes the window smaller for the first time.</strong></p>" +
                "<p>After the first time, everything should work normally." +
                "</p><p>Morbi euismod magna ac lorem rutrum elementum. " +
                "Donec viverra auctor lobortis. Pellentesque eu est a " +
                "nulla placerat dignissim. Morbi a enim in magna semper " +
                "bibendum. Etiam scelerisque, nunc ac egestas consequat, " +
                "odio nibh euismod nulla, eget auctor orci nibh vel nisi." +
                " Aliquam erat volutpat. Mauris vel neque sit amet nunc " +
                "gravida congue sed sit amet purus. Quisque lacus quam," +
                " egestas ac tincidunt a, lacinia vel velit. Aenean " +
                "facilisis nulla vitae urna.</p>",
            Label.CONTENT_XHTML));
        tabs.addTab(wrap, "Intro", null);

        Table t = new Table();
        t.addStyleName(Reindeer.TABLE_BORDERLESS);
        t.setSizeFull();
        t.addContainerProperty("Foo", String.class, null);
        t.addContainerProperty("Bar", String.class, null);
        for (int i = 1; i <= 1000; i++) {
            Item item = t.addItem(i);
            item.getItemProperty("Foo").setValue("Foo value " + i);
            item.getItemProperty("Bar").setValue("Bar value " + i);
        }
        tabs.addTab(t, "Dummy Table", null);

        return tabs;
    }

    /**
     * Actually show the dialog and register it with the application's
     * messagebus.
     */
    public void show(Application application) {
        application.getMainWindow().addWindow(this);
        MessageBusFactory.addComponent(application, this);
    }

    /**
     * Actually show the dialog.
     * @param someAttachedComponent just used to get the application
     */
    public void show(Component someAttachedComponent) {
        show(someAttachedComponent.getApplication());
    }
}
