package za.co.enerweb.ebr;

import java.io.IOException;
import java.util.Collection;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.annotations.OnEbrReload;
import za.co.enerweb.ebr.server.HomeFile;
import za.co.enerweb.ebr.server.HomeFileCategory;
import za.co.enerweb.ebr.server.HomeFileSpec;
import za.co.enerweb.ebr.server.IEbrServer;
import za.co.enerweb.ebr.server.IHomeService;
import za.co.enerweb.ebr.task_layout.ITaskLayoutRegistry;
import za.co.enerweb.toolbox.io.ResourceUtils;
import za.co.enerweb.toolbox.string.StringUtils;
import za.co.enerweb.toolbox.vaadin.Styles;

@Slf4j
public class VaadinPlugin {
    private static final String MODULE_NAME = "ebr-core-vaadin-plugin";
    private static final String HTML_LAYOUT_EXTENTION = "html";
    public static final String STYLESHEET_EXTENTION = "css";
    @Deprecated
    public static final String RESOURCES_PATH_STYLESHEETS =
        VaadinUrlService.STYLESHEETS;
    @Deprecated
    public static final String RESOURCES_PATH_JAVASCRIPT =
        VaadinUrlService.JAVASCRIPT;

    /**
     * @deprecated use Styles.STYLE_PADDED_CONTENT directly
     */
    @Deprecated
    public static final String STYLE_PADDED_CONTENT
        = Styles.STYLE_PADDED_CONTENT;

    public static final String TECHNOLOGY_NAME = "vaadin";
    public static final String LAYOUT_PROPERTY_LAYOUT_RELATIVE_PATH =
        ".LayoutRelativePath";
    public static final String README = "README.txt";

    private static String customCss = "";

    @OnEbrReload(synchronous=true)
    public static void onEbrReloadImmediate() {
//        log.debug("VaadinPlugin.onEbrReload()");
        IHomeService homeService = EbrFactory.getHomeService();
        registerHtmlLayouts(homeService);
        readStylesheets(homeService);
    }

    @OnEbrReload
    public static void onEbrReloadAsync() {
//        log.debug("VaadinPlugin.onEbrReloadAsync()");
        // registerLayout(null, TaskBasedTaskLayout.class);
        // registerLayout(TaskBasedTaskLayout.ID, TaskBasedTaskLayout.class);
        // registerLayout(MvcTaskLayout.ID, MvcTaskLayout.class);
        // registerLayout("default", MvcTaskLayout.class, MvcTaskLayout.ID,
        // null);


//        if (!SystemConfiguration.isUseTestHome()) {
//            EbrServer.getBackgroundTaskRunner().invokeLater(
//                new Task() {
//                    @Override
//                    public Object run() throws Exception {
        try {
            writeOutResources();
        } catch (IOException e) {
            log.warn("Could not write resources", e);
        }
//                        return null;
//                    }
//                });
        // }
    }

    /**
     * find the custom html templates and initialise the custom layouts with it
     * @param homeService
     */
    public static void registerHtmlLayouts(final IHomeService homeService) {
        Iterable<HomeFile> homeFiles = homeService.findFiles(
            new HomeFileSpec(HomeFileCategory.FRONTEND,
                VaadinPlugin.TECHNOLOGY_NAME,
                ITaskLayoutRegistry.RESOURCES_PATH_TASK_LAYOUTS),
                "html");
        // for (HomeFile homeFile : homeFiles) {
        // registerLayout(homeFile.getRelPath(), HtmlTaskLayout.class,
        // null, homeFile);
        // }
    }

    /**
     * reads all the .css files in ${EBR_HOME}/vaadin/stylesheets
     * into memmory for now.
     */
    public static void readStylesheets(final IHomeService homeService) {
        StringBuilder sb = new StringBuilder();

        Iterable<HomeFile> homeFiles = homeService.findFiles(
            new HomeFileSpec(HomeFileCategory.FRONTEND,
                VaadinPlugin.TECHNOLOGY_NAME,
                RESOURCES_PATH_STYLESHEETS), "css");
        for (HomeFile homeFile : homeFiles) {
            try {
                sb.append(homeFile.getContent());
            } catch (IOException e) {
                log.warn("Could not load css file: " + homeFile, e);
            }
        }
        customCss = sb.toString();
    }

    // XXX: We could probably factor this layout stuff into a common place
    // once we have more than one layout specification.
    public static void writeOutResources() throws IOException {
        IHomeService homeService = EbrFactory.getHomeService();
        homeService.writeFile(
            new HomeFileSpec(HomeFileCategory.FRONTEND,
                VaadinPlugin.TECHNOLOGY_NAME,
                ITaskLayoutRegistry.RESOURCES_PATH_TASK_LAYOUTS, README),
            "Every file with a '" + HTML_LAYOUT_EXTENTION
                + "' extension in this directory is "
                + "loaded as a html layout.");
    }

    public static Collection<String> getExampleResourcePaths()
        throws ClassNotFoundException {
        String examplePackage = getExamplesPath();
        Collection<String> examples =
            ResourceUtils.getResourcesForPackage(examplePackage);
        return examples;
    }

    private static String getExamplesPath() {
        return IEbrServer.RESOURCE_PACKAGE
            + "/" + StringUtils.camelCaseToUnderscoreSeparated(TECHNOLOGY_NAME)
            + "/" + ITaskLayoutRegistry.RESOURCES_PATH_EXAMPLE_LAYOUTS;
    }

    // private static void registerLayout(final String id,
    // final Class<? extends IVaadinTaskLayout> klass) {
    // // registerLayout(id, klass, null, null);
    // }
    //
    // private static void registerLayout(final String id,
    // final Class<? extends IVaadinTaskLayout> klass,
    // final String deprecatedById, final HomeFile homeFile) {
    // TaskLayoutMetadata layoutMetadata = new TaskLayoutMetadata();
    // layoutMetadata.setTypeClass(klass);
    // layoutMetadata.setDeprecatedById(deprecatedById);
    // if (homeFile != null) {
    // layoutMetadata.getAuxProperties().put(
    // LAYOUT_PROPERTY_LAYOUT_RELATIVE_PATH, homeFile.getRelPath());
    // }
    // EbrFactory.getTaskLayoutRegistry().registerLayout(id,
    // TECHNOLOGY_NAME, layoutMetadata);
    // }

    /**
     * Don't call this directly, rather use the VaadinConfigurationService
     * @return
     */
    public static String getCustomCss() {
        return customCss;
    }

    // public static IWindowService getWindowService() {
    // return EbrFactory.lookupEjb(MODULE_NAME, IWindowService.class);
    // }


}
