package za.co.enerweb.ebr.renderer.vaadin.progress;

import za.co.enerweb.ebr.VaadinPlugin;
import za.co.enerweb.ebr.annotations.EnvVarRenderer;
import za.co.enerweb.ebr.renderer.RendererMode;
import za.co.enerweb.ebr.renderer.vaadin.AbstractVaadinRenderer;

import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ProgressIndicator;
import com.vaadin.ui.UI;

@EnvVarRenderer(id = ProgressRenderer.ID,
    technology = VaadinPlugin.TECHNOLOGY_NAME,
    supportedTypes = {"java.io.Serializable"},
    rankPerType = {0},
    supports = RendererMode.VIEW)
@Deprecated
public class ProgressRenderer extends AbstractVaadinRenderer implements
        ITaskMonitorable {

    private static final long serialVersionUID = 937976871855255949L;
    public static final String ID = "progress";

    private final ProgressIndicator progMain = new ProgressIndicator(1.0f);
    private final Label lblDesc = new Label();
    private final HorizontalLayout layout = new HorizontalLayout();

    @Override
    public void done() {
        done("Completed!");
    }

    @Override
    public void done(String msg) {
        updateUi(100, msg);
    }

    @Override
    public Component getComponent() {
        layout.setSpacing(true);

        progMain.setPollingInterval(500);
        progMain.setIndeterminate(false);
        progMain.setImmediate(true);
        layout.addComponent(progMain);

        lblDesc.setValue("0%");
        lblDesc.setImmediate(true);
        layout.addComponent(lblDesc);

        setValue(this);

        return layout;
    }

    @Override
    public void reset() {
        updateUi(0, "0%");
    }

    @Override
    public void start() {
        updateUi(0, "0%");
    }

    @Override
    public void update(final double progress, String msg) {
        String detail = null;

        if (msg != null && msg.trim().length() > 0) {
            detail = " - " + msg.trim();
        }

        updateUi(progress, progress + "%" + detail);
    }

    @Override
    public void update(int progress, String msg) {
        update((double) progress, msg);
    }

    public void updateUi(final double progress, final String desc) {
        // All modifications to Vaadin components should be synchronized
        // over application instance. For normal requests this is done
        // by the servlet. Here we are changing the application state
        // via a separate thread.
        UI.getCurrent()
            .accessSynchronously(new Runnable() {

                @Override
                public void run() {
                    progMain.setValue(new Float(progress / 100));
                    lblDesc.setValue(desc);
                }
            });
    }

}
