package za.co.enerweb.ebr.gui.vaadin.menu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.beanutils.PropertyUtils;

import za.co.enerweb.ebr.gui.vaadin.messages.MEbrSetMainComponent;
import za.co.enerweb.toolbox.vaadin.messaging.MessageBusFactory;

import com.vaadin.ui.Component;
import com.vaadin.ui.Notification.Type;

@Slf4j
public class ActivateScreenMenuItem extends AbstractMenuItem {

    private Class<? extends Component> componentClass;

    /**
     * only instantiate it once?! we can make that optional if needed.
     */
    private transient Component component;

    private Map<String, Serializable> properties = new HashMap<
        String, Serializable>();

    public ActivateScreenMenuItem(String id, final int rank,
        final String caption,
        final Class<? extends Component> componentClass) {
        super(id, rank, caption);
        this.componentClass = componentClass;
    }

    public ActivateScreenMenuItem(final int rank, final String caption,
        final Class<? extends Component> componentClass) {
        this(componentClass.getName(), rank, caption, componentClass);
    }

    public ActivateScreenMenuItem set(String name, Serializable value) {
        properties.put(name, value);
        return this;
    }

    @Override
    public void menuSelected() {
        if (component == null) {
            try {
                component = componentClass.newInstance();
                for (Entry<String, Serializable> property:
                    properties.entrySet()) {
                    if (PropertyUtils.isWriteable(component,
                        property.getKey())) {
                        PropertyUtils.setSimpleProperty(component,
                            property.getKey(), property.getValue());
                    }
                }
                // addToCurrentMessageBus(component);
            } catch (Exception e) {
                log.warn("Could not activate screen: "
                            + componentClass.getName(), e);
                com.vaadin.ui.Notification.show(
                    "Could not activate screen: "
                        + componentClass.getSimpleName(),
                    Type.WARNING_MESSAGE);
            }
        }
        if (component != null) {
            List<String> contextList = new ArrayList<String>(getContext());
            if (!contextList.isEmpty()) {
                contextList.remove(0); // remove main
            }
            contextList.add(getCaption());

            // FIXME: set the main component on the main ui with a message on
            // the bus
            // getIMainApplication().setMainComponent(component,
            // contextList.toArray(new String[contextList.size()]));
            MessageBusFactory.fireMessage(component,
                new MEbrSetMainComponent(component, contextList));
        }
    }

}
