package za.co.enerweb.ebr.renderer.vaadin.dataresource;

import static za.co.enerweb.ebr.gui.vaadin.Styles.LINK;
import lombok.Getter;
import lombok.Setter;

import org.apache.commons.lang.StringUtils;

import com.vaadin.server.Resource;
import com.vaadin.ui.Link;

/**
 * If link text is supplied use that as the caption
 * otherwise call createCaption
 */
public abstract class AbstractLink extends Link {
    private static final long serialVersionUID = 1L;

    private String linkText = null;
    @Getter @Setter
    private int captionLength;

    public AbstractLink() {
        this(null);
    }

    public AbstractLink(String linkText) {
        this(linkText, 50);
    }

    public AbstractLink(String linkText, int captionLength) {
        this.linkText = linkText;
        this.captionLength = captionLength;
    }

    public void setLinkText(String linkText) {
        this.linkText = linkText;
    }

    public void attach() {
        Resource resource = getResource();
        if (resource != null) {
            setResource(resource);
        }
        if (StringUtils.isBlank(linkText)) {
            setCaption(StringUtils.abbreviateMiddle(
                createCaption(), "...", captionLength));
        } else {
            setCaption(StringUtils.abbreviate(linkText, captionLength));
        }
        setTargetName("_blank"); // open in new window
        // setWidth(url.length(), UNITS_EM);// make it as wide as the url
        setStyleName(LINK);
    }

    /**
     * only gets called if you don't provide a linkText,
     * you should normally override this
     * @return
     */
    public String createCaption() {
        return linkText;
    }

    public abstract Resource getResource();
}
