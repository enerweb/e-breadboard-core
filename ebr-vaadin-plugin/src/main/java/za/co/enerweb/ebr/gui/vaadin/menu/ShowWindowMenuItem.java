package za.co.enerweb.ebr.gui.vaadin.menu;

import lombok.extern.slf4j.Slf4j;

import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

@Slf4j
@SuppressWarnings("serial")
public class ShowWindowMenuItem extends AbstractMenuItem {

    private Class<? extends Window> windowClass;

    public ShowWindowMenuItem(final int rank, final String caption,
        final Class<? extends Window> windowClass) {
        super(windowClass.getName(), rank, caption);
        this.windowClass = windowClass;
    }

    @Override
    public void menuSelected() {
        try {
            Window window = windowClass.newInstance();
            UI.getCurrent().addWindow(window);
            addToCurrentMessageBus(window);
        } catch (Exception e) {
            log.warn("Could not activate screen: "
                            + windowClass.getName(), e);
            Notification.show(
                    "Could not activate screen: "
                        + windowClass.getSimpleName(),
                Type.WARNING_MESSAGE);
        }
    }
}
