package za.co.enerweb.ebr.renderer.vaadin;

import java.io.Serializable;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.env.EnvVarMetadata;
import za.co.enerweb.ebr.env.IEnvironmentManager;
import za.co.enerweb.ebr.gui.vaadin.EnvVarUtils;
import za.co.enerweb.ebr.server.IEbrServer;

import com.vaadin.ui.Component;

/*
 * XXX: Make it possible to use renderers outside of a Env context
 * to allow for non env things (see ParameterDataTable)
 * i.e. use a Propery-like interface
 * instead of env directly. But I think some renderers need an envId
 * so maybe in this case we don't support them.
 */
@SuppressWarnings("serial")
@Slf4j
public abstract class AbstractVaadinRenderer implements VaadinRenderer {
    private String envId;
    private String varName;
    private EnvVarMetadata envVarMetadata;
    protected transient IEbrServer oe =
        EbrFactory.getEbrServer();
    protected transient IEnvironmentManager envManager =
        EbrFactory.getEnvironmentManager();


    public abstract Component getComponent();

    public Component getComponent(final String envId,
        final EnvVarMetadata envVarMetadata) {
        this.envId = envId;
        this.envVarMetadata = envVarMetadata;
        varName = envVarMetadata.getName();
        return getComponent();
    }

    /**
     * @return
     * @deprecated use getVariableMetaData()
     */
    @Deprecated
    protected EnvVarMetadata getMetaData() {
        return getVariableMetaData();
    }

    protected EnvVarMetadata getVariableMetaData() {
        return envVarMetadata;
    }

    /**
     * @param <T>
     * @return null if the variable is undefined
     */
    @SuppressWarnings("unchecked")
    protected <T extends Serializable> T getValue() {
        return (T) envManager.getVariableValueOrNull(getEnvId(), getVarName());
    }

    protected void setValue(java.io.Serializable value) {
        new EnvVarUtils().setValue(
            getEnvId(), envVarMetadata, value);
    }

    /**
     * @param name
     * @param defaultValue
     * @return
     * @deprecated use getVariableProperty()
     */
    @Deprecated
    protected Serializable findMetaDataProperty(final String name,
        final Serializable defaultValue) {
        return getVariableProperty(name, defaultValue);
    }

    /**
     * Get a variable metadata auxiliary property
     * @param <T>
     * @param name
     * @param defaultValue
     * @return
     */
    @SuppressWarnings("unchecked")
    protected <T extends Serializable> T getVariableProperty(final String name,
        final T defaultValue) {
        Map<String, Serializable> properties = envVarMetadata
            .getAuxProperties();

        if (properties.containsKey(name)) {
            Serializable value = properties.get(name);
            try {
                return (T)value;
            } catch (ClassCastException e) {
                log.error("Could not cast variable property {} ({}"
                    + ") to the expected type:\n{}", new Object[] {
                    name, value.getClass().getSimpleName(),
                    e.getLocalizedMessage()});
            }
        }
        // log.debug("Could not find variable property {}.{}, "
        // + "using the default: {}.", new Object[] {getVarName(), name,
        // defaultValue});
        return defaultValue;
    }

    /**
     * @return the envId
     */
    protected String getEnvId() {
        return envId;
    }

    /**
     * @return the varName
     */
    protected String getVarName() {
        return varName;
    }
}
