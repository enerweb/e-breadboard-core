package za.co.enerweb.ebr.gui.vaadin.windows;

import javax.ejb.Local;

import com.vaadin.Application;
import com.vaadin.ui.Window;

@Local
// @Remote
public interface IWindowService {
    /**
     * @param windowName
     * @param window
     * @return window if the window is unknown (application can also decide if
     *         it wants to return the old window or a new one)
     */
    Window getWindow(Application application, Window window, String windowName);
}
