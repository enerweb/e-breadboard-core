package za.co.enerweb.ebr.renderer.vaadin.dataresource;

import java.io.InputStream;

import lombok.SneakyThrows;
import za.co.enerweb.ebr.server.HomeFile;

import com.vaadin.server.StreamResource.StreamSource;

final class HomeFileStreamSource implements StreamSource {
    private final HomeFile homeFile;
    private static final long serialVersionUID = 1L;

    public HomeFileStreamSource(HomeFile homeFile) {
        this.homeFile = homeFile;
    }

    @SneakyThrows
    @Override
    public InputStream getStream() {
        return homeFile.getInputStream();
    }

}
