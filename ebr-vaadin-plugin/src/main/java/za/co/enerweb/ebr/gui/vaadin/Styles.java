package za.co.enerweb.ebr.gui.vaadin;

/**
 * A set of styles that is assumed to be in the theme
 */
public interface Styles {
    String LINK = "ebr-link";
    String IMPORTANT_DETAIL = "ebr-important-detail";
    String GOOD = "ebr-good";
    String BAD = "ebr-bad";
    String UGLY = "ebr-ugly";
}
