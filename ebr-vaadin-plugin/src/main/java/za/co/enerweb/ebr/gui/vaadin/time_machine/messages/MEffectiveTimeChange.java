package za.co.enerweb.ebr.gui.vaadin.time_machine.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import org.joda.time.Period;

import za.co.enerweb.toolbox.vaadin.messaging.Message;

/**
 * This is the secondary message to request a render of a specific point in time.
 * Component can take as long as they wish
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class MEffectiveTimeChange extends Message {

    private MRenderTimeRange date;
    private Period periodToInc;
    private int count;
    private int size;
}
