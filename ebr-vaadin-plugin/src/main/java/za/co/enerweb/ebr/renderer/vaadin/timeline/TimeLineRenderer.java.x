package za.co.enerweb.ebr.renderer.vaadin.timeline;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.VaadinPlugin;
import za.co.enerweb.ebr.annotations.EnvVarRenderer;
import za.co.enerweb.ebr.renderer.RendererMode;
import za.co.enerweb.ebr.renderer.vaadin.table.BaseDataFrameRenderer;

import com.vaadin.addon.timeline.Timeline;
import com.vaadin.addon.timeline.Timeline.ChartMode;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;

@EnvVarRenderer(id = TimeLineRenderer.ID, technology = VaadinPlugin.TECHNOLOGY_NAME, supportedTypes = { "java.util.Map" }, rankPerType = { 0 }, supports = RendererMode.EDIT_AND_VIEW)
@Slf4j
public class TimeLineRenderer extends BaseDataFrameRenderer {
    private static final long serialVersionUID = 1L;
    public static final String ID = "timeline";
    public static final List<Color> COLORS = new ArrayList<Color>();
    static {
        COLORS.add(new Color(58, 95, 205)); // dark blue blueish
        COLORS.add(new Color(0, 238, 0)); // light green
        COLORS.add(new Color(191, 62, 255)); // purple
        COLORS.add(new Color(238, 64, 0)); // orange red
        COLORS.add(new Color(204, 204, 20)); // gray
        COLORS.add(new Color(51, 0, 0)); // black
        COLORS.add(new Color(113, 198, 113)); // sea green
        COLORS.add(new Color(198, 113, 113)); // salmon
        COLORS.add(new Color(142, 142, 56)); // turqoise
        COLORS.add(new Color(205, 155, 155)); // salmon
        COLORS.add(new Color(0, 100, 0));
        COLORS.add(new Color(238, 238, 0));
    }

    public static String PARM_TITLE = "title";
    public static String PARM_GRAPH_TYPE = "type";
    public static String PARM_SHOW_SCROLLER = "scroller";

    // private members
    private Timeline timeline;

    public TimeLineRenderer() {

    }

    @Override
    public Component getComponent() {
        if (initializeDataFrame()) {
            // create the timeline
            timeline = new Timeline();
            timeline.setGraphOutlineThickness(2.0);
            updateGraphTitle();
            updateBrowserVisible();
            updateGraphType();
            timeline.setWidth(getVariableProperty(AUX_PROP_WIDTH, "400px"));
            timeline.setHeight(getVariableProperty(AUX_PROP_HEIGHT, "300px"));
            timeline.setChartModesVisible(false);
            create();
            return timeline;
        } else {
            return new Label("The dataframe couldn't be intialized.");
        }
    }

    protected void create() {
        Object[] dates = createDateArray();
        String[] dataframeKeys = dataFrame.keySet().toArray(new String[0]);
        String[] columns = getHeaders(dataframeKeys);
        for (int col = 1; col < columns.length; col++) {
            IndexedContainer container = new IndexedContainer();
            container.addContainerProperty(Timeline.PropertyId.TIMESTAMP,
                    Date.class, null);
            container.addContainerProperty(Timeline.PropertyId.VALUE,
                    Float.class, 0);
            Object[] values = toArray(dataFrame.get(dataframeKeys[col]));
            for (int row = 0; row < values.length; row++) {
                Item item = container.addItem(dates[row]);
                item.getItemProperty(Timeline.PropertyId.TIMESTAMP).setValue(
                        dates[row]);
                item.getItemProperty(Timeline.PropertyId.VALUE).setValue(
                        values[row]);
            }
            Color c = COLORS.get(col);
            int r = c.getRed();
            int g = c.getGreen();
            int b = c.getBlue();
            timeline.addGraphDataSource(container);
            timeline.setGraphLegend(container, columns[col]);
            timeline.setGraphOutlineColor(container, c);
            timeline.setGraphFillColor(container, new Color(r, g, b, 40));
            timeline.setBrowserOutlineColor(container, c);
        }
    }

    protected Object[] createDateArray() {
        Object[] values = toArray(dataFrame.values().iterator().next());
        Date[] dates = new Date[values.length];
        for (int i = 0; i < values.length; i++) {
            dates[i] = (Date) convertObject(0, 1, values[i]);
        }
        return dates;
    }

    @Override
    protected Object convertObject(int column, int row, Object value) {
        if (column == 0) {
            return toDate(value);
        } else {
            return value;
        }
    }

    private void updateGraphTitle() {
        Object tmp = null;
        if ((tmp = getParameter(PARM_TITLE)) != null) {
            timeline.setCaption((String) tmp);
        }
    }

    private void updateBrowserVisible() {
        boolean showIt = false;
        Object tmp = null;
        if ((tmp = getParameter(PARM_SHOW_SCROLLER)) != null) {
            if (tmp instanceof Double)
                showIt = ((Double) tmp).intValue() != 0;
            else if (tmp instanceof Integer)
                showIt = ((Integer) tmp).intValue() != 0;
            else if (tmp instanceof Boolean)
                showIt = ((Boolean) tmp).booleanValue();
        }
        timeline.setBrowserVisible(showIt);
    }

    private void updateGraphType() {
        if (getParameter(PARM_GRAPH_TYPE) != null) {
            String type = getParameter(PARM_GRAPH_TYPE).toString().toUpperCase();
            try {
                ChartMode cm = ChartMode.valueOf(type);
                timeline.setChartMode(cm);
            } catch (Exception e) {
                timeline.setChartMode(ChartMode.LINE);
                log.error("Error setting graph type with value " + type);
            }
        } else {
            timeline.setChartMode(ChartMode.LINE);
        }
    }

}
