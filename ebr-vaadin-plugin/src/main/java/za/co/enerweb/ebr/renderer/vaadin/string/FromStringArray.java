package za.co.enerweb.ebr.renderer.vaadin.string;

import java.util.Collection;

import za.co.enerweb.ebr.VaadinPlugin;
import za.co.enerweb.ebr.annotations.EnvVarRenderer;
import za.co.enerweb.ebr.renderer.RendererMode;
import za.co.enerweb.ebr.renderer.vaadin.AbstractVaadinRenderer;

import com.google.common.base.Joiner;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TwinColSelect;

@SuppressWarnings("serial")
@EnvVarRenderer(id = FromStringArray.ID,
    technology = VaadinPlugin.TECHNOLOGY_NAME,
    supportedTypes = {
// "java.lang.Number",
"[Ljava.lang.String;"},
    rankPerType = {50},
    supports = RendererMode.EDIT)
public class FromStringArray extends AbstractVaadinRenderer
    implements ValueChangeListener {
    public static final String ID = "e19c874f-1faf-4957-945a-24500385ae0b";

    private static final String[] peoples = new String[] {"Kgomotso",
        "Kheeven", "Vickus", "Wernardt"};

    private TwinColSelect field;

    /*
     * (non-Javadoc)
     * @see za.co.enerweb.ebr.renderer.VaadinRenderer#getComponent()
     */
    @Override
    public Component getComponent() {
        // todo, should init this from env
        field = new TwinColSelect("Please select some peoples");
        for (String people : peoples) {
            field.addItem(people);
        }
        field.setRows(7);
        field.setNullSelectionAllowed(true);
        field.setMultiSelect(true);
        field.setImmediate(true);
        field.addListener(this);

        return field;
    }

    /*
     * (non-Javadoc)
     * @see com.vaadin.data.Property.ValueChangeListener#valueChange(
     * com.vaadin.data.Property.ValueChangeEvent)
     */
    @SuppressWarnings("unchecked")
    @Override
    public void valueChange(ValueChangeEvent event) {
        Collection<String> strCollection =
            (Collection<String>) field.getValue();
        String[] strs = new String[strCollection.size()];
        int i = 0;
        for (Object element : strCollection) {
            String string = (String) element;
            strs[i++] = string;
        }
        setValue(strs);
        Notification.show(
            Joiner.on(", ").join(strs) + " selected!");
    }

}
