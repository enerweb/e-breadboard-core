package za.co.enerweb.ebr.gui.vaadin;

import com.vaadin.ui.Component;

/**
 * The ebr application can be cast to IMainApplication
 */
public interface IMainApplication {
    void setMainComponent(final Component c,
        final String... breadCrumbStrings);

    void addBreadCrumb(String caption);

    void setBreadCrumbs(final String... breadCrumbStrings);

    void setBreadCrumb(final String caption, final int index);

    String getSessionId();

    void activateMenuItem(String id);
}
