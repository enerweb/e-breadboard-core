package za.co.enerweb.ebr.renderer.vaadin.dataresource;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import javax.xml.bind.ValidationException;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.datatype.dataresource.IDataResource;

import com.vaadin.server.StreamResource.StreamSource;

@Slf4j
public final class DocumentStreamSource implements StreamSource, Serializable {
    private static final long serialVersionUID = 1L;
    private final IDataResource document;

    /**
     * @param document
     */
    public DocumentStreamSource(final IDataResource document) {
        this.document = document;
    }

    @Override
    public InputStream getStream() {
        try {
            return document.getInputStream();
        } catch (IOException e) {
            log.error("Could not render document.",
                e);
            return null;
        } catch (ValidationException e) {
            log.debug("Invalid dataresource", e);
            return null;
        }
    }
}
