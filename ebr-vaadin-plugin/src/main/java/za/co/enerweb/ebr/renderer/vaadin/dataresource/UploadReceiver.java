package za.co.enerweb.ebr.renderer.vaadin.dataresource;

import java.io.IOException;
import java.io.OutputStream;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.server.HomeFile;

import com.vaadin.ui.Upload.Receiver;

@Slf4j
public class UploadReceiver implements Receiver {
    private static final long serialVersionUID = 5233612116511151676L;

    private String fileName;
    private String mtype;
    private HomeFile tempFile;

    public String getFileName() {
        return fileName;
    }

    public String getMimeType() {
        return mtype;
    }

    public HomeFile getTempFile() {
        return tempFile;
    }

    @Override
    public OutputStream receiveUpload(final String filename,
        final String mimetype) {
        fileName = filename;
        mtype = mimetype;

        try {
            tempFile = EbrFactory.getHomeService().getTempFile(filename);
            return tempFile.getOutputStream();

        } catch (IOException e) {
            log.error("Could not create temp file for uploading a file", e);
        }

        return null;
    }
}
