package za.co.enerweb.ebr.gui.vaadin.portlet;

public interface IPortalActionListener {

    public void maximize(boolean maximized);
    public void detachMessageBus(boolean detached);

}
