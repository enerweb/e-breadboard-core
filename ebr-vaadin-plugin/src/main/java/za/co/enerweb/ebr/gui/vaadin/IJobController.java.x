package za.co.enerweb.ebr.gui.vaadin;

import java.io.Serializable;

import za.co.enerweb.ebr.env.EnvVarMetadata;
import za.co.enerweb.ebr.exceptions.TaskExecutionException;
import za.co.enerweb.ebr.exceptions.TaskInstantiationException;
import za.co.enerweb.ebr.exceptions.UnknownTaskExecutionException;
import za.co.enerweb.ebr.task.TaskExecutionMetadata;
import za.co.enerweb.ebr.layout.vaadin.IVaadinTaskLayout;
import za.co.enerweb.ebr.session.SessionMetadata;

import com.vaadin.ui.Component;

public interface IJobController extends Serializable {
    public IVaadinTaskLayout getLayout();

    public SessionMetadata getSession();

    public Component getJobRendererComponent(
        EnvVarMetadata envVarMetadata, boolean editable);

    public void init(ITaskView view, String taskId)
        throws TaskInstantiationException;

    public void run() throws TaskExecutionException, UnknownJobException;

    public void processTaskExecutionMetadata(final TaskExecutionMetadata taskExecutionMetadata);

    void reset();
}
