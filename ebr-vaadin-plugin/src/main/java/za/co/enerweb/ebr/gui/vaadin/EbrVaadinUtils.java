package za.co.enerweb.ebr.gui.vaadin;

import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.VaadinConfigurationService;
import za.co.enerweb.ebr.gui.vaadin.messages.MEbrReload;
import za.co.enerweb.toolbox.vaadin.messaging.MessageBusFactory;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;

public class EbrVaadinUtils {
    public static final String SESSION_ATTR__EBR_SESSION_ID = "ebrSessionId";

    // private final Application application;

    // public EbrVaadinUtils(Application application) {
    // this.application = application;
    // }
    //
    // public EbrVaadinUtils(Component component) {
    // this(component.getApplication());
    // }

    public VaadinConfigurationService getVaadinConfigurationService() {
        return VaadinConfigurationService.getInstance();
    }

    public void reloadEBreadboard() {
        EbrFactory.getEbrServer().reload();
        reloadFrontend();
    }

    /**
     * among others reloads the menu
     */
    public void reloadFrontend() {
        MessageBusFactory.fireMessage(
            // I suppose we can track down the main menu for this, but that
            // looks like some effort with little gain.
            UI.getCurrent().getContent(),
            // application.getMainWindow(),
            new MEbrReload());
    }


    public String getSessionId() {
        return (String) VaadinSession.getCurrent().getAttribute(
            SESSION_ATTR__EBR_SESSION_ID);
    }
}
