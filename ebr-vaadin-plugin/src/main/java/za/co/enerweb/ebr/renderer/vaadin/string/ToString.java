package za.co.enerweb.ebr.renderer.vaadin.string;

import java.io.Serializable;

import za.co.enerweb.ebr.VaadinPlugin;
import za.co.enerweb.ebr.annotations.EnvVarRenderer;
import za.co.enerweb.ebr.renderer.RendererMode;
import za.co.enerweb.ebr.renderer.vaadin.AbstractVaadinRenderer;

import com.vaadin.ui.Component;
import com.vaadin.ui.Label;

@SuppressWarnings("serial")
@EnvVarRenderer(id = ToString.ID,
    technology = VaadinPlugin.TECHNOLOGY_NAME,
    supportedTypes = {"java.io.Serializable"},
    rankPerType = {1},
    supports = RendererMode.EDIT_AND_VIEW)
public class ToString extends AbstractVaadinRenderer {
    public static final String ID = "to-string";
    private Label field = new Label();

    public ToString() {
        // XXX: maybe make this optional but this can stay the default,
        // because maybe the user wants us to automatically wrap the text.
        field.setContentMode(Label.CONTENT_PREFORMATTED);
    }

    /*
     * (non-Javadoc)
     * @see za.co.enerweb.ebr.renderer.VaadinRenderer#getComponent()
     */
    @Override
    public Component getComponent() {
        Serializable value = getValue();
        if (value != null) {
            field.setValue(value.toString());
        }
        return field;
    }

}
