package za.co.enerweb.ebr.renderer.vaadin.dataresource;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.Serializable;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.VaadinPlugin;
import za.co.enerweb.ebr.annotations.EnvVarRenderer;
import za.co.enerweb.ebr.datatype.dataresource.IDataResource;
import za.co.enerweb.ebr.renderer.RendererMode;
import za.co.enerweb.ebr.renderer.vaadin.AbstractVaadinRenderer;

import com.vaadin.server.DownloadStream;
import com.vaadin.server.FileResource;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.VerticalLayout;

@EnvVarRenderer(id = DocumentRenderer.ID,
    technology = VaadinPlugin.TECHNOLOGY_NAME,
    supportedTypes = {
            "za.co.enerweb.ebr.datatype.dataresource.IDataResource",
            "za.co.enerweb.ebr.datatype.doc.ExternalDocument"},
    rankPerType = {50, 70},
    supports = RendererMode.VIEW)
@Slf4j
public class DocumentRenderer extends AbstractVaadinRenderer {
    private static final long serialVersionUID = 1L;
    private static final String AUX_PROP_AUTODOWNLOAD = "autodownload";

    private String width = "100%";
    private String height = "500px";

    public static final String ID = "doc";

    public DocumentRenderer() {

    }

    @Override
    public Component getComponent() {
        final IDataResource document = getValue();

        if (document != null) {

            readProperties();

            try {
                VerticalLayout verticalLayout = new VerticalLayout();

                // TODO: file a vaadin bug:
                // if you do the following, it does not get rendered in
                // the html layout
                // verticalLayout.setSizeFull();

                String fileName = document.getResourceMetadata().getFileName();
                StreamResource streamResource = new StreamResource(
                    new DocumentStreamSource(document), fileName);
                streamResource.setCacheTime(0);

                if (isAutoDownloadActive()) {
                    mainWindow.open(streamResource, "_blank");
                }
                verticalLayout.addComponent(new Link(fileName, streamResource,
                    "_blank",
                     (int) mainWindow.getWidth(),
                    (int) mainWindow.getHeight(), 0));
                return verticalLayout;
            } catch (Exception e) {
                log.debug("Invalid dataresource: "
                    + e.getLocalizedMessage());
                return new Label();
            }

        } else {
            return new Label();
        }
    }

    // TODO: getVariableProperty must be able to convert this to a boolean
    // for you if you pass in a boolean default..
    protected boolean isAutoDownloadActive() {
        Serializable tmp = getVariableProperty(AUX_PROP_AUTODOWNLOAD, 0D);
        if (tmp instanceof Double)
            return ((Double) tmp).intValue() != 0;
        else if (tmp instanceof Integer)
            return ((Integer) tmp).intValue() != 0;
        else if (tmp instanceof Boolean)
            return ((Boolean) tmp).booleanValue();
        else
            return false;
    }

    public class FileDownloadResource extends FileResource {
        private static final long serialVersionUID = 1L;

        public FileDownloadResource(String sourceFile,
            Application application) {
            super(new File(sourceFile), application);
        }

        public DownloadStream getStream() {
            try {
                final DownloadStream ds = new DownloadStream(
                    new FileInputStream(getSourceFile()), getMIMEType(),
                    getFilename());
                ds.setParameter("Content-Disposition", "attachment; filename=\""
                    + getFilename() + "\"");
                ds.setCacheTime(getCacheTime());
                return ds;
            } catch (final FileNotFoundException e) {
                // No logging for non-existing files at this level.
                return null;
            }
        }
    }

    public String getHeight() {
        return height;
    }

    public String getWidth() {
        return width;
    }

    protected void readProperties() {
        setWidth(getVariableProperty("width", getWidth()));
        setHeight(getVariableProperty("height", getHeight()));
    }

    public void setHeight(final String height) {
        this.height = height;
    }

    public void setWidth(final String width) {
        this.width = width;
    }

}
