package za.co.enerweb.ebr.renderer.vaadin.dataresource;

import lombok.Getter;

import org.apache.commons.lang.StringUtils;

import za.co.enerweb.ebr.datatype.dataresource.ResourceUrl;
import za.co.enerweb.ebr.datatype.dataresource.ResourceUrl.HomeFileAndRelPath;

import com.vaadin.server.ExternalResource;
import com.vaadin.server.Resource;
import com.vaadin.server.StreamResource;

/**
 * for showing external and internal links
 */
public class ResourceUrlLink extends AbstractLink {

    private static final long serialVersionUID = 1L;

    @Getter
    private ResourceUrl resourceUrl;

    public ResourceUrlLink(String url) {
        this(new ResourceUrl(url));
    }

    public ResourceUrlLink(String url, String caption) {
        this(new ResourceUrl(url), caption);
    }

    public ResourceUrlLink(ResourceUrl resourceUrl) {
        this(resourceUrl, null);
    }

    public ResourceUrlLink(ResourceUrl resourceUrl, String caption) {
        super(caption);
        this.resourceUrl = resourceUrl;
        if (resourceUrl != null) {
            setDescription(resourceUrl.getUrl());
        }
    }

    public String getUrl() {
        if (resourceUrl == null) {
            return "";
        }
        return resourceUrl.getUrl();
    }

    public String createCaption() {
        String url = getUrl();
//        url = StringUtils.split(url, '?')[0];
        return StringUtils.abbreviateMiddle(url, "...", getCaptionLength());
    }

    public Resource getResource() {
        if (resourceUrl != null) {
            final HomeFileAndRelPath hnr = resourceUrl.getHomeFileAndRelPath();
            if (hnr != null) {
                StreamResource streamResource = new StreamResource(
                    new HomeFileStreamSource(hnr.getHomeFile()),
                    hnr.getFileName()
                    //,
                    //getApplication()
                    );
                streamResource.setCacheTime(0);
                return streamResource;
            }
        }
        return new ExternalResource(getUrl());
    }

}
