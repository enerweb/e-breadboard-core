package za.co.enerweb.ebr.renderer.vaadin.math;

import java.util.ArrayList;
import java.util.List;

import za.co.enerweb.ebr.VaadinPlugin;
import za.co.enerweb.ebr.annotations.EnvVarRenderer;
import za.co.enerweb.ebr.datatype.math.GenericMatrix;
import za.co.enerweb.ebr.datatype.math.GenericMatrix.DoubleMatrix;
import za.co.enerweb.ebr.renderer.RendererMode;
import za.co.enerweb.ebr.renderer.vaadin.AbstractVaadinRenderer;

import com.vaadin.data.Container;
import com.vaadin.data.Container.ItemSetChangeEvent;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.Component;
import com.vaadin.ui.Field;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

@EnvVarRenderer(id = MatrixRenderer.ID,
        technology = VaadinPlugin.TECHNOLOGY_NAME,
        supportedTypes = {"za.co.enerweb.ebr.datatype.math.GenericMatrix"},
        rankPerType = {90}, supports = RendererMode.VIEW)
public class MatrixRenderer extends AbstractVaadinRenderer {
    private static final long serialVersionUID = 1L;
    public static final String ID = "matrix";

    // selectboxes
    private NativeSelect selDim1;
    private int dimension1;
    private NativeSelect selDim2;
    private int dimension2;
    private Table table;

    private List<NativeSelect> indexSelects = new ArrayList<NativeSelect>();

    @Override
    public Component getComponent() {
        final GenericMatrix<Object> matrix = getValue();
        int[] size = matrix.getSize();

        // default selected dimensions
        dimension1 = 0;
        dimension2 = 1;

        // main layout
        HorizontalLayout main = new HorizontalLayout();

        // create a 2 dimensional table
        VerticalLayout vertLayout = new VerticalLayout();
        vertLayout.addComponent(createDescLabel());
        vertLayout.addComponent(create2dTable(0, 1, matrix));
        main.addComponent(vertLayout);

        if (size.length > 2) {
            main.addComponent(createDimensionSelectors(size));
        } else {
            redrawTable(new int[0]);
        }

        return main;
    }

    protected Label createDescLabel() {
        final GenericMatrix<Object> matrix = getValue();
        int[] size = matrix.getSize();

        String desc = "";
        for (int i = 0; i < size.length; i++) {
            desc += size[i];
            if (i < size.length - 1) {
                desc += " X ";
            }
        }

        return new Label("Matrix size: " + desc);
    }

    protected Table create2dTable(final int dim1, final int dim2,
            final GenericMatrix<Object> matrix) {
        int[] size = matrix.getSize();

        table = new Table();
        table.setImmediate(true);
        table.setPageLength(size[dim1]);
        table.setRowHeaderMode(Table.ROW_HEADER_MODE_INDEX);
        table.setContainerDataSource(new MyContainer());

        for (int i = 0; i < 20; i++) {
            table.addContainerProperty(i, Double.class, (double) i, String
                    .valueOf(i + 1), null, null);
        }

        return table;
    }

    @SuppressWarnings("serial")
    protected AbstractLayout createDimensionSelectors(final int[] size) {
        VerticalLayout layout = new VerticalLayout();
        layout.setSpacing(true);
        layout.setMargin(true);

        // layout to add dimension selects
        HorizontalLayout dimLayout = new HorizontalLayout();
        dimLayout.addComponent(new Label("Selected dimensions:"));

        // create a list box for each of the dimensions
        selDim1 = new NativeSelect();
        selDim1.setNullSelectionAllowed(false);
        selDim1.setImmediate(true);

        dimLayout.addComponent(selDim1);

        selDim2 = new NativeSelect();
        selDim2.setNullSelectionAllowed(false);
        selDim2.setImmediate(true);

        dimLayout.addComponent(selDim2);

        layout.addComponent(dimLayout);
        layout.addComponent(createIndexSelects());

        // populate the dimension selects
        populateDimensionSelects();
        populateIndexes();
        redrawWithSelectedIndexes();

        selDim1.addListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(final ValueChangeEvent e) {
                Field.ValueChangeEvent a =
                    (com.vaadin.ui.Field.ValueChangeEvent) e;
                Integer index = (Integer) ((NativeSelect) a.getSource())
                        .getValue();
                dimension1 = index - 1;
                populateDimensionSelects();
                populateIndexes();
                redrawWithSelectedIndexes();
            }
        });

        selDim2.addListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(final ValueChangeEvent e) {
                Field.ValueChangeEvent a =
                    (com.vaadin.ui.Field.ValueChangeEvent) e;
                Integer index = (Integer) ((NativeSelect) a.getSource())
                        .getValue();

                dimension2 = index - 1;
                populateDimensionSelects();
                populateIndexes();
                redrawWithSelectedIndexes();
            }
        });

        return layout;
    }

    protected void redrawWithSelectedIndexes() {
        int[] indexes = new int[indexSelects.size()];
        int cntr = 0;
        for (NativeSelect select : indexSelects) {
            Integer value = (Integer) select.getValue();
            if (value == null) {
                return;
            }

            indexes[cntr++] = value - 1;
        }

        redrawTable(indexes);
    }

    protected void populateDimensionSelects() {
        final GenericMatrix<Object> matrix = getValue();
        int[] size = matrix.getSize();

        // dimension 1
        for (int i = 0; i < size.length; i++) {
            selDim1.addItem(i + 1);
        }
        selDim1.removeItem(dimension2 + 1);
        selDim1.setValue(dimension1 + 1);

        // dimension 2
        for (int i = 0; i < size.length; i++) {
            selDim2.addItem(i + 1);
        }
        selDim2.removeItem(dimension1 + 1);
        selDim2.setValue(dimension2 + 1);
    }

    protected void populateIndexes() {
        final GenericMatrix<Object> matrix = getValue();
        int[] size = matrix.getSize();

        final int[] indexes = new int[size.length - 2];

        int cntr = 0;
        for (int i = 0; i < size.length; i++) {
            if (i != dimension1 && i != dimension2) {
                indexes[cntr++] = i;
            }
        }

        cntr = 0;
        for (NativeSelect select : indexSelects) {

            try {
                select.removeAllItems();
            } catch (Throwable e) {
            }
            for (int j = 0; j < size[indexes[cntr]]; j++) {
                select.addItem(j + 1);
            }
            select.setValue(1);
            cntr++;
        }
    }

    @SuppressWarnings("serial")
    protected void redrawTable(final int[] indexes) {
        final DoubleMatrix matrix = getValue();
        int[] size = matrix.getSize();

        final MyContainer container = (MyContainer) table
                .getContainerDataSource();

        DoubleMatrix slice = (DoubleMatrix) matrix.slice(dimension1,
                dimension2, indexes);
        container.setMatrix(slice);

        // clear the container
        container.removeAllItems();

        // show only the needed columns
        List<Integer> visibleCols = new ArrayList<Integer>();
        for (int i = 0; i < size[dimension2]; i++) {
            visibleCols.add(i);
        }
        table.setVisibleColumns(visibleCols.toArray(new Integer[0]));

        // add the rows
        for (int i = 0; i < size[dimension1]; i++) {
            container.addItem(i);
        }

        table.containerItemSetChange(new ItemSetChangeEvent() {
            @Override
            public Container getContainer() {
                return container;
            }
        });
    }

    @SuppressWarnings("serial")
    protected HorizontalLayout createIndexSelects() {
        HorizontalLayout layout = new HorizontalLayout();
        layout.addComponent(new Label("Indexes of dimensions: "));

        final GenericMatrix<Object> matrix = getValue();
        int[] size = matrix.getSize();

        for (int i = 0; i < size.length; i++) {
            if (i != dimension1 && i != dimension2) {
                NativeSelect select = new NativeSelect();
                indexSelects.add(select);
                select.setImmediate(true);
                select.setValue(indexSelects.size());
                select.setNullSelectionAllowed(false);

                select.addListener(new Property.ValueChangeListener() {
                    @Override
                    public void valueChange(final ValueChangeEvent e) {
                        redrawWithSelectedIndexes();
                    }
                });

                layout.addComponent(select);
            }
        }

        return layout;
    }

    public static class MyContainer extends IndexedContainer {
        private static final long serialVersionUID = 1L;
        private DoubleMatrix matrix;

        @Override
        public Property getContainerProperty(final Object itemId,
            final Object propertyId) {
            int dim1 = (Integer) itemId;
            int dim2 = (Integer) propertyId;

            return new MyProperty(matrix.get(dim1, dim2));
        }

        public DoubleMatrix getMatrix() {
            return matrix;
        }

        public void setMatrix(final DoubleMatrix matrix) {
            this.matrix = matrix;
        }
    }

    public static class MyProperty implements Property {
        private static final long serialVersionUID = 1L;
        private Double value;

        public MyProperty(final Double value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }

        @Override
        public Class<?> getType() {
            return Double.class;
        }

        @Override
        public Object getValue() {
            return value;
        }

        @Override
        public boolean isReadOnly() {
            return true;
        }

        @Override
        public void setReadOnly(final boolean newStatus) {

        }

        @Override
        public void setValue(final Object newValue) {
            value = (Double) newValue;
        }

    }
}
