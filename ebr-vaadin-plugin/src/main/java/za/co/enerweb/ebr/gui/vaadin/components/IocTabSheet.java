package za.co.enerweb.ebr.gui.vaadin.components;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javassist.Modifier;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import com.vaadin.ui.Component;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Runo;

/**
 * Intended to make our screens more robust.
 * By inverting the control we can catch exceptions thrown
 * during the creation of each tab.
 * All public methods in subclasses with the @Tab
 * annotation and returns a component will be
 * added to this tabsheet.
 * If you don't know at compile time which tabs to show,
 * you can also implement createDynamicTab()
 * and getDynamicTabs() to handle the dynamic tabs.
 */
/*
 * Maybe make a Tab interface when a selected(boolean) method that
 * gets called on each tab every time the current tab changes
 */
@Slf4j
public class IocTabSheet extends TabSheet
implements TabSheet.SelectedTabChangeListener {
    private static final long serialVersionUID = 1L;
    private transient List<TabMetadata> tabs = new ArrayList<TabMetadata>();

    private transient boolean populated = false;

    @Getter
    private String selectedTabCaption = null;
    boolean fireEvents = true;

    private TabMetadata selectedTab;

    private List<ITabSelectedListener> iTabSelectedListeners =
        new ArrayList<IocTabSheet.ITabSelectedListener>();

    public void addITabSelectedListener(ITabSelectedListener tsl) {
        iTabSelectedListeners.add(tsl);
    }

    @Documented
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    @Inherited
    protected static @interface Tab {
        /**
         * Tab caption
         */
        String caption();
        /**
         * Freeform rank of tab. I.e the order in which the tabs must appear
         */
        byte rank();
        /**
         * If set to true this tab will be selected.
         * If you are stupid and mark multiple to be selected,
         * then the one with the highest rank will be selected, I think.
         */
        boolean selected() default false;
    }

    @EqualsAndHashCode(of = "caption")
    public static class TabMetadata
    implements Comparable<TabMetadata>, Serializable {
        private static final long serialVersionUID = 1L;
        @Getter
        private String caption;
        private int rank;
        private boolean selected = false;
        private Method method;
        private boolean dynamic = true;
        private boolean populated = false;
        private VerticalLayout layoutComponent;
        private Component tabComponent;

        public TabMetadata(String caption, int rank, boolean selected) {
            super();
            this.caption = caption;
            this.rank = rank;
            this.selected = selected;
        }

        private void reset() {
            selected = false;
            populated = false;
            layoutComponent = null;
        }

        @Override
        public int compareTo(TabMetadata o) {
            return rank - o.rank;
        }

        public String toString() {
            return caption;
        }
    }

    public static interface ITabSelectedListener {
        void tabSelected(TabMetadata tm);
    }

    public static interface ITabComponent {
        void tabSelected(boolean selected);
    }

    /**
     * @param rows
     * @param columns
     */
    public IocTabSheet() {
        setStyleName(Runo.TABSHEET_SMALL);
        setSizeFull();
        addListener((TabSheet.SelectedTabChangeListener)this);
    }

    public void attach() {
        super.attach();
        if (!populated || isRefreshOnAttach()) {
            refreshTabs();
        }
    }

    protected void refreshTabs() {
        String oldSelectedTab = getSelectedTabCaption();
        fireEvents = false;
        removeAllComponents(); // fires selection events
        tabs.clear();
        addAllTabs();
        populated = true;
        fireEvents = true;
        setSelectedTabByCaption(oldSelectedTab);
    }

    /**
     * Override this if you want us to re-populate the tabs
     * on every attachment. You may want this if youre tabs
     * are really dynamic, but then every attachment may not
     * be enough. This is untested..
     */
    protected boolean isRefreshOnAttach() {
        return false;
    }

    public Collection<TabMetadata> getDynamicTabs() {
        return Collections.emptyList();
    }

    public Component createDynamicTab(TabMetadata tab) {
        throw new UnsupportedOperationException("Unknown tab: "
            + tab.getCaption());
    }


    private void addAllTabs() {
        for (TabMetadata tm : getDynamicTabs()) {
            if (tabAlreadyPresent(tabs, tm.caption)) {
                log.warn("Dynamic Tab " + tm.caption +
                    " can't be added again, because a tab with this caption " +
                    "already exists.");
                continue;
            }
            tm.reset();
            tm.dynamic = true;
            tabs.add(tm);
        }

        Method[] methods = getClass().getDeclaredMethods();
        for (Method method : methods) {
            if (method.isAnnotationPresent(Tab.class)) {
                if (method.getParameterTypes().length != 0) {
                    log.warn("Expected method " + method.getName() +
                        " to take zero parameters.");
                    continue;
                }
                if (!Component.class.isAssignableFrom(method.getReturnType())) {
                    log.warn("Expected method " + method.getName() +
                        " to return a vaadin Component.");
                    continue;
                }
                if (!Modifier.isPublic(method.getModifiers())) {
                    log.warn("Method " + method.getName() +
                        " is not accessable, make it protected or public.");
                    continue;
                }
                Tab tabAnnotation = method.getAnnotation(Tab.class);
                if (tabAlreadyPresent(tabs, tabAnnotation.caption())) {
                    log.warn("Method " + method.getName() +
                        " is trying to add a tab that has already been added " +
                        " by another method.");
                    continue;
                }
                TabMetadata tm = new TabMetadata(
                    tabAnnotation.caption(), tabAnnotation.rank(),
                    tabAnnotation.selected());
                tm.dynamic = false;
                tm.method = method;
                tabs.add(tm);
            }
        }
        Collections.sort(tabs);
        for (TabMetadata tabMetadata : tabs) {
            tabMetadata.layoutComponent = new VerticalLayout();
            tabMetadata.layoutComponent.setSizeFull();
            addTab(tabMetadata.layoutComponent, tabMetadata.caption, null);
            if ((selectedTabCaption == null || tabMetadata.selected) ||
                (selectedTabCaption != null &&
                    selectedTabCaption.equals(tabMetadata.caption))) {
                selectedTabCaption = tabMetadata.caption;
                setSelectedTab(tabMetadata.layoutComponent);
            }
        }
        populateSelectedTab();
    }

    private boolean tabAlreadyPresent(List<TabMetadata> tabs, String caption) {
        for (TabMetadata tabMetadata : tabs) {
            if (tabMetadata.caption.equals(caption)) {
                return true;
            }
        }
        return false;
    }

    private void populateTab(TabMetadata tabMetadata) {
        if (tabMetadata.populated) {
            return;
        }
        tabMetadata.layoutComponent.removeAllComponents(); //for in case
        if (tabMetadata.dynamic) {
            try {
                Component tabComponent =createDynamicTab(tabMetadata);
                tabMetadata.tabComponent = tabComponent;
                tabMetadata.layoutComponent.addComponent(tabComponent);
            } catch (Exception e) {
                log.warn("Could not add dynamic tab "
                    + tabMetadata.caption, e);
            }
        } else {
            try {
                Component tabComponent = (Component) tabMetadata.method.invoke(
                    this, new Object[0]);
                tabMetadata.tabComponent = tabComponent;
                tabMetadata.layoutComponent.addComponent(tabComponent);
            } catch (Exception e) {
                log.warn("Could not add tab using method "
                    + tabMetadata.method.getName(), e);
            }
        }
        tabMetadata.populated = true;
    }

    @Override
    public void selectedTabChange(SelectedTabChangeEvent event) {
        // avoid populating the tab that gets added first.
        if (populated) {
            populateSelectedTab();
        }
        fireTabSelected();
    }

    private void populateSelectedTab() {
        Component selectedTabComponent = getSelectedTab();
        for (TabMetadata tabMetadata : tabs) {
            if (selectedTabComponent == tabMetadata.layoutComponent) {
                populateTab(tabMetadata);
                selectedTab = tabMetadata;
            }
        }
    }

    /**
     * This will override the selection specified by the tabs.
     * @param selectedTabCaption
     */
    public void setSelectedTabByCaption(String selectedTabCaption) {
        if (populated && selectedTabCaption != null) {
            for (TabMetadata tab : tabs) {
                if (tab.getCaption().equals(selectedTabCaption)) {
                    setSelectedTab(tab.layoutComponent);
                    selectedTab = tab;
                    fireTabSelected();
                }
            }
        }
    }

    protected void fireTabSelected() {
        if (!fireEvents || selectedTab == null) {
            return;
        }
        String previousSelectedTabCaption = selectedTabCaption;
        selectedTabCaption = selectedTab.getCaption();
        for (ITabSelectedListener l : iTabSelectedListeners) {
            l.tabSelected(selectedTab);
        }
        // ITabComponent
        for (TabMetadata aTab : tabs) {
            if (aTab.tabComponent instanceof ITabComponent) {
                boolean selected = aTab.equals(selectedTab);
                ITabComponent iTabComponent = (ITabComponent) aTab.tabComponent;
                if (selected) {
                    iTabComponent.tabSelected(selected);
                } else if (previousSelectedTabCaption != null
                    && previousSelectedTabCaption.equals(
                    aTab.getCaption())) {
                    iTabComponent.tabSelected(selected);
                }
            }
        }
    }

    public void unpopulateTabsByCaption(String... tabCaptions) {
        if (populated && tabCaptions != null && tabCaptions.length != 0) {
            for (TabMetadata tab : tabs) {
                if (Arrays.asList(tabCaptions).contains(tab.getCaption())) {
                    tab.populated = false;
                    tab.layoutComponent.removeAllComponents();
                }
            }
        }
    }

    /**
     * populate the transient stuff when vaadin deserializes this
     * @param in
     * @throws ClassNotFoundException
     * @throws IOException
     */
    private void readObject(ObjectInputStream ois)
    throws IOException, ClassNotFoundException {
        ois.defaultReadObject();
        tabs = new ArrayList<TabMetadata>();
        populated = false;
    }
}
