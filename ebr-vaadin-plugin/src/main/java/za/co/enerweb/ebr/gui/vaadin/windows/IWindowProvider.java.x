package za.co.enerweb.ebr.gui.vaadin.windows;

import com.vaadin.Application;
import com.vaadin.ui.Window;

/**
 * This window provider will be used at the following url:
 * <server url>/<name>
 */
public interface IWindowProvider {
    String getName();

    Window getWindow(Application application, Window window, String windowName);

}
