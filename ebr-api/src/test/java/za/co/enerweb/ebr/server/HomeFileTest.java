package za.co.enerweb.ebr.server;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import za.co.enerweb.ebr.server.HomeFile;
import za.co.enerweb.toolbox.io.TempDir;

public class HomeFileTest {

    private static final String DUMMY_FILE_NAME = "dummyfile.txt";
    private File tempDir = TempDir.createTempDir(
        HomeFileTest.class.getName());
    private HomeFile dummyDir, dummyFile;

    // private HomeFile dummyFile;

    @Before
    public void setup() throws IOException {
        dummyDir = new HomeFile(tempDir, "");
        dummyFile = new HomeFile(
            new File(tempDir, DUMMY_FILE_NAME), DUMMY_FILE_NAME);
        dummyFile.setContent("dummy content");
    }

    @Test
    public void testDelete() throws IOException {
        assertTrue(dummyFile.exists());
        dummyFile.delete();
        assertFalse(dummyFile.exists());

        assertTrue(dummyDir.exists());
        dummyDir.delete();
        assertFalse(dummyDir.exists());
    }

    @Test
    public void testGetChild() throws IOException {
        assertTrue(dummyDir.getChild(DUMMY_FILE_NAME).exists());
        assertFalse(dummyDir.getChild("non-existing.file").exists());
    }
}
