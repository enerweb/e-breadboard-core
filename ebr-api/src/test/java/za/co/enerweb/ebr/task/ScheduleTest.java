package za.co.enerweb.ebr.task;

import static org.junit.Assert.assertEquals;

import javax.ejb.ScheduleExpression;

import org.junit.Test;

public class ScheduleTest {

    @Test
    public void test() {
        Schedule s = new Schedule("1 2 3 4 5");
        ScheduleExpression se = s.getScheduleExpression();
        assertEquals("1", se.getMinute());
        assertEquals("2", se.getHour());
        assertEquals("3", se.getDayOfMonth());
        assertEquals("4", se.getMonth());
        assertEquals("5", se.getDayOfWeek());
    }

}
