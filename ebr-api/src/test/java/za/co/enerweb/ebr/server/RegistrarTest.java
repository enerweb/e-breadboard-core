package za.co.enerweb.ebr.server;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertThat;

import java.util.Set;

import junit.framework.AssertionFailedError;
import lombok.extern.slf4j.Slf4j;

import org.junit.Test;

@Slf4j
public class RegistrarTest {

    @SuppressWarnings("unchecked")
    @Test
    public void getClassesInPackage() {
        Set<Class<?>> classes =
            Registrar.getClassesInPackage(getClass().getPackage().getName());
        assertThat(classes, not(empty()));
        assertThat("don't want all the classes!",
            classes.size(), lessThan(25));
        // assertThat(classes, hasItems(rc));
        // assertThat(classes, contains(Arrays.asList(rc)));
        // assertThat(classes, containsInAnyOrder(RegistrarTest.class));
        assertContains(classes, getClass());
    }

    private void assertContains(Set<Class<?>> classes, Class<?> rc) {
        for (Class<?> class1 : classes) {
            if (rc.equals(class1)) {
                return;
            }
        }
        for (Class<?> class1 : classes) {
            log.debug(class1.getSimpleName());
        }
        throw new AssertionFailedError(rc + " not found :(");
    }

}
