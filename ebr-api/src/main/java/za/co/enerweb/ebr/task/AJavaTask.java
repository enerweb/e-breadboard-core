package za.co.enerweb.ebr.task;

import java.io.Serializable;

import za.co.enerweb.ebr.env.IEnvironment;
import za.co.enerweb.toolbox.reflection.PropertyUtils;


/**
 * pulls in variables from the environment and writes it back afterwards
 */
public abstract class AJavaTask extends AbstractTask {

    @Override
    public final void run() throws Exception {
        // set inputs
        IEnvironment inputEnv = getInputEnv();
        for (InputMetadata im : getTaskMetadata().getInputMetadata()) {
            String varName = im.getName();
            PropertyUtils.setProperty(this, varName, inputEnv
                .getVariableValue(varName));
        }

        runJavaTask();

        // get outputs
        IEnvironment outputEnv = getOutputEnv();
        for (OutputMetadata om : getTaskMetadata().getOutputMetadata()) {
            String varName = om.getName();
            outputEnv.setVariable(om,
                (Serializable) PropertyUtils.getProperty(this, varName));
        }
    }

    protected abstract void runJavaTask() throws Exception;

}
