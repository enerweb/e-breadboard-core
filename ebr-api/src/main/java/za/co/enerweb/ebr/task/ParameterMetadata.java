package za.co.enerweb.ebr.task;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import za.co.enerweb.ebr.env.EnvVarMetadata;

/**
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ParameterMetadata extends EnvVarMetadata {
    private static final long serialVersionUID = 1L;
    private ParameterEnvironment environment = ParameterEnvironment.JOB;

    public ParameterMetadata() {
    }

    public ParameterMetadata(final Class<? extends Serializable> type,
        final String name, final String description) {
        super(type, name, description);
    }
}
