package za.co.enerweb.ebr.server;

import java.net.URL;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

@Slf4j
public final class Registrar {
    private static Reflections reflections;

    public static synchronized Reflections getReflections() {
        if (reflections != null) {
            return reflections;
        }

        // We have a convention that plugins must be called
        // ebr-*-plugin.jar, so we can filter out urls that does not contain
        // Ebr and Plugin (we can make it optional/configurable)
        // XXX: I think I can contribute my way of filetering back to the
        // reflection project (maybe doing regex matches on urls)

        Set<URL> urlsForCurrentClasspath = getPluginFilteredClasspath();
        reflections =
            new Reflections(new ConfigurationBuilder().setUrls(
                urlsForCurrentClasspath).setScanners(
                new MethodAnnotationsScanner(),
                new TypeAnnotationsScanner(), new SubTypesScanner()));

        // the following is supposed to work if reflections metadata
        // is generated for each jar
        // reflections = Reflections.collect();
        return reflections;
    }

    public static synchronized Set<Class<?>> getClassesInPackage(
        String packageName) {
        // can probably cache these if needed..
        // thanks http://stackoverflow.com/a/9571146/381083
        Reflections reflections =
            new Reflections(
                new ConfigurationBuilder()
                    .addUrls(ClasspathHelper.forPackage(packageName))
                    .setScanners(new SubTypesScanner(
                        false /* don't exclude Object.class */))
                    .filterInputsBy(new FilterBuilder().include(
                        FilterBuilder.prefix(packageName)))
            );
        return reflections.getSubTypesOf(Object.class);
    }

    @Getter
    public static Set<URL> pluginFilteredClasspath =
        derivePluginFilteredClasspath();

    private static Set<URL> derivePluginFilteredClasspath() {
        // add container classpath
        Set<URL> urlsForCurrentClasspath =
            new HashSet<URL>(
                ClasspathHelper.forJavaClassPath());
        urlsForCurrentClasspath.addAll(ClasspathHelper
            .forClassLoader(ClasspathHelper.classLoaders()));

        // add the rest of the classpath eg. from ide
        final Set<URL> rest = ClasspathHelper.forPackage("/");
        if (rest != null) {
            urlsForCurrentClasspath.addAll(rest);
        }
        // .forgetUrlsForPackagePrefix(""));
        // filter out uninteresting jars and save 5 seconds on startup!
        for (Iterator<URL> iterator = urlsForCurrentClasspath.iterator(); iterator
            .hasNext();) {
            String url = (iterator.next()).toString();
            if (!url.matches(".*(([Ee]br[-_])|(classes)).*")) {
                iterator.remove();
                // log.debug("  ignoring " + url);
            } else {
                for (String excluded : SystemConfiguration
                    .getExcludedPlugins()) {
                    if (url.matches(".*(ebr-" + excluded + "-plugin).*")) {
                        log.debug("  excluding plugin with url " + url);
                        iterator.remove();
                    }
                }
                // log.debug("  keeping " + url);
            }
        }
        return urlsForCurrentClasspath;
    }

    private Registrar() {
    }

    public void reset() {
        reflections = null;
    }
}
