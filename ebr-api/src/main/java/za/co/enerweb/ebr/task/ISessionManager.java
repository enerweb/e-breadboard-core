package za.co.enerweb.ebr.task;

import java.util.Map;

import javax.ejb.Local;

import za.co.enerweb.ebr.session.SessionMetadata;

@Local
public interface ISessionManager {

    public void reloadSessions();

    public void deleteSession(final String id);

    public SessionMetadata getOrCreateSessionByName(final String name);

    /**
     * This will create the env if it does not exist
     * @param id
     * @param namespace
     * @return
     */
    public SessionMetadata ensureNamespace(final String id,
        final String namespace);

    public SessionMetadata getSession(final String id);

    public void saveSessions();

    public Map<String, SessionMetadata> getSessions();

    /**
     * @param id
     * @param name
     * @return can return a session with a new Id
     */
    public SessionMetadata getOrCreateSession(final String id,
        final String name);

    public SessionMetadata newSession(final String name);

    public SessionMetadata resetSession(final String id);

    public void saveSession(final String id);
}
