package za.co.enerweb.ebr.datatype.dataresource;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.NotImplementedException;

/**
 * stream can only be read once
 */
public class InputStreamDataSource extends AbstractDataSource {
    private static final long serialVersionUID = -5214801225870349066L;
    private InputStream stream;

    public InputStreamDataSource(String resourceName, InputStream stream) {
        super(resourceName);
        this.stream = stream;
        // throw new NotImplementedException();
    }

    /*
     * (non-Javadoc)
     * @see za.co.enerweb.ebr.datatype.dataresource.IDataResource
     * #asAbsolutePath()
     */
    @Override
    public String asAbsolutePath() throws IOException {
        throw new NotImplementedException();
    }

    public byte[] asBytes() throws IOException {
        return IOUtils.toByteArray(stream);
    }

    public InputStream asInputStream() throws FileNotFoundException {
        return stream;
    }
}
