package za.co.enerweb.ebr.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used for registering getting stuff to happen on a Ebr reload.
 * This happens when the Ebr Orhestration engine starts up too.
 * Can only be used on static methods that does not take parameters.
 */
/*
 * XXX: maybe add a parameter to say if this can happen in the background
 * then the orchestration engine can take care of running this in the
 * background at some time..
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface OnEbrReload {
    /**
     * Set this to true if you depend on this being executed synchronously
     */
    boolean synchronous() default false;

    /**
     * This gives you the option to be called before other @OnEbrReload methods.
     * Assign numbers between 0 and 1000.
     */
    int rank() default 10000;
}
