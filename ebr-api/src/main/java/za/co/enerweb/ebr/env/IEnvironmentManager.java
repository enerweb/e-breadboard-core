package za.co.enerweb.ebr.env;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import javax.ejb.Local;
import javax.ejb.Remote;

import za.co.enerweb.ebr.datatype.dataresource.IDataSource;

/**
 * Manages all the environments.
 */
@Local
@Remote
public interface IEnvironmentManager {

    /**
     * for unit testing
     */
    void clearCache();

    void copyVariable(final String sourceEnvId,
        final String sourceVarName,
        final String destEnvId, final String destVarName);

    void deleteEnvironment(final String envId);

    IEnvironment getEnvironment(final String envId);

    File getEnvironmentDir(final String envId);

    String getResourceAsAbsolutePath(final String envId,
        final String resourceId)
        throws IOException;

    byte[] getResourceAsBytes(final String envId,
        final String resourceId)
        throws IOException;

    InputStream getResourceAsStream(final String envId,
        final String resourceId)
        throws IOException;

    EnvResourceMetadata getResourceMetadata(final String envId,
        final String resourceId);

    EnvVarItem getVariable(final String envId, final String varName);

    EnvVarMetadata getVariableMetadata(final String envId,
        final String varName);

    <T extends Serializable> T getVariableValue(final String envId,
        final String varName);

    <T extends Serializable> T getVariableValueOrNull(final String envId,
        final String varName);

    boolean hasVariable(final String envId, final String varName);

    boolean isEnvironmentCached(final String envId);

    /**
     * Make a new environment and return the Environment instance.
     * @return
     */
    IEnvironment newEnvironment();

    /**
     * Make a new environment and return the id.
     * @return
     */
    String newEnvironmentId();

    /**
     * @deprecated use newIEnvironment
     */
    @Deprecated
    String newIEnvironmentd();

    /**
     * @param envId
     * @param dataSource
     * @return
     */
    String putResource(final String envId,
        final IDataSource dataSource);

    void saveEnvironment(final String envId);

    void setEnvironment(final IEnvironment env);

    void setVariable(final String envId, final EnvVarItem envItem);

    void setVariable(final String envId, final EnvVarMetadata metadata,
        final Serializable value);

    /**
     * @param envId
     * @param metadata
     */
    void setVariableMetadata(final String envId,
        final EnvVarMetadata metadata);

    void setVariableValue(final String envId, final String varName,
        final Serializable value);

    void setCapacity(int cacheCapacity);
}
