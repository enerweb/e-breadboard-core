package za.co.enerweb.ebr.datatype;

import java.io.Serializable;

import javax.ejb.Local;

import za.co.enerweb.ebr.exceptions.InvalidMetadataException;

@Local
// @Remote
public interface IDataTypeRegistryService {

    void clearAliasRegistry();
    void clearConverterRegistry();

    /**
     * @param typeAlias or typeName
     * @param namespace may be null
     * @return
     */
    ITypeConverter<Serializable> lookupConverter(
        final String namespace, final String typeAlias);

    /**
     * @param namespace may be null
     * @param alias
     * @return
     * @throws InvalidMetadataException if alias is not bound
     */
    String lookupType(final String namespace,
        final String alias);

    void refreshRegistry();

    /**
     * register alias globally
     * @param type
     * @param alias
     */
    void registerAlias(final Class<?> type,
        final String alias);

    /**
     * @param type
     * @param namespace may be null, then its registered globally
     * @param alias
     */
    void registerAlias(final Class<?> type,
        final String namespace,
        final String alias);

    /**
     * register alias globally
     * @param type
     * @param alias
     */
    void registerAlias(final String type,
        final String alias);

    /**
     * @param type
     * @param namespace may be null, then its registered globally
     * @param alias
     */
    void registerAlias(final String type, final String namespace,
        final String alias);

    /**
     * @param correctAlias
     * @param namespace may be null, then its registered globally
     * @param alias
     */
    void registerDeprecatedAlias(final String correctAlias,
        final String namespace,
        final String alias);

    void registerConverter(
        final ITypeConverter<Serializable> converter);

    /**
     * @param name
     * @param defaultConverter
     */
    void registerConverter(String type,
        final ITypeConverter<Serializable> converter);
}
