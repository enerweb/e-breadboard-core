package za.co.enerweb.ebr.exceptions;

/**
 *
 */
public class UnsupportedEnvVarRendererException extends Exception {

    private static final long serialVersionUID = 474732962083172010L;

    /**
     *
     */
    public UnsupportedEnvVarRendererException() {
    }

    /**
     * @param message
     */
    public UnsupportedEnvVarRendererException(final String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public UnsupportedEnvVarRendererException(final String message,
        final Throwable cause) {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public UnsupportedEnvVarRendererException(final Throwable cause) {
        super(cause);
    }

}
