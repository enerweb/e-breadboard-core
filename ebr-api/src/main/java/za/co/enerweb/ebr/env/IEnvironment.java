package za.co.enerweb.ebr.env;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

import za.co.enerweb.ebr.datatype.dataresource.IDataSource;

/**
 * Environment api
 */
public interface IEnvironment extends Serializable, Cloneable {

    String getId();

    void setId(String envId);

    boolean hasVariable(final String varName);

    Map<String, EnvVarItem> getVariables();

    EnvVarItem getVariableOrNull(final String varName);
    EnvVarItem getVariable(String varName);

    <T extends Serializable> T getVariableValue(String varName);

    EnvVarMetadata getVariableMetadata(String varName);

    void setVariable(EnvVarItem envItem);

    void setVariable(final EnvVarMetadata metadata,
        final Serializable value);

    void deleteVariable(String varName);

    void setVariableMetadata(EnvVarMetadata metadata);

    void setVariableValue(String varName, Serializable value);

    IDataSource getResource(final String resourceId);

    Collection<IDataSource> getResources();

    /**
     *
     * @param dataresource
     * @return new data resource id
     */
    String putResource(IDataSource dr);

    IEnvironment clone() throws CloneNotSupportedException;

}
