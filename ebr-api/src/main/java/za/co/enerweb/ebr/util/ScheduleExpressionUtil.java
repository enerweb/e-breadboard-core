package za.co.enerweb.ebr.util;

import javax.ejb.ScheduleExpression;

public class ScheduleExpressionUtil {

    public static ScheduleExpression parse(String schedule) {
        ScheduleExpression ret = null;
        if (schedule != null) {
            String[] split = schedule.split(" ");
            ret = new ScheduleExpression();
            if (split.length > 0) {
                ret.minute(split[0]);

                if (split.length > 1) {
                    ret.hour(split[1]);

                    if (split.length > 2) {
                        ret.dayOfMonth(split[2]);

                        if (split.length > 3) {
                            ret.month(split[3]);

                            if (split.length > 4) {
                                ret.dayOfWeek(split[4]);
                            }
                        }
                    }
                }
            }
        }
        return ret;
    }

}
