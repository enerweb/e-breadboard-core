package za.co.enerweb.ebr.task;

/**
 *
 */
public enum RunMode {
    INTERACTIVE, DEBUG, BATCH, DESIGN
}
