package za.co.enerweb.ebr.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used for registering getting stuff to happen on a Ebr startup.
 * Can only be used on static methods that does not take parameters.
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface OnEbrStart {
    /**
     * Set this to true if you depend on this being executed synchronously
     */
    boolean synchronous() default false;

    /**
     * This gives you the option to be called before other @OnEbrStart methods.
     * Assign numbers between 0 and 1000.
     */
    int rank() default 10000;
}
