package za.co.enerweb.ebr.task;

/**
 *
 */
public enum TaskExecutionStatus {
    UNINITIALISED(false, false), INITIALISED(true, false),
    BUSY(true, false),
    SUCCESS(false, true), FAIL(false, true), INTERRUPTED(false, true);

    private final boolean busy;
    private final boolean done;

    private TaskExecutionStatus(final boolean busy, final boolean done) {
        this.busy = busy;
        this.done = done;
    }

    public boolean isBusy() {
        return busy;
    }

    public boolean isBusyOrDone() {
        return busy || done;
    }

    public boolean isDone() {
        return done;
    }
}
