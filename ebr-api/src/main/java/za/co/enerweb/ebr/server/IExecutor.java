package za.co.enerweb.ebr.server;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;

public interface IExecutor {
    <T> Future<T> submit(Callable<T> task) throws Exception;
}
