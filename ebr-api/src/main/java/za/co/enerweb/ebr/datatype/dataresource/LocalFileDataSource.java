package za.co.enerweb.ebr.datatype.dataresource;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

/**
 * only works while file exists
 */
public class LocalFileDataSource extends AbstractDataSource {
    private static final long serialVersionUID = 7185373353145425345L;
    private File file;

    public LocalFileDataSource(final File file) {
        super(FilenameUtils.getName(file.getAbsolutePath()));
        this.file = file;
    }

    public LocalFileDataSource(final String envId, final String absolutePath) {
        super(FilenameUtils.getName(absolutePath));
        file = new File(absolutePath);
    }

    /*
     * (non-Javadoc)
     * @see
     * za.co.enerweb.ebr.datatype.dataresource.IDataSource#asAbsolutePath()
     */
    @Override
    public String asAbsolutePath() throws IOException {
        return file.getAbsolutePath();
    }

    public byte[] asBytes() throws IOException {
        return IOUtils.toByteArray(new FileInputStream(file));
    }

    public InputStream asInputStream() throws FileNotFoundException {
        return new FileInputStream(file);
    }

    @Override
    public String toString() {
        return "FileDataResource: "
            + (file != null ? file.getAbsoluteFile() : "null");
    }

}
