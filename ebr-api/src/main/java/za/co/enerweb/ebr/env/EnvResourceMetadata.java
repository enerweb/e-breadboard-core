package za.co.enerweb.ebr.env;

import java.io.Serializable;
import java.net.URLConnection;

import lombok.Data;

import org.apache.commons.io.FilenameUtils;

/**
 * Some metadata about an environment resource.
 */
@Data
public class EnvResourceMetadata implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * sensible name for the data this object represents,
     * used as a default name and for guessing the mime type
     */
    private final String fileName;
    private final String mimeType;

    public EnvResourceMetadata(final String fileName) {
        this.fileName = fileName;
        mimeType = URLConnection.guessContentTypeFromName(fileName);
    }

    public EnvResourceMetadata(final String fileName, final String mimeType) {
        this.fileName = fileName;
        this.mimeType = mimeType;
    }

    public String getFileBaseName() {
        return FilenameUtils.getBaseName(fileName);
    }

    public String getFileExtention() {
        return FilenameUtils.getExtension(fileName);
    }
}
