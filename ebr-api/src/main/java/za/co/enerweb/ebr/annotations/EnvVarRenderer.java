package za.co.enerweb.ebr.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import za.co.enerweb.ebr.renderer.RendererMode;

/**
 * Annotation used for registering Environment Variable Renderers.
 * For jsf renderers, it is assumed that .jsf resource with the same
 * name and package as the annotated class is available
 * which will do the rendering.
 * (Additional annotations would need to be made if this is not sufficient.)
 */
/*
 * XXX: this should be split up into seperate annotations
 * in order to do the ranking more elegantly
 * and support setting the classess (not hardcoding the types that only
 * break at runtime)
 * @SupportedType(rank=50, type="za.co.enerweb..")
 * @SupportedClass(rank=50, type=String.class)
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface EnvVarRenderer {

    /**
     * Unique id which a task can specify to be a renderer for its outputs
     * @return
     */
    String id();

    /**
     * Which technology this renderer is implemented with.
     * @return
     */
    String technology();

    /**
     * Classes for which this renderer is supported.
     * This array should be the same length as the
     * rankPerType array.
     */
    String[] supportedTypes();

    /**
     * If there are more than one renderer defined for a type,
     * you need to give us a clue how much you would like us to pick yours.
     * This number should be between 1 and 100 (highest value has highest
     * rank).
     * If you are unsure make it 50, except for over general renderers it
     * should
     * be 0 which will only be chosen if the renderer was specified
     * explicitly.
     * This array should be the same length as the
     * supportedTyeps array.
     * If an empty array is passed, the rank is assumed to be 50 for
     * everything.
     * @return
     */
    byte[] rankPerType() default { };

    /**
     * True if this renderer can be used as an editor too.
     * @return
     */
    RendererMode supports();
    // can't add a default, because this breaks older jdks:
    // http://bugs.sun.com/view_bug.do?bug_id=6512707
    // default RendererMode.EDIT
}
