package za.co.enerweb.ebr.server;

import static org.apache.commons.lang.StringUtils.isNotBlank;

import java.util.Collections;
import java.util.List;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

import za.co.enerweb.toolbox.config.ResourceBundleUtils;

/**
 * This is only for stuff that absolutely needs to be in a properties file, i.e.
 * the system can't start up without it.
 * XXX: Make this accessible from the EbrFactory.getSystemConfigurationService
 */
@Slf4j
public class SystemConfiguration {

    public static final String EBR_PROPERTIES = "ebr";
    public static final String EBR_BRANDING_PROPERTIES = "ebr-branding";
    private static final String VERSION = "version";
    private static final String BUILD_TIME = "build.time";
    private static final String UNKNOWN = "unknown";

    @Deprecated
    @Getter
    private static boolean registerHelloWorld = false;
    @Getter
    private static String brandName = "EBR";
    @Getter
    private static String version = UNKNOWN;

    @Getter
    private static String buildTime = UNKNOWN;

    @Getter
    private static String brandNameAndVersion = UNKNOWN;

    @Getter
    private static boolean useTestHome = false;
    @Getter
    private static boolean activateEasterEggs = false;

    // move so configuration service
    @Getter
    private static List<String> excludedPlugins = Collections.emptyList();

    static {
        try {
            ResourceBundleUtils rbu =
                new ResourceBundleUtils(EBR_BRANDING_PROPERTIES);
            brandName = rbu.getStringWithDefault("name", brandName);
            version = rbu.getStringWithDefault(VERSION, version);
            buildTime = rbu.getStringWithDefault(BUILD_TIME, buildTime);

            StringBuilder sb = new StringBuilder(brandName);
            if (!version.equals(UNKNOWN) && isNotBlank(version)
                && !version.contains("${")) {
                sb.append(" " + version.replace("-SNAPSHOT", ""));
            }
            if (!buildTime.equals(UNKNOWN) && isNotBlank(buildTime)
                && !buildTime.contains("${")) {
                sb.append(" (" + buildTime + ")");
            }
            brandNameAndVersion = sb.toString();

        } catch (Exception e) {
            log.debug("Could not read ebr_branding.properties",
                e);
        }

        try {
            ResourceBundleUtils rbu = new ResourceBundleUtils(EBR_PROPERTIES);
            registerHelloWorld = rbu.getBooleanWithDefault(
                "register_hello_world_task", registerHelloWorld);

            // String excludedPluginsStr = rbu.getStringWithDefault(
            // "exclude_plugins", "");
            // if (excludedPluginsStr.length() > 0) {
            // excludedPlugins = Arrays.asList(
            // scriptPaths.split("\\w,"));
            // }

            useTestHome = rbu.getBooleanWithDefault("use_test_home",
                useTestHome);
            useTestHome =
                Boolean.valueOf(System.getProperty("ebr.useTestHome", ""
                    + useTestHome));
            if (useTestHome) {
                log.info("*** Using a test e-breadboard home, to override it, "
                    + "add this vm argument: -Debr.useTestHome=false");
            }

            activateEasterEggs = rbu.getBooleanWithDefault(
                "activate_easter_eggs", activateEasterEggs);
            DateTime today = new DateTime();
            if (today.getMonthOfYear() == DateTimeConstants.APRIL
                && today.getDayOfMonth() == 1) {
                activateEasterEggs = true;
            }
        } catch (Exception e) {
        }

    }

}
