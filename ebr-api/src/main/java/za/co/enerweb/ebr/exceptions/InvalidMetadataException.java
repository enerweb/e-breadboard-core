package za.co.enerweb.ebr.exceptions;

/**
 *
 */
public class InvalidMetadataException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public InvalidMetadataException() {
        super();
    }

    public InvalidMetadataException(final String message) {
        super(message);
    }

    public InvalidMetadataException(final String message,
        final Throwable cause) {
        super(message, cause);
    }

    public InvalidMetadataException(final Throwable cause) {
        super(cause);
    }

}
