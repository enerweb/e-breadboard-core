package za.co.enerweb.ebr.task_layout;

import java.util.List;

import javax.ejb.Local;

/**
 * One stop shop to advertise and find layouts.
 * Register a default layout for your technology with a null id.
 */
@Local
public interface ITaskLayoutRegistry {
    public static final String RESOURCES_PATH_TASK_LAYOUTS = "task-layouts";

    @Deprecated
    public static final String RESOURCES_PATH_EXAMPLE_LAYOUTS =
        "example_layouts";

    void clearRegistry();

    ITaskLayout getLayout(
        final List<String> supportedTechs,
        final String layoutId);

    void registerLayout(final String id,
        final String technology,
        final TaskLayoutMetadata taskLayoutMetadata);
}
