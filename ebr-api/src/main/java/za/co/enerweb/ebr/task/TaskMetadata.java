package za.co.enerweb.ebr.task;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 */
@Data
@EqualsAndHashCode(doNotUseGetters = true)
@XmlRootElement
public final class TaskMetadata implements Serializable, Cloneable,
    Comparable<TaskMetadata> {
    private static final long serialVersionUID = 1L;

    private String id;
    private String title;
    private String description;
    private Class<?> taskImplementationClass;
    private boolean visible = true;
    private double secondsBeforeProgressShown = 2000;
    private boolean runOnInit = false;
    private Schedule schedule;

    private Map<String, InputMetadata> inputMetadata =
        new LinkedHashMap<String, InputMetadata>();
    private Map<String, OutputMetadata> outputMetadata =
        new LinkedHashMap<String, OutputMetadata>();

    private final Map<String, Serializable> auxProperties =
        new HashMap<String, Serializable>();

    private String layout = null;

    // XXX: maybe be we can make common items eg. fieldName and renderer
    // more accessible in future
    private final List<Map<String, Serializable>> layoutItems =
        new ArrayList<Map<String, Serializable>>();

    // XXX: throws (possible Error ids, should have error registry)
    // XXX: default run feedback mode
    // XXX: maybe register input editor events, to allow dynamic mutation

    public TaskMetadata() {

    }

    public TaskMetadata(final String id,
        final Class<?> taskImplementationClass) {
        super();
        this.id = id;
        title = taskImplementationClass.getSimpleName();
        this.taskImplementationClass = taskImplementationClass;
    }

    public TaskMetadata(final String id, final String title,
        final String description, final Class<?> taskImplementationClass) {
        super();
        this.id = id;
        this.title = title;
        this.description = description;
        this.taskImplementationClass = taskImplementationClass;
    }

    @XmlElement
    public Collection<InputMetadata> getInputMetadata() {
        return Collections.unmodifiableCollection(inputMetadata.values());
    }

    @XmlTransient
    public Iterator<InputMetadata> getInputMetadataIterator() {
        return getInputMetadata().iterator();
    }

    @XmlTransient
    public Iterator<OutputMetadata> getOutputMetadataIterator() {
        return getOutputMetadata().iterator();
    }

    @XmlElement
    public Collection<OutputMetadata> getOutputMetadata() {
        return Collections.unmodifiableCollection(outputMetadata.values());
    }

    /**
     * @param name
     * @return null if not found
     */
    public OutputMetadata getOutputMetadata(final String name) {
        return outputMetadata.get(name);
    }

    /**
     * @param name
     * @return null if not found
     */
    public InputMetadata getInputMetadata(final String name) {
        return inputMetadata.get(name);
    }

    public OutputMetadata registerOutput(
        final Class<? extends Serializable> type, final String name,
        final String paramDescription) {
        return registerOutput(new OutputMetadata(type, name, paramDescription));
    }

    public OutputMetadata registerOutput(final OutputMetadata outputMetadata) {
        outputMetadata.validate();
        this.outputMetadata.put(outputMetadata.getName(), outputMetadata);
        return outputMetadata;
    }

    public InputMetadata registerInput(
        final Class<? extends Serializable> type, final String name,
        final String paramDescription, final Serializable defaultValue,
        final boolean optional) {
        return registerInput(new InputMetadata(type, name, paramDescription,
            defaultValue, optional));
    }

    public InputMetadata registerInput(final InputMetadata inputMetaData) {
        inputMetaData.validate();
        inputMetadata.put(inputMetaData.getName(), inputMetaData);
        return inputMetaData;
    }

    public void setAuxPropery(final String name, final Serializable property) {
        auxProperties.put(name, property);
    }

    public Serializable getAuxPropery(final String name) {
        return auxProperties.get(name);
    }

    @Override
    public TaskMetadata clone() throws CloneNotSupportedException {
        TaskMetadata ret = (TaskMetadata) super.clone();
        ret.setId(id);
        ret.setTitle(title);
        ret.setDescription(description);
        ret.setTaskImplementationClass(taskImplementationClass);
        ret.inputMetadata =
            new LinkedHashMap<String, InputMetadata>(inputMetadata);
        ret.outputMetadata =
            new LinkedHashMap<String, OutputMetadata>(outputMetadata);
        return ret;
    }

    public int compareTo(final TaskMetadata o) {
        return title.compareTo(o.title);
    }

    /**
     * min hour dayofmonth month dayofweek
     * eg: 0 5 * * 1
     * @param scheduleStr
     */
    public void setSchedule(String scheduleStr) {
        schedule = new Schedule(scheduleStr);
    }

    // public ScheduleExpression getScheduleExpression() {
    // return schedule.getScheduleExpression();
    // }

    public String getTitleAndId() {
        return title + " (" + id + ")";
    }
}
