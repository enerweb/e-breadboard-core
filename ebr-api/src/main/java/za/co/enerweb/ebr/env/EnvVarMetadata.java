package za.co.enerweb.ebr.env;

import static za.co.enerweb.toolbox.string.StringUtils.variableName2Caption;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import za.co.enerweb.ebr.session.SessionMetadata;
import za.co.enerweb.toolbox.validation.ValidationException;

/**
 * Describes things that are stored in the environment.
 */
@Data
@EqualsAndHashCode(of = "name", callSuper = true)
public class EnvVarMetadata extends AbstractTypeDescriptor implements
    Cloneable {
    private static final long serialVersionUID = -6058908239652175861L;
    @NonNull
    private String name = "unnamed variable";
    private String caption = null;
    private String description;
    @NonNull
    private String namespace = SessionMetadata.DEFAULT_NAMESPACE;

    private String renderer = null;

    private final Map<String, Serializable> auxProperties =
        new HashMap<String, Serializable>();

    public EnvVarMetadata() {
        super();
    }

    public EnvVarMetadata(final String type, final String name,
        final String description) {
        super(type);
        this.name = name;
        this.description = description;
    }

    public EnvVarMetadata(final Class<? extends Serializable> type,
        final String name, final String description) {
        super(type);
        this.name = name;
        this.description = description;
    }

    /**
     * copy constructor
     * @param cloneMe
     */
    public EnvVarMetadata(final EnvVarMetadata cloneMe) {
        this(cloneMe.getTypeName(), cloneMe.name, cloneMe.description);
        auxProperties.putAll(cloneMe.auxProperties);
        setNamespace(cloneMe.namespace);
        setRenderer(cloneMe.renderer);
    }

    public String getCaption() {
        if (caption != null) {
            return caption;
        }
        return variableName2Caption(name);
    }

    public String getQualifiedName() {
        if (namespace.equals(SessionMetadata.DEFAULT_NAMESPACE)) {
            return name;
        }
        return namespace + ":" + name;
    }

    public String toString() {
        return getQualifiedName();
    }

    public Serializable getAuxProperty(final String propertyName) {
        return auxProperties.get(propertyName);
    }

    /**
     * @deprecated use getAuxProperty
     */
    @Deprecated
    public Serializable getAuxPropery(final String propertyName) {
        return getAuxProperty(propertyName);
    }

    /**
     * Validate once and for all what exactly we consider to be valid
     * variable name:
     * abc
     * abcDef
     * abc_def
     * abc.def
     */
    public void validate() {
        if (!name.matches("[\\w\\.]+")) {
            throw new ValidationException("Field name may only contain "
                + "characters, numbers, underscores and dots.");
        }
    }

    public boolean deepEquals(final EnvVarMetadata avm2) {
        return equals(avm2) && description.equals(avm2.description);
    }

    /**
     * @deprecated rather use the copy constructor directly
     */
    @Deprecated
    @Override
    public EnvVarMetadata clone() {
        return new EnvVarMetadata(this);
    }

}
