package za.co.enerweb.ebr.task_layout;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;
import za.co.enerweb.ebr.env.AbstractTypeDescriptor;

/**
 * Houses the class which implements the layout, and
 * keeps some auxiliary properties for you.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TaskLayoutMetadata extends AbstractTypeDescriptor {
    private static final long serialVersionUID = 1L;
    private String deprecatedById = null;
    private final Map<String, Serializable> auxProperties =
        new HashMap<String, Serializable>();
}
