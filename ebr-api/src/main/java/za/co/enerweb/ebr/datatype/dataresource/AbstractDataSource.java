package za.co.enerweb.ebr.datatype.dataresource;

import lombok.Data;
import za.co.enerweb.ebr.env.EnvResourceMetadata;
import za.co.enerweb.ebr.server.IdGenerator;

@Data
public abstract class AbstractDataSource implements IDataSource {
    private static final long serialVersionUID = -1115264251095560205L;

    private String resourceId = IdGenerator.getLocallyUniqueId();

    private EnvResourceMetadata resourceMetadata;

    public AbstractDataSource(EnvResourceMetadata resourceMetadata) {
        this.resourceMetadata = resourceMetadata;
    }

    public AbstractDataSource(String fileName) {
        this.resourceMetadata = new EnvResourceMetadata(fileName);
    }

    public AbstractDataSource(String fileName,
        String mimeType) {
        this.resourceMetadata = new EnvResourceMetadata(fileName, mimeType);
    }

    @Override
    public String toString() {
        return resourceId + "/" + resourceMetadata.toString();
    }
}
