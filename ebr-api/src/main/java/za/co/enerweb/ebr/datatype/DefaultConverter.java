package za.co.enerweb.ebr.datatype;

import java.io.Serializable;

import za.co.enerweb.ebr.datatype.ITypeConverter;

/**
 * does no conversion
 */
public class DefaultConverter implements ITypeConverter<Serializable> {

    private static final long serialVersionUID = 1L;

    /*
     * (non-Javadoc)
     * @see za.co.enerweb.ebr.api.datatype.ITypeConverter
     * #fromSimpleStructure(java.lang.Object)
     */
    @Override
    public Serializable fromSimpleStructure(String envId,
        String variableName, Object simpleStructure) {
        return (Serializable) simpleStructure;
    }

    /*
     * (non-Javadoc)
     * @see za.co.enerweb.ebr.api.datatype.ITypeConverter#getType()
     */
    @Override
    public String getType() {
        return Serializable.class.getName();
    }

    /*
     * (non-Javadoc)
     * @see za.co.enerweb.ebr.api.datatype.ITypeConverter
     * #toSimpleStructure(java.lang.Object)
     */
    @Override
    public Object toSimpleStructure(String envId, String variableName,
        Serializable specificObject) {
        return specificObject;
    }
}
