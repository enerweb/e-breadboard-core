package za.co.enerweb.ebr.exceptions;

/**
 * Task could not be executed.
 */
public class TaskExecutionException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public TaskExecutionException() {
    }

    public TaskExecutionException(final String message) {
        super(message);
    }

    public TaskExecutionException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public TaskExecutionException(final Throwable cause) {
        super(cause);
    }

}
