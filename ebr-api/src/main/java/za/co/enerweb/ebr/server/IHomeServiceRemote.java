package za.co.enerweb.ebr.server;

import java.io.IOException;

import javax.ejb.Remote;

/*
 * This is intended for use by remote clients.
 * I think this should be turned into a webservice.
 */
@Remote
public interface IHomeServiceRemote {

    void writeFile(HomeFileSpec hfs,
        String fileContent) throws IOException;

    /**
     * @param hfs
     * @return returns the file in the trash.
     * @throws IOException
     */
    HomeFile deleteFile(HomeFileSpec hfs) throws IOException;
}
