package za.co.enerweb.ebr.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FileUtils;

import za.co.enerweb.ebr.exceptions.ServiceException;
import za.co.enerweb.toolbox.collections.ArrayUtils;

/**
 * Object containing all the info you could possibly want about a file
 * managed by the home service.
 * For local use only! 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class HomeFile implements Serializable, Comparable<HomeFile> {
    private static final long serialVersionUID = 3L;

    private transient File file;

    /**
     * Path relative to the home.
     */
    private String relPath;

    /**
     * Get the homeFiles in this dir. (If this is not a directory just return a
     * empty list)
     */
    public List<HomeFile> getChildren() {
        List<HomeFile> ret = new ArrayList<HomeFile>();
        if (isDirectory()) {
            File[] children = getDir().listFiles();
            for (File child : children) {
                ret.add(new HomeFile(child,
                    getRelPath() + "/" + child.getName()));
            }
        }
        return ret;
    }

    /**
     * Get the homeFiles in this dir. (If this is not a directory just return a
     * an invalid homefile)
     */
    public HomeFile getChild(String name) {
        return new HomeFile(new File(getFile(), name),
            getRelPath() + "/" + name);
    }

    public HomeFile getParentHomeFile() {
        String[] relPathElements = getRelPathElements();
        String[] parentRelPathElements = ArrayUtils.subArray(String.class,
            relPathElements, 0, relPathElements.length - 1);
        return new HomeFile(getFile().getParentFile(),
            HomeFileSpec.join(parentRelPathElements));
    }

    /**
     * Splits the relative path up into elements. Assumes that this happens
     * locally otherwise we would need to normalise it.
     * @return
     */
    public String[] getRelPathElements() {
        return HomeFileSpec.split(relPath);
    }

    /**
     * create parent directories if it doesn't exist yet
     */
    public void makeParentDirs() {
        if (valid() && !file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
    }

    /**
     * Validates if this is actually a directory.
     * @return the File object that represents a directory.
     * @throws ServiceException if this is not an existing directory.
     */
    public File getDir() {
        if (!isDirectory()) {
            throw new ServiceException(this + " is not a directory.");
        }
        return file;
    }

    /**
     * If the file does not exist, create the directory,
     * if it does exist, validate that it is a directory
     * @return
     * @throws IOException if it exists and is not a directory
     */
    public File getOrCreateDir() {
        if (exists()) {
            return getDir();
        }
        getFile().mkdirs();
        if (!file.isDirectory()) {
            log.warn("Could not create dir, maybe delete some peers: "
                + file);
        }
        return file;
    }

    /**
     * Checks that the internal file object is not null.
     * @throws ServiceException if invalid
     */
    public File getFile() {
        if (!valid()) {
            // avoid NPEs, maybe this is a bit overkill
            throw new ServiceException("File not found: " + this);
        }
        return file;
    }

    /**
     * If the file does not exist yes, touch it (i.e. create an empty file).
     * @return
     * @throws IOException if it exists and is not a normal file
     */
    public File getOrCreateFile() {
        if (exists()) {
            if (!isFile()) {
                throw new ServiceException(this + " is not a file.");
            }
        } else {
            try {
                makeParentDirs();
                getFile().createNewFile();
            } catch (IOException e) {
                throw new ServiceException("Could not create file: " + this,
                    e);
            }
        }
        return file;
    }

    // vvv Some delegators that may be useful
    public boolean exists() {
        return valid() && file.exists();
    }

    public boolean canRead() {
        return valid() && file.canRead();
    }

    /**
     * Deletes file directly recursively (never throwing an exception).
     * @return true if it was actually deleted.
     */
    public boolean delete() {
        if (exists()) {
            return FileUtils.deleteQuietly(file);
        }
        return false;
    }

    public String getAbsolutePath() {
        return getFile().getAbsolutePath();
    }

    public String getName() {
        return getFile().getName();
    }

    public String getParent() {
        return getFile().getParent();
    }

    public File getParentFile() {
        return getFile().getParentFile();
    }

    /**
     * @return true if and only if the file denoted by this abstract pathname
     *         exists and is a directory
     */
    public boolean isDirectory() {
        return valid() && file.isDirectory();
    }

    /**
     * @return True if and only if the file exists and is a normal file
     */
    public boolean isFile() {
        return valid() && file.isFile();
    }

    public boolean equals(Object o) {
        if (o instanceof HomeFile) {
            HomeFile hf = (HomeFile) o;
            return this.getFile().equals(hf.getFile());
        }
        if (o instanceof File) {
            File f = (File) o;
            return this.getFile().equals(f);
        }
        return false;
    }

    @Override
    public int hashCode() {
        // using the file's hashcode gave use StreamCorruptedException
        // when serializing
        return relPath.hashCode();
    }

    // ^^^

    // vvv Some util methods that may be useful
    public String getContent() throws IOException {
        return FileUtils.readFileToString(getFile());
    }

    public void setContent(String data) throws IOException {
        FileUtils.writeStringToFile(getFile(), data);
    }

    public byte[] getContentAsBytes() throws IOException {
        return FileUtils.readFileToByteArray(getFile());
    }

    /**
     * @deprecated use getContent
     */
    public String readAsString() throws IOException {
        return getContent();
    }

    public InputStream getInputStream() throws IOException {
        return new FileInputStream(getFile());
    }

    public Reader getReader() throws FileNotFoundException {
        return new FileReader(getFile());
    }

    /**
     * @deprecated use getInputStream
     */
    public InputStream readAsStream() throws IOException {
        return getInputStream();
    }

    public OutputStream getOutputStream() throws IOException {
        return new FileOutputStream(getFile());
    }

    public Writer getWriter() throws IOException {
        return new FileWriter(getFile());
    }

    // ^^^

    /**
     * returns true if we actually have a file object.
     */
    public boolean valid() {
        return file != null;
    }

    public String toString() {
        if (valid()) {
            return file.getAbsolutePath();
        }
        return relPath;
    }


    /**
     * Custom deserialization is needed.
     * @throws ClassNotFoundException
     * @throws IOException
     */
    private void readObject(ObjectInputStream in) throws IOException,
        ClassNotFoundException {
        in.defaultReadObject();
        file = new File((String) in.readObject());
    }

    /**
     * Custom serialization is needed.
     */
    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
        out.writeObject(file.getAbsolutePath());
    }

    @Override
    public int compareTo(HomeFile o) {
        return relPath.compareTo(o.relPath);
    }

}
