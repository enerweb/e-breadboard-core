package za.co.enerweb.ebr.datatype.dataresource;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang.NotImplementedException;

/**
 * uses lots of memory
 */
public class ByteArrayDataSource extends AbstractDataSource {
    private static final long serialVersionUID = 7185373353145425345L;
    private byte[] bytes;
    private transient File file = null;

    public ByteArrayDataSource(final String fileName, final byte[] bytes) {
        super(fileName);
        this.bytes = bytes;
        throw new NotImplementedException();
    }

    /*
     * (non-Javadoc)
     * @see
     * za.co.enerweb.ebr.datatype.dataresource.IDataSource#asAbsolutePath()
     */
    @Override
    public String asAbsolutePath() throws IOException {
        if (file == null) {
            // TODO: write file out to disk because its needed that way
            // normally for some external integration that needs a file path

        }
        // return file.getAbsolutePath();
        throw new NotImplementedException();
    }

    public byte[] asBytes() throws IOException {
        return bytes;
    }

    public InputStream asInputStream() throws FileNotFoundException {
        return new ByteArrayInputStream(bytes);
    }

    @Override
    public String toString() {
        return "ByteArrayDataResource: " + bytes.length + " bytes.";
    }
}
