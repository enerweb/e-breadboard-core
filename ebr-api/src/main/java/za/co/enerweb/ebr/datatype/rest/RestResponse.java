package za.co.enerweb.ebr.datatype.rest;

import java.io.Serializable;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class RestResponse implements Serializable {

    private transient Response response;

    public static RestResponse build(ResponseBuilder builder) {
        return new RestResponse(builder.build());
    }
}
