package za.co.enerweb.ebr.exceptions;

/**
 *
 */
public class UnknownTaskExecutionException extends Exception {

    private static final long serialVersionUID = 7152136968153395733L;

    /**
     *
     */
    public UnknownTaskExecutionException() {
    }

    /**
     * @param message
     */
    public UnknownTaskExecutionException(final String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public UnknownTaskExecutionException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public UnknownTaskExecutionException(final Throwable cause) {
        super(cause);
    }

}
