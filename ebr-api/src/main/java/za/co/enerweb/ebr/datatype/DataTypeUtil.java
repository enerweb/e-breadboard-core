package za.co.enerweb.ebr.datatype;

import za.co.enerweb.toolbox.reflection.ClassNameUtils;

/**
 * Central place for class manipulations
 */
public final class DataTypeUtil {
    private DataTypeUtil() {
    }

    public static Class<?> getTypeClass(final String typeName)
        throws ClassNotFoundException {
        return ClassNameUtils.getClass(typeName);
    }
}
