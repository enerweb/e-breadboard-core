package za.co.enerweb.ebr.renderer;

/**
 * Different renderers support different modes.
 */
public enum RendererMode {
    EDIT, VIEW, EDIT_AND_VIEW;

    public boolean isSupportsEditing() {
        return equals(EDIT) || equals(EDIT_AND_VIEW);
    }

    public boolean isSupportsViewing() {
        return equals(VIEW) || equals(EDIT_AND_VIEW);
    }
}
