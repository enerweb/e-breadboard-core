package za.co.enerweb.ebr.exceptions;

import javax.ejb.ApplicationException;

@ApplicationException
public class CouldNotFindTaskException extends ServiceException {

    private static final long serialVersionUID = 7002873675695443557L;

    /**
     *
     */
    public CouldNotFindTaskException() {
    }

    /**
     * @param message
     */
    public CouldNotFindTaskException(final String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public CouldNotFindTaskException(final String message,
        final Throwable cause) {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public CouldNotFindTaskException(final Throwable cause) {
        super(cause);
    }

}
