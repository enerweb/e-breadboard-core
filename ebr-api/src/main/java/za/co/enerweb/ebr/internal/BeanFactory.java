package za.co.enerweb.ebr.internal;

import javax.naming.Context;
import javax.naming.NamingException;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;


/*
 * At the moment we only give out Local proxies, because we have no
 * remote use cases at the moment.
 * Should we need it in the future, we would need a configuration file
 * that specifies which local proxies to substitute for remote proxies,
 * i.e. if a client asks for a Remote interface he will get it unless
 * otherwise configured.
 * In EJB3.1 the names are standardised
 * http://jcp.org/aboutJava/communityprocess/final/jsr318/index.html
 * to:
 * java:global[/<app-name (ear name)>]/<module-name>/<bean-name>[!<fully-quali-
 * fied-interface-name>]
 * java:app/<module-name>/<bean-name>[!<fully-qualified-interface-name>]
 * java:module/<bean-name>[!<fully-qualified-interface-name>]
 *
 * I think it will be tricky to configure the module-name once we need that.
 * so I'm taking that into the interface so long.
 */
@Slf4j
public abstract class BeanFactory {

    /**
     * This needs to be set before looking up anything
     * @param ejbContext
     */
    @Setter
    @Getter
    private Context ejbContext;

    // @SneakyThrows
    // public InitialContext getEjbContext() {
    // return new InitialContext();
    // }

    /**
     *
     * @param moduleName This is not used at the moment, but may be used in the
     * future if we move over to portable jndi names.
     * If there is some unresolvable problems with this, we may want to alllow
     * overriding this from a optional configuration file.
     * @param returnClass
     * @return
     */
    protected abstract String lookupEjbJndiName(String moduleName,
        String beanName, Class<?> returnClass);

    @SuppressWarnings("unchecked")
    public <T> T lookupEjb(final String moduleName, String beanName,
        final Class<T> returnClass) {
        String name = lookupEjbJndiName(moduleName, beanName, returnClass);
        // log.debug("bean name = " + name);
        try {
            T ret = (T) getEjbContext().lookup(name);
            if (ret == null) {
                throw new RuntimeException(
                    "Could not lookup ejb, this is only allowed when called "
                        + "from within a managed bean");
            }
            return ret;
        } catch (NamingException e) {
            throw new RuntimeException(
                "Could not look up " + name, e);
        }
    }
}
