package za.co.enerweb.ebr.server;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true, fluent = true)
public class LogFileSpec {
    public static enum LogLayout {
        /**
         * prints content verbatim, doensn't even add newlines
         */
        PLAIN,
        /**
         * prints content verbatim, but adds a newline after each message
         */
        LINE,
        /**
         * full log message, complete with dates and sources
         */
        DETAIL;
    }
    /*
     * this is here to decouple us from log4j
     */
    public static enum LogLevel {
//        TRACE,
        DEBUG,
        INFO,
        WARN,
        ERROR,
//        FATAL
        ;
    }


    /**
     * Name will be used as part of the filename eg. {name}.log
     */
    private String name;

    /**
     * defaults to 1MB
     */
    private long maximumFileSize = 1000 * 1000;

    private int numberOfBackupFiles = 0;

    private LogLayout layout = LogLayout.DETAIL;
    private LogLevel level = LogLevel.INFO;

    /**
     * Names of the loggers this should attach to.
     * If it is empty or null or contains a null element,
     * it will be attached to the root logger.
     */
    private List<String> loggerNames = new ArrayList<String>(1);

    public LogFileSpec logger(String logger) {
        this.loggerNames.add(logger);
        return this;
    }

    public LogFileSpec logger(Class<?> klass) {
        return this.logger(klass.getName());
    }
}
