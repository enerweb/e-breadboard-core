package za.co.enerweb.ebr.server;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.ini4j.Profile.Section;

/*
 * a little protection against api bleed
 */
public class ConfSection implements Map<String, String> {

    private Section section;

    public ConfSection(Section section) {
        this.section = section;
    }

    /**
     * Will never return null.
     * The returned list is not guaranteed to be backed by the real
     * configuration. Use setAll to update multivalued items.
     */
    public List<String> getAll(Object key) {
        List<String> ret = section.getAll(key);
        if (ret == null) {
            ret = Collections.emptyList();
        }
        return ret;
    }


    // vvv delegation :( where oh where have my lombok.@Diuigate gone :( vvv//

    public <T> T getAll(Object key, Class<T> clazz) {
        return section.getAll(key, clazz);
    }

    public String getComment(Object key) {
        return section.getComment(key);
    }

    public String putComment(String key, String comment) {
        return section.putComment(key, comment);
    }

    public void add(String key, String value) {
        section.add(key, value);
    }

    public void add(String key, Object value) {
        section.add(key, value);
    }

    public void add(String key, String value, int index) {
        section.add(key, value, index);
    }

    public String removeComment(Object key) {
        return section.removeComment(key);
    }

    public void add(String key, Object value, int index) {
        section.add(key, value, index);
    }

    public String get(Object key, int index) {
        return section.get(key, index);
    }

    public <T> T as(Class<T> clazz) {
        return section.as(clazz);
    }

    public int length(Object key) {
        return section.length(key);
    }

    public <T> T as(Class<T> clazz, String keyPrefix) {
        return section.as(clazz, keyPrefix);
    }

    public String put(String key, String value, int index) {
        return section.put(key, value, index);
    }

    public List<String> putAll(String key, List<String> values) {
        return section.putAll(key, values);
    }

    public String fetch(Object key) {
        return section.fetch(key);
    }

    public String fetch(Object key, String defaultValue) {
        return section.fetch(key, defaultValue);
    }

    public String remove(Object key, int index) {
        return section.remove(key, index);
    }

    public String fetch(Object key, int index) {
        return section.fetch(key, index);
    }

    public <T> T fetch(Object key, Class<T> clazz) {
        return section.fetch(key, clazz);
    }

    public <T> T fetch(Object key, Class<T> clazz, T defaultValue) {
        return section.fetch(key, clazz, defaultValue);
    }

    public <T> T fetch(Object key, int index, Class<T> clazz) {
        return section.fetch(key, index, clazz);
    }

    public <T> T fetchAll(Object key, Class<T> clazz) {
        return section.fetchAll(key, clazz);
    }

    public void from(Object bean) {
        section.from(bean);
    }

    public void from(Object bean, String keyPrefix) {
        section.from(bean, keyPrefix);
    }

    public String get(Object key, String defaultValue) {
        return section.get(key, defaultValue);
    }

    public <T> T get(Object key, Class<T> clazz) {
        return section.get(key, clazz);
    }

    public <T> T get(Object key, Class<T> clazz, T defaultValue) {
        return section.get(key, clazz, defaultValue);
    }

    public Section getChild(String key) {
        return section.getChild(key);
    }

    public <T> T get(Object key, int index, Class<T> clazz) {
        return section.get(key, index, clazz);
    }

    public String getName() {
        return section.getName();
    }

    public Section getParent() {
        return section.getParent();
    }

    public String put(String key, Object value) {
        return section.put(key, value);
    }

    public String getSimpleName() {
        return section.getSimpleName();
    }

    public String put(String key, Object value, int index) {
        return section.put(key, value, index);
    }

    public Section addChild(String key) {
        return section.addChild(key);
    }

    public void putAll(String key, Object value) {
        section.putAll(key, value);
    }

    public String[] childrenNames() {
        return section.childrenNames();
    }

    public Section lookup(String... path) {
        return section.lookup(path);
    }

    public void to(Object bean) {
        section.to(bean);
    }

    public void to(Object bean, String keyPrefix) {
        section.to(bean, keyPrefix);
    }

    public void removeChild(String key) {
        section.removeChild(key);
    }

    public int size() {
        return section.size();
    }

    public boolean isEmpty() {
        return section.isEmpty();
    }

    public boolean containsKey(Object key) {
        return section.containsKey(key);
    }

    public boolean containsValue(Object value) {
        return section.containsValue(value);
    }

    public String get(Object key) {
        return section.get(key);
    }

    public String put(String key, String value) {
        return section.put(key, value);
    }

    public String remove(Object key) {
        return section.remove(key);
    }

    public void putAll(Map<? extends String, ? extends String> m) {
        section.putAll(m);
    }

    public void clear() {
        section.clear();
    }

    public Set<String> keySet() {
        return section.keySet();
    }

    public Collection<String> values() {
        return section.values();
    }

    public Set<java.util.Map.Entry<String, String>> entrySet() {
        return section.entrySet();
    }

    public boolean equals(Object o) {
        return section.equals(o);
    }

    public int hashCode() {
        return section.hashCode();
    }

}
