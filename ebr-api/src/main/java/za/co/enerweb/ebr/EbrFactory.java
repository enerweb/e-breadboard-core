package za.co.enerweb.ebr;

import static org.apache.commons.lang.StringUtils.removeStart;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

import lombok.Cleanup;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.datatype.IDataTypeRegistryService;
import za.co.enerweb.ebr.env.IEnvironmentManager;
import za.co.enerweb.ebr.env.IEnvironmentValueReader;
import za.co.enerweb.ebr.internal.BeanFactory;
import za.co.enerweb.ebr.internal.OpenEjbBeanFactory;
import za.co.enerweb.ebr.server.HomeFile;
import za.co.enerweb.ebr.server.HomeFileCategory;
import za.co.enerweb.ebr.server.HomeFileSpec;
import za.co.enerweb.ebr.server.IConfigurationService;
import za.co.enerweb.ebr.server.IEbrServer;
import za.co.enerweb.ebr.server.IHomeService;
import za.co.enerweb.ebr.server.SystemConfiguration;
import za.co.enerweb.ebr.task.ISessionManager;
import za.co.enerweb.ebr.task.ITaskExecutorManager;
import za.co.enerweb.ebr.task.ITaskRegistry;
import za.co.enerweb.ebr.task_layout.ITaskLayoutRegistry;
import za.co.enerweb.toolbox.io.ResourceUtils;

/**
 * Get all your e-breadboard services here!
 */
@Slf4j
public class EbrFactory {
    private static final String LOG4J_PROPERTIES_PATH = "log4j.properties";
    public static final String CORE_EJB_MODULE_NAME =
        "ebr-orchestration-engine";
    private static final String EJB_JNDI_PROPERTIES_PATH = "ebr-ejb.properties";

    // private static Context EJB_CONTEXT_SINGLETON;
    // private static BeanFactory BEAN_FACTORY;
    @Getter
    private static volatile boolean open = false;
    private static volatile boolean closing = false;
    private static Properties properties;
    private static boolean printedProperties = false;

    // static {
    // // Let anything that tries to log to java commons logging rather
    // // log to slf4j, and set it once at startup.
    // log
    // .debug(" ***** OrchestrationEngineFactory SLF4JBridgeHandler.install()");
    // SLF4JBridgeHandler.install();
    // }

    public static void addInitialContextProperty(final String key,
        final String value) {
        properties.put(key, value);
    }

    // /**
    // * public for testing purposes
    // * @param openIfClosed
    // * @return
    // */
    // public static Context getEjbContext(
    // boolean openIfClosed) {
    // if (EJB_CONTEXT_SINGLETON != null && !closing) {
    // // first check without synchronizing, so that in 99% of cases this
    // // method can be *much* quicker!
    // return EJB_CONTEXT_SINGLETON;
    // }
    // synchronized (EbrFactory.class) {
    // if (openIfClosed && (!open || EJB_CONTEXT_SINGLETON == null
    // || BEAN_FACTORY == null)) {
    // open = true;
    // }
    // if (open) {
    // if (EJB_CONTEXT_SINGLETON == null) {
    // // fireOnBeforeEbrStart();
    //
    // getProperties();
    // try {
    // EJB_CONTEXT_SINGLETON = new InitialContext(properties);
    // detectJndiNameFactory(EJB_CONTEXT_SINGLETON);
    // } catch (NamingException e) {
    // startupFailed();
    // throw new IllegalStateException(
    // "Could not instantiate an initial context for "
    // + "looking up our ejb.", e);
    // } catch (RuntimeException e) {
    // startupFailed();
    // throw e;
    // }
    // }
    // } else {
    // throw new IllegalStateException("EbrFactory "
    // + "is Closed, you should open it before using it.");
    // }
    // return EJB_CONTEXT_SINGLETON;
    // }
    // }

    private static Properties getProperties() {
        if (properties == null) {
            synchronized (EbrFactory.class) {
                if (properties == null) {
                    properties = new Properties();
                    loadProperties(properties, "ebr-ejb-lookup.properties");
                    // load log4j properties if we can,
                    // so we don't have to declare it in two different files
                    loadProperties(properties, LOG4J_PROPERTIES_PATH);
                    // loadProperties(properties, EJB_JNDI_PROPERTIES_PATH);

                    if (log.isDebugEnabled() && !printedProperties) {
                        properties.list(System.out);
                        printedProperties = true;
                    }
                }
            }
        }
        return properties;
    }

    @SneakyThrows
    public static InitialContext getEjbContext(
        boolean openIfClosed) {
        // Properties p = new Properties();
        // p.put(Context.INITIAL_CONTEXT_FACTORY,
        // "org.apache.openejb.client.LocalInitialContextFactory");
        // // set any other properties you want
        // loadProperties(p, "jndi.properties");
        // loadProperties(properties, LOG4J_PROPERTIES_PATH);
        if (openIfClosed) {
            open = true;
        }
        return new InitialContext(getProperties());
        // return new InitialContext();
    }

    private static void startupFailed() {
        String asciiart = "";
        try {
            asciiart = "\n" + new String(
                ResourceUtils.getResourceAsBytes("ebr-fail-ascii.txt"));
        } catch (IOException e) {
            log.debug("Could not load ascii logo.", e);
        }

        log.error("{}\n   #   {} startup FAILED :'(   #\n\n",
            asciiart, SystemConfiguration.getBrandNameAndVersion());
    }

    private static BeanFactory detectJndiNameFactory(Context ejbContext) {
        // We only support openejb at the moment
        // if we want to support other containers in the future
        // we will need to implement detection or configuration
        // to chose other factories.
        // Hopefully all the jndi names will be standard in future
        // so we will not need this!
        // if (BEAN_FACTORY == null) {
        BeanFactory BEAN_FACTORY = new OpenEjbBeanFactory();
            BEAN_FACTORY.setEjbContext(ejbContext);
        // }
        return BEAN_FACTORY;
    }

    // private static void fireOnBeforeEbrStart() {
    // // find all classes annotated by @OnBeforeEbrStart
    // Set<Method> annotated =
    // Registrar.getReflections().getMethodsAnnotatedWith(
    // OnBeforeEbrStart.class);
    // for (Method annotatedMethod : annotated) {
    // if (!Modifier.isStatic(annotatedMethod.getModifiers())) {
    // log.warn("@BeforeEbrStart annotation found on "
    // + "a non-static method:\n" + annotatedMethod);
    // continue;
    // }
    // if (annotatedMethod.getParameterTypes().length != 0) {
    // log.warn("@BeforeEbrStart annotation found on "
    // + "a method that takes parameters:\n" + annotatedMethod);
    // continue;
    // }
    //
    // try {
    // annotatedMethod.invoke(null);
    // } catch (Exception e) {
    // log.error("Could not invoke @BeforeEbrStart method:\n"
    // + annotatedMethod, e);
    // }
    // }
    // }

    private static void loadProperties(final Properties properties,
        final String resourcePath) {
        try {
            ClassLoader classLoader = Thread.currentThread()
                .getContextClassLoader();
            @Cleanup InputStream resourceAsStream = classLoader
                .getResourceAsStream(resourcePath);
            if (resourceAsStream == null) {
                log.debug("Could not load properties from {}.",
                    resourcePath);
                log.debug("Thread.currentThread(): "
                    + Thread.currentThread());
                log.debug("Thread.currentThread().getName(): "
                    + Thread.currentThread().getName());
                log.debug("Thread.currentThread().getContextClassLoader(): "
                    + Thread.currentThread().getContextClassLoader());
                log.debug(
                    "EbrFactory.class.getClassLoader(): "
                        + EbrFactory.class.getClassLoader());
                return;
            }

            properties.load(resourceAsStream);
        } catch (IOException e) {
            log.debug("Could not load properties from " + resourcePath
                + ".", e);
        }
    }

    // private static String lookupJndiName(final String ejbNamesFile,
    // final String interfaceName) {
    // try {
    // ResourceBundleUtils ejb_jndi_names_rb =
    // new ResourceBundleUtils(ejbNamesFile);
    // String name = ejb_jndi_names_rb.getString(
    // interfaceName);
    // return name;
    // } catch (MissingResourceException e1) {
    // throw new RuntimeException(
    // "Could not look up jndi-name for '" + interfaceName
    // + "' in " + ejbNamesFile + ".properties", e1);
    // }
    // }

    public static <T> T lookupEjb(final Class<T> returnClass) {
        return lookupEjb(CORE_EJB_MODULE_NAME, returnClass);
    }

    /**
     * Only use this method if the bean name can be derived from the interface
     * by removing the leading I (which we add by convention).
     * @param moduleName
     * @param returnClass
     * @return
     */
    public static <T> T lookupEjb(final String moduleName,
        final Class<T> returnClass) {
        return lookupEjb(moduleName, returnClass, true);
    }

    public static <T> T lookupEjbIfOpen(final String moduleName,
        final Class<T> returnClass) {
        return lookupEjbIfOpen(moduleName, getBeanName(returnClass),
            returnClass);
    }

    private static <T> String getBeanName(final Class<T> returnClass) {
        return removeStart(returnClass.getSimpleName(), "I");
    }

    public static <T> T lookupEjb(final String moduleName,
        final Class<T> returnClass, boolean openIfClosed) {
        return lookupEjb(moduleName, getBeanName(returnClass), returnClass,
            openIfClosed);
    }

    /**
     * @param moduleName
     * @param beanName the ejb-name of the enterprise bean. For enterprise beans
     *        defined via annotation, it
     *        defaults to the unqualified name of the session bean class, unless
     *        specified in the contents of the
     *        Stateless/Stateful/Singleton annotation name() attribute.
     * @param returnClass
     * @return
     */
    public static <T> T lookupEjb(final String moduleName, String beanName,
        final Class<T> returnClass) {
        return lookupEjb(moduleName, beanName, returnClass, true);
    }

    public static <T> T lookupEjbIfOpen(final String moduleName,
        String beanName,
        final Class<T> returnClass) {
        if (!open || closing) {
            return null;
        }
        return lookupEjb(moduleName, beanName, returnClass, false);
    }

    private static <T> T lookupEjb(final String moduleName, String beanName,
        final Class<T> returnClass, boolean openIfClosed) {
        // need to get the context all the time or it doesn't work
        // if (BEAN_FACTORY == null) {
        Context ejbContext = getEjbContext(openIfClosed);
        // }
        return detectJndiNameFactory(ejbContext)
            .lookupEjb(moduleName, beanName, returnClass);
    }

    // @SuppressWarnings("unchecked")
    // public static <T> T lookupEjb(final String ejbNamesFile,
    // final Class<T> returnClass, boolean openIfClosed) {
    // String interfaceName = returnClass.getSimpleName();
    // String name = lookupJndiName(ejbNamesFile, interfaceName);
    // try {
    // return (T) getEjbContext(openIfClosed).lookup(name);
    // } catch (NamingException e) {
    // throw new RuntimeException(
    // "Could not look up " + name, e);
    // }
    // }

    /**
     * Starts up the orchestration engine.
     * @deprecated Every OrchestrationEngineFactory user does not need to open
     *             any more, because it will be done automatically now when
     *             looking up
     *             services.
     */
    public static void open() {
        getEjbContext(true);
    }

    public static IEbrServer getEbrServer() {
        return lookupEjb(IEbrServer.class);
    }

    public static IHomeService getHomeService() {
        return lookupEjb(IHomeService.class);
    }

    /**
     * experimental, can only be called from within a container..
     * @return
     */
    @SneakyThrows
    public static IDataTypeRegistryService getDataTypeRegistryService() {
        return lookupEjb(IDataTypeRegistryService.class);
    }

    @SneakyThrows
    public static IEnvironmentManager getEnvironmentManager() {
        return lookupEjb(IEnvironmentManager.class);
    }

    public static IEnvironmentValueReader getEnvironmentValueReader() {
        return lookupEjb(IEnvironmentValueReader.class);
    }

    public static ITaskRegistry getTaskRegistry() {
        return lookupEjb(ITaskRegistry.class);
    }

    public static ITaskExecutorManager getTaskExecutorManager() {
        return lookupEjb(ITaskExecutorManager.class);
    }

    public static ISessionManager getSessionManager() {
        return lookupEjb(ISessionManager.class);
    }

    public static ITaskLayoutRegistry getTaskLayoutRegistry() {
        return lookupEjb(ITaskLayoutRegistry.class);
    }

    // public static ITaskRegistryRemote getTaskRegistryRemote() {
    // return lookupEjb(ITaskRegistryRemote.class);
    // }

    public static IConfigurationService getConfigurationService() {
        return lookupEjb(IConfigurationService.class);
    }

    public static void setOrchestrationEngineClosing() {
        if (open) {
            closing = true;
        }
    }

    public static void setOrchestrationEngineDestroyed() {
        closing = false;
        open = false;
    }

    /**
     * Conf file pattern we want to promote: when running normally,
     * get the conf file out of e-breadboard-home/etc,
     * but if it doesn't exist or if e-breadboard is not started yet,
     * just get it out of the classpath.
     * @param fileName
     * @return
     */
    // TODO: implement a getConfFileContent if you need to get files from
    // within jars
    @SneakyThrows
    public static File getConfFile(String fileName) {
        if (isOpen()) {
            HomeFile confHomeFile =
                EbrFactory.getHomeService().getFile(
                    new HomeFileSpec(HomeFileCategory.ETC,
                        fileName));
            if (confHomeFile.canRead()) {
                log.debug("Using conf file: " + confHomeFile);
                return confHomeFile.getFile();
            } else {
                log.info("Can't read conf file: " + confHomeFile);
            }
        } else {
            log.info("Factory is closed :(");
        }
        File ret = ResourceUtils.getResourceAsFile(fileName);
        log.warn("falling back to conf file in classpath: " + ret);
        return ret;
    }

}
