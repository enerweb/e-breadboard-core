package za.co.enerweb.ebr.datatype.dataresource;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import javax.xml.bind.ValidationException;

/**
 * logical bundle of files
 */
public interface ITarDataResource extends IDataResource {
    byte[] getBytes(String entryName) throws IOException,
        ValidationException;

    Iterator<String> getEntryNames() throws IOException,
        ValidationException;

    InputStream getInputStream(String entryName) throws IOException,
        ValidationException;
}
