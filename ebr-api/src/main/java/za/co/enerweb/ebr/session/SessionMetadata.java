package za.co.enerweb.ebr.session;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import lombok.Data;

import org.apache.commons.lang.NotImplementedException;

/**
 * Everything you want to know about a session.
 * Its meant to be portable, lightweight and passed through layers.
 */
@Data
public class SessionMetadata implements Serializable {
    public static final String DEFAULT_NAMESPACE = "";
    private static final long serialVersionUID = 1L;
    private String id;
    private String name;
    private Map<String, String> environmentIds = new HashMap<String, String>();

    public SessionMetadata() { }

    public SessionMetadata(final String sessionId, final String sessionName,
        final String environmentId) {
        super();
        id = sessionId;
        name = sessionName;
        setEnvironmentId(DEFAULT_NAMESPACE, environmentId);
    }

    public void setEnvironmentId(final String namespace,
            final String IEnvironmentId){
        environmentIds.put(namespace, IEnvironmentId);
    }

    /**
     * @return null if it does not exist
     */
    public String getEnvironmentId(final String namespace){
        return environmentIds.get(namespace);
    }

    public String getOrCreateEnvironmentId(final String namespace) {
        throw new NotImplementedException();
        // String ret = getEnvironmentId(namespace);
        // if (ret == null) {
        // IEnvironmentManager envManager = EbrFactory
        // .getEnvironmentManager();
        // ret = envManager.newEnvironmentId();
        // environmentIds.put(namespace, ret);
        // }
        // return ret;
    }

    public void setEnvironmentId(final String IEnvironmentId){
        environmentIds.put(DEFAULT_NAMESPACE, IEnvironmentId);
    }

    public String getEnvironmentId(){
        return environmentIds.get(DEFAULT_NAMESPACE);
    }

    /**
     * @deprecated use setEnvironmentId
     */
    @Deprecated
    public void setEnvId(final String IEnvironmentId){
        setEnvironmentId(IEnvironmentId);
    }

    /**
     * @deprecated use getEnvironmentId
     */
    @Deprecated
    public String getEnvId(){
        return getEnvironmentId();
    }
}
