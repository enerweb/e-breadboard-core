package za.co.enerweb.ebr.task;

import java.util.List;

import javax.ejb.Local;

import za.co.enerweb.ebr.env.IEnvironment;
import za.co.enerweb.ebr.exceptions.TaskExecutionException;
import za.co.enerweb.ebr.exceptions.TaskInstantiationException;
import za.co.enerweb.ebr.exceptions.UnknownTaskExecutionException;

@Local
public interface ITaskExecutorManager {
    TaskExecutionMetadata getTaskExecutionMetadata(final String jobId)
        throws UnknownTaskExecutionException;

    /**
     * @param taskId
     * @param sessionId may be null
     * @param inputEnvironment may be null
     * @return
     * @throws TaskInstantiationException
     */
    TaskExecutionMetadata initTaskExecution(final String taskId,
        final String sessionId,
        IEnvironment inputEnvironment)
        throws TaskInstantiationException;

    /**
     * @param taskMetadata
     * @param sessionId may be null
     * @param inputEnvironment may be null
     * @return
     * @throws TaskInstantiationException
     */
    TaskExecutionMetadata initTaskExecution(
        final TaskMetadata taskMetadata,
        final String sessionId, IEnvironment inputEnvironment)
        throws TaskInstantiationException;

    TaskExecutionMetadata run(final String taskExecutionId,
        final RunMode runMode)
        throws TaskExecutionException, UnknownTaskExecutionException;

    List<TaskExecutionMetadata> getTaskExecutionMetadata();

    void taskExecutionDone(String id);
}
