package za.co.enerweb.ebr.internal;

import java.lang.annotation.Annotation;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Remote;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OpenEjbBeanFactory extends BeanFactory {

    @SuppressWarnings("unchecked")
    private static Class<? extends Annotation>[] supportedAnnotations
        = new Class[]{Local.class, Remote.class, LocalBean.class};

    @Override
    protected String lookupEjbJndiName(String moduleName, String beanName,
        Class<?> returnClass) {
        for (Class<? extends Annotation> an : supportedAnnotations) {
            if (returnClass.isAnnotationPresent(an)) {

                final String ret = // "java:comp/env/" +
                    beanName + an.getSimpleName()
                ;

                // "java:module/" +
                // + beanName
//                    + an.getSimpleName();
//                log.debug("beanName=" + ret);
                return ret;
            }
        }
        throw new RuntimeException("The specified ejb type ("
            + returnClass + ") does not have a "
            + "recognised annotation.");
    }

}
