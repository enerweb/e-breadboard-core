package za.co.enerweb.ebr.task;

import java.util.Collection;
import java.util.Iterator;

/**
 * null perspective id is the default
 */
public interface ITaskRegistryRemote {
    Collection<String> getPerspectiveIds();

    Iterator<TaskMetadata> getTaskIterator(
        final String perspectiveId);

    /**
     * Throws CouldNotFindTaskException if the task could not be found.
     */
    TaskMetadata getTaskMetadata(final String perspectiveId,
        final String id);

    Collection<TaskMetadata> getTasks(
        final String perspectiveId);

    void refreshRegistry();

    void registerTask(final TaskMetadata taskMetadata);

    void registerTask(final String perspectiveId,
        final TaskMetadata taskMetadata);
}
