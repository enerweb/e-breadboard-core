package za.co.enerweb.ebr.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Joiner;

/**
 * Specifies a HomeFile location.
 * When constructing a HomeFileSpec, all arguments will be split up.
 */
@Data
@AllArgsConstructor
public class HomeFileSpec {

    public static final String SEPARATOR = "/";

    // for display purposes only
    public static final Joiner PATH_JOINER = Joiner.on(SEPARATOR);

    private HomeFileCategory category;
    private List<String> parameters;
    private List<String> pathElements;

    public static String join(String... pathElements) {
        return PATH_JOINER.join(pathElements);
    }

    public static String join(Iterable<?> pathElements) {
        return PATH_JOINER.join(pathElements);
    }

    public static String[] split(String relPath) {
        return StringUtils.split(relPath, SEPARATOR);
    }

    public HomeFileSpec(HomeFileCategory category) {
        this(category, new String[0]);
    }

    /**
     * @param category
     * @param parametersAndPathElements the category determines how many
     *        elements are considered parameters
     */
    public HomeFileSpec(HomeFileCategory category,
            String... parametersAndPathElements) {
        this(category, Arrays.asList(parametersAndPathElements));
    }

    public HomeFileSpec(HomeFileCategory category,
        String[] firstParameterOrPathElements,
        String... parametersAndPathElements) {
        this(category, join(firstParameterOrPathElements,
            parametersAndPathElements));
    }

    private static List<String> join(String[] firstParameterOrPathElements,
        String... parametersAndPathElements) {
        List<String> ppl = new ArrayList<String>(
            firstParameterOrPathElements.length +
            parametersAndPathElements.length);
        ppl.addAll(Arrays.asList(firstParameterOrPathElements));
        ppl.addAll(Arrays.asList(parametersAndPathElements));
        return ppl;
    }

    // vvv some extra helper static constructors
    /**
     * @deprecated rather use subdir
     */
    public static HomeFileSpec new1(HomeFileCategory category,
        String firstParameterOrPathElement,
        String... parametersAndPathElements) {
        return new HomeFileSpec(category,
            join(new String[]{firstParameterOrPathElement},
            parametersAndPathElements));
    }

    /**
     * @deprecated rather use subdir
     */
    public static HomeFileSpec new2(HomeFileCategory category,
        String firstParameterOrPathElement,
        String secondParameterOrPathElement,
        String... parametersAndPathElements) {
        return new HomeFileSpec(category, join(
            new String[]{firstParameterOrPathElement,
            secondParameterOrPathElement},
            parametersAndPathElements));
    }

    public HomeFileSpec(HomeFileCategory category,
            Collection<String> parametersAndPathElements) {
        this.category = category;
        int paramCount = category.getParamCount();
        if (parametersAndPathElements.size() < paramCount) {
            throw new IllegalArgumentException(String.format("Category %s "
                + "requires at least %s parameters %s provided.",
                category, paramCount, parametersAndPathElements.size()));
        }

        // don't convert it to a list if it is already one
        List<String> papeList;
        if (parametersAndPathElements instanceof List) {
            papeList = (List<String>) parametersAndPathElements;
        } else {
            papeList = new ArrayList<String>(parametersAndPathElements);
        }

        pathElements = new ArrayList<String>(
            parametersAndPathElements.size() - paramCount);
        splitAndAddAll(papeList.subList(paramCount, papeList.size()),
            pathElements);

        // split the params from the path elements
        if (paramCount > 0) {
            parameters = new ArrayList<String>(paramCount);
            splitAndAddAll(papeList.subList(0, paramCount), parameters);
        } else {
            parameters = Collections.emptyList();
        }
    }

    /**
     * Subdir or subfile, elements may include path segments.
     */
    public HomeFileSpec subDir(
        String... pathElements) {
        return subDir(Arrays.asList(pathElements));
    }

    /**
     * Subdir or subfile, elements may include path segments.
     */
    public HomeFileSpec subDir(
        Collection<String> subPathElements) {

        if (subPathElements.isEmpty()) {
            return this;
        }
        List<String> newPathElements = new ArrayList<String>(pathElements);
        splitAndAddAll(subPathElements, newPathElements);
        return new HomeFileSpec(category, parameters, newPathElements);
    }

    private static void splitAndAddAll(Collection<String> source,
        Collection<String> target) {
        for (String e : source) {
            if (e != null) {
                for (String ee: split(e)) {
                    target.add(ee);
                }
            }
        }
    }

    public String toString() {
        return category + ":" + PATH_JOINER.join(parameters)
            + ">" + PATH_JOINER.join(pathElements);
    }

    public HomeFile getInvalidHomeFile() {
        return new HomeFile(null, toString());
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        return toString().equals(obj.toString());
    }
}
