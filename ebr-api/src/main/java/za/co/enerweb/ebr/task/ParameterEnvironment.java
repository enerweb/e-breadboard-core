package za.co.enerweb.ebr.task;

/**
 *
 */
public enum ParameterEnvironment {
    JOB, SESSION;

    @Override
    public String toString() {
        return name().toLowerCase().replace('_', '-');
    }

    public static ParameterEnvironment fromString(final String string) {
        return valueOf(string.toUpperCase().replace('-', '_'));
    }
}
