package za.co.enerweb.ebr.datatype.dataresource;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

@Data
@EqualsAndHashCode(callSuper=true)
public class EnvFileDataSource extends AbstractDataSource {
    private static final long serialVersionUID = 7185373353145425345L;
    private String relativePath;
    private String envId;
    private File file;

    // public EnvFileDataSource(final String envId, final String
    // envRelativePath) {
    // this(envId, EbrFactory.getHomeService().getFile(new HomeFileSpec(
    // HomeFileCategory.ENVIRONMENTS, envId, envRelativePath)).getFile(),
    // envRelativePath);
    // }

    /**
     * assumes the file is in the envDir
     * @param envId
     * @param file
     * @param envRelativePath
     */
    public EnvFileDataSource(final String envId, final File file,
        final String envRelativePath) {
        super(FilenameUtils.getName(envRelativePath));
        relativePath = envRelativePath;
        this.file = file;
    }

    public void instantiateFile(final File envDir) {
        file = new File(envDir, relativePath);
    }

    public byte[] asBytes() throws IOException {
        return IOUtils.toByteArray(new FileInputStream(file));
    }

    public InputStream asInputStream() throws FileNotFoundException {
        return new FileInputStream(file);
    }

    @Override
    public String toString() {
        return "FileDataResource: "
            + (file != null ? file.getAbsoluteFile() : "null");
    }

    /*
     * (non-Javadoc)
     * @see
     * za.co.enerweb.ebr.datatype.dataresource.IDataSource#asAbsolutePath()
     */
    @Override
    public String asAbsolutePath() throws IOException {
        return file.getAbsolutePath();
    }

}
