package za.co.enerweb.ebr.task;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import javax.ejb.Local;

@Local
public interface ITaskRegistry extends ITaskRegistryRemote {
    void clear();

    Collection<ATaskParser> getAllTaskParsers();

    /*
     * Here for testing
     */
    void parseTaskPerspective(final String perspectiveId,
        final InputStream stream) throws IOException,
        CloneNotSupportedException ;
}
