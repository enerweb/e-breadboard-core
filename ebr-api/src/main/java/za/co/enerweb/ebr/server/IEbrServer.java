package za.co.enerweb.ebr.server;

import java.io.IOException;
import java.io.Serializable;

import javax.ejb.Local;
import javax.ejb.Remote;

import za.co.enerweb.ebr.env.EnvVarItem;
import za.co.enerweb.ebr.env.EnvVarMetadata;

/**
 * Manages the orchestration engine.
 */
@Local
@Remote
public interface IEbrServer {
    public static final String RESOURCE_PACKAGE = "za/co/enerweb/ebr";
    public static final String EBR_ABOUT_HTML = "ebr-about.html";

    @Deprecated
    String newEnvironment();

    @Deprecated
    void deleteEnvironment(String envId);

    @Deprecated
    void saveEnvironment(String envId);

    @Deprecated
    EnvVarItem getEnvVar(String envId, String varName);

    @Deprecated
    Serializable getEnvVarValue(String envId, String varName);

    @Deprecated
    EnvVarMetadata getEnvVarMetadata(String envId, String varName);

    @Deprecated
    void setEnvVar(String envId, EnvVarItem envItem);

    /**
     * reuses previous metadata if available
     * @param envId
     * @param varName
     * @param value
     */
    @Deprecated
    void setEnvVarValue(String envId, String varName, Serializable value);

    @Deprecated
    void setEnvVarMetadata(String envId, EnvVarMetadata metadata);

    @Deprecated
    byte[] getEnvResourceAsBytes(String envId, String resourceName)
        throws IOException;

    /**
     * @return a new globally unique id
     */
    String getNewGuid();

    /**
     * Reload all registries from the ebr home (everything the user may have
     * changed since startup).
     */
    void reload();

    /**
     * Only reload the tasks
     */
    void reloadTasks();

    /**
     * Reload all caches and registries, mainly for unit tests.
     */
    void reloadAll();

    // /**
    // * Indicate that all the ebr services should start up.
    // */
    // void start();
    //
    // /**
    // * Indicate that all the ebr services should shut down.
    // */
    // void stop();

    /*
     * XXX: should move to the SystemConfigurationService when it is
     * accessible from the EBRFactory
     */
    boolean isInDevMode();

    HomeFile addLogFile(LogFileSpec lfs)
        throws IOException;
}
