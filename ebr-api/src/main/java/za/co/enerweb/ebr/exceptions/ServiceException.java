package za.co.enerweb.ebr.exceptions;

import javax.ejb.ApplicationException;

/**
 * Used by most non-development error code.
 */
@ApplicationException
public class ServiceException extends RuntimeException {

    private static final long serialVersionUID = 5575931823267545793L;

    /**
     *
     */
    public ServiceException() {
    }

    /**
     * @param message
     */
    public ServiceException(final String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public ServiceException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public ServiceException(final Throwable cause) {
        super(cause);
    }

}
