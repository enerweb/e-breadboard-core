package za.co.enerweb.ebr.server;

import za.co.enerweb.toolbox.string.StringUtils;

public class IdGenerator {

    public static String getTaskExecId(final String taskTitle) {
        return getUniqueTaskName(taskTitle)
            + "_" + getLocallyUniqueId();
    }

    public static String getUniqueTaskName(final String taskTitle) {
        return StringUtils.caption2VariableName(taskTitle.toLowerCase())
            .replace('_', '-');
    }

    /**
     * It is shorter but has a tiny possibility of getting duplicates
     * if generated on different systems.
     * @return
     */
    public static String getLocallyUniqueId() {
        return za.co.enerweb.toolbox.id.IdGenerator.getLocallyUniqueId();
    }

    /**
     * get universally unique id
     * @return
     */
    public static String getUuid() {
        return za.co.enerweb.toolbox.id.IdGenerator.getUuid();
    }
}
