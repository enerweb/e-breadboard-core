package za.co.enerweb.ebr.exceptions;

/**
 *
 */
public class TaskInstantiationException extends RuntimeException {

    private static final long serialVersionUID = 7002873675695443557L;

    /**
     *
     */
    public TaskInstantiationException() {
    }

    /**
     * @param message
     */
    public TaskInstantiationException(final String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public TaskInstantiationException(final String message,
        final Throwable cause) {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public TaskInstantiationException(final Throwable cause) {
        super(cause);
    }

}
