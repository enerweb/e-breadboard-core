package za.co.enerweb.ebr.datatype.dataresource;

import java.io.IOException;
import java.io.InputStream;

import za.co.enerweb.ebr.datatype.ITypeConverter;
import za.co.enerweb.ebr.exceptions.ServiceException;

public class FileDataResource extends AbstractDataResource
    implements ITypeConverter<FileDataResource> {
    private static final long serialVersionUID = 1L;
    public static final String TYPE_ALIAS = "file";
    public static final String DEPRECATED_TYPE_ALIAS = "document";

    public FileDataResource() {
    }

    public FileDataResource(
        final String envId, final String path) {
        super(envId, path);
    }

    public FileDataResource(
        final String envId, String fileName, final InputStream data) {
        super(envId, fileName, data);
    }

    @Override
    public FileDataResource fromSimpleStructure(final String envId,
        final String variableName,
        final Object simpleStructure) {
        if (simpleStructure instanceof InputStream) {
            return new FileDataResource(
                envId, variableName, (InputStream) simpleStructure);
        }
        return new FileDataResource(
            envId, (String) simpleStructure);
    }

    @Override
    public Object toSimpleStructure(final String envId,
        final String variableName,
        final FileDataResource specificObject) {
        try {
            return (specificObject).getAbsolutePath();
        } catch (IOException e) {
            throw new ServiceException("Could not read file", e);
        }
    }

    @Override
    public String getType() {
        return getClass().getName();
    }
}
