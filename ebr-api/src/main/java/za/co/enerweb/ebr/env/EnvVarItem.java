package za.co.enerweb.ebr.env;

import java.io.Serializable;

import lombok.Data;

/**
 * A EnvVarMetadata + value pair.
 */
@Data
public class EnvVarItem implements Serializable {
    private static final long serialVersionUID = 1355033923894801843L;
    private EnvVarMetadata metadata;
    private Serializable value;

    public EnvVarItem() {

    }

    public EnvVarItem(final EnvVarMetadata metadata, final Serializable value) {
        super();
        this.metadata = metadata;
        this.value = value;
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> T getValue() {
        return (T) value;
    }
}
