package za.co.enerweb.ebr.env;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import za.co.enerweb.ebr.datatype.DataTypeUtil;

/**
 * Parent class of everything that among others describes a java type.
 */
@Data
@EqualsAndHashCode(of = "typeName")
public abstract class AbstractTypeDescriptor implements Serializable {

    private static final long serialVersionUID = 1L;

    // store type as string in order to support List<String> etc.
    private String typeName = null;

    public AbstractTypeDescriptor() {
    }

    public AbstractTypeDescriptor(final String typeName) {
        this.typeName = typeName;
    }

    public AbstractTypeDescriptor(final Class<? extends Serializable> type) {
        typeName = type.getName();
    }

    /**
     * Not intended for the user's eyes
     * @return
     */
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(final String typeName) {
        this.typeName = typeName;
    }

    /**
     * not an excact science, but mayb be useful in certain instances.
     * @return
     * @throws ClassNotFoundException
     */
    @SuppressWarnings("unchecked")
    public Class<? extends Serializable> getTypeClass()
        throws ClassNotFoundException {
        return (Class<? extends Serializable>) DataTypeUtil
            .getTypeClass(typeName);
    }

    public void setTypeClass(final Class<? extends Serializable> type) {
        typeName = type.getName();
    }

}
