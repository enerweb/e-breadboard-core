package za.co.enerweb.ebr.task;

import lombok.Data;

import org.slf4j.Logger;

import za.co.enerweb.ebr.env.IEnvironment;
import za.co.enerweb.ebr.task.TaskExecutionMetadata;

//TODO: move to the core plugin or ebr-api
@Data
public abstract class AbstractTask implements ITask {

    private transient IEnvironment inputEnv;

    private transient IEnvironment outputEnv;

    private transient Logger logger;

    private TaskMetadata taskMetadata;

    private TaskExecutionMetadata taskExecutionMetadata;

    private String ebrHomePath;

    private String workdir;

    public IEnvironment getInputEnv() {
        return inputEnv;
    }

    public void setInputEnv(final IEnvironment inputEnv) {
        this.inputEnv = inputEnv;
    }

    public IEnvironment getOutputEnv() {
        return outputEnv;
    }

    public void setOutputEnv(final IEnvironment outputEnv) {
        this.outputEnv = outputEnv;
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(final Logger logger) {
        this.logger = logger;
    }

    public TaskMetadata getTaskMetadata() {
        return taskMetadata;
    }

    public void setTaskMetadata(final TaskMetadata taskMetadata) {
        this.taskMetadata = taskMetadata;
    }
}
