package za.co.enerweb.ebr.datatype.dataresource;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import javax.xml.bind.ValidationException;

import za.co.enerweb.ebr.env.EnvResourceMetadata;
import za.co.enerweb.ebr.env.IEnvironment;

/**
 * abstract view of a data resource
 */
public interface IDataResource extends Serializable {
    void addToEnvironmentIfNeeded(IEnvironment env)
        throws ValidationException;

    String getAbsolutePath() throws IOException, ValidationException;

    byte[] getBytes() throws IOException, ValidationException;

    InputStream getInputStream() throws IOException,
        ValidationException;

    EnvResourceMetadata getResourceMetadata()
        throws ValidationException;
}
