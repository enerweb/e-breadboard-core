package za.co.enerweb.ebr.task;

import lombok.Data;

/**
 *
 */
@Data
public class Error {
    private String id;
    private String message;
    private Throwable throwable;
}
