package za.co.enerweb.ebr.server;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.ejb.Local;

@Local
public interface IHomeService extends IHomeServiceRemote {

    /**
     * filename of used for all readme files
     */
    String README = "README.txt";

    String TASK_PERSPECTIVE_EXTENTION = "ini";

    /**
     * @return
     * @deprecated Can we get rid of this at some point please.
     *             Why do tasks have to know where this is?!
     *             We are keeping it here for now in order to be a little more
     *             backwards
     *             compatible.
     */
    File getMainHomeDir();

    List<File> getAllHomeDirs();

    /**
     * @param homeFileSpec
     * @return first file supported by layout, may not exist.
     */
    HomeFile getFile(HomeFileSpec homeFileSpec);

    /**
     * Look for an existing file.
     * @param category
     * @param pathElements
     * @return an invalid HomeFile if a file can not be found
     */
    HomeFile findFile(HomeFileSpec homeFileSpec);


    /**
     * Find files (not directories) recursively.
     * @param homeFileSpec
     * @param extensions an array of extensions, eg. {"java","xml"}.
     *        If this
     *        parameter is empty, all files are returned.
     * @return an Iterable of objects that contains all the information you
     *         can dream of about each file. (It can only be used once,
     *         then it auto-destructs :)
     */
    Iterable<HomeFile> findFiles(HomeFileSpec homeFileSpec,
        final String... extensions);

    /**
     * List the files and directories under the matching the home file spec
     * (non-recursive)
     * @param homeFileSpec
     * @return
     */
    Iterable<HomeFile> list(HomeFileSpec homeFileSpec);

    /**
     * Gives you a non-existing file in the temp space,
     * you can make it a directory or write stuff to it or whatever.
     * @param templateFileName (the temp file name will end in this)
     * @return
     */
    HomeFile getTempFile(String templateFileName);

    /**
     * Move a file or directory to the trash
     */
    HomeFile deleteFile(HomeFile homeFile) throws IOException;

    /**
     * Write a resource from the classpath to the specified directory.
     * (uses the file name from the resource)
     * Creates dir if it does not exist yet.
     * @param homeFileSpec
     * @param resoucePath eg. "my/package/resource.txt"
     * @throws IOException
     */
    void writeFileFromClasspath(HomeFileSpec homeFileSpec, String resoucePath)
        throws IOException;

    void addHome(File dir);

    // /**
    // * Rather use the deleteFile api if at all possible.
    // * Gives you a non-existing file in the trash space,
    // * you can make it a directory or write stuff to it or whatever.
    // * If it turns out that we need to delete arbitrary files then rather make
    // * an api for that.
    // */
    // HomeFile getTrashFile(String templateFileName);

    void setUpCleanupScheadules();
}
