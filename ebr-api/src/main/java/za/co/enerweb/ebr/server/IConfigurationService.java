package za.co.enerweb.ebr.server;

import javax.ejb.Local;

/**
 * Maybe this should be json and maybe send the json object arround to
 * plugins/applications..
 * This is only for stuff that is configured in the
 * e-breadboard-home/etc/config.ini and it should be possible to edit it from
 * the frontend. We should write out a good template to
 * e-breadboard-home/etc/config.example.ini I think we should just store it
 * generically section -> key -> value So that plugins can also use this, by
 * extending this. I think all the plugins to use this single file, but they may
 * write out their own examples eg.
 * e-breadboard-home/etc/config.vaadin-example.ini
 */
@Local
public interface IConfigurationService {
    public static final String FILENAME = "config.ini";
    public static final String CORE_SECTION = "core";
    public static final String CORE_CLEANUP_SECTION = "core/cleanup";

    /**
     * Will never return null
     */
    public ConfSection getSection(String sectionName);

    public ConfSection getCoreSection();

    public ConfSection getCoreCleanupSection();

    public void reload();

}
