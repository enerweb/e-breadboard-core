package za.co.enerweb.ebr.task;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlTransient;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class InputMetadata extends ParameterMetadata {
    private static final long serialVersionUID = 1L;
    private boolean optional = false;

    @Getter(onMethod = @_({@XmlTransient}))
    // XXX: maybe we want to be able to obtain this using a callback
    // because it may be expensive to initialise.
    private Serializable defaultValue;

    public InputMetadata() {

    }

    public InputMetadata(final Class<? extends Serializable> type,
        final String name, final String description,
        final Serializable defaultValue, final boolean optional) {
        super(type, name, description);
        this.optional = optional;
        this.defaultValue = defaultValue;
    }
}
