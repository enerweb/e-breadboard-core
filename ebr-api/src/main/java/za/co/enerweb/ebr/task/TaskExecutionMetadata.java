package za.co.enerweb.ebr.task;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.SneakyThrows;

import org.joda.time.Period;
import org.joda.time.format.PeriodFormatterBuilder;

/**
 * Everything you want to know about a job except its log, inputs and outputs
 * (which might be big and inconvenient to keep here)
 * Its meant to be portable, lightweight and passed through layers.
 */
@Data
@EqualsAndHashCode(of = "id")
public class TaskExecutionMetadata implements Serializable {
    private static final int UNDEFINED_DURATION = -1;
    private static final long serialVersionUID = 1L;
    private String id;
    private TaskMetadata taskMetadata = new TaskMetadata();
    private TaskExecutionStatus status = TaskExecutionStatus.UNINITIALISED;
    private RunMode runMode = RunMode.BATCH;
    private String inputIEnvironmentId;
    private String outputIEnvironmentId;
    private Date startTime;
    private Date stopTime;
    private String message = "";

    /**
     * ratio to indicate progress:
     * 0 = just started
     * 1 = finished
     */
    private float progress;

    public TaskExecutionMetadata() {
    }

    public TaskExecutionMetadata(final String id,
        final TaskMetadata taskMetadata) {
        this.id = id;
        this.taskMetadata = taskMetadata;
    }

    private DateFormat getDateFormat() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }

    public String getStartTimeString(){
        if (startTime == null) {
            return "";
        }
        return getDateFormat().format(startTime);
    }

    public String getStopTimeString(){
        if (stopTime == null) {
            return "";
        }
        return getDateFormat().format(stopTime);
    }

    /**
     * the total ducation or the duration so far if not stopped yet
     * @return
     */
    public long getDurationMillis(){
        if (startTime == null) {
            return UNDEFINED_DURATION;
        }
        if (stopTime == null) {
            Date now = new Date();
            return now.getTime() - startTime.getTime();
        }
        return stopTime.getTime() - startTime.getTime();
    }

    public String getDurationString(){
        long durationMillis = getDurationMillis();
        if (durationMillis == UNDEFINED_DURATION) {
            return "";
        }
        return formatDuration(durationMillis);
    }

    private String formatDuration(final long durationMillis) {
        return new PeriodFormatterBuilder()
            // not working:
            //.appendDays().appendSuffix(" day", " days").appendSeparator(" ")
            .appendHours().appendSeparator(":")
            .appendMinutes().appendSeparator(":")
            .appendSecondsWithOptionalMillis()
            .toFormatter().print(new Period(durationMillis)) + "s";
    }

    @Deprecated
    public String getInputIEnvironmentd() {
        return inputIEnvironmentId;
    }

    @Deprecated
    public void setInputIEnvironmentd(final String inputIEnvironmentId) {
        this.inputIEnvironmentId = inputIEnvironmentId;
    }

    @Deprecated
    public String getOutputIEnvironmentd() {
        return outputIEnvironmentId;
    }

    @Deprecated
    public void setOutputIEnvironmentd(final String outputIEnvironmentId) {
        this.outputIEnvironmentId = outputIEnvironmentId;
    }

    @SneakyThrows
    public void waitTillDone() {
        int i = 1;
        while(status.isBusy()){
            Thread.sleep(100*i);
            if (i > 100) {
                throw new IllegalStateException(
                    "Gave up waiting for task execution:"
                + this.taskMetadata);
            }
        }
    }
}
