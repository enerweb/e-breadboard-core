package za.co.enerweb.ebr.exceptions;

/**
 * Could not find a specified renderer.
 */
public class InvalidEnvVarRendererException extends Exception {

    private static final long serialVersionUID = 474732962083172010L;

    /**
     *
     */
    public InvalidEnvVarRendererException() {
    }

    /**
     * @param message
     */
    public InvalidEnvVarRendererException(final String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public InvalidEnvVarRendererException(final String message,
        final Throwable cause) {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public InvalidEnvVarRendererException(final Throwable cause) {
        super(cause);
    }

}
