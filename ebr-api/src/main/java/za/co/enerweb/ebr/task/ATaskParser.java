package za.co.enerweb.ebr.task;

import static za.co.enerweb.ebr.datatype.control.Action.isActionInput;
import static za.co.enerweb.ebr.server.HomeFileCategory.TASKS;

import java.io.File;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FileUtils;

import za.co.enerweb.ebr.datatype.IDataTypeRegistryService;
import za.co.enerweb.ebr.datatype.ITypeConverter;
import za.co.enerweb.ebr.datatype.control.Action;
import za.co.enerweb.ebr.env.EnvVarMetadata;
import za.co.enerweb.ebr.exceptions.InvalidMetadataException;
import za.co.enerweb.ebr.server.HomeFile;
import za.co.enerweb.ebr.server.HomeFileSpec;
import za.co.enerweb.ebr.server.IEbrServer;
import za.co.enerweb.ebr.server.IHomeService;
import za.co.enerweb.toolbox.io.ResourceUtils;
import za.co.enerweb.toolbox.string.StringUtils;
import za.co.enerweb.toolbox.timing.Stopwatch;

/**
 * Base class for all task parsers.
 * Simple structure = primitives, lists, maps and combinations thereof.
 */
@Slf4j
public abstract class ATaskParser {
    public static final String EXAMPLE_TASK_PACKAGE = "example_tasks";

    /**
     * Closure workaround. Used for implementing generic algorithms,
     * that require calling some function deep in the algorithm.
     */
    private static interface ListCallBack {
        void callback(TaskMetadata tmd,
            @SuppressWarnings("rawtypes") List taskInput);
    }

    /**
     * Closure workaround. Used for implementing generic algorithms,
     * that require calling some function deep in the algorithm.
     */
    static interface MapCallBack {
        void callback(TaskMetadata tmd,
            @SuppressWarnings("rawtypes") Map taskInput) throws Exception;
    }

    private static final int INPUT_DEFAULT_INDEX = 3;

    // vvv XXX: go back to prefixing these with PARAM_ATTR_

    public static final String RENDERER_ATTRIBUTE = "renderer";

    public static final String DEFAULT_ATTRIBUTE = "default";

    public static final String ENV_ATTRIBUTE = "env";

    public static final String DESCRIPTION_ATTRIBUTE = "description";

    public static final String TYPE_ATTRIBUTE = "type";

    public static final String NAME_ATTRIBUTE = "name";
    public static final String CAPTION_ATTRIBUTE = "caption";
    public static final String NAMESPACE_ATTRIBUTE = "namespace";

    public static final String TASK_PROPERTY_TASK_RELATIVE_PATH =
        ".TaskRelativePath";

    public static final String RESOURCES_PATH_TASKS = "tasks";
    public static final String RESOURCES_PATH_COMMON = "common";

    public static final String VARIABLE_NAME_TASK_ID = "TaskId";
    public static final String VARIABLE_NAME_TASK_TITLE = "TaskTitle";
    public static final String VARIABLE_NAME_TASK_DESCRIPTION =
        "TaskDescription";
    public static final String VARIABLE_NAME_TASK_LAYOUT = "TaskLayoutType";
    public static final String VARIABLE_NAME_TASK_VISIBLE = "TaskVisible";
    public static final String
        VARIABLE_NAME_TASK_SECONDS_BEFORE_PROGRESS_SHOWN
        = "TaskSecondsBeforeProgressShown";
    public static final String VARIABLE_NAME_TASK_INPUTS = "TaskInputs";
    public static final String VARIABLE_NAME_TASK_OUTPUTS = "TaskOutputs";
    public static final String VARIABLE_NAME_TASK_LAYOUT_ITEMS =
        "TaskLayoutItems";
    public static final String VARIABLE_NAME_SCHEDULE = "Schedule";

    public static final String VARIABLE_PROPERTY_NATIVE_TYPE = ".NativeType";

    public static String getNativeType(final EnvVarMetadata envVarMetadata) {
        return (String) envVarMetadata.getAuxProperties().get(
            VARIABLE_PROPERTY_NATIVE_TYPE);
    }

    private static Serializable getRequired(
        @SuppressWarnings("rawtypes") final Map map,
        final String fieldName,
        final String context) {
        Serializable ret = (Serializable) map.get(fieldName);
        if (ret == null) {
            throw new InvalidMetadataException(
                "Required attribute missing: " + context + "." + fieldName);
        }
        return ret;
    }

    public static String getScriptRelPath(final TaskMetadata taskMetadata) {
        return (String) taskMetadata
            .getAuxPropery(TASK_PROPERTY_TASK_RELATIVE_PATH);
    }


    private String name;

    private String[] extentions;

    @Getter
    @Setter
    private IHomeService homeService;

    @Getter
    @Setter
    private ITaskRegistry taskRegistry;

    @Getter
    @Setter
    private IDataTypeRegistryService dataTypeRegistryService;

    // private String extentionCaption;


    // private File taskDir;
    // private boolean supportsCommon = true;

    /**
     * @param name eg. R or jython
     * @param extentions eg. R or py
     */
    public ATaskParser(
        final String name, final String... extentions) {
        this.homeService = homeService;
        this.taskRegistry = taskRegistry;
        this.name = name;
        this.extentions = extentions;
        // extentionCaption = Joiner.on(", ").join(extentions);
        // EbrFactory.open(); // for in case
        this.dataTypeRegistryService = dataTypeRegistryService;
    }

    protected final IHomeService getHomeService() {
        return homeService;
    }

    /**
     * {@link #readVariable(String)} gets you to a
     * simple structure {@link ATaskParser},
     * so this converts that to the proper java object.
     * @param nativeType Native Type
     * @param parameterMetadata Parameter Metadata
     * @param simpleStructure {@link ATaskParser}
     * @return a proper java object
     * @throws Exception for in case something goes wrong
     */
    protected final Serializable convertToProperJavaObject(
        final String nativeType, final ParameterMetadata parameterMetadata,
        final Object simpleStructure) throws Exception {
        return dataTypeRegistryService
            .lookupConverter(getName(), nativeType).fromSimpleStructure(
                null, parameterMetadata.getName(), simpleStructure);
    }

    public String getName() {
        return name;
    }

    /**
     * If you call init, you have to call cleanup in a finally block
     * @throws Exception
     */
    public void init() throws Exception {
    }

    /**
     * Override this to add cleanup tasks.
     * This method must be called come hell or highwater.
     * This may be called multiple times
     * @throws Exception
     */
    public void cleanup() throws Exception {
    }



    /**
     * Takes a native object and converts it to a proper java object.
     * @param envId Environment Id
     * @param variableName Variable Name
     * @param nativeObject Native Object
     * @param typeAlias Type Alias
     * @return a proper java Object
     */
    public final Object nativeToJava(final String envId,
        final String variableName,
        final Object nativeObject, final String typeAlias) {
        Object simpleStructure = nativeToSimpleStructure(nativeObject);
        if (simpleStructure == null) {
            return null;
        }
        return dataTypeRegistryService.lookupConverter(getName(), typeAlias)
            .fromSimpleStructure(envId, variableName, simpleStructure);
    }

    /**
     * Convert native object to a simple structure.
     * @param nativeObject a native object
     * @return {@link ATaskParser}
     */
    public abstract Object nativeToSimpleStructure(Object nativeObject);

    /**
     * @param tmd
     * @throws Exception
     */
    protected void parseInputMetaData(final TaskMetadata tmd) throws Exception {
        parseMetaDataList(tmd, VARIABLE_NAME_TASK_INPUTS,
            new ListCallBack() {

                @Override
                public void callback(final TaskMetadata tmd,
                    @SuppressWarnings("rawtypes") final List taskInput) {
                    parseInputMetaData(tmd, taskInput);
                }
            }, new MapCallBack() {

                @Override
                public void callback(final TaskMetadata tmd,
                    @SuppressWarnings("rawtypes") final Map taskInput)
                    throws Exception {
                    parseInputMetaData(tmd, taskInput);
                }
            });
    }

    /**
     * @param tmd
     * @param taskInputO
     */
    private void parseInputMetaData(final TaskMetadata tmd,
        @SuppressWarnings("rawtypes") final List taskInput) {
        InputMetadata imd = new InputMetadata();
        String nativeType =
            setCommonParameterProperties(imd, taskInput,
                VARIABLE_NAME_TASK_INPUTS);
        Object defaultO = null;
        if (taskInput.size() > INPUT_DEFAULT_INDEX) {
            defaultO = taskInput.get(INPUT_DEFAULT_INDEX);
        }
        setDefault(imd, nativeType, defaultO);
        tmd.registerInput(imd);
    }

    /**
     * @param tmd
     * @param taskInputO
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    protected void parseInputMetaData(final TaskMetadata tmd,
        @SuppressWarnings("rawtypes") final Map taskInput)
        throws Exception {
        InputMetadata imd = new InputMetadata();
        String nativeType =
            setCommonParameterProperties(imd, taskInput,
                VARIABLE_NAME_TASK_INPUTS);
        Object defaultO = taskInput.get(DEFAULT_ATTRIBUTE);
        setDefault(imd, nativeType, defaultO);
        if (isActionInput(imd)) {
            Object runOnInit = taskInput.get(Action.RUN_ON_INIT);
            if (runOnInit != null) {
                tmd.setRunOnInit(true);
                try {
                    imd.getAuxProperties().put(Action.RUN_ON_INIT_VALUE,
                        convertToProperJavaObject(nativeType, imd, runOnInit));
                } catch (Exception e) {
                    throw new InvalidMetadataException(
                        "Could not use run on init value for input:"
                            + imd.getName(),
                        e);
                }
            }
        }

        tmd.registerInput(imd);
        // XXX: warn the user that an unknown key has been used..
        // maybe just keep track of what is being checked and then check..
        // will that be annoying for aux properties? should they
        // then register what to alow then?
    }

    @SuppressWarnings("rawtypes")
    protected void parseMetaDataList(final TaskMetadata tmd,
        final String variablName,
        final ListCallBack listCallBack, final MapCallBack mapCallBack)
        throws Exception {
        Object variableValueO = readVariable(variablName);
        if (variableValueO != null) {
            boolean complainedAboutLists = false;
            if (variableValueO instanceof List) {
                List variableValueL = (List) variableValueO;
                for (Object listItemO : variableValueL) {
                    if (listItemO instanceof Map) {
                        mapCallBack.callback(tmd, (Map) listItemO);
                    } else if (listCallBack != null
                        && listItemO instanceof List) {
                        listCallBack.callback(tmd, (List) listItemO);
                        if (!complainedAboutLists) {
                            complainedAboutLists = true;
                            log.warn("[{}] Specifying {} as a list is "
                                + "deprecated, please convert it to a "
                                    + "dictionary.\n{}", new Object[]{
                                        tmd.getTitle(), variablName, tmd.getId()});
                        }
                    } else {
                        String orAList = "";
                        if (listCallBack != null) {
                            orAList = " or a list";
                        }
                        throw new InvalidMetadataException("Unsupported "
                            + variablName + " metadata type for "
                            + tmd.getTitle() + ", expeced a dictionary"
                            + orAList + ".");
                    }
                }
            } else {
                throw new InvalidMetadataException(variablName
                    + " was expected to be a list.");
            }
        }
    }

    /**
     * @param tmd
     * @throws Exception
     */
    protected void parseOutputMetaData(final TaskMetadata tmd)
        throws Exception {
        parseMetaDataList(tmd, VARIABLE_NAME_TASK_OUTPUTS,
            new ListCallBack() {

                @Override
                public void callback(final TaskMetadata tmd,
                    @SuppressWarnings("rawtypes") final List taskInput) {
                    OutputMetadata omd = new OutputMetadata();
                    setCommonParameterProperties(omd, taskInput,
                        VARIABLE_NAME_TASK_OUTPUTS);
                    tmd.registerOutput(omd);
                }
            }, new MapCallBack() {

                @Override
                public void callback(final TaskMetadata tmd,
                    @SuppressWarnings("rawtypes") final Map taskInput)
                    throws Exception {
                    parseOutputMetaData(tmd, taskInput);
                }
            });
    }

    private void parseTaskLayoutItems(final TaskMetadata tmd)
        throws Exception {
        final List<Map<String, Serializable>> layoutItems =
            tmd.getLayoutItems();
        parseMetaDataList(tmd, VARIABLE_NAME_TASK_LAYOUT_ITEMS, null,
            new MapCallBack() {
                @SuppressWarnings({"unchecked", "rawtypes"})
                @Override
                public void callback(final TaskMetadata tmd,
                    final Map layoutItem)
                    throws Exception {
                    // TODO maybe we should look up the renderer here
                    // and check the supported attributes and types?!
                    layoutItems.add(layoutItem);
                }
            });
    }

    protected TaskMetadata parseTaskMetaData(final String taskRelPath,
        final Class<?> type)
        throws Exception {
        String id = readRequiredVariable(VARIABLE_NAME_TASK_ID);
        String title = readRequiredVariable(VARIABLE_NAME_TASK_TITLE);
        String description = readVariable(VARIABLE_NAME_TASK_DESCRIPTION);

        TaskMetadata tmd = new TaskMetadata(id, title, description, type);
        tmd.setAuxPropery(TASK_PROPERTY_TASK_RELATIVE_PATH, taskRelPath);

        // may be null
        tmd.setLayout((String) readVariable(VARIABLE_NAME_TASK_LAYOUT));

        Boolean visible = readVariable(VARIABLE_NAME_TASK_VISIBLE);
        if (visible != null) {
            tmd.setVisible(visible);
        }

        Number msbps = readVariable(
            VARIABLE_NAME_TASK_SECONDS_BEFORE_PROGRESS_SHOWN);
        if (msbps != null) {
            tmd.setSecondsBeforeProgressShown(msbps.doubleValue());
        }
        tmd.setSchedule(this.<String> readVariable(VARIABLE_NAME_SCHEDULE));

        parseInputMetaData(tmd);
        parseOutputMetaData(tmd);
        parseTaskLayoutItems(tmd);
        return tmd;
    }

    /**
     * @param scriptRelPath
     * @param taskAbsPath
     * @return
     * @throws Exception
     */
    public TaskMetadata parseTaskMetaDataFromFile(final String taskRelPath,
        final String taskAbsPath) throws Exception {
        Class<?> type =
            startParseTaskMetaDataFromFile(taskRelPath, taskAbsPath);
        TaskMetadata ret = parseTaskMetaData(taskRelPath, type);
        stopParseTaskMetaData();
        return ret;
    }

    public TaskMetadata parseTaskMetaDataFromString(
        final String taskRelPath,
        final String script) throws Exception {
        Class<?> type =
            startParseTaskMetaDataFromString(taskRelPath, script);
        TaskMetadata ret = parseTaskMetaData(taskRelPath, type);
        stopParseTaskMetaData();
        return ret;
    }

    /**
     * @param typeString
     * @return
     */
    protected final String parseTypeString(final String typeString) {
        try {
            return dataTypeRegistryService.lookupType(getName(), typeString);
        } catch (java.lang.IllegalArgumentException e) {
            throw new InvalidMetadataException(
                "Invalid type: " + typeString, e);
        }
    }

    public abstract Object readNativeVariable(String variableName);

    /**
     * @param variableName
     * @return value
     * @throws InvalidMetadataException if the variable is null
     */
    @SuppressWarnings("unchecked")
    protected <T> T readRequiredVariable(final String variableName) {
        T ret = (T) readVariable(variableName);
        if (ret == null) {
            throw new InvalidMetadataException(
                "Required variable is not specified: " + variableName);
        }
        return ret;
    }

    /**
     * Should return a simple structure {@link ATaskParser}.
     * When you don't know, resist the temptation to guess
     * and rather pass back the native object so that higher layers can
     * process
     * it further if need be. The user may expect to receive a object of a
     * specific type back, so you should cast it for him.
     * @param variableName
     * @return value or null
     */
    @SuppressWarnings("unchecked")
    public final <T> T readVariable(final String variableName) {
        Object nativeObject = readNativeVariable(variableName);
        if (nativeObject != null) {
            return (T) nativeToSimpleStructure(nativeObject);
        }
        return null;
    }

    public final void registerTasks() throws Exception {
        // log.debug("Start registering {} tasks.", name);
        init();
        try {
            Stopwatch sw = new Stopwatch();
            int count = 0;
            for (HomeFile homeFile: getHomeService().findFiles(
                    new HomeFileSpec(TASKS), extentions)) {
                String taskRelPath = homeFile.getRelPath();
                // log.debug("Registering {} task: {}", name, taskRelPath);
                String taskAbsPath = homeFile.getAbsolutePath();

                try {
                    TaskMetadata taskMetaData =
                        parseTaskMetaDataFromFile(taskRelPath, taskAbsPath);
                    taskRegistry.registerTask(taskMetaData);
                    count++;
                } catch (Throwable e) {
                    log.error("Could not register task: " + taskRelPath, e);
                }
            }

            log.debug("Registering {} {} task(s) took {} ms", new Object[] {
                count, name, sw.getTotalMilliSeconds()});
        } finally {
            cleanup();
        }
    }

    private String safeToString(final Object obj) {
        if (obj == null) {
            return null;
        }
        return obj.toString();
    }

    @SuppressWarnings("rawtypes")
    private String setCommonParameterProperties(final ParameterMetadata md,
        final List list, final String context) {

        int size = list.size();
        if (size < 2) {
            throw new InvalidMetadataException(
                "Required at least 2 attributes: " + context);
        }

        String typeString = list.get(0).toString();
        md.setTypeName(parseTypeString(typeString));
        md.setName(list.get(1).toString());

        // assign optional attributes
        if (size > 2) {
            md.setDescription(list.get(2).toString());
        }
        md.getAuxProperties().put(VARIABLE_PROPERTY_NATIVE_TYPE, typeString);
        return typeString;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private String setCommonParameterProperties(final ParameterMetadata md,
        final Map map, final String context) {
        md.setName(getRequired(map, NAME_ATTRIBUTE, context).toString());
        String typeString =
            getRequired(map, TYPE_ATTRIBUTE, context).toString();
        md.setTypeName(parseTypeString(typeString));

        // assign optional attributes
        md.setCaption(safeToString(map.get(CAPTION_ATTRIBUTE)));
        md.setDescription(safeToString(map.get(DESCRIPTION_ATTRIBUTE)));
        md.setRenderer(safeToString(map.get(RENDERER_ATTRIBUTE)));
        Object namespace = map.get(NAMESPACE_ATTRIBUTE);
        if (namespace != null) {
            md.setNamespace(namespace.toString());
        }

        Object env = map.get(ENV_ATTRIBUTE);
        if (env != null) {
            md.setEnvironment(ParameterEnvironment
                .fromString(env.toString()));
        }

        Map<String, Serializable> auxProperties = md.getAuxProperties();
        // XXX: maybe add one by one for better errror messages
        auxProperties.putAll(map);
        auxProperties.put(VARIABLE_PROPERTY_NATIVE_TYPE, typeString);
        return typeString;
    }

    private void setDefault(final InputMetadata imd,
        final String nativeType,
        final Object defaultO) {
        boolean isOptional = defaultO != null;
        imd.setOptional(isOptional);
        if (isOptional) {
            try {
                imd.setDefaultValue(convertToProperJavaObject(nativeType,
                    imd, defaultO));
            } catch (Exception e) {
                throw new InvalidMetadataException(
                    "Could not use default value for input:" + imd.getName(),
                    e);
            }
        }
    }

    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @param taskRelPath
     * @param taskAbsPath
     * @return the class which must be instantiated for this task
     * @throws Exception
     */
    protected abstract Class<?> startParseTaskMetaDataFromFile(
        String taskRelPath, String taskAbsPath) throws Exception;

    /**
     * You should overwrite this if your scripting api supports parsing
     * strings
     * @param taskRelPath
     * @param script
     * @return
     * @throws Exception
     */
    protected Class<?> startParseTaskMetaDataFromString(
        final String taskRelPath,
        final String script) throws Exception {
        File tempfile = File.createTempFile("ParseTaskMetaDataFromString_"
            + getName(), extentions[0]);
        tempfile.deleteOnExit();
        FileUtils.writeStringToFile(tempfile, script);
        return startParseTaskMetaDataFromFile(taskRelPath, tempfile
            .getAbsolutePath());
    }

    /**
     * You can clean up your resources initialised in start here.
     */
    protected void stopParseTaskMetaData() {
    }

    /**
     * set simpleStructure value in native api
     * @param envId
     * @param envVarMetadata
     * @param object
     */
    public final void write(final String envId,
        final EnvVarMetadata envVarMetadata,
        final Serializable object) {
        write(envId, envVarMetadata.getName(),
            getNativeType(envVarMetadata), object);
    }

    /**
     * set value in native api
     * @param envId
     * @param variableName
     * @param typeAlias
     * @param object
     */
    public final void write(final String envId, final String variableName,
        final String typeAlias, final Serializable object) {
        Object simpleStructure =
            getSimpleStructure(envId, variableName, typeAlias, object);
        writeSimpleStructure(envId, variableName, simpleStructure);
    }

    private Object getSimpleStructure(final String envId,
        final String variableName, final String typeAlias,
        final Serializable object) {
        Object simpleStructure = null;
        if (object != null) {
            ITypeConverter<Serializable> converter = dataTypeRegistryService
                .lookupConverter(getName(), typeAlias);
            simpleStructure =
                converter.toSimpleStructure(envId, variableName, object);
        }
        return simpleStructure;
    }

    public Collection<String> getExampleResourcePaths()
        throws ClassNotFoundException {
        String examplePackage = getExamplesResourcePath();
        Collection<String> examples =
            ResourceUtils.getResourcesForPackage(examplePackage);
        return examples;
    }

    /**
     * @return
     */
    private String getExamplesResourcePath() {
        return IEbrServer.RESOURCE_PACKAGE
            + "/" + getNameAsPackageName() + "/"
            + EXAMPLE_TASK_PACKAGE;
    }

    public String getNameAsPackageName() {
        return StringUtils.camelCaseToUnderscoreSeparated(name).toLowerCase();
    }

    /**
     * set simpleStructure value in native api
     * @param envId
     * @param variableName
     * @param simpleStructure
     */
    public abstract void writeSimpleStructure(String envId,
        String variableName, Object simpleStructure);

    @Override
    protected void finalize() throws Throwable {
        cleanup();
    }

    public void
        parseOutputMetaData(final TaskMetadata tmd, final Map taskInput) {
        OutputMetadata omd = new OutputMetadata();
        setCommonParameterProperties(omd, taskInput,
            VARIABLE_NAME_TASK_OUTPUTS);
        tmd.registerOutput(omd);
    }

}
