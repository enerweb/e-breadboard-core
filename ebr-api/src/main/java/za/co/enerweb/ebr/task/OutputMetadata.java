package za.co.enerweb.ebr.task;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class OutputMetadata extends ParameterMetadata {
    private static final long serialVersionUID = 1L;

    public OutputMetadata() {
        super();
    }

    public OutputMetadata(final Class<? extends Serializable> type,
        final String name, final String description) {
        super(type, name, description);
    }

}
