package za.co.enerweb.ebr.task;

import javax.ejb.ScheduleExpression;

import lombok.Data;
import za.co.enerweb.ebr.util.ScheduleExpressionUtil;

/**
 * min hour dayofmonth month dayofweek
 * eg: 0 5 * * 1
 */
@Data
public class Schedule {
    private String schedule;
    private ScheduleExpression scheduleExpression;

    public Schedule(String schedule) {
        this.setSchedule(schedule);
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
        this.scheduleExpression = ScheduleExpressionUtil.parse(schedule);
    }

    @Override
    public String toString() {
        return schedule;
    }

    public boolean isConfigured() {
        return scheduleExpression != null;
    }
}
