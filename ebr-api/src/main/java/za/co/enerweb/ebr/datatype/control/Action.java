package za.co.enerweb.ebr.datatype.control;

import java.io.Serializable;

import lombok.Data;
import za.co.enerweb.ebr.datatype.ITypeConverter;
import za.co.enerweb.ebr.task.InputMetadata;

/**
 * For supporting multiple actions in one task.
 */
@Data
public class Action implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final String TYPE_ALIAS = "action";
    public static final String RUN_ON_INIT = "runOnInit";
    public static final String RUN_ON_INIT_VALUE = "RUN_ON_INIT_VALUE";

    private Serializable value; // value of button that has been pressed

    public Action() {
    }

    public Action(Serializable value) {
        super();
        this.value = value;
    }

    public static boolean isActionInput(InputMetadata imd) {
        return imd.getTypeName().equals(
            Action.class.getName());
    }

    public static Serializable getRunOnInitActionValue(InputMetadata imd) {
        return imd.getAuxProperty(RUN_ON_INIT_VALUE);
    }

    public static class Converter implements ITypeConverter<Action> {
        /* (non-Javadoc)
         * @see za.co.enerweb.ebr.api.datatype.ITypeConverter
         *  #fromSimpleStructure(java.lang.Object)
         */
        @Override
        public Action fromSimpleStructure(String envId, String variableName,
                Object simpleStructure) {
            return new Action((Serializable)simpleStructure);
        }

        /* (non-Javadoc)
         * @see za.co.enerweb.ebr.api.datatype.ITypeConverter
         *  #toSimpleStructure(java.lang.Object)
         */
        @Override
        public Object toSimpleStructure(String envId, String variableName,
            Action specificObject) {
            return specificObject.value;
        }

        /* (non-Javadoc)
         * @see za.co.enerweb.ebr.api.datatype.ITypeConverter#getType()
         */
        @Override
        public String getType() {
            return Action.class.getName();
        }
    }
}
