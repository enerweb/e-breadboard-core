package za.co.enerweb.ebr.datatype;

import java.io.Serializable;

/**
 * @param <T> The proper java type we can convert to and from
 */
public interface ITypeConverter<T extends Serializable> extends Serializable {

    /**
     * @param envId may be null
     * @param variableName
     * @param simpleStructure
     * @return specific Object
     */
    T fromSimpleStructure(String envId, String variableName,
        Object simpleStructure);

    /**
     * @param envId may be null
     * @param variableName
     * @param specificObject
     * @return simpleStructure
     */
    Object toSimpleStructure(String envId, String variableName,
        T specificObject);

    /**
     * The proper java type we can convert to and from
     * @return
     */
    String getType();
}
