package za.co.enerweb.ebr.task;

import org.slf4j.Logger;

import za.co.enerweb.ebr.env.IEnvironment;

public interface ITask {

    void setEbrHomePath(String ebrHomePath);

    void setWorkdir(String workdir);

    void run() throws Exception;

    String getEbrHomePath();

    /**
     * Get the directory of the output environment, where tasks can
     * put auxiliary files, especially for data resource outputs.
     * @return
     */
    String getWorkdir();

    IEnvironment getInputEnv();

    void setInputEnv(final IEnvironment inputEnv);

    IEnvironment getOutputEnv();

    void setOutputEnv(final IEnvironment outputEnv);

    Logger getLogger();

    void setLogger(final Logger logger);

    TaskMetadata getTaskMetadata();

    void setTaskMetadata(final TaskMetadata taskMetadata);

    TaskExecutionMetadata getTaskExecutionMetadata();

    void setTaskExecutionMetadata(TaskExecutionMetadata taskExecutionMetadata);

}
