package za.co.enerweb.ebr.task_layout;

import java.io.Serializable;

/**
 * All layouts must implement this.
 */
public interface ITaskLayout extends Serializable {
    void setTaskLayoutMetadata(TaskLayoutMetadata taskLayoutMetadata);
}
