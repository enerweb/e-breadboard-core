package za.co.enerweb.ebr.datatype.dataresource;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FilenameUtils;

import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.env.EnvResourceMetadata;
import za.co.enerweb.ebr.env.IEnvironment;
import za.co.enerweb.ebr.env.IEnvironmentManager;
import za.co.enerweb.ebr.exceptions.ServiceException;
import za.co.enerweb.toolbox.validation.ValidationException;

@NoArgsConstructor
@EqualsAndHashCode(of = {"envId", "resourceId"})
@Slf4j
public class AbstractDataResource implements IDataResource {

    private String envId;
    private String resourceId;
    private IDataSource dataSource;

    @SneakyThrows
    protected IEnvironmentManager getEnvManager() {
        return EbrFactory.getEnvironmentManager();
    }

    public AbstractDataResource(
        final IDataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * envId may be null if the path is absolute
     */
    public AbstractDataResource(
        final String envId, final String path) {
        this.envId = envId;
        setResourceByPath(envId, path);
    }

    /**
     * write inputstream to an environment file
     * (the stream may disapear if it comes from a web service or something)
     */
    @SneakyThrows
    public AbstractDataResource(String envId, String relpath,
        InputStream data) {
        this.envId = envId;
        // this.dataSource = new InputStreamDataSource(fileName, data);
        File environmentDir = getEnvManager().getEnvironmentDir(envId);

        File file = new File(environmentDir, relpath);

        Files.copy(data, file.toPath());

        // @Cleanup FileOutputStream fos = new FileOutputStream(file);
        // IOUtils.copy(data, fos);

        dataSource = new EnvFileDataSource(envId, file, relpath);
    }

    /**
     * Adds it to the client env and on the server
     */
    public void addToEnvironmentIfNeeded(final IEnvironment env)
        throws ValidationException {
        if (isNotUsingEnvResesource(false)) {
            env.putResource(dataSource);
            addToEnvironmentIfNeeded(env.getId());
        }
    }

    /**
     * Just adds it on the server.
     */
    public void addToEnvironmentIfNeeded(final String envId)
    throws ValidationException {
        if (isNotUsingEnvResesource(false)) {
            this.envId = envId;
            // set it in on the server too!
            resourceId = getEnvManager().putResource(envId, dataSource);
            dataSource = null;
        }
    }


    // XXX: we may want to move this functionality into the envManager again.
    /**
     * @param envId
     * @param path EnvRelative or absolute is allowed
     * @return
     */
    protected void setResourceByPath(final String envId, final String path) {
        if (envId != null) {
            File environmentDir = getEnvManager().getEnvironmentDir(envId);
            // first check if its an env file
            File file = new File(environmentDir, path);
            if (file.exists()) {
                dataSource = new EnvFileDataSource(envId, file, path);
                return;
            }
        }
        File file = new File(path);
        if (file.exists()) {
            dataSource = new LocalFileDataSource(envId, path);
            return;
        }
        throw new ServiceException("Could not find file: " + path);
    }

    /*
     * (non-Javadoc)
     * @see za.co.enerweb.ebr.datatype.dataresource.IDataSource
     * #getResourceMetadata()
     */
    @Override
    public EnvResourceMetadata getResourceMetadata()
        throws ValidationException {
        if (isNotUsingEnvResesource()) {
            return dataSource.getResourceMetadata();
        }
        return getEnvManager().getResourceMetadata(envId, resourceId);
    }

    private boolean isNotUsingEnvResesource() throws ValidationException {
        return isNotUsingEnvResesource(true);
    }

    private boolean isNotUsingEnvResesource(boolean warn
    ) throws ValidationException {
        if (envId == null || resourceId == null) {
            if (dataSource == null) {
                throw new ValidationException("Resource has not been saved in "
                    + "an environment "
                    + "and no datasource has been set: " + this);
            } else if (warn) {
                log.warn("Resource has not been saved in an environment: "
                    + this);
            }
            return true;
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see za.co.enerweb.ebr.datatype.dataresource.IDataSource#asAbsolutePath()
     */
    @Override
    public String getAbsolutePath() throws IOException, ValidationException {
        String path;
        if (isNotUsingEnvResesource()) {
            path = dataSource.asAbsolutePath();
        } else {
            path = getEnvManager().getResourceAsAbsolutePath(envId, resourceId);
        }
        return FilenameUtils.normalize(path);
    }

    public File getFile() throws ValidationException, IOException {
        return new File(getAbsolutePath());
    }

    /*
     * (non-Javadoc)
     * @see za.co.enerweb.ebr.datatype.dataresource.IDataSource#asBytes()
     */
    @Override
    public byte[] getBytes() throws IOException, ValidationException {
        if (isNotUsingEnvResesource()) {
            return dataSource.asBytes();
        }
        return getEnvManager().getResourceAsBytes(envId, resourceId);
    }

    /*
     * (non-Javadoc)
     * @see za.co.enerweb.ebr.datatype.dataresource.IDataResource
     * #asInputStream()
     */
    @Override
    public InputStream getInputStream() throws IOException,
        ValidationException {
        if (isNotUsingEnvResesource()) {
            return dataSource.asInputStream();
        }
        return getEnvManager().getResourceAsStream(envId, resourceId);
    }

    public boolean isSet() {
        return (envId != null && resourceId != null) || dataSource != null;
    }

    public IDataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(final IDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public String getEnvId() {
        return envId;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setEnvId(final String envId) {
        this.envId = envId;
    }

    public void setResourceId(final String resourceId) {
        this.resourceId = resourceId;
    }

    @Override
    public String toString() {
        if (dataSource != null) {
            return dataSource.toString();
        }
        return envId + "/" + resourceId;
    }

}
