package za.co.enerweb.ebr.server;

import lombok.Getter;

/**
 * Plugins etc should limit their use of this to the directories they own.
 */
public enum HomeFileCategory {
    CACHE_PER_TECHNOLOGY(1),
    /**
     * Directory used for storing embedded dbs.
     * It takes a technology name parameter
     */
    DB(1),
    /**
     * storage environment parent dir
     */
    ENVIRONMENTS,
    /**
     * user configurations go here
     */
    ETC,
    /**
     * Takes the technology and the sub-technology/group  as parameters
     */
    FRONTEND(2),
    /**
     * FIXME: rename to TASK_EXECUTIONS or something
     */
    JOBS,
    LIB_PER_TECHNOLOGY(1),
    LOGS,
    RESOURCES,
    RESOURCES_PER_TECHNOLOGY(1),
//    /**
//     * This is the root of the home. You should not be using it.
//     */
//    ROOT,
    /**
     * Considers all tasks equal
     */
    TASKS,
    /**
     * First path element should always be the technology name.
     */
    TASKS_PER_TECHNOLOGY(1),
    TASK_COMMON,
    TASK_COMMON_PER_TECHNOLOGY(1),
    TASK_PERSPECTIVES,
    /**
     * stuff in here gets deleted after 24 hours (last modification time)
     */
    TEMP,
    /**
     * stuff in here gets deleted after a couple of months
     * (if it follows the naming conventions)
     * Maybe it should be zipped after 24 hours.
     * (apply to old files not ending in .zip)
     */
    TRASH,
    SHARE,
    SESSIONS,
    /**
     * For storing data and other variable content (but not for caching).
     * This is mostly reserved for core components.
     * This takes the component as a parameter.
     */
    VAR_PER_COMPONENT(1),
    WEB_CONTENT;

    @Getter
    private int paramCount = 0;

    private HomeFileCategory() {
    }

    private HomeFileCategory(int paramCount) {
        this.paramCount = paramCount;
    }

    public HomeFileSpec spec(String... parametersAndPathElements) {
        return new HomeFileSpec(this, parametersAndPathElements);
    }
}
