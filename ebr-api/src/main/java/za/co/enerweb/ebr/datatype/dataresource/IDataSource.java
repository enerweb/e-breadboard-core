package za.co.enerweb.ebr.datatype.dataresource;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import za.co.enerweb.ebr.env.EnvResourceMetadata;

/**
 * datasource api
 */
public interface IDataSource extends Serializable {
    String asAbsolutePath() throws IOException;

    byte[] asBytes() throws IOException;

    InputStream asInputStream() throws IOException;

    String getResourceId();

    EnvResourceMetadata getResourceMetadata();

}
