package za.co.enerweb.ebr.env;

import java.io.IOException;

import javax.ejb.Local;
import javax.ejb.Remote;

/**
 * Helper for providing a input stream reading from the env.
 */
@Local
@Remote
public interface IEnvironmentValueReader {
    public void init(final String envId,
        final String resourceId) throws IOException;
    public byte[] read(final int len) throws IOException;

    void close() throws IOException;
}
