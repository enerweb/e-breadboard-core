package za.co.enerweb.ebr;

import static java.lang.String.format;
import static javax.ejb.embeddable.EJBContainer.MODULES;
import static za.co.enerweb.toolbox.io.OpenPortUtils.getAvailablePort;
import static za.co.enerweb.toolbox.io.OpenPortUtils.isPortAvailable;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import javax.naming.NamingException;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EbrEmbeddedTomee {
    private static final String PROPERTY__HTTP_PORT =
        "tomee.ejbcontainer.http.port";
    private Context context;
    private EJBContainer container;
    private Map<String, Integer> ports = new HashMap<>();

    @Getter(// lazy = true,
        value = AccessLevel.PRIVATE)
    private final static EbrEmbeddedTomee instance = new EbrEmbeddedTomee();

    /**
     * launch with
     * -Djavax.ejb.embeddable.modules=/<project-path>/target/<name>-<version>"/>
     */
    public static void main(String[] args) throws Exception {
        // can maybe take a timeout arg in future if needed
        getInstance().runAndWait();
        Thread.sleep(200);
    }

    @SneakyThrows
    public static void start(String[] args) {
        getInstance().run();
    }

    @SneakyThrows
    public static void stop(String[] args) {
        getInstance().close();
    }

    private int getProperty(String name, int defaultValue) {
        // System.out.println(name + "=" + System.getProperty(name));
        return Integer.parseInt(
            System.getProperty(name, "" + defaultValue));
    }

    public synchronized void run() throws Exception {
        ports.put("httpejbd", getProperty(PROPERTY__HTTP_PORT, 8080));
        ports.put("admin", 4200);
        ports.put("ejbd", 4201);
        ports.put("ejbds", 4203);
        checkPorts();

        Properties p = new Properties(System.getProperties());
        loadProperties(p, "embedded-openejb.properties");
        loadProperties(p, "log4j.properties");

        p.setProperty(EJBContainer.PROVIDER, "tomee-embedded");

        String webappBase = getWebappBase();
        log.info("Starting the following webapp: " + webappBase);
        p.setProperty(MODULES, webappBase);

        p.setProperty("openejb.log.factory", "slf4j");
        p.setProperty("openejb.logger.external", "true");

        for (Entry<String, Integer> entry : ports.entrySet()) {
            p.setProperty(entry.getKey() + ".port", ""
                + entry.getValue());
        }
        p.setProperty(PROPERTY__HTTP_PORT,
            "" + getHttpPort());
        p.list(System.out);
        try {
            container = EJBContainer.createEJBContainer(p);
            context = container.getContext();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Could create container :(", e);
            close();
        }
    }

    public synchronized void runAndWait() throws Exception {
        run();
        if (context != null) {
            log.info("Running Tomee until you kill it");
            try {
                while (true) {
                    this.wait(); // sometimes gets spurious notifies :(
                }
            } catch (Exception e) {
                log.error("Could not wait any more :(", e);
                e.printStackTrace();
            } finally {
                close();
            }
        }
    }

    @SneakyThrows
    public void close() throws NamingException {
        if (context != null) {
            context.close();
            context = null;
        }
        if (container != null) {
            container.close();
            container = null;
        }

        // for some reason thease ports are not getting released :(
        for (Entry<String, Integer> entry : ports.entrySet()) {
            Integer port = entry.getValue();
            for (int i = 1; i < 5 && !isPortAvailable(port); i++) {
                log.debug(i + ") waiting for " + entry.getKey()
                    + ".port to close: "
                    + entry.getValue());
                Thread.sleep(200 * i);
            }
        }
    }

    public synchronized void runAndWait(int seconds) throws Exception {
        run();
        log.info(format("Running Tomee for %s seconds.",
            seconds));
        try {
            for (int i = 0; i < seconds; i++) {
                if (i > 0 && i % 10 == 0) {
                    log.info(format("%s/%s seconds", i, seconds));
                }
                Thread.sleep(1000);
            }
            log.info("Done");
        } catch (Exception e) {
            e.printStackTrace();
        }
        close();
        Thread.sleep(200);
    }

    private String getWebappBase() {
        String ret = System.getProperty(MODULES);
        if (ret == null) {
            ret = ResourceBundle.getBundle("maven").getString("webappDir");
            if (ret == null) {
                throw new RuntimeException(
                    "Please include a maven.properties file "
                        + "in your project specify which defines a webappDir OR"
                        + "specify -D" + MODULES
                        + "=<path to your extracted war file>");
            }
        }
        return ret;
    }

    protected static void loadProperties(final Properties properties,
        final String resourcePath) {
        try {
            ClassLoader classLoader = Thread.currentThread()
                .getContextClassLoader();
            InputStream resourceAsStream = classLoader
                .getResourceAsStream(resourcePath);
            if (resourceAsStream == null) {

                log.info("Could not load properties from {}." +
                    resourcePath);
                log.info("Thread.currentThread(): "
                    + Thread.currentThread());
                log.info("Thread.currentThread().getName(): "
                    + Thread.currentThread().getName());
                System.out
                    .println("Thread.currentThread().getContextClassLoader(): "
                        + Thread.currentThread().getContextClassLoader());
                // log.info(
                // "EbrFactory.class.getClassLoader(): "
                // + EbrFactory.class.getClassLoader());
                return;
            }

            properties.load(resourceAsStream);
        } catch (IOException e) {
            log.info("Could not load properties from " + resourcePath
                + ".");
            e.printStackTrace();
        }
    }

    private void checkPorts() {
        for (Entry<String, Integer> entry : ports.entrySet()) {
            int actualPort = getAvailablePort(entry.getValue());
            entry.setValue(actualPort);
            log.debug(entry.getKey() + ".port = " + actualPort);
        }
    }

    public int getHttpPort() {
        return ports.get("httpejbd");
    }
}
