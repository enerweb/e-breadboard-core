package za.co.enerweb.ebr.adf.entities;


import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true, of={})
@ToString(callSuper=true, of={})
@Entity
@Table(name = "I_HIERARCHY_NODE_PAR")
public class IHierarchyNodePar extends IbInstanceParameterNorm {
    private static final long serialVersionUID = 1L;

    @JoinColumn(name = "HIERARCHY_NODE_ID",
        referencedColumnName = "ID")
    @ManyToOne
    private IHierarchyNode iHierarchyNode;
}
