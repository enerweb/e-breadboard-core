package za.co.enerweb.ebr.adf.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import za.co.enerweb.ebr.adf.data_types.AdfResourceUrl;
import za.co.enerweb.ebr.adf.data_types.DtAdfResourceUrl;
import za.co.enerweb.ebr.adf.data_types.DtText;

@Data
@EqualsAndHashCode(callSuper = true, of = { })
@ToString(callSuper = true, of = { })
@AdfInstanceTypeDef(entity = EAuxInfo.class)
public class TInfoDocument extends AdfInstance {
    public static final String KEY = deriveKey(TInfoDocument.class);

    @AdfParamDef(type = DtAdfResourceUrl.class)
    public AdfResourceUrl document;

    @AdfParamDef(type = DtText.class)
    public String documentSource;

    @AdfHierarchyChild(entity = EHierarchy.class,
        key = EHierarchy.INFO_DOCUMENT_HIER_KEY)
    private List<AdfInstance> linkedInstances = new ArrayList<AdfInstance>();

}
