package za.co.enerweb.ebr.adf.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import za.co.enerweb.ebr.adf.data_types.DtBoolean;
import za.co.enerweb.ebr.adf.data_types.DtText;

@Data
@EqualsAndHashCode(callSuper = true, of = { })
@ToString(callSuper = true, of = { })
@AdfInstanceTypeDef(entity = EAuxInfo.class)
public class TInfoSql extends AdfInstance {
    public static final String KEY = deriveKey(TInfoSql.class);

    @AdfParamDef(type = DtText.class)
    public String sql;

    @AdfParamDef(type = DtText.class)
    public String sqlUnit;

    /**
     * @deprecated we should not rely on this..
     */
    @AdfParamDef(type = DtBoolean.class)
    public Boolean sqlUniqueId;

}
