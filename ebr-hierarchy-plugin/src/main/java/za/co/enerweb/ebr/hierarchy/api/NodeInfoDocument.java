package za.co.enerweb.ebr.hierarchy.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import za.co.enerweb.ebr.adf.data_types.AdfResourceUrl;

@Data
@AllArgsConstructor
public class NodeInfoDocument implements INodeInfo {
    protected HierarchyNode sourceOfInfo;
    private String sourceOfDocument;
    private AdfResourceUrl resource;
}
