package za.co.enerweb.ebr.hierarchy;

import za.co.enerweb.ebr.adf.AdfFactory;

public abstract class HierarchyAdfFactory extends AdfFactory {

    private static final long serialVersionUID = 1L;

    // vvv extensions

    @Override
    public Class<? extends HierarchyDbLayer> getDefaultDbLayerClass() {
        return HierarchyDbLayer.class;
    }

    public HierarchyDbLayer getDbLayer() {
        return (HierarchyDbLayer) super.getDbLayer();
    }
}
