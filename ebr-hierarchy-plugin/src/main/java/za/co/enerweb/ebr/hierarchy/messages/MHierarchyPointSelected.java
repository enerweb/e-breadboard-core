package za.co.enerweb.ebr.hierarchy.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import za.co.enerweb.ebr.hierarchy.api.HierarchyNode;
import za.co.enerweb.toolbox.vaadin.messaging.Message;

@Data
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
public class MHierarchyPointSelected extends Message {

    private HierarchyNode []selected;
    private boolean initialLoad;//some components may not want to listen to these
}
