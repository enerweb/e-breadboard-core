package za.co.enerweb.ebr.adf.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true, of={})
@ToString(callSuper=true, of={})
@Entity
@Table(name = "H_HIERARCHY",
    uniqueConstraints = {
        @UniqueConstraint(columnNames = {"HIERARCHY_ID",
            "PARENT_TYPE_ID", "PARENT_ID", "CHILD_TYPE_ID", "CHILD_ID",
            "START_DATETIME", "END_DATETIME"})})
public class HHierarchy extends HierarchyRelationship {

    // deriveRelationhsipKey(
    // TTypeDummy1.class, TTypeDummy2.class);
    private static final long serialVersionUID = 1L;


}
