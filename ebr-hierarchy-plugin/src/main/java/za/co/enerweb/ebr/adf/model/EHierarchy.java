package za.co.enerweb.ebr.adf.model;


public class EHierarchy extends AdfHierarchyEntity {
    public static final String KEY = deriveKey(EHierarchy.class);

    public static final String INFO_DOCUMENT_HIER_KEY = "INFO_DOCUMENT";
}
