package za.co.enerweb.ebr.hierarchy.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SQLDataSourceNodeInfo implements INodeInfo {
    protected HierarchyNode sourceOfInfo;
    private String caption;
    private String sql;
    private String unit;
    private boolean needsUniqueID;

    public SQLDataSourceNodeInfo(String caption, String sql, String unit,
        boolean needsUniqueID) {
        this.caption = caption;
        this.sql = sql;
        this.unit = unit;
        this.needsUniqueID = needsUniqueID;
    }
}
