package za.co.enerweb.ebr.hierarchy.api;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NodeInfoData implements INodeInfo{
    protected HierarchyNode sourceOfInfo;
    private int uniqueID;
    private String name;
    private String otherStuff;
}
