package za.co.enerweb.ebr.hierarchy.api;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NodeInfoGIS implements INodeInfo{

    protected HierarchyNode sourceOfInfo;
    private double log;
    private double lat;
}
