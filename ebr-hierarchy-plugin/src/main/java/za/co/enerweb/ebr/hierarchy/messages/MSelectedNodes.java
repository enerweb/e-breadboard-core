package za.co.enerweb.ebr.hierarchy.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import za.co.enerweb.ebr.hierarchy.api.HierarchyNode;
import za.co.enerweb.toolbox.vaadin.messaging.Message;
@Data
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
public class MSelectedNodes extends Message {

    private HierarchyNode []currentlySelected;
    private MGetSelectedNodes inResponseTo;

}
