package za.co.enerweb.ebr.adf.entities;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Used to store auxiliary information that can be linked to any instances via
 * hierarchies
 */

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, of = { })
@ToString(callSuper = true, of = { })
@Entity
@Table(name = "I_AUX_INFO")
public class IAuxInfo extends IbInstance {
    private static final long serialVersionUID = 1L;

    // public static final String ENTITY_AUX_INFO = "AUX_INFO";

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "iAuxInfo")
    private Collection<IAuxInfoPar> iAuxInfoParCollection;
}
