package za.co.enerweb.ebr.hierarchy.api;


public interface INodeInfo {


    /**
     * This is the node that is set in the info when the info is pulled.
     * 1) It does not need to be stored in the database
     * 2) It does represent all the HierarchyNode's this info is associated to
     *
     * Each implementation of the
     */


    HierarchyNode getSourceOfInfo();

}
