
package za.co.enerweb.ebr.hierarchy.api;

import lombok.Data;
import za.co.enerweb.ebr.adf.entities.CmHierarchyDef;

// FIXME: rename to CmHierarchyTypeDef
@Data
public class Hierarchy extends HierarchyNode {

    private static final long serialVersionUID = 1L;

    private CmHierarchyDef cmHierarchyDef;
    private String key;

    public Hierarchy(String uniqueId, CmHierarchyDef cmHierarchyDef,
        String key, String caption){
        super(null,uniqueId, caption, -1, true);
        this.cmHierarchyDef = cmHierarchyDef;
        this.key = key;
    }
}
