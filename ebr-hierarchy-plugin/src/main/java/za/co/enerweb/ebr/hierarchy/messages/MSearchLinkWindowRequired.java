package za.co.enerweb.ebr.hierarchy.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import za.co.enerweb.ebr.renderer.vaadin.dataresource.ResourceUrlLink;
import za.co.enerweb.toolbox.vaadin.messaging.Message;

@Data
@AllArgsConstructor
public class MSearchLinkWindowRequired extends Message{

    private MGetSelectedNodes selectedNodesMessage;
    private ResourceUrlLink utlLink;
}
