package za.co.enerweb.ebr.hierarchy.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import za.co.enerweb.ebr.hierarchy.api.HierarchyNode;
import za.co.enerweb.toolbox.vaadin.messaging.Message;

/**
 * A message that can be sent to the Tree or other component to select nodes
 */
@Data
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
public class MSelectPointsInHierarchy extends Message {

    private HierarchyNode []toSelect;
}
