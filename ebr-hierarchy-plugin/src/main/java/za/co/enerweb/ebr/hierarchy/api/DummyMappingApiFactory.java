package za.co.enerweb.ebr.hierarchy.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;

import za.co.enerweb.ebr.datatype.gis.GisLocation;

public class DummyMappingApiFactory implements IMappingApiFactory {

    private String TXMDMS_QUERY =
        "SELECT p.KWH_EXPORT,p.DATE_TIME ,p.RECORDER FROM " +
            "LS_TSODS_TXMDMS_HOURLY_PROFILE p where p.recorder=? and " +
            "p.DATE_TIME between ? and ? " +
            "order by p.DATE_TIME";
    private String ZINC_QUERY =
        "SELECT DATAPOINTVALUE,DATAPOINTDATETIME, DATAPOINTTYPEID FROM "
        + "LS_ODS_INET_COIMP_ZINC where DATAPOINTDATETIME between ? and ? "
        + "order by DATAPOINTDATETIME";
    private String DIESEL_QUERY =
        "SELECT DATAPOINTTYPEID,DATAPOINTDATETIME, DATAPOINTVALUE FROM "
        + "LS_ODS_INET_COEP_DIESEL  where DATAPOINTDATETIME between ? and ? "
        + "order by DATAPOINTDATETIME";

    private static final String CATEGORY = "category";
    public static final int ROOT_COUNT = 2;
    public static final int CHILD_COUNT = 4;
    public static final int DATA_SOURCES = 2;
    private static final int DEPTH = 2;

    public List<Hierarchy> getHierarchies() {
        ArrayList<Hierarchy> dss = new ArrayList<Hierarchy>();
        for (int count = 1; count < (DATA_SOURCES + 1); count++) {// add 1
                                                                  // because I
                                                                  // start at 1
            dss.add(new Hierarchy("Hierarchy:" + count, null,
                "DATA_SOURCE_" + count, "Data Source " + count));
        }

        return dss;

    }

    public List<HierarchyNode> getRoots(Hierarchy hierarchy,
        Date effectiveDate) {
        ArrayList<HierarchyNode> roots = new ArrayList<HierarchyNode>();
        for (int count = 1; count < (ROOT_COUNT + 1); count++) {
            List<GisLocation> gisLocation = Collections.emptyList();
            HierarchyNode node = new HierarchyNode(null, "Root:" + count,
                hierarchy.getCaption() + " Root " + count, 0, true);
            node.setGisLocation(gisLocation);
            LinkedHashMap<String, Serializable> attr =
                new LinkedHashMap<String, Serializable>();
            attr.put(CATEGORY, "rootlayer");
            node.setAttributes(attr);
            roots.add(node);
        }

        return roots;

    }

    public List<HierarchyNode> getChildrenAtNextLevel(
        Hierarchy hierarchy,
        HierarchyNode node,
        Date effectiveDate) {

        ArrayList<HierarchyNode> nodes = new ArrayList<HierarchyNode>();
        if (node.isHasChildren() == false) {
            return nodes;
        }

        boolean hasChildren = true;
        int curLevel = node.getLevel();
        if (curLevel > DEPTH) {
            hasChildren = false;
            return nodes;
        }

        for (int count = 1; count < (CHILD_COUNT + 1); count++) {
            HierarchyNode mNode = new HierarchyNode(node, "Node:"
                + node.getUniqueId() + ":" + (count +
                CHILD_COUNT),
                "Node " + curLevel + 1
                    + "-" + count, curLevel + 1,
                hasChildren);
            List<GisLocation> gisArea = Collections.emptyList();
            String name = "Node " + curLevel + 1 + "-" + count;
            LinkedHashMap<String, Serializable> attr =
                new LinkedHashMap<String, Serializable>();
//            if (curLevel == DEPTH) {
//                // Random rand = new Random();
//                gisArea = new ArrayList<GisLocation>();
//                int uid = count + CHILD_COUNT;
//
//                switch (count) {
//                case 1:
//
//                    attr.put("hierarchy ID", hierarchy.getUniqueId());
//                    attr.put("hierarchy Level", hierarchy.getLevel());
//                    attr.put("hierarchy Name", hierarchy.getCaption());
//                    attr.put("Leaf", count);
//                    // attr.put("Leaf Name", name);
//                    double offetx = (1d / uid) - (count * 3);
//                    double offety = (1d / uid) - (count * 2);
//                    GisLocation gis =
//                        new GisLocation(-33.924816 - offetx, 18.424056
//                            + offety);
//                    name += " " + gis.toString();
//                    attr.put("Leaf Name", name);
//                    gisArea.add(gis);
//                    break;
//                case 2:
//                    attr.put("hierarchy ID", hierarchy.getUniqueId());
//                    attr.put("hierarchy Level", hierarchy.getLevel());
//                    attr.put("hierarchy Name", hierarchy.getCaption());
//                    attr.put("Leaf", count);
//                    // attr.put("Leaf Name", name);
//                    offetx = (1d / uid) - (count * 3);
//                    offety = (1d / uid) - (count * 2);
//                    gis =
//                        new GisLocation(-33.93259 - offetx, 25.569963
//                            + offety);
//                    name += " " + gis.toString();
//                    attr.put("Leaf Name", name);
//                    gisArea.add(gis);
//                    break;
//                case 3:
//
//                    attr.put("hierarchy ID", hierarchy.getUniqueId());
//                    attr.put("hierarchy Level", hierarchy.getLevel());
//                    attr.put("hierarchy Name", hierarchy.getCaption());
//                    attr.put("Leaf", count);
//
//                    offetx = (1d / uid) - (count * 3);
//                    offety = (1d / uid) - (count * 2);
//                    gis =
//                        new GisLocation(-29.857936 - offetx, 31.026777
//                            + offety);
//                    name += " " + gis.toString();
//                    attr.put("Leaf Name", name);
//                    gisArea.add(gis);
//                    break;
//                case 4:
//
//                    attr.put("hierarchy ID", hierarchy.getUniqueId());
//                    attr.put("hierarchy Level", hierarchy.getLevel());
//                    attr.put("hierarchy Name", hierarchy.getCaption());
//                    attr.put("Leaf", count);
//                    attr.put("MDMS_ID", "2003 888");
//                    // attr.put("Leaf Name", name);
//                    offetx = (1d / uid) - (count * 3);
//                    offety = (1d / uid) - (count * 2);
//                    gis =
//                        new GisLocation(-29.117771 - offetx, 26.226181
//                            + offety);
//                    name += " " + gis.toString();
//                    attr.put("Leaf Name", name);
//                    gisArea.add(gis);
//                    break;
//
//                }

            // }
            attr.put(CATEGORY, "layer_" + (curLevel + 1));

            mNode.setAttributes(attr);
            mNode.setCaption(name);
            mNode.setGisLocation(gisArea);
            nodes.add(mNode);
        }

        return nodes;
    }

    /**
     * Get the nodeInfo for the input node
     * @param node
     * @return all the info for this node
     */
    public List<INodeInfo> getNodeInfo(HierarchyNode node, Date effectiveDate) {
        List<INodeInfo> data = new ArrayList<INodeInfo>();

        Random r = new Random();
        r.setSeed(System.currentTimeMillis());
        for (int count = 0; count < 2; count++) {
            int random = r.nextInt();
            data.add(new NodeInfoData(node, random, "Data " + random, ""));

//            AdfResourceUrl url =
//                new AdfResourceUrl(null, null,
//                    "http://www.google.co.za/#q=" + node.getCaption() + " "
//                        + random);
//
//            data.add(new NodeInfoDocument(node, "google", url));
//
//            data.add(new SQLDataSourceNodeInfo(node, "Tx MDMS Export",
//                TXMDMS_QUERY, "KWH", true));
//            data.add(new SQLDataSourceNodeInfo(node, "Zinc Price", ZINC_QUERY,
//                "Cent", false));
//            data.add(new SQLDataSourceNodeInfo(node, "Diesel Price",
//                DIESEL_QUERY, "Cent", false));
        }

        return data;

    }

}
