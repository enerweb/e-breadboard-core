package za.co.enerweb.ebr.hierarchy.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import za.co.enerweb.toolbox.vaadin.messaging.Message;

@Data
@AllArgsConstructor
public class MWindowRequired extends Message{


    private String windowRequired;
    private MGetSelectedNodes selectedNodesMessage;
}
