package za.co.enerweb.ebr.hierarchy;

import static za.co.enerweb.ebr.adf.dbl.GisDbLayer.PARAM_LOCATION;
import static za.co.enerweb.toolbox.io.CopyOrMove.COPY;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.adf.data_types.AdfResourceUrl;
import za.co.enerweb.ebr.adf.dbl.AdfDbLayer;
import za.co.enerweb.ebr.adf.entities.CmEntity;
import za.co.enerweb.ebr.adf.entities.CmHierarchyDef;
import za.co.enerweb.ebr.adf.entities.CmParamdef;
import za.co.enerweb.ebr.adf.entities.CmReltypedef;
import za.co.enerweb.ebr.adf.entities.CmTypedef;
import za.co.enerweb.ebr.adf.entities.IHierarchyNode;
import za.co.enerweb.ebr.adf.entities.IbInstance;
import za.co.enerweb.ebr.adf.entities.IbInstanceParameter;
import za.co.enerweb.ebr.adf.model.AdfDataTypeClass;
import za.co.enerweb.ebr.adf.model.AdfInstance;
import za.co.enerweb.ebr.adf.model.EAuxInfo;
import za.co.enerweb.ebr.adf.model.TInfoDocument;
import za.co.enerweb.ebr.adf.model.TInfoSql;
import za.co.enerweb.ebr.datatype.gis.GisLocation;
import za.co.enerweb.ebr.hierarchy.api.Hierarchy;
import za.co.enerweb.ebr.hierarchy.api.HierarchyNode;
import za.co.enerweb.ebr.hierarchy.api.IMappingApiFactory;
import za.co.enerweb.ebr.hierarchy.api.INodeInfo;
import za.co.enerweb.ebr.hierarchy.api.NodeInfoDocument;
import za.co.enerweb.ebr.hierarchy.api.SQLDataSourceNodeInfo;

@Slf4j
public abstract class HierarchyDbLayer extends AdfDbLayer
    implements IMappingApiFactory {

    // public static final String TYPE_INFO_DOCUMENT = "INFO_DOCUMENT";
    // public static final String PARAM_DOCUMENT = "DOCUMENT";
    // public static final String PARAM_DOCUMENT_SOURCE = "DOCUMENT_SOURCE";

    // public static final String TYPE_INFO_SQL = "INFO_SQL";
    // public static final String PARAM_SQL = "SQL";
    // public static final String PARAM_SQL_UNIT = "SQL_UNIT";

    // /**
    // * @deprecated we should not rely on this..
    // */
    // public static final String PARAM_SQL_UNIQUE_ID = "SQL_NEEDS_UNIQUE_ID";

    // vvv metadata setup
    public synchronized void checkMetadata() {
        super.checkMetadata();
        // data types

        // CmEntity entityHierarchyNode = getHierarchyNodeEntity();

        // CmEntity entityAuxInfo = getAuxInfoEntity();

        // CmTypedef typedefDoc = addInfoType(entityAuxInfo,
        // TYPE_INFO_DOCUMENT);
        // getOrCreateCmParamdef(typedefDoc, PARAM_DOCUMENT,
        // INPUT, RESOURCE_URL,
        // SCALAR, UNIT_OF_MEASURE_PER_UNIT, null);
        // getOrCreateCmParamdef(typedefDoc, PARAM_DOCUMENT_SOURCE,
        // INPUT, TEXT, SCALAR, UNIT_OF_MEASURE_PER_UNIT, null);
        // getOrCreateHierarchy(TYPE_INFO_DOCUMENT);

        // CmTypedef typedefSql = addInfoType(entityAuxInfo,
        // TYPE_INFO_SQL);
        // getOrCreateCmParamdef(typedefSql, PARAM_SQL,
        // INPUT, TEXT, SCALAR, UNIT_OF_MEASURE_PER_UNIT, null);
        // getOrCreateCmParamdef(typedefSql, PARAM_SQL_UNIT,
        // INPUT, TEXT, SCALAR, UNIT_OF_MEASURE_PER_UNIT, null);
        // getOrCreateCmParamdef(typedefSql, PARAM_SQL_UNIQUE_ID,
        // INPUT, BOOLEAN, SCALAR, UNIT_OF_MEASURE_PER_UNIT, null);
        // getOrCreateHierarchy(TYPE_INFO_SQL);
        em.flush();
    }

    // protected CmEntity getAuxInfoEntity() {
    // return getOrCreateCmEntity(
    // IAuxInfo.ENTITY_AUX_INFO, "Auxiliary Info", EntityType.INSTANCE);
    // }

    protected CmTypedef addInfoType(CmEntity infoEntity,
        String docTypeKey) {
        CmTypedef typedefInfo = getOrCreateCmTypedef(infoEntity,
            docTypeKey, toCaption(docTypeKey) + " Type");
        return typedefInfo;
    }

    // public CmTypedef getOrCreateHierarchyNodetype(String nodeTypeDefKey) {
    // return super.getOrCreateHierarchyNodetype(ENTITY_HIERARCHY_NODE,
    // nodeTypeDefKey);
    // }

    // protected CmEntity getHierarchyNodeEntity() {
    // return super.getHierarchyNodeEntity(
    // ENTITY_HIERARCHY_NODE);
    // }

    // ^^^ metadata setup

    public TInfoDocument addDocument(String caption,
        AdfResourceUrl resource,
        String source, Date appldatetime) {

        TInfoDocument ret = addDocument(caption, source);
        ret.setDocument(resource);

        // resource.setInstance(ret);
        // resource.setCmParamdef(getCmParamdef(
        // docTypeDef, PARAM_DOCUMENT));
        // addParamValue(ret, PARAM_DOCUMENT, appldatetime,
        // resource, null);

        return ret;
    }

    /**
     * sourceFile will be copied into the adf document store
     * @throws IOException
     */
    public TInfoDocument addLocalDocument(String caption,
        File sourceFile, String source,
        Date appldatetime) throws IOException {

        CmTypedef docTypeDef = findTypedefByKey("INFO_DOCUMENT");
        CmParamdef docParamdef = getCmParamdef(docTypeDef, "DOCUMENT");
        TInfoDocument ret = addDocument(caption, source);
        saveAdfInstance(ret);
        IbInstance ibInstance = findInstance(ret);

//        getAdfRegistry().getAdfTypeDef(TInfoDocument.class).get
        AdfResourceUrl aru =
            AdfResourceUrl.saveFromLocalFile(
                adfFactory, ibInstance, docParamdef, sourceFile, COPY);
//        AdfResourceUrl aru = AdfResourceUrl.saveFromLocalFile(
//            getAdfFactory(), ret,
//            docParamdef, sourceFile, COPY);


        ret.setDocument(aru);
        saveAdfInstance(ret);
        // addParamValue(ret, PARAM_DOCUMENT, appldatetime,
        // aru, null);

        return ret;
    }

    public TInfoDocument addUrlDocument(String caption,
        String url, String source,
        HierarchyNode relatedNode, Date appldatetime) {
        return addDocument(caption,
            new AdfResourceUrl(null, null, url), source,
            appldatetime);
    }

    private TInfoDocument addDocument(String caption,
        String source) {

        TInfoDocument ret = new TInfoDocument();
        ret.setDocumentSource(source);
        this.saveAdfInstance(ret);

        return ret;
    }

    public void linkDocumentInfo(TInfoDocument info,
        // IbInstance info,
        List<AdfInstance> parents
        // List<? extends IbInstance> parents
        ) {

        info.setLinkedInstances(parents);
        // linkHierarchyInstances(EHierarchy.INFO_DOCUMENT_HIER_KEY,
        // parents, info, null, null);
    }

// public <T extends IbInstance> void addRelationships(IAuxInfo ret,
// T... relatedInstances) {
// addRelationships(ret, Arrays.asList(relatedInstances));
// }
//
// public <T extends IbInstance> void addRelationships(IAuxInfo ret,
// Iterable<T> relatedInstances) {
// for (IbInstance relatedInstance : relatedInstances) {
// createRelationship(ret, relatedInstance);
// }
// }

    public TInfoSql addSqlInfo(
        SQLDataSourceNodeInfo info) {
        // getAdfRegistry().get
        // CmTypedef nodeTypedef = findTypedefByKey(TYPE_INFO_SQL);
        TInfoSql ret = new TInfoSql();
        ret.setSql(info.getSql());
        ret.setSqlUnit(info.getUnit());
        ret.setSqlUniqueId(info.isNeedsUniqueID());

        // IAuxInfo ret = getOrCreateInstance(nodeTypedef,
        // info.getCaption());

        // addParamValue(ret, PARAM_SQL_NAME, EPOCH,
        // info.getName(), null);
        // addParamValue(ret, PARAM_SQL, EPOCH,
        // info.getSql(), null);
        // addParamValue(ret, PARAM_SQL_UNIT, EPOCH,
        // info.getUnit(), null);
        // addParamValue(ret, PARAM_SQL_UNIQUE_ID, EPOCH,
        // info.isNeedsUniqueID(), null);
        return ret;
    }

    public void linkSqlInfo(IbInstance info,
        List<? extends IbInstance> parents) {
        linkHierarchyInstances("INFO_SQL", parents, info, null, null);
    }

    @Override
    public List<Hierarchy> getHierarchies() {
        List<Hierarchy> ret = new ArrayList<Hierarchy>();
        for (CmHierarchyDef hierarchy : findAll(CmHierarchyDef.class)) {
            String hierarchyKey = hierarchy.getKey();
            ret.add(new Hierarchy(CmReltypedef.class.getSimpleName() + ":"
                + hierarchyKey,
                hierarchy, hierarchyKey, hierarchy.getCaption()));
        }
        return ret;
    }

    /**
     * add all scalar attributes of the node
     */
    private void addAttributes(IbInstance instance,
        HierarchyNode node, Date effectiveDate) {

        Query q =
            em.createQuery(
                "SELECT i.cmParamdefPK.key FROM "
                    + CmParamdef.class.getSimpleName()
                    + " i"
                    + " WHERE i.cmParamdefPK.cmTypedef = :typedef"
                    + " AND i.cmDatatype.cmDatatypeclass.key = :typeClass"
                );
        q.setParameter("typedef", instance.getCmTypedef());
        q.setParameter("typeClass", AdfDataTypeClass.SCALAR.name());
        @SuppressWarnings("unchecked") List<String> resultList =
            q.getResultList();
        for (String paramKey : resultList) {
            Serializable value = getParamValue(instance, paramKey,
                effectiveDate, null);
            node.addAttribute(paramKey, value);
        }
    }

    protected final HierarchyNode createHierachyNode(
        IbInstance instance,
        Date effectiveDate) {
        HierarchyNode node = new HierarchyNode(instance);

        addAttributes(instance, node, effectiveDate);

        CmParamdef locationParameterDef = findParameterDef(
            instance.getCmTypedef(),
            PARAM_LOCATION);
        addLocation(locationParameterDef, instance, node);
        return node;
    }

    public IHierarchyNode addHierarchyNode(String typedefKey, String caption,
        List<GisLocation> area) {
        IHierarchyNode ret = getOrCreateInstance(getCmTypedef(typedefKey),
            caption);
        if (area != null) {
            int i = 0;
            for (GisLocation point : area) {
                addParamValue(ret, PARAM_LOCATION, EPOCH,
                    point, "" + i);
                i++;
            }
        }
        return ret;
    }

    protected void addLocation(CmParamdef locationParameterDef,
        IbInstance instance, HierarchyNode node) {
        List<IbInstanceParameter> points =
            getParamValues(instance,
                locationParameterDef);
        if (!points.isEmpty()) {
            GisLocation[] area = new GisLocation[points.size()];
            for (IbInstanceParameter point : points) {
                try {
                    area[Integer.parseInt(point.getIndx())] =
                        getParamValue(point, locationParameterDef);
                    // point.getParamValue(locationParameterDef);
                } catch (NumberFormatException nfe) {
                    log.warn("Unable to parse point <" + point.getIndx() + ">");
                }
            }
            node.setGisLocation(Arrays.asList(area));
        }
    }

    @Override
    public List<HierarchyNode> getRoots(Hierarchy hierarchy,
        Date effectiveDate) {
        return getChildrenAtNextLevel(
            hierarchy, null, effectiveDate);
    }

    @Override
    public List<HierarchyNode> getChildrenAtNextLevel(
        Hierarchy hierarchy, HierarchyNode parentNode,
        Date effectiveDate) {

        IbInstance parent = null;
        if (parentNode != null) {
            parent = parentNode.getInstance();
        }
        List<IbInstance> hierarchyChildren = getHierarchyChildren(
            hierarchy.getCmHierarchyDef().getKey(), parent,
            effectiveDate);
        List<HierarchyNode> ret = new ArrayList<HierarchyNode>();
        for (IbInstance ibInstance : hierarchyChildren) {
            HierarchyNode node = createHierachyNode(
                ibInstance, effectiveDate);
            // load the children
            node.setChildren(getChildrenAtNextLevel(
                hierarchy, node, effectiveDate));
            ret.add(node);
        }
        return ret;
    }

    /*
     * Only accepts nodes created by this factory
     * synchronized because of possible openejb bug:
     * java.rmi.RemoteException: Concurrent calls not allowed.
     */
    @Override
    public synchronized List<INodeInfo> getNodeInfo(HierarchyNode node,
        Date effectiveDate) {
        List<INodeInfo> ret = new ArrayList<INodeInfo>();

        IbInstance parent = node.getInstance();
        if (parent != null) {

            for (IbInstance docInstance : getHierarchyChildren(
                "INFO_DOCUMENT", parent, effectiveDate)) {
                if (!docInstance.getCmTypedef().getKey().equals(
                    "INFO_DOCUMENT")) {
                    continue;
                }

                TInfoDocument adfInstance = findAdfInstance(
                    EAuxInfo.class, docInstance.getId());

                // AdfResourceUrl doc = getParamValue(docInstance,
                // "DOCUMENT", effectiveDate, null);
                //
                // String docSource = getParamValue(docInstance,
                // "DOCUMENT_SOURCE", effectiveDate, null);
                ret.add(new NodeInfoDocument(node,
                    adfInstance.getDocumentSource(),
                    adfInstance.getDocument()));
            }

            for (IbInstance sqlInstance : getHierarchyChildren(
                "INFO_SQL", parent, effectiveDate)) {
                SQLDataSourceNodeInfo info =
                    getSQLDataSourceNodeInfo(node, effectiveDate, sqlInstance);
                if (info != null) {
                    continue;
                }
                ret.add(info);
            }
        }
        return ret;
    }

    public SQLDataSourceNodeInfo getSQLDataSourceNodeInfo(HierarchyNode node,
        Date effectiveDate, IbInstance sqlInstance) {
        if (!sqlInstance.getCmTypedef().getKey().equals(
            "INFO_SQL")) {
            return null;
        }

        TInfoSql adfInstance = findAdfInstance(
            EAuxInfo.class, sqlInstance.getId());

        SQLDataSourceNodeInfo info = new SQLDataSourceNodeInfo(
            node,
            sqlInstance.getCaption(),
            adfInstance.getSql(),
            adfInstance.getSqlUnit(),
            adfInstance.getSqlUniqueId());
        return info;
    }


}
