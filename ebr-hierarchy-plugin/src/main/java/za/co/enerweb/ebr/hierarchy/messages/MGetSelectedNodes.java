package za.co.enerweb.ebr.hierarchy.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import za.co.enerweb.ebr.hierarchy.api.HierarchyNode;
import za.co.enerweb.toolbox.vaadin.messaging.Message;

@Data
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
/**
 * Fire this message and get a MSelectedNodes back
 */
@NoArgsConstructor
public class MGetSelectedNodes extends Message{

    private HierarchyNode []responseCurrentlySelected;



}
