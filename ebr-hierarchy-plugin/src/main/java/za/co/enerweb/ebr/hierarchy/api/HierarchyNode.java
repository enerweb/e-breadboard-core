package za.co.enerweb.ebr.hierarchy.api;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import za.co.enerweb.ebr.adf.entities.IbInstance;
import za.co.enerweb.ebr.datatype.gis.GisLocation;

/**
 * XXX: factor out the common stuff to abstract base class
 * and leave the stuff that is not
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HierarchyNode implements Serializable {
    private static final long serialVersionUID = 1L;

    private HierarchyNode parent;

    /**
     * Globally unique id for this node.
     */
    private String uniqueId;

    private String caption;
    private int level;//level 0 is root
    private boolean hasChildren;

    /**
     * This is optionally poplulated so that multiple levels can be loaded
     * in a single roundtrip. (Must be null if it is not loaed)
     */
    private transient List<HierarchyNode> children;

    /**
     * One or more geographical points associated with this node
     */
    private List<GisLocation> gisLocation = Collections.emptyList();

    /**
     * Freeform attributes for this node
     */
    private Map<String, Serializable> attributes =
        new HashMap<String, Serializable>();

    private boolean selected = false;
    private IbInstance instance;

    public HierarchyNode(HierarchyNode parent, String uniqueId,
        String caption,
        int level, boolean hasChildren) {
        this.parent = parent;
        this.uniqueId = uniqueId;
        this.caption = caption;
        this.level = level;
        this.hasChildren = hasChildren;
    }

    public HierarchyNode(IbInstance instance) {
// this.parent = parent; unknown :(
        this.uniqueId = instance.getCmTypedef().getKey() + ":"
            + instance.getId();
        this.caption = instance.getCaption();
        this.level = 0;
        this.hasChildren = true;
        this.instance = instance;
    }

    public String toString(){
        return caption;
    }

    public void addAttribute(String key, Serializable value) {
        attributes.put(key, value);
    }

    public Serializable getAttribute(String key) {
        return attributes.get(key);
    }
}
