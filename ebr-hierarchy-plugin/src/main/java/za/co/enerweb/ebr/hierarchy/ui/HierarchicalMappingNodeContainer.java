package za.co.enerweb.ebr.hierarchy.ui;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import za.co.enerweb.ebr.hierarchy.api.HierarchyNode;
import za.co.enerweb.ebr.hierarchy.api.IMappingApiFactory;
import za.co.enerweb.ebr.hierarchy.api.Hierarchy;

import com.vaadin.data.Container.Hierarchical;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;

/** Extension of BeanItemContainer that implements Hierarchical */
public class HierarchicalMappingNodeContainer
    extends BeanItemContainer<HierarchyNode> implements Hierarchical {
    // The contained bean type uses this property to store
    // the parent relationship.
    Object parentPID;

    private IMappingApiFactory treeFactory;
    private Hierarchy dataSource;

    public HierarchicalMappingNodeContainer(IMappingApiFactory treeFactory,
        Hierarchy dataSource,
        Object parentPropertyId) {
        super(HierarchyNode.class);
        this.treeFactory = treeFactory;
        this.dataSource = dataSource;
        this.parentPID = parentPropertyId;

        addRoots();

    }

    private void addRoots() {
        List<HierarchyNode> roots = treeFactory.getRoots(dataSource,
            new Date());
        for (HierarchyNode root : roots) {
            // addBean(root);
            if (this.getItem(root) == null) {
                addBean(root);
            }
        }
    }

    @Override
    public Collection<?> getChildren(Object itemId) {
        // LinkedList<Object> children = new LinkedList<Object>();
        //
        // // This implementation has O(n^2) complexity when
        // // painting the tree, so it's really inefficient.
        // for (Object candidateId: getItemIds()) {
        // Object parentRef = getItem(candidateId).
        // getItemProperty(parentPID).getValue();
        // if (parentRef == itemId)
        // children.add(candidateId);
        // }
        //
        // if (children.size() > 0)
        // return children;
        // else
        // return null;

        HierarchyNode node = null;
        if (itemId instanceof HierarchyNode) {
            node = (HierarchyNode) itemId;
        }
        else if (itemId instanceof BeanItem) {
            node = (HierarchyNode) ((BeanItem) itemId).getBean();
        }

        List<HierarchyNode> children = node.getChildren();

        if (children == null){
        children = treeFactory.getChildrenAtNextLevel(
            dataSource, node, new Date());
        }

        node.setChildren(children);

        /*
                List<BeanItem<HierarchyNode>> beans
                = new LinkedList<BeanItem<HierarchyNode>>();*/
        for (HierarchyNode child : children) {

            child.setParent(node);

            // BeanItem<HierarchyNode> bean = this.addBean(child);
            // bean.addItemProperty("name", new
            // ObjectProperty(child.getName()));
            if (this.getItem(child) == null) {
                addBean(child);
            }
        }

        // if (children.isEmpty()){
        // return false;
        // }

        return children;

    }

    @Override
    public Object getParent(Object itemId) {
        HierarchyNode node = null;
        if (itemId instanceof HierarchyNode) {
            node = (HierarchyNode) itemId;
            return node.getParent();
        }
        return null;

    }

    @Override
    public Collection<?> rootItemIds() {
        LinkedList<Object> result = new LinkedList<Object>();
        for (Object candidateId : getItemIds()) {
            Object parentRef = getItem(candidateId).
                getItemProperty(parentPID).getValue();
            if (parentRef == null)
                result.add(candidateId);
        }

        // XXX: not sure why you wanted to return null which causes exceptions..
//        if (result.size() > 0)
            return result;
//        else
//            return null;
    }

    @Override
    public boolean setParent(Object itemId, Object newParentId)
        throws UnsupportedOperationException {
        throw new UnsupportedOperationException(
            "Not implemented here");
    }

    @Override
    public boolean areChildrenAllowed(Object itemId) {

//        if (itemId instanceof HierarchyNode) {
//            HierarchyNode node = (HierarchyNode) itemId;
//            List<HierarchyNode> children =
//                treeFactory
//                    .getChildrenAtNextLevel(dataSource, node, new Date());
//
//            if (children.isEmpty()) {
//                return false;
//            }
//        }
        //return true;
        return hasChildren(itemId);
    }

    @Override
    public boolean setChildrenAllowed(Object itemId,
        boolean childrenAllowed)
        throws UnsupportedOperationException {
        throw new UnsupportedOperationException(
            "Not implemented here");
    }

    @Override
    public boolean isRoot(Object itemId) {
        if (itemId instanceof HierarchyNode) {
            HierarchyNode node = (HierarchyNode) itemId;
            if (node.getLevel() == 0) {
                return true;
            }

        }
        return false;
    }

    @Override
    public boolean hasChildren(Object itemId) {

        if ((this.getChildren(itemId) == null) || ( this.getChildren(itemId).isEmpty())){
            return false;
        }

//        if (itemId instanceof HierarchyNode) {
//            HierarchyNode node = (HierarchyNode) itemId;
//            List<HierarchyNode> children =
//                treeFactory
//                    .getChildrenAtNextLevel(dataSource, node, new Date());
//
//            if (children.isEmpty()) {
//                return false;
//            }
//        }
        return true;
    }
}
/*

public class HierarchicalMappingNodeContainer
    extends BeanItemContainer<HierarchyNode> implements Hierarchical {
    // The contained bean type uses this property to store
    // the parent relationship.
    Object parentPID;

    private IMappingApiFactory treeFactory;
    private Hierarchy dataSource;

    public HierarchicalMappingNodeContainer(IMappingApiFactory treeFactory,
        Hierarchy dataSource,
        Object parentPropertyId) {
        super(HierarchyNode.class);
        this.treeFactory = treeFactory;
        this.dataSource = dataSource;
        this.parentPID = parentPropertyId;

        addRoots();

    }

    private void addRoots() {
        List<HierarchyNode> roots = treeFactory.getRoots(dataSource,
            new Date());
        for (HierarchyNode root : roots) {
            // addBean(root);
            if (this.getItem(root) == null) {
                addBean(root);
            }
        }
    }

    @Override
    public Collection<?> getChildren(Object itemId) {
        // LinkedList<Object> children = new LinkedList<Object>();
        //
        // // This implementation has O(n^2) complexity when
        // // painting the tree, so it's really inefficient.
        // for (Object candidateId: getItemIds()) {
        // Object parentRef = getItem(candidateId).
        // getItemProperty(parentPID).getValue();
        // if (parentRef == itemId)
        // children.add(candidateId);
        // }
        //
        // if (children.size() > 0)
        // return children;
        // else
        // return null;

        HierarchyNode node = null;
        if (itemId instanceof HierarchyNode) {
            node = (HierarchyNode) itemId;
        }
        else if (itemId instanceof BeanItem) {
            node = (HierarchyNode) ((BeanItem) itemId).getBean();
        }


        List<HierarchyNode> children = node.getChildren();
        if (children != null){
            return children;
        }


        children = treeFactory.getChildrenAtNextLevel(
            dataSource, node, new Date());

        node.setChildren(children);




        for (HierarchyNode child : children) {

            // BeanItem<HierarchyNode> bean = this.addBean(child);
            // bean.addItemProperty("name", new
            // ObjectProperty(child.getName()));
            if (this.getItem(child) == null) {
                addBean(child);
            }
        }

        // if (children.isEmpty()){
        // return false;
        // }

        return children;

    }

    @Override
    public Object getParent(Object itemId) {
        HierarchyNode node = null;
        if (itemId instanceof HierarchyNode) {
            node = (HierarchyNode) itemId;
            return node.getParent();
        }
        return null;

    }

    @Override
    public Collection<?> rootItemIds() {
        LinkedList<Object> result = new LinkedList<Object>();
        for (Object candidateId : getItemIds()) {
            Object parentRef = getItem(candidateId).
                getItemProperty(parentPID).getValue();
            if (parentRef == null)
                result.add(candidateId);
        }

        // XXX: not sure why you wanted to return null which causes exceptions..
//        if (result.size() > 0)
            return result;
//        else
//            return null;
    }

    @Override
    public boolean setParent(Object itemId, Object newParentId)
        throws UnsupportedOperationException {
        throw new UnsupportedOperationException(
            "Not implemented here");
    }

    @Override
    public boolean areChildrenAllowed(Object itemId) {

        if (itemId instanceof HierarchyNode) {
//            HierarchyNode node = (HierarchyNode) itemId;
//            List<HierarchyNode> children =
//                treeFactory
//                    .getChildrenAtNextLevel(dataSource, node, new Date());

            Collection children = this.getChildren(itemId);


            if ((children == null) || (children.isEmpty())) {
                return false;
            }
        }
        return true;
        // return hasChildren(itemId);
    }

    @Override
    public boolean setChildrenAllowed(Object itemId,
        boolean childrenAllowed)
        throws UnsupportedOperationException {
        throw new UnsupportedOperationException(
            "Not implemented here");
    }

    @Override
    public boolean isRoot(Object itemId) {
        if (itemId instanceof HierarchyNode) {
            HierarchyNode node = (HierarchyNode) itemId;
            if (node.getLevel() == 0) {
                return true;
            }

        }
        return false;
    }

    @Override
    public boolean hasChildren(Object itemId) {

        return areChildrenAllowed(itemId);

//        if (itemId instanceof HierarchyNode) {
//            HierarchyNode node = (HierarchyNode) itemId;
//            List<HierarchyNode> children =
//                treeFactory
//                    .getChildrenAtNextLevel(dataSource, node, new Date());
//
//            if (children.isEmpty()) {
//                return false;
//            }
//        }
//        return true;
    }
}

*/