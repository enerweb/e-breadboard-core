package za.co.enerweb.ebr.hierarchy.api;

import java.util.Date;
import java.util.List;

public interface IMappingApiFactory {

    /**
     * Get all the available hierarchies, example Transmission , Distribution
     * etc
     * HierarchyNode is dumb and has no logic
     * @return The roots
     */
    public List<Hierarchy> getHierarchies();

    /**
     * Get all the available roots, but not their children
     * HierarchyNode is dumb and has no logic
     * @return The roots
     */
    List<HierarchyNode> getRoots(Hierarchy hierarchy,
        Date effectiveDate);
    /**
     * Get all the children for the input note at the next level
     * i.e. if the input level is as level 2, it will only get level 3 data, not
     * level 4 and 5
     * @param node
     * @param effectiveDate the date search criteria
     * @return
     */
    List<HierarchyNode>
        getChildrenAtNextLevel(Hierarchy hierarchy,
            HierarchyNode node, Date effectiveDate);

    /**
     * Get the nodeInfo for the input node
     * @param node
     * @return all the info for this node
     */
    List<INodeInfo> getNodeInfo(HierarchyNode node, Date effectiveDate);

}
