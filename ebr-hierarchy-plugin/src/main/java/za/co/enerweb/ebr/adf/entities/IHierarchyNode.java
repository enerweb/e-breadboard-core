package za.co.enerweb.ebr.adf.entities;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * I_HierachyNode
 * + mdms_parents : node[]
 * + spacial_parents : node[]
 * + mdms_id
 * + location : point[]
 * + map layers?
 * + default map zoom?
 */

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, of = { })
@ToString(callSuper = true, of = { })
@Entity
@Table(name = "I_HIERARCHY_NODE")
public class IHierarchyNode extends IbInstance {
    private static final long serialVersionUID = 1L;
    // public static final String ENTITY_HIERARCHY_NODE = "HIERARCHY_NODE";

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "iHierarchyNode")
    private Collection<IHierarchyNodePar> iHierarchyNodeParCollection;
}
