package za.co.enerweb.ebr.tasks.rest;

import static org.junit.Assert.assertEquals;

import java.io.File;

import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.openejb.api.LocalClient;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import za.co.enerweb.ebr.server.AEbrTest;

import com.jayway.restassured.response.Response;

@LocalClient
public class TaskRestTest extends AEbrTest {

    @Rule
    public TemporaryFolder tempDir = new TemporaryFolder();

    public void setUpAfterInjection() throws Exception {
        super.setUpAfterInjection();
        AEbrTest.addDummyHome("RestTaskHome");
    }

    @Test
    public void testResponseBody() throws Exception {
        Response response = givenApplicationUri()
            .post("task/{task-id}", "dummy-rest-task");

        response.then()
            .assertThat().statusCode(Status.OK.getStatusCode());

        assertEquals(DummyRestTask.RESPONSE_BODY,
            response.getBody().asString());
    }

    @Test
    public void testPostBodyPayload() throws Exception {
        String dummyContent = StringUtils.repeat("my dummy payload ", 3);
        File tmpFile =
            tempDir.newFile(getClass().getSimpleName() + "tempfile.dat");
        FileUtils.write(tmpFile, dummyContent);
        Response response = givenApplicationUri()
            .multiPart("restPayload",
                tmpFile)
            // .multiPart("restPayload_fromfile",
            // ResourceUtils.getResourceAsFile("some_image.jpg")
            // )

            // writing bytes or input stream doesn't work :(
            // .multiPart("restPayload", "dummyfile.dat",
            // new ByteArrayInputStream(dummyContent.getBytes()),
            // MediaType.OCTET_STREAM.toString())
                // .multiPart("restPayload2", "dummyfile2.txt",
                // dummyContent.getBytes())
            .when()
            .post("task/{task-id}", "dummy-rest-task");

        response.then()
            .assertThat().statusCode(Status.OK.getStatusCode());

        assertEquals(dummyContent,
            response.getBody().asString());


    }
}
