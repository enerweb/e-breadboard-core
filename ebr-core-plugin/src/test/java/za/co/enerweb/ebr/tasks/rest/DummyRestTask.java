package za.co.enerweb.ebr.tasks.rest;

import javax.ws.rs.core.Response;

import lombok.Data;
import za.co.enerweb.ebr.datatype.dataresource.FileDataResource;
// import za.co.enerweb.ebr.datatype.dataresource.FileDataResource;
import za.co.enerweb.ebr.datatype.rest.RestResponse;
import za.co.enerweb.ebr.task.AJavaTask;

/**
 * A simple java task used for manual and automated testing.
 */
@Data
public class DummyRestTask extends AJavaTask {

    public static final String RESPONSE_BODY = "hurray task ran fine!";
    private RestResponse restResponse;
    private FileDataResource restPayload;

    @Override
    public void runJavaTask() throws Exception {
        getLogger().info("running task.");

        String ret;
        if (restPayload != null) {
            ret = new String(restPayload.getBytes());
        } else {
            ret = RESPONSE_BODY;
        }
        restResponse = RestResponse.build(Response.ok(ret));
    }

}
