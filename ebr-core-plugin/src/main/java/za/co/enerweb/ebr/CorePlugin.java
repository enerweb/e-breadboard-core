package za.co.enerweb.ebr;

import java.util.Date;

import za.co.enerweb.ebr.annotations.OnEbrReload;
import za.co.enerweb.ebr.datatype.IDataTypeRegistryService;
import za.co.enerweb.ebr.datatype.control.Action;
import za.co.enerweb.ebr.datatype.control.MenuItemRefefrence;
import za.co.enerweb.ebr.datatype.control.TaskRefefrence;
import za.co.enerweb.ebr.datatype.dataresource.FileDataResource;
import za.co.enerweb.ebr.datatype.dataresource.ResourceUrl;
import za.co.enerweb.ebr.datatype.datetime.DateStringConverter;
import za.co.enerweb.ebr.datatype.datetime.DateTimeConverter;
import za.co.enerweb.ebr.datatype.datetime.DateTimeStringConverter;
import za.co.enerweb.ebr.datatype.db.SqlQueryResult;
import za.co.enerweb.ebr.datatype.html.HtmlTar;
import za.co.enerweb.ebr.datatype.image.Image;

public class CorePlugin {

    @OnEbrReload
    public static void onEbrReload() {
        IDataTypeRegistryService typeReg =
            EbrFactory.getDataTypeRegistryService();

        //TODO: maybe type aliases should be registered by annotation too..
        typeReg.registerAlias(Action.class, Action.TYPE_ALIAS);
        typeReg.registerAlias(TaskRefefrence.class, TaskRefefrence.TYPE_ALIAS);
        typeReg.registerAlias(MenuItemRefefrence.class,
            MenuItemRefefrence.TYPE_ALIAS);

        typeReg.registerAlias(Image.class, Image.TYPE_ALIAS);
        typeReg.registerAlias(FileDataResource.class,
            FileDataResource.TYPE_ALIAS);
        // typeReg.registerAlias(FileDataResource.class,
        // MediaType.APPLICATION_OCTET_STREAM);

        typeReg.registerDeprecatedAlias(FileDataResource.TYPE_ALIAS, null,
            FileDataResource.DEPRECATED_TYPE_ALIAS);

        typeReg.registerAlias(HtmlTar.class, HtmlTar.HTML_DIR_TYPE_ALIAS);
        typeReg.registerAlias(Date.class, DateTimeConverter.TYPE_ALIAS);
        typeReg.registerAlias(Date.class, DateStringConverter.TYPE_ALIAS);
        typeReg.registerAlias(Date.class, DateTimeStringConverter.TYPE_ALIAS);

        typeReg.registerAlias(SqlQueryResult.class, SqlQueryResult.TYPE_ALIAS);
        typeReg.registerAlias(ResourceUrl.class, ResourceUrl.TYPE_ALIAS);

        // java primatives
        typeReg.registerAlias(Long.class, "java", "long");
        typeReg.registerAlias(Integer.class, "java", "int");
        typeReg.registerAlias(Short.class, "java", "short");
        typeReg.registerAlias(Byte.class, "java", "byte");
        typeReg.registerAlias(Double.class, "java", "double");
        typeReg.registerAlias(Float.class, "java", "float");
        typeReg.registerAlias(Boolean.class, "java", "boolean");
        typeReg.registerAlias(Character.class, "java", "char");
    }
}
