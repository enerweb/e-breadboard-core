package za.co.enerweb.ebr.datatype;


/**
 * converts from a number to an Double
 */
public class DoubleConverter implements ITypeConverter<Double> {

    private static final long serialVersionUID = 1L;

    /* (non-Javadoc)
     * @see za.co.enerweb.ebr.api.datatype.ITypeConverter
     *   #fromSimpleStructure(java.lang.Object)
     */
    @Override
    public Double fromSimpleStructure(final String envId,
        final String variableName,
        final Object simpleStructure) {
        if (simpleStructure instanceof Double) {
            return (Double) simpleStructure;
        }
        if (simpleStructure instanceof Number) {
            return ((Number) simpleStructure).doubleValue();
        }
        return Double.parseDouble(simpleStructure.toString());
    }

    /* (non-Javadoc)
     * @see za.co.enerweb.ebr.api.datatype.ITypeConverter
     *   #toSimpleStructure(java.lang.Object)
     */
    @Override
    public Object toSimpleStructure(final String envId,
        final String variableName, final Double specificObject) {
        return specificObject;
    }

    /* (non-Javadoc)
     * @see za.co.enerweb.ebr.api.datatype.ITypeConverter#getType()
     */
    @Override
    public String getType() {
        return Double.class.getName();
    }

}
