package za.co.enerweb.ebr.datatype.dataresource;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.apache.commons.io.FilenameUtils;

import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.server.HomeFile;
import za.co.enerweb.ebr.server.HomeFileCategory;
import za.co.enerweb.ebr.server.HomeFileSpec;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ResourceUrl implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final String TYPE_ALIAS = "resource-url";
    public static final String RESOURCE_URL_PREFIX = "resource://";

    private String url;

    public String toString() {
        return "" + url;
    }

    @AllArgsConstructor
    @Data
    public static class HomeFileSpecAndRelPath {
        HomeFileSpec homeFileSpec;
        String relpath;

        public String getFileName() {
            return FilenameUtils.getName(getRelpath());
        }
    }

    @AllArgsConstructor
    @Data
    public static class HomeFileAndRelPath {
        HomeFile homeFile;
        String relpath;

        public String getFileName() {
            return FilenameUtils.getName(getRelpath());
        }
    }

    /**
     * @return a HomeFile if this is a home resource, otherwise returns null
     */
    public HomeFileSpecAndRelPath getHomeFileSpecAndRelPath() {
        String relPath = removePrefix(RESOURCE_URL_PREFIX);
        if (relPath != null) {
            return new HomeFileSpecAndRelPath(
                new HomeFileSpec(HomeFileCategory.RESOURCES, relPath),
                relPath);
        }
        return null;
    }

    /**
     * @return a HomeFile if this is a home resource, otherwise returns null
     */
    public HomeFileAndRelPath getHomeFileAndRelPath() {
        HomeFileSpecAndRelPath homeFileSpecAndRelPath =
            getHomeFileSpecAndRelPath();
        if (homeFileSpecAndRelPath != null) {
            return new HomeFileAndRelPath(
                EbrFactory.getHomeService().getFile(
                    homeFileSpecAndRelPath.getHomeFileSpec()
                    ), homeFileSpecAndRelPath.getRelpath());
        }
        return null;
    }

    /**
     * @return null if it didn't have the prefix
     */
    public String removePrefix(String prefix) {
        if (url != null && url.startsWith(prefix)) {
            return url.substring(prefix.length());
        }
        return null;
    }
}
