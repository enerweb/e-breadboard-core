package za.co.enerweb.ebr.datatype.dataresource;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import javax.xml.bind.ValidationException;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

/**
 * implemented as a zip of a bunch of files
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AbstractTarDataResource extends AbstractDataResource
implements ITarDataResource {
    private static final long serialVersionUID = -5363312827006336954L;

    public AbstractTarDataResource(
        final String envId, final String path) {
        super(envId, path);
    }

    public Iterator<String> getEntryNames()
    throws IOException, ValidationException {
        return new Iterator<String>(){
            ZipInputStream zis = new ZipInputStream(getInputStream());
            ZipEntry ze = zis.getNextEntry();
            @Override
            public boolean hasNext() {
                boolean ret = ze != null;
                if (!ret) {
                    IOUtils.closeQuietly(zis);
                }
                return ret;
            }

            @Override
            public String next() {
                if (!hasNext()) {
                    throw new NoSuchElementException("No more elements left");
                }
                String ret = ze.getName();
                try {
                    ze = zis.getNextEntry();
                } catch (IOException e) {
                    throw new NoSuchElementException(e.getLocalizedMessage());
                }
                return ret;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }};
    }

    public byte[] getBytes(final String entryName)
    throws IOException, ValidationException {
        return IOUtils.toByteArray(getInputStream(entryName));
    }

    public InputStream getInputStream(String entryName)
    throws IOException, ValidationException {
        // XXX: we should probably memoise this
        String absolutePath = getAbsolutePath();
        ZipFile zipFile = new ZipFile(absolutePath);

        // apparently ZipEntry names are separated using the system separator
        entryName = FilenameUtils.separatorsToSystem(entryName);
        ZipEntry entry = zipFile.getEntry(entryName);
        if (entry == null) {
            throw new IOException( "'" + entryName + "' not found in '"
                + absolutePath + "'");
        }
        return zipFile.getInputStream(entry);
    }

}
