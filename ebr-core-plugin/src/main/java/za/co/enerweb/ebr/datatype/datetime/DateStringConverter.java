package za.co.enerweb.ebr.datatype.datetime;

import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

/**
 * converts from a string to a date and back again.
 */
public class DateStringConverter extends DateTimeStringConverter {

    public static final String TYPE_ALIAS = "date-string";
    //public static final String DATE_FORMAT = "yyyy-MM-dd";

    @Override
    protected DateTimeFormatter instantiateFormatter() {
        return ISODateTimeFormat.date();
    }

    /* (non-Javadoc)
     * @see za.co.enerweb.ebr.api.datatype.ITypeConverter#getType()
     */
    @Override
    public String getType() {
        return TYPE_ALIAS;
    }

}
