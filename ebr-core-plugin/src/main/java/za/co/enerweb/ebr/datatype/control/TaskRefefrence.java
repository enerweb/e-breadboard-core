package za.co.enerweb.ebr.datatype.control;

import java.io.Serializable;

import lombok.Data;
import za.co.enerweb.ebr.datatype.ITypeConverter;

@Data
public class TaskRefefrence implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final String TYPE_ALIAS = "task";
    private String taskId;

    public TaskRefefrence(String taskId) {
        this.taskId = taskId;
    }

    public static class Converter implements ITypeConverter<TaskRefefrence> {
        private static final long serialVersionUID = 1L;

        /* (non-Javadoc)
         * @see za.co.enerweb.ebr.api.datatype.ITypeConverter
         *  #fromSimpleStructure(java.lang.Object)
         */
        @Override
        public TaskRefefrence fromSimpleStructure(String envId,
            String variableName, Object simpleStructure) {
            return new TaskRefefrence((String)simpleStructure);
        }

        /* (non-Javadoc)
         * @see za.co.enerweb.ebr.api.datatype.ITypeConverter
         *  #toSimpleStructure(java.lang.Object)
         */
        @Override
        public Object toSimpleStructure(String envId, String variableName,
            TaskRefefrence specificObject) {
            return specificObject.taskId;
        }

        /* (non-Javadoc)
         * @see za.co.enerweb.ebr.api.datatype.ITypeConverter#getType()
         */
        @Override
        public String getType() {
            return TaskRefefrence.class.getName();
        }
    }
}
