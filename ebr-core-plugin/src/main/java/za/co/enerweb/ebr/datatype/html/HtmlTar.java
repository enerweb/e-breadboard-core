package za.co.enerweb.ebr.datatype.html;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FilenameUtils;

import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.datatype.ITypeConverter;
import za.co.enerweb.ebr.datatype.dataresource.AbstractTarDataResource;
import za.co.enerweb.ebr.env.IEnvironmentManager;
import za.co.enerweb.ebr.exceptions.ServiceException;
import za.co.enerweb.toolbox.io.ZipUtils;

/*
 * FIXME: we need a HtmlTar example
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Slf4j
public class HtmlTar extends AbstractTarDataResource {

    public static final String HTML_DIR_TYPE_ALIAS = "html-dir";

    private String mainFile = "index.html";

    public HtmlTar(
        final String envId, final String path,
        final String mainFile) {
        super(envId, path);
        this.mainFile = mainFile;
    }

    @Override
    public String toString() {
        return mainFile;
    }

    @Deprecated
    public static HtmlTar fromScript(final String envId,
        final String dirAndMainfileCsv)
        throws IOException {
        return new HtmlDirConverter().fromSimpleStructure(envId, null,
            dirAndMainfileCsv);
    }

    public static HtmlTar fromHtmlDir(IEnvironmentManager envManager,
        final String envId, String dirname,
        final String mainFileName) throws IOException {
        dirname = FilenameUtils.normalize(dirname);
        File envDir = EbrFactory.getEnvironmentManager()
            .getEnvironmentDir(envId);
        // first check if its a relative path to the env
        File htmlDir = new File(envDir, dirname);
        if (!htmlDir.exists()) {
            // see if its an absolute path:
            htmlDir = new File(dirname);
        }
        // XXX: maybe rather throw exceptions with good messages so
        // the user can see what is wrong on the frontend
        if (!htmlDir.exists()) {
            log.error("Directory '" + dirname + "' does not exist.");
            return null;
        }
        if (!htmlDir.isDirectory()) {
            log.error("'" + dirname + "' is not a directory.");
            return null;
        }
        dirname = htmlDir.getName();

        File zipFile = File.createTempFile(dirname, ".zip", envDir);
        String newZipFilepath = FilenameUtils.normalize(
            zipFile.getAbsolutePath());
        log.debug("Zipping html dir to: " + newZipFilepath);
        ZipUtils.zipRecursively(htmlDir, zipFile, false);

        return new HtmlTar(envId, newZipFilepath,
            FilenameUtils.normalize(mainFileName));
    }

    /**
     * Supports a String or list source structure.
     * Converts to a List<String>.
     */
    public static class HtmlDirConverter implements ITypeConverter<HtmlTar> {
        /*
         * (non-Javadoc)
         * @see za.co.enerweb.ebr.api.datatype.ITypeConverter
         * #fromSimpleStructure(java.lang.Object)
         */
        @SuppressWarnings("unchecked")
        @Override
        public HtmlTar fromSimpleStructure(final String envId,
            final String variableName,
                final Object simpleStructure) {
            List<String> dirAndMainfile = null;
            if (simpleStructure instanceof String) {
                String dirAndMainfileCsv = (String) simpleStructure;
                dirAndMainfile = Arrays.asList(dirAndMainfileCsv.split(","));
            } else if (simpleStructure instanceof List<?>) {
                dirAndMainfile = (List<String>) simpleStructure;
            }

            if (dirAndMainfile != null) {
                if (dirAndMainfile.size() != 2) {
                    log.error("Html-dir of the wrong form, expected " +
                        "'dirname,mainfile' got: " + simpleStructure);
                    return null;
                }
                try {
                    return fromHtmlDir(EbrFactory.getEnvironmentManager(),
                        envId, dirAndMainfile.get(0),
                        dirAndMainfile.get(1));
                } catch (IOException e) {
                    throw new ServiceException("Could not zip dir", e);
                }
            }
            log.error("Don't know how to convert {} to {}.",
                simpleStructure, getClass().getSimpleName());
            return null;
        }

        /*
         * (non-Javadoc)
         * @see za.co.enerweb.ebr.api.datatype.ITypeConverter
         * #toSimpleStructure(java.lang.Object)
         */
        @Override
        public Object toSimpleStructure(final String envId,
            final String variableName, final HtmlTar specificObject) {
            try {
                File envDir =
                    EbrFactory.getEnvironmentManager()
                        .getEnvironmentDir(envId);
                File htmldir = new File(envDir, "html-dir-"
                        + za.co.enerweb.toolbox.id.IdGenerator
                            .getLocallyUniqueId());
                ZipUtils.unZip(specificObject.getInputStream(), htmldir);

                return Arrays.asList(new String[] {
                    htmldir.getAbsolutePath(),
                    specificObject.getMainFile()});
            } catch (IOException e) {
                throw new ServiceException("Could not read html zip", e);
            }
        }

        /*
         * (non-Javadoc)
         * @see za.co.enerweb.ebr.api.datatype.ITypeConverter#getType()
         */
        @Override
        public String getType() {
            return HTML_DIR_TYPE_ALIAS;
        }
    }
}
