package za.co.enerweb.ebr.datatype;


/**
 * converts from a number to an Integer
 */
public class IntegerConverter implements ITypeConverter<Integer> {

    /* (non-Javadoc)
     * @see za.co.enerweb.ebr.api.datatype.ITypeConverter
     *   #fromSimpleStructure(java.lang.Object)
     */
    @Override
    public Integer fromSimpleStructure(String envId, String variableName,
        Object simpleStructure) {
        if (simpleStructure instanceof Integer) {
            return (Integer) simpleStructure;
        }
        if (simpleStructure instanceof Number) {
            return ((Number)simpleStructure).intValue();
        }
        return Integer.parseInt(simpleStructure.toString());
    }

    /* (non-Javadoc)
     * @see za.co.enerweb.ebr.api.datatype.ITypeConverter
     *   #toSimpleStructure(java.lang.Object)
     */
    @Override
    public Object toSimpleStructure(String envId, String variableName,
        Integer specificObject) {
        return specificObject;
    }

    /* (non-Javadoc)
     * @see za.co.enerweb.ebr.api.datatype.ITypeConverter#getType()
     */
    @Override
    public String getType() {
        return Integer.class.getName();
    }

}
