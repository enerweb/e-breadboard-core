package za.co.enerweb.ebr.datatype.dataresource;

import lombok.Data;
import lombok.NoArgsConstructor;
import za.co.enerweb.ebr.datatype.ITypeConverter;

// XXX: maybe we need a generic objec<=>map converter..
@NoArgsConstructor
@Data
public class ResourceUrlConverter implements ITypeConverter<ResourceUrl> {
    private static final long serialVersionUID = 1L;

    @Override
    public ResourceUrl fromSimpleStructure(String envId,
        String variableName, Object simpleStructure) {
        if (simpleStructure instanceof String) {
            return new ResourceUrl(
                (String) simpleStructure);
        }
        return null;
    }

    @Override
    public Object toSimpleStructure(String envId, String variableName,
        ResourceUrl specificObject) {
        return specificObject.getUrl();
    }

    @Override
    public String getType() {
        return ResourceUrl.class.getName();
    }

}
