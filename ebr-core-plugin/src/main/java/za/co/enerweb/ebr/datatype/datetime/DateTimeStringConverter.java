package za.co.enerweb.ebr.datatype.datetime;

import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import za.co.enerweb.ebr.datatype.ITypeConverter;
import za.co.enerweb.ebr.exceptions.ServiceException;

/**
 * converts from a string to a date and back again.
 */
public class DateTimeStringConverter implements ITypeConverter<Date> {

    public static final String TYPE_ALIAS = "date-time-string";
    //public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";

    /**
     * It is not serializable, so we need to lazily instantiate it.
     */
    private transient DateTimeFormatter formatter;

    /* (non-Javadoc)
     * @see za.co.enerweb.ebr.api.datatype.ITypeConverter
     *   #fromSimpleStructure(java.lang.Object)
     */
    @Override
    public Date fromSimpleStructure(final String envId,
        final String variableName, final Object simpleStructure) {
        if (simpleStructure instanceof Date) {
            return (Date)simpleStructure;
        } else if (simpleStructure instanceof String) {
            return getFormatter().parseDateTime(
                (String) simpleStructure).toDate();
        } else {
            throw new ServiceException("Could not convert object of type "
                + simpleStructure.getClass() + " to a " + getType() + ".\n"
                + "Either cast it to String or try using a different type.");
        }
    }

    /* (non-Javadoc)
     * @see za.co.enerweb.ebr.api.datatype.ITypeConverter
     *   #toSimpleStructure(java.lang.Object)
     */
    @Override
    public Object toSimpleStructure(final String envId,
        final String variableName, final Date specificObject) {
        return getFormatter().print(new DateTime(specificObject));
    }

    /* (non-Javadoc)
     * @see za.co.enerweb.ebr.api.datatype.ITypeConverter#getType()
     */
    @Override
    public String getType() {
        return TYPE_ALIAS;
    }

    public final DateTimeFormatter getFormatter() {
        if (formatter == null) {
            formatter = instantiateFormatter();
        }
        return formatter;
    }

    protected DateTimeFormatter instantiateFormatter() {
        return ISODateTimeFormat.dateTime();
    }

}
