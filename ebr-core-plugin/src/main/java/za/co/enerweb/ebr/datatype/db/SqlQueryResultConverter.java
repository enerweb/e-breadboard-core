package za.co.enerweb.ebr.datatype.db;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import lombok.NoArgsConstructor;
import za.co.enerweb.ebr.datatype.ITypeConverter;

// XXX: maybe we need a generic objec<=>map converter..
@NoArgsConstructor
@Data
public class SqlQueryResultConverter implements ITypeConverter<SqlQueryResult> {
    private static final long serialVersionUID = 1L;

    @Override
    public SqlQueryResult fromSimpleStructure(String envId,
        String variableName, Object simpleStructure) {
        if (simpleStructure instanceof Map) {
            @SuppressWarnings("unchecked") Map<String, ?> info =
                (Map<String, ?>) simpleStructure;
            Boolean isJpaQuery = (Boolean) info.get("isJpaQuery");
            if (isJpaQuery == null) {
                isJpaQuery = Boolean.FALSE;
            }
            return new SqlQueryResult(
                (String) info.get("dbId"),
                (String) info.get("query"),
                isJpaQuery);
        }
        return null;
    }

    @Override
    public Object toSimpleStructure(String envId, String variableName,
        SqlQueryResult specificObject) {
        Map<String, Object> ret = new HashMap<String, Object>();
        ret.put("dbId", specificObject.getDbId());
        ret.put("query", specificObject.getQuery());
        ret.put("isJpaQuery", specificObject.isJpaQuery());
        return ret;
    }

    @Override
    public String getType() {
        return SqlQueryResult.class.getName();
    }

}
