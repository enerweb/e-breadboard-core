package za.co.enerweb.ebr.utils;

import java.util.Arrays;
import java.util.Collection;

import com.google.common.base.Joiner;

public class BreadCrumbUtils {
    public static final String BREAD_CRUMB_SEPARATOR = " > ";
    private static Joiner joiner = Joiner.on(BREAD_CRUMB_SEPARATOR);

    public static String join(Object... values) {
        return join(Arrays.asList(values));
    }

    public static String join(Collection<?> values) {
        return joiner.join(values);
    }
}
