package za.co.enerweb.ebr.datatype.db;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class SqlQueryResult implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final String TYPE_ALIAS = "sql-query-result";

    private String dbId;
    private String query;
    private boolean isJpaQuery = false; // else native

}
