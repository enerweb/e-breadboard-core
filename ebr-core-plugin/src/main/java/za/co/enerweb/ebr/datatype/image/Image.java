package za.co.enerweb.ebr.datatype.image;

import java.io.IOException;

import za.co.enerweb.ebr.datatype.ITypeConverter;
import za.co.enerweb.ebr.datatype.dataresource.AbstractDataResource;
import za.co.enerweb.ebr.exceptions.ServiceException;


public class Image extends AbstractDataResource
implements ITypeConverter<Image> {
    private static final long serialVersionUID = -5363312827006336954L;
    public static final String TYPE_ALIAS = "image";

    public Image() {
        super();
    }

    public Image(
        String envId, String path) {
        super(envId, path);
    }

    @Override
    public Image fromSimpleStructure(String envId, String variableName,
        Object simpleStructure) {
        return new Image(envId, (String) simpleStructure);
    }

    @Override
    public Object toSimpleStructure(String envId, String variableName,
        Image specificObject) {
        try {
            return ((Image)specificObject).getAbsolutePath();
        } catch (IOException e) {
            throw new ServiceException("Could not read image", e);
        }
    }

    @Override
    public String getType() {
        return getClass().getName();
    }
}
