package za.co.enerweb.ebr.datatype.control;

import java.io.Serializable;

import lombok.Data;
import za.co.enerweb.ebr.datatype.ITypeConverter;

@Data
public class MenuItemRefefrence implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final String TYPE_ALIAS = "menu";
    private String menuItemId;

    public MenuItemRefefrence(String menuItemId) {
        this.menuItemId = menuItemId;
    }

    public static class Converter
            implements ITypeConverter<MenuItemRefefrence> {
        private static final long serialVersionUID = 1L;

        /* (non-Javadoc)
         * @see za.co.enerweb.ebr.api.datatype.ITypeConverter
         *  #fromSimpleStructure(java.lang.Object)
         */
        @Override
        public MenuItemRefefrence fromSimpleStructure(String envId,
            String variableName, Object simpleStructure) {
            return new MenuItemRefefrence((String)simpleStructure);
        }

        /* (non-Javadoc)
         * @see za.co.enerweb.ebr.api.datatype.ITypeConverter
         *  #toSimpleStructure(java.lang.Object)
         */
        @Override
        public Object toSimpleStructure(String envId, String variableName,
            MenuItemRefefrence specificObject) {
            return specificObject.menuItemId;
        }

        /* (non-Javadoc)
         * @see za.co.enerweb.ebr.api.datatype.ITypeConverter#getType()
         */
        @Override
        public String getType() {
            return MenuItemRefefrence.class.getName();
        }
    }
}
