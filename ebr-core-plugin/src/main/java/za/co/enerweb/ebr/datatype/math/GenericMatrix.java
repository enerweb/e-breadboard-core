package za.co.enerweb.ebr.datatype.math;

import java.io.Serializable;

public abstract class GenericMatrix<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    protected int[] size;
    protected T[] data;

    public GenericMatrix() {

    }

    public GenericMatrix(int... size) {
        this.size = size;

        this.data = createArray(calcSize());
    }

    public GenericMatrix(T[] data, int... size) {
        this.size = size;
        this.data = data;
    }

    protected abstract T[] createArray(int size);

    protected abstract <C extends GenericMatrix<?>> C newMatrix(T[] data, int... size);

    protected int calcSize() {
        int total = 1;

        for (int i : size) {
            total *= i;
        }

        return total;
    }

    public GenericMatrix<T> slice(int dim1, int dim2, int... indexes) {
        if (indexes.length + 2 != size.length) {
            throw new IndexOutOfBoundsException(
                    "Incorrect number of indexes given");
        }

        T[] slice = createArray(size[dim1] * size[dim2]);
        int[] dimDositions = new int[2];
        int[] pos = new int[size.length];

        // find the positions of the 2 dimensions
        int cntr = 0;
        for (int i = 0; i < pos.length; i++) {
            if (i == dim1)
                dimDositions[0] = i;
            else if (i == dim2)
                dimDositions[1] = i;
            else
                pos[i] = indexes[cntr++];
        }

        cntr = 0;
        for (int i = 0; i < size[dim2]; i++) {
            for (int j = 0; j < size[dim1]; j++) {
                pos[dimDositions[0]] = j;
                pos[dimDositions[1]] = i;

                slice[cntr++] = get(pos);
            }
        }

        return newMatrix(slice, size[dim1], size[dim2]);
    }

    public void set(T v, int... pos) {
        data[pos2ind(pos)] = v;
    }

    public T get(int... pos) {
        int index = pos2ind(pos);
        T value = data[index];

        return value;
    }

    public int pos2ind(final int... pos) {
        int idx = 0;
        int factor = 1;
        for (int dim = 0; dim < pos.length; ++dim) {
            if (pos[dim] > size[dim]) {
                throw new IndexOutOfBoundsException(
                        "pos exceeded dimension for dimension " + dim + " ("
                                + pos[dim] + " > " + size[dim] + ")");
            }
            idx += (pos[dim]) * factor;
            factor *= size[dim];
        }
        return idx;
    }

    public int[] getSize() {
        return size;
    }

    public T[] getData() {
        return data;
    }

    public static class DoubleMatrix extends GenericMatrix<Double> {
        private static final long serialVersionUID = 1L;

        public DoubleMatrix() {
            super();
        }

        public DoubleMatrix(int... size) {
            super(size);
        }

        public DoubleMatrix(Double[] data, int... size) {
            super(data, size);
        }

        public DoubleMatrix(double[] data, int... size) {
            super();
            this.size = size;
            this.data = convert(data);
        }

        @SuppressWarnings("unchecked")
        @Override
        protected DoubleMatrix newMatrix(Double[] data, int... size) {
            return new DoubleMatrix(data, size);
        }

        protected Double[] convert(double[] data) {
            Double[] tmp = new Double[data.length];
            for (int i = 0; i < data.length; i++) {
                tmp[i] = data[i];
            }
            return tmp;
        }

        @Override
        protected Double[] createArray(int size) {
            return new Double[size];
        }

    }

}
