package za.co.enerweb.ebr.datatype.datetime;

import za.co.enerweb.ebr.datatype.DefaultConverter;


/**
 * does not do any conversion, assumes native implementations
 * can handle dates.
 */
public class DateTimeConverter extends DefaultConverter {

    public static final String TYPE_ALIAS = "date-time";

    /* (non-Javadoc)
     * @see za.co.enerweb.ebr.api.datatype.ITypeConverter#getType()
     */
    @Override
    public String getType() {
        return TYPE_ALIAS;
    }
}
