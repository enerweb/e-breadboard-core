# README #

e-breadboard is a extensible platform for building TomEE + Vaadin applications, handling a lot of common solution features.
It has been used to developed and maintained several solutions based on it eg. integration, data analysis, visualization, jython integration, R integration, calculation engines.

By itself it doesn't do much plugins and configurations need to be added to build up a solution.

Since is mainly used in house at the company I work and it was mainly developed as part of solutions (extending it as required) there is unfortunately not much online documentation and examples to speak off. Ask if you'd like to know something.

### What is this repository for? ###

This contains all the core functionality.

### How do I get set up? ###

* add repostitory to your maven settings.xml: https://maven.enerweb.co.za/mavenrepo/content/groups/public
* mvn install
* [TODO: add a simple example that can be fired up off the bat]

### Contribution guidelines ###

* Writing tests first is prefered.
* Code will be reviewed.
* Fairly strict formatting guidelines [TODO: add details]

### Who do I talk to? ###

* amanic@gmail.com