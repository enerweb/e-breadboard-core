package za.co.enerweb.ebr.gui.vaadin;

import za.co.enerweb.toolbox.vaadin.InfoComponent;
import za.co.enerweb.toolbox.vaadin.InfoDialog;

import com.vaadin.server.ExternalResource;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Link;
import com.vaadin.ui.VerticalLayout;

public class HelpWindow extends InfoDialog {
    private static final long serialVersionUID = 1L;

    public HelpWindow() {
        setCaption("Online e-breadboard forum.");
    }

    @Override
    public Component getInfoDialogContent() {
        VerticalLayout ret = new VerticalLayout();
        // getDialogLayout().setSpacing(false);
        // getDialogLayout().setMargin(false);
        // ret.setMargin(false);
        ret.addComponent(new InfoComponent(
            "You can post public questions below (by creating a new topic) "
                + "or search through previous discussions."));
        Embedded e = new Embedded("",
                new ExternalResource(
                "https://groups.google.com/forum/embed/?place=forum/"
                    + "e-breadboard&showsearch=true&showpopout=true"));
//        e.setAlternateText("Vaadin web site");
        e.setType(Embedded.TYPE_BROWSER);

        e.setWidth("640px");
        e.setHeight("480px");
        ret.addComponent(e);

        Link l = new Link("https://groups.google.com/d/forum/e-breadboard",
            new ExternalResource(
                "https://groups.google.com/d/forum/e-breadboard"));
        l.setTargetName("_blank");
        ret.addComponent(l);

        return ret;
    }
}
