package za.co.enerweb.ebr.gui.vaadin;

import java.io.IOException;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.server.IEbrServer;
import za.co.enerweb.ebr.server.SystemConfiguration;
import za.co.enerweb.toolbox.io.ResourceUtils;
import za.co.enerweb.toolbox.vaadin.InfoDialog;

import com.vaadin.ui.Component;
import com.vaadin.ui.Label;

@Slf4j
public class AboutWindow extends InfoDialog {
    private static final long serialVersionUID = 1L;

    public AboutWindow() {
        setCaption("About " + SystemConfiguration.getBrandName());
    }

    @Override
    public Component getInfoDialogContent() {
        Label label = new Label(getHtmlContent(), Label.CONTENT_XHTML);
        label.setWidth("350px");
        return label;
    }

    private String getHtmlContent() {
        try {
            return ResourceUtils.getResourceAsString(
                IEbrServer.EBR_ABOUT_HTML);
        } catch (IOException e) {
            String err = "Could not load help content.";
            log.error(err, e);
            return err;
        }
    }

}
