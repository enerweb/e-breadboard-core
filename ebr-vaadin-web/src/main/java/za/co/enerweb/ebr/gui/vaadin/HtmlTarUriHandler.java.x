package za.co.enerweb.ebr.gui.vaadin;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FilenameUtils;

import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.datatype.html.HtmlTar;

import com.vaadin.server.DownloadStream;
import com.vaadin.server.URIHandler;

@Slf4j
public class HtmlTarUriHandler implements URIHandler {

    private static final long serialVersionUID = 1L;

    public static final String URI_IDENTIFIER = "htmltar/";

    public DownloadStream handleURI(final URL context,
        final String relativeUri) {

        if (!relativeUri.startsWith(URI_IDENTIFIER)) {
            return null;
        }

        // this path was constructed in VaadinHtmlTarRenderer
        String[] pathComponents = relativeUri.split("/", 5);

        String envId = pathComponents[1];
        String varName = pathComponents[2];
        // needed so it gets refreshed if the resource id changes.
        //String resourceId = pathComponents[3];
        String path = pathComponents[4];

        Serializable variable =
            EbrFactory.getEnvironmentManager()
                .getVariableValue(envId, varName);

        if (variable instanceof HtmlTar) {
            HtmlTar tar = (HtmlTar) variable;

            try {
                return new DownloadStream(tar.getInputStream(path), null,
                    FilenameUtils.getName(path));

            } catch (Exception e) {

                log.error("Couldn't properly download html tar resource "
                        + path, e);

                return createEmptyDownloadStream();
            }

        } else {

            // TODO possibly handle other resources here
        }

        log.error("HtmlTarUriHandler found something that's not a HtmlTar : "
                        + variable);

        return createEmptyDownloadStream();
    }

    protected DownloadStream createEmptyDownloadStream() {
        // create a null input stream
        InputStream is = new ByteArrayInputStream(new byte[0]);

        return new DownloadStream(is, null, null);
    }
}
