package za.co.enerweb.ebr;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import lombok.val;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ApplicationPath("/r")
// can't seem to get rid of the /r without braking vaadin
public class EbrRestApplication extends Application {

    public Set<Class<?>> getClasses() {
        val ret = new HashSet<Class<?>>();
        try {
            ret.add(Class.forName("za.co.enerweb.ebr.task.TaskRestService"));
        } catch (ClassNotFoundException e) {
            log.debug("Could not load rest class", e);
        }
        return ret;
    }
}
