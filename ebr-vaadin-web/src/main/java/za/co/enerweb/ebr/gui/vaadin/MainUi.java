package za.co.enerweb.ebr.gui.vaadin;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static za.co.enerweb.toolbox.vaadin.Styles.STYLE_PADDED_CONTENT;

import java.util.List;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.VaadinConfigurationService;
import za.co.enerweb.ebr.gui.vaadin.menu.MainMenu;
import za.co.enerweb.ebr.gui.vaadin.messages.MEbrReload;
import za.co.enerweb.ebr.gui.vaadin.messages.MEbrSetMainComponent;
import za.co.enerweb.ebr.server.SystemConfiguration;
import za.co.enerweb.ebr.session.SessionMetadata;
import za.co.enerweb.ebr.utils.BreadCrumbUtils;
import za.co.enerweb.toolbox.vaadin.messaging.Message;
import za.co.enerweb.toolbox.vaadin.messaging.MessageBusFactory;
import za.co.enerweb.toolbox.vaadin.messaging.MessageListener;

import com.vaadin.annotations.Theme;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Slf4j
// @PreserveOnRefresh
@Theme("ebr")
public class MainUi extends UI
    implements
    // ParameterHandler,
    // BrowserCookies.UpdateListener,
    // IMainApplication,
    MessageListener {


    private static final String USER_SESSION_DEFAULT_NAME = "user-session";
    private static final String EBR_SESSION_ID_COOKIE_NAME = "EbrSessionId";

    private VerticalLayout mainLayout = new VerticalLayout();
    private MainMenu mainMenu = new MainMenu();

    // private BrowserCookies browserCookies;
    private Component currentView;
    private JobViewer jobViewer;
    private String taskPerspectiveId = null;
    private String sessionId = null;
    // I suppose we dont need to make this transient,
    // but I rather that it gets reloaded every time the server restarts.
    private transient SessionMetadata session = null;
    // private CSSInject cssInject = new CSSInject();
    private boolean firstload = true;
    // XXX: split out breadCrumbs into its own class
    private HorizontalLayout breadCrumbs = new HorizontalLayout();

    // private transient VaadinConfigurationService vaadinConfigurationService;

    @Override
    protected void init(VaadinRequest request) {
        this.setSizeFull();
        final Page page = Page.getCurrent();
        page.setTitle(
                SystemConfiguration.getBrandNameAndVersion());

        VaadinSession.getCurrent().setAttribute(
            EbrVaadinUtils.SESSION_ATTR__EBR_SESSION_ID,
            "testing123");

        // Access the HTTP service parameters
        // File baseDir = VaadinService.getCurrent().getBaseDirectory();

        // set this somehow for tabbed browsing,
        // maybe have a tools>new window menuitem that can do this
        // hopefully there is some detectin for this then it can be automatic
        // mainWindow.setName();

        mainLayout.addStyleName("ebr-main-layout");
        mainLayout.setSizeFull();
        setContent(mainLayout);

        MessageBusFactory.registerComponent(mainLayout);

        // add a handler which can load resources out of a the EbrHome
        // mainWindow.addURIHandler(new EbrHomeUriHandler(this));

        // add a handler which can load resources out of a HtmlTar
        // mainWindow.addURIHandler(new HtmlTarUriHandler());

        // mainWindow.addParameterHandler(this);

        // BrowserCookies widget (by default reads the cookie values)
        // browserCookies = new BrowserCookies();
        // Listen when cookies are available and read them
        // browserCookies.addListener(this);

        // mainMenu = new MainMenu();
        buildMainLayout();
    }

    @Override
    public void attach() {
        super.attach();
        // buildMainLayout(); session not set here so messagebus breaks
    }

    // public String getTheme() {
    // // not sure if this works, maybe it must go in the
    // // UIProvider.getTheme()
    // // https://vaadin.com/forum/#!/thread/1154742
    // // https://vaadin.com/forum/#!/thread/1803904/1805113
    // // FIXME: return getVaadinConfigurationService().getTheme();
    // return "ebr";
    // }

    private void buildMainLayout() {

        mainLayout.setSizeFull();

        HorizontalLayout topBar = new HorizontalLayout();
        topBar.addStyleName("ebr-top-bar");
        topBar.setSpacing(false);
        topBar.setMargin(false);
        topBar.setWidth("100%");
        HorizontalLayout toolbar = createToolbar();
        topBar.addComponent(toolbar);
        topBar.setExpandRatio(toolbar, 1.5f);
        topBar.setComponentAlignment(toolbar,
            Alignment.MIDDLE_LEFT);
        HorizontalLayout contextbar = createContextbar();
        topBar.addComponent(contextbar);
        topBar.setExpandRatio(contextbar, 1f);

        mainLayout.addComponent(topBar);


        // mainLayout.addComponent(browserCookies);

        // mainLayout.addComponent(applyCss());

        breadCrumbs.setSpacing(true);
        breadCrumbs.addStyleName(STYLE_PADDED_CONTENT);

        MessageBusFactory.getInstance(this).addNonComponentMessageListerner(
            this);

        if (currentView != null) {
            setMainComponent();
        }

        // activate the initial screen from here
        mainMenu.activateMenuItem(
            VaadinConfigurationService.getInstance().getStartMenuItemId()
            );
    }

    // public CSSInject applyCss() {
    // // TODO: we can maybe just link to it from EbrVaadinServlet
    // // page.write("<link rel=\"stylesheet\" type=\"text/css\"
    // // href=\"/TestMap/VAADIN/themes/reindeer/lib/css/jquery.jqplot.css\"
    // // />");
    // // then we may be able to refresh with ctrl+shift+r
    // if (cssInject == null) {
    // // this happens when the server was restarted
    // cssInject = new CSSInject();
    // }
    // cssInject.setValue(getVaadinConfigurationService().getCustomCss());
    // return cssInject;
    // }

    public HorizontalLayout createToolbar() {
        HorizontalLayout toolbar = new HorizontalLayout();
        toolbar.setStyleName("ebr-toolbar");
        toolbar.setMargin(true);

        List<String> emptyList = emptyList();
        Component menu = mainMenu.getComponent(
            VaadinConfigurationService.getInstance().getHideMenuItemIds()
            );
        toolbar.addComponent(menu);
        menu.setWidth("100%");

        toolbar.setWidth("100%");
        toolbar.setComponentAlignment(menu,
            Alignment.MIDDLE_LEFT);
        // Label placeholder = new Label(""); // just a placeholder
        // toolbar.addComponent(placeholder);
        // toolbar.setExpandRatio(placeholder, 1);

        return toolbar;
    }

    public void addBreadCrumb(String caption) {
        if (breadCrumbs.iterator().hasNext()) {
            caption = BreadCrumbUtils.BREAD_CRUMB_SEPARATOR + caption;
        }
        breadCrumbs.addComponent(createBreadCrumbComponent(caption));
    }

    public void setBreadCrumb(String caption, final int index) {
        if (index > 0 && !caption.isEmpty()) {
            caption = BreadCrumbUtils.BREAD_CRUMB_SEPARATOR + caption;
        }
        Label breadCrumb = createBreadCrumbComponent(caption);
        try {
            Component currentComponent = breadCrumbs.getComponent(index);
            breadCrumbs.replaceComponent(currentComponent, breadCrumb);
        } catch (IndexOutOfBoundsException e) {
            breadCrumbs.addComponent(breadCrumb);
        }
    }

    public void setBreadCrumbs(final String... breadCrumbStrings) {
        setBreadCrumbs(asList(breadCrumbStrings));
    }

    public void setBreadCrumbs(final List<String> breadCrumbStrings) {
        breadCrumbs.removeAllComponents();
        for (String crumb : breadCrumbStrings) {
            addBreadCrumb(crumb);
        }
    }

    private Label createBreadCrumbComponent(String caption) {
        Label breadCrumb = new Label(caption);
        breadCrumb.addStyleName("ebr-bread-crumb");
        return breadCrumb;
    }

    public HorizontalLayout createContextbar() {
        HorizontalLayout contextbar = new HorizontalLayout();
        contextbar.setStyleName("ebr-contextbar");
        contextbar.setMargin(false);

        // contextbar.setWidth("100%");
        // contextbar.setHeight("100%");
        contextbar.setSizeFull();
        breadCrumbs.setMargin(false);
        // breadCrumbs.setWidth("100%");
        breadCrumbs.addStyleName("ebr-bread-crumbs");
        contextbar.addComponent(breadCrumbs);
        contextbar.setComponentAlignment(breadCrumbs,
            Alignment.MIDDLE_CENTER);
        contextbar.setExpandRatio(breadCrumbs, 0.8f);

        // Embedded image =
        // new Embedded(null, new ThemeResource("img/logo.png"));
        // image.setStyleName("ebr-logo");
        // contextbar.addComponent(image);
        // contextbar.setComponentAlignment(image, Alignment.MIDDLE_RIGHT);
        // contextbar.setExpandRatio(image, 0.2f);
        return contextbar;
    }

    public JobViewer getJobViewer() {
        if (jobViewer == null) {
            jobViewer = new JobViewer();
        }
        return jobViewer;
    }

    // private void setMainComponent(final Component c,
    // final String... breadCrumbStrings) {
    // setBreadCrumbs(breadCrumbStrings);
    // setMainComponent(c);
    // }

    public void setMainComponent(final Component c) {
        /**
         * Only set the component if it differs from the current one. Otherwise
         * all child components' detach() function gets called which causes
         * problems with embedded components and application resources.
         */
        if (currentView != c) {
            if (currentView != null) {
                mainLayout.removeComponent(currentView);
            }
            currentView = c;
            setMainComponent();
            // applyCss();
        }
    }

    private void setMainComponent() {
        mainLayout.addComponent(currentView);
        /* Allocate all available extra space to the horizontal split panel */
        mainLayout.setExpandRatio(currentView, 1);
    }

    // /**
    // * This method is called often by vaadin, not just on startup
    // * @see
    // com.vaadin.terminal.ParameterHandler#handleParameters(java.util.Map)
    // */
    // @Override
    // public void handleParameters(final Map<String, String[]> parameters) {
    // // make sure this is set after eclipse reload...
    // MessageBusFactory.addComponent(this.getMainWindow());
    // // MessageBusFactory.fireMessage(this.getMainWindow(),
    // // new MCgiParameters(parameters));
    // taskPerspectiveId = getParameter(parameters, "task_perspective_id");
    //
    // applyCss();
    // // if we don't check, this gets called at weird times..
    // if (firstload) {
    // buildMainLayout();
    // // this is the main page we show the user
    // showRunTaskView();
    // String menuItemId = getParameter(parameters,
    // "start_at_menu_item_id");
    // if (menuItemId == null) {
    // menuItemId = getVaadinConfigurationService()
    // .getStartMenuItemId();
    // }
    // activateMenuItem(menuItemId);
    // firstload = false;
    // }
    // }

    private String getParameter(final Map<String, String[]> parameters,
        String parameterName) {
        String ret = null;
        String[] values = parameters.get(parameterName);
        if (values != null && values.length > 0) {
            ret = values[0];
        }
        return ret;
    }

    // @Override
    // public Window getWindow(String name) {
    // // log.debug("getWindow: " + name);
    // // If we already have the requested window, use it
    // Window w = super.getWindow(name);
    // boolean wasNull = w == null;
    // w = VaadinPlugin.getWindowService().getWindow(this, w, name);
    // if (wasNull && w != null) {
    // // set windows name to the one requested
    // // log.debug("window name: " + w.getName());
    // if (w.getName() == null) {
    // w.setName(name);
    // }
    //
    // // add it to this application
    // addWindow(w);
    //
    // // w.open(new ExternalResource(
    // // "http://www.google.com"),
    // // "_top");
    // }
    // return w;
    // }

    // /**
    // *
    // */
    // private void showRunTaskView() {
    // AbstractMenuItem runTaskMenuItem = mainMenu.getRunTaskMenuItem();
    // // FIXME: runTaskMenuItem.setApplication(this);
    // runTaskMenuItem.menuSelected();
    // setBreadCrumbs(ChooseAndRunTaskView.RUN_TASK_CONTEXT_CAPTION);
    // }

    public String getTaskPerspectiveId() {
        return taskPerspectiveId;
    }

    public void setTaskPerspectiveId(final String taskPerspectiveId) {
        this.taskPerspectiveId = taskPerspectiveId;
    }

    // /*
    // * (non-Javadoc)
    // * @see
    // * org.vaadin.browsercookies.BrowserCookies.UpdateListener#cookiesUpdated
    // * (org.vaadin.browsercookies.BrowserCookies)
    // */
    // @Override
    // public void cookiesUpdated(final BrowserCookies browserCookies) {
    // // don't set it multiple times
    // if (sessionId == null || session == null) {
    // log.debug("cookiesUpdated()");
    // sessionId = browserCookies.getCookie(EBR_SESSION_ID_COOKIE_NAME);
    // // make sure session exist
    // session = null;
    // getSessionId();
    // }
    // }

    public String getSessionId() {
        if (sessionId == null) {
            newSession();
        }

        // avoid loading the session too much. assume if we have a instance
        // its ok. maybe if someone else deletes it, we may get unexpected
        // results because of this.
        if (session == null) {
            // make sure the session exists
            session = EbrFactory.getSessionManager().getOrCreateSession(
                sessionId, USER_SESSION_DEFAULT_NAME);

            // session may have not existed anymore, so make sure we use the one
            // we got back from the session manager
            if (!sessionId.equals(session.getId())) {
                setSessionId(session.getId());
            }
        }

        // log.debug("Loaded environment.\n" +
        // OrchestrationEngineFactory.getEnvironmentManager().getEnvironment(
        // session.getEnvId()).variablesToString());
        return sessionId;
    }

    private void newSession() {
        session = EbrFactory.getSessionManager().newSession(
            USER_SESSION_DEFAULT_NAME);
        setSessionId(session.getId());
    }

    private void setSessionId(final String sessionId) {
        this.sessionId = sessionId;
        // don't set it multiple times
        // if (!sessionId.equals(browserCookies.getCookie(
        // EBR_SESSION_ID_COOKIE_NAME))) {
        // MutableDateTime expirationDate = new MutableDateTime();
        // expirationDate.addDays(30);
        // browserCookies.setCookie(EBR_SESSION_ID_COOKIE_NAME, sessionId,
        // expirationDate.toDate());
        // log.debug("Setting sessionId in cookie: " + sessionId);
        // }
    }

    public void resetSession() {
        session = EbrFactory.getSessionManager().resetSession(
            getSessionId());
        setSessionId(session.getId());
        Notification.show("Session has been reset.", Type.TRAY_NOTIFICATION);
    }

    // @Override
    // public String getVersion() {
    // return SystemConfiguration.getVersion();
    // }


    // private VaadinConfigurationService getVaadinConfigurationService() {
    // if (vaadinConfigurationService == null) {
    // vaadinConfigurationService =
    // VaadinConfigurationService.getInstance();
    // }
    // return vaadinConfigurationService;
    // }

    // @Override
    // public void activateMenuItem(String id) {
    // if (id != null) {
    // mainMenu.activateMenuItem(id);
    // }
    // }

    @Override
    public void onMessage(Message message) {
        if (message instanceof MEbrReload) {
            VaadinConfigurationService.reload();
            mainMenu.reload(VaadinConfigurationService.getInstance()
                .getHideMenuItemIds());
            // applyCss();
        } else if (message instanceof MEbrSetMainComponent) {
            MEbrSetMainComponent m = (MEbrSetMainComponent) message;
            setMainComponent(m.getComponent());
            setBreadCrumbs(m.getBreadCrumbStrings());
        }
    }

}
