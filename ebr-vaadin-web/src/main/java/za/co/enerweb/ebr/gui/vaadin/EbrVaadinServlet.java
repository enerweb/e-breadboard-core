package za.co.enerweb.ebr.gui.vaadin;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import lombok.extern.slf4j.Slf4j;

import com.vaadin.server.VaadinServlet;

/*
 * @WebServlet(urlPatterns = {
 * "/*"
 * // "/myui/*",
 * // "", "/", "/VAADIN/*", "/UIDL/*", "/HEARTBEAT/*"
 * }, loadOnStartup = 0,
 * asyncSupported = true)
 * @VaadinServletConfiguration(
 * productionMode = false,
 * ui = MainUi.class,
 * widgetset = "za.co.enerweb.ebr.gui.vaadin.widgetset.Ebr_vaadin_webWidgetset",
 * resourceCacheTime = 43200)
 */
@Slf4j
public class EbrVaadinServlet extends VaadinServlet {
    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
    }

    /*
        @Override
        protected void writeAjaxPageHtmlVaadinScripts(Window window,
            String themeName, Application application, BufferedWriter page,
            String appUrl, String themeUri, String appId,
            HttpServletRequest request) throws ServletException, IOException {
            super.writeAjaxPageHtmlVaadinScripts(window, themeName, application,
                page,
                appUrl, themeUri, appId, request);

            // add javascripts to the main application page.
            IHomeService homeservice = EbrFactory.getHomeService();
            page.write("<script type=\"text/javascript\">EBR_CONTEXT_PATH='"
                + request.getContextPath() + "'</script>\n");
            for (HomeFile homeFile : homeservice.findFiles(
                new HomeFileSpec(HomeFileCategory.FRONTEND,
                    VaadinUrlService.VAADIN,
                    VaadinUrlService.JAVASCRIPT), "js")) {
                String scriptPath = VaadinUrlService.joinPaths(
                    request.getContextPath(), VaadinUrlService.EBR,
                    VaadinUrlService.JAVASCRIPT,
                    homeFile.getRelPath());
                log.debug("using javascript: " + scriptPath);
                page.write("<script type=\"text/javascript\" src=\""
                    + scriptPath + "\"></script>\n");
            }
        }
        */

    /*
     * https://vaadin.com/forum/#!/thread/1803904/1805113
    protected void onVaadinSessionStarted(WrappedHttpServletRequest request,
    4            VaadinServletSession session) throws ServletException {
    5        session.addUIProvider(new AbstractUIProvider() {
    6            public Class<? extends UI> getUIClass(WrappedRequest request) {
    7                String path = request.getRequestPathInfo();
    8                if (path != null) {
    9                    if (path.startsWith("/ui1"))
    10                        return UI1.class;
    11                    if (path.startsWith("/ui2"))
    12                        return UI2.class;
    13                }
    14                return null; // No default UI set
    15            }
    16        });
    17        super.onVaadinSessionStarted(request, session);
    18    }
    */
}
