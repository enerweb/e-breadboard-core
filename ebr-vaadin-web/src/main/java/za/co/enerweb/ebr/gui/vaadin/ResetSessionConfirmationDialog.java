package za.co.enerweb.ebr.gui.vaadin;

import za.co.enerweb.toolbox.vaadin.ConfirmationDialog;
import za.co.enerweb.toolbox.vaadin.ConfirmationDialog.ConfirmationListener;

public final class ResetSessionConfirmationDialog extends ConfirmationDialog
    implements ConfirmationListener {

    private static final long serialVersionUID = 1L;

    public ResetSessionConfirmationDialog() {
        super("Reset current session.",
            "Are you sure that you want to delete all data in the "
            + "current session?");
        setWidth(50, Unit.PERCENTAGE);
        setHeight(6, Unit.CM);
        addConfirmationListener(this);
    }

    @Override
    public void onOk() {
        // FIXME: send message
        // ((MainUi) getApplication()).resetSession();
    }

    @Override
    public void onCancel() {
    }
}
