package za.co.enerweb.ebr.gui.vaadin.menu;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.vaadin.ui.Component;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MenuActionCommand implements Command {
    private static final long serialVersionUID = 1L;
    private Component parentComponent;
    private AbstractMenuItem menuItem;

    @Override
    public void menuSelected(final MenuItem selectedItem) {
        // menuItem.setApplication(parentComponent.getApplication());
        menuItem.menuSelected();
    }

}
