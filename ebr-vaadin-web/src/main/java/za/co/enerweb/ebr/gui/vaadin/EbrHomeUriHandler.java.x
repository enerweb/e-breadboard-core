package za.co.enerweb.ebr.gui.vaadin;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FilenameUtils;

import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.VaadinUrlService;
import za.co.enerweb.ebr.server.HomeFile;
import za.co.enerweb.ebr.server.HomeFileCategory;
import za.co.enerweb.ebr.server.HomeFileSpec;
import za.co.enerweb.ebr.server.IHomeService;

import com.vaadin.server.DownloadStream;
import com.vaadin.server.URIHandler;

/**
 * FIXME: write integration testing to make sure this works!!!!
 * (will wait for the tomEE)
 */
@Slf4j
public class EbrHomeUriHandler implements URIHandler {
    private static final long serialVersionUID = 1L;

    public static final String URI_IDENTIFIER = VaadinUrlService.EBR + "/";
    private static final String DEPRECATED_EBR_HOME = "ebr_home";
    private static final String DEPRECATED_VAR_WEBCONTENT
        = "var/web-content";
    private static final String DEPRECATED_LIB_JAVASCRIPT = "lib/javascript";

    // private transient VaadinUrlService urlservice;

    public EbrHomeUriHandler(MainUi ui) {
        // urlservice = VaadinUrlService.getInstance(application);
    }

    public DownloadStream handleURI(final URL context,
        final String relativeUri) {
        if (relativeUri.startsWith(URI_IDENTIFIER)) {
            String[] pathComponents = relativeUri.split("/", 3);
            String category = pathComponents[1];
            String categoryRelPath = pathComponents[2];
            // FIXME: we need to get a list of supported HomeFileSpecs
            // and see if this url matches any of them
            // then we can read the homefiles.
            // String webContentRelPath = urlservice.getWebContentRelPath();

            // XXX: I suppose we may need to put in some filters
            // or access control
            // here, as soon as we know what we want to restrict.
            // I think we just need var/webcontent and the envs

            // make sure we can run R annimations after we do this,
            // do we also want to
            // support the resources dir?! - email the list about this
            // as soon as it is working.. and maybe after the api is done...
            if (category.equals(VaadinUrlService.WEB_CONTENT)) {
                return getHomeResource(new HomeFileSpec(
                    HomeFileCategory.WEB_CONTENT, categoryRelPath));
            } else if (category.equals(VaadinUrlService.JAVASCRIPT)) {
                return getHomeResource(new HomeFileSpec(
                    HomeFileCategory.FRONTEND, VaadinUrlService.VAADIN,
                    VaadinUrlService.JAVASCRIPT, categoryRelPath));
            }
            log.error("Unsupported e-breadboard home file: "
                + category);
            return createEmptyDownloadStream();
        }

        // only deprecate old urls..
        // http://localhost:17080/ebr-vaadin-web/ebr_home/var/web-content/x.html
        if (relativeUri.startsWith(DEPRECATED_EBR_HOME)) {
            String[] pathComponents = relativeUri.split("/", 2);
            String ebrHomeRelativePath = pathComponents[1];
            if (ebrHomeRelativePath.startsWith(DEPRECATED_VAR_WEBCONTENT)) {
                String categoryRelPath = ebrHomeRelativePath.substring(
                    DEPRECATED_VAR_WEBCONTENT.length());
                log.warn("Deprecated url path[1], rather use /"
                    + URI_IDENTIFIER
                    + VaadinUrlService.WEB_CONTENT + categoryRelPath
                    + " :\n[1] "
                    + relativeUri);
                return getHomeResource(new HomeFileSpec(
                    HomeFileCategory.WEB_CONTENT, categoryRelPath));
            } else if (ebrHomeRelativePath.startsWith(
                DEPRECATED_LIB_JAVASCRIPT)) {
                String categoryRelPath = ebrHomeRelativePath.substring(
                    DEPRECATED_LIB_JAVASCRIPT.length());
                log.warn("Deprecated url path[1], files in the designated "
                    + "javascript directory will get loaded automatically. "
                    + "Javascript files that must be visible externally must "
                    + "go in /"
                    + URI_IDENTIFIER
                    + VaadinUrlService.WEB_CONTENT + categoryRelPath
                    + " :\n[1] "
                    + relativeUri);
                return getHomeResource(new HomeFileSpec(
                    HomeFileCategory.WEB_CONTENT, categoryRelPath));
            }
            log.error("Unsupported e-breadboard home file: "
                + context + "<" + ebrHomeRelativePath + ">");
            return createEmptyDownloadStream();
        }

        return null;

    }

    public DownloadStream getHomeResource(HomeFileSpec hfs) {
        IHomeService homeService = EbrFactory.getHomeService();
        HomeFile hf = homeService.findFile(hfs);
        if (hf.exists()) {
            try {
            DownloadStream downloadStream = new DownloadStream(
                hf.getInputStream(), null,
                    FilenameUtils.getName(hf.getName()));
            downloadStream.setCacheTime(0);
            return downloadStream;
            } catch (Exception e) {
                log.error("Couldn't properly download e-breadboard-home"
                    + " resource " + hf, e);
                return createEmptyDownloadStream();
            }
        } else {
            log.error("Couldn't properly download e-breadboard-home resource "
                + hf);
            return createEmptyDownloadStream();
        }
    }

    protected DownloadStream createEmptyDownloadStream() {
        InputStream is = new ByteArrayInputStream(new byte[0]);
        return new DownloadStream(is, null, null);
    }
}
