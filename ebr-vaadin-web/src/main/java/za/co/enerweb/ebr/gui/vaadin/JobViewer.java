package za.co.enerweb.ebr.gui.vaadin;

import com.vaadin.ui.HorizontalSplitPanel;

public class JobViewer extends HorizontalSplitPanel {
    private static final long serialVersionUID = 1L;

    public JobViewer(){
        setSplitPosition(20, Unit.PERCENTAGE);
    }
}
