package za.co.enerweb.ebr.gui.vaadin.menu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.gui.vaadin.AboutWindow;
import za.co.enerweb.ebr.gui.vaadin.HelpWindow;
import za.co.enerweb.ebr.gui.vaadin.ResetSessionConfirmationDialog;
import za.co.enerweb.ebr.server.Registrar;
import za.co.enerweb.toolbox.timing.Stopwatch;

import com.vaadin.ui.Component;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;

/**
 * All classes extending AbstractMenuFactory will be loaded like magic.
 */
@Slf4j
public class MainMenu extends AbstractMenuFactory implements Serializable {

    private MenuItemContainer mainMenu;

    private MenuBar menubar = new MenuBar();
    {
        menubar.addStyleName("ebr-main-menu");
    }

    // @Getter
    // private AbstractMenuItem runTaskMenuItem =
    // new ActivateScreenMenuItem(
    // 10, ChooseAndRunTaskView.RUN_TASK_CONTEXT_CAPTION,
    // ChooseAndRunTaskView.class);

    private List<String> hideMenuItemIds;

    private boolean isMenuItemHidden(AbstractMenuItem menuItem) {
        return hideMenuItemIds.contains(menuItem.getId());
    }

    public Component getComponent(List<String> hideMenuItemIds) {
        reload(hideMenuItemIds);
        return menubar;
    }

    public void reload(List<String> hideMenuItemIds) {
        this.hideMenuItemIds = hideMenuItemIds;
        populateLogicalMenu();
        mainMenu.sort();
        menubar.removeItems();
        for (final AbstractMenuItem menuItem : mainMenu.getMenuItems()) {
            if (isMenuItemHidden(menuItem)) {
                continue;
            }
            if (menuItem instanceof MenuItemContainer) {
                MenuItem menuBarItem = menubar.addItem(
                    menuItem.getCaption(), null);
                addSubMenu(menuBarItem, (MenuItemContainer) menuItem);
            } else {
                menubar.addItem(menuItem.getCaption(),
                    createActionMenuItem(menuItem));
            }
        }
    }

    private Command createActionMenuItem(final AbstractMenuItem menuItem) {
        Command menuCommand = new MenuActionCommand(menubar, menuItem);
        return menuCommand;
    }

    /**
     * @param menuBarItem
     * @param menuItem
     */
    private void addSubMenu(final MenuItem parentMenuBarItem,
        final MenuItemContainer menuItemContainer) {
        List<AbstractMenuItem> menuItems = menuItemContainer.getMenuItems();
        menuItemContainer.sort();
        for (final AbstractMenuItem menuItem :
            menuItems) {
            if (isMenuItemHidden(menuItem)) {
                continue;
            }
            if (menuItem instanceof MenuItemContainer) {
                MenuItem menuBarItem = parentMenuBarItem.addItem(
                    menuItem.getCaption(), null);
                addSubMenu(menuBarItem, (MenuItemContainer) menuItem);
            } else {
                parentMenuBarItem.addItem(menuItem.getCaption(),
                    createActionMenuItem(menuItem));
            }
        }
    }

    private void populateLogicalMenu() {
        Stopwatch sw = new Stopwatch();
        int count = 0;
        mainMenu = new MenuItemContainer(
            0, MAIN_MENU_NAME);

        Set<Class<? extends AbstractMenuFactory>> factoryClasses =
            Registrar.getReflections().getSubTypesOf(
                AbstractMenuFactory.class);

        List<AbstractMenuFactory> factories =
            new ArrayList<AbstractMenuFactory>(factoryClasses.size());

        for (Class<? extends AbstractMenuFactory> factoryClass :
            factoryClasses) {
            if (factoryClass == null) {
                continue;
            }
            AbstractMenuFactory factory = null;
            try {
                // log.debug("found menu factory: " + factoryClass);
                factory = factoryClass.newInstance();
            } catch (Exception e) {
                log.warn("Could not instantiate menu item factory:"
                    + factoryClass.getName(), e);
                continue;
            }
            factories.add(factory);
        }
        Collections.sort(factories);
        for (AbstractMenuFactory factory : factories) {
            factory.addToMenu(MAIN_MENU_NAME, mainMenu);
            count++;
        }
        if (log.isDebugEnabled()) {
            log.debug("Registering {} menu factory(ies) took {} ms", count,
                sw.getTotalMilliSeconds());
        }
    }

    /**
     * add the core menu items
     */
    @Override
    public void addToMainMenu(final MenuItemContainer menuContainer) {
        // menuContainer.addMenuItem(runTaskMenuItem);

        addToolsMenu(menuContainer);

        // the jobviewer isn't implemented yet, so don't show this
        // menuContainer.addMenuItem(
        // new ActivateScreenMenuItem(30, "Jobs",
        // JobViewer.class));

        menuContainer.findOrAddSubMenu(2000,
            CONFIG_MENU_NAME);


        MenuItemContainer help = menuContainer.findOrAddSubMenu(2010,
            "Help");

        help.addMenuItem(
            new ShowWindowMenuItem(1000, "Online e-breadboard Help",
                HelpWindow.class));

        help.addMenuItem(
            new ShowWindowMenuItem(1010, "About",
                AboutWindow.class));

    }

    /**
     * If the id was not unique, then all items with this id will be activated
     * sequentially.
     * @param id
     */
    public void activateMenuItem(String id) {
        // mainMenu.setApplication(menubar.getApplication());
        mainMenu.menuSelected(id);
    }

    private void addToolsMenu(final MenuItemContainer menuContainer) {
        MenuItemContainer tools = menuContainer.findOrAddSubMenu(20,
            TOOLS_MENU_NAME);

        tools.addMenuItem(
            new ReloadMenuItem(100, "Reload"));
        // refreshB.setDescription("Clear the task library, " +
        // "scan for tasks and parse their meta data.");

        tools.addMenuItem(
            new ShowWindowMenuItem(110, "Reset Session",
                ResetSessionConfirmationDialog.class));

        // tools.addMenuItem(
        // new ShowWindowMenuItem(120, "Guid Generator",
        // GuidGeneratorWindow.class));
    }

    public int getRank() {
        return 0;
    }

}
