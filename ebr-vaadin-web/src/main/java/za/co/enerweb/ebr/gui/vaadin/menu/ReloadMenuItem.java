package za.co.enerweb.ebr.gui.vaadin.menu;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.gui.vaadin.EbrVaadinUtils;

@SuppressWarnings("serial")
@Slf4j
public class ReloadMenuItem extends AbstractMenuItem {

    public ReloadMenuItem(final int rank, final String caption) {
        super(ReloadMenuItem.class.getName(), rank, caption);
    }

    @Override
    public void menuSelected() {
        new EbrVaadinUtils().reloadEBreadboard();
    }
}
