package za.co.enerweb.ebr.gui.vaadin;

import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.toolbox.vaadin.Heading;
import za.co.enerweb.toolbox.vaadin.InfoDialog;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;

public class GuidGeneratorWindow extends InfoDialog {
    private static final long serialVersionUID = 1L;
    private VerticalLayout layout;

    public GuidGeneratorWindow() {
        setWidth(50, UNITS_PERCENTAGE);
    }

    /* (non-Javadoc)
     * @see za.co.enerweb.toolbox.vaadin.EasyCloseWindow#init()
     */
    @SuppressWarnings("serial")
    @Override
    public Component getInfoDialogContent() {
        setButtonsAtTheBottom(false);
        layout = new VerticalLayout();
        setCaption("New Globally unique id.");
        addButton(new Button("Another One", new Button.ClickListener() {

            @Override
            public void buttonClick(final ClickEvent event) {
                addGuid();
            }
        }));
        addGuid();
        layout.setSizeFull();
        return layout;
    }

    private void addGuid() {
        String uuid = EbrFactory
            .getEbrServer().getNewGuid();

        Heading heading = new Heading(uuid);
        setWidth(uuid.length(), Unit.EM);
        layout.addComponent(heading);
        layout.setSizeFull();
        center();
    }

}
