package za.co.enerweb.ebr.gui.vaadin.task;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import za.co.enerweb.ebr.VaadinPlugin;
import za.co.enerweb.ebr.exceptions.TaskExecutionException;
import za.co.enerweb.ebr.exceptions.TaskInstantiationException;
import za.co.enerweb.ebr.exceptions.UnknownTaskExecutionException;
import za.co.enerweb.ebr.gui.vaadin.ITaskView;
import za.co.enerweb.ebr.task.TaskExecutionMetadata;
import za.co.enerweb.ebr.layout.vaadin.TaskBasedTaskLayout;
import za.co.enerweb.ebr.server.AbstractOrchestrationEngineTest;
import za.co.enerweb.ebr.session.SessionManagerFactory;
import za.co.enerweb.ebr.session.SessionMetadata;
import za.co.enerweb.ebr.task.TaskExecutionStatus;
import za.co.enerweb.ebr.task.TaskRegistryService;
import za.co.enerweb.ebr.tasks.rest.HelloWorld;

import com.vaadin.Application;

public class JobControllerTest extends AbstractOrchestrationEngineTest {
    JobController jobController;
    SessionMetadata session;

    ITaskView view = new ITaskView(){

        @Override
        public void error(final String msg, final Exception e) {
        }

        @Override
        public Application getApplication() {
            return null;
        }

        @Override
        public void resetTask(final String taskId) {
        }

        @Override
        public void setTaskId(final String taskId) {
        }
    };

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUpAfterInjection() {
        VaadinPlugin.onEbrReload();
        TaskRegistryService.registerTask(HelloWorld.getInitTaskMetaData());
        session = SessionManagerFactory.getInstance().newSession("test");
        jobController = new JobController() {
            private static final long serialVersionUID = 1L;

            @Override
            protected String getTaskPerspectiveId() {
                return null;
            }

            @Override
            protected String getSessionId() {
                return session.getId();
            }

            @Override
            public SessionMetadata getSession() {
                return session;
            }
        };
        try {
            jobController.init(view, HelloWorld.ID);
        } catch (TaskInstantiationException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testInit() {
        assertEquals(jobController.getLayout().getClass(),
            TaskBasedTaskLayout.class);
        assertEquals(jobController.getTaskMetadata().getId(),
            HelloWorld.ID);
    }

    @Test
    public void testRun() throws TaskExecutionException, UnknownJobException,
        InterruptedException {
        TaskExecutionMetadata taskExecutionMetadata = jobController.getTaskExecutionMetadata();
        String inputEnvId = taskExecutionMetadata.getInputIEnvironmentId();
        getEnvManager().setVariableValue(
            inputEnvId, HelloWorld.TEXT_INPUT, "x");
        // if the test server is busy then this may take longer than 2 seconds.
//        taskExecutionMetadata.getTaskMetadata().setSecondsBeforeProgressShown(60);
        jobController.run();
        for (int i = 0; i < 6000; i++) {
            Thread.sleep(10); // wait for job to finish
            if (jobController.getTaskExecutionMetadata().getStatus().isDone()) {
                break;
            }
        }
        assertEquals(TaskExecutionStatus.SUCCESS,
            jobController.getTaskExecutionMetadata().getStatus());
    }

    @Test
    public void testRunWithProgress() throws TaskExecutionException,
        UnknownJobException,
    InterruptedException {
        TaskExecutionMetadata taskExecutionMetadata = jobController.getTaskExecutionMetadata();
        String inputEnvId = taskExecutionMetadata.getInputIEnvironmentId();
        getEnvManager().setVariableValue(
            inputEnvId, HelloWorld.TEXT_INPUT, "x");
        JobProgressDialog jobProgressDialog = new JobProgressDialog(
            jobController, taskExecutionMetadata);
        jobController.startJobInBackend();
        jobProgressDialog.start();
        jobProgressDialog.join();
        assertEquals(TaskExecutionStatus.SUCCESS, taskExecutionMetadata.getStatus());
    }

}
