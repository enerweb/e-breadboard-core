package za.co.enerweb.ebr;

import org.junit.Test;

public class EbrEmbeddedTomeeIT extends EbrEmbeddedTomee {

    public EbrEmbeddedTomeeIT() {
    }

    public static void main(String[] args) throws Exception {
        new EbrEmbeddedTomeeIT().runAndWait();
    }

    @Test
    public void testRun() throws Exception {
        runAndWait(3);
    }

}
