#!/usr/bin/env python

__author__ = "Marius Kruger (amanic@gmail.com)"
_script_version = (2, 0, 0)

import ConfigParser, getopt, os, os.path, re
import signal, shlex, socket, subprocess, sys, time
import fcntl
import xml.dom.minidom


DEFAULT_TIMEOUT = 15
CWD=os.getcwd()
release_dirs = (
    {
        "dir": CWD + ".release", # change to use current dir.release
        "trunk_branch":"https://bitbucket.org/enerweb/e-breadboard-core.git",
        "mvn_profiles":",ew.deploy-public"
    },
#    {
#        "dir":"/stf/prj/DIAS/branches/release",
#        "trunk_branch":
#            "https+urllib://gforge.enerweb.co.za/git/pux0801/DIAS/trunk",
#        "mvn_profiles":",dias-all,ew.deploy-private"
#    },
    )
version_pom = CWD + ".release/ebr-parent-pom/pom.xml"
#"/stf/prj/e-breadboard2/e-breadboard-core/ebr-parent-pom/pom.xml"

#trunk_branch = "https+urllib://gforge.enerweb.co.za/git/pux0801/DIAS/trunk"
maven_profiles = "release,-dev"


def usage(out=sys.stdout):
    print >> out, "Usage: %s [-h --help -v --verbose -u --usage --commit --deploy --skip-tests --update-branches --skip-create-branches --push-version-branch] <version>"


def help(out=sys.stdout):
    print >> out, """
This is the most awesomest maven+git release script.
Current best practices:
 * --deploy all releases and snapshot releases
   (this takes long and is big, so do this from icu2)
 * Previously I thought we should only --commit snapshot releases, 
   but thinking about it again, would it not be better to have a release 
   commit, that way if we keep a release branch around it's poms will be updated.
   On the other hand if somebody slice a new custom build off of there it
   is probably better that it will stay it is a snapshot again...
"""
    usage(out)


def wrap(s, wrapper="  # %s"):
    if not s:
        return ''
    return ''.join([wrapper % l for l in s.splitlines(True)])


def setNonBlocking(fd):
    """Make a fd non-blocking."""
    flags = fcntl.fcntl(fd, fcntl.F_GETFL)
    flags = flags | os.O_NONBLOCK
    fcntl.fcntl(fd, fcntl.F_SETFL, flags)


def cd(dir, verbose=False):
    #if verbose:
    print "cd %s" % dir
    os.chdir(dir)

class CommandFailedException(Exception):
    pass

def cmd(args, verbose=False, fail_on_error=False, out=sys.stdout):
    if isinstance(args, str):
        args = shlex.split(args)
    cmd = ' '.join(args)
    #if verbose:
    #print >> self._out, "\n  $ %s" % cmd
    print "\n  $ %s" % cmd
    last_exit_status = 0
    if verbose:
        p = subprocess.Popen(args, shell=False,
                #stdin=subprocess.PIPE,
                #stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                close_fds=True)
    else:
        p = subprocess.Popen(args, shell=False,
                #stdin=subprocess.PIPE,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                close_fds=True)
    out, err =  p.communicate()
    #if verbose or (fail_on_error and p.returncode != 0):
    if fail_on_error and p.returncode != 0:
        print wrap(out, wrapper="  #> %s")
        print wrap(err, wrapper="  #! %s")
        raise CommandFailedException("Command failed:\n$ %s" % cmd)
    print "."
    return (p.returncode, out, err) #out and err is ''


def git(args, verbose=False, fail_on_error=False):
    if isinstance(args, str):
        args = shlex.split(args)
    args = ["git"] + list(args)
    return cmd(args, verbose=verbose, fail_on_error=fail_on_error)


def mvn(args, verbose=False, fail_on_error=False, extra_mvn_profiles=""):
    if isinstance(args, str):
        args = shlex.split(args)
    #args = shlex.split("mvn -P %s -o" % maven_profiles) + args
    args = shlex.split("mvn -P %s%s" % (maven_profiles, extra_mvn_profiles)) + args
    return cmd(args, verbose=verbose, fail_on_error=fail_on_error)



def updateVersionPom(version, verbose=False,
    mvn_action="install" # can be deploy
    ):
    '''
need to update the following in the parentpom too..
<properties>
    <ebr.version>0.11.0-SNAPSHOT</ebr.version>'''

    print("updating " + version_pom)
    doc = xml.dom.minidom.parse(version_pom)
    project = doc.getElementsByTagName('project')[0]
    version_node = None
    for node in project.childNodes:
        if node.nodeType != node.TEXT_NODE and 'tagName' in dir(node)  and node.tagName == 'version':
            #version_node = project.getElementsByTagName('version')[0]
            version_node = node

    #print "version_node: ", version_node
    #print "dir version_node: ", dir(version_node)
    #    print "version_node.firstChild.data: ", version_node.firstChild.data
    version_node.firstChild.data = version
    #    print "version_node.firstChild.data: ", version_node.firstChild.data

    properties_element = project.getElementsByTagName('properties')[0]
    ebr_version_element = project.getElementsByTagName('ebr.version')[0]
    ebr_version_element.firstChild.data = version

    doc.normalize()
    #print doc.toprettyxml()

    fw = open(version_pom, 'w')
    #fw.write(doc.toprettyxml(indent="  ", newl="\n"))
    fw.write(doc.toxml())
    fw.close()

    #not sure why we were doing this:
    #pom_dir = os.path.dirname(version_pom)
    #cd(pom_dir, verbose)
    #mvn("-U clean %s" % mvn_action,
    #        verbose=verbose, fail_on_error=True)

    ##git("ci -m 'release version %s' '%s'" % (version, version_pom),
    ##        verbose=verbose, fail_on_error=False)
    ##git("push", verbose=verbose, fail_on_error=True)

def update_parent_versions(release_dir, version_pom, version):
    # assume poms can be only one level deep
    for prj_name in os.listdir(release_dir):
        pom = os.path.join(release_dir, prj_name, "pom.xml")
        if (not os.path.isdir(os.path.join(release_dir, prj_name)) or
            not os.path.exists(pom) or
                os.path.abspath(version_pom) == os.path.abspath(pom)):
            # skip the parent pom
            continue;
        print "pom: ", pom
        doc = xml.dom.minidom.parse(pom)
        parent = doc.getElementsByTagName('parent')[0]
        parent_name_node = parent.getElementsByTagName('artifactId')[0]
        version_node = parent.getElementsByTagName('version')[0]
        if parent_name_node.firstChild.data != 'ebr-parent-pom':
            print "skipping parent: " + parent_name_node.firstChild.data
            continue

        version_node.firstChild.data = version

        relpath_nodes = parent.getElementsByTagName('relativePath')
        for node in relpath_nodes:
            parent.removeChild(node)
        #if not relpath_node:
        #   #delete it
        relpath_node = doc.createElement('relativePath')
        relpath_node.appendChild(doc.createTextNode('../ebr-parent-pom/pom.xml'))
        parent.appendChild(relpath_node)    

        doc.normalize()
        #    print doc.toprettyxml()

        fw = open(pom, 'w')
        #fw.write(doc.toprettyxml(indent="  ", newl="\n"))
        fw.write(doc.toxml())
        fw.close()


def update_subproject(release_dir, trunk_branch, version, verbose=False, updatebranches=False):
    print("Releasing %s version %s" % (release_dir, version))
    # deleting and checking out again is the only way to do this reliably..
    if not updatebranches:
      cmd("rm -rf '%s'" % release_dir)
      git(("clone", "--reference", CWD, trunk_branch, release_dir), fail_on_error=True)
    else:
        #if not os.path.exists(release_dir):
        #    os.makedirs(release_dir)
        #    git(("branch", "--use-existing-dir", trunk_branch, release_dir), fail_on_error=True)

       cd(release_dir, verbose)
       #ecmd("revert", verbose=verbose, fail_on_error=False)
       git("pull", #% trunk_branch,
          verbose=verbose, fail_on_error=True)
       git("checkout HEAD", verbose=verbose, fail_on_error=True)
    

def release_subproject(release_dir, version, verbose=False,
        commit=False, mvn_action="install", # can be deploy
        skip_tests=False, push_version_branch=False, extra_mvn_profiles=""
    ):
    print("Releasing %s version %s" % (release_dir, version))
    try:
        cd(release_dir, verbose)

	mvn_options = "-U"
        if skip_tests:
            mvn_options += " -DskipTests"
        
        # build and install it first
        mvn("%s clean install" % mvn_options,
            verbose=verbose, fail_on_error=True, extra_mvn_profiles=extra_mvn_profiles)
        if mvn_action == "deploy":
            # since some deploys can fail (because they happened before), we don't fail if 
            # this doesn't work, so please check that everything actually got uploaded 
            # I suppose we should check for just this 400 errors, but that will be tricky
            mvn("-fn -DskipTests deploy",
                verbose=True, fail_on_error=True, extra_mvn_profiles=extra_mvn_profiles)
        
        #maybe we should check that the versions are correct
        if commit:
            git(("commit", "-a", "-m", "bump version to %s" % version #, "pom.xml"
                ), verbose=verbose, fail_on_error=True)
        git("tag --force v%s" % version,
            verbose=verbose, fail_on_error=True)
        
        if commit:
	     git("push --all",# % trunk_branch,
		 verbose=verbose, fail_on_error=True)
	     git("push --tags",# % trunk_branch,
		 verbose=verbose, fail_on_error=True)
        if push_version_branch:
            git("branch v%s" % version) 
            git("push")
 
        cd(CWD, verbose)
        git("pull");
    except CommandFailedException, e:
        print "Could not release because: %s" % e


def release(version, verbose=True, commit=False, mvn_action="deploy", # can be install or deploy
    skip_tests=False, updatebranches=False, push_version_branch=False, create_branches=True
    ):
    import re
    startTime = time.time()

    # validate version
    if not re.match(r"^\d+\.\d+\.\d+(-((SNAPSHOT)|(RC\d+)))?$", version):
        print("Invalid version number: %s\nIt must be in one of the following "
            "forms:\n 1.5.0 or 0.6.2-SNAPSHOT or 0.7.0-RC1" % version)
        return
    
    if create_branches:
        for release_dir in release_dirs:
            print release_dir
            update_subproject(release_dir["dir"], release_dir["trunk_branch"],
                version=version, verbose=verbose, updatebranches=updatebranches)

    updateVersionPom(version, verbose=verbose, mvn_action=mvn_action)
    
    for release_dir in release_dirs:
        print release_dir
        print "************* release", release_dir["mvn_profiles"]
        update_parent_versions(release_dir["dir"], version_pom, version)
        release_subproject(release_dir["dir"], 
            version=version, verbose=verbose, commit=commit, mvn_action=mvn_action, skip_tests=skip_tests, push_version_branch=push_version_branch, extra_mvn_profiles=release_dir["mvn_profiles"])

    elapsed = time.time() - startTime
    print "Finished. Total time: " + time.strftime('%H:%M:%S', time.gmtime(elapsed))

def service_command(argv):
    try:
      opts, commandargs = getopt.getopt(argv, "hvucd",
      ["help", "verbose", "usage", "commit", "deploy", "skip-tests", "update-branches", "push-version-branch", "skip-create-branches"])
    except getopt.GetoptError:
      usage()
      sys.exit(os.EX_USAGE)

    verbose = False
    commit = False
    mvn_action = "install"
    skip_tests = False
    updatebranches = False
    push_version_branch = False
    create_branches = True
    for opt, arg in opts:
      if opt in ("-h", "--help"):
          help()
          sys.exit(os.EX_USAGE)
      elif opt in ("-v", "--verbose"):
          verbose = True
          argv=argv[1:]
      elif opt in ("-u", "--usage"):
          usage()
          sys.exit(os.EX_USAGE)
      elif opt in ("-c", "--commit"):
          commit = True
      elif opt in ("--update-branches"):
          updatebranches = True
      elif opt in ("--skip-create-branches"):
          create_branches = False
      elif opt in ("--push-version-branch"):
          push_version_branch = True
      elif opt in ("-d", "--deploy"):
          mvn_action = "deploy"
      elif opt in ("--skip-tests"):
          skip_tests = True
      else:
          print "Unknown option: " + opt
          help()
          sys.exit(os.EX_USAGE)

    if len(argv) < 1:
        print "Not enough arguments."
        help()
        sys.exit(os.EX_USAGE)
    else:
        version = argv[-1]
        release(version, verbose, commit, mvn_action, skip_tests=skip_tests, updatebranches=updatebranches, push_version_branch=push_version_branch, create_branches=create_branches)


if __name__ == "__main__":
    try:
        service_command(sys.argv[1:])
    except KeyboardInterrupt:
        print "\n  *** terminated by user. ***"
