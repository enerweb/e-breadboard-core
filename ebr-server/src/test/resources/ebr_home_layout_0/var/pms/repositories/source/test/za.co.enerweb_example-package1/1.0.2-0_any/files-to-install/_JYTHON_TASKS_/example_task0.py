TaskId = 'pms.example_task0'
TaskTitle = 'example_task0'
TaskDescription = """This is a platform specific test task from a package."""

TaskOutputs = [{'name': 'file_content',
         'type': 'str',
         'description': 'Content of data file',
        }]

def run():
    return file(ebrhome + "/jython/resources/example_data.csv").read();
