TaskId <- "za.co.enerweb.r.examples.basic_task"
TaskTitle <- "R Task Example"
TaskDescription <- "Basic input and outputs."

TaskInputs <- list(
                list(name='str_in',
                     type='character',
                     description='A string input.',
                     default='a default value'),
                list(name='int_in',
                     caption='Int in custom caption', # Only specify this if
                       # you really don't want the default caption which is
                       # derived from the name.
                     type='integer',
                     description='An integer input.',
                     default=12345)
              )

TaskOutputs <- list(
                list(name='str_out', type='character',
                    description='A string output.'),
                list(name='int_out', type='integer',
                    description='An integer output')
             )

run<-function(str_in, int_in){
    print(paste(TaskTitle, 'running'))
    print(paste('str_in:', str_in))
    print(paste('int_in:', int_in))
    return(list('some text', 123))
}
