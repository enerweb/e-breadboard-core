TaskId = 'za.co.enerweb.jython.examples.basic_task'
TaskTitle = 'Jython Task Example'
TaskDescription = """Basic input and outputs."""


TaskInputs = [{'type': 'str',
               'name': 'str_in',
               'description': 'Some string please',
               'default': 'hi'},
              {'type': 'int',
               'name': 'int_in',
               'caption': 'Int in custom caption', # Only specify this if
                       # you really don't want the default caption which is
                       # derived from the name.
               'description': 'Some integer please',
               'default': 0},
    ]

TaskOutputs = [{'type': 'str',
                'name': 'str_out',
                'description': 'Some string in reverse'},
               {'type': 'int',
                'name': 'int_out',
                'description': 'Some negated int'},
]


def run(str_in, int_in):
    print 'int_in: %i' % int_in
    print 'str_in: %s' % str_in
    return (str_in[::-1], -int_in)
