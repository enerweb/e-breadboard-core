package za.co.enerweb.ebr.task;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import za.co.enerweb.ebr.datatype.dataresource.IDataResource;
import za.co.enerweb.toolbox.io.ResourceUtils;
import za.co.enerweb.toolbox.io.TempDir;
import za.co.enerweb.toolbox.io.ZipUtils;

@Slf4j
public class ParserTestUtils {
    public static final String TEST_IMAGE_PATH = "images/testimage.png";
    public static final String TEST_HTML_TAR_PATH = "html/html-tar.zip";
    public static final String TEST_HTML_TAR_MAIN_FILE = "index.htm";

    // we need to write these out otherwise tests depending on it in a jar
    // does not seem to work
    private static File writeOutResource(String path) throws IOException {
        File file = File.createTempFile("ParserTestUtils", ".tmp");
        file.deleteOnExit();
        FileUtils.copyURLToFile(ResourceUtils.getResourceAsUrl(path), file);
        return file;
    }

    @SneakyThrows
    public static File getTestImageFile() {
        return writeOutResource(TEST_IMAGE_PATH);
    }

    @SneakyThrows
    public static File getTestHtmlTarFile() {
        return writeOutResource(TEST_HTML_TAR_PATH);
    }

    @SneakyThrows
    public static File getTestHtmlDir() {
        File htmldir = TempDir.createTempDir("html-dir", true);
        ZipUtils.unZip(ResourceUtils.getResourceAsStream(TEST_HTML_TAR_PATH),
            htmldir);
        return htmldir;
    }

    public static Object assertWriteAndReadNative(ATaskParser parser,
        String envId, String type, Serializable origValue) {
        String variableName = "assertReadWriteNative";
        parser.write(envId, variableName, type, origValue);

        Object readValue = parser.nativeToJava(envId, variableName, parser
            .readNativeVariable(variableName), type);

        assertEquals(origValue, readValue);
        return readValue;
    }

    @SneakyThrows
    public static void assertWriteAndReadNativeDataResource(
        ATaskParser parser, String envId, String type,
        IDataResource origValue) {
        IDataResource readValue = (IDataResource) assertWriteAndReadNative(
            parser, envId, type, origValue);

        assertTrue("content not the same", IOUtils.contentEquals(origValue
            .getInputStream(), readValue.getInputStream()));
    }

    @SneakyThrows
    public static void assertEqualZipEntries(File expected, File actual) {
        Set<String> expectedSet = new HashSet<String>(
            getEntryNameList(expected));
        Set<String> actualSet = new HashSet<String>(getEntryNameList(actual));
        assertTrue("contained files not the same",
            expectedSet.equals(actualSet));
    }

    /**
     * sometimes we get an exception here:
     * "java.util.zip.ZipException: error in opening zip file"
     * so this is needed in order to retry, and to try to debug it better.
     * @param zipFile
     * @return
     */
    @SneakyThrows
    private static List<String> getEntryNameList(File zipFile){
        List<String> entryNameList = null;
        for (int i = 0; i < 10 && entryNameList == null; i++) {
            try {
                entryNameList = ZipUtils.getEntryNameList(zipFile);
            } catch (IOException e) {
                log.error("Could not open file: "
                    + zipFile.getAbsolutePath(), e);
                Thread.sleep(1000);
            }
        }
        return entryNameList;
    }
}
