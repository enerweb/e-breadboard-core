package za.co.enerweb.ebr.home_layout;


import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

import java.io.File;

import javax.ejb.EJB;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FileUtils;

import za.co.enerweb.ebr.server.AEbrTest;
import za.co.enerweb.ebr.task.ITaskRegistry;
import za.co.enerweb.toolbox.io.ResourceUtils;

/**
 * All subclasses need @LocalClient if they want stuff to be injected
 */
@Slf4j
public abstract class AbstractTestUpgradeHomeLayout extends AEbrTest {

    @EJB
    private ITaskRegistry taskRegistry;

    protected File homeDir;

    public AbstractTestUpgradeHomeLayout(int sourceVersion,
            int targetVersion) {
        this.sourceVersion = sourceVersion;
        this.targetVersion = targetVersion;
    }

    private int sourceVersion;
    private int targetVersion;

    public void setUpAfterInjection() throws Exception {
        super.setUpAfterInjection();
        createHome();
    }

    @SneakyThrows
    private File createHome() {
        String path = "ebr_home_layout_" + sourceVersion;
        File irADir = ResourceUtils.getResourceAsFile(path);

        homeDir = getTempDir();
        // homeDir = TempDir.createGroupedTempDir(
        // AbstractOrchestrationEngineTest.EBR_TEST,
        // testName.getMethodName(), true);
        // deleteAfterTest.add(homeDir);
        FileUtils.copyDirectory(irADir, homeDir);
        return homeDir;
    }

    @SneakyThrows
    protected void upgrade() {
        HomeLayoutManager layoutManager = new HomeLayoutManager(taskRegistry);
        assertEquals("Layout not at the expected source version.",
            sourceVersion, layoutManager.detectLayoutVersion(homeDir));
        layoutManager.upgradeLayout(homeDir, targetVersion);
        assertEquals("Layout was not upgraded. Version",
            targetVersion, layoutManager.detectLayoutVersion(homeDir));

    }

    // /**
    // * @throws java.lang.Exception
    // */
    // @Before
    // public void setUp() throws Exception {
    // // create us a version 0 ebr_home
    // createHome();
    // }
    //
    // @After
    // public final void tearDownOrchestrationEngine() {
    // for (File dir : deleteAfterTest) {
    // // log.debug("Deleting: temp dir:" + dir);
    // FileUtils.deleteQuietly(dir);
    // }
    // }


    private File getFile(String relPath) {
        File file = new File(homeDir, relPath);
        log.debug("checking: " + file.getAbsolutePath());
        return file;
    }

    protected void assertDirExists(String relPath) {
        assertTrue("Dir must exist: " + relPath,
            getFile(relPath).isDirectory());
    }

    protected void assertFileExists(String relPath) {
        assertTrue("File must exist: " + relPath,
            getFile(relPath).isFile());
    }

    protected void assertMissing(String relPath) {
        assertTrue(relPath + " must not exist: ",
            !getFile(relPath).exists());
    }
}
