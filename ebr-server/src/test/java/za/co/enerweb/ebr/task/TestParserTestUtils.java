package za.co.enerweb.ebr.task;


import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

public class TestParserTestUtils {

    /**
     * Smoke test to make sure we can extract the html dir
     */
    @Test
    public void testHtmlDir() throws IOException {
        ParserTestUtils.getTestHtmlDir();
        File testHtmlDir = ParserTestUtils.getTestHtmlDir();
        assertTrue(testHtmlDir.isDirectory());
    }

}
