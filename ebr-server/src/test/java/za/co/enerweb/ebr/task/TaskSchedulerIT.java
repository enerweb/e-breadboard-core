package za.co.enerweb.ebr.task;

import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeTrue;

import java.util.List;

import javax.ejb.EJB;

import lombok.extern.slf4j.Slf4j;

import org.apache.openejb.api.LocalClient;
import org.junit.Test;

import za.co.enerweb.ebr.datatype.IDataTypeRegistryService;
import za.co.enerweb.ebr.env.IEnvironmentManager;
import za.co.enerweb.ebr.server.AEbrTest;
import za.co.enerweb.ebr.tasks.example.DummyJavaTask;

@Slf4j
@LocalClient
public class TaskSchedulerIT extends AEbrTest {

    @EJB
    ITaskRegistry taskRegistry;

    @EJB
    ITaskExecutorManager taskExecutorManager;

    @EJB
    IEnvironmentManager environmentManager;

    @EJB
    IDataTypeRegistryService dataTypeRegistryService;

    public void setUpAfterInjection() throws Exception {
        super.setUpAfterInjection();
        addDummyHome("taskSchedulerHome");
    }

    @Test
    public void testParseBasicMetadataWithoutOptionals() throws Exception {
        TaskMetadata tm = getDummyJavaScheduledTask();

        // maybe we get unlucky and it ran before we get here :(
        assumeTrue(taskExecutorManager.getTaskExecutionMetadata().isEmpty());

        Thread.sleep(60000); // wait for scheduled task to run.. ..

        List<TaskExecutionMetadata> tems =
            taskExecutorManager.getTaskExecutionMetadata();
        assertEquals(1, tems.size());
        TaskExecutionMetadata tem = tems.get(0);
        log.debug("TaskExecutionMetadata.id=" + tem.getId());
        assertEquals(TaskExecutionStatus.SUCCESS, tem.getStatus());
    }

    private TaskMetadata getDummyJavaScheduledTask() {
        return taskRegistry.getTaskMetadata(null,
            DummyJavaTask.class.getPackage().getName()
                + ".DummyJavaScheduledTask");
    }

}
