package za.co.enerweb.ebr.task;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import za.co.enerweb.ebr.tasks.example.HelloWorld;


public class TestTaskMetaData {
    @Test
    public void testClone() throws CloneNotSupportedException {
        TaskMetadata taskMD = new TaskMetadata("test-id", "test-title",
            "test-description", HelloWorld.class);
        TaskMetadata taskMD2 = taskMD.clone();
        assertEquals(taskMD.getId(), taskMD2.getId());
        assertEquals(taskMD.getTitle(), taskMD2.getTitle());
        assertEquals(taskMD.getDescription(), taskMD2.getDescription());
        assertEquals(taskMD.getTaskImplementationClass(),
            taskMD2.getTaskImplementationClass());
        assertEquals(taskMD.isVisible(), taskMD2.isVisible());
        assertEquals(taskMD.getAuxProperties(), taskMD2.getAuxProperties());
        assertEquals(taskMD.getLayout(), taskMD2.getLayout());
        assertEquals(taskMD.getLayoutItems(), taskMD2.getLayoutItems());
        assertEquals(taskMD, taskMD2);
    }
}
