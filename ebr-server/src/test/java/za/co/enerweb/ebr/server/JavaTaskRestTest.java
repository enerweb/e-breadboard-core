package za.co.enerweb.ebr.server;

import javax.ws.rs.core.Response.Status;

import org.apache.openejb.api.LocalClient;
import org.junit.Test;


@LocalClient
public class JavaTaskRestTest extends AEbrTest {

    public void setUpAfterInjection() throws Exception {
        super.setUpAfterInjection();
        AEbrTest.addDummyHome("javaTaskHome");
    }

    @Test
    public void testDummyJavaTask() throws Exception {
        givenApplicationUri()

            .post("task/{taskid}",
                "za.co.enerweb.ebr.tasks.example.DummyJavaTask")
            .then().assertThat().statusCode(Status.OK.getStatusCode()
            );
    }

}
