package za.co.enerweb.ebr.task;

import java.util.HashMap;
import java.util.Map;

public class MockTaskParser extends ATaskParser {
    public static final String NAME = "MockTask";
    public static final String EXTENTION = "mt";
    public static final String EXTENTION2 = "mt2";

    public static final String ID = "124";
    public static final String TITLE = "Mock Task";

    protected String taskRelPath;
    protected Map<String, Object> variables = new HashMap<
        String, Object>();

    public MockTaskParser() {
        super(
            NAME, new String[] {EXTENTION, EXTENTION2});
        putVariable(ATaskParser.VARIABLE_NAME_TASK_ID, ID);
        putVariable(ATaskParser.VARIABLE_NAME_TASK_TITLE, TITLE);
    }

    public void putVariable(final String name, final Object value) {
        variables.put(name, value);
    }

    @Override
    protected Class<?> startParseTaskMetaDataFromFile(final String taskRelPath,
        final String taskAbsPath) throws Exception {
        this.taskRelPath = taskRelPath;
        return null;
    }

    @Override
    public Object readNativeVariable(final String variableName) {
        return variables.get(variableName);
    }

    @Override
    public Object nativeToSimpleStructure(final Object nativeObject) {
        return nativeObject;
    }

    @Override
    public void writeSimpleStructure(final String envId,
        final String variableName,
        final Object simpleStructure) {
        variables.put(variableName, simpleStructure);
    }

}
