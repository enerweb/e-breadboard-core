package za.co.enerweb.ebr.env;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import javax.ejb.EJB;

import org.apache.openejb.api.LocalClient;
import org.junit.Test;

import za.co.enerweb.toolbox.openejb.AOpenEjbTest;

/**
 * See if our Environment Manager Cache is working.
 */
@LocalClient
public class EnvironmentManagerCacheTest
    extends AOpenEjbTest {

    @EJB
    private IEnvironmentManager em;

    @Test
    public void testSaveAll() throws CloneNotSupportedException {
        em.setCapacity(1);

        Environment env1 = new Environment();
        env1.setVariableValue("x", 1);
        String envId1 = em.newEnvironmentId();
        env1.setId(envId1);
        em.setEnvironment(env1);
        assertTrue(em.isEnvironmentCached(envId1));
        assertValues(env1, envId1);

        Environment env2 = env1.clone();
        String envId2 = em.newEnvironmentId();
        env2.setId(envId2);
        em.setEnvironment(env2);
        assertValues(env2, envId2);

        assertTrue(em.isEnvironmentCached(envId2));
        assertFalse(em.isEnvironmentCached(envId1));

        // check that it can be loaded again
        assertValues(env1, envId1);
        em.deleteEnvironment(envId1);
        em.deleteEnvironment(envId2);
    }

    private void assertValues(final Environment env,
        final String envId) {
        IEnvironment loadedEnv = em.getEnvironment(envId);
        EnvVarTestUtils.assertEnvironmentsAreEqual(env, loadedEnv, em);
    }
}
