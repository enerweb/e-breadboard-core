package za.co.enerweb.ebr.tasks.example;

import za.co.enerweb.ebr.task.AbstractTask;
import za.co.enerweb.ebr.task.TaskMetadata;

/**
 * A simple java task used for manual and automated testing.
 */
// TODO: move to the core plugin
public class HelloWorld extends AbstractTask {
    private static final int INT_INPUT_DEFAULT = 123;
    public static final String ID = "1cc2cd7c-ac2c-4b56-b2f3-33eafa26093a";
    public static final String TEXT_INPUT_DEFAULT = "my";
    public static final String TEXT_INPUT = "text_input";
    public static final String INT_INPUT = "integer_input";
    public static final String TEXT_OUTPUT = "text_output";
    public static final String INT_OUTPUT = "integer_output";

    public static TaskMetadata getInitTaskMetaData() {
        TaskMetadata tmd =
            new TaskMetadata(HelloWorld.ID, "Hello World",
                "This is just a hello world task.", HelloWorld.class);

        tmd.registerInput(String.class, TEXT_INPUT,
            "This is just a sample text input", TEXT_INPUT_DEFAULT, false);
        tmd.registerInput(Integer.class, INT_INPUT,
            "This is just a sample text input", INT_INPUT_DEFAULT, true);
        tmd.registerOutput(String.class, TEXT_OUTPUT,
            "This is just a sample text output");
        tmd.registerOutput(Integer.class, INT_OUTPUT,
            "This is just a sample integer output");

        return tmd;
    }

    public HelloWorld() {
    }

    /*
     * (non-Javadoc)
     * @see za.co.enerweb.ebr.api.task.Task#run(za.co.enerweb.ebr.task.TaskExecution,
     * za.co.enerweb.ebr.api.env.IEnvironment,
     * za.co.enerweb.ebr.api.env.IEnvironment)
     */
    @Override
    public void run() throws Exception {
        getLogger().info("running task.");
        Object inputValue = getInputEnv().getVariableValue(TEXT_INPUT);
        getLogger().info("inputValue=" + inputValue);
        Object intValue = getInputEnv().getVariableValue(INT_INPUT);
        getLogger().info("intValue=" + intValue);
        getOutputEnv().setVariableValue(TEXT_OUTPUT, "Hello " + inputValue
            + " world.");
        getOutputEnv().setVariableValue(INT_OUTPUT, ((Integer) intValue)
            .intValue() * 2);
        // getLogger().info("done with task.");
    }

}
