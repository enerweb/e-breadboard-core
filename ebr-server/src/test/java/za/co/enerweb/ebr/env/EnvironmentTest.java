package za.co.enerweb.ebr.env;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class EnvironmentTest {

    private static final String SOME_VALUE = "some value";
    private Environment env;
    private EnvVarMetadata metadata;
    private EnvVarItem item;

    @Before
    public void setUp() throws Exception {
        env = new Environment();
        metadata = new EnvVarMetadata(String.class,
            "myvariable", "My Variable");
        item = new EnvVarItem(metadata, SOME_VALUE);
    }

    /**
     * Test method for {@link za.co.enerweb.ebr.env.Environment#setVariable(
     *  za.co.enerweb.ebr.env.EnvVarItem)}.
     */
    @Test
    public void testSetVariable() {
        env.setVariable(item);
        assertVariable();
    }


    /**
     * Test method for {@link za.co.enerweb.ebr.env.Environment
     *  #setVariableMetadata(za.co.enerweb.ebr.env.EnvVarMetadata)}.
     */
    @Test
    public void testSetVariableMetadata() {
        env.setVariableMetadata(metadata);
        EnvVarTestUtils.assertEqualEnvVarMetadata(metadata,
            env.getVariableMetadata(metadata.getName()));
        assertEquals(null, env.getVariableValue(metadata.getName()));

        env.setVariableValue(metadata.getName(), SOME_VALUE);
        assertVariable();
    }

    /**
     * Test method for {@link za.co.enerweb.ebr.env.Environment
     *  #setVariableValue(java.lang.String, java.io.Serializable)}.
     */
    @Test
    public void testSetVariableValue() {
        env.setVariableValue(metadata.getName(), SOME_VALUE);

        EnvVarTestUtils.assertEqualEnvVarMetadata(new EnvVarMetadata(
            metadata.getTypeName(), metadata.getName(), metadata.getName()),
            env.getVariableMetadata(metadata.getName()));
        assertEquals(SOME_VALUE, env.getVariableValue(metadata.getName()));

        env.setVariableMetadata(metadata);
        assertVariable();
    }

    private void assertVariable() {
        EnvVarTestUtils.assertEqualEnvVarMetadata(metadata,
            env.getVariableMetadata(metadata.getName()));
        assertEquals(SOME_VALUE, env.getVariableValue(metadata.getName()));
    }
}
