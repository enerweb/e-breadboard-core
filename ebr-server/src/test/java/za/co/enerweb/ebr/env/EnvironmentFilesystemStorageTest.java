package za.co.enerweb.ebr.env;

import javax.ejb.EJB;

import org.apache.openejb.api.LocalClient;
import org.junit.Test;

import za.co.enerweb.toolbox.openejb.AOpenEjbTest;

@LocalClient
public class EnvironmentFilesystemStorageTest extends AOpenEjbTest {
    @EJB
    private IEnvironmentManager em;


    @Test
    public void testSaveAll() {
        Environment env = new Environment();
        String envId = em.newEnvironmentId();
        env.setId(envId);
        env.setVariableValue("x", 1);
        env.setVariableValue("y", "Y");
        em.setEnvironment(env);
        em.setVariableValue(envId, "z", 0.123);
        env.setVariableValue("z", 0.123); //needed for asserts
        assertValues(env, envId);

        em.saveEnvironment(envId);
        assertValues(env, envId);
        em.clearCache();
        assertValues(env, envId);
        em.deleteEnvironment(envId);
    }

    private void assertValues(final Environment env,
        final String envId) {
        // test em.getAll()
        IEnvironment loadedEnv = em.getEnvironment(envId);
        EnvVarTestUtils.assertEnvironmentsAreEqual(env, loadedEnv, em);
    }
}
