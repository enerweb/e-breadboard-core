package za.co.enerweb.ebr.session;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Map;

import javax.ejb.EJB;

import org.apache.openejb.api.LocalClient;
import org.junit.Test;

import za.co.enerweb.ebr.env.IEnvironmentManager;
import za.co.enerweb.ebr.task.ISessionManager;
import za.co.enerweb.toolbox.openejb.AOpenEjbTest;

@LocalClient
public class SessionManagerTest extends AOpenEjbTest {
    private static final String ENVVAR_VALUE = "i courn't believe it";
    private static final String ENVVAR_NAME = "something";
    private static final String SESSION_NAME = "SessionManagerTest";
    @EJB
    private IEnvironmentManager envManager;
    @EJB
    private ISessionManager sessionManager;

    public void setUpAfterInjection() throws Exception {
        super.setUpAfterInjection();
    }

    @Test
    public void testCreateSaveDelete() {
        // IEnvironmentManager envManager = getEnvManager();
        Map<String, SessionMetadata> sessions = sessionManager.getSessions();
        assertTrue(sessions.isEmpty());
        SessionMetadata session1 = sessionManager.newSession(SESSION_NAME);
        assertFalse(sessions.isEmpty());
        assertTrue(session1.getId().length() > 10);
        assertEquals(SESSION_NAME, session1.getName());
        String namespace = "test namespace";
        sessionManager.ensureNamespace(session1.getId(), namespace);
        envManager.setVariableValue(
            session1.getEnvironmentId(namespace), ENVVAR_NAME,
            ENVVAR_VALUE);
        sessionManager.saveSession(session1.getId());
        sessionManager.reloadSessions();

        SessionMetadata session2 = sessionManager.getOrCreateSessionByName(
            SESSION_NAME);
        sessionManager.ensureNamespace(session2.getId(), namespace);
        assertEquals(session1.getId(), session2.getId());
        assertEquals(SESSION_NAME, session2.getName());
        assertEquals(ENVVAR_VALUE, envManager.getVariableValue(
            session1.getEnvironmentId(namespace), ENVVAR_NAME));
        assertEquals(ENVVAR_VALUE, envManager.getVariableValue(
            session2.getEnvironmentId(namespace), ENVVAR_NAME));

        sessionManager.deleteSession(session1.getId());
        sessionManager.reloadSessions();
        sessions = sessionManager.getSessions();
        assertTrue(sessions.isEmpty());
        try {
            assertEquals(ENVVAR_VALUE, envManager.getVariableValue(
                session1.getEnvironmentId(namespace), ENVVAR_NAME));
            fail("should have thrown an exception, "
                + "because env should be deleted.");
        } catch (Exception e) {
        }
    }
}
