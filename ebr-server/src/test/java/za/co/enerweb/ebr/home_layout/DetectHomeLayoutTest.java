package za.co.enerweb.ebr.home_layout;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;

import za.co.enerweb.toolbox.io.ResourceUtils;

@RunWith(JUnitParamsRunner.class)
public class DetectHomeLayoutTest extends AbstractHomeLayoutTest {

    @Test
    public void testEmptyDir() {
        assertEquals(0, layoutManager.detectLayoutVersion(homeDir));
    }

    @Test
    @Parameters(method = "getLayoutVersions")
    public void testDetectLayout(int layoutVersion) throws IOException {
        FileUtils.cleanDirectory(homeDir);
        FileUtils.copyDirectory(
            ResourceUtils.getResourceAsFile("ebr_home_layout_" + layoutVersion),
            homeDir);
        assertEquals(layoutVersion, layoutManager.detectLayoutVersion(homeDir));
    }
}
