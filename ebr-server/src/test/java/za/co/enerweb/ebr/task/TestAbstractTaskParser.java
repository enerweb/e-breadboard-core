package za.co.enerweb.ebr.task;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;

import org.apache.openejb.api.LocalClient;
import org.junit.Test;

import za.co.enerweb.ebr.datatype.IDataTypeRegistryService;
import za.co.enerweb.ebr.server.AEbrTest;
import za.co.enerweb.ebr.server.HomeFileCategory;
import za.co.enerweb.ebr.server.HomeFileSpec;
import za.co.enerweb.ebr.server.IHomeService;
import za.co.enerweb.ebr.server.TaskTestUtils;

import com.google.common.collect.Iterators;

@LocalClient
public class TestAbstractTaskParser extends AEbrTest {

    private static final String INT_IN_NAME = "int_in";
    private static final String INT_IN_CAPTION = "Int in";
    private static final String INT_IN_CLASS = "java.lang.Integer";
    private static final String INT_IN_TYPE = INT_IN_CLASS;//"int";
    private static final String INT_IN_DESCRIPTION = "int in";
    private static final Integer INT_IN_DEFAULTVALUE = new Integer(1);
    private static final String INT_IN_ENV = "session";
    private static final String INT_IN_NAMESPACE = "test_namespace";

    private static final String STR_OUT_NAME = "str_out";
    private static final String STR_OUT_CAPTION = "Str out";
    private static final String STR_OUT_CLASS = "java.lang.String";
    private static final String STR_OUT_TYPE = STR_OUT_CLASS;//"str";
    private static final String STR_OUT_DESCRIPTION = "str out";
    private MockTaskParser parser;

    @EJB
    IHomeService homeService;

    @EJB
    ITaskRegistry taskRegistry;

    @EJB
    IDataTypeRegistryService dataTypeRegistryService;

    public void setUpAfterInjection() throws Exception {
        super.setUpAfterInjection();
        String taskFileName = "task." + MockTaskParser.EXTENTION;
        parser = new MockTaskParser();
        parser.setHomeService(homeService);
        parser.setDataTypeRegistryService(dataTypeRegistryService);
        parser.setTaskRegistry(taskRegistry);
        homeService.writeFile(new HomeFileSpec(
            HomeFileCategory.TASKS_PER_TECHNOLOGY, parser.getName(),
            taskFileName),
            "task text");
    }

    @Test
    public void testParseBasicMetadata() throws Exception {
        parser.registerTasks();
        TaskMetadata tmd = getTaskMetadata();

        TaskTestUtils.assertTaskBasicMetadata(tmd, MockTaskParser.ID,
            MockTaskParser.TITLE, null, null, true);
    }

    private TaskMetadata getTaskMetadata() {
        return taskRegistry.getTaskMetadata(null,
            MockTaskParser.ID);
    }

    @Test
    public void testParseOptionalBasicMetadata() throws Exception {
        String description = "This is a mock task";
        parser.putVariable(ATaskParser.VARIABLE_NAME_TASK_DESCRIPTION,
            description);
        String layout = "default";
        parser.putVariable(ATaskParser.VARIABLE_NAME_TASK_LAYOUT,
            layout);
        Boolean visible = false;
        parser.putVariable(ATaskParser.VARIABLE_NAME_TASK_VISIBLE,
            visible);

        parser.registerTasks();
        TaskMetadata tmd = getTaskMetadata();

        TaskTestUtils.assertTaskBasicMetadata(tmd, MockTaskParser.ID,
            MockTaskParser.TITLE, description, layout, visible);
    }

    @Test
    public void testParseInputMetadata() throws Exception {
        ArrayList<Object> taskInputs = new ArrayList<Object>();
        HashMap<String, Object> int_in = new HashMap<String, Object>();

        int_in.put(ATaskParser.NAME_ATTRIBUTE, INT_IN_NAME);
        int_in.put(ATaskParser.TYPE_ATTRIBUTE, INT_IN_TYPE);
        int_in.put(ATaskParser.DESCRIPTION_ATTRIBUTE,
            INT_IN_DESCRIPTION);
        int_in.put(ATaskParser.DEFAULT_ATTRIBUTE, INT_IN_DEFAULTVALUE);
        int_in.put(ATaskParser.ENV_ATTRIBUTE, INT_IN_ENV);
        int_in.put(ATaskParser.NAMESPACE_ATTRIBUTE, INT_IN_NAMESPACE);

        String arbValue = "arb_value";
        String arbProperty = "arb_property";
        int_in.put(arbProperty, arbValue);

        taskInputs.add(int_in);
        parser.putVariable(ATaskParser.VARIABLE_NAME_TASK_INPUTS,
            taskInputs);

        parser.registerTasks();
        TaskMetadata tmd = getTaskMetadata();
        TaskTestUtils.assertInputMetadata(tmd, INT_IN_NAME, INT_IN_CAPTION,
            INT_IN_TYPE,
            INT_IN_CLASS,
            INT_IN_DESCRIPTION, INT_IN_DEFAULTVALUE,
            ParameterEnvironment.SESSION,
            INT_IN_NAMESPACE);
        assertEquals(arbValue,
            tmd.getInputMetadata(INT_IN_NAME).getAuxProperty(arbProperty));
    }

    @Test
    public void testParseInputMetadataNoOptionals() throws Exception {
        ArrayList<Object> taskInputs = new ArrayList<Object>();
        HashMap<String, Object> int_in = new HashMap<String, Object>();

        int_in.put(ATaskParser.NAME_ATTRIBUTE, INT_IN_NAME);
        int_in.put(ATaskParser.TYPE_ATTRIBUTE, INT_IN_TYPE);
        taskInputs.add(int_in);
        parser.putVariable(ATaskParser.VARIABLE_NAME_TASK_INPUTS,
            taskInputs);

        parser.registerTasks();
        TaskMetadata tmd = getTaskMetadata();
        TaskTestUtils.assertInputMetadata(tmd, INT_IN_NAME, INT_IN_CAPTION,
            INT_IN_TYPE,
            INT_IN_CLASS,
            null, null, ParameterEnvironment.JOB, null);
    }

    @Test
    public void testParseInputMetadataFromList() throws Exception {
        ArrayList<Object> taskInputs = new ArrayList<Object>();
        List<Object> int_in = new ArrayList<Object>();

        int_in.add(INT_IN_TYPE);
        int_in.add(INT_IN_NAME);
        int_in.add(INT_IN_DESCRIPTION);
        int_in.add(INT_IN_DEFAULTVALUE);
        taskInputs.add(int_in);
        parser.putVariable(ATaskParser.VARIABLE_NAME_TASK_INPUTS,
            taskInputs);

        parser.registerTasks();
        TaskMetadata tmd = getTaskMetadata();
        TaskTestUtils.assertInputMetadata(tmd, INT_IN_NAME, null, INT_IN_TYPE,
            INT_IN_CLASS,
            INT_IN_DESCRIPTION, INT_IN_DEFAULTVALUE, ParameterEnvironment.JOB,
            null);
    }

    @Test
    public void testParseInputMetadataFromListNoOptionals() throws Exception {
        ArrayList<Object> taskInputs = new ArrayList<Object>();
        List<Object> int_in = new ArrayList<Object>();

        int_in.add(INT_IN_TYPE);
        int_in.add(INT_IN_NAME);
        taskInputs.add(int_in);
        parser.putVariable(ATaskParser.VARIABLE_NAME_TASK_INPUTS,
            taskInputs);

        parser.registerTasks();
        TaskMetadata tmd = getTaskMetadata();
        TaskTestUtils.assertInputMetadata(tmd, INT_IN_NAME, INT_IN_CAPTION,
            INT_IN_TYPE,
            INT_IN_CLASS, null, null, ParameterEnvironment.JOB, null);
    }

    @Test
    public void testParseOutputMetadata() throws Exception {
        ArrayList<Object> taskOutputs = new ArrayList<Object>();
        HashMap<String, Object> str_out = new HashMap<String, Object>();
        str_out.put(ATaskParser.NAME_ATTRIBUTE, STR_OUT_NAME);
        String dummyCaption = STR_OUT_CAPTION + " Caption";
        str_out.put(ATaskParser.CAPTION_ATTRIBUTE, dummyCaption);
        str_out.put(ATaskParser.TYPE_ATTRIBUTE, STR_OUT_TYPE);
        str_out.put(ATaskParser.DESCRIPTION_ATTRIBUTE,
            STR_OUT_DESCRIPTION);

        String arbValue = "arb_value";
        String arbProperty = "arb_property";
        str_out.put(arbProperty, arbValue);

        taskOutputs.add(str_out);
        parser.putVariable(ATaskParser.VARIABLE_NAME_TASK_OUTPUTS,
            taskOutputs);

        parser.registerTasks();
        TaskMetadata tmd = getTaskMetadata();
        TaskTestUtils.assertOutputMetadata(tmd, STR_OUT_NAME, dummyCaption,
            STR_OUT_TYPE,
            STR_OUT_CLASS,
            STR_OUT_DESCRIPTION, ParameterEnvironment.JOB, null);

        assertEquals(arbValue,
            tmd.getOutputMetadata(STR_OUT_NAME).getAuxProperty(arbProperty));
    }

    @Test
    public void testParseOutputMetadataFromList() throws Exception {
        ArrayList<Object> taskOutputs = new ArrayList<Object>();
        List<Object> str_out = new ArrayList<Object>();
        str_out.add(STR_OUT_TYPE);
        str_out.add(STR_OUT_NAME);
        str_out.add(STR_OUT_DESCRIPTION);
        taskOutputs.add(str_out);
        parser.putVariable(ATaskParser.VARIABLE_NAME_TASK_OUTPUTS,
            taskOutputs);

        parser.registerTasks();
        TaskMetadata tmd = getTaskMetadata();
        TaskTestUtils.assertOutputMetadata(tmd, STR_OUT_NAME, STR_OUT_CAPTION,
            STR_OUT_TYPE,
            STR_OUT_CLASS,
            STR_OUT_DESCRIPTION, ParameterEnvironment.JOB, null);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testParseLayoutItems() throws Exception {
        ArrayList<Object> layoutItems = new ArrayList<Object>();
        HashMap<String, Object> str_out = new HashMap<String, Object>();
        str_out.put(ATaskParser.NAME_ATTRIBUTE, STR_OUT_NAME);
        String type = "textbox";
        str_out.put(ATaskParser.TYPE_ATTRIBUTE, type);
        String someListAttribute = "some_list";
        List<Integer> someList = Arrays.asList(new Integer[] {1, 2, 3});
        str_out.put(someListAttribute, someList);
        layoutItems.add(str_out);
        parser.putVariable(ATaskParser.VARIABLE_NAME_TASK_LAYOUT_ITEMS,
            layoutItems);

        parser.registerTasks();
        TaskMetadata tmd = getTaskMetadata();
        List<Map<String, Serializable>> layoutItems2 = tmd.getLayoutItems();
        assertEquals(1, layoutItems2.size());
        Map<String, Serializable> layoutItem2 = layoutItems2.get(0);
        assertEquals(STR_OUT_NAME,
            layoutItem2.get(ATaskParser.NAME_ATTRIBUTE));
        assertEquals(type,
            layoutItem2.get(ATaskParser.TYPE_ATTRIBUTE));
        List<Integer> someList2 = (List<Integer>)layoutItem2.get(
            someListAttribute);
        assertTrue(Iterators.elementsEqual(someList.iterator(),
            someList2.iterator()));
    }

    @Test
    public void testParseArbitaryLayoutItem() throws Exception {
        ArrayList<Object> layoutItems = new ArrayList<Object>();
        HashMap<String, Object> str_out = new HashMap<String, Object>();
        String arbitaryText = "arbitary.text";
        str_out.put(ATaskParser.NAME_ATTRIBUTE, arbitaryText);
        String type = "text";
        str_out.put(ATaskParser.TYPE_ATTRIBUTE, type);
        String someText = "text";
        String someTextValue = "text value";
        str_out.put(someText, someTextValue);
        layoutItems.add(str_out);
        parser.putVariable(ATaskParser.VARIABLE_NAME_TASK_LAYOUT_ITEMS,
            layoutItems);

        parser.registerTasks();
        TaskMetadata tmd = getTaskMetadata();
        List<Map<String, Serializable>> layoutItems2 = tmd.getLayoutItems();
        assertEquals(1, layoutItems2.size());
        Map<String, Serializable> layoutItem2 = layoutItems2.get(0);
        assertEquals(arbitaryText,
            layoutItem2.get(ATaskParser.NAME_ATTRIBUTE));
        assertEquals(type,
            layoutItem2.get(ATaskParser.TYPE_ATTRIBUTE));
        assertEquals(someTextValue,
            layoutItem2.get(someText));
    }
}
