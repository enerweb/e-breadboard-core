package za.co.enerweb.ebr.server;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static za.co.enerweb.ebr.server.LogFileSpec.LogLayout.LINE;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.ejb.EJB;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;
import org.apache.openejb.api.LocalClient;
import org.junit.Test;

import za.co.enerweb.toolbox.openejb.AOpenEjbTest;

@LocalClient
@Slf4j
public class TestLogFileSplit extends AOpenEjbTest {

    @EJB
    IEbrServer ebrServer;

    @Test
    public void testLog() throws IOException {
        HomeFile logFile = ebrServer.addLogFile(new LogFileSpec()
            .name(getClass().getName())
            .maximumFileSize(100)
            .numberOfBackupFiles(1)
            .layout(LINE));

        log.debug("test log file: " + logFile);

        String logMessage = "Hi from "
            + getClass().getName() + " "
            + new Date();
        log.info(logMessage);
        String loggedContent = logFile.getContent();
        assertEquals(logMessage + "\n", loggedContent);

        String bytes100 = StringUtils.repeat("0123456789", 5);
        for (int i = 0; i < 4; i++) {
            log.info(bytes100);
        }
        assertEquals(51, logFile.getFile().length());
        File f1 = new File(logFile.getAbsolutePath() + ".1");
        assertTrue(f1.exists());
        assertEquals(102, f1.length());
        assertFalse(new File(logFile.getAbsolutePath() + ".2").exists());
    }

}
