package za.co.enerweb.ebr.tasks.example;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import org.apache.commons.lang3.StringUtils;

import za.co.enerweb.ebr.task.AJavaTask;

/**
 * A simple java task used for manual and automated testing.
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DummyJavaTask extends AJavaTask {

    String strIn;
    String strOut;

    @Override
    public void runJavaTask() throws Exception {
        getLogger().info("running task.");
        getLogger().info("strIn=" + strIn);
        strOut = StringUtils.reverse(strIn);
        getLogger().info("strOut=" + strOut);
    }

}
