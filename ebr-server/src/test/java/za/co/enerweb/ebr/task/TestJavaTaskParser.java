package za.co.enerweb.ebr.task;

import static org.junit.Assert.assertEquals;

import javax.ejb.EJB;

import lombok.extern.slf4j.Slf4j;

import org.apache.openejb.api.LocalClient;
import org.junit.Test;

import za.co.enerweb.ebr.datatype.IDataTypeRegistryService;
import za.co.enerweb.ebr.env.IEnvironment;
import za.co.enerweb.ebr.env.IEnvironmentManager;
import za.co.enerweb.ebr.server.AEbrTest;
import za.co.enerweb.ebr.tasks.example.DummyJavaTask;

@Slf4j
@LocalClient
public class TestJavaTaskParser extends AEbrTest {

    @EJB
    ITaskRegistry taskRegistry;

    @EJB
    ITaskExecutorManager taskExecutorManager;

    @EJB
    IEnvironmentManager environmentManager;

    @EJB
    IDataTypeRegistryService dataTypeRegistryService;

    public void setUpAfterInjection() throws Exception {
        super.setUpAfterInjection();
        addDummyHome("javaTaskHome");
    }

    @Test
    public void testParseBasicMetadataWithoutOptionals() throws Exception {
        TaskMetadata tm = getTaskMetadata();
        assertEquals(DummyJavaTask.class.getName(),
            tm.getAuxProperties().get("Class"));
        assertEquals("Dummy Java Task", tm.getTitle());
        assertEquals(null, tm.getDescription());

        InputMetadata strInMetadata = tm.getInputMetadata("strIn");
        assertEquals(String.class, strInMetadata.getTypeClass());
        assertEquals("abc", strInMetadata.getDefaultValue());
        assertEquals("Str in", strInMetadata.getCaption());
        assertEquals(null, strInMetadata
            .getDescription());

        OutputMetadata strOutMetadata = tm.getOutputMetadata("strOut");
        assertEquals(String.class, strOutMetadata.getTypeClass());
        assertEquals("Str out", strOutMetadata.getCaption());
        assertEquals(null, strOutMetadata.getDescription());

        // test running the task
        /* There is potential for a nicer execution helper that can easily
         * set and get variable values
         */
        TaskExecutionMetadata taskExecution =
            taskExecutorManager.initTaskExecution(tm, null, null);
        taskExecution =
            taskExecutorManager.run(taskExecution.getId(), RunMode.BATCH);
        taskExecution.waitTillDone();
        IEnvironment outEnv = environmentManager.getEnvironment(
            taskExecution.getOutputIEnvironmentId());
        String strOut = outEnv.getVariableValue(strOutMetadata.getName());
        assertEquals("cba", strOut);
    }

    @Test
    public void testParseBasicMetadataWithOptionals() throws Exception {
        TaskMetadata tm = getTaskMetadata1();
        assertEquals(DummyJavaTask.class.getName(),
            tm.getAuxProperties().get("Class"));
        assertEquals("Dummy Java Task 1", tm.getTitle());
        assertEquals("This is a dummy java task.", tm.getDescription());

        InputMetadata strInMetadata = tm.getInputMetadata("strIn");
        assertEquals(String.class, strInMetadata.getTypeClass());
        assertEquals("abc", strInMetadata.getDefaultValue());
        assertEquals("Dummy String In", strInMetadata.getCaption());
        assertEquals("Dummy String In Description", strInMetadata
            .getDescription());

        OutputMetadata strOutMetadata = tm.getOutputMetadata("strOut");
        assertEquals(String.class, strOutMetadata.getTypeClass());
        assertEquals("Dummy String Out", strOutMetadata.getCaption());
        assertEquals("Dummy String Out Description", strOutMetadata
            .getDescription());

        // test running the task
        IEnvironment inEnv = environmentManager.newEnvironment();
        inEnv.setVariable(strInMetadata, "def");
        TaskExecutionMetadata taskExecution =
            taskExecutorManager.initTaskExecution(tm, null, inEnv);
        // Future<TaskExecutionMetadata> res =
            taskExecutorManager.run(taskExecution.getId(), RunMode.BATCH);
        // taskExecution = res.get();
        taskExecution.waitTillDone();
        IEnvironment outEnv = environmentManager.getEnvironment(
            taskExecution.getOutputIEnvironmentId());
        String strOut = outEnv.getVariableValue(strOutMetadata.getName());
        assertEquals("fed", strOut);
    }


    private TaskMetadata getTaskMetadata() {
        return taskRegistry.getTaskMetadata(null,
            DummyJavaTask.class.getName());
    }

    private TaskMetadata getTaskMetadata1() {
        return taskRegistry.getTaskMetadata(null,
            DummyJavaTask.class.getName() + "-1");
    }

    // @Test
    // public void testParseOptionalBasicMetadata() throws Exception {
    // String description = "This is a mock task";
    // parser.putVariable(ATaskParser.VARIABLE_NAME_TASK_DESCRIPTION,
    // description);
    // String layout = "default";
    // parser.putVariable(ATaskParser.VARIABLE_NAME_TASK_LAYOUT,
    // layout);
    // Boolean visible = false;
    // parser.putVariable(ATaskParser.VARIABLE_NAME_TASK_VISIBLE,
    // visible);
    //
    // parser.registerTasks();
    // TaskMetadata tmd = getTaskMetadata();
    //
    // TaskTestUtils.assertTaskBasicMetadata(tmd, MockTaskParser.ID,
    // MockTaskParser.TITLE, description, layout, visible);
    // }
    //
    // @Test
    // public void testParseInputMetadata() throws Exception {
    // ArrayList<Object> taskInputs = new ArrayList<Object>();
    // HashMap<String, Object> int_in = new HashMap<String, Object>();
    //
    // int_in.put(ATaskParser.NAME_ATTRIBUTE, INT_IN_NAME);
    // int_in.put(ATaskParser.TYPE_ATTRIBUTE, INT_IN_TYPE);
    // int_in.put(ATaskParser.DESCRIPTION_ATTRIBUTE,
    // INT_IN_DESCRIPTION);
    // int_in.put(ATaskParser.DEFAULT_ATTRIBUTE, INT_IN_DEFAULTVALUE);
    // int_in.put(ATaskParser.ENV_ATTRIBUTE, INT_IN_ENV);
    // int_in.put(ATaskParser.NAMESPACE_ATTRIBUTE, INT_IN_NAMESPACE);
    //
    // String arbValue = "arb_value";
    // String arbProperty = "arb_property";
    // int_in.put(arbProperty, arbValue);
    //
    // taskInputs.add(int_in);
    // parser.putVariable(ATaskParser.VARIABLE_NAME_TASK_INPUTS,
    // taskInputs);
    //
    // parser.registerTasks();
    // TaskMetadata tmd = getTaskMetadata();
    // TaskTestUtils.assertInputMetadata(tmd, INT_IN_NAME, INT_IN_CAPTION,
    // INT_IN_TYPE,
    // INT_IN_CLASS,
    // INT_IN_DESCRIPTION, INT_IN_DEFAULTVALUE,
    // ParameterEnvironment.SESSION,
    // INT_IN_NAMESPACE);
    // assertEquals(arbValue,
    // tmd.getInputMetadata(INT_IN_NAME).getAuxProperty(arbProperty));
    // }
    //
    // @Test
    // public void testParseInputMetadataNoOptionals() throws Exception {
    // ArrayList<Object> taskInputs = new ArrayList<Object>();
    // HashMap<String, Object> int_in = new HashMap<String, Object>();
    //
    // int_in.put(ATaskParser.NAME_ATTRIBUTE, INT_IN_NAME);
    // int_in.put(ATaskParser.TYPE_ATTRIBUTE, INT_IN_TYPE);
    // taskInputs.add(int_in);
    // parser.putVariable(ATaskParser.VARIABLE_NAME_TASK_INPUTS,
    // taskInputs);
    //
    // parser.registerTasks();
    // TaskMetadata tmd = getTaskMetadata();
    // TaskTestUtils.assertInputMetadata(tmd, INT_IN_NAME, INT_IN_CAPTION,
    // INT_IN_TYPE,
    // INT_IN_CLASS,
    // null, null, ParameterEnvironment.JOB, null);
    // }
    //
    // @Test
    // public void testParseInputMetadataFromList() throws Exception {
    // ArrayList<Object> taskInputs = new ArrayList<Object>();
    // List<Object> int_in = new ArrayList<Object>();
    //
    // int_in.add(INT_IN_TYPE);
    // int_in.add(INT_IN_NAME);
    // int_in.add(INT_IN_DESCRIPTION);
    // int_in.add(INT_IN_DEFAULTVALUE);
    // taskInputs.add(int_in);
    // parser.putVariable(ATaskParser.VARIABLE_NAME_TASK_INPUTS,
    // taskInputs);
    //
    // parser.registerTasks();
    // TaskMetadata tmd = getTaskMetadata();
    // TaskTestUtils.assertInputMetadata(tmd, INT_IN_NAME, null, INT_IN_TYPE,
    // INT_IN_CLASS,
    // INT_IN_DESCRIPTION, INT_IN_DEFAULTVALUE, ParameterEnvironment.JOB,
    // null);
    // }
    //
    // @Test
    // public void testParseInputMetadataFromListNoOptionals() throws Exception
    // {
    // ArrayList<Object> taskInputs = new ArrayList<Object>();
    // List<Object> int_in = new ArrayList<Object>();
    //
    // int_in.add(INT_IN_TYPE);
    // int_in.add(INT_IN_NAME);
    // taskInputs.add(int_in);
    // parser.putVariable(ATaskParser.VARIABLE_NAME_TASK_INPUTS,
    // taskInputs);
    //
    // parser.registerTasks();
    // TaskMetadata tmd = getTaskMetadata();
    // TaskTestUtils.assertInputMetadata(tmd, INT_IN_NAME, INT_IN_CAPTION,
    // INT_IN_TYPE,
    // INT_IN_CLASS, null, null, ParameterEnvironment.JOB, null);
    // }
}
