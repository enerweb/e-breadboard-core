package za.co.enerweb.ebr.tasks.example;

import static org.junit.Assert.assertEquals;

import javax.ejb.EJB;

import lombok.SneakyThrows;

import org.apache.openejb.api.LocalClient;
import org.junit.Before;
import org.junit.Test;

import za.co.enerweb.ebr.env.Environment;
import za.co.enerweb.ebr.env.IEnvironment;
import za.co.enerweb.ebr.env.IEnvironmentManager;
import za.co.enerweb.ebr.exceptions.TaskInstantiationException;
import za.co.enerweb.ebr.server.AEbrTest;
import za.co.enerweb.ebr.session.SessionMetadata;
import za.co.enerweb.ebr.task.ISessionManager;
import za.co.enerweb.ebr.task.ITaskExecutorManager;
import za.co.enerweb.ebr.task.ITaskRegistry;
import za.co.enerweb.ebr.task.ParameterEnvironment;
import za.co.enerweb.ebr.task.RunMode;
import za.co.enerweb.ebr.task.TaskExecutionMetadata;
import za.co.enerweb.ebr.task.TaskExecutionStatus;
import za.co.enerweb.ebr.task.TaskMetadata;

@LocalClient
public class TestHelloWorldTask extends AEbrTest {

    // @EJB
    // private ITaskExecutorManager homeService;
    @EJB
    private ITaskRegistry taskRegistry;
    @EJB
    private IEnvironmentManager environmentManager;

    @EJB
    private ISessionManager sessionManager;

    // private TaskExecution taskExecutor;

    private TaskExecutionMetadata taskExecutionMetadata;

    @EJB
    private ITaskExecutorManager taskExecutorManager;

    private Environment inputs;

    private IEnvironment outputs;

    private String sessionId = null;

    @Before
    public void setUpAfterInjection() throws Exception {
        super.setUpAfterInjection();
        // actually testing the registry too
        taskRegistry.registerTask(HelloWorld.getInitTaskMetaData());
        inputs = new Environment();
        // taskExecutor =
        // new TaskExecution(homeService, environmentManager, taskRegistry,
        // AJavaTask.ID, null, inputs);
    }

    @SneakyThrows
    private void run() {
        taskExecutionMetadata =
            taskExecutorManager.initTaskExecution(HelloWorld.ID, sessionId, inputs);
        taskExecutorManager.run(taskExecutionMetadata.getId(),
            RunMode.INTERACTIVE);
        taskExecutionMetadata.waitTillDone();
        outputs = environmentManager.getEnvironment(
            taskExecutionMetadata.getOutputIEnvironmentId());
    }

    @Test
    public void testRunTask()
    throws Exception {
        inputs.setVariableValue(HelloWorld.TEXT_INPUT, "EBR");

        run();

        TaskMetadata taskMetadata = taskExecutionMetadata.getTaskMetadata();
        assertEquals(taskMetadata.getId(), HelloWorld.ID);
        assertEquals(taskMetadata.getTitle(), "Hello World");
        assertEquals(taskMetadata.getDescription(),
            "This is just a hello world task.");

        assertEquals(TaskExecutionStatus.SUCCESS, taskExecutionMetadata.getStatus());
        assertEquals("Hello EBR world.",
            outputs.getVariableValue(HelloWorld.TEXT_OUTPUT));
        assertEquals(246,
            outputs.getVariableValue(HelloWorld.INT_OUTPUT));
    }

    // need a task with non-optional inputs that don't have defaults to test
    // this
    // @Test
    // public void testRunWithMissingInputs()
    // throws Exception {
    // run();
    //
    // assertEquals(TaskExecutionStatus.FAIL, taskExecutionMetadata.getStatus());
    // // TODO: assert that log has a useful message
    // }

    @Test
    public void testRunTaskWithOptionalInputs()
    throws Exception {
        inputs.setVariableValue(HelloWorld.TEXT_INPUT, "EBR");
        inputs.setVariableValue(HelloWorld.INT_INPUT, 111);

        run();

        assertEquals(TaskExecutionStatus.SUCCESS, taskExecutionMetadata.getStatus());
        assertEquals(222,
            outputs.getVariableValue(HelloWorld.INT_OUTPUT));
    }

    /**
     * @throws Exception
     */
    @Test
    public void testRunTaskWithSessionIO()
        throws Exception {
        SessionMetadata session = sessionManager
            .newSession("testRunTaskWithSessionIO");
        sessionId = session.getId();
        String sessionEnvId = session.getEnvironmentId();
        environmentManager.setVariableValue(sessionEnvId, HelloWorld.TEXT_INPUT,
            "EBR Session");

        runHelloWorldTaskWithSessionIO();

        assertEquals("Hello EBR Session world.",
            outputs.getVariableValue(HelloWorld.TEXT_OUTPUT));
    }

    /**
     * @throws Exception
     */
    @Test
    public void testRunTaskWithNoSession()
        throws Exception {
        runHelloWorldTaskWithSessionIO();

        assertEquals("Hello EBR world.",
            outputs.getVariableValue(HelloWorld.TEXT_OUTPUT));
    }

    private void runHelloWorldTaskWithSessionIO()
        throws TaskInstantiationException {
        inputs.setVariableValue(HelloWorld.TEXT_INPUT, "EBR");

        run();

        // change params to read from and write to the session
        TaskMetadata taskMetadata = taskExecutionMetadata.getTaskMetadata();
        taskMetadata.getInputMetadata(HelloWorld.TEXT_INPUT).setEnvironment(
            ParameterEnvironment.SESSION);
        taskMetadata.getOutputMetadata(HelloWorld.TEXT_OUTPUT).setEnvironment(
            ParameterEnvironment.SESSION);

        run();
        // TODO: assert that it actually reads the stuff from the session
        assertEquals(TaskExecutionStatus.SUCCESS, taskExecutionMetadata.getStatus());
    }
}
