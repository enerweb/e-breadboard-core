package za.co.enerweb.ebr.server;

import static org.junit.Assert.assertFalse;

import org.junit.Test;

import za.co.enerweb.ebr.server.SystemConfiguration;

public class ConfigurationServiceTest {

    @Test
    public void testVersion() {
        assertFalse(SystemConfiguration.getVersion().isEmpty());
        assertFalse(SystemConfiguration.getVersion().equals("unknown"));
    }
}
