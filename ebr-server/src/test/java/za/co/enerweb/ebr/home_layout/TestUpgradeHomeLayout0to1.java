package za.co.enerweb.ebr.home_layout;


import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.openejb.api.LocalClient;
import org.junit.Test;

@LocalClient
public class TestUpgradeHomeLayout0to1 extends AbstractTestUpgradeHomeLayout{

    public TestUpgradeHomeLayout0to1() {
        super(0, 1);
    }


    @Test
    public void testMoveTaskParserContent() {
        upgrade();
        assertDirExists("bin/tasks");
        assertFileExists("bin/tasks/basic_task.mt");
        assertFileExists("bin/tasks/basic_task.mt2");
        assertFileExists("cache/MockTask/cache_file.txt");
        assertFileExists("share/task-common/task_common.mt");
        assertFileExists("lib/MockTask/test_lib.mt");
        assertFileExists("share/resources/example/mocktask_example_resource.csv"
            );
        assertMissing("MockTask");
    }

    @Test
    public void testMoveSystemData() {
        upgrade();
        assertMissing("env");
        assertFileExists("var/env/old_env/variables_xstream_v1.xml");

        assertMissing("jobs");
        assertDirExists("var/jobs");

        assertMissing("sessions");
        assertFileExists("var/sessions/old_session_xstream_v2.xml");

        assertMissing("task_perspectives");
        assertFileExists("bin/task-perspectives/example_task_perspective.ini");

        assertMissing("var/web-content");
        assertFileExists("share/web-content/test.html");
        assertFileExists("share/web-content/images/gradient.png");

        assertMissing("vaadin");
        assertFileExists(
            "share/frontend/vaadin/task-layouts/html-layout-example.html");
        assertFileExists("share/frontend/vaadin/stylesheets/example.css");
        assertMissing("lib/javascript");
        assertFileExists("share/frontend/vaadin/javascript/example.js");

        assertMissing("resources");
        assertFileExists("share/resources/example/example_resource.csv");

        assertMissing("pms/path_aliases.txt");
        assertFileExists("etc/pms_path_aliases.txt");
    }

    @Test
    public void testDeleteUnsupportedDirs() {
        upgrade();
        // all unknown stuff must be deleted
        assertMissing("examples");
        assertMissing("unsupported_dir");
        // only care about unsupported directories lying around for now.
        //assertMissing("unsupported_file.txt");
    }

    /**
     * Readme files are definitely gonna be duplicated
     * @throws IOException
     */
    @Test
    public void testDuplicateFiles() throws IOException {
        File file = new File(homeDir, "bin/tasks/README.txt");
        file.getParentFile().mkdirs();
        FileUtils.writeStringToFile(file, "hi");
        upgrade();
        assertFileExists("bin/tasks/README.txt");
        assertMissing("MockTask");
    }
}
