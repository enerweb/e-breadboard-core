package za.co.enerweb.ebr.home_layout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;

import za.co.enerweb.toolbox.io.ResourceUtils;

@RunWith(JUnitParamsRunner.class)
public class OpenHomeLayoutTest extends AbstractHomeLayoutTest {

    @Test
    public void testEmptyDir() {
        AbstractHomeLayout homeLayout = layoutManager.openEbrHome(homeDir);
        assertEquals("automatically upgraded",
            HomeLayoutManager.getMaxLayoutVersion(), homeLayout.getVersion());
    }

    @Test
    public void testReadOnlyEmptyDir() {
        homeDir.setReadOnly();
        AbstractHomeLayout homeLayout = layoutManager.openEbrHome(homeDir);
        assertEquals(0, homeLayout.getVersion());
    }

    @Test
    @Parameters(method = "getLayoutVersions")
    public void testLayout(int layoutVersion) throws IOException {
        AbstractHomeLayout homeLayout = createLayout(layoutVersion, false);
        assertTrue("automatically upgraded",
            homeLayout.getVersion() >= HomeLayoutManager.getMaxLayoutVersion());
    }

    @Test
    @Parameters(method = "getLayoutVersions")
    public void testReadOnlyLayout(int layoutVersion) throws IOException {
        AbstractHomeLayout homeLayout = createLayout(layoutVersion, true);
        assertEquals("can't upgrade readonly home", layoutVersion,
            homeLayout.getVersion());
    }

    private AbstractHomeLayout createLayout(int version, boolean readOnly)
        throws IOException {
        FileUtils.cleanDirectory(homeDir);
        FileUtils.copyDirectory(
            ResourceUtils.getResourceAsFile("ebr_home_layout_" + version),
            homeDir);
        if (readOnly) {
            homeDir.setReadOnly();
        }
        return layoutManager.openEbrHome(homeDir);
    }
}
