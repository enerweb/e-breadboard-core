package za.co.enerweb.ebr.task;

import javax.ejb.EJB;

import org.apache.commons.io.IOUtils;
import org.apache.openejb.api.LocalClient;
import org.junit.Before;
import org.junit.Test;

import za.co.enerweb.ebr.server.AEbrTest;
import za.co.enerweb.ebr.server.TaskTestUtils;

@LocalClient
public class TestTaskPerspectiveParser extends AEbrTest {

    private static final String MY_VAR_VALUE = "x";
    private static final String TASK_ID = "123";
    private static final String ORIG_TASK_TITLE = "orig task";
    private static final String ORIG_TASK_DESCRIPTION = "orig task desc";

    private static final String OVERRIDDEN_TASK_TITLE = "overridden task";
    private static final String OVERRIDDEN_TASK_DESCRIPTION =
        "overridden task desc";
    private static final String TEST_PERSPECTIVE = "test_perspective";

    TaskMetadata origTask;

    @EJB
    ITaskRegistry taskRegistry;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUpAfterInjection() {
        origTask = new TaskMetadata();
        origTask.setId(TASK_ID);
        origTask.setTitle(ORIG_TASK_TITLE);
        origTask.setDescription(ORIG_TASK_DESCRIPTION);
        taskRegistry.registerTask(origTask);
    }

    @Test
    public void testParseBasicMetadata() throws Exception {
        taskRegistry.parseTaskPerspective(TEST_PERSPECTIVE,
            IOUtils.toInputStream("[" + TASK_ID + "]\n"
                + "title = " + OVERRIDDEN_TASK_TITLE + "\n"
                + "description = " + OVERRIDDEN_TASK_DESCRIPTION));
        TaskMetadata otmd = taskRegistry.getTaskMetadata(
            TEST_PERSPECTIVE, TASK_ID);
        TaskTestUtils.assertTaskBasicMetadata(otmd, TASK_ID,
            OVERRIDDEN_TASK_TITLE,
            OVERRIDDEN_TASK_DESCRIPTION, null, true);
    }

    @Test
    public void testParseWithVariables() throws Exception {
        taskRegistry.parseTaskPerspective(TEST_PERSPECTIVE,
            IOUtils.toInputStream("[default]\n"
                + "my_var = " + MY_VAR_VALUE + "\n"
                + "[" + TASK_ID + "]\n"
                + "title = " + OVERRIDDEN_TASK_TITLE + "${default/my_var}\n"
                + "description = " + OVERRIDDEN_TASK_DESCRIPTION
                + "${default/my_var}"));
        TaskMetadata otmd = taskRegistry.getTaskMetadata(
            TEST_PERSPECTIVE, TASK_ID);
        TaskTestUtils.assertTaskBasicMetadata(otmd, TASK_ID,
            OVERRIDDEN_TASK_TITLE + MY_VAR_VALUE,
            OVERRIDDEN_TASK_DESCRIPTION + MY_VAR_VALUE, null, true);
    }
}
