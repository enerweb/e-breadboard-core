package za.co.enerweb.ebr.tasks.example;

import java.util.Random;

import za.co.enerweb.ebr.task.AbstractTask;
import za.co.enerweb.ebr.task.TaskMetadata;

// TODO: move to the core plugin
public class Goodbye extends AbstractTask {
    private static final Random RANDOM = new Random();
    public static final String ID = "08f1efcd-9dfc-48af-aed4-3f80bd43b294";
    public static final String TEXT_INPUT = "text_input";
    public static final String INT_INPUT = "integer_input";
    public static final String TEXT_OUTPUT = "text_output";
    public static final String INT_OUTPUT = "integer_output";

    public static TaskMetadata getInitTaskMetaData() {
        TaskMetadata tmd =
            new TaskMetadata(Goodbye.ID, "Goodbye",
                "This is just a goodbye people task.", Goodbye.class);

        tmd.registerInput(String[].class, TEXT_INPUT,
            "This is just a sample text input", "my", false);
        // registerInput(String.class, INT_INPUT,
        // "This is just a sample text input", 123, true);
        tmd.registerOutput(String.class, TEXT_OUTPUT,
            "This is just a sample text output");
        tmd.registerOutput(Integer.class, INT_OUTPUT,
            "This is just a sample integer output");

        return tmd;
    }

    public Goodbye() {
    }

    @Override
    public void run() throws Exception {
        getLogger().info("running task.");
        Object inputValue = getInputEnv().getVariableValue(TEXT_INPUT);
        getLogger().info("inputValue=" + inputValue);
        // Object intValue = getInputValue(INT_INPUT);
        // log("intValue=" + intValue);

        StringBuilder sb = new StringBuilder();
        String[] strs = (String[]) inputValue;
        if (strs.length > 0) {
            sb.append("Goodbye ");
            if (strs.length > 1) {
                for (int i = 0; i < strs.length - 1; i++) {
                    sb.append(strs[i]);
                    if (i < strs.length - 2) {
                        sb.append(", ");
                    }
                }
                sb.append(" and ");
            }
            sb.append(strs[strs.length - 1]);
            sb.append(" :-(   -- we will miss you.. :'(");
        } else {
            sb.append("Noboddy is leaving!!");
        }

        getOutputEnv().setVariableValue(TEXT_OUTPUT, sb.toString());
        // setOutputValue(INT_OUTPUT, ((Integer)intValue).intValue() * 2);
        getOutputEnv().setVariableValue(INT_OUTPUT, RANDOM.nextInt());
        // getLogger().info("done with task.");
    }
}
