package za.co.enerweb.ebr.server;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Rule;
import org.junit.rules.TestName;

import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.toolbox.io.ResourceUtils;
import za.co.enerweb.toolbox.io.TempDir;
import za.co.enerweb.toolbox.openejb.ARestOpenEjbTest;

/**
 * All subclasses need @LocalClient if they want stuff to be injected
 */
@Slf4j
public abstract class AEbrTest extends ARestOpenEjbTest {
    public static final double EPSILON = 0.00001;
    public static final String EBR_TEST = "ebr-test";

    private List<File> deleteAfterTest = new ArrayList<File>();

    @Rule
    public TestName testName = new TestName();

    // @Rule
    // public TemporaryFolder tempFolder = new TemporaryFolder();

    /**
     * get a unique test-specific temp dir
     * @return
     */
    public File getTempDir() {
        return getTempDir(testName.getMethodName());
    }

    public File getTempDir(final String dirName) {
        File tempDirForTest = TempDir.createGroupedTempDir(EBR_TEST,
            dirName, true);
        deleteAfterTest.add(tempDirForTest);
        return tempDirForTest;
    }

    @After
    public final void tearDownOrchestrationEngine() {
        for (File dir : deleteAfterTest) {
            // log.debug("Deleting: temp dir:" + dir);
            FileUtils.deleteQuietly(dir);
        }
    }

    // should not be needed any more
    // /*
    // * Needed because some stuff still use the the EbrFactory eg.
    // * the db-managar
    // */
    // @After
    // public final void closeEbrFactory() {
    // EbrFactory.close();
    // }

    /*
     * for some reason the resource isn't always available right after
     * restarting..
     */
    @SneakyThrows
    public static void addDummyHome(String ebrHomeRootResource) {
        for (int i = 0;; i += 100) {
            if (i > 0) {
                log.debug("Sleeging waiting for " + ebrHomeRootResource + ": "
                    + i);
                Thread.sleep(i);
            }
            try {
                File root =
                    ResourceUtils.getResourceAsFile(ebrHomeRootResource);
                EbrFactory.getHomeService().addHome(root);
                EbrFactory.getTaskRegistry().refreshRegistry();
                return;
            } catch (Exception e) {
                if (i > 100) {
                    log.warn("Can't add dummy home :(", e);
                }
                if (i >= 10000) {
                    throw e;
                }
            }
        }
        // throw new RuntimeException("Give up waiting of resource: "
        // + ebrHomeRootResource);
    }
}
