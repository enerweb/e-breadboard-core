package za.co.enerweb.ebr.env;

import static org.junit.Assert.assertEquals;

import java.util.Map;

public class EnvVarTestUtils {

    public static void assertEqualEnvVarMetadata(final EnvVarMetadata expected,
        final EnvVarMetadata actual) {
        assertEquals(expected.getTypeName(), actual.getTypeName());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getAuxProperties(), actual.getAuxProperties());
    }

    public static void assertEnvironmentsAreEqual(final IEnvironment env,
        final IEnvironment loadedEnv, final IEnvironmentManager em) {
        for (Map.Entry<String, EnvVarItem> entry
                : env.getVariables().entrySet()) {
            String varName = entry.getKey();
            assertEqualEnvVarMetadata(
                env.getVariableMetadata(varName),
                loadedEnv.getVariableMetadata(varName));
            // test em.getVariableValue()
            assertEquals(env.getVariableValue(varName),
                em.getVariableValue(loadedEnv.getId(), varName));
        }
    }

}
