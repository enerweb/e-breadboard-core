package za.co.enerweb.ebr.task;

import java.io.File;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import za.co.enerweb.ebr.tasks.example.HelloWorld;
import za.co.enerweb.toolbox.io.TempDir;

@Slf4j
public class TestTaskUtils {
    private static final String PING = "ping";
    private TaskUtils taskUtils;
    private File tempDir;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        HelloWorld task = new HelloWorld();
        taskUtils = new TaskUtils(task);
        tempDir = TempDir.createTempDir();
    }

    @After
    public void tearDown() throws Exception {
        FileUtils.deleteQuietly(tempDir);
    }

    @Test
    @SneakyThrows
    public void testExcecuteCommand() {
        String os = getOs();
        if (os.equals("Linux")) {
            taskUtils.excecuteCommand(tempDir.getAbsolutePath(),
                    PING, "-c", "1", "localhost");
        } else if (os.equals("Windows")) {
            taskUtils.excecuteCommand(tempDir.getAbsolutePath(),
                    PING, "-n", "1", "localhost");

        } else {
            log.warn("could not run test for os: " + os);
        }
    }


    private String getOs() {
        return System.getProperty("os.name");
    }

}
