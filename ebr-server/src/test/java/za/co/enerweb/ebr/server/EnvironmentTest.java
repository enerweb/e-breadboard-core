package za.co.enerweb.ebr.server;


import static org.junit.Assert.assertEquals;

import javax.ejb.EJB;

import org.apache.openejb.api.LocalClient;
import org.junit.Test;

import za.co.enerweb.ebr.env.EnvVarItem;
import za.co.enerweb.ebr.env.EnvVarMetadata;
import za.co.enerweb.ebr.env.EnvVarTestUtils;
import za.co.enerweb.ebr.env.IEnvironmentManager;

@LocalClient
public class EnvironmentTest extends AEbrTest {
    private static final double FLOAT_VALUE = 0.3;

    @EJB
    private IEnvironmentManager environmentManager;

    @Test
    public void test() {
        String envId = environmentManager.newEnvironmentId();
        EnvVarMetadata aMetadata = new EnvVarMetadata(Integer.class, "a", "A");
        EnvVarItem aEnvItem = new EnvVarItem(aMetadata, 1);
        environmentManager.setVariable(envId, aEnvItem);

        EnvVarMetadata bMetadata = new EnvVarMetadata(String.class, "b", "B");
        environmentManager.setVariableValue(envId, bMetadata.getName(), "2");
        environmentManager.setVariableMetadata(envId, bMetadata);

        EnvVarMetadata cMetadata = new EnvVarMetadata(Float.class, "c", "C");
        // test other sequence of setting value and metadata
        environmentManager.setVariableMetadata(envId, cMetadata);
        environmentManager.setVariableValue(envId, cMetadata.getName(),
            FLOAT_VALUE);

        environmentManager.saveEnvironment(envId);

        EnvVarItem aEnvItem2 =
            environmentManager.getVariable(envId, aMetadata.getName());
        assertEquals(aEnvItem.getValue(), aEnvItem2.getValue());
        EnvVarTestUtils.assertEqualEnvVarMetadata(
            aMetadata, aEnvItem2.getMetadata());

        assertEquals("2",
            environmentManager.getVariableValue(envId, bMetadata.getName()));
        EnvVarTestUtils.assertEqualEnvVarMetadata(bMetadata,
            environmentManager.getVariableMetadata(envId, bMetadata.getName()));

        assertEquals(FLOAT_VALUE,
            environmentManager.getVariableValue(envId, cMetadata.getName()));
        EnvVarTestUtils.assertEqualEnvVarMetadata(cMetadata,
            environmentManager.getVariableMetadata(envId, cMetadata.getName()));

        environmentManager.deleteEnvironment(envId);
    }
}
