package za.co.enerweb.ebr.datatype;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.ejb.EJBException;

import org.apache.openejb.api.LocalClient;
import org.junit.Test;

import za.co.enerweb.toolbox.openejb.AOpenEjbTest;

@LocalClient
public class DataTypeRegistryServiceTest extends AOpenEjbTest {
    public static final class TestTypeConverter implements
        ITypeConverter<Serializable> {

        private static final long serialVersionUID = 1L;

        @Override
            public Object toSimpleStructure(final String envId,
                final String variableName,
                final Serializable specificObject) {
            return null;
        }

        @Override
        public Serializable fromSimpleStructure(final String envId,
            final String variableName, final Object simpleStructure) {
            return null;
        }

        @Override
        public String getType() {
            return TYPE_NAME;
        }
    }

    private static final String ALIAS = "byte";
    private static final String OLD_ALIAS = "BYTE";
    private static final String NAMESPACE = "ns";
    private static final Class<Byte> TYPE = Byte.class;
    private static final String TYPE_NAME = TYPE.getName();
    @EJB
    private IDataTypeRegistryService typeReg;

    public void setUpAfterInjection() throws Exception {
        // typeReg = EbrFactory.getDataTypeRegistryService();
        // typeReg.clearAliasRegistry();
        try {
            typeReg.lookupType(null, ALIAS);
            fail(ALIAS + " should not be registered yet");
        } catch (EJBException e) {
            // we must get here
        }
        try {
            typeReg.lookupType(null, OLD_ALIAS);
            fail(OLD_ALIAS + " should not be registered yet");
        } catch (EJBException e) {
            // we must get here
        }
    }

    @Test
    public void registerGlobalAliasTest() {
        typeReg.registerAlias(TYPE, null, ALIAS);
        String typeNameFound = typeReg.lookupType(null, ALIAS);
        assertEquals(TYPE_NAME, typeNameFound);

        typeNameFound = typeReg.lookupType(
            "namespace_not_containing_byte", ALIAS);
        assertEquals(TYPE_NAME, typeNameFound);
    }

    @Test
    public void registerNamespacedAliasTest() {
        typeReg.registerAlias(TYPE, NAMESPACE, ALIAS);
        String typeNameFound = typeReg
            .lookupType(NAMESPACE, ALIAS);
        assertEquals(TYPE_NAME, typeNameFound);
    }

    @Test
    public void registerDeprecatedGlobalAliasTest() {
        typeReg.registerAlias(TYPE, null, ALIAS);
        typeReg.registerDeprecatedAlias(ALIAS, null, OLD_ALIAS);
        String typeNameFound =
            typeReg.lookupType(null, OLD_ALIAS);
        assertEquals(TYPE_NAME, typeNameFound);

        typeNameFound = typeReg.lookupType(
            "namespace_not_containing_byte", OLD_ALIAS);
        assertEquals(TYPE_NAME, typeNameFound);
    }

    @Test
    public void registerDeprecatedNamespacedAliasTest() {
        typeReg.registerAlias(TYPE, NAMESPACE, ALIAS);
        typeReg.registerDeprecatedAlias(ALIAS, null, OLD_ALIAS);
        String typeNameFound = typeReg
            .lookupType(NAMESPACE, OLD_ALIAS);
        assertEquals(TYPE_NAME, typeNameFound);
    }

    @Test
    public void registerConverterTest() {
        ITypeConverter<Serializable> converter
            = new TestTypeConverter();
        typeReg.registerConverter(converter);
        ITypeConverter<Serializable> converterFound
            = typeReg.lookupConverter(null, TYPE_NAME);
        assertEquals(converter.getClass(), converterFound.getClass());

        typeReg.registerAlias(TYPE, NAMESPACE, ALIAS);
        converterFound = typeReg.lookupConverter(NAMESPACE, ALIAS);
        assertEquals(converter.getClass(), converterFound.getClass());
    }
}
