package za.co.enerweb.ebr.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import lombok.SneakyThrows;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import za.co.enerweb.ebr.home_layout.AbstractHomeLayout;

import com.google.common.collect.Lists;

/*
 * This should only be testing the thin HomeService layer.
 * XXX: This only runs with the latest home layout version.
 */
public class HomeServiceFilesTest extends AEbrTest {
    private static final String FILE_NAME = "example.txt";
    private static final String FILE_CONTENT = "#file content";
    private static final HomeFileSpec hfsFile = new HomeFileSpec(
        HomeFileCategory.TRASH, FILE_NAME);
    private static final HomeFileSpec hfsTrash = new HomeFileSpec(
        HomeFileCategory.TRASH);

    private HomeService homeService;
    private File ebrHome;

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Before
    @SneakyThrows
    public void openHome() {
        // manually create a home instance so that we can test that it
        // works as expected without having to add apis!
        homeService = new HomeService();
        homeService.open();
        ebrHome = homeService.getMainHomeDir();
    }

    protected AbstractHomeLayout addHome() {
        File newHomeFolder = tempFolder.newFolder(getClass().getSimpleName()
            + "_additional_home");
        return homeService.addHome(newHomeFolder, null);
    }

    @After
    public void tearDown() {
        homeService.close();
    }

    @Test(expected = RuntimeException.class)
    public void testDontFindFileAsAbsPath() throws FileNotFoundException {
        homeService.findFile(hfsFile).getAbsolutePath();
    }

    @Test
    public void testFindFileAsAbsPath() throws IOException {
        homeService.writeFile(hfsFile, FILE_CONTENT);
        assertFoundHomeFilePath(homeService.findFile(hfsFile));
    }

    @Test
    public void testFindFileInSecondHome() throws IOException {
        AbstractHomeLayout home2 = addHome();
        home2.writeFile(hfsFile, FILE_CONTENT);
        assertFoundHomeFilePath(home2.getRoot(), homeService.findFile(hfsFile));
    }

    @Test
    public void testFindFiles() throws IOException {
        homeService.writeFile(hfsFile, FILE_CONTENT);

        // findFiles
        Iterator<HomeFile> foundFiles = homeService.findFiles(hfsTrash,
            new String[] {"txt"}).iterator();
        assertFoundHomeFilePath(foundFiles.next());

        // list
        foundFiles = homeService.list(hfsTrash).iterator();
        assertFoundHomeFilePath(foundFiles.next());
    }

    @Test
    public void testFindFilesInMultipleHomes() throws IOException {
        homeService.writeFile(hfsFile, FILE_CONTENT);
        AbstractHomeLayout home2 = addHome();
        home2.writeFile(hfsFile, FILE_CONTENT);

        // findFiles
        Iterator<HomeFile> foundFiles = homeService.findFiles(hfsTrash,
            new String[] {"txt"}).iterator();
        assertFoundHomeFilePath(foundFiles.next());
        assertFoundHomeFilePath(home2.getRoot(), foundFiles.next());
        assertFalse(foundFiles.hasNext());

        // list
        foundFiles = homeService.list(hfsTrash).iterator();
        assertFoundHomeFilePath(foundFiles.next());
        assertFoundHomeFilePath(home2.getRoot(), foundFiles.next());
        assertFalse(foundFiles.hasNext());
    }

    @Test
    public void cleanDir() throws IOException {
        long beforeCreatingFiles = new Date().getTime();

        homeService.writeFile(hfsFile, FILE_CONTENT);
        String SUBDIR = "subdir";
        HomeFileSpec hfsDir = new HomeFileSpec(
            HomeFileCategory.TRASH, SUBDIR);
        HomeFileSpec hfsFile2 = hfsDir.subDir(FILE_NAME);
        homeService.writeFile(hfsFile2, FILE_CONTENT);

        homeService.cleanDir(hfsTrash, beforeCreatingFiles - 1000, false);

        // findFiles
        ArrayList<HomeFile> foundFiles = Lists.newArrayList(
            homeService.findFiles(hfsTrash, new String[] {"txt"}));
        assertEquals(2, foundFiles.size());
        assertFoundHomeFilePath(foundFiles.get(0));
        assertFoundHomeFilePath(foundFiles.get(1));
        assertTrue(foundFiles.get(0).getAbsolutePath().contains(SUBDIR));

        homeService.cleanDir(hfsTrash, new Date().getTime() + 1000, false);

        foundFiles = Lists.newArrayList(
            homeService.findFiles(hfsTrash, new String[] {"txt"}));
        assertEquals(0, foundFiles.size());
    }

    private void assertFoundHomeFilePath(HomeFile homeFile) {
        assertFoundHomeFilePath(ebrHome, homeFile);
    }

    private void assertFoundHomeFilePath(File ancestorDir, HomeFile homeFile) {
        String absPath = homeFile.getFile().getAbsolutePath();
        assertTrue(absPath.startsWith(ancestorDir.getAbsolutePath()));
        assertTrue(absPath.endsWith(FILE_NAME));
    }

}
