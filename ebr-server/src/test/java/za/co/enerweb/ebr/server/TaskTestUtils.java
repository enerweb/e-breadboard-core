package za.co.enerweb.ebr.server;

import static org.junit.Assert.assertEquals;

import java.io.Serializable;

import junit.framework.Assert;
import za.co.enerweb.ebr.task.ATaskParser;
import za.co.enerweb.ebr.task.InputMetadata;
import za.co.enerweb.ebr.task.OutputMetadata;
import za.co.enerweb.ebr.task.ParameterEnvironment;
import za.co.enerweb.ebr.task.ParameterMetadata;
import za.co.enerweb.ebr.task.TaskMetadata;

public class TaskTestUtils {

    public static void assertTaskBasicMetadata(final TaskMetadata tmd,
        final String id,
        final String title, final String description, final String layout,
        final boolean visisble) {
        assertEquals(id, tmd.getId());
        assertEquals(title, tmd.getTitle());
        assertEquals(description, tmd.getDescription());
        assertEquals(layout, tmd.getLayout());
        assertEquals(visisble, tmd.isVisible());
    }

    public static void assertParameterMetadata(final ParameterMetadata pmd,
        final String name, final String caption, final String nativeType,
        final String typeName,
        final String description,
        final ParameterEnvironment parameterEnvironment,
        final String namespace) {
        assertEquals(typeName, pmd.getTypeName());
        Assert.assertNotNull(pmd.getTypeName());
        assertEquals(name, pmd.getName());
        if (caption != null) { // only test it if it was specified
            assertEquals(caption, pmd.getCaption());
        }
        assertEquals(description, pmd.getDescription());
        assertEquals(parameterEnvironment, pmd.getEnvironment());
        if (namespace != null) { // only test it if it was specified
            assertEquals(namespace, pmd.getNamespace());
        }
        assertEquals(nativeType, pmd.getAuxProperties().get(
            ATaskParser.VARIABLE_PROPERTY_NATIVE_TYPE));
    }

    public static void assertInputMetadata(final TaskMetadata tmd,
        final String name,
        final String caption,
        final String nativeType, final String typeName,
        final String description,
        final Serializable defaultValue,
        final ParameterEnvironment parameterEnvironment,
            final String namespace) {
        InputMetadata inp_md = tmd.getInputMetadata(name);
        assertParameterMetadata(inp_md, name, caption, nativeType, typeName,
            description, parameterEnvironment, namespace);
        assertEquals(defaultValue, inp_md.getDefaultValue());
    }

    public static void assertOutputMetadata(final TaskMetadata tmd,
        final String name, final String caption,
        final String nativeType, final String typeName,
        final String description,
        final ParameterEnvironment parameterEnvironment,
        final String namespace) {
        OutputMetadata outp_md = tmd.getOutputMetadata(name);
        assertParameterMetadata(outp_md, name, caption, nativeType, typeName,
            description, parameterEnvironment, namespace);
    }

}
