package za.co.enerweb.ebr.home_layout;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;

import javax.ejb.EJB;

import lombok.NoArgsConstructor;

import org.junit.runners.Parameterized.Parameters;

import za.co.enerweb.ebr.server.AEbrTest;
import za.co.enerweb.ebr.task.ITaskRegistry;

/**
 * All subclasses need @LocalClient if they want stuff to be injected
 */
@NoArgsConstructor
public abstract class AbstractHomeLayoutTest extends AEbrTest {

    @EJB
    protected ITaskRegistry taskRegistry;
    protected HomeLayoutManager layoutManager;
    protected File homeDir;

//    @Rule
//    public TemporaryFolder tempFolder = new TemporaryFolder();

    public void setUpAfterInjection() throws Exception {
        super.setUpAfterInjection();
//        tempFolder = getTempDir();
//        homeDir = tempFolder.newFolder(getClass().getSimpleName());
        homeDir = getTempDir();
        layoutManager = new HomeLayoutManager(taskRegistry);
    }

    // @After
    // public void tearDownHomeDir() {
    // if (homeDir != null) {
    // homeDir.setWritable(true);
    // }
    // }

    /*
     * For running the tests for all layout versions
     */
    protected static Object[] getLayoutVersions() {
        // XXX: eventually we should run this for all supported layouts
        // automagically
      return new Object[][] { { 0 }, { 1 } };
    }

    @SuppressWarnings("rawtypes")
    @Parameters
    public static Collection getLayoutVersionCollection() {
      Object[] layoutVersions = getLayoutVersions();
    return Arrays.asList(layoutVersions);
    }
}
