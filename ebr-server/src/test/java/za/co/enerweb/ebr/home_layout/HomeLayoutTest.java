package za.co.enerweb.ebr.home_layout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.openejb.api.LocalClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import za.co.enerweb.ebr.server.HomeFile;
import za.co.enerweb.ebr.server.HomeFileCategory;
import za.co.enerweb.ebr.server.HomeFileSpec;
import za.co.enerweb.ebr.server.IHomeService;

/*
 * These test run for all layout versions!
 */
@LocalClient
@RunWith(value = Parameterized.class)
public class HomeLayoutTest extends AbstractHomeLayoutTest {

    private static final String SUBDIR1 = "subdir1";
    private static final String FILE_NAME = "example.txt";
    private static final String FILE_CONTENT = "#file content";
    private int filesInSubdir = 0;
    private static HomeFileSpec hfs = new HomeFileSpec(HomeFileCategory.TRASH,
        FILE_NAME);
    private HomeFileSpec hfsSubDir = new HomeFileSpec(
        HomeFileCategory.TRASH, SUBDIR1);
    private static HomeFileSpec hfsInSubdir = new HomeFileSpec(
        HomeFileCategory.TRASH, SUBDIR1, FILE_NAME);
    private static HomeFileSpec hfsWeDontWant = new HomeFileSpec(
        HomeFileCategory.TRASH, SUBDIR1, FILE_NAME + "file_we_dont_want");
    private static HomeFileSpec hfsWithTilde = new HomeFileSpec(
        HomeFileCategory.TRASH, SUBDIR1, "~" + FILE_NAME);
    private static HomeFileSpec hfsWithTildeDir = new HomeFileSpec(
        HomeFileCategory.TRASH, SUBDIR1, "~", "tilde" + FILE_NAME);

    private int layoutVersion;
    protected AbstractHomeLayout homeLayout;

    public HomeLayoutTest(int layoutVersion) {
        this.layoutVersion = layoutVersion;
    }

    public void setUpAfterInjection() throws Exception {
        super.setUpAfterInjection();
        homeLayout = layoutManager.initLayout(homeDir, layoutVersion);
    }

    private void checkFileInSubdir(HomeFile homeFile) {
        if (pathContains(homeFile, SUBDIR1)) {
            filesInSubdir++;
        }
    }

    private boolean pathContains(HomeFile homeFile, String part) {
        return homeFile.getRelPath().contains(part) &&
            homeFile.getFile().getAbsolutePath().contains(part);
    }

    @Test
    public void testWriteAndFindFile() throws IOException {
        homeLayout.writeFile(
            hfs, FILE_CONTENT);
        HomeFile foundFile = homeLayout.findFile(hfs);
        assertNotNull("not found", foundFile);
        assertEquals(FILE_CONTENT, foundFile.getContent());
    }

    @Test
    public void testWriteAndFindFileInNonexistingDir() throws IOException {
        homeLayout.writeFile(hfsInSubdir, FILE_CONTENT);
        HomeFile foundFile = homeLayout.findFile(hfsInSubdir);
        assertNotNull("not found", foundFile);
        assertEquals(FILE_CONTENT, foundFile.getContent());
        assertTrue(foundFile.getAbsolutePath().contains(SUBDIR1));
    }

    @Test
    public void testWriteAndGetFile() throws IOException {
        homeLayout.writeFile(hfs, FILE_CONTENT);
        HomeFile foundFile = homeLayout.getFile(hfs);
        assertNotNull("not found", foundFile);
        assertEquals(FILE_CONTENT, foundFile.getContent());
        assertTrue(foundFile.exists());
    }

    @Test
    public void testGetFile() throws IOException {
        HomeFile foundFile = homeLayout.getFile(hfs);
        assertNotNull("not found", foundFile);
        assertFalse(foundFile.exists());
    }

    protected void writeFiles() throws IOException {
        homeLayout.writeFile(hfsWeDontWant, FILE_CONTENT);
        homeLayout.writeFile(hfsWithTilde, FILE_CONTENT);
        homeLayout.writeFile(hfsWithTildeDir, FILE_CONTENT);
        homeLayout.writeFile(hfs, FILE_CONTENT);
        homeLayout.writeFile(hfsInSubdir, FILE_CONTENT);
    }

    @Test
    public void testWriteAndFindFiles() throws IOException {
        writeFiles();
        Iterator<HomeFile> foundFiles = homeLayout.findFiles(
            new HomeFileSpec(HomeFileCategory.TRASH),
            new String[] {"txt"});

        for (int i = 0; i < 2; i++) {
            HomeFile homeFile = foundFiles.next();
            assertNotNull("not found", homeFile.getFile());
            assertEquals(FILE_CONTENT, homeFile.getContent());
            checkFileInSubdir(homeFile);
        }

        assertEquals(1, filesInSubdir);
        assertFalse(foundFiles.hasNext());
    }

    @Test
    public void testWriteAndList() throws IOException {
        writeFiles();
        Iterator<HomeFile> foundFiles = homeLayout.list(
            new HomeFileSpec(HomeFileCategory.TRASH));

        int fileCount = 0;
        int dirCount = 0;
        for (int i = 0; i < 2; i++) {
            HomeFile homeFile = foundFiles.next();
            assertNotNull("not found", homeFile.getFile());
            if (homeFile.isFile()) {
                assertEquals(FILE_CONTENT, homeFile.getContent());
                fileCount++;
            }
            if (homeFile.isDirectory()) {
                dirCount++;
            }
            checkFileInSubdir(homeFile);
        }

        assertEquals(1, filesInSubdir);
        assertFalse(foundFiles.hasNext());
        assertEquals(1, fileCount);
        assertEquals(1, dirCount);
    }

    @Test
    public void testDeleteFile() throws IOException {
        homeLayout.writeFile(hfsInSubdir, FILE_CONTENT);

        homeLayout.deleteFile(hfsInSubdir);
        assertFalse(homeLayout.getFile(hfsInSubdir).exists());
    }

    @Test
    public void testDeleteHomeFile() throws IOException {
        homeLayout.writeFile(hfsInSubdir, FILE_CONTENT);

        HomeFile homeFile = homeLayout.deleteHomeFile(
            homeLayout.getFile(hfsInSubdir));
        assertFalse(homeLayout.getFile(hfsInSubdir).exists());

        assertEquals(FILE_CONTENT, homeFile.getContent());
        assertTrue(homeFile.getRelPath().endsWith(FILE_NAME));
        assertTrue(homeFile.getRelPath().startsWith(
            homeLayout.getFile(new HomeFileSpec(HomeFileCategory.TRASH))
                .getRelPath()));
    }

    @Test
    public void testDeleteDir() throws IOException {
        homeLayout.writeFile(hfsInSubdir, FILE_CONTENT);
        homeLayout.deleteFile(hfsSubDir);
        assertFalse(homeLayout.getFile(hfsSubDir).exists());
        assertFalse(homeLayout.getFile(hfsInSubdir).exists());
    }

    @Test
    public void testDeleteHomeDir() throws IOException {
        homeLayout.writeFile(hfsInSubdir, FILE_CONTENT);

        HomeFile homeFile = homeLayout.deleteHomeFile(
            homeLayout.getFile(hfsSubDir));
        assertFalse(homeLayout.getFile(hfsSubDir).exists());
        assertFalse(homeLayout.getFile(hfsInSubdir).exists());

        assertEquals(FILE_CONTENT, FileUtils.readFileToString(
            new File(homeFile.getDir(), FILE_NAME)));
        assertTrue(homeFile.getRelPath().endsWith(SUBDIR1));
        assertTrue(homeFile.getRelPath().startsWith(
            homeLayout.getFile(new HomeFileSpec(HomeFileCategory.TRASH))
                .getRelPath()));
    }

    @Test
    public void testGetTempFile() throws IOException {
        String templateFileName = " $@#>xx123.txt";
        String fixedFileName = "_____xx123.txt";
        HomeFile homeFile = homeLayout.getTempFile(templateFileName);
        assertNotNull("not found", homeFile);
        assertNotNull("not found", homeFile.getFile());
        assertTrue(homeFile.getRelPath().endsWith(fixedFileName));
        assertTrue(homeFile.getRelPath().startsWith(
            homeLayout.getFile(new HomeFileSpec(HomeFileCategory.TEMP))
                .getRelPath()));
    }

    @Test
    public void testWriteTaskPerspectivesReadmeFiles() throws IOException,
        InterruptedException {
        homeLayout.writeTaskPerspectivesReadmeFile();
        HomeFile taskPerspectivesReadme = homeLayout.getFile(
            new HomeFileSpec(
                HomeFileCategory.TASK_PERSPECTIVES, IHomeService.README));
        assertEquals(AbstractHomeLayout.TASK_PERSPECTIVES_README_CONTENT,
            taskPerspectivesReadme.getContent());
    }

    @Test
    public void testWriteTaskReadmeFiles() throws IOException,
        InterruptedException {
        homeLayout.writeTaskReadmeFiles();
        List<HomeFile> homeFiles = homeLayout.getFiles(
            new HomeFileSpec(
                HomeFileCategory.TASK_COMMON, IHomeService.README));
        assertTrue(homeFiles.size() > 0);
        for (HomeFile homeFile : homeFiles) {
            assertEquals(AbstractHomeLayout.COMMON_README_CONTENT,
                homeFile.getContent());
        }
        homeFiles = homeLayout.getFiles(
            new HomeFileSpec(
                HomeFileCategory.TASKS, IHomeService.README));
        assertTrue(homeFiles.size() > 0);
        for (HomeFile homeFile : homeFiles) {
            assertEquals(AbstractHomeLayout.TASK_README_CONTENT,
                homeFile.getContent());
        }
    }
}
