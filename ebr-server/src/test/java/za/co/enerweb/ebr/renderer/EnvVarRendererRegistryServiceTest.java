package za.co.enerweb.ebr.renderer;

import static org.junit.Assert.assertTrue;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import lombok.SneakyThrows;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import za.co.enerweb.ebr.env.EnvVarMetadata;

public class EnvVarRendererRegistryServiceTest {
    private static final int RANK = 50;
    private static final String TECHNOLOGY_NAME = "dummy technology";
    protected static final List<String> SUPPORTED_TECHNOLOGIES =
        Collections.singletonList(TECHNOLOGY_NAME);

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        EnvVarRendererRegistryService.clearRegistry();
    }

    @After
    public void tearDown() throws Exception {
        EnvVarRendererRegistryService.clearRegistry();
    }

    @Test
    public void testSimpleLookups() {
        registerRenderer(Serializable.class, DummyRenderer.class, RANK, true);
        assertLookup("direct lookup", Serializable.class, DummyRenderer.class);
        assertLookup("interface lookup", Integer.class, DummyRenderer.class);
    }

    @Test
    public void testSuperClassLookups() {
        registerRenderer(Number.class, DummyRenderer.class, RANK, false);
        assertLookup("direct lookup", Number.class, DummyRenderer.class);
        assertLookup("super class lookup", Integer.class, DummyRenderer.class);
    }

    @Test
    public void testLookupByRank() {
        registerRenderer(Serializable.class, DummyRenderer.class, 0, true);
        registerRenderer(Number.class, DummyRenderer1.class, 1, true);
        assertLookup("direct lookup", Serializable.class, DummyRenderer.class);
        assertLookup("direct lookup", Number.class, DummyRenderer1.class);
        assertLookup("super class lookup", Integer.class, DummyRenderer1.class);
    }

    @Test
    public void testLookupByRankTieBreak() {
        registerRenderer(Serializable.class, DummyRenderer.class, 0, true);
        registerRenderer(Serializable.class, DummyRenderer1.class, 0, true);
        assertLookup("direct lookup", Serializable.class, DummyRenderer1.class);
    }

    @SneakyThrows
    private void registerRenderer(final Class<? extends Serializable> type,
        final Class<?> renderer, final int rank, final boolean editable) {
        EnvVarRendererRegistryService.registerRenderer(
            getClass().getSimpleName() + "/" + renderer.getName(),
            renderer, TECHNOLOGY_NAME,
            new String[]{type.getName()},
            new byte[] {(byte) rank}, RendererMode.EDIT);
    }

    @SneakyThrows
    private void assertLookup(final String msg,
        final Class<? extends Serializable> type,
        final Class<?> expectedRenderer
    ) {
        assertLookup(msg + " editable", type, expectedRenderer, true);
        assertLookup(msg + " non-editable", type, expectedRenderer, false);
    }

    @SneakyThrows
    private void assertLookup(final String msg,
        final Class<? extends Serializable> type,
        final Class<?> expectedRenderer, final boolean editable
        ) {
        EnvVarMetadata md = new EnvVarMetadata();
        md.setTypeClass(type);
        Object renderer = EnvVarRendererRegistryService.getRenderer(
            SUPPORTED_TECHNOLOGIES, md, editable);
        assertTrue(msg, expectedRenderer.isInstance(renderer));
    }

}
