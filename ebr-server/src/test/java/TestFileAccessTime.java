import java.io.File;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;


public class TestFileAccessTime {
    public static void main(String[] args) throws Exception {
        File f = new File("/tmp/amanica/properties.index");
        for (int i = 0; i < 5; i++) {
            FileTime lastAccessTime =
                (FileTime) Files.getAttribute(f.toPath(), "lastAccessTime");
            System.out.println("a " + lastAccessTime.toMillis());

            // FileTime lastModifiedTime =
            // (FileTime) Files.getAttribute(f.toPath(),
            // "lastModifiedTime");
            // System.out.println("m " + lastModifiedTime.toMillis());

            BasicFileAttributes attrs = Files.readAttributes(
                f.toPath(), BasicFileAttributes.class);
            FileTime accessTime = attrs.lastAccessTime();
            System.out.println("b " + accessTime.toMillis());
            System.out.println("c " + accessTime);
            Thread.sleep(1000);
        }
        // Files.setAttribute(path, "lastAccessTime", fileTime);
    }
}
