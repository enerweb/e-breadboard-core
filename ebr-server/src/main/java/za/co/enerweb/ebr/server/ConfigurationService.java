package za.co.enerweb.ebr.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

import javax.ejb.Singleton;

import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;

import org.ini4j.Ini;
import org.ini4j.Profile.Section;

import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.annotations.OnEbrStart;

/**
 * maybe make some property metadata so we dont have to manually read and
 * write each value eg.
 * @Data
 *       class X{
 *       int x = 123; // automatically read this from the property file as int..
 *       // and the default is 123, maybe even store the defaults somewhere safe
 *       // before loading so it can be reset from frontend..
 *       }
 *       http://stackoverflow.com/questions/29197149/injecting-constants-via-
 *       custom-annotation/29600519#29600519
 */
@Singleton
@Slf4j
public/*final ejbs doesn't like this :(*/class ConfigurationService
    implements IConfigurationService {


    private Ini confIni;

    private Ini getConfIni() {
        if (confIni == null) {


            try {
                File f = EbrFactory.getConfFile(FILENAME);
                if (f.exists()) {
                    Ini ini = new Ini();
                    try {
                        @Cleanup InputStream inputStream =
                            new FileInputStream(f);
                        ini.load(inputStream);
                        confIni = ini;
                    } catch (Exception e) {
                        log.warn("Could not parse config file: '" + f + "'",
                            e);
                    }
                }
            } catch (Exception e) {
                log.debug("Could not init.", e);
            }
        }
        return confIni;
    }

    public Set<String> getConfSections() {
        return getConfIni().keySet();
    }

    public ConfSection getSection(String sectionName) {
        Section ret = getConfIni().get(sectionName);
        if (ret == null) {
            ret = getConfIni().add(sectionName);
        }
        return new ConfSection(ret);
    }

    public ConfSection getCoreSection() {
        return getSection(CORE_SECTION);
    }

    public ConfSection getCoreCleanupSection() {
        return getSection(CORE_CLEANUP_SECTION);
    }

    public void reload() {
        confIni = null;
    }

    @OnEbrStart(rank = 1010)
    public static void writeExample() throws IOException {
        EbrFactory.getHomeService().writeFileFromClasspath(
            HomeFileCategory.ETC.spec(),
            "za.co.enerweb.ebr.examples/config_example.ini");
    }
}
