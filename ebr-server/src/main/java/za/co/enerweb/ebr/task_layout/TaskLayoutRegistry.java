package za.co.enerweb.ebr.task_layout;

import java.util.HashMap;
import java.util.List;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.exceptions.InvalidMetadataException;
import za.co.enerweb.ebr.exceptions.ServiceException;
import za.co.enerweb.ebr.task.ATaskParser;

@Singleton
@Lock(LockType.READ)
@Slf4j
public final class TaskLayoutRegistry implements
    ITaskLayoutRegistry {
    // public static final String RESOURCES_PATH_TASK_LAYOUTS = "task-layouts";
    // public static final String RESOURCES_PATH_EXAMPLE_LAYOUTS =
    // "example_layouts";

    /**
     * Maps layout alias to a layout type.
     */
    private static class LayoutIdToMetadataMap extends
        HashMap<String, TaskLayoutMetadata> {

        private static final long serialVersionUID = 1L;
    }

    /**
     * Maps technology to LayoutIdToTypeMap map.
     */
    private static class TechToLayoutIdMap extends
        HashMap<String, LayoutIdToMetadataMap> {

        private static final long serialVersionUID = 1L;
    }

    /**
     * For each technology there is a map of layout.id -> layout metadata
     */
    private TechToLayoutIdMap techToLayoutIdMap =
        new TechToLayoutIdMap();

    public void clearRegistry() {
        techToLayoutIdMap.clear();
    }

    private static void couldNotInstantiateLayout(final Exception e) {
        log.error("Could not instantiate TaskLayout", e);
    }

    public ITaskLayout getLayout(
        final List<String> supportedTechs,
        final String layoutId) {
        TaskLayoutMetadata layoutMetadata = null;
        // find the first matching id, traversing the technologies
        // in the order given.
        for (String layoutTechnology : supportedTechs) {
            try {
                LayoutIdToMetadataMap metadataMap =
                    techToLayoutIdMap.get(layoutTechnology);
                if (metadataMap != null) {
                    layoutMetadata =
                        metadataMap.get(layoutId);
                    if (layoutMetadata != null) {
                        String deprecatedById =
                            layoutMetadata.getDeprecatedById();
                        if (deprecatedById != null) {
                            log.warn(
                                ATaskParser.VARIABLE_NAME_TASK_LAYOUT
                                    + " '{}' is deprecated, "
                                    + "please use: {}",
                                    layoutId, deprecatedById);
                        }
                        ITaskLayout taskLayout = (ITaskLayout) layoutMetadata
                            .getTypeClass().newInstance();
                        taskLayout.setTaskLayoutMetadata(layoutMetadata);
                        return taskLayout;
                    }
                }
            } catch (InstantiationException e) {
                couldNotInstantiateLayout(e);
            } catch (IllegalAccessException e) {
                couldNotInstantiateLayout(e);
            } catch (ClassNotFoundException e) {
                couldNotInstantiateLayout(e);
            }
        }
        if (layoutId != null) {
            log.error("Unknown {}: {}",
                ATaskParser.VARIABLE_NAME_TASK_LAYOUT, layoutId);
            return getLayout(supportedTechs, null);
        }
        throw new ServiceException("No default "
            + ATaskParser.VARIABLE_NAME_TASK_LAYOUT
            + " available, please specify one.");
    }

    public synchronized void registerLayout(final String id,
        final String technology,
        final TaskLayoutMetadata taskLayoutMetadata) {
        // log.debug("Registering layout: " + id);
        LayoutIdToMetadataMap idToRendererMap =
            techToLayoutIdMap.get(technology);
        if (idToRendererMap == null) {
            idToRendererMap = new LayoutIdToMetadataMap();
            techToLayoutIdMap.put(technology, idToRendererMap);
        }

        TaskLayoutMetadata existingLayout =
            idToRendererMap.put(id, taskLayoutMetadata);
        if (existingLayout != null &&
            !existingLayout.getTypeName().equals(
                taskLayoutMetadata.getTypeName())) {
            throw new InvalidMetadataException(
                "Two layouts are using the same id, please change one:\n  "
                    + existingLayout.getTypeName()
                    + "\n  " + taskLayoutMetadata.getTypeName() + "\n  "
                    + id);
        }
    }
}
