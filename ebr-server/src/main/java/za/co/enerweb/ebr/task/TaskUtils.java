package za.co.enerweb.ebr.task;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

import org.slf4j.Logger;

import za.co.enerweb.ebr.task.TaskExecutionMetadata;
import za.co.enerweb.toolbox.io.CommandUtils;
import za.co.enerweb.toolbox.io.OutputHandler;

import com.google.common.base.Joiner;

/**
 * TODO: the implementations should probably move to the javatoolbox
 */
@Slf4j
public class TaskUtils {
    private static final class StdErrHandler implements OutputHandler {
        private final Logger log;
        TaskExecutionMetadata taskExecutionMetadata;

        /**
         * @param log
         */
        private StdErrHandler(final TaskExecutionMetadata taskExecutionMetadata, final Logger log) {
            this.taskExecutionMetadata = taskExecutionMetadata;
            this.log = log;
        }

        @Override
        public void handleLine(final String line) {
            // make sure streams are read..
            if (taskExecutionMetadata != null) {
                taskExecutionMetadata.setMessage(line);
            }
            if (log != null) {
                log.warn(line);
            }
        }

        @Override
        public void clear() {
        }
    }

    private static final class StdOutHandler implements OutputHandler {
        private final TaskExecutionMetadata taskExecutionMetadata;
        private final Logger log;

        /**
         * @param taskExecutionMetadata
         * @param log
         */
        private StdOutHandler(final TaskExecutionMetadata taskExecutionMetadata, final Logger log) {
            this.taskExecutionMetadata = taskExecutionMetadata;
            this.log = log;
        }

        @Override
        public void handleLine(final String line) {
            // make sure streams are read..
            if (taskExecutionMetadata != null) {
                taskExecutionMetadata.setMessage(line);
            }
            if (log != null) {
                log.info(line);
            }
        }

        @Override
        public void clear() {
        }
    }

    private ITask task;

    public TaskUtils(final ITask task) {
        this.task = task;
    }

    /**
     * Tries to detect the proxy from the system preferences,
     * or try to parse the applicable environment variables
     * @param url the url you will be using the proxy for
     * @return {{type, host, port}} or
     *         {{null}} for a direct connection to the internet
     *         (or no proxy configured)
     */
    public String[][] getProxyConfigs(final String url) {
        // XXX: avoid adding the same values more than once
        List<String[]> ret = new ArrayList<String[]>();
        try {
            System.setProperty("java.net.useSystemProxies", "true");
            URI targetUri = new URI(url);
            List<Proxy> proxies = ProxySelector.getDefault().select(targetUri);

            for (Proxy proxy : proxies) {
                InetSocketAddress addr = (InetSocketAddress)
                    proxy.address();

                if (addr == null) {
                    log.debug("No Proxy Configured for " + url);
                } else {
                    ret.add(new String[] {
                        proxy.type().toString().toLowerCase(),
                        addr.getHostName(), String.valueOf(addr.getPort())});
                }
            }

            // check environment variables
            URL targetUrl = new URL(url);
            String proxyEnvVarName = targetUrl.getProtocol() + "_proxy";
            String proxyEnvVarValue = System.getenv(proxyEnvVarName);
            if (proxyEnvVarValue != null) {
                String noProxyEnvVarValue = System.getenv("no_proxy");
                if (noProxyEnvVarValue != null) {
                    String targetHost = targetUrl.getHost();
                    String[] noProxyHosts = noProxyEnvVarValue.split("[, ;]+");
                    log.debug("noProxyHosts: "
                        + Joiner.on(", ").join(noProxyHosts));
                    for (String noProxyHost : noProxyHosts) {
                        if (targetHost.equals(noProxyHost)) {
                            return null; // direct
                        }
                    }
                }
                URL proxyUrl = new URL(proxyEnvVarValue);

                ret.add(new String[] {proxyUrl.getProtocol(),
                    proxyUrl.getHost(), String.valueOf(proxyUrl.getPort())});
            }
        } catch (Exception e) {
            log.info("No Proxy Configured for " + url, e);
            ret.add(null); // avoid duplicate logging
        }
        if (ret.size() == 0) {
            log.debug("No Proxy Configured for " + url);
            ret.add(null);
        }
        String[][] retArray = ret.toArray(
            new String[ret.size()][]);
        return retArray;
    }

    /**
     * @param url
     * @return the first proxy config we find, because some clients
     *         will ignore the orthers in any case.
     */
    public String[] getProxyConfig(final String url) {
        return getProxyConfigs(url)[0];
    }

    /**
     * In future we may want to return an object with the output and the\
     * stderr output, returncode etc.
     * @param workingDirPath
     * @param task
     * @param commandAndArgs
     * @throws IOException
     * @throws InterruptedException
     */
    public void excecuteCommand(final String workingDirPath,
        final String executable, final String... args) throws IOException,
        InterruptedException {
        final TaskExecutionMetadata taskExecutionMetadata = task.getTaskExecutionMetadata();
        final Logger logger = task.getLogger();

        File workingDir = new File(workingDirPath);

        // execWithPlexus(taskExecutionMetadata, log, workingDir, executable, args);

        execWithJavaToolbox(taskExecutionMetadata, logger, workingDir, executable, args);
    }

    private void execWithJavaToolbox(final TaskExecutionMetadata taskExecutionMetadata,
        final Logger logger, final File workingDir, final String executable,
        final String... args) throws IOException {
        OutputHandler stdout = new StdOutHandler(taskExecutionMetadata, logger);

        OutputHandler stderr = new StdErrHandler(task.getTaskExecutionMetadata(), logger);

        CommandUtils cu = new CommandUtils(executable, args);
        cu.setStdoutHandler(stdout);
        cu.setStderrHandler(stderr);
        cu.setWorkingDir(workingDir);
        cu.excecute();
    }

}
