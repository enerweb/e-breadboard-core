package za.co.enerweb.ebr.env;

import java.util.Map;

import za.co.enerweb.toolbox.collections.Cache;

public class EnvironmentCache extends Cache<String, IEnvironment> {

    private IEnvironmentStorage envStorage;

    public EnvironmentCache(final IEnvironmentStorage envStorage) {
        this.envStorage = envStorage;
    }

    /**
     * @param envStorage
     * @param cacheCapacity
     */
    public EnvironmentCache(final IEnvironmentStorage envStorage,
        final int cacheCapacity) {
        super(cacheCapacity);
        this.envStorage = envStorage;
    }

    public IEnvironment put(final IEnvironment ret) {
        return super.put(ret.getId(), ret);
    }

    @Override
    public IEnvironment put(final String envId, final IEnvironment env) {
        // make sure id is set consistently
        env.setId(envId);
        return super.put(envId, env);
    }

    @Override
    protected boolean removeEldestEntry(
        final Map.Entry<String, IEnvironment> eldest) {
        boolean ret = super.removeEldestEntry(eldest);
        if (ret) {
            envStorage.save(eldest.getKey(), eldest.getValue());
        }
        return ret;
    }
}
