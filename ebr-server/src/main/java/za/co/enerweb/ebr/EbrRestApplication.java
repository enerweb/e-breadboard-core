package za.co.enerweb.ebr;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import za.co.enerweb.ebr.task.TaskMetaRestService;
import za.co.enerweb.ebr.task.TaskRestService;

@ApplicationPath("/" + EbrRestApplication.URI_FRAGMENT)
public class EbrRestApplication extends Application {
    public final static String URI_FRAGMENT = "r";

    /* TODO: maybe let plugins register glasses with an annotation?
     or maybe auto register? all @Path classes?
     or even better get rid of this application class :(
     just make sure rest and vaadin still works
     */
    public Set<Class<?>> getClasses() {
        return new HashSet<Class<?>>(Arrays.asList(
            TaskRestService.class,
            TaskMetaRestService.class));
    }
}
