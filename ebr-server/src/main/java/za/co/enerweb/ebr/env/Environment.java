package za.co.enerweb.ebr.env;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.datatype.dataresource.IDataSource;
import za.co.enerweb.ebr.exceptions.ServiceException;
import za.co.enerweb.ebr.server.IdGenerator;

/**
 * A place to stare variables and data resources
 */
@Slf4j
@Data
public class Environment implements IEnvironment {
    private static final long serialVersionUID = 1L;
    private String id = IdGenerator.getUuid();
    private Map<String, EnvVarItem> variables
        = new HashMap<String, EnvVarItem>(0);
    private Map<String, IDataSource> resources
        = new HashMap<String, IDataSource>(0);

    //private boolean shouldBeSaved = true;
    //private transient boolean saved = false;

    public boolean hasVariable(final String varName) {
        // log.debug("Checking envVar: " + this.getId() + "." + varName);
        return variables.containsKey(varName);
    }

    public EnvVarItem getVariableOrNull(final String varName) {
        return variables.get(varName);
    }

    public EnvVarItem getVariable(final String varName) {
        EnvVarItem envVarItem = getVariableOrNull(varName);
        if (envVarItem == null) {
            throw new ServiceException("Unknown environment variable: "
                + varName + "\n (in env://" + id + ")");
        }
        return envVarItem;
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> T getVariableValue(final String varName) {
        return (T) getVariable(varName).getValue();
    }

    public EnvVarMetadata getVariableMetadata(final String varName) {
        return getVariable(varName).getMetadata();
    }

    public void setVariable(final EnvVarItem envItem) {
        variables.put(envItem.getMetadata().getName(), envItem);
    }

    public void setVariable(final EnvVarMetadata metadata,
        final Serializable value) {
        setVariable(new EnvVarItem(metadata, value));
    }

    public void deleteVariable(final String varName) {
        variables.remove(varName);
    }

    /**
     * @param envId
     * @param metadata
     */
    public void setVariableMetadata(final EnvVarMetadata metadata) {
        String varName = metadata.getName();
        EnvVarItem item = variables.get(varName);
        if (item == null) {
            item = new EnvVarItem();
        }
        item.setMetadata(metadata);
        variables.put(varName, item);
    }

    public void setVariableValue(final String varName,
        final Serializable value) {
        // log.debug("Saving envVar: " + this.getId() + "." + varName);
        EnvVarItem item = variables.get(varName);
        if (item == null) {
            // make up a simple description if none exists
            EnvVarMetadata metad = new EnvVarMetadata();
            metad.setName(varName);
            metad.setDescription(varName);
            if (value != null) {
                metad.setTypeClass(value.getClass());
            }
            item = new EnvVarItem(metad, value);
        } else {
            item.setValue(value);
        }
        variables.put(varName, item);
    }

    public IDataSource getResource(final String resourceId) {
        IDataSource dataSource = resources.get(resourceId);
        if (dataSource == null) {
            throw new ServiceException("Could not find resource " + resourceId
                + " in environment " + id);
        }
        return dataSource;
    }

    public Set<String> resourceNames() {
        return resources.keySet();
    }

    public Collection<IDataSource> getResources() {
        return resources.values();
    }

    public String putResource(final IDataSource dr) {
        // check if the datasource is already present, so we dont add the same
        // resource twice
//        for (Map.Entry<String, IDataSource> entry : resources.entrySet()) {
//            if (entry.getValue() == dr) {
//                return entry.getKey();
//            }
//        }
        String resourceId = dr.getResourceId();
        resources.put(resourceId, dr);
        return resourceId;
    }

    @Override
    public Environment clone() throws CloneNotSupportedException {
        Environment ret = (Environment) super.clone();
        //Environment ret = new Environment();
        // don't set id, a new one must be generated
        ret.variables.putAll(variables);
        ret.resources.putAll(resources);
        return ret;
    }

    public String variablesToString() {
        StringBuilder sb = new StringBuilder("Variables of env: " + getId()
            + "\n");
        for (Entry<String, EnvVarItem> entry : variables.entrySet()) {
            sb.append("  " + entry.getKey() + " = "
                + entry.getValue().getValue() + "\n");
        }
        return sb.toString();
    }
}
