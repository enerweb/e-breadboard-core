package za.co.enerweb.ebr.env;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.datatype.dataresource.IDataSource;
import za.co.enerweb.ebr.exceptions.ServiceException;
import za.co.enerweb.ebr.server.HomeFileCategory;
import za.co.enerweb.ebr.server.HomeFileSpec;
import za.co.enerweb.ebr.server.IHomeService;

/**
 * Manages all the environments.
 * NB. When its a running in a cluster, it will have a instance of this
 * in each node/vm.
 */
@Singleton
@DependsOn( {"HomeService"})
@Lock(LockType.READ)
@Slf4j
public class EnvironmentManager implements IEnvironmentManager {
    @EJB
    private IHomeService homeService;

    private IEnvironmentStorage envStorage;
    private EnvironmentCache envCache;

    public EnvironmentManager() {
    }

    @PostConstruct
    private void init() {
        envStorage =
            new EnvironmentFilesystemStorage(homeService);
        envCache = new EnvironmentCache(envStorage);
    }

    /**
     * mainly for unit testing
     */
    @Override
    public void setCapacity(final int cacheCapacity) {
        EnvironmentCache old = envCache;
        envCache = new EnvironmentCache(envStorage, cacheCapacity);
        envCache.putAll(old);
    }

    /**
     * for unit testing
     */
    @Lock(LockType.WRITE)
    public void clearCache() {
        envCache.clear();
    }

    @Lock(LockType.WRITE)
    public void copyVariable(final String sourceEnvId,
        final String sourceVarName,
        final String destEnvId, final String destVarName) {
        EnvVarItem variable = getVariable(sourceEnvId, sourceVarName);
        variable.setMetadata(new EnvVarMetadata(variable.getMetadata()));
        setVariable(destEnvId, variable);
    }

    @Lock(LockType.WRITE)
    public void deleteEnvironment(final String envId) {
        envCache.remove(envId);
        envStorage.deleteEnvironment(envId);
        log.info("Deleted environment: " + envId);
    }

    public IEnvironment getEnvironment(final String envId) {
        if (envCache.size() > 100) {
        log.debug("envCache.size(): " + envCache.size());
        }
        IEnvironment ret = envCache.get(envId);
        if (ret == null) {
            try {
                ret = envStorage.loadEnvironment(envId);
            } catch (ServiceException e) {
                log.error(
                    "Unknown or broken environment, re-creating it: {}\n{}"
                        , envId, e.getLocalizedMessage());
                ret = new Environment();
                ret.setId(envId);
            }
            envCache.put(ret);
        }
        return ret;
    }

    public File getEnvironmentDir(final String envId) {
        return homeService.getFile(
            new HomeFileSpec(HomeFileCategory.ENVIRONMENTS,
                envId)).getOrCreateDir();
    }

    public String getResourceAsAbsolutePath(final String envId,
        final String resourceId)
        throws IOException {
        return getEnvironment(envId).getResource(resourceId)
            .asAbsolutePath();
    }

    public byte[] getResourceAsBytes(final String envId,
        final String resourceId)
        throws IOException {
        return getEnvironment(envId).getResource(resourceId).asBytes();
    }

    @SneakyThrows
    public InputStream getResourceAsStream(final String envId,
        final String resourceId)
        throws IOException {
        // return getEnvironment(envId).getResource(resourceId).asInputStream();
        // XXX: support serialising the stream over the ejb devide, can we?
        IEnvironmentValueReader environmentValueReader =
            EbrFactory.getEnvironmentValueReader();
        return new EnvironmentInputStream(environmentValueReader, envId,
            resourceId);
    }

    public EnvResourceMetadata getResourceMetadata(final String envId,
        final String resourceId) {
        return getEnvironment(envId).getResource(resourceId)
            .getResourceMetadata();
    }

    public EnvVarItem getVariable(final String envId, final String varName) {
        return getEnvironment(envId).getVariable(varName);
    }

    public EnvVarMetadata getVariableMetadata(final String envId,
        final String varName) {
        return getVariable(envId, varName).getMetadata();
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> T getVariableValue(final String envId,
        final String varName) {
        return (T) getVariable(envId, varName).getValue();
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> T getVariableValueOrNull(final String envId,
        final String varName) {
        IEnvironment environment = getEnvironment(envId);
        EnvVarItem envVarItem = environment.getVariableOrNull(varName);
        if (envVarItem != null) {
            return (T) envVarItem.getValue();
        }
        return null;
    }

    public boolean hasVariable(final String envId, final String varName) {
        return getEnvironment(envId).hasVariable(varName);
    }

    public boolean isEnvironmentCached(final String envId) {
        return envCache.containsKey(envId);
    }

    /**
     * Make a new environment and return the Environment instance.
     * @return
     */
    @Lock(LockType.WRITE)
    public Environment newEnvironment() {
        Environment env = new Environment();
        envCache.put(env);
        return env;
    }

    @Deprecated
    public String newIEnvironmentd() {
        return newEnvironmentId();
    }

    /**
     * Make a new environment and return the id.
     * @return
     */
    @Lock(LockType.WRITE)
    public String newEnvironmentId() {
        return newEnvironment().getId();
    }

    /**
     * @param envId
     * @param dataSource
     * @return
     */
    @Lock(LockType.WRITE)
    public String putResource(final String envId,
        final IDataSource dataSource) {
        return getEnvironment(envId).putResource(dataSource);
    }

    @Lock(LockType.WRITE)
    public void saveEnvironment(final String envId) {
        IEnvironment environment = getEnvironment(envId);
        envStorage.save(envId, environment);
        // logger.debug("Saved environment.\n" +
        // environment.variablesToString());
    }

    @Lock(LockType.WRITE)
    public void setEnvironment(final IEnvironment env) {
        envCache.put(env.getId(), env);
    }

    @Lock(LockType.WRITE)
    public void setVariable(final String envId, final EnvVarItem envItem) {
        getEnvironment(envId).setVariable(envItem);
    }

    @Lock(LockType.WRITE)
    public void setVariable(final String envId, final EnvVarMetadata metadata,
        final Serializable value) {
        getEnvironment(envId).setVariable(metadata, value);
    }

    /**
     * @param envId
     * @param metadata
     */
    @Lock(LockType.WRITE)
    public void setVariableMetadata(final String envId,
        final EnvVarMetadata metadata) {
        getEnvironment(envId).setVariableMetadata(metadata);
    }

    @Lock(LockType.WRITE)
    public void setVariableValue(final String envId, final String varName,
        final Serializable value) {
        getEnvironment(envId).setVariableValue(varName, value);
    }

}
