package za.co.enerweb.ebr.home_layout;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import za.co.enerweb.ebr.server.HomeFile;
import za.co.enerweb.ebr.server.HomeFileCategory;
import za.co.enerweb.ebr.server.HomeFileSpec;
import za.co.enerweb.ebr.server.IHomeService;
import za.co.enerweb.ebr.server.SystemConfiguration;
import za.co.enerweb.ebr.task.ITaskRegistry;

import com.google.common.base.Function;
import com.google.common.collect.Iterators;

/**
 * An abstract API of the E_BREADBOARD_HOME layout.
 * This is an internal API which is very convenient for now,
 * plugins should not use it.
 * All supported directories are not guaranteed to exist and must be created
 * when needed.
 */
// FIXME: When opening the layout for writing we should lock the layout with
// FileLock so that there may not be
// concurrent users to be safe, especially while upgrading.
// If the layout is already locked it must be opened in readonly mode.
// We should expose isWritable, which should be used by the homeservice.
@Data
@Slf4j
public abstract class AbstractHomeLayout {
    private static AtomicInteger fileCounter = new AtomicInteger();

    // vvv some generic dir names
    protected static final String BIN = "bin";
    protected static final String CACHE = "cache";
    protected static final String COMMON = "common";
    protected static final String DB = "db";
    protected static final String ENV = "env";
    protected static final String ETC = "etc";
    protected static final String FRONTEND = "frontend";
    protected static final String JOBS = "jobs";
    protected static final String LIB = "lib";
    protected static final String LOGS = "logs";
    protected static final String OPT = "opt";
    protected static final String RESOURCES = "resources";
    protected static final String VAR = "var";
    protected static final String SESSIONS = "sessions";
    protected static final String SHARE = "share";
    protected static final String SRC = "src";
    protected static final String TASKS = "tasks";
    protected static final String TASK_COMMON = "task-common";
    protected static final String TASK_PERSPECTIVES = "task-perspectives";
    protected static final String TRASH = "trash";
    protected static final String TMP = "tmp";
    protected static final String WEB_CONTENT = "web-content";
    // ^^^

    static final String TASK_PERSPECTIVES_README_CONTENT =
        "Every file with a '"
            + IHomeService.TASK_PERSPECTIVE_EXTENTION
            + "' extension in this directory is "
            + "loaded as a task perspective.";

    static final String COMMON_README_CONTENT = "Every file with appropriate "
        + "extension in this directory is prepended to every task of its kind.";

    static final String TASK_README_CONTENT = "Every file with appropriate "
        + "extension in this directory is loaded as a task.";

    private ITaskRegistry taskRegistry;

    protected File root;

    protected final Map<HomeFileCategory, String[]> baseLayout;

    private boolean isTempHome = false;

    public AbstractHomeLayout(File root,
        Map<HomeFileCategory, String[]> baseLayout) {
        this.root = root;
        this.baseLayout = baseLayout;
    }

    public String[] getKnownRootDirs() {
        throw new UnsupportedOperationException();
    }

    public abstract int getVersion();

    public List<HomeFile> getFiles(HomeFileSpec homeFileSpec) {
        try {
            return Collections.singletonList(getFile(homeFileSpec));
        } catch (UnsupportedOperationException e) {
            log.debug("File not found here: " + this, e);
            return Collections.emptyList();
        }
    }

    /**
     * Override this if you need to cater for special stuff.
     * NB. This may not be supported for all categories for all layouts.
     * @param category
     * @param pathElements
     * @return null if not supported. it may not exist.
     */
    public HomeFile getFile(HomeFileSpec homeFileSpec) {
        String[] base = baseLayout.get(homeFileSpec.getCategory());
        if (base != null) {
            return getFile(HomeFileSpec.join(base),
                HomeFileSpec.join(homeFileSpec.getPathElements()));
        }

        throw new UnsupportedOperationException(String.format(
            "Obtaining the location of files with HomeContentCategory %s is "
                + "not supported for "
                + "this home layout: %s", homeFileSpec.getCategory(), this));
    }

    /**
     * @param pathElements
     * @return may be non-existing
     */
    protected final HomeFile getFile(String... pathElements) {
        String relPath = HomeFileSpec.join(pathElements);
        return new HomeFile(new File(root, relPath), relPath);
    }

    /**
     * @return null if it could not be found
     */
    public HomeFile findFile(HomeFileSpec homeFileSpec) {
        try {
            for (HomeFile hf : getFiles(homeFileSpec)) {
                if (hf.exists()) {
                    return hf;
                } else {
                    log.debug("File not found here: " + hf);
                }
            }
        } catch (UnsupportedOperationException e) {
            log.debug("File not found here: " + this, e);
        }
        return homeFileSpec.getInvalidHomeFile();
    }

    /**
     * @param homeFileSpec
     * @param extensions an array of extensions, eg. {"java","xml"}. If this
     *        parameter is empty, all files are returned.
     * @return
     */
    @SuppressWarnings("unchecked")
    public Iterator<HomeFile> findFiles(HomeFileSpec homeFileSpec,
        String... extensions) {

        if (extensions.length == 0) {
            extensions = null;
        }

        Collection<Iterator<HomeFile>> iters =
            new ArrayList<Iterator<HomeFile>>();
        for (HomeFile hf : getFiles(homeFileSpec)) {
            final File searchRoot = hf.getFile();
            final String searchRootPath =
                FilenameUtils.normalize(searchRoot.getAbsolutePath());
            final int baseLength = searchRootPath.length() + 1;
            if (searchRoot.exists() && searchRoot.isDirectory()) {
                iters.add(Iterators.transform(
                    za.co.enerweb.toolbox.io.FileUtils.listFiles(
                    searchRoot, extensions, true).iterator(),
                    new Function<File, HomeFile>() {
                        @Override
                        public HomeFile apply(final File from) {
                            // log.debug("found file: " + from);
                            return new HomeFile(from,
                                FilenameUtils.normalize(
                                    from.getAbsolutePath()).substring(
                                    baseLength)
                            // send relative path;
                            );
                        }
                    }));
            }
        }
        return IteratorUtils.chainedIterator(iters);
    }

    @SuppressWarnings("unchecked")
    public Iterator<HomeFile> list(HomeFileSpec homeFileSpec) {
        Collection<Iterator<HomeFile>> iters =
            new ArrayList<Iterator<HomeFile>>();
        for (HomeFile hf : getFiles(homeFileSpec)) {
            final File searchRoot = hf.getFile();
            final String searchRootPath =
                FilenameUtils.normalize(searchRoot.getAbsolutePath());
            final int baseLength = searchRootPath.length() + 1;
            if (searchRoot.exists() && searchRoot.isDirectory()) {
                iters.add(Iterators.transform(
                    Arrays.asList(searchRoot.listFiles()).iterator(),
                    new Function<File, HomeFile>() {
                        @Override
                        public HomeFile apply(final File from) {
                            // log.debug("found file: " + from);
                            return new HomeFile(from,
                                FilenameUtils.normalize(
                                    from.getAbsolutePath()).substring(
                                    baseLength)
                            // send relative path;
                            );
                        }
                    }));
            }
        }
        return IteratorUtils.chainedIterator(iters);
    }

    public HomeFile writeFile(HomeFileSpec homeFileSpec,
        String fileContent) throws IOException {
        HomeFile ret = getFile(homeFileSpec);
        // log.debug("Writing out file: " + ret);
        // apparently we don't need to create parent directories:
        // ret.getParentFile().mkdirs();
        FileUtils.writeStringToFile(ret.getFile(), fileContent);
        return ret;
    }

    public HomeFile writeFileIfPossible(HomeFileSpec homeFileSpec,
        String fileContent) {
        try {
            return writeFile(homeFileSpec, fileContent);
        } catch (IOException e) {
            log.warn("Could not write out file: "
                + homeFileSpec + "\n" + e.getMessage());
        }
        return homeFileSpec.getInvalidHomeFile();
    }

    /**
     * Forever delete the entire home, this should only be used for test homes
     */
    public void destroy() {
        if (isTempHome) {
            log.warn("Destroying home: " + root);
            FileUtils.deleteQuietly(root);
        }
    }

    /**
     * Just moves it to the trash
     * @param homeRelativePathElements
     * @return the File object in the trash
     * @throws IOException
     */
    public HomeFile deleteFile(
        final HomeFileSpec homeFileSpec) throws IOException {
        return deleteHomeFile(findFile(homeFileSpec));
    }

    /**
     * Move a file or directory to the trash
     * @param src
     * @return the File object in the trash (null if src didn't exist)
     * @throws IOException
     */
    public HomeFile deleteHomeFile(HomeFile src) throws IOException {
        if (src.exists()) {
            HomeFile dst = getTrashFile(src.getRelPath());
            log.debug("Moving '{}' to the trash:\n{}", src, dst);
            if (src.isDirectory()) {
                FileUtils.moveDirectory(src.getFile(), dst.getFile());
            } else {
                FileUtils.moveFile(src.getFile(), dst.getFile());
            }
            return dst;
        }
        // send an invalid file back
        return new HomeFile(null, src.toString());
    }

    /**
     * Gives you a non-existing file in the trash,
     * you can make it a directory or write stuff to it or whatever.
     */
    public HomeFile getTrashFile(String templateFileName) {
        return getTimeStampedFile(HomeFileCategory.TRASH,
            templateFileName);
    }

    /**
     * Gives you a non-existing file in the temp space,
     * you can make it a directory or write stuff to it or whatever.
     */
    public HomeFile getTempFile(String templateFileName) {
        return getTimeStampedFile(HomeFileCategory.TEMP,
            templateFileName);
    }

    /**
     * Gives you a non-existing file,
     * you can make it a directory or write stuff to it or whatever.
     */
    public HomeFile getTimeStampedFile(HomeFileCategory category,
        String templateFileName) {
        return getTimeStampedFile(new HomeFileSpec(category), templateFileName);
    }

    public HomeFile getTimeStampedFile(HomeFileSpec homeFileSpec,
        String templateFileName) {
        // yyyymmddhhmmss#_filename

        String filename = String.format("%1$tY%1$tm%1$td%1$tH%1$tM%tS%s_%s",
            new Date(), fileCounter.incrementAndGet(),
            templateFileName.replaceAll("[^\\w\\.]", "_"));

        HomeFile ret = getFile(homeFileSpec.subDir(filename));
        ret.makeParentDirs();
        return ret;
    }

    public String toString() {
        return "version=" + getVersion() + " root=" + root;
    }

    /**
     * Write out the resources in the background because it should not delay
     * the system from starting up.
     * @param taskParser
     */
    public void updateReadmeFiles() {
        // don't write it out into a test home
        if (!SystemConfiguration.isUseTestHome()) {
            // FIXME: run in background
            // EbrServer.getBackgroundTaskRunner().invokeLater(
            // new Task() {
            // @Override
            // public Object run() throws Exception {
                        writeTaskPerspectivesReadmeFile();
                        writeTaskReadmeFiles();
            // return null;
            // }
            // });
        }
    }

    /*
     * Among others, this ensures the directories the users may care about is
     * also created.
     */
    public void writeTaskPerspectivesReadmeFile() {
        // FIXME: don't write out this crap all the time
        // maybe only write it out on upgrade, creation and every 100 days.
        HomeFileSpec dest = new HomeFileSpec(
            HomeFileCategory.TASK_PERSPECTIVES, IHomeService.README);
        writeFileIfPossible(dest,
            TASK_PERSPECTIVES_README_CONTENT);
    }

    /**
     * writes out resources from za/co/enerweb/ebr/name/examples/*
     * za/co/enerweb/ebr/name/tasks/README.txt to our home
     */
    public void writeTaskReadmeFiles() {
        try {
            List<HomeFile> commonDirs = getFiles(
                new HomeFileSpec(HomeFileCategory.TASK_COMMON));
            for (HomeFile homeFile : commonDirs) {
                FileUtils.writeStringToFile(new File(homeFile.getOrCreateDir(),
                    IHomeService.README),
                    COMMON_README_CONTENT);
            }
            List<HomeFile> taskDirs = getFiles(
                new HomeFileSpec(HomeFileCategory.TASKS));
            for (HomeFile homeFile : taskDirs) {
                FileUtils.writeStringToFile(new File(homeFile.getOrCreateDir(),
                    IHomeService.README), TASK_README_CONTENT);
            }
        } catch (IOException e) {
            log.warn("Could not write task readme files."
                + e.getMessage());
        }
    }

    protected static String join(String... pathElements) {
        return HomeFileSpec.join(pathElements);
    }

    protected static String join(Iterable<?> pathElements) {
        return HomeFileSpec.join(pathElements);
    }

}
