package za.co.enerweb.ebr.server;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.joda.time.DateTime;

import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.home_layout.AbstractHomeLayout;
import za.co.enerweb.ebr.home_layout.HomeLayoutManager;
import za.co.enerweb.ebr.task.ITaskRegistry;
import za.co.enerweb.ebr.util.ScheduleExpressionUtil;
import za.co.enerweb.toolbox.collections.OneTimeIteratable;
import za.co.enerweb.toolbox.io.ResourceUtils;
import za.co.enerweb.toolbox.io.TempDir;

@Singleton
@Startup
@DependsOn({"Executor"})
@Lock(LockType.READ)
@Slf4j
public class HomeService implements IHomeService {
    private static final String HOME_IN_USER_HOME = ".e-breadboard";
    protected static final String ENV_VAR__E_BREADBOARD_HOME
        = "E_BREADBOARD_HOME";
    protected static final String PROPERTY__E_BREADBOARD_HOME =
        "e-breadboard.home";
    protected static final String ENV_VAR__EBR_HOME = "EBR_HOME";
    protected static final String TEMP_HOME_DIR_NAME = "e-breadboard";
    private boolean testing = SystemConfiguration.isUseTestHome();

    @EJB
    private ITaskRegistry taskRegistry;
    @EJB
    private IExecutor executor;
    @Resource
    private TimerService timerService;

    private HomeLayoutManager homeLayoutManager;

    // this should always be writable and available for e-breadboard to work
    private AbstractHomeLayout mainHome;
    private List<AbstractHomeLayout> stackedHomes =
        new ArrayList<AbstractHomeLayout>();

    @Override
    public void addHome(File dir) {
        addHome(dir, null);
    }

    /*
     * creates the dir for the user if it does not exist
     */
    AbstractHomeLayout addHome(File dir, String namespace) {
        try {
            dir.mkdirs();
            if (dir.exists()) {
                // can maintain a set for performance sake if needed...
                // ...test first!
                for (AbstractHomeLayout sh : stackedHomes) {
                    if (sh.getRoot().equals(dir)) {
                        log.debug("Could not add E_BREADBOARD_HOME because "
                            + "it has already been added: "
                            + dir.getAbsolutePath());
                        return null;
                    }
                }
                AbstractHomeLayout newHome = homeLayoutManager.openEbrHome(dir);
                stackedHomes.add(newHome);
                log.info("added home:" + dir);
                if (doesntHaveWritableMainHome()) {
                    mainHome = newHome;
                }
                return newHome;
            } else {
                log.info("Could not add nonexisting E_BREADBOARD_HOME: "
                    + dir.getAbsolutePath());
                return null;
            }
        } catch (Exception e) {
            log.warn("Could not add E_BREADBOARD_HOME: "
                + dir.getAbsolutePath(), e);
            return null;
        }
    }

    // TODO: addNestedHome

    public HomeFile getFile(HomeFileSpec homeFileSpec) {
        // first try to find an existing file anywhere
        HomeFile ret = null;
        for (AbstractHomeLayout layout : stackedHomes) {
            try {
                HomeFile file = layout.getFile(homeFileSpec);
                if (file.exists()) {
                    return file;
                }
                if (ret == null) {
                    ret = file;
                }
            } catch (UnsupportedOperationException e) {
                log.warn("Could not find file in this home: ", e);
            }
        }
        if (ret != null) {
            return ret;
        }
        return homeFileSpec.getInvalidHomeFile();
    }

    public HomeFile findFile(HomeFileSpec homeFileSpec) {
        for (AbstractHomeLayout layout : stackedHomes) {
            HomeFile file = layout.findFile(homeFileSpec);
            if (file.exists()){
                return file;
            }
        }
        return homeFileSpec.getInvalidHomeFile();
    }

    @SuppressWarnings("unchecked")
    @Override
    public Iterable<HomeFile> findFiles(HomeFileSpec homeFileSpec,
        final String... extensions) {
        final Collection<Iterator<HomeFile>> iterators
            = new ArrayList<Iterator<HomeFile>>();
        for (AbstractHomeLayout layout : stackedHomes) {
            iterators.add(layout.findFiles(homeFileSpec,
                extensions));
        }
        final Iterator<HomeFile> chainedIterator
                = IteratorUtils.chainedIterator(iterators);
        return new OneTimeIteratable<HomeFile>(
            chainedIterator);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Iterable<HomeFile> list(HomeFileSpec homeFileSpec) {
        final Collection<Iterator<HomeFile>> iterators =
            new ArrayList<Iterator<HomeFile>>();
        for (AbstractHomeLayout layout : stackedHomes) {
            iterators.add(layout.list(homeFileSpec));
        }
        final Iterator<HomeFile> chainedIterator =
            IteratorUtils.chainedIterator(iterators);
        return new OneTimeIteratable<HomeFile>(
            chainedIterator);
    }

    public void writeFile(HomeFileSpec homeFileSpec,
        String fileContent) throws IOException {
        mainHome.writeFile(homeFileSpec, fileContent);
    }

    @Override
    public void writeFileFromClasspath(HomeFileSpec homeFileSpec,
        String resoucePath) throws IOException {
        log.debug("Writing out " + resoucePath + " to " + homeFileSpec);
        String fileName = FilenameUtils.getName(resoucePath);
        HomeFileSpec dest = homeFileSpec.subDir(fileName);
        mainHome.writeFile(dest,
            ResourceUtils.getResourceAsString(resoucePath));
    }

    @Override
    public HomeFile deleteFile(
        final HomeFileSpec homeFileSpec) throws IOException {
        return mainHome.deleteFile(homeFileSpec);
    }

    public HomeFile deleteFile(HomeFile homeFile) throws IOException {
        return mainHome.deleteHomeFile(homeFile);
    }

    @Override
    public HomeFile getTempFile(String templateFileName) {
        return mainHome.getTempFile(templateFileName);
    }

    public HomeFile getTrashFile(String templateFileName) {
        return mainHome.getTrashFile(templateFileName);
    }

    @PostConstruct
    public synchronized void open() {
        homeLayoutManager = new HomeLayoutManager(taskRegistry);

        // make sure we start with a clean slate
        if (mainHome != null) {
            // already open
            return;
        }
        stackedHomes.clear();
        if (testing) {
           // 0) Tests must always get a test home
            addHome(TempDir.createGroupedTempDir("ebr-test",
                TEMP_HOME_DIR_NAME, true), null).setTempHome(true);
            // log.debug("Created test home: " + mainHome);
        } else {
            // 1a) try -De-breadboard
            String homePath = System.getProperty(PROPERTY__E_BREADBOARD_HOME);
            if (homePath != null) {
                addHome(new File(homePath), null);
            }

            // 1b) try $E_BREADBOARD_HOME
            homePath = System.getenv(ENV_VAR__E_BREADBOARD_HOME);
            if (homePath != null) {
                addHome(new File(homePath), null);
            }

            // 1c) try $EBR_HOME
            String ebrHomePath = System.getenv(ENV_VAR__EBR_HOME);
            if (ebrHomePath != null) {
                log.warn("The environment variable $" + ENV_VAR__EBR_HOME
                    + " has been deprecated.\n"
                    + "Rather use $" + ENV_VAR__E_BREADBOARD_HOME);
                addHome(new File(ebrHomePath), null);
            }
        }
        if (doesntHaveWritableMainHome()) {
            // 2) try user home if $EBR_HOME is undefined
            // 3) try user home if main home is readonly
            tryUsingUserHome();
        }
        if (doesntHaveWritableMainHome()) {
            // 4) fall back to a temp dir if main home is still readonly
            if (mainHome != null) {
                log.info("E_BREADBOARD_HOME '{}' is not writable.\n"
                    + "Will use a temp directory for the "
                    + "main E_BREADBOARD_HOME.", mainHome.getRoot());
            }
            addHome(
                TempDir.createDirInTempDir(TEMP_HOME_DIR_NAME), null);
        }
        log.info("Main E_BREADBOARD_HOME: " + mainHome);

        initCleanups();
    }

    protected void initCleanups() {
        try {
            if (executor != null) { // only scheadule if running as ejb

                // do initial cleanups on startup, mainly for desktop use..
                executor.submit(new Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        // here to make sure we started already
                        EbrFactory.getHomeService().setUpCleanupScheadules();
                        return null;
                    }

                });
            }
        } catch (Exception e) {
            log.warn("could not scheadule initial cleanups", e);
        }
    }

    public void setUpCleanupScheadules() {
        // for in case..
        cancelAllCleanupTimers();

        // [core/cleanup]
        // ; m h dom mon dow
        // ; 0 5 * * 1
        // trash.schedule=0 0 * * *
        // temp.schedule=0 0 * * *
        // env.schedule=0 0 * * *
        //
        // ; week=10080,day=1440
        // trash.ageMinutes=2880
        // temp.ageMinutes=2880
        // env.ageMinutes=2880

        // ConfigService depends on HomeService, so don't inject this:
        ConfSection cleanupSection =
            EbrFactory.getConfigurationService().getCoreCleanupSection();

        for (CleanupTimer t : CleanupTimer.values()) {

            String sectionPrefix = t.name()
                .toLowerCase();
            int ageMinutes = cleanupSection.get(sectionPrefix
                + ".ageMinutes", Integer.class, 2880);

            // clean on startup..
            cleanDir(t.getHomeFileSpec(),
                new DateTime().minusMinutes(ageMinutes).getMillis(),
                t.isUseAccessTime());

            final TimerConfig conf =
                new TimerConfig(new TimerConf(t, ageMinutes), false);
            String schedule = cleanupSection.get(sectionPrefix + ".schedule",
                "0 0 * * *");
            log.debug("scheaduling cleanup " + t + " " + schedule
                + " ageMinutes:" + ageMinutes);
            timerService.createCalendarTimer(ScheduleExpressionUtil.parse(
                schedule)
                , conf);
        }
    }

    @Timeout
    public void timeout(Timer timer) {
        if (timer.getInfo() instanceof TimerConf) {
            TimerConf tc = (TimerConf) timer.getInfo();
                cleanDir(tc.cleanupTimer.getHomeFileSpec(),
                    new DateTime().minusMinutes(tc.ageMinutes).getMillis(),
                tc.cleanupTimer.isUseAccessTime());

        }
    }

    private void tryUsingUserHome() {
        if (mainHome != null) {
            log.info("E_BREADBOARD_HOME '{}' is not writable.\nWill use a "
                + "directory for the main E_BREADBOARD_HOME in the "
                + "user's home.",
                mainHome.getRoot());
        }
        File homeDir = getHomePathUserHome(HOME_IN_USER_HOME);
        moveHome(".ebr", homeDir);
        moveHome(".ebreadboard", homeDir);
        mainHome = addHome(homeDir, null);
    }

    private void moveHome(String oldHomeName, File newHome) {
        String newHomeName = newHome.getName();
        File oldHome = getHomePathUserHome(oldHomeName);
        if (oldHome.exists()) {
            String userHome = System.getProperty("user.home");
            if (newHome.exists()) {
                log.warn(
                    String.format("Not using %1$s/%2$s\nbecause "
                        + "%1$s/%3$s exists.", userHome, oldHomeName,
                        newHomeName));
            } else {
                log.info(
                    String.format("Renaming %1$s/%2$s to\n"
                        + "%1$s/%3$s", userHome, oldHomeName,
                        newHomeName));
                try {
                    FileUtils.moveDirectory(oldHome, newHome);
                } catch (IOException e) {
                    log.warn(
                        String.format("Could not rename %1$s/%2$s to "
                            + "%1$s/%3$s", userHome, oldHomeName,
                            newHomeName), e);
                }
            }
        }
    }

    private boolean doesntHaveWritableMainHome() {
        return mainHome == null || !mainHome.getRoot().canWrite();
    }

    private File getHomePathUserHome(String dirname) {
        return new File(System.getProperty("user.home") + File.separator
            + dirname);
    }

    protected void setTesting(boolean testing) {
        this.testing = testing;
    }

    @PreDestroy
    public void close() {
        // for in case..
        cancelAllCleanupTimers();

        // delete temp homes
        if (testing) {
            // I suppose we can check if it is actually a temp home.
            for (AbstractHomeLayout home : stackedHomes) {
                home.destroy();
            }
        }
        mainHome = null;
        stackedHomes.clear();
    }

    protected void cancelAllCleanupTimers() {
        if (timerService != null && timerService.getTimers() != null) {
            for (Timer t : timerService.getTimers()) {
                t.cancel();
            }
        }
    }

    @Override
    public File getMainHomeDir() {
        return mainHome.getRoot();
    }

    @Override
    public List<File> getAllHomeDirs() {
        List<File> ret = new ArrayList<File>(stackedHomes.size());
        for (AbstractHomeLayout home : stackedHomes) {
            ret.add(home.getRoot());
        }
        return ret;
    }

    void cleanDir(HomeFileSpec hfs, long olderThan,
        boolean useAccessTime) {
        try {
            log.debug("EMPTYING " + hfs + "!!! < " + olderThan);
            for (HomeFile homeFile : list(hfs)) {
                long fileTime = getFileTime(homeFile, useAccessTime);
                // log.debug(" * " + homeFile + " fileTime: " + fileTime);
                if (fileTime < olderThan) {
                    log.debug(" * deleting: " + homeFile);
                    homeFile.delete();
                }
            }
        } catch (Exception e) {
            log.info("Could not clean up dir task: " + hfs, e);
        }
    }

    private long getFileTime(HomeFile homeFile, boolean useAccessTime) {
        File file = homeFile.getFile();
        if (useAccessTime) {
            FileTime lastAccessTime;
            try {
                lastAccessTime =
                    (FileTime) Files.getAttribute(file.toPath(),
                        "lastAccessTime");
                return lastAccessTime.toMillis();
            } catch (IOException e) {
                log.debug("Could not read accessTime for file : " + file, e);
            }
        }
        return file.lastModified();
    }

    @Getter
    @AllArgsConstructor
    private static enum CleanupTimer {
        TRASH(new HomeFileSpec(HomeFileCategory.TRASH), false),
        TEMP(new HomeFileSpec(HomeFileCategory.TEMP), false),
        ENV(new HomeFileSpec(HomeFileCategory.ENVIRONMENTS), false);
        private HomeFileSpec homeFileSpec;
        // the files are accessed often so doesn't make sense to use
        // this :(
        private boolean useAccessTime;
    };

    @AllArgsConstructor
    @ToString
    private static class TimerConf implements Serializable {
        private CleanupTimer cleanupTimer;
        private int ageMinutes;
    }
}
