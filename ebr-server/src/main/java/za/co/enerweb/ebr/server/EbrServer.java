package za.co.enerweb.ebr.server;

import static java.lang.String.format;
import static za.co.enerweb.ebr.server.HomeFileCategory.LOGS;
import static za.co.enerweb.ebr.server.LogFileSpecUtil.convert;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.val;
import lombok.extern.slf4j.Slf4j;

import org.apache.log4j.LogManager;
import org.apache.log4j.RollingFileAppender;

import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.annotations.OnEbrReload;
import za.co.enerweb.ebr.annotations.OnEbrStart;
import za.co.enerweb.ebr.datatype.IDataTypeRegistryService;
import za.co.enerweb.ebr.env.EnvVarItem;
import za.co.enerweb.ebr.env.EnvVarMetadata;
import za.co.enerweb.ebr.env.IEnvironmentManager;
import za.co.enerweb.ebr.renderer.EnvVarRendererRegistryService;
import za.co.enerweb.ebr.task.ISessionManager;
import za.co.enerweb.ebr.task.ITaskRegistry;
import za.co.enerweb.ebr.task_layout.ITaskLayoutRegistry;
import za.co.enerweb.toolbox.io.ResourceUtils;

/**
 * Session Bean implementation class IEbrServer
 */
// FIXME: (re)move environment operations
@Singleton
@Startup
@DependsOn({"DataTypeRegistryService", "EnvironmentManager",
    "HomeService", "Executor", "SessionManager"})
@Lock(LockType.READ)
@Slf4j
public class EbrServer implements IEbrServer {

    @EJB
    private IExecutor executor;
    // alternatively
    // @Resource
    // private ManagedExecutorService executor;

    @EJB
    private IEnvironmentManager envManager;

    @EJB
    private IHomeService homeService;

    @EJB
    private IDataTypeRegistryService dataTypeRegistryService;

    @EJB
    private ITaskRegistry taskRegistry;

    @EJB
    private ITaskLayoutRegistry taskLayoutRegistry;

    @EJB
    private ISessionManager sessionManager;

    private boolean started = false;

    @PostConstruct
    private void init() {
        start();
    }

    // either plugins that need this internal db, should use the
    // db-manager-plugin
    // or make the db-manager part of the core,
    // then we wont't need this crazyness
    // @OnBeforeEbrStart
    // public static void setupInitialContext() {
    // // This is a special case for not using HomeService as an ejb:
    // // we need to set up the context before we can load ejb's but we
    // // need to detect the main home before that.
    // // Unfortunately this means the home is detected twice.
    // // According to timings in HomeServiceOpenTest it seems this only takes
    // // Avg=9.2ms, Total=46.0, Min=1.0, Max=28.0ms
    // // So I'm not gonna worry about that.
    // HomeService hs = new HomeService();
    // try {
    // hs.open();
    // log.debug("Instantiated non-ejb HomeService: "
    // + hs.getMainHomeDir());
    //
    // // add the jdbc url for our internal database
    // String dbPath = hs.getFile(new HomeFileSpec(
    // HomeFileCategory.DB, "h2", "ebr"))
    // .getAbsolutePath();
    // // TODO:let the user explicitly specify this so we can easily switch
    // // between memory and tempdir for testing.
    // // Default to jdbc:log4jdbc:h2:file:${EBR_HOME}/db/h2/ebr
    // String jdbcUrl = "jdbc:log4jdbc:h2:file:" + dbPath;
    // if (SystemConfiguration.isUseTestHome()) {
    // // jdbcUrl = "jdbc:log4jdbc:h2:mem:test";
    //
    // // for debugging
    // jdbcUrl = "jdbc:log4jdbc:h2:file:" +
    // TempDir.createTempDir("ebr-test-h2", false, false) + "/ebr";
    // }
    // jdbcUrl = jdbcUrl + ";AUTO_SERVER=TRUE"; // allow multiple processes
    // if (log.isDebugEnabled()) {
    // // give a usable url
    // log.debug("jdbcUrl: " +
    // StringUtils.replace(jdbcUrl, ":log4jdbc:", ":"));
    // }
    // EbrFactory.addInitialContextProperty(
    // "jdbc/ebr.JdbcUrl", jdbcUrl);
    //
    // // XXX: add a hook so that each domain can check its db structure
    // // version in ebrHome/db/conf.ini eg. pms=v1
    // // then we can run db update scripts to for example remove columns
    // // it will possibly need to run multiple updates eg.
    // // v1_TO_v2.sql and v2_TO_v3.sql
    // } finally {
    // if (hs != null) {
    // hs.close();
    // }
    // }
    // }

    @AllArgsConstructor
    private static class AnnotatedMethod implements Comparable<AnnotatedMethod> {
        Method method;
        boolean synchronous;
        int rank;

        @Override
        public int compareTo(AnnotatedMethod o) {
            return rank - o.rank;
        }
    }

    private void fireOnReload() {
        // find all classes annotated by @OnEbrReload
        Set<Method> annotated =
            Registrar.getReflections().getMethodsAnnotatedWith(
                OnEbrReload.class);
        // if (annotated.size() == 0) {
        // log.debug("Could not find any @OnEbrReload annotations.");
        // } else {
        // log.debug("Found " + annotated.size()
        // + " @OnEbrReload annotations");
        // }
        log.debug("Processing all @OnEbrReload annotations");
        val methods = new ArrayList<AnnotatedMethod>();
        for (final Method annotatedMethod : annotated) {
            if (!Modifier.isStatic(annotatedMethod.getModifiers())) {
                log.warn("@OnEbrReload annotation found on "
                    + "a non-static method:\n" + annotatedMethod);
                continue;
            }
            if (annotatedMethod.getParameterTypes().length != 0) {
                log.warn("@OnEbrReload annotation found on "
                    + "a method that takes parameters:\n" + annotatedMethod);
                continue;
            }
            methods.add(new AnnotatedMethod(annotatedMethod,
                annotatedMethod.getAnnotation(OnEbrReload.class).synchronous(),
                annotatedMethod.getAnnotation(OnEbrReload.class).rank()));
        }
        invokeAnnotatedMethods(methods);
    }

    private void fireOnStart() {
        Set<Method> annotated =
            Registrar.getReflections().getMethodsAnnotatedWith(
                OnEbrStart.class);
        log.debug("Processing all @OnEbrStart annotations");
        val methods = new ArrayList<AnnotatedMethod>();
        for (Method annotatedMethod : annotated) {
            if (!Modifier.isStatic(annotatedMethod.getModifiers())) {
                log.warn("@OnEbrStart annotation found on "
                    + "a non-static method:\n" + annotatedMethod);
                continue;
            }
            if (annotatedMethod.getParameterTypes().length != 0) {
                log.warn("@OnEbrStart annotation found on "
                    + "a method that takes parameters:\n" + annotatedMethod);
                continue;
            }

            methods.add(new AnnotatedMethod(annotatedMethod,
                annotatedMethod.getAnnotation(OnEbrStart.class).synchronous(),
                annotatedMethod.getAnnotation(OnEbrStart.class).rank()));
        }
        invokeAnnotatedMethods(methods);
    }

    /*
     * (non-Javadoc)
     * @see za.co.enerweb.ebr.api.OrchestrationEngineI#reload()
     */
    @Override
    // if something goes wrong here don't roll back the transactions of other
    // beans
        @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
        public synchronized
        void reload() {
        if (!started) {
            // done here so it at least happens before the on reloads
            fireOnStart();
        }
        fireOnReload();
        dataTypeRegistryService.refreshRegistry();
        EnvVarRendererRegistryService.refreshRegistry();
        reloadTasks();
    }

    public synchronized void reloadTasks() {
        // XXX: maybe cache all the tasks in the db and refresh in the backround
        // (if we can show that this is slow)
        taskRegistry.refreshRegistry();
    }

    // if something goes wrong here don't roll back the transactions of other
    // beans
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public synchronized void reloadAll() {
        envManager.clearCache();
        taskLayoutRegistry.clearRegistry();
        // SessionManagerFactory.reset();
        reload();
    }

    @SneakyThrows
    private synchronized void start() {

        if (!started) {
            // // Let anything that tries to log to java commons logging rather
            // // log to slf4j, and set it once at startup.
            // log
            // .debug(" ***** EbrServer SLF4JBridgeHandler.install()"
            // );
            // SLF4JBridgeHandler.install();

            started = true;
            // make sure we are properly started before firing any events!
            executor.submit(new Callable<Object>() {
                @Override
                public Object call() throws Exception {
                    EbrFactory.open(); // make sure the Factory think it's
                                       // open before firing events!

                    fireOnStart();
                    reload();
                    runStartupScripts();

                    String asciiart = "";
                    try {
                        asciiart = "\n" + new String(
                            ResourceUtils.getResourceAsBytes("ebr-ascii.txt"));
                    } catch (IOException e) {
                        log.debug("Could not load ascii logo.", e);
                    }

                    log.info("{}\n   #   {} started up SUCCESSFULLY   #\n\n",
                        asciiart, SystemConfiguration.getBrandNameAndVersion());

                    return null;
                }
            });
        }
    }

    private void runStartupScripts() {
        for (String script : EbrFactory.getConfigurationService()
            .getCoreSection().getAll("scriptToRunOnStartup")) {
            log.debug("Running " + script);
            try {
                Runtime.getRuntime().exec(script);
            } catch (IOException e) {
                log
                    .error("Something went wrong while running: "
                        + script, e);
            }
        }
    }

    private void invokeAnnotatedMethods(
        final List<AnnotatedMethod> annotatedMethods) {
        Collections.sort(annotatedMethods);
        for (AnnotatedMethod annotatedMethod : annotatedMethods) {
            invokeAnnotatedMethod(annotatedMethod);
        }
    }

    private void invokeAnnotatedMethod(final AnnotatedMethod annotatedMethod) {
        final Method method = annotatedMethod.method;
        try {
            if (!annotatedMethod.synchronous) {
                // log.debug("** submitting " + annotatedMethod);
                executor.submit(new Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        log.debug("** invoking ASYNC: " + method);
                        try {
                            method.invoke(null);
                        } catch (Exception e) {
                            log.warn(format("Could not execute task %s.%s()",
                                method.getDeclaringClass(),
                                method.getName()), e);
                        }
                        return null;
                    }
                });
            } else {
                log.debug("** invoking SYNC: " + method);
                method.invoke(null);
            }
        } catch (Throwable e) {
            log.error("Could not invoke method:\n"
                + method, e);
        }
    }

    @PreDestroy
    public synchronized void stop() {
        EbrFactory.setOrchestrationEngineClosing();
        if (started) {
            started = false;
            sessionManager.saveSessions();
        }
        EbrFactory.setOrchestrationEngineDestroyed();
    }

    public void deleteEnvironment(final String envId) {
        envManager.deleteEnvironment(envId);
    }

    public byte[] getEnvResourceAsBytes(final String envId,
        final String resourceName) throws IOException {
        return envManager.getResourceAsBytes(envId, resourceName);
    }

    public EnvVarItem getEnvVar(final String envId, final String varName) {
        return envManager.getVariable(envId, varName);
    }

    public EnvVarMetadata getEnvVarMetadata(final String envId,
        final String varName) {
        return envManager.getVariable(envId, varName).getMetadata();
    }

    public Serializable getEnvVarValue(final String envId,
        final String varName) {
        return envManager.getVariableValue(envId, varName);
    }

    public String getNewGuid() {
        return IdGenerator.getUuid();
    }

    public String newEnvironment() {
        return envManager.newEnvironmentId();
    }

    public void saveEnvironment(final String envId) {
        envManager.saveEnvironment(envId);
    }

    public void setEnvVar(final String envId, final EnvVarItem envItem) {
        envManager.setVariable(envId, envItem);
    }

    public void setEnvVarMetadata(final String envId,
        final EnvVarMetadata metadata) {
        envManager.setVariableMetadata(envId, metadata);
    }

    /**
     * Reuses previous metadata if available.
     * @param envId
     * @param varName
     * @param value
     */
    public void setEnvVarValue(final String envId, final String varName,
        final Serializable value) {
        envManager.setVariableValue(envId, varName, value);
    }

    /**
     * Should return true iff this is running in a development environment.
     * Can be used to check weather development menu items should be added.
     */
    public boolean isInDevMode() {
        // check if we can load a test resource
        try {
            return ResourceUtils.getResourceAsFile(
                "za/co/enerweb/ebr/mock_task/example_tasks/example.mt")
                .exists();
        } catch (Exception e) {
        }
        return false;
    }

    /**
     * Currently assumes and only works with Log4j
     */
    public HomeFile addLogFile(LogFileSpec lfs)
        throws IOException {
        HomeFileSpec homeFileSpec = new HomeFileSpec(
            LOGS, lfs.name() + ".log");
        HomeFile homeFile = homeService.getFile(homeFileSpec);
        homeFile.makeParentDirs();

        RollingFileAppender a = new RollingFileAppender(
            convert(lfs.layout()),
            homeFile.getAbsolutePath(),
            false);
        a.setMaximumFileSize(lfs.maximumFileSize());
        a.setMaxBackupIndex(lfs.numberOfBackupFiles());
        a.setThreshold(convert(lfs.level()));
        // a.activateOptions()

        if (lfs.loggerNames() == null || lfs.loggerNames().isEmpty()) {
            LogManager.getRootLogger().addAppender(a);
        } else {
            for (String loggerName : lfs.loggerNames()) {
                if (loggerName == null) {
                    LogManager.getRootLogger().addAppender(a);
                } else {
                    LogManager.getLogger(loggerName).addAppender(a);
                }
            }
        }
        return homeFile;
    }
}
