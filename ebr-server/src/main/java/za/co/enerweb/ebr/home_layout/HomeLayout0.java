package za.co.enerweb.ebr.home_layout;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import za.co.enerweb.ebr.server.HomeFile;
import za.co.enerweb.ebr.server.HomeFileCategory;
import za.co.enerweb.ebr.server.HomeFileSpec;
import za.co.enerweb.ebr.task.ATaskParser;

public class HomeLayout0 extends AbstractHomeLayout {
    public static final String TASK_LAYOUTS = "task-layouts";
    public static final String JAVASCRIPT = "javascript";
    public static final String STYLESHEETS = "stylesheets";
    public static final String LAYOUTS = "layouts";
    public static final String VAADIN = "vaadin";
    public static final String PMS_PATH_ALIASES_TXT = "pms_path_aliases.txt";

    // the old layout used an underscore yuck
    protected static final String TASK_PERSPECTIVES = "task_perspectives";

    private static final Map<HomeFileCategory, String[]> baseLayout =
        new EnumMap<HomeFileCategory, String[]>(HomeFileCategory.class);
    static {
        baseLayout.put(HomeFileCategory.ENVIRONMENTS, new String[] {VAR, ENV});
        baseLayout.put(HomeFileCategory.ETC, new String[] {ETC});
        baseLayout.put(HomeFileCategory.JOBS, new String[] {VAR, JOBS});
        baseLayout.put(HomeFileCategory.LOGS, new String[] {LOGS});
        baseLayout.put(HomeFileCategory.RESOURCES, new String[] {RESOURCES});
        //baseLayout.put(HomeFileCategory.ROOT, new String[] { });
        baseLayout.put(HomeFileCategory.SESSIONS,
            new String[] {VAR, SESSIONS});
        baseLayout.put(HomeFileCategory.TASK_PERSPECTIVES,
            new String[] {TASK_PERSPECTIVES});
        // we didn't support this previously, but I don't see harm in adding it
        baseLayout.put(HomeFileCategory.TEMP, new String[] {TMP});
        baseLayout.put(HomeFileCategory.TRASH, new String[] {TRASH});
        baseLayout.put(HomeFileCategory.WEB_CONTENT,
            new String[] {VAR, WEB_CONTENT});
    }

    protected HomeLayout0(File root,
        Map<HomeFileCategory,
            String[]> baseLayout) {
        super(root, baseLayout);
    }

    public HomeLayout0(File home) {
        super(home, baseLayout);
    }

    public HomeLayout0() {
        super(null, baseLayout);
    }

    @Override
    public int getVersion() {
        return 0;
    }

    public List<HomeFile> getFiles(HomeFileSpec hfs) {
        switch (hfs.getCategory()) {
        case TASKS:
            return findParserTechDirs(hfs, TASKS);
        case TASK_COMMON:
            return findParserTechDirs(hfs, TASK_COMMON);
        case RESOURCES:
            List<HomeFile> resourcesDirs =
                findParserTechDirs(hfs, RESOURCES);
            List<String> noParameters = Collections.emptyList();
            resourcesDirs.add(super.getFile(
                new HomeFileSpec(HomeFileCategory.RESOURCES,
                    noParameters, hfs.getPathElements())));
            return resourcesDirs;
        }
        return super.getFiles(hfs);
    }

    public HomeFile getFile(HomeFileSpec hfs) {
        switch (hfs.getCategory()) {
        case CACHE_PER_TECHNOLOGY:
            String cacheTech = hfs.getParameters().get(0);
            // organise the new stuff right from the start..
            if (cacheTech.equals("lucene")) {
                return getFile(CACHE, cacheTech,
                    join(hfs.getPathElements()));
            }
            return getFile(cacheTech, CACHE,
                join(hfs.getPathElements()));
        case DB:
            return getFile(VAR, DB, hfs.getParameters().get(0),
                join(hfs.getPathElements()));
        case FRONTEND:
            String frontendTech = hfs.getParameters().get(0);
            if (frontendTech.equals(VAADIN)) {
                String subtech = hfs.getParameters().get(1);
                // supporting this for backwards compatibility:
                if (subtech.equals(TASK_LAYOUTS)) {
                    // vaadin/layouts <= task-layouts
                    return getFile(frontendTech, LAYOUTS,
                        join(hfs.getPathElements()));
                } else if (subtech.equals(STYLESHEETS)) {
                    // vaadin/stylesheets
                    return getFile(frontendTech, STYLESHEETS,
                        join(hfs.getPathElements()));
                } else if (subtech.equals(JAVASCRIPT)) {
                    // lib/javascript
                    return getFile(LIB, JAVASCRIPT,
                        join(hfs.getPathElements()));
                }
                return getFile(SHARE, FRONTEND, hfs.getParameters().get(0),
                    subtech,
                    join(hfs.getPathElements()));
            }
        case LIB_PER_TECHNOLOGY:
            return getFile(hfs.getParameters().get(0), LIB,
                join(hfs.getPathElements()));
        case RESOURCES_PER_TECHNOLOGY:
            return getFile(hfs.getParameters().get(0), RESOURCES,
                join(hfs.getPathElements()));
        case TASKS_PER_TECHNOLOGY:
            return getFile(hfs.getParameters().get(0), TASKS,
                join(hfs.getPathElements()));
        case TASK_COMMON_PER_TECHNOLOGY:
            return getFile(hfs.getParameters().get(0), COMMON,
                join(hfs.getPathElements()));
        case VAR_PER_COMPONENT:
            return getFile(VAR, hfs.getParameters().get(0),
                join(hfs.getPathElements()));
        case ETC:
            // support this for backwards compatibility:
            if (!hfs.getPathElements().isEmpty() &&
                hfs.getPathElements().get(0).equals(PMS_PATH_ALIASES_TXT)) {
                return getFile("pms", "path_aliases.txt");
            }
        }
        return super.getFile(hfs);
    }

    private List<HomeFile> findParserTechDirs(HomeFileSpec hfs,
            String subdirName) {
        // check in all the task dirs
        List<HomeFile> ret = new ArrayList<HomeFile>();
        for (ATaskParser taskParser : getTaskRegistry()
            .getAllTaskParsers()) {
            String parserName = taskParser.getName();
            HomeFile parserDir = getFile(parserName);
            ret.add(getFile(parserDir.getName(),
                    subdirName,
                    join(hfs.getPathElements())));
        }
        return ret;
    }
}
