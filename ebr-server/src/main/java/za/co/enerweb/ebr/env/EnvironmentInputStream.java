package za.co.enerweb.ebr.env;

import java.io.IOException;
import java.io.InputStream;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EnvironmentInputStream extends InputStream
// implements Serializable
{
    private boolean complainedAboutOneByteReads = false;
    private final byte[] onebyteReads = new byte[1];
    private String envId;
    private String resourceId;

    private transient IEnvironmentValueReader environmentValueReader;

    public EnvironmentInputStream(
        IEnvironmentValueReader environmentValueReader,
        final String envId,
        final String resourceId) throws IOException {
        this.environmentValueReader = environmentValueReader;
        this.envId = envId;
        this.resourceId = resourceId;
        environmentValueReader.init(envId, resourceId);
    }

    /*
     * (non-Javadoc)
     * @see java.io.InputStream#read()
     */
    @Override
    public int read() throws IOException {
        if (!complainedAboutOneByteReads) {
            log.warn("Reading one byte at a time which is really "
                + "inefficient.");
            complainedAboutOneByteReads = true;
        }
        int read = read(onebyteReads, 0, 1);
        if (read == 1) {
            int ret = onebyteReads[0];
            if (ret < 0) {
                ret += 256;
            }
            return ret;
        } else {
            return -1;
        }
    }

    @Override
    public int read(final byte[] b, final int off, final int len)
        throws IOException {
        byte[] read = environmentValueReader.read(len);
        if (read.length == 0) {
            return -1;
        }
        System.arraycopy(read, 0, b, off, read.length);
        return read.length;
    }

    @Override
    public void close() throws IOException {
        environmentValueReader.close();
    }
}
