package za.co.enerweb.ebr.task;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.env.IEnvironment;
import za.co.enerweb.ebr.env.IEnvironmentManager;
import za.co.enerweb.ebr.exceptions.TaskExecutionException;
import za.co.enerweb.ebr.exceptions.TaskInstantiationException;
import za.co.enerweb.ebr.exceptions.UnknownTaskExecutionException;
import za.co.enerweb.ebr.server.IHomeService;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableList;

/**
 * Server side. actually runs the task.
 */
// XXX: When its a @Singleton and running in a cluster , it will have a instance
// of
// this in each node/vm. So the joblist must be synced with the db
// from time to time..
@Singleton
// @Path("/task-exec")
@Lock(LockType.READ)
@Slf4j
public class TaskExecutorManager implements ITaskExecutorManager {

    // only meant to keep it while it is running
    private Cache<String, TaskExecution> activeTaskExecutions =
        CacheBuilder.newBuilder().weakValues()
            .expireAfterWrite(1, TimeUnit.DAYS)
            .build();

    // TODO: make size configurable
    private Cache<String, TaskExecutionMetadata> taskExecutions =
        CacheBuilder.newBuilder().maximumSize(200)
            .expireAfterAccess(7, TimeUnit.DAYS).build();

    @EJB
    private IEnvironmentManager environmentManager;

    @EJB
    private ITaskRegistry taskRegistry;

    @EJB
    private IHomeService homeService;

    private TaskExecution getActiveTaskExecution(final String taskExecutionId)
        throws UnknownTaskExecutionException {
        if (activeTaskExecutions.size() > 100) {
        log.debug("activeTaskExecutions.size(): " + activeTaskExecutions.size());
        }
        if (taskExecutions.size() > 100) {
        log.debug("taskExecutions.size(): " + taskExecutions.size());
        }
        TaskExecution taskExecution =
            activeTaskExecutions.getIfPresent(taskExecutionId);
        if (taskExecution == null) {
            throw new UnknownTaskExecutionException("Unknown taskExecution: "
                + taskExecutionId);
        }
        return taskExecution;
    }

    @Override
    public TaskExecutionMetadata getTaskExecutionMetadata(
        final String taskExecutionId)
        throws UnknownTaskExecutionException {
        TaskExecutionMetadata taskExecution =
            taskExecutions.getIfPresent(taskExecutionId);
        if (taskExecution == null) {
            throw new UnknownTaskExecutionException("Unknown taskExecution: "
                + taskExecutionId);
        }
        return taskExecution;
        // return getActiveTaskExecution(jobId).getTaskExecutionMetadata();
    }

    @Override
    public List<TaskExecutionMetadata> getTaskExecutionMetadata() {
        return ImmutableList.copyOf(taskExecutions.asMap().values());
    }

//    public String getLog(final String jobId)
//        throws UnknownTaskExecutionException {
//        return getActiveTaskExecution(jobId).getLog();
//    }

    // @Resource
    // private SessionContext sessionContext;

    @SneakyThrows
    private TaskExecution getTaskExecution()  {
        return EbrFactory.lookupEjb(TaskExecution.class);
    }

    @Override
    public TaskExecutionMetadata initTaskExecution(final String taskId, final String sessionId,
        IEnvironment inputEnvironment)
        throws TaskInstantiationException {
        TaskExecution taskExecution = getTaskExecution();
        taskExecution.init(taskId, sessionId, inputEnvironment);
        TaskExecutionMetadata taskExecutionMetadata = taskExecution.getTaskExecutionMetadata();
        activeTaskExecutions.put(taskExecutionMetadata.getId(), taskExecution);
        return taskExecutionMetadata;
    }

    @Override
    public TaskExecutionMetadata initTaskExecution(final TaskMetadata taskMetadata,
        final String sessionId, IEnvironment inputEnvironment)
        throws TaskInstantiationException {
        TaskExecution taskExecution = getTaskExecution();
        taskExecution.init(taskMetadata,
            sessionId, inputEnvironment);
        TaskExecutionMetadata taskExecutionMetadata = taskExecution.getTaskExecutionMetadata();
        activeTaskExecutions.put(taskExecutionMetadata.getId(), taskExecution);
        return taskExecutionMetadata;
    }

    public TaskExecutionMetadata run(final String taskExecutionId,
        final RunMode runMode)
        throws TaskExecutionException, UnknownTaskExecutionException {
        TaskExecution taskExecution = getActiveTaskExecution(taskExecutionId);
        TaskExecutionMetadata tem = taskExecution.getTaskExecutionMetadata();
        tem.setRunMode(runMode);
        taskExecutions.put(tem.getId(), tem);
        return taskExecution.runInBackground();
    }

    public void taskExecutionDone(String id) {
        TaskExecution ate = activeTaskExecutions.getIfPresent(id);
        activeTaskExecutions.invalidate(id);
        if (ate != null) {
            ate.dispose();
        }
    }
}
