package za.co.enerweb.ebr.server;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Local;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;

import lombok.extern.slf4j.Slf4j;

/**
 * XXX: add a EbrFactory.runInBackground(){executor.submit()} method
 * that looks up the executor and everything for you..
 * http://tomee.apache.org/examples-trunk/async-postconstruct/README.html
 */
@Slf4j
@Singleton
@Local
@Lock(LockType.READ)
public class Executor implements IExecutor {

    @Asynchronous
    public <T> Future<T> submit(Callable<T> task) throws Exception {
        try {
//            log.debug("** Executor.submit " + task);
            return new AsyncResult<T>(task.call());
        } catch (Exception ex) {
            log.error("Could not call task", ex);
            throw ex;
        }
    }

}
