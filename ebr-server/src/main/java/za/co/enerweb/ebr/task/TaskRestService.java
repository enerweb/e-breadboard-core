package za.co.enerweb.ebr.task;

import static java.lang.String.format;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import lombok.extern.slf4j.Slf4j;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.ContentDisposition;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;

import za.co.enerweb.ebr.datatype.IDataTypeRegistryService;
import za.co.enerweb.ebr.datatype.ITypeConverter;
import za.co.enerweb.ebr.datatype.dataresource.FileDataResource;
import za.co.enerweb.ebr.datatype.rest.RestResponse;
import za.co.enerweb.ebr.env.IEnvironment;
import za.co.enerweb.ebr.env.IEnvironmentManager;

/**
 * get /task-meta/<task-id> => task info
 * post,get /task/<task-id>&_execute=batch&var1=abc => run and return result
 */
@Singleton
@Startup
@LocalBean
@Path("/task")
@Lock(LockType.READ)
@Slf4j
// @MultipartConfig(location = "/tmp", fileSizeThreshold = 1024 * 1024,
// maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 5 * 5)
public class TaskRestService {
    @EJB
    private IEnvironmentManager environmentManager;

    @EJB
    private ITaskRegistry taskRegistry;

    @EJB
    private IDataTypeRegistryService dataTypeRegistryService;

    @EJB
    private ITaskExecutorManager taskExecutorManager;

    @Produces({MediaType.TEXT_PLAIN})
    @GET
    @Path("{taskId}")
    public Response getExecuteRestTask(@PathParam("taskId") String taskId,
        @Context HttpServletRequest request, @Context UriInfo ui) {
        return execRestTask(taskId, request, null, ui);
    }

    @Consumes({MediaType.MULTIPART_FORM_DATA})
    @Produces({MediaType.TEXT_PLAIN})
    @POST
    @Path("{taskId}")
    public Response postExecuteRestTask(@PathParam("taskId") String taskId,
        @Context HttpServletRequest request,
        MultipartBody body, @Context UriInfo ui) {

        return execRestTask(taskId, request, body, ui);
    }

    @Consumes({
        MediaType.APPLICATION_FORM_URLENCODED})
    @Produces({MediaType.TEXT_PLAIN})
    @POST
    @Path("{taskId}")
    public Response postEcuteRestTask(@PathParam("taskId") String taskId,
        @Context HttpServletRequest request, @Context UriInfo ui) {

        return execRestTask(taskId, request, null, ui);
    }

    private Response execRestTask(String taskId, HttpServletRequest request,
        MultipartBody body, UriInfo ui) {
        fixupEncoding(request);

        try {
            TaskMetadata taskMetadata =
                taskRegistry.getTaskMetadata(null, taskId);
            IEnvironment inEnv = environmentManager.newEnvironment();

            readQueryParametersIntoEnv(taskMetadata, inEnv, ui);
            readMultipartBodyIntoEnv(taskMetadata, inEnv, body);
            readRequestPartsIntoEnv(taskMetadata, inEnv, request);

            TaskExecutionMetadata taskExecution =
                taskExecutorManager
                    .initTaskExecution(taskMetadata, null, inEnv);
            // Future<TaskExecutionMetadata> res =
            taskExecutorManager.run(taskExecution.getId(), RunMode.BATCH);
            // taskExecution = res.get();
            taskExecution.waitTillDone();
            IEnvironment outEnv = environmentManager.getEnvironment(
                taskExecution.getOutputIEnvironmentId());
            // String strOut =
            // outEnv.getVariableValue(strOutMetadata.getName());

            for (OutputMetadata om : taskMetadata.getOutputMetadata()) {
                if (om.getTypeClass().equals(RestResponse.class)) {
                    RestResponse response =
                        outEnv.getVariableValue(om.getName());
                    if (response != null) {
                        Response ret = response.getResponse();
                        if (ret != null) {
                            return ret;
                        }
                    }
                }
            }
            // TODO: maybe build up a json version of the outputs?!

            return Response.ok(
                format("Executed task. Status=%s. time=%s",
                    taskExecution.getStatus(), taskExecution
                        .getDurationString())
                )
                .header("task-execution-status",
                    taskExecution.getStatus().name())
                .header("task-execution-duration-millis",
                    taskExecution.getDurationMillis())
                .build();
            // } catch (CouldNotFindTaskException e) {
        } catch (Exception e) {
            log.debug("Rest task failed.", e);
            return Response.status(Status.BAD_REQUEST).entity(
                "Could not execute task: " + e.getMessage()).build();
        }
    }

    private String fixupEncoding(HttpServletRequest request) {
        String encoding = request.getCharacterEncoding();
        if (encoding == null) {
            try {
                request.setCharacterEncoding(encoding = "UTF-8");
            } catch (UnsupportedEncodingException ex) {
                log.error("Unsupported encoding: ", ex);
            }
        }
        return encoding;
    }

    /*
     * not sure if this is used or works..
     */
    private void readRequestPartsIntoEnv(TaskMetadata taskMetadata,
        IEnvironment inEnv, HttpServletRequest request) throws IOException,
        ServletException {
        // log.debug("got " + request.getParts().size() + " parts");
        for (Part part : request.getParts()) {
            String contentType = part.getContentType();
            String variableName = part.getName();

            InputMetadata inputMetadata =
                taskMetadata.getInputMetadata(variableName);
            if (inputMetadata == null) {
                log.debug("Ignoring mutipart message part because "
                    + "the task has no such variable: " + variableName);
            } else {
                ITypeConverter<Serializable> converter =
                    dataTypeRegistryService.lookupConverter(null,
                        inputMetadata.getTypeName());
                inEnv.setVariableValue(variableName,
                    converter.fromSimpleStructure(inEnv.getId(),
                        variableName, part.getInputStream()));
            }

            // only limited supported types for now,
            // rather use standard converters and data types
            // switch (contentType) {
            // case MediaType.APPLICATION_OCTET_STREAM:
            // part.getInputStream();
            //
            // break;
            //
            // default:
            // log.warn("Unknown content type {} for {}.", contentType,
            // variableName );
            // break;
            // }

        }
    }

    private void readMultipartBodyIntoEnv(TaskMetadata taskMetadata,
        IEnvironment inEnv,
        MultipartBody body) throws IOException {
        if (body == null) {
            return;
        }
        // log.debug("getAllAttachments:"
        // + StringUtils.join(body.getAllAttachments(), ","));
        // log.debug("getRootAttachment:"
        // + body.getRootAttachment());
        // log.debug("getChildAttachments:"
        // + StringUtils.join(body.getChildAttachments(), ","));

        for (Attachment att : body.getAllAttachments()) {
            ContentDisposition contentDisposition =
                att.getContentDisposition();
            // log.debug("att.getContentDisposition().getParameter(\"name\")='"
            // + contentDisposition.getParameter("name") + "'");
            // log.debug("att.getContentDisposition().getParameter(\"filename\")='"
            // + contentDisposition.getParameter("file`name") + "'");
            // log.debug("att.getContentId() " + att.getContentId());
            // log.debug("att.getDataHandler().getName()='"
            // + att.getDataHandler().getName() + "'");

            // FileItemIterator itemIterator =
            // new ServletFileUpload().getItemIterator(request);
            // for (; itemIterator.hasNext();) {
            // FileItemStream fis = (FileItemStream)
            // itemIterator.next();
            // log.debug("got multi part: " +
            // fis.getFieldName() + " " + fis.getName()
            // );
            //
            // String variableName = fis.getFieldName();
            // String fileName = fis.getName();
            // fis.openStream()

            String variableName =
                att.getContentDisposition().getParameter("name");
            String fileName = att.getDataHandler().getName();
            InputMetadata inputMetadata =
                taskMetadata.getInputMetadata(variableName);
            if (inputMetadata == null) {
                log.debug("Ignoring mutipart message attachment because "
                    + "the task has no such variable: " + variableName);
            } else {
                FileDataResource fileDataResource =
                    new FileDataResource(
                        inEnv.getId(), fileName, att.getDataHandler()
                            .getInputStream());
                fileDataResource.addToEnvironmentIfNeeded(inEnv);

                inEnv.setVariableValue(variableName, fileDataResource);
            }
        }
    }

    private void readQueryParametersIntoEnv(TaskMetadata taskMetadata,
        IEnvironment inEnv, UriInfo ui) throws IOException {
        if (ui == null) {
            return;
        }
        for (Entry<String, List<String>> entry : ui.getQueryParameters()
            .entrySet()) {

            String variableName = entry.getKey();

            InputMetadata inputMetadata =
                taskMetadata.getInputMetadata(variableName);
            List<String> values = entry.getValue();
            if (values.isEmpty()) {
                continue;
            }
            if (values.size() > 1) {
                log.debug("Multiple query parameters are not supported,"
                    + "using the last value.");
            }
            String value = values.get(values.size() - 1);
            if (inputMetadata == null) {
                log.debug("Ignoring query parameter because "
                    + "the task has no such variable: " + variableName);
            } else {
                ITypeConverter<Serializable> converter =
                    dataTypeRegistryService.lookupConverter(null,
                        inputMetadata.getTypeName());

                inEnv.setVariableValue(variableName,
                    converter.fromSimpleStructure(inEnv.getId(),
                        variableName, value));
            }
        }
    }
}
