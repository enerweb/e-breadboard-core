package za.co.enerweb.ebr.home_layout;

public class CouldNotUpgradeEbrHomeLayoutException extends Exception {

    private static final long serialVersionUID = 1L;

    public CouldNotUpgradeEbrHomeLayoutException() {
        super();
    }

    public CouldNotUpgradeEbrHomeLayoutException(int oldVersion,
            Throwable cause) {
        super(String.format("Could not upgrade EBR_HOME layout from version %s "
            + "to version %s", oldVersion, oldVersion+1), cause);
    }

    public CouldNotUpgradeEbrHomeLayoutException(String message,
        Throwable cause) {
        super(message, cause);
    }

    public CouldNotUpgradeEbrHomeLayoutException(String message) {
        super(message);
    }

    public CouldNotUpgradeEbrHomeLayoutException(Throwable cause) {
        super(cause);
    }


}
