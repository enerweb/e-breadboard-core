package za.co.enerweb.ebr.home_layout;

import java.io.File;
import java.util.EnumMap;
import java.util.Map;

import za.co.enerweb.ebr.server.HomeFile;
import za.co.enerweb.ebr.server.HomeFileCategory;
import za.co.enerweb.ebr.server.HomeFileSpec;

public class HomeLayout1 extends AbstractHomeLayout {

    private static final Map<HomeFileCategory, String[]> baseLayout
        = new EnumMap<HomeFileCategory, String[]>(HomeFileCategory.class);
    static {
        baseLayout.put(HomeFileCategory.ENVIRONMENTS, new String[]{VAR, ENV});
        baseLayout.put(HomeFileCategory.ETC, new String[] {ETC});
        baseLayout.put(HomeFileCategory.FRONTEND,
            new String[]{SHARE, FRONTEND});
        baseLayout.put(HomeFileCategory.JOBS, new String[] {VAR, JOBS});
        baseLayout.put(HomeFileCategory.RESOURCES,
            new String[] {SHARE, RESOURCES});
        baseLayout.put(HomeFileCategory.RESOURCES_PER_TECHNOLOGY,
            new String[] {SHARE, RESOURCES});
        //baseLayout.put(HomeFileCategory.ROOT, new String[] {});
        baseLayout.put(HomeFileCategory.SESSIONS,
            new String[] {VAR, SESSIONS});
        baseLayout.put(HomeFileCategory.TASKS, new String[]{BIN, TASKS});
        baseLayout.put(HomeFileCategory.TASKS_PER_TECHNOLOGY,
            new String[]{BIN, TASKS});
        baseLayout.put(HomeFileCategory.TASK_COMMON,
            new String[]{SHARE, TASK_COMMON});
        baseLayout.put(HomeFileCategory.TASK_COMMON_PER_TECHNOLOGY,
            new String[]{SHARE, TASK_COMMON});
        baseLayout.put(HomeFileCategory.TASK_PERSPECTIVES,
            new String[]{BIN, TASK_PERSPECTIVES});
        baseLayout.put(HomeFileCategory.TEMP, new String[]{TMP});
        baseLayout.put(HomeFileCategory.TRASH, new String[]{TRASH});
        baseLayout.put(HomeFileCategory.WEB_CONTENT,
            new String[]{SHARE, WEB_CONTENT});
    }

    public HomeLayout1(File home) {
        super(home, baseLayout);
    }

    public HomeLayout1() {
        super(null, baseLayout);
    }

    @Override
    public int getVersion(){
        return 1;
    }

    public String[] getKnownRootDirs() {
        return new String[] {BIN, CACHE, ETC, LIB, OPT,
            SHARE, SRC, TRASH, TMP, VAR};
    }

    public HomeFile getFile(HomeFileSpec hfs) {
        switch (hfs.getCategory()) {
        case CACHE_PER_TECHNOLOGY:
            return getFile(CACHE, hfs.getParameters().get(0),
                join(hfs.getPathElements()));
        case DB:
            return getFile(VAR, DB, hfs.getParameters().get(0),
                join(hfs.getPathElements()));
        case FRONTEND:
            return getFile(SHARE, FRONTEND, hfs.getParameters().get(0),
                hfs.getParameters().get(1),
                join(hfs.getPathElements()));
        case LIB_PER_TECHNOLOGY:
            return getFile(LIB, hfs.getParameters().get(0),
                join(hfs.getPathElements()));
        case VAR_PER_COMPONENT:
            return getFile(VAR, hfs.getParameters().get(0),
                join(hfs.getPathElements()));
        }
        return super.getFile(hfs);
    }

}
