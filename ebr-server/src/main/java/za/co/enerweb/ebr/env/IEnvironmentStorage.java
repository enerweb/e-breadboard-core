package za.co.enerweb.ebr.env;

import java.util.Map;

import za.co.enerweb.ebr.session.SessionMetadata;

public interface IEnvironmentStorage {
    public void deleteEnvironment(String envId);

    /**
     * @param sessionId
     */
    void deleteSession(String sessionId);

    /**
     * @return
     */
    Map<String, SessionMetadata> loadAllSession();

    public Environment loadEnvironment(final String envId);

    /**
     * @param sessionId
     * @return
     */
    SessionMetadata loadSession(String sessionId);

    public void save(String envId, IEnvironment iEnvironment);

    /**
     * @param sessionId
     * @param session
     */
    void save(String sessionId, SessionMetadata session);
}
