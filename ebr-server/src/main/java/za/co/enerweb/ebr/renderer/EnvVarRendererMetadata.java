package za.co.enerweb.ebr.renderer;

import lombok.Data;

/**
 * Renderer metadata
 */
@Data
public class EnvVarRendererMetadata implements
    Comparable<EnvVarRendererMetadata> {
    private final String id;
    private final Class<?> rendererClass;
    private final byte rank;
    private final RendererMode supports;

    /**
     * compare ranks
     * (uses the class name as consistent tie breaker)
     */
    public int compareTo(final EnvVarRendererMetadata o) {
        int ret = rank - o.rank;
        // tie breaker
        if (ret == 0) {
            ret =
                rendererClass.getName().compareTo(o.rendererClass.getName());
            // if it is still 0, it does not matter, just be consistent :)
        }
        return ret;
    }

    public boolean isSupportsEditing() {
        return supports.isSupportsEditing();
    }

    public boolean isSupportsViewing() {
        return supports.isSupportsViewing();
    }
}
