package za.co.enerweb.ebr.home_layout;

import java.io.File;
import java.io.IOException;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FileUtils;

import za.co.enerweb.ebr.server.HomeFile;
import za.co.enerweb.ebr.server.HomeFileCategory;
import za.co.enerweb.ebr.server.HomeFileSpec;
import za.co.enerweb.ebr.task.ITaskRegistry;

@Slf4j
public abstract class AbstractUpgradeHomeLayout {
    protected AbstractHomeLayout oldHomeLayout;
    protected AbstractHomeLayout newHomeLayout;
    protected File home;

    public void upgrade(ITaskRegistry taskRegistry, File writableHome) {
        home = writableHome;
        oldHomeLayout = new HomeLayout0(home);
        oldHomeLayout.setTaskRegistry(taskRegistry);
        newHomeLayout = new HomeLayout1(home);
        newHomeLayout.setTaskRegistry(taskRegistry);
        upgrade(taskRegistry);
        newHomeLayout.updateReadmeFiles();
    }

    protected abstract void upgrade(ITaskRegistry taskRegistry);

    private void copyFiles(File srcDir, File targetDir) {
        try {
            if (srcDir.exists() && !srcDir.equals(targetDir)) {
                log.info("Copying files from:\n"
                    + srcDir + " to:\n" + targetDir);
                if (!targetDir.exists()) {
                    targetDir.mkdirs();
                }
                FileUtils.copyDirectory(srcDir, targetDir);
            }
        } catch (IOException e) {
            log.error("Could not copy files to the new layout, "
                + "the old files will be moved to the trash:\n"
                + newHomeLayout.getFile(
                    new HomeFileSpec(HomeFileCategory.TRASH)), e);
        }
    }

    protected void moveDir(HomeFileCategory category) {
        moveDir(category, new String[0]);
    }

    protected void moveDir(HomeFileCategory category,
            String[] parameters) {
        try {
            HomeFile srcHomeFile = oldHomeLayout.getFile(
                new HomeFileSpec(category, parameters));
            if (srcHomeFile.exists()) {
                File srcDir = srcHomeFile.getDir();
                File targetDir = newHomeLayout.getFile(
                    new HomeFileSpec(category, parameters)).getOrCreateDir();

                if (!srcDir.equals(targetDir) ) {
                    // Copy the srcDir content and then delete the original dir
                    copyFiles(srcDir, targetDir);
                    delete(srcHomeFile);
                }
            }
        } catch (Exception e) {
            // isolate the different moves from each other
            log.error("Could not move files to the new layout.", e);
        }
    }

    private void delete(HomeFile srcDir) {
        log.info("Moving file or directory to the trash:\n"
            + srcDir);
        try {
            newHomeLayout.deleteHomeFile(srcDir);
        } catch (IOException e) {
            log.error("Could not move file or directory tasks to the "
                + "trash:\n" + srcDir, e);
        }
    }

    protected void moveFile(HomeFileCategory category,
        String[] parameters) {
        try {
            HomeFile srcHomeFile = oldHomeLayout.getFile(
                new HomeFileSpec(category, parameters));
            if (srcHomeFile.exists()) {
                HomeFile targetHomeFile = newHomeLayout.getFile(
                    new HomeFileSpec(category, parameters));
                if (!srcHomeFile.equals(targetHomeFile) ) {
                    File srcFile = srcHomeFile.getFile();
                    targetHomeFile.makeParentDirs();
                    // Copy the srcFile and then delete the original
                    FileUtils.copyFile(srcFile, targetHomeFile.getFile());
                    delete(srcHomeFile);
                }
            }
        } catch (Exception e) {
            // isolate the different moves from each other
            log.error("Could not move files to the new layout.", e);
        }
    }
}
