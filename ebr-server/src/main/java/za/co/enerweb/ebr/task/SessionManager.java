package za.co.enerweb.ebr.task;

import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.env.EnvironmentFilesystemStorage;
import za.co.enerweb.ebr.env.IEnvironmentManager;
import za.co.enerweb.ebr.env.IEnvironmentStorage;
import za.co.enerweb.ebr.server.IHomeService;
import za.co.enerweb.ebr.server.IdGenerator;
import za.co.enerweb.ebr.session.SessionMetadata;

// FIXME: don't insist on loading all the sessions on startup!
// TODO: When its a @Singleton and running in a cluster , it will have a
// instance of
// this in each node/vm. So the sessionlist must be synced with the db
// from time to time..
@Slf4j
@Singleton
public class SessionManager implements ISessionManager {

    @EJB
    private IHomeService homeService;

    @EJB
    private IEnvironmentManager environmentManager;

    private IEnvironmentStorage sessionStorage;

    private Map<String, SessionMetadata> sessions;

    @PostConstruct
    private void init() {
        sessionStorage =
            new EnvironmentFilesystemStorage(homeService);
        reloadSessions();
    }

    /**
     * mainly for testing
     */
    public void reloadSessions() {
        sessions = sessionStorage.loadAllSession();
    }

    public void deleteSession(final String id) {
        SessionMetadata session = getSession(id);
        deleteSessionEnvironments(session);
        sessions.remove(id);
        sessionStorage.deleteSession(id);
        log.info("Deleted session: " + id);
    }

    private void deleteSessionEnvironments(final SessionMetadata session) {
        for (Iterator<String> iterator =
                session.getEnvironmentIds().values().iterator();
                iterator.hasNext();) {
            String envId = iterator.next();
            environmentManager.deleteEnvironment(envId);
            iterator.remove();
        }
    }

    public SessionMetadata findSession(final String name) {
        for (SessionMetadata session : sessions.values()) {
            if (session.getName().equals(name)) {
                return session;
            }
        }
        return null;
    }

    /**
     * not foolproof as there might be duplicate names
     * @param name
     * @return
     */
    public SessionMetadata getOrCreateSessionByName(final String name) {
        SessionMetadata session = findSession(name);
        if (session == null) {
            session = newSession(name);
            saveSession(session.getId());
        }
        return session;
    }

    @Override
    public SessionMetadata getOrCreateSession(final String id,
            final String name) {
        SessionMetadata session = getSession(id);
        if (session == null) {
            session = newSession(name);
        }
        return session;
    }

    @Override
    public SessionMetadata getSession(final String id) {
        return sessions.get(id);
    }

    @Override
    public SessionMetadata ensureNamespace(final String id,
            final String namespace) {
        SessionMetadata session = getSession(id);
        String envId = session.getEnvironmentId(namespace);
        if (envId == null) {
            envId = environmentManager.newEnvironmentId();
            session.setEnvironmentId(namespace, envId);
        }
        return session;
    }

    public Map<String, SessionMetadata> getSessions() {
        return Collections.unmodifiableMap(sessions);
    }

    @Override
    public SessionMetadata newSession(final String name) {
        String id = IdGenerator.getUuid();
        SessionMetadata ret =
            new SessionMetadata(id, name, environmentManager.newEnvironmentId());
        sessions.put(id, ret);
        saveSession(id);
        return ret;
    }

    @Override
    public SessionMetadata resetSession(final String id) {
        SessionMetadata ret = getSession(id);
        if (ret == null) {
            // XXX: maybe we should rather take in a name?
            ret = newSession("session created by resetSession");
        } else {
            // reset all namespaces
            deleteSessionEnvironments(ret);
            // add the default environment again
            ret.setEnvironmentId(environmentManager.newEnvironmentId());
        }
        saveSession(ret.getId());
        return ret;
    }

    public String newSessionId(final String name) {
        return newSession(name).getId();
    }

    public void saveSession(final String id) {
        saveSession(environmentManager, id);
    }

    private void saveSession(final IEnvironmentManager envManager,
        final String id) {
        SessionMetadata session = getSession(id);
        if (session != null) {
            sessionStorage.save(id, session);
            for (String envId: session.getEnvironmentIds().values()) {
                envManager.saveEnvironment(envId);
            }
        }
    }

    public void saveSessions() {
        for (SessionMetadata session : sessions.values()) {
            try {
                // log.debug("Saving session: " + session.getId());
                saveSession(environmentManager, session.getId());
            } catch (Exception e) {
                log.error("Could not save session: " + session.getId(), e);
            }
        }
        log.debug("Saved {} session(s).", sessions.size());
    }
}
