package za.co.enerweb.ebr.env;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import javax.annotation.PreDestroy;
import javax.ejb.PrePassivate;
import javax.ejb.Remove;
import javax.ejb.Stateful;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.datatype.dataresource.IDataSource;

/**
 * Helper for providing a input stream reading from the env.
 */
@Stateful
@Slf4j
public class EnvironmentValueReader
    implements IEnvironmentValueReader {
    private transient InputStream inputStream;

    // private int readCount = 0;

    // @EJB
    // private EnvironmentManager environmentManager;

    /*
     * (non-Javadoc)
     * @see za.co.enerweb.ebr.env.IEnvironmentValueReader#init(java.lang.String,
     * java.lang.String)
     */
    @Override
    public void init(final String envId, final String resourceId)
        throws IOException {
        IEnvironment env = // environmentManager
            EbrFactory.getEnvironmentManager()
            .getEnvironment(envId);
        IDataSource resource = env.getResource(resourceId);
        // log.debug("init " + resource);
        inputStream = resource.asInputStream();
    }

    /*
     * (non-Javadoc)
     * @see za.co.enerweb.ebr.env.IEnvironmentValueReader#read(int)
     */
    @Override
    public byte[] read(final int len) throws IOException {
        if (inputStream == null) {
            // log.debug("You can't read from a clossed "
            // + "EnvironmentValueReader");
            return new byte[0];
        }
        // log.debug("reading " + readCount + ".." + (readCount += len));
        // XXX: keep buffer array around, and re-use if the length is the same
        // which should be the case most of the time...
        byte[] ret = new byte[len];
        int read = inputStream.read(ret);
        if (read == -1) {
            close();
            return new byte[0];
        }
        if (read < len) {
            close();
            return Arrays.copyOf(ret, read);
        }
        return ret;
    }

    @Override
    @PreDestroy
    @PrePassivate
    @Remove
    public void close() throws IOException {
        // log.debug("Closing" + resource);
        if (inputStream != null) {
            inputStream.close();
            inputStream = null;
        }
    }
}
