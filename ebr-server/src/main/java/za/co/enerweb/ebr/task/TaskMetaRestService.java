package za.co.enerweb.ebr.task;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.datatype.IDataTypeRegistryService;
import za.co.enerweb.ebr.env.IEnvironmentManager;
import za.co.enerweb.toolbox.rest.ResponsTypeUtil;

/**
 * get /task-meta/<task-id> => task info
 */
@Singleton
@Startup
@LocalBean
@Path("/task-meta")
@Lock(LockType.READ)
@Slf4j
// @MultipartConfig(location = "/tmp", fileSizeThreshold = 1024 * 1024,
// maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 5 * 5)
public class TaskMetaRestService {
    @EJB
    private IEnvironmentManager environmentManager;

    @EJB
    private ITaskRegistry taskRegistry;

    @EJB
    private IDataTypeRegistryService dataTypeRegistryService;

    @EJB
    private ITaskExecutorManager taskExecutorManager;

    // not sure what we want this to mean yet
    @Produces({MediaType.APPLICATION_JSON
        , MediaType.APPLICATION_XML
        , MediaType.TEXT_PLAIN
    })
    @GET
    @Path("{taskId}" + ResponsTypeUtil.PATH)
    public Response executeRestTask(@PathParam("taskId") String taskId,
        @PathParam(ResponsTypeUtil.PARAM) String responseType) {
        try {
            TaskMetadata taskMetadata =
                taskRegistry.getTaskMetadata(null, taskId);
            return ResponsTypeUtil.parse(Response.ok(taskMetadata)
                // .header("task-execution-status",
                // taskExecution.getStatus().name())
                , responseType).build();
        } catch (Exception e) {
            log.debug("Rest task not found.", e);
            return ResponsTypeUtil.parse(
                Response.status(Status.BAD_REQUEST),
                "Could not find task", e, responseType)
                .build();
        }
    }
}
