package za.co.enerweb.ebr.task;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FilenameUtils;
import org.ini4j.Ini;
import org.ini4j.Profile.Section;

import za.co.enerweb.ebr.datatype.IDataTypeRegistryService;
import za.co.enerweb.ebr.exceptions.CouldNotFindTaskException;
import za.co.enerweb.ebr.server.HomeFile;
import za.co.enerweb.ebr.server.HomeFileCategory;
import za.co.enerweb.ebr.server.HomeFileSpec;
import za.co.enerweb.ebr.server.IHomeService;
import za.co.enerweb.ebr.server.Registrar;
import za.co.enerweb.toolbox.timing.Stopwatch;

@Singleton
@Lock(LockType.READ)
@Slf4j
public class TaskRegistry implements ITaskRegistry {


    private static class TaskMap extends
        LinkedHashMap<String, TaskMetadata> {
        private static final long serialVersionUID = 1L;
    };

    private static class TaskPerspectiveMap extends
        HashMap<String, TaskMap> {
        private static final long serialVersionUID = 1L;
    };

    /**
     * perspectiveId=null is the default task registry
     */
    private TaskPerspectiveMap taskPerspectiveMap =
        new TaskPerspectiveMap();

    @EJB
    private IHomeService homeService;

    @EJB
    private IDataTypeRegistryService dataTypeRegistryService;

    @EJB
    private ITaskExecutorManager taskExecutorManager;

    @Resource
    private TimerService timerService;

    public void clear() {
        taskPerspectiveMap = new TaskPerspectiveMap();
        if (timerService != null && timerService.getTimers() != null) {
            for (Timer t : timerService.getTimers()) {
                t.cancel();
            }
        }
    }

    public Collection<String> getPerspectiveIds() {
        return taskPerspectiveMap.keySet();
    }

    /**
     * @param perspectiveId if null, the default perspective is given.
     * @return a {@link TaskMap}, if it does not exist it will be created
     */
    private TaskMap getTaskMap(final String perspectiveId) {
        TaskMap ret = taskPerspectiveMap.get(perspectiveId);
        if (ret == null) {
            ret = new TaskMap();
            taskPerspectiveMap.put(perspectiveId, ret);
        }
        return ret;
    }

    public Iterator<TaskMetadata> getTaskIterator(
            final String perspectiveId) {
        return getTaskMap(perspectiveId).values().iterator();
    }

    public TaskMetadata getTaskMetadata(final String perspectiveId,
        final String id) throws CouldNotFindTaskException {
        TaskMetadata taskMetadata = getTaskMap(perspectiveId).get(id);
        if (taskMetadata == null) {
            throw new CouldNotFindTaskException(
                "Could not find task with id: " + id);
        }
        return taskMetadata;
    }

    public Collection<TaskMetadata> getTasks(
            final String perspectiveId) {
        return Collections.unmodifiableCollection(
            getTaskMap(perspectiveId).values());
    }

    /*
     * XXX: Maybe do these in parallel, if we can show that it will help.
     */
    private void processAbstractTaskParserSubclasses() {
        for (ATaskParser taskParser : getAllTaskParsers()) {
            try {
                taskParser.registerTasks();
            } catch (Throwable e) {
                log.error("Could not regester tasks from: "
                    + taskParser.getName(), e);
            }
        }
    }

    public Collection<ATaskParser> getAllTaskParsers() {
        // find all classes implementing ATaskParser
        Set<Class<? extends ATaskParser>> taskParsers =
            Registrar.getReflections().getSubTypesOf(
                ATaskParser.class);

        Collection<ATaskParser> ret = new ArrayList<ATaskParser>(
            );

        // if (taskParsers.size() == 0) {
        // logger.debug("Could not find any TaskParsers.");
        // } else {
        // logger.debug("Found " + taskParsers.size() + " TaskParsers");
        // }
        for (Class<? extends ATaskParser> taskParserClass
            : taskParsers) {
            // log.debug(taskParserClass.getName());
            // only consider non-abstract subclasses
            if (!Modifier.isAbstract(taskParserClass.getModifiers())) {
                try {
                    ATaskParser taskParser =
                        taskParserClass.newInstance();

                    taskParser.setHomeService(homeService);
                    taskParser.setTaskRegistry(this);
                    taskParser
                        .setDataTypeRegistryService(dataTypeRegistryService);

                    ret.add(taskParser);
                } catch (Throwable e) {
                    log.error("Could not instantiate task parser: "
                        + taskParserClass.getName(), e);
                }
            } else {
                log.debug("Ignoring abstract "
                    + taskParserClass.getName());
            }
        }
        return ret;
    }

    @Timeout
    public void timeout(Timer timer) {
        if (timer.getInfo() instanceof TaskMetadata) {
            TaskMetadata tm = (TaskMetadata) timer.getInfo();
            try {
                // log.debug("Launching scheduled task: " + tm.getTitleAndId());
                TaskExecutionMetadata tem =
                    taskExecutorManager.initTaskExecution(
                        tm, null, null);
                taskExecutorManager.run(tem.getId(), RunMode.BATCH);
            } catch (Exception e) {
                log.info("Could not launch scheduled task: " + tm, e);
            }
        }
    }

    private void processRegisterTaskPerspectives() {
        Iterable<HomeFile> htmlResourcePaths = homeService.findFiles(
            new HomeFileSpec(HomeFileCategory.TASK_PERSPECTIVES),
            new String[] {IHomeService.TASK_PERSPECTIVE_EXTENTION});
        for (HomeFile htmlResourcePath : htmlResourcePaths) {
            registerTaskPerspective(htmlResourcePath);
        }
    }

    private void registerTaskPerspective(
        final HomeFile homeFile) {
        String perspectiveId = FilenameUtils.getBaseName(
            homeFile.getName());

        try {
            parseTaskPerspective(perspectiveId, homeFile.getInputStream());
        } catch (Exception e) {
            log.error("Could not read task perspective file: "
                + homeFile, e);
        }
    }

    public void parseTaskPerspective(final String perspectiveId,
        final InputStream stream) throws IOException,
        CloneNotSupportedException {
        Ini ini = new Ini();
        ini.load(stream);
        for (Section section : ini.values()) {
            String taskId = section.getName();
            if (!taskId.equals("default")) { // ignore default section
                try {
                    TaskMetadata taskMetadata =
                        getTaskMetadata(null, taskId).clone();
                    String title = section.fetch("title");
                    if (title != null) {
                        taskMetadata.setTitle(title);
                    }
                    String description = section.fetch("description");
                    if (description != null) {
                        taskMetadata.setDescription(description);
                    }
                    registerTask(perspectiveId, taskMetadata);
                } catch (CouldNotFindTaskException e) {
                    log.error("Could not find task with id '"
                        + taskId + "' referenced by task perspective : "
                        + perspectiveId);
                    continue;
                }
            }
        }
    }

    public void refreshRegistry() {
        try {
            // logger.debug("Refreshing Task Registry");
            clear();

            // if (SystemConfiguration.isRegisterHelloWorld()) {
            // TaskMetadata taskMetaData = HelloWorld.getInitTaskMetaData();
            // registerTask(taskMetaData);
            //
            // // taskMetaData = Goodbye.getInitTaskMetaData();
            // // registerTask(taskMetaData);
            // }

            Stopwatch sw = new Stopwatch();

            processAbstractTaskParserSubclasses();

            processRegisterTaskPerspectives();

            log.info("Registering all Tasks took {} ms", sw
                .getTotalMilliSeconds());

            for (TaskMetadata tm : getTasks(null)) {
                Schedule schedule = tm.getSchedule();
                if (schedule.isConfigured()) {
                    log.info("Scheduling task: " + schedule + " "
                        + tm.getTitleAndId());
                    final TimerConfig conf = new TimerConfig(tm, false);
                    timerService.createCalendarTimer(
                        schedule.getScheduleExpression(), conf);
                }
            }

        } catch (Throwable e) {
            log.error("Could not refresh task registry.", e);
        }
    }

    public void registerTask(final TaskMetadata taskMetadata) {
        registerTask(null, taskMetadata);
    }

    // XXX: maybe make this asynchronous and keep track what is still
    // outstanding in a private collection - if we can prove that this is slow.
    public void registerTask(final String perspectiveId,
        final TaskMetadata taskMetadata) {
        if (taskMetadata != null) {
            getTaskMap(perspectiveId).put(taskMetadata.getId(),
                taskMetadata);
        }
    }
}
