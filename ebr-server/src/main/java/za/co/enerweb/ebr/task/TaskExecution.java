package za.co.enerweb.ebr.task;

import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.Callable;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.xml.bind.ValidationException;

import lombok.Getter;
import lombok.SneakyThrows;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.enerweb.ebr.datatype.control.Action;
import za.co.enerweb.ebr.datatype.dataresource.IDataResource;
import za.co.enerweb.ebr.env.EnvVarItem;
import za.co.enerweb.ebr.env.IEnvironment;
import za.co.enerweb.ebr.env.IEnvironmentManager;
import za.co.enerweb.ebr.exceptions.ServiceException;
import za.co.enerweb.ebr.exceptions.TaskInstantiationException;
import za.co.enerweb.ebr.server.IExecutor;
import za.co.enerweb.ebr.server.IHomeService;
import za.co.enerweb.ebr.server.IdGenerator;
import za.co.enerweb.ebr.session.SessionMetadata;

// TODO load test running 1000 jobs (making 2 environments each)
// is it slow? does it take too much memory?
@Stateful
// (name = "TaskExecution")
@LocalBean
@Lock(LockType.READ)
public class TaskExecution implements Runnable {
    private static final String logID_PREFIX = "ebrjob.";

    @EJB
    private IHomeService homeService;

    @EJB
    private IEnvironmentManager environmentManager;

    @EJB
    private ITaskRegistry taskRegistry;

    @EJB
    private ISessionManager sessionManager;

    @EJB
    private IExecutor executor;

    @EJB
    private ITaskExecutorManager taskExecutorManager;

    @Getter
    private TaskExecutionMetadata taskExecutionMetadata;


    private String sessionId;
    private SessionMetadata session = null;
    private transient ITask task;
    // private transient Thread thread;
    private transient Logger log;
    private transient IEnvironment inputEnvironment;
    private transient IEnvironment outputEnvironment;
    private transient IEnvironment sessionEnvironment;

    /**
     *
     * @param taskId
     * @param sessionId may be null
     * @throws TaskInstantiationException
     */
    public void init(
        final String taskId, final String sessionId)
    throws TaskInstantiationException {
        init(taskId, sessionId, null);
    }

    /**
     * allow constructing input environment outside
     * @param taskId
     * @param sessionId may be null
     * @param inputEnvironment
     * @throws TaskInstantiationException
     */
    public void init(final String taskId, final String sessionId,
        final IEnvironment inputEnvironment)
        throws TaskInstantiationException {
        init(taskRegistry.getTaskMetadata(
            null, taskId),
            sessionId, inputEnvironment);
    }

    /**
     * - * @param taskMetadata
     * - * @param sessionId may be null
     * - * @throws TaskInstantiationException
     * -
     */
    public void init(
        final TaskMetadata taskMetadata, final String sessionId)
        throws TaskInstantiationException {
        init(taskMetadata, sessionId,
            environmentManager
            .newEnvironment());
    }

    public void init(
        final TaskMetadata taskMetadata, final String sessionId,
        final IEnvironment inputEnvironment)
        throws TaskInstantiationException {

        init(null, taskMetadata, sessionId, inputEnvironment);
    }

    /**
     * allow constructing input environment outside
     * @param taskMetadata
     * @param sessionId may be null
     * @param inputEnvironment
     * @throws TaskInstantiationException
     */
    public void init(String taskId,
        TaskMetadata taskMetadata, final String sessionId,
        IEnvironment inputEnvironment)
        throws TaskInstantiationException {
        this.sessionId = sessionId;

        if (taskMetadata == null) {
            taskMetadata = taskRegistry.getTaskMetadata(
                null, taskId);
        }

        if (inputEnvironment == null) {
            inputEnvironment = environmentManager.newEnvironment();
        }

        try {
            task = instantiateTask(taskMetadata);
        } catch (Exception e) {
            throw new TaskInstantiationException(
                "Could not instantiate task: " + taskMetadata.getTitle()
                    + "\n(id: " + taskMetadata.getId() + ")", e);
        }



        String taskExecId = IdGenerator.getTaskExecId(taskMetadata.getTitle());

        taskExecutionMetadata = new TaskExecutionMetadata();
        taskExecutionMetadata.setId(taskExecId);
        taskExecutionMetadata.setTaskMetadata(taskMetadata);
        taskExecutionMetadata.setStatus(TaskExecutionStatus.INITIALISED);
        log = LoggerFactory.getLogger(logID_PREFIX + taskExecId);

        this.inputEnvironment = inputEnvironment;
        IEnvironmentManager envMan = environmentManager;
        outputEnvironment = envMan.newEnvironment();

        task.setEbrHomePath(homeService.getMainHomeDir().getAbsolutePath());
        task.setWorkdir(environmentManager
                .getEnvironmentDir(outputEnvironment.getId()).getAbsolutePath());

        taskExecutionMetadata.setInputIEnvironmentId(inputEnvironment.getId());
        taskExecutionMetadata.setOutputIEnvironmentId(outputEnvironment.getId());

        if (sessionId != null) {
            session = sessionManager.getSession(sessionId);
            sessionEnvironment = envMan.getEnvironment(
                session.getEnvironmentId());
        }

        Serializable runOnInitActionValue = null;
        InputMetadata actionInput = null;

        // set input defaults
        for (InputMetadata inputMd : taskExecutionMetadata.getTaskMetadata()
            .getInputMetadata()) {
            IEnvironment env = inputEnvironment;
            if (inputMd.getEnvironment().equals(ParameterEnvironment.SESSION)
                && sessionEnvironment != null) {
                env = sessionEnvironment;
            }
            if (!env.hasVariable(inputMd.getName())) {
                // log.debug("setting default: " + env.getId()
                // + " " + inputMd.getName() + " " + inputMd.getDefaultValue()
                // );
                env.setVariable(inputMd, inputMd.getDefaultValue());
            }
            if (runOnInitActionValue == null && Action.isActionInput(inputMd)) {
                actionInput = inputMd;
                runOnInitActionValue = Action.getRunOnInitActionValue(inputMd);
            }
        }
        envMan.setEnvironment(
            inputEnvironment);

        if (taskMetadata.isRunOnInit()) {
            Serializable oldActionValue = null;
            if (actionInput != null) {
                oldActionValue = inputEnvironment.getVariableValue(
                    actionInput.getName());
                inputEnvironment.setVariable(actionInput, runOnInitActionValue);
            }
            run(false);
            if (actionInput != null) {
                inputEnvironment.setVariable(actionInput, oldActionValue);
            }
        }
    }

    // if it turns out that things other than jobs can instantiate tasks
    // then maybe move this to TaskMetadata
    private ITask instantiateTask(final TaskMetadata taskMetadata)
    throws InstantiationException, IllegalAccessException {
        return (ITask) taskMetadata
            .getTaskImplementationClass()
            .newInstance();
    }

    // @Asynchronous
    @SneakyThrows
    public TaskExecutionMetadata runInBackground() {
        // make sure the status is set to busy before we return
        taskExecutionMetadata.setStatus(TaskExecutionStatus.BUSY);
        // truly run in the background, had some concurrency issues with
        // @Asynchronous
        executor.submit(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                run();
                return null;
            }
        });
        return taskExecutionMetadata;

        // // XXX: Maybe this should be called by a Session Bean:
        // // every transaction runs in a dedicated Session Bean instance
        // // and thread.
        // thread = new Thread(this, "TaskExecution: " + taskExecutionMetadata.getJobId());
        // thread.setDaemon(true);
        // thread.setPriority(Thread.NORM_PRIORITY - 1);
        // thread.start();

    }

    public void run() {
        run(true);
    }

    private void run(boolean requireAllInputs) {
        // make sure we have an updated copy of our environments
        inputEnvironment =
            environmentManager.getEnvironment(inputEnvironment.getId());

        taskExecutionMetadata.setStartTime(new Date());
        taskExecutionMetadata.setStopTime(null);
        taskExecutionMetadata.setMessage("");
        taskExecutionMetadata.setProgress(0);
        taskExecutionMetadata.setStatus(TaskExecutionStatus.BUSY);

        try {
            processInputs(environmentManager, session, requireAllInputs);
            task.setTaskMetadata(taskExecutionMetadata.getTaskMetadata());
            task.setInputEnv(inputEnvironment);
            task.setOutputEnv(outputEnvironment);
            task.setLogger(log);
            task.setTaskExecutionMetadata(taskExecutionMetadata);
            task.run();
            processOutputs(environmentManager, session);
            taskExecutionMetadata.setStatus(TaskExecutionStatus.SUCCESS);
            // log.debug("TaskExecution done");
            taskExecutionMetadata.setProgress(1);
        } catch (InterruptedException e) {
            taskExecutionMetadata.setStatus(TaskExecutionStatus.INTERRUPTED);
            String msg = "TaskExecution was interrupted.";
            taskExecutionMetadata.setMessage(msg);
            log.warn(msg, e);
        } catch (Throwable e) {
            taskExecutionMetadata.setStatus(TaskExecutionStatus.FAIL);
            log.error("Task failed", e);
            taskExecutionMetadata.setMessage(e.getLocalizedMessage());
        } finally {
            taskExecutionMetadata.setStopTime(new Date());
            taskExecutorManager.taskExecutionDone(
                taskExecutionMetadata.getId());
        }
    }

    private void processInputs(final IEnvironmentManager envManager,
        final SessionMetadata session, boolean requireAllInputs) {
        // validate that all non-optional input parameters are specified
        // and insert them into the environment
        for (InputMetadata inputMD : taskExecutionMetadata.getTaskMetadata()
            .getInputMetadata()) {
            String varName = inputMD.getName();

            // copy stuff from the session where needed
            if (session != null && inputMD.getEnvironment().equals(
                ParameterEnvironment.SESSION)) {
                String envId = session.getEnvironmentId(inputMD.getNamespace());
                if (envManager.hasVariable(envId, varName)) {
                    inputEnvironment.setVariableValue(varName,
                        envManager.getVariableValue(envId,
                            varName));
                }
            }

            if (requireAllInputs && !inputEnvironment.hasVariable(varName)) {
                log.error("Missing input parameter: " + varName);
                taskExecutionMetadata.setStatus(TaskExecutionStatus.FAIL);
                return;
            }

            inputEnvironment.setVariableMetadata(inputMD);
        }

        // save the final environment used for this job
        envManager.setEnvironment(inputEnvironment);
    }

    private void processOutputs(final IEnvironmentManager envManager,
        final SessionMetadata session) throws ValidationException {
        for (OutputMetadata outputMD : taskExecutionMetadata.getTaskMetadata()
            .getOutputMetadata()) {
            String varName = outputMD.getName();
            EnvVarItem item = outputEnvironment.getVariable(varName);
            if (item == null) {
                log.error("Missing output parameter: " + varName);
                taskExecutionMetadata.setStatus(TaskExecutionStatus.FAIL);
                return;
            }
            outputEnvironment.setVariableMetadata(outputMD);
            Serializable value = item.getValue();
            // add resources to the environment as needed
            if (value instanceof IDataResource) {
                ((IDataResource) value).addToEnvironmentIfNeeded(
                    outputEnvironment);
            }

            // copy stuff to the session where needed
            if (session != null && outputMD.getEnvironment().equals(
                ParameterEnvironment.SESSION)) {
                String envId = session.getEnvironmentId(
                    outputMD.getNamespace());
                envManager.setVariable(envId, outputMD,
                    outputEnvironment.getVariableValue(varName));
            }
        }

        envManager.setEnvironment(outputEnvironment);

        if (session != null) {
            // hopefully saving the session after every
            // job run is not too much overhead?!
            // I think we will only have problems once we
            // have monte-carlo nested jobs
            try {
                sessionManager.saveSession(session.getId());
            } catch (ServiceException e) {
                log.error(
                    "Could not save session environment after running job.", e);
            }
        }
    }

    // public void waitUntilDone() throws InterruptedException {
    // thread.wait();
    // }

    public Serializable getInputValue(final String variableName) {
        return inputEnvironment.getVariableValue(variableName);
    }

    public String getLog() {
        // TODO: figure out how to read the log
        return "";
    }

    /**
     * get rid of this bean..
     */
    @Remove
    public void dispose() {
    }
}
