package za.co.enerweb.ebr.renderer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.annotations.EnvVarRenderer;
import za.co.enerweb.ebr.env.EnvVarMetadata;
import za.co.enerweb.ebr.exceptions.InvalidEnvVarRendererException;
import za.co.enerweb.ebr.exceptions.UnsupportedEnvVarRendererException;
import za.co.enerweb.ebr.server.Registrar;
import za.co.enerweb.toolbox.timing.Stopwatch;

import com.google.common.collect.UnFinalHashMultimap;

/**
 * One stop shop to advertise and find renderers.
 * XXX: this should be done in the client?! Then we can split up
 * our plugins into the ones that go into the orchestration engine
 * and those that goes in the client.
 * I don't think this should be an EJB because its a client thing..
 */
@Slf4j
public final class EnvVarRendererRegistryService {
    public static final int MAX_RANK = 100;
    public static final int DEFAULT_RANK = 50;
    public static final int MIN_RANK = 0;

    private EnvVarRendererRegistryService() {
    }

    /**
     * Maps types to renderers.
     * (the same type may have multiple renderers registered for it)
     */
    private static class TypeToRendererMap extends
        UnFinalHashMultimap<String, EnvVarRendererMetadata> {

        private static final long serialVersionUID = 1L;
    }

    /**
     * Maps technologies to renderers.
     */
    private static class TechToRenderers extends
        HashMap<String, Map<String, Class<?>>> {
        private static final long serialVersionUID = 1L;
    }

    /**
     * Maps technologies to types.
     */
    private static class TechToType
        extends
        HashMap<String, TypeToRendererMap> {
        private static final long serialVersionUID = 1L;
    }

    /**
     * List of Multimaps.
     */
    private static class TypeToRendererMaps extends
        ArrayList<TypeToRendererMap> {
        private static final long serialVersionUID = 1L;
    }

    /**
     * For each technology there is a map of renderer.id -> renderer.class
     */
    private static TechToRenderers techToRenderers = new TechToRenderers();

    /**
     * For each technology there is a map of types->renderers
     */
    private static TechToType techToType = new TechToType();

    public static void clearRegistry() {
        techToRenderers.clear();
        techToType.clear();
    }

    private static void couldNotInstantiateRenderer(final Exception e) {
        log.error("Could not instantiate EnvVarRenderer", e);
    }

    /**
     * @param envVarMetaData
     * @return
     * @throws UnsupportedEnvVarRendererException
     */
    public static Object getRenderer(
        final List<String> supportedTechs,
        final EnvVarMetadata envVarMetaData, final boolean editable)
        throws UnsupportedEnvVarRendererException {
        Class<?> rendererClass = null;
        String rendererId = envVarMetaData.getRenderer();
        if (rendererId != null) {
            // find the first matching id, traversing the technologies
            // in the order given.
            for (String rendererTechnology : supportedTechs) {
                try {
                    rendererClass =
                        techToRenderers.get(rendererTechnology).get(
                            rendererId);
                    if (rendererClass != null) {
                        return rendererClass.newInstance();
                    }
                } catch (InstantiationException e) {
                    couldNotInstantiateRenderer(e);
                } catch (IllegalAccessException e) {
                    couldNotInstantiateRenderer(e);
                }
            }
            logUnknownRendererId(supportedTechs, envVarMetaData, rendererId);
        }
        try {
            TypeToRendererMaps typeToRendererMaps =
                getTypeToRendererMaps(supportedTechs);
            List<EnvVarRendererMetadata> eledgebleRenderers;
            try {
                eledgebleRenderers =
                    lookupRenderers(typeToRendererMaps, envVarMetaData
                        .getTypeClass());
            } catch (ClassNotFoundException e) {
                throw new UnsupportedEnvVarRendererException(e);
            }
            List<EnvVarRendererMetadata> wrongEditabiltyRenderers =
                new ArrayList<EnvVarRendererMetadata>();
            // move inappropriate editable ones to the back
            for (Iterator<EnvVarRendererMetadata> iterator =
                eledgebleRenderers.iterator(); iterator.hasNext();) {
                EnvVarRendererMetadata envVarRendererMetadata =
                    iterator.next();
                if ((editable && !envVarRendererMetadata.isSupportsEditing())
                    || (!editable
                    && !envVarRendererMetadata.isSupportsViewing())) {
                    iterator.remove();
                    wrongEditabiltyRenderers.add(envVarRendererMetadata);
                }
            }
            if (!eledgebleRenderers.isEmpty()) {
                if (eledgebleRenderers.size() > 1) {
                    // sort from lowest to highest rank
                    Collections.sort(eledgebleRenderers);

                }
                // use the one with the highest rank
                return eledgebleRenderers.get(eledgebleRenderers.size() - 1)
                    .getRendererClass().newInstance();
            }
            // XXX: maybe do this in getRenderers() by subtracting 100
            // from renderers with the wrong editablility
            if (!wrongEditabiltyRenderers.isEmpty()) {
                if (wrongEditabiltyRenderers.size() > 1) {
                    Collections.sort(wrongEditabiltyRenderers);
                }
                return wrongEditabiltyRenderers.get(
                    wrongEditabiltyRenderers.size() - 1).getRendererClass()
                    .newInstance();
            }
        } catch (InstantiationException e) {
            couldNotInstantiateRenderer(e);
        } catch (IllegalAccessException e) {
            couldNotInstantiateRenderer(e);
        }
        String msg = "No output renderer registered for output of type: ";
        log.info(msg + " : " + envVarMetaData.getTypeName());
        throw new UnsupportedEnvVarRendererException(msg
            + envVarMetaData.getTypeName());
    }

    /**
     * from a collection supported technologies, obtain which renderers are
     * applicable if any.
     * @param typeToRendererMaps
     * @param valueType
     * @return
     */
    private static List<EnvVarRendererMetadata> getRenderers(
        final TypeToRendererMaps typeToRendererMaps, final String valueType) {
        List<EnvVarRendererMetadata> rendererIterators =
            new ArrayList<EnvVarRendererMetadata>();
        for (TypeToRendererMap typeToRendererMap : typeToRendererMaps) {
            rendererIterators.addAll(typeToRendererMap.get(valueType));
        }
        return rendererIterators;
    }

    /**
     * from the technology we can basically get a list of supported value
     * types.
     * @param technologies
     * @return
     */
    public static TypeToRendererMaps getTypeToRendererMaps(
        final Collection<String> technologies) {

        TypeToRendererMaps ret = new TypeToRendererMaps();
        for (String rendererTechnology : technologies) {
            TypeToRendererMap t2rMap =
                techToType.get(rendererTechnology);
            if (t2rMap != null) {
                ret.add(t2rMap);
            }
        }
        return ret;
    }

    private static void logUnknownRendererId(
        final List<String> supportedTechs,
        final EnvVarMetadata envVarMetaData, final String rendererId)
        throws UnsupportedEnvVarRendererException {
        TypeToRendererMaps typeToRendererMaps =
            getTypeToRendererMaps(supportedTechs);
        List<EnvVarRendererMetadata> eledgebleRenderers;
        try {
            eledgebleRenderers =
                lookupRenderers(typeToRendererMaps, envVarMetaData
                    .getTypeClass());
        } catch (ClassNotFoundException e) {
            throw new UnsupportedEnvVarRendererException(e);
        }
        StringBuilder sb = new StringBuilder("Valid renderer types are: ");
        for (Iterator<EnvVarRendererMetadata> iterator =
            eledgebleRenderers.iterator(); iterator.hasNext();) {
            EnvVarRendererMetadata envVarRendererMetadata = iterator.next();
            sb.append(envVarRendererMetadata.getId());
            if (iterator.hasNext()) {
                sb.append(", ");
            }
        }
        log.error("Unknown output renderer: {}\n{}", rendererId, sb
            .toString());
    }

    /**
     * see if type or any of its super classes or implemented interfaces are
     * registered.
     * @param type
     * @return
     */
    private static List<EnvVarRendererMetadata> lookupRenderers(
        final TypeToRendererMaps typeToRendererMaps, final Class<?> type) {
        if (type != null) {
            List<EnvVarRendererMetadata> ret =
                getRenderers(typeToRendererMaps, type.getName());

            // XXX: we should probably substract 5-10 rank points for
            // each level we go up the inheritance tree

            // also check if a super type is registered
            ret.addAll(lookupRenderers(typeToRendererMaps, type
                .getSuperclass()));

            // also check if the implemented interfaces are registered
            for (Class<?> implementedInterface : type.getInterfaces()) {
                ret.addAll(lookupRenderers(typeToRendererMaps,
                    implementedInterface));
            }
            return ret;
        }
        return Collections.emptyList();
    }

    public static void refreshRegistry() {
        // log.debug("Refreshing Renderer Registry");
        clearRegistry();
        Stopwatch sw = new Stopwatch();

        // find all classes annotated by @EnvVarRenderer
        Set<Class<?>> annotated =
            Registrar.getReflections().getTypesAnnotatedWith(
                EnvVarRenderer.class);
        // if (annotated.size() == 0) {
        // log.debug("Could not find any @EnvVarRenderers.");
        // } else {
        // log.debug("Found " + annotated.size() + " @EnvVarRenderers");
        // }
        int count = 0;
        for (Class<?> annotatedClass : annotated) {
            // this does not always work because of classloader issue
            EnvVarRenderer envVarRenderer =
                annotatedClass.getAnnotation(EnvVarRenderer.class);
            if (envVarRenderer != null) {
                // if (Serializable.class.isAssignableFrom(annotated_class))
                // {
                try {
                    registerRenderer(envVarRenderer.id(), annotatedClass,
                        envVarRenderer.technology(), envVarRenderer
                            .supportedTypes(), envVarRenderer.rankPerType(),
                        envVarRenderer.supports());
                    count++;
                } catch (InvalidEnvVarRendererException e) {
                    log.error(e.getLocalizedMessage());
                }
                /*
                 * } else {
                 * log.warn("@EnvVarRenderer annotation found on " +
                 * "a class that does not implement Serializable, " +
                 * "ignoring it:\n" + annotated_class); }
                 */
            } else {
                log.error("Could not load envVarRenderer "
                    + annotatedClass.getSimpleName()
                    + " probably because its been loaded by "
                    + "the wrong classloader.");
            }
        }
        log.info("Registering {} renderer(s) took {} ms", count,
            sw.getTotalMilliSeconds());
    }

    public static void registerRenderer(final String id,
        final Class<?> rendererClass, final String technology,
        final String[] supportedTypes, final byte[] rankPerType,
        final RendererMode rendererMode)
        throws InvalidEnvVarRendererException {
        // log.debug("Registering renderer: " + rendererClass
        // + "\nFor types:\n  " + Joiner.on("\n  ").join(supportedTypes));
        if (supportedTypes.length < 1) {
            throw new InvalidEnvVarRendererException(
                "A renderer needs to support at least one type.");
        }
        boolean hasRanks = rankPerType.length != 0;
        if (hasRanks && supportedTypes.length != rankPerType.length) {
            throw new InvalidEnvVarRendererException(
                "A renderer must have the same number of rankPerType as the "
                    + "number of supportedTypes is has.");
        }
        TypeToRendererMap typeToRendererMap =
            techToType.get(technology);
        if (typeToRendererMap == null) {
            typeToRendererMap = new TypeToRendererMap();
            techToType.put(technology, typeToRendererMap);
        }
        Map<String, Class<?>> idToRendererMap =
            techToRenderers.get(technology);
        if (idToRendererMap == null) {
            idToRendererMap = new HashMap<String, Class<?>>();
            techToRenderers.put(technology, idToRendererMap);
        }

        Class<?> existingRenderer = idToRendererMap.put(id, rendererClass);
        if (existingRenderer != null) {
            throw new InvalidEnvVarRendererException(
                "Two renderers are using the same id, please change one:\n  "
                    + existingRenderer + "\n  " + rendererClass + "\n  "
                    + id);
        }
        for (int i = 0; i < supportedTypes.length; i++) {
            String supportedType = supportedTypes[i];
            byte rank;
            if (hasRanks) {
                rank = rankPerType[i];
            } else {
                rank = DEFAULT_RANK;
            }

            EnvVarRendererMetadata rendererMD =
                new EnvVarRendererMetadata(id, rendererClass, rank,
                    rendererMode);
            typeToRendererMap.put(supportedType, rendererMD);
        }
    }
}
