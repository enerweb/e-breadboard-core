package za.co.enerweb.ebr.task;

import java.beans.PropertyDescriptor;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Map;

import lombok.Cleanup;

import org.ini4j.Config;
import org.ini4j.Ini;
import org.ini4j.Profile.Section;

import za.co.enerweb.toolbox.reflection.PropertyUtils;
import za.co.enerweb.toolbox.string.StringUtils;
import za.co.enerweb.toolbox.validation.ValidationException;

public class JavaTaskParser extends ATaskParser {
    private static final String TASK_PROPERTY_CLASS = "Class";
    private static final String VARIABLE_NAME_CLASS = TASK_PROPERTY_CLASS;
    public static final String NAME = "java";
    private Ini ini;
    private Section defaultSection;
    private String className;
    private Class<?> taskClass;

    public JavaTaskParser() {
        super(
            NAME, new String[] {"ini"});

    }

    @Override
    protected Class<?> startParseTaskMetaDataFromFile(String taskRelPath,
        String taskAbsPath) throws Exception {
        ini = new Ini();
        @Cleanup InputStream inputStream = new FileInputStream(taskAbsPath);
        Config config = new Config();
        config.setGlobalSection(true);
        ini.setConfig(config);
        ini.load(inputStream);

        defaultSection = ini.get(config.getGlobalSectionName());
        className = defaultSection.get(VARIABLE_NAME_CLASS);
        taskClass = Class.forName(className);

        // default taskId to the class name
        if (defaultSection.get(VARIABLE_NAME_TASK_ID) == null) {
            defaultSection.add(VARIABLE_NAME_TASK_ID, className);
        }

        // default taskTitle to the class name
        if (defaultSection.get(VARIABLE_NAME_TASK_TITLE) == null) {
            defaultSection.add(VARIABLE_NAME_TASK_TITLE,
                StringUtils.variableName2Title(
                    taskClass.getSimpleName()));
        }

        return taskClass;
    }

    @Override
    protected TaskMetadata parseTaskMetaData(String taskRelPath, Class<?> type)
        throws Exception {
        TaskMetadata ret = super.parseTaskMetaData(taskRelPath, type);
        ret.getAuxProperties().put(TASK_PROPERTY_CLASS, className);
        return ret;
    }

    @Override
    protected void parseInputMetaData(TaskMetadata tmd) throws Exception {
        parseIoMetaData(tmd, VARIABLE_NAME_TASK_INPUTS, new MapCallBack() {
            @Override
            public void callback(final TaskMetadata tmd,
                @SuppressWarnings("rawtypes") final Map taskInput)
                throws Exception {
                parseInputMetaData(tmd, taskInput);
            }
        }, true, false);
    }

    @Override
    protected void parseOutputMetaData(TaskMetadata tmd) throws Exception {
        parseIoMetaData(tmd, VARIABLE_NAME_TASK_OUTPUTS, new MapCallBack() {
            @Override
            public void callback(final TaskMetadata tmd,
                @SuppressWarnings("rawtypes") final Map taskInput)
                throws Exception {
                parseOutputMetaData(tmd, taskInput);
            }
        }, false, true);
    }

    protected void parseIoMetaData(TaskMetadata tmd, String variableName,
        MapCallBack mapCallBack,
        boolean validateWrite, boolean validateRead)
        throws Exception {
        Section inputs = ini.get(variableName);
        if (inputs != null) {
            String[] fieldNames = inputs.childrenNames();
            for (int i = 0; i < fieldNames.length; i++) {
                String fieldName = fieldNames[i];
                Section inputSection = inputs.getChild(fieldName);
                inputSection.put(NAME_ATTRIBUTE, fieldName);

                PropertyDescriptor descriptor;
                try {
                    descriptor = PropertyUtils.getPropertyDescriptor(taskClass,
                        fieldName);
                } catch (Exception e) {
                    throw new ValidationException("Class "
                        + taskClass.getName()
                        + " doesn't hava property: " + fieldName, e);
                }

                validateMethod(validateRead, fieldName,
                    descriptor.getReadMethod());
                validateMethod(validateWrite, fieldName,
                    descriptor.getWriteMethod());
                inputSection.put(TYPE_ATTRIBUTE,
                    descriptor.getPropertyType().getName());
                mapCallBack.callback(tmd, inputSection);
                // super.parseInputMetaData(tmd, inputSection);
            }
        }
    }

    public void validateMethod(boolean validate, String fieldName,
        Method method) {
        if (validate && method == null) {
            throw new ValidationException("Class " + taskClass
                + " doesn't hava a setter for property: " + fieldName);
        }
    }

    @Override
    public Object readNativeVariable(String variableName) {
        return defaultSection.get(variableName);
    }

    @Override
    public Object nativeToSimpleStructure(Object nativeObject) {
        return nativeObject;
    }

    @Override
    public void writeSimpleStructure(String envId, String variableName,
        Object simpleStructure) {
        // defaultSection
    }

    // protected final Serializable convertToProperJavaObject(
    // final String nativeType, final ParameterMetadata parameterMetadata,
    // final Object simpleStructure) throws Exception {
    // return dataTypeRegistryService
    // .lookupConverter(getName(), nativeType).fromSimpleStructure(
    // null, parameterMetadata.getName(), simpleStructure);
    // }
}
