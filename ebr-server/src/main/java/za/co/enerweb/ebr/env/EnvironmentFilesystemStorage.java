package za.co.enerweb.ebr.env;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import lombok.Cleanup;
import lombok.val;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import za.co.enerweb.ebr.datatype.dataresource.EnvFileDataSource;
import za.co.enerweb.ebr.datatype.dataresource.IDataSource;
import za.co.enerweb.ebr.exceptions.ServiceException;
import za.co.enerweb.ebr.server.HomeFile;
import za.co.enerweb.ebr.server.HomeFileCategory;
import za.co.enerweb.ebr.server.HomeFileSpec;
import za.co.enerweb.ebr.server.IHomeService;
import za.co.enerweb.ebr.session.SessionMetadata;
import za.co.enerweb.toolbox.timing.Stopwatch;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;
import com.thoughtworks.xstream.converters.ConversionException;

/**
 * Environment storage implementation that uses the file system.
 */
@Slf4j
public class EnvironmentFilesystemStorage implements IEnvironmentStorage {
    private static final String VARIABLE_FILE = "variables_xstream_v1.xml";
    private static final String SESSION_FILE = "_session_xstream_v2.xml";

    private XStream xstream = new XStream();

    private IHomeService homeService;

    public EnvironmentFilesystemStorage(IHomeService homeService) {
        this.homeService = homeService;
    }

    /*
     * (non-Javadoc)
     * @see za.co.enerweb.ebr.server.env.EnvironmentStorageI#deleteEnvironment(
     * java.lang.String)
     */
    @Override
    public void deleteEnvironment(final String envId) {
        FileUtils.deleteQuietly(getEnvDir(envId));
    }

    @Override
    public void deleteSession(final String sessionId) {
        getSessionFile(sessionId).delete();
    }

    private File getEnvDir(final String envId) {
        return getEnvDirHomeFile(envId).getOrCreateDir();
    }

    private HomeFile getEnvDirHomeFile(final String envId) {
        return homeService.getFile(
            new HomeFileSpec(HomeFileCategory.ENVIRONMENTS,
                envId));
    }

    private HomeFile getSessionFile(final String sessionId) {
        return homeService.getFile(
            new HomeFileSpec(HomeFileCategory.SESSIONS,
                sessionId + SESSION_FILE));
    }

    private File getVariableFile(final String envId) {
        return new File(getEnvDir(envId), VARIABLE_FILE);
    }

    @Override
    public Map<String, SessionMetadata> loadAllSession() {
        Stopwatch sw = new Stopwatch();
        Map<String, SessionMetadata> sessions =
            new HashMap<String, SessionMetadata>();

        for (HomeFile file : homeService.findFiles(
            new HomeFileSpec(HomeFileCategory.SESSIONS), "xml")) {
            if (file.getName().endsWith(SESSION_FILE)) {
                try {
                    @Cleanup val is = file.getInputStream();
                    SessionMetadata session =
                        (SessionMetadata) xstream.fromXML(is);
                    if (session != null) {
                        sessions.put(session.getId(), session);
                    }
                } catch (IOException e) {
                    throw new ServiceException("Session file not found", e);
                } catch (XStreamException e) {
                    log.error("Could not read session file, it may be "
                        + " corrupt\n or in the wrong format, or not even a "
                        + "session file :\n "
                        + file);
                    try {
                        homeService.deleteFile(file);
                    } catch (IOException e1) {
                        log.error("Could not delete session file: " + file, e);
                    }
                }
            }
        }
        log.debug("Took {} ms to read sessions.", sw.getTotalMilliSeconds());
        return sessions;
    }

    /*
     * (non-Javadoc)
     * @see za.co.enerweb.ebr.server.env.EnvironmentStorageI#loadAll(
     * java.lang.String)
     */
    @Override
    public Environment loadEnvironment(final String envId) {
        try {
            HomeFile envDirHomeFile = getEnvDirHomeFile(envId);
            if (envDirHomeFile.exists()) {
                @Cleanup FileReader fileReader =
                    new FileReader(getVariableFile(envId));
                Environment env = (Environment) xstream.fromXML(fileReader);
                // instantiate real handles to files again
                for (IDataSource dr : env.getResources()) {
                    if (dr instanceof EnvFileDataSource) {
                        ((EnvFileDataSource) dr)
                            .instantiateFile(envDirHomeFile.getDir());
                    }
                }
                return env;
            } else {
                throw new ServiceException("Environment dir not found: "
                    + envId);
            }
        } catch (ConversionException e) {
            throw new ServiceException("Could not parse environment " + ": "
                + envId);
        } catch (IOException e) {
            throw new ServiceException("Environment file not found", e);
        }
    }

    @Override
    public SessionMetadata loadSession(final String sessionId) {
        try {
            @Cleanup val is = getSessionFile(sessionId).getInputStream();
            SessionMetadata session =
                (SessionMetadata) xstream.fromXML(is);

            return session;
        } catch (IOException e) {
            throw new ServiceException("Environment file not found", e);
        }
    }

    /*
     * (non-Javadoc)
     * @see za.co.enerweb.ebr.server.env.EnvironmentStorageI#save(
     * java.lang.String, java.lang.String, java.io.Serializable)
     */
    @Override
    public void save(final String envId, final IEnvironment env) {
        try {
            // convert all resources to EnvFileDataSources
            for (final IDataSource dr : env.getResources()) {
                if (!(dr instanceof EnvFileDataSource)) {
                    EnvResourceMetadata rmd = dr.getResourceMetadata();
                    File envDir = getEnvDir(envId);
                    String fileName =
                        rmd.getFileBaseName() + dr.getResourceId() + "."
                            + rmd.getFileExtention();
                    File file = new File(envDir, fileName);
                    log.debug("writing file:" + file);
                    @Cleanup FileOutputStream fos = new FileOutputStream(file);
                    @Cleanup InputStream is = dr.asInputStream();
//                    try (FileOutputStream fos = new FileOutputStream(file);
//                        InputStream is = dr.asInputStream()) {
                        IOUtils.copy(is, fos);
//                    }
//                    fos.flush();
                    fos.close();
                    is.close();
                    EnvFileDataSource dr2 =
                        new EnvFileDataSource(envId, file, fileName);
                    dr2.setResourceMetadata(dr.getResourceMetadata());
                    dr2.setResourceId(dr.getResourceId()); // overwrite
                    // resource
                    env.putResource(dr2);
                }
            }
            @Cleanup
//            try(
                FileWriter fileWriter =
                new FileWriter(getVariableFile(envId));
            //) {
                xstream.toXML(env, fileWriter);
//            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new ServiceException(
                "Could not save environment variables", e);
        }
    }

    @Override
    public void save(final String sessionId, final SessionMetadata session) {
        try {
            HomeFile sessionFile = getSessionFile(sessionId);
            sessionFile.makeParentDirs();
            @Cleanup val os = sessionFile.getOutputStream();
            xstream.toXML(session, os);
        } catch (IOException e) {
            throw new ServiceException(
                "Could not save environment variables", e);
        }
    }
}
