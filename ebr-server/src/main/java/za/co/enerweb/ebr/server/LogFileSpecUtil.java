package za.co.enerweb.ebr.server;

import lombok.extern.slf4j.Slf4j;

import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;

import za.co.enerweb.ebr.server.LogFileSpec.LogLayout;
import za.co.enerweb.ebr.server.LogFileSpec.LogLevel;

/*
 * Done here because we don't want to depend on Level in the api
 */
@Slf4j
public abstract class LogFileSpecUtil {

    public static Level convert(LogLevel logLevel) {
        switch (logLevel) {
        case DEBUG:
            return Level.DEBUG;
        case INFO:
            return Level.INFO;
        case WARN:
            return Level.WARN;
        case ERROR:
            return Level.ERROR;
        }
        log.warn("Unknown log level: " + logLevel);
        return Level.ERROR;
    }

    public static Layout convert(LogLayout logLayout) {
        switch (logLayout) {
        case PLAIN:
            return new PatternLayout("%m");
        case LINE:
            return new PatternLayout("%m%n");
        case DETAIL:
            return new PatternLayout("%d:%p\t%m \t- %c%n");
        }
        log.warn("Unknown log layout: " + logLayout);
        return new PatternLayout("%d:%p\t%m \t- %c%n");
    }

}
