package za.co.enerweb.ebr.datatype;

import java.io.Serializable;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.exceptions.InvalidMetadataException;
import za.co.enerweb.ebr.server.Registrar;
import za.co.enerweb.toolbox.timing.Stopwatch;

/**
 * Manages a type and type-alias registry.
 * Aliases may be specific to a namespace, where the null namespace is the
 * global namespace.
 * NB. When it's running in a cluster, it will have a instance of this
 * in each node/vm.
 */
@Singleton
@Lock(LockType.READ)
@Slf4j
public class DataTypeRegistryService implements IDataTypeRegistryService {

    // This doesn't need to be static
    private final DefaultConverter defaultConverter =
        new DefaultConverter();

    private class AliasRegistry extends
        HashMap<String, Map<String, String>> {
        private static final long serialVersionUID = 1L;
    }

    /**
     * namespace => (alias => type)
     * (the null namespace is the global one)
     */
    private final AliasRegistry aliasRegistry = new AliasRegistry();

    /**
     * All the aliases which are deprecated, but still supported.
     * namespace => (alias => type)
     * (the null namespace is the global one)
     */
    private final AliasRegistry deprecatedAliasRegistry = new AliasRegistry();

    /**
     * typeAlias => converter
     */
    private final Map<String, ITypeConverter<Serializable>> converterRegistry =
        new HashMap<String, ITypeConverter<Serializable>>();

    @Lock(LockType.WRITE)
    public void clearAliasRegistry() {
        aliasRegistry.clear();
    }

    @Lock(LockType.WRITE)
    public void clearConverterRegistry() {
        converterRegistry.clear();
    }

    /**
     * @param typeAlias or typeName
     * @param namespace may be null
     * @return
     */
    public ITypeConverter<Serializable> lookupConverter(
        final String namespace, final String typeAlias) {
        ITypeConverter<Serializable> ret = null;
        // first see if there is a converter registered for the alias
        ret = converterRegistry.get(typeAlias);
        if (ret == null) {
            try {
                String type = lookupType(namespace, typeAlias);
                ret = converterRegistry.get(type);
            } catch (Exception e) {
                log.warn("Could not look up coverter for unknown alias, "
                    + "using default: " + typeAlias, e);
            }
            if (ret == null) {
                return defaultConverter;
            }
        }
        return ret;
    }

    /**
     * @param namespace may be null
     * @param alias
     * @return
     * @throws InvalidMetadataException if alias is not bound
     */
    public String lookupType(final String namespace,
        final String alias) {
        String ret = lookupTypeInNamespaceOrGlobal(
            aliasRegistry, namespace, alias);
        if (ret != null) {
            return ret;
        }

        // if we still can't find it validate that its a valid class name
        try {
            if (!DataTypeUtil.getTypeClass(alias).isPrimitive()) {
                return alias;
            }
        } catch (ClassNotFoundException e) {
            // logger.debug("Could not find alias", e);
        }

        // see if its a deprecated alias
        String correctAlias = lookupTypeInNamespaceOrGlobal(
            deprecatedAliasRegistry, namespace, alias);
        if (correctAlias != null) {
            log.warn("Type '{}' is depricated, please use '{}'.",
                alias, correctAlias);
            return lookupType(namespace, correctAlias);
        }

        throw new InvalidMetadataException(
            "Unbound alias or invalid class specified: " + alias);
    }

    /**
     * @param namespace
     * @param alias
     * @return type or null
     */
    private String lookupTypeInNamespace(
        final AliasRegistry registry,
        final String namespace, final String alias) {
        Map<String, String> aliasToType = registry.get(namespace);
        if (aliasToType != null) {
            return aliasToType.get(alias);
        }
        return null;
    }

    private String lookupTypeInNamespaceOrGlobal(
        final AliasRegistry registry, final String namespace,
        final String alias) {
        if (namespace != null) {
            String ret = lookupTypeInNamespace(registry, namespace, alias);
            if (ret != null) {
                return ret;
            }
        }

        // check global namespace
        String ret = lookupTypeInNamespace(registry, null, alias);
        if (ret != null) {
            return ret;
        }
        return null;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Lock(LockType.WRITE)
    public void refreshRegistry() {
        // log.debug("Refreshing DataType Registry");
        clearConverterRegistry();
        Stopwatch sw = new Stopwatch();

        // find all classes implementing ITypeConverter
        Set<Class<? extends ITypeConverter>> converters =
            Registrar.getReflections().getSubTypesOf(ITypeConverter.class);
        // if (converters.size() == 0) {
        // log.debug("Could not find any ITypeConverters.");
        // } else {
        // log.debug("Found " + converters.size() + " ITypeConverters");
        // }
        for (Class<? extends ITypeConverter> converterClass : converters) {
            // only consider non-abstract subclasses
            if (!Modifier.isAbstract(converterClass.getModifiers())) {
                try {
                    // log.debug("registering: " + converterClass);
                    ITypeConverter converter = converterClass.newInstance();
                    registerConverter(converter);
                } catch (IllegalAccessException e) {
                    log.warn("Not registering converter {} " +
                        "(because we don't have access)",
                        converterClass.getName());
                } catch (Exception e) {
                    log.error("Could not register a converter from: "
                        + converterClass.getName()
                        + " (Maybe it needs a no-parameter constructor)", e);
                }
            } else {
                log.debug(
                    "Ignoring abstract " + converterClass.getName());
            }
        }
        log.debug("Registering DataType converters took {} ms", sw
            .getTotalMilliSeconds());
    }

    /**
     * register alias globally
     * @param type
     * @param alias
     */
    @Lock(LockType.WRITE)
    public void registerAlias(final Class<?> type,
        final String alias) {
        registerAlias(type, null, alias);
    }

    /**
     * @param type
     * @param namespace may be null, then its registered globally
     * @param alias
     */
    @Lock(LockType.WRITE)
    public void registerAlias(final Class<?> type,
        final String namespace,
        final String alias) {
        registerAlias(type.getName(), namespace, alias);
    }

    /**
     * register alias globally
     * @param type
     * @param alias
     */
    @Lock(LockType.WRITE)
    public void registerAlias(final String type,
        final String alias) {
        registerAlias(aliasRegistry, type, null, alias);
    }

    /**
     * @param type
     * @param namespace may be null, then its registered globally
     * @param alias
     */
    @Lock(LockType.WRITE)
    public void registerAlias(final String type, final String namespace,
        final String alias) {
        registerAlias(aliasRegistry, type, namespace, alias);
    }

    /**
     * @param correctAlias
     * @param namespace may be null, then its registered globally
     * @param alias
     */
    @Lock(LockType.WRITE)
    public void registerDeprecatedAlias(final String correctAlias,
        final String namespace,
        final String alias) {
        registerAlias(deprecatedAliasRegistry, correctAlias, namespace, alias);
    }

    private void registerAlias(final AliasRegistry registry,
        final String type, final String namespace,
        final String alias) {
        // log.debug("registerAlias " + type + " " + namespace + " " +
        // alias);
        Map<String, String> aliasToType = registry.get(namespace);
        if (aliasToType == null) {
            aliasToType = new HashMap<String, String>();
            registry.put(namespace, aliasToType);
        }
        aliasToType.put(alias, type);
    }

    // TODO, why is this not automatic ?
    @Lock(LockType.WRITE)
    public void registerConverter(
        final ITypeConverter<Serializable> converter) {
        converterRegistry.put(converter.getType(), converter);
    }

    @Lock(LockType.WRITE)
    public void registerConverter(final String type,
        final ITypeConverter<Serializable> converter) {
        converterRegistry.put(type, converter);
    }
}
