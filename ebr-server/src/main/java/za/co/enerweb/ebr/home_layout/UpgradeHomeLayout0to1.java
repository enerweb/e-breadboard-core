package za.co.enerweb.ebr.home_layout;


import java.io.File;
import java.io.IOException;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.ArrayUtils;

import za.co.enerweb.ebr.server.HomeFile;
import za.co.enerweb.ebr.server.HomeFileCategory;
import za.co.enerweb.ebr.task.ATaskParser;
import za.co.enerweb.ebr.task.ITaskRegistry;

@Slf4j
public class UpgradeHomeLayout0to1 extends AbstractUpgradeHomeLayout {

    private ITaskRegistry taskRegistry;

    @Override
    protected void upgrade(ITaskRegistry taskRegistry) {
        this.taskRegistry = taskRegistry;
        moveTaskParserContent();
        moveSystemData();
        deleteUnsupportedDirs();
    }

    private void moveSystemData() {
        // don't move env to var/ because it is there in recent diases

        // move jobs to var/
        moveDir(HomeFileCategory.JOBS);

        // move sessions to var/
        moveDir(HomeFileCategory.SESSIONS);

        // move task_perspectives to bin/
        moveDir(HomeFileCategory.TASK_PERSPECTIVES);

        // move var/web-content to share/
        moveDir(HomeFileCategory.WEB_CONTENT);

        // move vaadin to share/
        moveDir(HomeFileCategory.FRONTEND, new String[]{HomeLayout0.VAADIN,
            HomeLayout0.TASK_LAYOUTS});
        moveDir(HomeFileCategory.FRONTEND, new String[]{HomeLayout0.VAADIN,
            HomeLayout0.STYLESHEETS});

        // move lib/javascript to share/vaadin/
        moveDir(HomeFileCategory.FRONTEND, new String[]{HomeLayout0.VAADIN,
            HomeLayout0.JAVASCRIPT});

        // move main resources
        moveDir(HomeFileCategory.RESOURCES);

        // pms/path_aliases.txt etc/pms_path_aliases.txt
        moveFile(HomeFileCategory.ETC, new String[]{
            HomeLayout0.PMS_PATH_ALIASES_TXT});
    }

    private void deleteUnsupportedDirs() {
        String[] knownRootDirs = newHomeLayout.getKnownRootDirs();
        for (File rootFile : this.home.listFiles()) {
            if (rootFile.isDirectory() &&
                !ArrayUtils.contains(knownRootDirs, rootFile.getName())) {
                try {
                    HomeFile trashFile = newHomeLayout.deleteHomeFile(
                        new HomeFile(rootFile, rootFile.getName()));
                    log.warn("Moved '{}' to the trash[1] because it is an "
                        + "unsupported $EBR_HOME root dir.\n[1] {}",
                        rootFile, trashFile);
                } catch (IOException e) {
                    log.warn(String.format("Could not move '%s' to the "
                        + "trash", rootFile), e);
                }
            }
        }
    }

    private void moveTaskParserContent() {
        for (ATaskParser taskParser : taskRegistry.getAllTaskParsers()) {
            String parserName = taskParser.getName();
            HomeFile parserDir = oldHomeLayout.getFile(parserName);
            if (parserDir.exists()) {
                String[] tech = new String[]{parserName};
                // move tasks
                moveDir(HomeFileCategory.TASKS_PER_TECHNOLOGY,tech);

                // move cache
                moveDir(HomeFileCategory.CACHE_PER_TECHNOLOGY,tech);

                // move lib
                moveDir(HomeFileCategory.LIB_PER_TECHNOLOGY,tech);

                // move task-common
                moveDir(HomeFileCategory.TASK_COMMON_PER_TECHNOLOGY,tech);

                // move the parser/resources
                moveDir(HomeFileCategory.RESOURCES_PER_TECHNOLOGY,tech);
                try {
                    log.info("Moving parser specific dir to the trash:\n"
                        + parserDir);
                    newHomeLayout.deleteHomeFile(parserDir);
                } catch (IOException e) {
                    log.error("Could not move parser specific dir to the "
                        + "trash:\n" + parserDir , e);
                }
            }
        }
    }

}
