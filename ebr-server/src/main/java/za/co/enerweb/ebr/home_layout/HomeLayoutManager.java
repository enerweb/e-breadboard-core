package za.co.enerweb.ebr.home_layout;

import java.io.File;
import java.io.IOException;

import lombok.extern.slf4j.Slf4j;

import org.ini4j.Ini;

import za.co.enerweb.ebr.task.ITaskRegistry;
import za.co.enerweb.toolbox.os.OsUtils;

/*
 * Manages all the home layouts :)
 * FIXME: We should not automatically when a new layout is first released
 * so that we can easier roll back if needed.
 * 1) Make it configurable
 * 2) change the default max-upgrade version when we do the next release.
 * XXX: Currently it looks for a .ebr_home.ini file in the home root,
 * but I suppose each layout implementation should check for itself if it
 * recognises the structure?!
 */
@Slf4j
public class HomeLayoutManager {
    private static final String PROPERTY_HOME_LAYOUT_VERSION = "version";

    private static final String SECTION__EBR_HOME_LAYOUT = "ebr_home_layout";

    private static final String EBR_HOME_INI = ".ebr_home.ini";

    @SuppressWarnings("unchecked")
    // http://stackoverflow.com/questions/749425/how-do-i-use-generics-with-an-array-of-classes
    private static final Class<? extends AbstractUpgradeHomeLayout>[]
        LAYOUT_UPGRADERS = new Class[] {UpgradeHomeLayout0to1.class};

    @SuppressWarnings("unchecked")
    static final Class<? extends AbstractHomeLayout>[] LAYOUTS =
        new Class[] {HomeLayout0.class, HomeLayout1.class};

    private ITaskRegistry taskRegistry;

    public HomeLayoutManager(ITaskRegistry taskRegistry) {
        this.taskRegistry = taskRegistry;
    }

    public static int getMaxLayoutVersion() {
        // FIXME: make this configurable
        // XXX: maybe let each layout decide if
        // it wants to be upgraded to another one.
        // maybe give the layout ids eg. (colocated0 and colocated1)?!
        // I'm thinking if we ever build a linux package then the content
        // can actually be split up into the real var, bin, etc dirs...
        //return LAYOUTS.length - 1;
        return 0;
    }

    public AbstractHomeLayout openEbrHome(File root) {
        if (!root.exists()) {
            throw new RuntimeException("Could not open non-existing EBR_HOME:" +
                root);
        }
        Integer layoutVersion = null;
        if (root.canWrite()) {
            // only try to upgrade writable homes
            try {
                layoutVersion = upgradeLayout(root);
            } catch (CouldNotUpgradeEbrHomeLayoutException e) {
                log.info(
                    "Could not upgrade writable EBR_HOME to the latest "
                        + "version: " + root, e);
            }
        }
        if (layoutVersion == null) {
            // avoid multiple detection
            layoutVersion = detectLayoutVersion(root);
        }
        return initLayout(root, layoutVersion);
    }

    AbstractHomeLayout initLayout(File root, int layoutVersion) {
        AbstractHomeLayout ret;
        try {
            ret = LAYOUTS[layoutVersion].newInstance();
            ret.setTaskRegistry(taskRegistry);
        } catch (Exception e) {
            throw new RuntimeException("Could not instantiate layout:" +
                LAYOUTS[layoutVersion], e);
        }
        ret.setRoot(root);
        return ret;
    }

    public int detectLayoutVersion(File writableHome) {
        // look for ${EBR_HOME}/var/ebr_installation.ini
        try {
            File installationFile = new File(writableHome,
                EBR_HOME_INI);
            if (installationFile.exists()) {
                Ini ini = new Ini(installationFile);
                return ini.get(SECTION__EBR_HOME_LAYOUT,
                    PROPERTY_HOME_LAYOUT_VERSION, Integer.class);
            }
        } catch (IOException e) {
            log.info("Could not detect the EBR_HOME layout version.", e);
        }
        return 0;
    }

    public int upgradeLayout(File writableHome)
        throws CouldNotUpgradeEbrHomeLayoutException {
        return upgradeLayout(writableHome, getMaxLayoutVersion());
    }

    public int upgradeLayout(File writableHome, int newVersion)
        throws CouldNotUpgradeEbrHomeLayoutException {
        int curLayoutVersion = detectLayoutVersion(writableHome);
        if (curLayoutVersion < newVersion) {
            if (writableHome.list().length == 0) {
                curLayoutVersion = newVersion;
            } else {
                while (curLayoutVersion < newVersion) {
                    log.warn(String.format(
                        "Upgrading your EBR_HOME layout from version %s to "
                            + "version %s:\n%s", curLayoutVersion,
                        curLayoutVersion + 1, writableHome.getAbsoluteFile()));
                    try {
                        LAYOUT_UPGRADERS[curLayoutVersion].newInstance()
                            .upgrade(taskRegistry, writableHome);
                        curLayoutVersion++;
                    } catch (Exception e) {
                        throw new CouldNotUpgradeEbrHomeLayoutException(
                            curLayoutVersion, e);
                    }
                }
            }
            // write out the new version
            writeVersion(writableHome, curLayoutVersion);
        }
        return curLayoutVersion;
    }

    public void writeVersion(File writableHome, int newVersion) {
        try {
            File installationFile = new File(writableHome,
                EBR_HOME_INI);
            Ini ini = new Ini();
            if (installationFile.exists()) {
                ini.load(installationFile);
            }
            ini.put(SECTION__EBR_HOME_LAYOUT,
                PROPERTY_HOME_LAYOUT_VERSION, newVersion);
            ini.setComment(
                "You should not change stuff in here if you do not know what "
                + "you are doing, because things will break and eat your cat.");
            ini.store(installationFile);
            hide(installationFile);
        } catch (IOException e) {
            log.info("Could save the EBR_HOME layout version.", e);
        }
    }

    // http://stackoverflow.com/questions/1294989/make-a-file-folder-hidden-on-windows-with-java
    private void hide(File src) {
        try {
            if (OsUtils.isWindows()) {
                Process p = Runtime.getRuntime().exec(
                    "attrib +h " + src.getPath());
                p.waitFor();
            }
        } catch (Exception e) {
            log.debug("Could not hide " + src.getAbsolutePath(), e);
        }
    }
}
