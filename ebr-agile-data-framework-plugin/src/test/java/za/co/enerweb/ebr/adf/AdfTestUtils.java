package za.co.enerweb.ebr.adf;

import za.co.enerweb.ebr.adf.dbl.DummyAdfDbLayer;
import za.co.enerweb.ebr.adf.dummy.DummyAdfFactory;
import za.co.enerweb.ebr.edbm.DbSpec;

public class AdfTestUtils {

    public static DbSpec getDbSpec() {
        return getAdfFactory().getDbSpec();
    }

    public static DummyAdfFactory getAdfFactory() {
        return DummyAdfFactory.getWorkInstance();
    }

    public static DummyAdfDbLayer getAdfDbLayer() {
        AdfFactory.resetCheckedDbs();
        return getAdfFactory().getDbLayer();
    }
}
