package za.co.enerweb.ebr.adf.dummy.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import za.co.enerweb.ebr.adf.entities.HierarchyRelationship;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true, of={})
@ToString(callSuper=true, of={})
@Entity
@Table(name = "H_DUMMY_HIERARCHY",
    uniqueConstraints = {
        @UniqueConstraint(columnNames = {"HIERARCHY_ID",
            "PARENT_TYPE_ID", "PARENT_ID", "CHILD_TYPE_ID", "CHILD_ID",
            "START_DATETIME", "END_DATETIME"})})
public class HDummyHierarchy extends HierarchyRelationship {
    public static final String DUMMY_HIER_KEY = "DUMMY_HIERARCHY";
    // deriveRelationhsipKey(
    // TTypeDummy1.class, TTypeDummy2.class);
    private static final long serialVersionUID = 1L;

}
