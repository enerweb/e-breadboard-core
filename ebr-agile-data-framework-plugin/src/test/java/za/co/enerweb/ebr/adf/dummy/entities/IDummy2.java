package za.co.enerweb.ebr.adf.dummy.entities;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import za.co.enerweb.ebr.adf.entities.IbInstance;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true, of={})
@ToString(callSuper=true, of={})
@Entity
@Table(name = "I_DUMMY2",
    uniqueConstraints = {
        @UniqueConstraint(columnNames = {"TYPEDEF_ID", "ID"})})
public class IDummy2 extends IbInstance {
    private static final long serialVersionUID = 1L;

    // input parameters
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "iDummy2")
    private Collection<IDummy2Par> iDummy2ParCollection;

    // output parameters
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "iDummy2")
    private Collection<IDummy2Out> iDummy2OutCollection;

    // relationships
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "iDummy2")
    private Collection<RDummy1Dummy2> rDummyInstance2Collection;

}
