package za.co.enerweb.ebr.adf.dbl;

import static junit.framework.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import za.co.enerweb.ebr.adf.model.AdfEntity;
import za.co.enerweb.ebr.adf.model.EntityType;

public class AdfEntityDefTest {

    public class EDummyEntity extends AdfEntity {

        @Override
        public EntityType getEntityType() {
            return null;
        }

    }

    public class FailDummyEntity extends AdfEntity {

        @Override
        public EntityType getEntityType() {
            return null;
        }

    }

    private EDummyEntity eDummyEntity;
    private FailDummyEntity failDummyEntity;

    @Before
    public void setup() {
        eDummyEntity = new EDummyEntity();
    }

    @Test
    public void testGetKey() {
        assertEquals("DUMMY_ENTITY", eDummyEntity.getKey());
    }

    @Test
    public void testGetCaption() {
        assertEquals("Dummy Entity", eDummyEntity.getCaption());
    }

    @Test(expected = IllegalStateException.class)
    public void testGetKeyFail() {
        failDummyEntity = new FailDummyEntity();
        failDummyEntity.getKey();
    }

    @Test(expected = IllegalStateException.class)
    public void testGetCaptionFail() {
        failDummyEntity = new FailDummyEntity();
        failDummyEntity.getCaption();
    }
}
