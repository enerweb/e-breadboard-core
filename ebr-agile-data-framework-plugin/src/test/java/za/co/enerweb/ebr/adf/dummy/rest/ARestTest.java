package za.co.enerweb.ebr.adf.dummy.rest;

import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.equalTo;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.adf.dbl.AAdfDbLayerWithDummiesTest;
import za.co.enerweb.ebr.server.AEbrTest;

import com.jayway.restassured.builder.ResponseSpecBuilder;
import com.jayway.restassured.specification.ResponseSpecification;

@Slf4j
public class ARestTest extends AEbrTest {
    public static final String CLASS__ADF_CONF__SLUG = "adf_configuration-1";
    public static final String CLASS__ADF_CONF__CAPTION = "Adf Configuration";
    public static final String CLASS__ADF_CONF__DESCRIPTION =
        "Adf Configuration";

    public static final String FIELD__TYPE_DUMMY__DUMMY_NUMBER__SLUG =
        "type_dummy2-5.dummy_number";
    public static final String FIELD__TYPE_DUMMY__DUMMY_NUMBER__CAPTION =
        "Dummy Number";
    public static final String FIELD__TYPE_DUMMY__DUMMY_NUMBER__DESCRIPTION =
        "";

    public static final String INSTANCE__DUMMY1_INSTANCE_A__SLUG =
        "type_dummy1-4.dummy_1_instance_a-1";

    public void setUpAfterInjection() throws Exception {
        super.setUpAfterInjection();
        AAdfDbLayerWithDummiesTest t = new AAdfDbLayerWithDummiesTest() {
        };
        t.setUpAfterInjection();
    }

    /**
     * checks meta at the given root
     * "meta":{"href":
     * "http://localhost:20302/dummyApp/<groupUriFragment>/<idFieldValue>",
     * "mediaType":"application/json;,application/xml;,"},
     */
    protected ResponseSpecification metaSpec(String rootPath,
        String expectedPathTail) {
        ResponseSpecBuilder builder = new ResponseSpecBuilder();
        builder.rootPath(rootPath);
        builder.expectBody("meta.href", endsWith(expectedPathTail));

        builder.expectBody("meta.mediaType",
            equalTo("application/json;,application/xml;,"));

        ResponseSpecification responseSpec = builder.build();
        return responseSpec;
    }

    protected ResponseSpecification noMetaSpec(String rootPath) {
        ResponseSpecBuilder builder = new ResponseSpecBuilder();
        // for some reason this doesn't work for xml :(
        if (isNotXml()) {
            // builder.rootPath(rootPath);
            // make sure there is no meta for this
            builder.expectBody(rootPath + "{ it.containsKey('meta') }",
                is(false));
        }
        ResponseSpecification responseSpec = builder.build();
        return responseSpec;
    }

}
