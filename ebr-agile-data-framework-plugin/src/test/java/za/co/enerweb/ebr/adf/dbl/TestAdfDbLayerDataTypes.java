package za.co.enerweb.ebr.adf.dbl;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import za.co.enerweb.ebr.adf.data_types.DtText;
import za.co.enerweb.ebr.adf.entities.CmDatatype;
import za.co.enerweb.ebr.adf.model.AdfDataType;

public class TestAdfDbLayerDataTypes extends AbstractAdfDbLayerTest {
    public void setUpAfterInjection() throws Exception {
        super.setUpAfterInjection();
    }

    @Test
    public void testDataTypeRegistration() {
        // make sure it is registered in the class registry
        AdfDataType<?> dt = dbl.getAdfDataType(DtText.class);
        assertTrue(dt instanceof DtText);
        dt = dbl.getAdfDataType(DtText.KEY);
        assertTrue(dt instanceof DtText);

        // make sure our registry in the db worked and can be read
        List<CmDatatype> allCmDatatypes = dbl.findAll(CmDatatype.class);
        CmDatatype cmTypedef = dbl.getCmDataType(DtText.class);
        assertTrue(allCmDatatypes.contains(cmTypedef));
    }

}
