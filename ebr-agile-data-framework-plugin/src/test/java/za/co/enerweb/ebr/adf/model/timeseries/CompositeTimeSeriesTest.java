package za.co.enerweb.ebr.adf.model.timeseries;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.Iterator;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

public class CompositeTimeSeriesTest {

    private static final int START_VALUE = 2101;
    private static final DateTime START = new DateTime(2101, 1, 1, 0,0,0 ,0);
    private CompositeTimeSeries ct;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        ct = new CompositeTimeSeries();
    }

    private void add2Timeserii(int rows) {
        SparseTimeSeries<Double> a = new SparseTimeSeries<Double>();
        SparseTimeSeries<Double> b = new SparseTimeSeries<Double>();

        // add some values
        double valueGenerator = START_VALUE;
        DateTime cur = START;
        a.put(cur, valueGenerator++);
        for (int i = 0; i < rows-2; i++) {
            cur = cur.plusHours(1);
            a.put(cur, valueGenerator);
            b.put(cur, valueGenerator++);
        }
        cur = cur.plusHours(1);
        b.put(cur, valueGenerator++);
        ct.add(a);
        ct.add(b);
    }

    @Test
    public void testIterateNoTimeSeries() {
        Iterator<ITimeSeriesItem<Object[]>> i = ct.sparseIterator();
        assertFalse(i.hasNext());
    }

    @Test
    public void testIterateTimeSeriesWithOneItem() {
        SparseTimeSeries<Double> a = new SparseTimeSeries<Double>();
        ct.add(a);
        Date onlyDate = new Date();
        double onlyValue = 123.456;
        a.put(onlyDate, onlyValue);
        Iterator<ITimeSeriesItem<Object[]>> i = ct.sparseIterator();
        assertTrue(i.hasNext());
        ITimeSeriesItem<Object[]> onlyItem = i.next();
        assertEquals(onlyDate, onlyItem.getDateTime());
        assertEquals(onlyValue, onlyItem.getValue()[0]);
        assertFalse(i.hasNext());
    }

    @Test
    public void testIterateEmptyTimeSerii() {
        SparseTimeSeries<Double> a = new SparseTimeSeries<Double>();
        ct.add(a);
        Iterator<ITimeSeriesItem<Object[]>> i = ct.sparseIterator();
        assertFalse(i.hasNext());
    }

    @Test
    public void testIterateOnlyEmptyTimeSerii() {
        SparseTimeSeries<Double> a = new SparseTimeSeries<Double>();
        ct.add(a);
        SparseTimeSeries<Double> b = new SparseTimeSeries<Double>();
        ct.add(b);
        Iterator<ITimeSeriesItem<Object[]>> i = ct.sparseIterator();
        assertFalse(i.hasNext());
    }

    @Test
    public void testIterate() {
        add2Timeserii(5);
        double valueGenerator = START_VALUE;
        Iterator<ITimeSeriesItem<Object[]>> itr = ct.sparseIterator();
        ITimeSeriesItem<Object[]> tsi = itr.next();
        assertEquals(tsi.getDateTime(), START.toDate());
        assertEquals(valueGenerator++, tsi.getValue()[0]);
        assertNull(tsi.getValue()[1]);
        for (int i = 0; i < 3; i++) {
            tsi = itr.next();
            assertEquals(valueGenerator, tsi.getValue()[0]);
            assertEquals(valueGenerator++, tsi.getValue()[1]);
        }
        tsi = itr.next();
        assertNull(tsi.getValue()[0]);
        assertEquals(valueGenerator++, tsi.getValue()[1]);

        int i = 0;
        itr = ct.sparseIterator();
        while (itr.hasNext()) {
            ITimeSeriesItem<java.lang.Object[]> tsi2 =
                (ITimeSeriesItem<java.lang.Object[]>) itr.next();
            System.out.print(i + " " + tsi2.getDateTime());
            Object[] values = tsi2.getValue();
            for (int j = 0; j < values.length; j++) {
                System.out.print(", " + values[j]);
            }
            System.out.println();
            i++;
        }
    }
}
