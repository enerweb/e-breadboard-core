package za.co.enerweb.ebr.adf.dbl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;

import org.apache.openejb.api.LocalClient;
import org.junit.Test;

import za.co.enerweb.ebr.adf.AdfFactory;
import za.co.enerweb.ebr.adf.dummy.DummyAdfFactory;
import za.co.enerweb.ebr.adf.entities.CmTypedef;

@LocalClient
public class TestExtendedAdfDbLayer extends AAdfDbLayerWithDummiesTest {

    public static final class DummyAdfFactoryExtension extends DummyAdfFactory {
        @Override
        public Class<? extends AdfDbLayer> getDefaultDbLayerClass() {
            return ExtendedAdfDbLayer.class;
        }
    }

    @Stateful
    @LocalBean
    public static class ExtendedAdfDbLayer extends DummyAdfDbLayer {
        @SuppressWarnings("unchecked")
        public <T> List<T> extendedFindAll(Class<T> klass) {
            return em.createQuery("SELECT x FROM "
                + klass.getSimpleName() + " x")
                .getResultList();
        }
    }

    private ExtendedAdfDbLayer dbl;

    public AdfFactory getAdfFactory() {
        return new DummyAdfFactoryExtension();
    }

    // @Override
    // protected DummyAdfDbLayer createAdfDbLayer() {
    // ExtendedAdfDbLayer extendedAdfDbLayer = dbl = EbrFactory.lookupEjb(
    // "adf-test-pu", ExtendedAdfDbLayer.class);
    // extendedAdfDbLayer.setAdfFactory(AdfTestUtils.getAdfFactory());
    // return extendedAdfDbLayer;
    // }

    public void setUpAfterInjection() throws Exception {
        super.setUpAfterInjection();
        dbl = (ExtendedAdfDbLayer) super.dbl;
    }

    /*
     * make sure inherited methods work
     */
    @Test
    public void testFindAllTypeDefs() {
        assertGotTypes(dbl.findAll(CmTypedef.class, "id"));
    }

    /*
     * make sure child methods work
     */
    @Test
    public void testExtendedFindAllTypeDefs() {
        assertGotTypes(dbl.extendedFindAll(CmTypedef.class));
    }
}
