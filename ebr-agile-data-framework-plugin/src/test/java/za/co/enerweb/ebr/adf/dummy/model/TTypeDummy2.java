package za.co.enerweb.ebr.adf.dummy.model;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import za.co.enerweb.ebr.adf.data_types.DtNumber;
import za.co.enerweb.ebr.adf.data_types.DtText;
import za.co.enerweb.ebr.adf.dummy.entities.HDummyHierarchy;
import za.co.enerweb.ebr.adf.model.AdfHierarchyParent;
import za.co.enerweb.ebr.adf.model.AdfInstance;
import za.co.enerweb.ebr.adf.model.AdfInstanceTypeDef;
import za.co.enerweb.ebr.adf.model.AdfParamDef;
import za.co.enerweb.ebr.adf.model.AdfRelationshipFrom;

/**
 * Custom key
 */
@Data
@EqualsAndHashCode(callSuper = true, of = { })
@ToString(callSuper = true, of = { })
@AdfInstanceTypeDef(entity = EDummy2.class, key = TTypeDummy2.KEY)
public class TTypeDummy2 extends AdfInstance {
    public static final String KEY = "TYPE_DUMMY2";

    @AdfParamDef(type = DtText.class)
    public String name;

    @AdfParamDef(type = DtNumber.class)
    public Double dummyNumber;

    @AdfRelationshipFrom(key = TTypeDummy1.R_DUMMY1_DUMMY2_KEY)
    private List<TTypeDummy1> typeDummy1;

    @AdfHierarchyParent(entity = EDummyHierarchy.class,
        key = HDummyHierarchy.DUMMY_HIER_KEY,
        uniqueFieldPerParent = "name")
    private TTypeDummy1 typeDummy1hier;
}
