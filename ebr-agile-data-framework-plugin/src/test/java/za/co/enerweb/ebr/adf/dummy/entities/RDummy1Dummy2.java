package za.co.enerweb.ebr.adf.dummy.entities;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import za.co.enerweb.ebr.adf.entities.CmReltypedef;
import za.co.enerweb.ebr.adf.entities.Relationship;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true, of={})
@ToString(callSuper=true, of={})
@Entity
@Table(name = "R_DUMMY1_DUMMY2")
public class RDummy1Dummy2 extends Relationship {
    private static final long serialVersionUID = 1L;

    @JoinColumn(name = "DUMMY1_ID", referencedColumnName = "ID",
        nullable = false)
    @ManyToOne(optional = false)
    private IDummy1 iDummy1;

    @JoinColumn(name = "DUMMY2_ID", referencedColumnName = "ID",
        nullable = false)
    @ManyToOne(optional = false)
    private IDummy2 iDummy2;

    @OneToMany(mappedBy = "rDummy1Dummy2")
    private Collection<RDummy1Dummy2Par>
        rDummy1Dummy2ParCollection;

    public RDummy1Dummy2(Integer id) {
        super(id);
    }

    public RDummy1Dummy2(Integer id, CmReltypedef cmReltypedef) {
        super(id, cmReltypedef);
    }

}
