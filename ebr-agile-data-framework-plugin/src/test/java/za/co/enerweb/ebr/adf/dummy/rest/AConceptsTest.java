package za.co.enerweb.ebr.adf.dummy.rest;

import static java.lang.String.format;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assume.assumeTrue;
import static za.co.enerweb.ebr.adf.dummy.rest.AConceptsTest.TestContentType.JSON;
import static za.co.enerweb.ebr.adf.dummy.rest.AConceptsTest.TestContentType.XML;
import lombok.extern.slf4j.Slf4j;

import org.junit.Test;

import com.jayway.restassured.response.Response;
import com.jayway.restassured.response.ValidatableResponse;
import com.jayway.restassured.specification.ResponseSpecification;

@Slf4j
public abstract class AConceptsTest extends ARestTest {

    protected enum TestContentType {
        JSON, XML;

        protected String getSingleItemRootPath(AConceptsTest test) {
            if (this.equals(XML)) {
                return test.itemUriFragment;
            }
            return "";
        }
        protected String getGroupRootPath(AConceptsTest test) {
            if (this.equals(XML)) {
                return test.groupUriFragment;
            }
            return "";
        }

        protected String getItemInGroupRootPath(AConceptsTest test) {
            if (this.equals(XML)) {
                return format("%s." +
                    "items.find { it."
                    + test.getIdField() + " == '%s'} ",
                    test.groupUriFragment,
                    test.idFieldValue);
            }
            return format(
                "items.find { "
                    // + "it.containsKey(\"" + test.getIdField()
                    // + "\") &&"
                    + " it."
                    + test.getIdField() + " == '%s'} ",
                test.idFieldValue);
        }
    };

    protected String groupUriFragment;
    protected String itemUriFragment;
    protected String idFieldValue;

    // protected String itemInGroupRootPath;
    // protected String groupRootPath;
    // protected String singleItemRootPath;

    public AConceptsTest(String groupUriFragment, String itemUriFragment,
        String idFieldValue) {
        this.groupUriFragment = groupUriFragment;
        this.itemUriFragment = itemUriFragment;
        this.idFieldValue = idFieldValue;
//        this.itemInGroupRootPath =
//            format("%s." +
//                "items.find { it."
//                + getIdField() + " == '%s'} ",
//                groupUriFragment,
//                idFieldValue);
//        // this.itemInGroupRootPath =
//        // format("%s.items.find{it.containsKey(\"" + getIdField()
//        // + "\") && it."
//        // + getIdField() + " == '%s'}",
//        // groupUriFragment, idFieldValue);
//        // this.groupRootPath = // "";//
//        // groupUriFragment;
//        // this.singleItemRootPath = // "";//
//        // itemUriFragment;
    }

    protected String getGroupRootPath(TestContentType t) {
        return groupUriFragment;
    }

    protected String getJsonGroupRootPath() {
        return "";
    }

    protected String getIdField() {
        return "id";
    }

    protected boolean runSingles() {
        return true;
    }

    // protected ResponseSpecification metaSpec() {
    // return metaSpec(itemInGroupRootPath, itemUriFragment, idFieldValue);
    // }

    protected ResponseSpecification groupMetaSpec(TestContentType t) {
        return metaSpec(t.getGroupRootPath(this), groupUriFragment);
    }

    protected ResponseSpecification itemInGroupMetaSpec(TestContentType t) {
        return metaSpec(t.getItemInGroupRootPath(this),
            join(itemUriFragment, idFieldValue));
    }

    protected ResponseSpecification singleItemMetaSpec(TestContentType t) {
        return metaSpec(t.getSingleItemRootPath(this), join(itemUriFragment, idFieldValue));
    }

    protected ValidatableResponse multiBody(Response r, TestContentType t) {
        return r.then().log().all()
            .spec(groupMetaSpec(t))
            .spec(itemInGroupMetaSpec(t))
            .root(t.getItemInGroupRootPath(this))
            .body(getIdField(), equalTo(idFieldValue));
    }

    protected ValidatableResponse getMulti() {
        return multiBody(json(givenApplicationUri()
            .get(groupUriFragment)), JSON);
    }

    protected ValidatableResponse getMultiXml() {
        return multiBody(xml(givenApplicationUri()
            .get(groupUriFragment + ".xml")), XML);
    }

    protected ValidatableResponse getMultiJson() {
        return multiBody(json(givenApplicationUri()
            .get(groupUriFragment + ".json")), JSON);
    }

    protected ValidatableResponse singleBody(Response r, TestContentType t) {
        return r.then().log().all()
            .spec(singleItemMetaSpec(t))
            .root(t.getSingleItemRootPath(this))
            .body(getIdField(), equalTo(idFieldValue));
    }

    protected ValidatableResponse getSingle() {
        return singleBody(json(givenApplicationUri()
            .get(join(itemUriFragment, idFieldValue))), JSON);
    }

    protected ValidatableResponse getSingleXml() {
        return singleBody(xml(givenApplicationUri()
            .get(join(itemUriFragment, idFieldValue + ".xml"))), XML);
    }

    protected ValidatableResponse getSingleJson() {
        return singleBody(json(givenApplicationUri()
            .get(join(itemUriFragment, idFieldValue + ".json"))), JSON);
    }

    protected abstract void testGet(ValidatableResponse r);

    @Test
    public void testGetMulti() throws Exception {
        testGet(getMulti());
    }

    @Test
    public void testGetMultiXml() throws Exception {
        testGet(getMultiXml());
    }

    @Test
    public void testGetMultiJson() throws Exception {
        testGet(getMultiJson());
    }

    @Test
    public void testGetSingle() throws Exception {
        assumeTrue(runSingles());
        testGet(getSingle());
    }

    @Test
    public void testGetSingleXml() throws Exception {
        assumeTrue(runSingles());
        testGet(getSingleXml());
    }

    @Test
    public void testGetSingleJson() throws Exception {
        assumeTrue(runSingles());
        testGet(getSingleJson());
    }

}
