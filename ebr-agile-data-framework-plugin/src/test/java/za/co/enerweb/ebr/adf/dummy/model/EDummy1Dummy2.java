package za.co.enerweb.ebr.adf.dummy.model;

import za.co.enerweb.ebr.adf.model.AdfInstanceEntity;
import za.co.enerweb.ebr.adf.model.AdfRelationshipEntity;

public class EDummy1Dummy2 extends AdfRelationshipEntity {
    public static final String KEY = deriveKey(EDummy1Dummy2.class);

    @Override
    public Class<? extends AdfInstanceEntity> getFromInstanceEntity() {
        return EDummy1.class;
    }

    @Override
    public Class<? extends AdfInstanceEntity> getToInstanceEntity() {
        return EDummy2.class;
    }
}
