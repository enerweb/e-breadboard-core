package za.co.enerweb.ebr.adf;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;

import za.co.enerweb.ebr.adf.dbl.AbstractAdfDbLayerTest;
import za.co.enerweb.ebr.adf.dummy.DummyAdfFactory;
import za.co.enerweb.ebr.adf.dummy.entities.IDummy1;
import za.co.enerweb.ebr.adf.dummy.entities.IDummy2Out;
import za.co.enerweb.ebr.adf.entities.CmEntitytype;
import za.co.enerweb.ebr.adf.entities.CmEnum;

public class AdfFactoryTest extends AbstractAdfDbLayerTest {

    /*
     * Test that we can load common ADF entities
     */
    @Test
    public void testInstantiateCmJpaEntity() {
        CmEntitytype frankenstein = AdfTestUtils.getAdfFactory()
            .instantiateJpaEntity(CmEntitytype.class.getSimpleName());
        assertNotNull(frankenstein);
    }

    /*
     * Test that we can load entities from extened applictations
     * with their own package structure.
     */
    @Test
    public void testInstantiateDummyJpaEntity() {
        IDummy1 frankenstein = AdfTestUtils.getAdfFactory()
            .instantiateJpaEntity(IDummy1.class.getSimpleName());
        assertNotNull(frankenstein);
    }

    @Test
    public void testFindAllEntities() {
        Collection<Class<?>> entities =
            AdfTestUtils.getAdfFactory().findAllEntities();
        // for (Class<?> entity : entities) {
        // System.out.println(entity);
        // }
        assertTrue(entities.contains(IDummy2Out.class));
        assertTrue(entities.contains(CmEnum.class));
    }

    @Test
    public void testFindAllAdfFactories() {
        Collection<Class<? extends AdfFactory>> factories =
            AdfFactory.findAllAdfFactories();
        // for (Class<?> adfFactory : factories) {
        // System.out.println("Found adfFactory: " + adfFactory);
        // }
        assertTrue(factories.contains(DummyAdfFactory.class));
    }
}
