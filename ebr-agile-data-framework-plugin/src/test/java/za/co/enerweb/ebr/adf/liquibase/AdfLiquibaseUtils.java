package za.co.enerweb.ebr.adf.liquibase;

import java.io.Closeable;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.SQLException;

import javax.persistence.EntityManager;

import za.co.enerweb.ebr.adf.AdfTestUtils;
import za.co.enerweb.ebr.edbm.DBManagerFactory;
import za.co.enerweb.ebr.edbm.DbSpec;

public class AdfLiquibaseUtils implements Closeable {
    private EntityManager entityManager;
    private Connection connection;
    DbSpec dbSpec;

    public AdfLiquibaseUtils(DbSpec dbSpec) {
        this.dbSpec = dbSpec;
        entityManager = DBManagerFactory.getDbManager()
            .getEntityManager(dbSpec);
        connection = entityManager.unwrap(Connection.class);
    }

    public void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        entityManager.close();
    }

    public void printChangeLog(PrintStream out) {
//        Liquibase liquibase = new Liquibase(getChangeLog(),
//            createResourceOpener(), new JdbcConnection(c));
//
//        Liquibase liquibase = null;
//        try {
//            c = getDataSource().getConnection();
//            liquibase = createLiquibase(c);
//            liquibase.update(getContexts());
//        } catch (SQLException e) {
//            throw new DatabaseException(e);
//        } finally {
//            if (liquibase != null) {
//                liquibase.forceReleaseLocks();
//            }
//            if (c != null) {
//                try {
//                    c.rollback();
//                    c.close();
//                } catch (SQLException e) {
//                    //nothing to do
//                }
//            }
//        }
    }

    public static void main(String... args) {

        new AdfLiquibaseUtils(AdfTestUtils.getDbSpec())
            .printChangeLog(System.out);


    }


}
