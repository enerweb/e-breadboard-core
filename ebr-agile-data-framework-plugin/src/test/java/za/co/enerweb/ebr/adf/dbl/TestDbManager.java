package za.co.enerweb.ebr.adf.dbl;

import static org.junit.Assert.assertEquals;

import javax.persistence.EntityManager;

import org.apache.openejb.api.LocalClient;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import za.co.enerweb.ebr.adf.AdfFactory;
import za.co.enerweb.ebr.adf.AdfTestUtils;
import za.co.enerweb.ebr.adf.entities.CmEntitytype;
import za.co.enerweb.ebr.server.AEbrTest;

@LocalClient
public class TestDbManager extends AEbrTest {

    @Rule
    public TestName testName = new TestName();

    private EntityManager em;

    private AdfFactory adfFactory;

    public void setUpAfterInjection() throws Exception {
        super.setUpAfterInjection();
        adfFactory = AdfTestUtils.getAdfFactory();
        em = adfFactory.getEntityManager("adf-test-pu");
        Assert.assertNotNull(em);
    }

    @Test
    public void testQueryForeignEntity() {
        String typeKey = testName.getMethodName();
        CmEntitytype testEntityType = new CmEntitytype(typeKey,
            typeKey + " description");
        em.getTransaction().begin();
        em.persist(testEntityType);
        em.getTransaction().commit();

        CmEntitytype foundEt = em.find(CmEntitytype.class,
            testEntityType.getKey());
        assertEquals(testEntityType.getKey(), foundEt.getKey());
        assertEquals(testEntityType.getDescription(), foundEt.getDescription());
    }

/*
select APPLDATETIME,
AVG(convert(VAL, double)) - (2 * STDDEV(convert(VAL, double))),
AVG(convert(VAL, double)),
AVG(convert(VAL, double)) + (2 * STDDEV(convert(VAL, double))) from I_LOAD_OUT
 WHERE KEY = 'LOAD'
group by APPLDATETIME
order by APPLDATETIME

select APPLDATETIME, VAL, INDX from I_LOAD_OUT
 WHERE KEY = 'LOAD'
order by APPLDATETIME, convert(INDX,integer)
 */
//    @SuppressWarnings("unchecked")
//    @Test
//    public void testNativeQuery() {
//        EntityManager em = adfFactory.getEntityManager();
//        new AdfFactory(new DbSpec("test")).getAdfDbLayer();
//        Query q = em.createNativeQuery(
//            "SELECT APPLDATETIME, VAL FROM I_LOAD_PAR "
//            + "WHERE KEY = 'LOAD' "
//            + "order by APPLDATETIME, convert(INDX,integer)");
//        //q.setParameter("key", "LOAD");
//        List<Object[]> resultList = q.getResultList();;
//        for (Object[] row : resultList) {
//            log.debug(Arrays.toString(row));
//            Date time = (Date) row[0];
//            double value = Double.parseDouble((String) row[1]);
//            log.debug(time + " " + value);
//        }
//    }
}
