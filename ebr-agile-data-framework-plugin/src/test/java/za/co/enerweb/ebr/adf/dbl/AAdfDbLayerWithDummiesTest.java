package za.co.enerweb.ebr.adf.dbl;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static za.co.enerweb.ebr.adf.dbl.AdfDbLayer.toCaption;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Period;

import za.co.enerweb.ebr.adf.dummy.entities.IDummy1;
import za.co.enerweb.ebr.adf.dummy.entities.IDummy2;
import za.co.enerweb.ebr.adf.dummy.model.EDummy1;
import za.co.enerweb.ebr.adf.dummy.model.EDummy1Dummy2;
import za.co.enerweb.ebr.adf.dummy.model.EDummy2;
import za.co.enerweb.ebr.adf.dummy.model.TTypeDummy1;
import za.co.enerweb.ebr.adf.dummy.model.TTypeDummy2;
import za.co.enerweb.ebr.adf.dummy.model.TTypeGisDummy1;
import za.co.enerweb.ebr.adf.entities.CmEntity;
import za.co.enerweb.ebr.adf.entities.CmParamdef;
import za.co.enerweb.ebr.adf.entities.CmReltypedef;
import za.co.enerweb.ebr.adf.entities.CmTypedef;
import za.co.enerweb.ebr.adf.entities.CmUnitofmeasure;
import za.co.enerweb.ebr.adf.entities.Relationship;
import za.co.enerweb.ebr.adf.model.TAdfConfiguration;

@Slf4j
public abstract class AAdfDbLayerWithDummiesTest
extends AbstractAdfDbLayerTest {

    protected static final int VALUE_START = 2101;
    protected static final int VALUE_END = VALUE_START + 11;
    protected static final String PARAM_DUMMY_NUMBER = "DUMMY_NUMBER";
    protected static final String PARAM_DUMMY_NUMBER_ARRAY =
        "DUMMY_NUMBER_ARRAY";
    protected static final String PARAM_DUMMY_INTEGER = "DUMMY_INTEGER";
    protected static final String PARAM_DUMMY_BOOLEAN = "DUMMY_BOOLEAN";
    protected static final String PARAM_DUMMY_TEXT = "DUMMY_TEXT";

    // XXX: when we have a proper db-manager we should update these
    protected static final String ENTITY_DUMMY1 = EDummy1.KEY;
    public static final String ENTITY_DUMMY2 = EDummy2.KEY;
    public static final String ENTITY_DUMMY1__DUMMY2
        = "DUMMY1_DUMMY2";

    protected static final String TYPE_DUMMY1 = "TYPE_DUMMY1";
    protected static final String TYPE_DUMMY2 = "TYPE_DUMMY2";

    protected static final String REL_DUMMY1__DUMMY2 =
        TTypeDummy1.R_DUMMY1_DUMMY2_KEY;

    protected static final DateTime START
        = new DateTime(2101, 1, 1, 0, 0, 0, 0);
    protected static final DateTime END = START.plusYears(1);
    protected static final Period PERIOD = Period.months(1);

    protected static final int ARRAY_SIZE = 10;

    protected CmEntity entityDummy1;
    protected CmEntity entityDummy2;
    protected CmEntity entityTypeDummy1TypeDummy2;
    protected CmTypedef typeDummy1;
    protected CmTypedef typeDummy2;
    protected List<CmTypedef> allTypes;
    protected IDummy1 iDummy1InstanceA;
    protected IDummy1 iDummy1InstanceB;
    protected IDummy2 iDummy2InstanceC;
    protected CmParamdef cmParamdefNumber;
    protected CmParamdef cmParamdefNumberArray;
    protected CmParamdef cmParamdefInteger;
    protected CmParamdef cmParamdefBoolean;
    protected CmParamdef cmParamdefText;
    protected Interval paramInterval = new Interval(
        START,
        new DateTime(2102, 1, 1, 0, 0, 0, 0));
    protected CmReltypedef relTypeDef;
    protected Relationship dummy1ADummy2CRel1;

    DummyAdfDbLayer dummyAdfDbLayer;
    protected TTypeDummy1 dummy1InstanceA;
    protected TTypeDummy2 dummy2InstanceC;

    public void setUpAfterInjection()  throws Exception {
        super.setUpAfterInjection();
        dummyAdfDbLayer = (DummyAdfDbLayer) dbl;

        addDummyData();
    }

    private void addDummyData() {
        entityDummy1 = dbl.getCmEntity(ENTITY_DUMMY1);
        entityDummy2 = dbl.getCmEntity(ENTITY_DUMMY2);
        entityTypeDummy1TypeDummy2 = dbl.getCmEntity(ENTITY_DUMMY1__DUMMY2);

        typeDummy1 = dbl.getCmTypedef(TTypeDummy1.class);
        typeDummy2 = dbl.getCmTypedef(TTypeDummy2.class);
        allTypes = Arrays.asList(dbl.getCmTypedef(TAdfConfiguration.KEY),
            getDynamicDummy(),
            dbl.getCmTypedef(TTypeGisDummy1.KEY),
            typeDummy1, typeDummy2);

        assertNotNull(typeDummy1.getCmParamdefs());

        relTypeDef = dbl.getCmReltypedef(
            dbl.getCmEntity(EDummy1Dummy2.class),
            TTypeDummy1.R_DUMMY1_DUMMY2_KEY);

        addParamDefs();

        dummy1InstanceA = new TTypeDummy1();
        dummy1InstanceA.setCaption("Dummy 1 Instance A");
        dbl.saveAdfInstance(dummy1InstanceA);
        iDummy1InstanceA = dbl.findInstance(
            typeDummy1, dummy1InstanceA.getId());
        addParamValues(iDummy1InstanceA);
        // reload after params were added.
        // this must actually load the array again!
        dummy1InstanceA = dbl.findAdfInstance(dummy1InstanceA, true);

        iDummy1InstanceB = dbl.getOrCreateInstance(
            typeDummy1, toCaption("Dummy 1 Instance B"));
        addParamValues(iDummy1InstanceB);

        dummy2InstanceC = new TTypeDummy2();
        dummy2InstanceC.setCaption("Dummy 2 Instance C");
        dbl.saveAdfInstance(dummy2InstanceC);
        iDummy2InstanceC = dbl.findInstance(
            typeDummy2, dummy2InstanceC.getId());

        // create this relationship with our new object api
        // dbl.createRelationship(RRelationshipTypeDummy1Dummy2.class,
        // dummy1InstanceA, dummy2InstanceC);
        dummy1InstanceA.setTypeDummy2(dummy2InstanceC);
        dummy1InstanceA.addTypeDummy2hier(dummy2InstanceC);
        dbl.saveAdfInstance(dummy1InstanceA, false, true);

        // wait for relationships before loading it
        dummy2InstanceC = dbl.findAdfInstance(EDummy2.class,
            dummy2InstanceC.getId());

        dummy1ADummy2CRel1 = dbl.findRelationship(
            iDummy1InstanceA, iDummy2InstanceC, relTypeDef);
        dbl.createRelationship(iDummy1InstanceB, iDummy2InstanceC);
    }

    protected CmTypedef getDynamicDummy() {
        return new CmTypedef(2, entityDummy2, "TYPE_DYNAMIC_DUMMY",
            "Type Dynamic Dummy");
    }

    private void addParamDefs() {
        cmParamdefNumber = dbl.getCmParamdef(typeDummy1,
            PARAM_DUMMY_NUMBER);
        assertNotNull(cmParamdefNumber);

        //FIXME: this should be declared in annotation
        CmUnitofmeasure mb = dbl.getOrCreateUnitOfmeasure("MB");
        mb.setDescription("Megabytes");
        dbl.save(mb);
        cmParamdefNumber.setCmUnitofmeasure(mb);
        dbl.save(cmParamdefNumber);

        cmParamdefNumberArray = dbl.getCmParamdef(typeDummy1,
            PARAM_DUMMY_NUMBER_ARRAY);
        assertNotNull(cmParamdefNumberArray);

        cmParamdefInteger = dbl.getCmParamdef(typeDummy1,
            PARAM_DUMMY_INTEGER);
        assertNotNull(cmParamdefInteger);

        cmParamdefBoolean = dbl.getCmParamdef(typeDummy1,
            PARAM_DUMMY_BOOLEAN);
        assertNotNull(cmParamdefBoolean);

        cmParamdefText = dbl.getCmParamdef(typeDummy1,
            PARAM_DUMMY_TEXT);
        assertNotNull(cmParamdefText);
    }

    // private void addParamValues(
    // TTypeDummy1 iDummy1) {
    // int valueGenerator = VALUE_START;
    // for (DateTime cur = START; cur.isBefore(END); cur = cur.plus(PERIOD)) {
    // int curVal = valueGenerator++;
    // Date appldatetime = cur.toDate();
    // iDummy1.setDummyNumber(curVal + 0.1);
    // val numArray = new ArrayList<Double>();
    private void addParamValues(
            IDummy1 iDummy1) {
        int valueGenerator = VALUE_START;

        for (DateTime cur = START; cur.isBefore(END); cur = cur.plus(PERIOD)) {
            int curVal = valueGenerator++;
            Date appldatetime = cur.toDate();
            // log.debug("cur=" + cur);
            dbl.addParamValue(iDummy1, cmParamdefNumber.getKey(), curVal + 0.1,
                appldatetime, null);
            for (int i = 0; i < ARRAY_SIZE; i++) {
                double val = curVal + 0.1 + i;
                dbl.addParamValue(iDummy1, cmParamdefNumberArray.getKey(),
                    val, appldatetime, "" + i);
            }

            dbl.addParamValue(iDummy1, cmParamdefInteger.getKey(),
                curVal, appldatetime, null);

            dbl.addParamValue(iDummy1, cmParamdefBoolean.getKey(),
                valueGenerator % 2 == 0, appldatetime, null);

            dbl.addParamValue(iDummy1, cmParamdefText.getKey(),
                "a" + curVal, appldatetime, null);
        }
    }

    protected void assertGotTypes(List<CmTypedef> cmTypedefs) {
        assertEquals(allTypes, cmTypedefs);
        // assertTrue(cmTypedefs.size() > 0);
        // // should find the test entity
        // boolean foundDummy = false;
        // for (CmTypedef cmTypedef : cmTypedefs) {
        // log.debug("typeCoalSupply: " + cmTypedef.toString());
        // if (cmTypedef.getEntity().equals(ENTITY_DUMMY_INSTANCE)) {
        // foundDummy = true;
        // }
        // }
        // assertTrue(foundDummy);
    }
}
