package za.co.enerweb.ebr.adf.dummy.rest;

import static org.hamcrest.Matchers.equalTo;
import lombok.extern.slf4j.Slf4j;

import org.apache.openejb.api.LocalClient;

import za.co.enerweb.ebr.adf.rest.model.RCUnitOfMeasure;
import za.co.enerweb.ebr.adf.rest.model.RCUnitOfMeasures;

import com.jayway.restassured.response.ValidatableResponse;

/*
 * Failing because stupid jettison insists on returning a single element
 * in stead of a single element list :(
 * need to move to jackson it appears
 * http://openejb.979440.n4.nabble.com/json-list-returning-one-element-lacks-square
 * -bracket-pair-td4661524.html
 */
@Slf4j
@LocalClient
public class RUnitOfMeasuresTest extends AConceptsTest {

    private static final String CAPTION = "MB";
    private static final String DESCRIPTION = "Megabytes";
    private static final String SLUG = "mb";

    public RUnitOfMeasuresTest() {
        super(RCUnitOfMeasures.URI_FRAGMENT, RCUnitOfMeasure.URI_FRAGMENT,
            SLUG);
    }

    // {"UnitOfMeasures":{"items":{"@xsi.type":"rcUnitOfMeasure",
    // "id":"mb","meta":{"href":"http:\/\/localhost:8080\/dummyApp\/UnitOfMeasure\/mb","mediaType":
    // "application\/json;,application\/xml;,"},"caption":"MB"},
    // "meta":{"href":"http:\/\/localhost:8080\/dummyApp\/UnitOfMeasures",
    // "mediaType":"application\/json;,application\/xml;,"}}}


    protected void testGet(ValidatableResponse r) {
        r.body("caption", equalTo(CAPTION))
            .body("description", equalTo(DESCRIPTION));
    }

}
