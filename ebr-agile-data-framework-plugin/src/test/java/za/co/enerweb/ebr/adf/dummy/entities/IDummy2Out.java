package za.co.enerweb.ebr.adf.dummy.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import za.co.enerweb.ebr.adf.entities.CmParamdef;
import za.co.enerweb.ebr.adf.entities.IbInstanceParameterNorm;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, of = { })
@ToString(callSuper = true, of = { })
@Entity
@Table(name = "I_DUMMY2_OUT")
public class IDummy2Out extends IbInstanceParameterNorm {
    private static final long serialVersionUID = 1L;

    @JoinColumn(name = "DUMMY2_ID", referencedColumnName = "ID")
    @ManyToOne
    private IDummy2 iDummy2;

    public IDummy2Out(CmParamdef cmParamdef, Date appldatetime,
        String val) {
        super(cmParamdef, appldatetime, val);
    }

}
