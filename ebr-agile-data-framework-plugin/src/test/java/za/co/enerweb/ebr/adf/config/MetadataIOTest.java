package za.co.enerweb.ebr.adf.config;

import static org.hamcrest.CoreMatchers.equalTo;

import org.apache.openejb.api.LocalClient;
import org.junit.Assert;
import org.junit.Test;

import za.co.enerweb.ebr.adf.AdfTestUtils;
import za.co.enerweb.ebr.adf.config.json.MetadataIO;
import za.co.enerweb.ebr.adf.dummy.DummyAdfFactory;
import za.co.enerweb.ebr.server.AEbrTest;
import za.co.enerweb.toolbox.io.ResourceUtils;

@LocalClient
public class MetadataIOTest extends AEbrTest {

    @Test
    public void testImportExport() throws Exception {
        DummyAdfFactory adfFactory = AdfTestUtils.getAdfFactory();
        // DummyAdfDbLayer dbl = adfFactory.getDbLayer();

        MetadataIO mio = adfFactory.getMetadataIO();
        String testMetadata = ResourceUtils.getResourceAsString(
            "test_metadata.json");
        mio.importFromJsonText(testMetadata);

        String exptortedMetadata = mio.exportToText();
        Assert.assertThat(exptortedMetadata, equalTo(testMetadata));
    }

}
