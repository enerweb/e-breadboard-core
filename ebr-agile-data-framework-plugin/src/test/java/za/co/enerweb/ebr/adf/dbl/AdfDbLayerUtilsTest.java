package za.co.enerweb.ebr.adf.dbl;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import za.co.enerweb.ebr.adf.dummy.entities.IDummy1;
import za.co.enerweb.ebr.adf.dummy.entities.IDummy1Par;

public class AdfDbLayerUtilsTest {

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testEfsEntityToJpaEntity() {
        assertEquals("ILoad", AdfDbLayerUtils.efsEntityToJpaInstanceEntity(
            "LOAD"));
        assertEquals("IPowerstation",
            AdfDbLayerUtils.efsEntityToJpaInstanceEntity("POWERSTATION"));
        assertEquals("ICoalPowerstation",
            AdfDbLayerUtils.efsEntityToJpaInstanceEntity("COAL_POWERSTATION"));
    }

    @Test
    public void testJpaEntityToEfsEntity() {
        assertEquals("LOAD", AdfDbLayerUtils.jpaEntityToEfsEntity("ILoad"));
    }

    @Test
    public void testParamTableToJpaEntity() {
        assertEquals("IWeatherWeathersaOut",
            AdfDbLayerUtils.paramTableToJpaEntity("I_WEATHER_WEATHERSA_OUT"));
    }

    @Test
    public void testParamKeyToJpaProperty() {
        assertEquals("costOfSupply",
            AdfDbLayerUtils.paramKeyToJpaProperty("COST_OF_SUPPLY"));
    }

    @Test
    public void testSetProperty() {
        IDummy1Par obj = new IDummy1Par();
        IDummy1 value = new IDummy1();
        value.setId(42); // make sure equals works
        AdfDbLayerUtils.setProperty(obj, "iDummy1", value);
        assertEquals(value, obj.getIDummy1());
        assertEquals(42, obj.getIDummy1().getId().intValue());
    }
}
