package za.co.enerweb.ebr.adf.dbl;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotSame;
import static za.co.enerweb.ebr.adf.dummy.model.TTypeDynamicDummy.STRING_PARAM;

import org.junit.Test;

import za.co.enerweb.ebr.adf.AdfFactory;
import za.co.enerweb.ebr.adf.data_types.DtText;
import za.co.enerweb.ebr.adf.dummy.model.TTypeDynamicDummy;
import za.co.enerweb.ebr.adf.entities.CmTypedef;
import za.co.enerweb.ebr.adf.model.ParameterKind;

public class TestAdfDbLayerDynamicParams
    extends AAdfDbLayerWithDummiesTest {

    public void setUpAfterInjection() throws Exception {
        super.setUpAfterInjection();
        CmTypedef cmTypedef = dbl.getCmTypedef(TTypeDynamicDummy.KEY);
        dbl.getOrCreateCmParamdef(cmTypedef,
            STRING_PARAM,
            STRING_PARAM,
            null,
            dbl.getOrCreateParamKind(ParameterKind.INPUT),
            dbl.getCmDataType(DtText.class), null, null);
        AdfFactory.resetCheckedDbs(); // force metdatata reload
        dbl = adfFactory.getAdfDbLayer();
    }

    @Test
    public void testSaveAndLoad() {
        TTypeDynamicDummy i = new TTypeDynamicDummy();
        String value = "my value";
        i.setCaption("hi");
        i.set(adfFactory, STRING_PARAM, value);
        dbl.saveAdfInstance(i);
        dbl.invalidateCachedAdfInstances(dbl.findInstance(i));

        TTypeDynamicDummy j = dbl.findAdfInstance(i);
        assertNotSame(i, j);
        assertEquals(value, j.get(adfFactory, STRING_PARAM));
    }
}
