package za.co.enerweb.ebr.adf.dummy.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import za.co.enerweb.ebr.adf.entities.CmTypedef;
import za.co.enerweb.ebr.adf.entities.IbInstance;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, of = { })
@ToString(callSuper = true, of = { })
@Entity
@Table(name = "I_DUMMY1",
    uniqueConstraints = {
        @UniqueConstraint(columnNames = {"TYPEDEF_ID", "ID"})})
public class IDummy1 extends IbInstance {
    private static final long serialVersionUID = 1L;

    // @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "iDummy1")
    // private Collection<IDummy1Out> iDummy1OutCollection;
    // @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "iDummy1")
    // private Collection<RDummy1Dummy2> rDummyInstanc2Collection;
    // @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "iDummy1")
    // private Collection<IDummy1Par> iDummy1ParCollection;

    public IDummy1(CmTypedef cmTypedef, String caption) {
        super(cmTypedef, caption);
    }

}
