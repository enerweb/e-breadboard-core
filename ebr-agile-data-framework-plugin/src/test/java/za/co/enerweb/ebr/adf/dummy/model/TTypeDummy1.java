package za.co.enerweb.ebr.adf.dummy.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import za.co.enerweb.ebr.adf.data_types.DtBoolean;
import za.co.enerweb.ebr.adf.data_types.DtInteger;
import za.co.enerweb.ebr.adf.data_types.DtNumber;
import za.co.enerweb.ebr.adf.data_types.DtNumberArray;
import za.co.enerweb.ebr.adf.data_types.DtText;
import za.co.enerweb.ebr.adf.dummy.entities.HDummyHierarchy;
import za.co.enerweb.ebr.adf.model.AdfHierarchyChild;
import za.co.enerweb.ebr.adf.model.AdfInstance;
import za.co.enerweb.ebr.adf.model.AdfInstanceTypeDef;
import za.co.enerweb.ebr.adf.model.AdfParamDef;
import za.co.enerweb.ebr.adf.model.AdfRelationshipTo;

@Data
@EqualsAndHashCode(callSuper = true, of = { })
@ToString(callSuper = true, of = { })
@AdfInstanceTypeDef(entity = EDummy1.class)
public class TTypeDummy1 extends AdfInstance {
    public static final String KEY = deriveKey(TTypeDummy1.class);
    public static final String R_DUMMY1_DUMMY2_KEY = "DUMMY1_DUMMY2";
    @AdfParamDef(type = DtNumber.class)
    public Double dummyNumber;

    @AdfParamDef(type = DtNumberArray.class)
    public List<Double> dummyNumberArray = new ArrayList<Double>();
    // = Arrays.asList(0.1, 0.2);

    @AdfParamDef(type = DtInteger.class)
    public Integer dummyInteger;

    @AdfParamDef(type = DtBoolean.class)
    public Boolean dummyBoolean;

    @AdfParamDef(type = DtText.class)
    public String dummyText;

    @AdfRelationshipTo(entity = EDummy1Dummy2.class,
        key = TTypeDummy1.R_DUMMY1_DUMMY2_KEY)
    private TTypeDummy2 typeDummy2;

    @AdfHierarchyChild(entity = EDummyHierarchy.class,
        key = HDummyHierarchy.DUMMY_HIER_KEY)
    private List<TTypeDummy2> typeDummy2hier = new ArrayList<TTypeDummy2>();

    public void addTypeDummy2hier(TTypeDummy2 x) {
        typeDummy2hier.add(x);
    }
}
