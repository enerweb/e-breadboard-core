package za.co.enerweb.ebr.adf.dummy.rest;

import static org.hamcrest.Matchers.equalTo;
import lombok.extern.slf4j.Slf4j;

import org.apache.openejb.api.LocalClient;

import za.co.enerweb.ebr.adf.rest.model.RCProperty;
import za.co.enerweb.ebr.adf.rest.model.RCProperties;

import com.jayway.restassured.response.ValidatableResponse;

@Slf4j
@LocalClient
public class RPropertiesTest extends AConceptsTest {

    private final static String capiton =
        FIELD__TYPE_DUMMY__DUMMY_NUMBER__CAPTION;
    private final static String description =
        FIELD__TYPE_DUMMY__DUMMY_NUMBER__DESCRIPTION;

    public RPropertiesTest() {
        super(RCProperties.URI_FRAGMENT, RCProperty.URI_FRAGMENT,
            FIELD__TYPE_DUMMY__DUMMY_NUMBER__SLUG);
    }

    // {"Fields":{"items":[{"@xsi.type":"RCProperty",
    // "id":"type_dummy2-5.dummy_number",
    // "meta":{"href":"http:\/\/localhost:20302\/dummyApp\/Field\/type_dummy2-5.dummy_number",
    // "mediaType":"application\/json;,application\/xml;,"},
    // "caption":"Dummy Number","description":""}

    protected void testGet(ValidatableResponse r) {
        r.body("caption", equalTo(capiton))
            .body("description", equalTo(description));
    }
}
