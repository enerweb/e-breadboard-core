package za.co.enerweb.ebr.adf.dbl;

import static org.junit.Assert.assertEquals;
import static za.co.enerweb.ebr.adf.dbl.AdfDbLayer.toCaption;

import java.util.Date;
import java.util.List;

import org.junit.Test;

import za.co.enerweb.ebr.adf.data_types.GisLocation;
import za.co.enerweb.ebr.adf.dummy.entities.IDummy1;
import za.co.enerweb.ebr.adf.dummy.model.TTypeGisDummy1;
import za.co.enerweb.ebr.adf.entities.CmTypedef;

public class TestGisDbLayer
    extends AAdfDbLayerWithDummiesTest {

    private static final GisLocation GIS_LOCATION = new GisLocation(31.77838,
        35.229587);
    private static final GisLocation GIS_LOCATION1 = new GisLocation(31.77839,
        35.229588);
    private GisDbLayer gisDbLayer;
    private IDummy1 gisInstance;

    public void setUpAfterInjection() throws Exception {
        super.setUpAfterInjection();
        gisDbLayer = adfFactory.getGisDbLayer();
        gisDbLayer.checkMetadata();

        // add a hierarchy
        // data types

        CmTypedef cmTypedef = dbl.getCmTypedef(TTypeGisDummy1.class);
        gisInstance = dbl.getOrCreateInstance(
            cmTypedef, toCaption("Gis Dummy Instance"));
        dbl.addParamValue(gisInstance, GisDbLayer.PARAM_LOCATION
            , GIS_LOCATION1, null, "1");
        dbl.addParamValue(gisInstance, GisDbLayer.PARAM_LOCATION
            , GIS_LOCATION, null, "0");
    }

    @Test
    public void testGetGisLocations() {
        List<List<GisLocation>> gisLocations = gisDbLayer.getGisLocations(
            gisInstance, new Date());
        GisLocation gisLocation = gisLocations.get(0).get(0);
        assertEquals(GIS_LOCATION, gisLocation);
        assertEquals(GIS_LOCATION.getLatitude(), gisLocation.getLatitude()
            , EPSILON);
        assertEquals(GIS_LOCATION.getLongitude(), gisLocation.getLongitude()
            , EPSILON);

        GisLocation gisLocation1 = gisLocations.get(0).get(1);
        assertEquals(GIS_LOCATION1, gisLocation1);
    }

}
