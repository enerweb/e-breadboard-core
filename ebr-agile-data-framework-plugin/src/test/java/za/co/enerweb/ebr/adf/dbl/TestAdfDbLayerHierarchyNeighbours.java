package za.co.enerweb.ebr.adf.dbl;

import static junit.framework.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import za.co.enerweb.ebr.adf.dummy.entities.HDummyHierarchy;
import za.co.enerweb.ebr.adf.dummy.model.TTypeDummy1;
import za.co.enerweb.ebr.adf.dummy.model.TTypeDummy2;
import za.co.enerweb.ebr.adf.entities.IbInstance;

public class TestAdfDbLayerHierarchyNeighbours
    extends AbstractAdfDbLayerTest {
    private static final String newChildCaption = "Dummy 2 Instance D";
    private static final String UNIQUE_NAME = "unique_name";
    private static final String DUMMY_TEXT_VALUE = "AAAAA";
    protected TTypeDummy1 dummy1InstanceA;
    protected TTypeDummy2 dummy2InstanceC;

    @Before
    public void setup() {
        // create 2 nodes with some properties
        dummy1InstanceA = new TTypeDummy1();
        dummy1InstanceA.setCaption("Dummy 1 Instance A");
        dummy1InstanceA.setDummyText(DUMMY_TEXT_VALUE);
        dbl.saveAdfInstance(dummy1InstanceA);

        dummy2InstanceC = new TTypeDummy2();
        dummy2InstanceC.setName(UNIQUE_NAME);
        dummy2InstanceC.setCaption("Dummy 2 Instance C");
        dbl.saveAdfInstance(dummy2InstanceC);

        // link them
        dummy1InstanceA.addTypeDummy2hier(dummy2InstanceC);
        dbl.saveAdfInstance(dummy1InstanceA, false, true);
    }

    @Test
    public void testHierarchies() {
        // load entities from scratch
        dummy1InstanceA = dbl.findAdfInstance(dummy1InstanceA, true);
        dummy2InstanceC = dbl.findAdfInstance(dummy2InstanceC);
        // find neighbour
        assertEquals(dummy2InstanceC,
            dummy1InstanceA.getTypeDummy2hier().get(0));
        assertEquals(dummy1InstanceA, dummy2InstanceC.getTypeDummy1hier());
    }

    @Test
    public void testHierarchyChildren() {
        List<IbInstance> hierarchyChildren = dbl.findHierarchyChildren(
            HDummyHierarchy.DUMMY_HIER_KEY,
            dbl.findInstance(dummy1InstanceA), null);
        assertEquals(1, hierarchyChildren.size());
        assertEquals(dummy2InstanceC.getId(), hierarchyChildren.get(0).getId());
    }

    @Test
    public void testHierarchyDuplicateChildren() {
        TTypeDummy2 dummy2InstanceD = new TTypeDummy2();
        dummy2InstanceD.setName("some name");
        dummy2InstanceD.setCaption(newChildCaption);
        dbl.saveAdfInstance(dummy2InstanceD);

        // link them
        dummy1InstanceA.getTypeDummy2hier().clear();
        dummy1InstanceA.addTypeDummy2hier(dummy2InstanceD);
        dbl.saveAdfInstance(dummy1InstanceA, false, true);

        dummy1InstanceA = dbl.findAdfInstance(dummy1InstanceA, true);

        List<IbInstance> hierarchyChildren = dbl.findHierarchyChildren(
            HDummyHierarchy.DUMMY_HIER_KEY,
            dbl.findInstance(dummy1InstanceA), null);
        assertEquals(1, hierarchyChildren.size());
        assertEquals(dummy2InstanceD.getId(), hierarchyChildren.get(0).getId());

        assertEquals(newChildCaption,
            dummy1InstanceA.getTypeDummy2hier().get(0).getCaption());
    }

    @Test
    public void testHierarchyUniqueChildren() {

        // add a child, then add one again with the same name
        // (assuming it's parent is set..)

        TTypeDummy2 dummy2InstanceD = new TTypeDummy2();
        dummy2InstanceD.setTypeDummy1hier(dummy1InstanceA);
        dummy2InstanceD.setName(UNIQUE_NAME);
        dummy2InstanceD.setCaption(newChildCaption);
        dbl.saveAdfInstance(dummy2InstanceD);

        // link them
        dummy1InstanceA.getTypeDummy2hier().clear();
        // ^ not needed since they have the same id
        // XXX: but it still saves duplicates if the list contains
        // multiple items with the same id :(
        dummy1InstanceA.addTypeDummy2hier(dummy2InstanceD);
        dbl.saveAdfInstance(dummy1InstanceA, false, true);

        dummy1InstanceA = dbl.findAdfInstance(dummy1InstanceA, true);

        List<IbInstance> hierarchyChildren = dbl.findHierarchyChildren(
            HDummyHierarchy.DUMMY_HIER_KEY,
            dbl.findInstance(dummy1InstanceA), null);
        assertEquals(1, hierarchyChildren.size());
        assertEquals("must overwrite the old value cos it's got the same name",
            dummy2InstanceC.getId(), hierarchyChildren.get(0).getId());
        assertEquals(dummy2InstanceD.getId(), hierarchyChildren.get(0).getId());

    }

    @Test
    public void testHierarchyFilter() {
        // find neighbour with including filter
        List<IbInstance> hierarchyChildren = dbl.findHierarchyNeighbours(
            HDummyHierarchy.DUMMY_HIER_KEY,
            dbl.findInstance(dummy2InstanceC), null,
            false, new InstanceFilter().fieldName("dummyText").value(
                DUMMY_TEXT_VALUE).instanceClass(TTypeDummy1.class)
            );
        assertEquals(1, hierarchyChildren.size());
        assertEquals(dummy2InstanceC.getId(), hierarchyChildren.get(0).getId());

        // don't find neighbour with excluding filter
        hierarchyChildren = dbl.findHierarchyNeighbours(
            HDummyHierarchy.DUMMY_HIER_KEY,
            dbl.findInstance(dummy2InstanceC), null,
            false, new InstanceFilter().fieldName("dummyText").value(
                "don't match please" + DUMMY_TEXT_VALUE
                ).instanceClass(TTypeDummy1.class)
            );
        assertEquals(0, hierarchyChildren.size());
    }
}
