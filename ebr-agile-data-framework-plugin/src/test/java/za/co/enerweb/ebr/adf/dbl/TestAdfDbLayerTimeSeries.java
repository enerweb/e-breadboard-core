package za.co.enerweb.ebr.adf.dbl;

import static org.junit.Assert.assertEquals;

import org.joda.time.DateTime;
import org.junit.Ignore;
import org.junit.Test;

import za.co.enerweb.ebr.adf.model.timeseries.ITimeSeries;

public class TestAdfDbLayerTimeSeries
    extends AAdfDbLayerWithDummiesTest {

    public void setUpAfterInjection() throws Exception {
        super.setUpAfterInjection();
    }

    /**
     * FIXME: This test fails because our current model doesn't take into
     * account
     * changes in time. I think AdfInstance should be renamed to
     * InstanceInTime and always have an effective date
     * i.e. you must always look it up with a date even if it is
     * armageddon or today. when saving only save values if it
     * is different from the previous value.
     * if the table is denormalized I think we need to copy it.
     * I think the api must also lean towards objects looking differently
     * over time because that is how adf was designed.
     * We should add calls to get a timeseries of InstanceInTime objects
     * maybe call that AdfInstance, it should contain all values EVER.
     */
    @Ignore("Need to refactor how our instances work :(")
    @Test
    public void testGetScalarTimeSeries() {
        ITimeSeries<Double> ts = dbl.getScalarTimeSeries(
            iDummy1InstanceA, cmParamdefNumber);

        assertEquals(12, ts.size());
        assertEquals(START.toDate(), ts.getFirstDate());
        assertEquals(START.plusMonths(11).toDate(), ts.getLastDate());
        assertEquals(VALUE_START + 0.1, ts.get(START.toDate()), EPSILON);
    }

    @Test
    public void testGetFilteredScalarTimeSeries() {
        DateTime start = START.plusMonths(1);
        ITimeSeries<Double> ts = dbl.getScalarTimeSeries(
            iDummy1InstanceA, cmParamdefNumber,
            start.toDate(),
            start.plusMonths(2).toDate());

        assertEquals(2, ts.size());
        assertEquals(start.toDate(), ts.getFirstDate());
        assertEquals(start.plusMonths(1).toDate(), ts.getLastDate());
        assertEquals(VALUE_START + 1.1, ts.get(start.toDate()), EPSILON);
    }
}
