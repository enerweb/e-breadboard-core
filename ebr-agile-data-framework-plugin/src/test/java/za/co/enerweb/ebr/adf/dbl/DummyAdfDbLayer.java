package za.co.enerweb.ebr.adf.dbl;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.persistence.Query;

import za.co.enerweb.ebr.adf.entities.CmHierarchyDef;
import za.co.enerweb.ebr.adf.entities.IbInstance;

@Stateful
@LocalBean
public class DummyAdfDbLayer extends AdfDbLayer {

    public synchronized void checkMetadata() {
        super.checkMetadata();
    }

    /**
     * removes this instance from all roots even if it has no parents
     */
    public int deleteHierarchyRoot(String hierarchyKey,
        IbInstance instance) {
        CmHierarchyDef cmHierarchyDef = findHierarchy(hierarchyKey);
        String hierarchyEntity = cmHierarchyDef.getJpaHierarchyEntity();
        Query q = em.createQuery("delete FROM "
            + hierarchyEntity + " j"
            + " WHERE j.cmHierarchyDef = :cmHierarchyDef"
            + " AND (j.parentCmTypedef is null"
            + " AND j.parentInstanceId is null)"
            + " AND (j.childCmTypedef = :instanceCmTypedef"
            + " AND j.childInstanceId = :instanceId)"
            );
        q.setParameter("cmHierarchyDef", cmHierarchyDef);
        q.setParameter("instanceCmTypedef", instance.getCmTypedef());
        q.setParameter("instanceId", instance.getId());
        return q.executeUpdate();
    }

}
