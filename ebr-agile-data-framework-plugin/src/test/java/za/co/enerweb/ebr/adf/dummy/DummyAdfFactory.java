package za.co.enerweb.ebr.adf.dummy;

import lombok.Getter;
import za.co.enerweb.ebr.adf.AdfFactory;
import za.co.enerweb.ebr.adf.dbl.AdfDbLayer;
import za.co.enerweb.ebr.adf.dbl.DummyAdfDbLayer;

/**
 * Test implementation of AdfFactory
 */
public class DummyAdfFactory extends AdfFactory {

    @Getter
    private static DummyAdfFactory workInstance = new DummyAdfFactory();

    @Override
    public Class<? extends AdfDbLayer> getDefaultDbLayerClass() {
        return DummyAdfDbLayer.class;
    }

    @SuppressWarnings("unchecked")
    public DummyAdfDbLayer getDbLayer() {
        return (DummyAdfDbLayer) super.getAdfDbLayer();
    }

    @Override
    public String getModuleName() {
        return "adf-test-pu";
    }

    @Override
    public String getPackageName() {
        return "za.co.enerweb_adf-test-package";
    }
}
