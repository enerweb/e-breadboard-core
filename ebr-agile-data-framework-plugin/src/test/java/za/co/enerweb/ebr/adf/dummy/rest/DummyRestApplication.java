package za.co.enerweb.ebr.adf.dummy.rest;

import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import za.co.enerweb.ebr.server.Registrar;



@ApplicationPath("/adfdummy")
public class DummyRestApplication extends Application {
    public Set<Class<?>> getClasses() {
        return Registrar.getClassesInPackage("za.co.enerweb.ebr.adf.dummy.rest.adf");
    }
}
