package za.co.enerweb.ebr.adf.dummy.model;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import za.co.enerweb.ebr.adf.data_types.DtText;
import za.co.enerweb.ebr.adf.model.AdfInstance;
import za.co.enerweb.ebr.adf.model.AdfInstanceTypeDef;
import za.co.enerweb.ebr.adf.model.AdfParamDef;

/**
 * Custom key
 */
@Data
@EqualsAndHashCode(callSuper = true, of = { })
@ToString(callSuper = true, of = { })
@AdfInstanceTypeDef(entity = EDummy2.class)
public class TTypeDynamicDummy extends AdfInstance {
    public static final String KEY = deriveKey(TTypeDynamicDummy.class);
    public static final String STRING_PARAM = "STRING_PARAM";

    @AdfParamDef(type = DtText.class)
    public String name;
}
