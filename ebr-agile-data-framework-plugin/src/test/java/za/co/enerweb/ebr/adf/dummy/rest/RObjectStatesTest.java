package za.co.enerweb.ebr.adf.dummy.rest;

import static java.lang.String.format;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import lombok.extern.slf4j.Slf4j;

import org.apache.openejb.api.LocalClient;

import za.co.enerweb.ebr.adf.rest.model.RCObject;
import za.co.enerweb.ebr.adf.rest.model.RCObjectState;
import za.co.enerweb.ebr.adf.rest.model.RCObjectStates;

import com.jayway.restassured.response.ValidatableResponse;
import com.jayway.restassured.specification.ResponseSpecification;

@Slf4j
@LocalClient
public class RObjectStatesTest extends AConceptsTest {

    private static final String ID = "2101-01-01T00:00:00+02:00";

    public RObjectStatesTest() {
        super(RCObjectStates.URI_FRAGMENT, RCObjectState.URI_FRAGMENT,
            ID);
    }

    // {"ObjectStates":{"items":[{
    // "applDateTime":"1970-01-01T00:00:00+02:00",
    // "propertyValues":{"items":[{
    // "propertyId":"type_dummy1-4.dummy_boolean","value":"N"},
    // {"propertyId":"type_dummy1-4.dummy_integer","value":2112},
    //
    // {"@xsi.type":"rcObjectState","applDateTime":"2101-01-01T00:00:00+02:00",
    // "propertyValues":{"items":[{"@xsi.type":
    // "rcPropertyValue","propertyId":"type_dummy1-4.dummy_boolean","value":"Y"},
    // {"@xsi.type":"rcPropertyValue","propertyId":"type_dummy1-4.dummy_integer","value":2101},
    // {"@xsi.type":"rcPropertyValue","propertyId":"type_dummy1-4.dummy_number","value":2101.1},
    // {"@xsi.type":"rcPropertyValue","propertyId":"type_dummy1-4.dummy_text","value":"a2101"}]}},

    protected void testGet(ValidatableResponse r) {
        if (isNotXml()) {
            r.body("propertyValues.items.size", greaterThan(0))

                // FIXME should be true
                .body("propertyValues.items.find{it.propertyId == "
                    + "'type_dummy1-4.dummy_boolean'}.value", equalTo("Y"))
                .body("propertyValues.items.find{it.propertyId == "
                    + "'type_dummy1-4.dummy_integer'}.value", equalTo("2101"))// should
                                                                              // be
                                                                              // unquoted
                .body("propertyValues.items.find{it.propertyId == "
                    + "'type_dummy1-4.dummy_number'}.value",
                    equalTo("2101.1"))// f should be unquoted
                .body("propertyValues.items.find{it.propertyId == "
                    + "'type_dummy1-4.dummy_text'}.value", equalTo("a2101"));
        }
    }

    // http://127.0.0.1:8080/dummyApp/Instance/type_dummy1-4.dummy_1_instance_a-1/InstanceValues.json
    // {"InstanceValues":{"items":[{"@xsi.type":"rcInstanceValue",
    // "applDateTime":"1970-01-01T00:00:00+02:00","fieldValues":""},
    // {"@xsi.type":"rcInstanceValue","applDateTime":"2101-01-01T00:00:00+02:00",
    // "fieldValues":""}...
    // ],"meta":{"href":"http:\/\/127.0.0.1:8080\/dummyApp\/Instance/type_dummy1-4.dummy_1_instance_a-1/InstanceValues",
    // "mediaType":"application\/json;,application\/xml;,"}}}
    // @Test
    // public void testGetMulti() throws Exception {
    // testGet(json(givenApplicationUri()
    // .get(groupUriFragment)).then()
    // .spec(metaSpec(rootPath, itemUriFragment, idFieldValue))
    // .root(rootPath)
    // .body(getIdField(), equalTo(idFieldValue)));
    // }

    protected String getApplicationUrl() {
        return join(super.getApplicationUrl(), RCObject.URI_FRAGMENT,
            INSTANCE__DUMMY1_INSTANCE_A__SLUG);
    }

    protected String getIdField() {
        return "applDateTime";
    }

    protected ResponseSpecification groupMetaSpec(TestContentType t) {
        return metaSpec(t.getGroupRootPath(this), join(RCObject.URI_FRAGMENT,
            INSTANCE__DUMMY1_INSTANCE_A__SLUG, groupUriFragment));
    }

    protected ResponseSpecification itemInGroupMetaSpec(TestContentType t) {
        return noMetaSpec(format("items.any "
        // ,t.getGroupRootPath(this)
//            groupUriFragment
        )
        );
    }

    protected ResponseSpecification singleItemMetaSpec(TestContentType t) {
        return noMetaSpec(t.getSingleItemRootPath(this));
    }

    protected boolean runSingles() {
        return false; // FIXME: should be possible to add this..
    }
}
