package za.co.enerweb.ebr.adf.dummy.rest;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;
import static za.co.enerweb.toolbox.test.ToolboxMatchers.like;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lombok.extern.slf4j.Slf4j;

import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.openejb.api.LocalClient;
import org.junit.Test;

import za.co.enerweb.ebr.adf.rest.model.RCClass;
import za.co.enerweb.ebr.adf.rest.model.RCClasses;
import za.co.enerweb.toolbox.string.StringUtils;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ValidatableResponse;

@Slf4j
@LocalClient
public class RClassesTest extends AConceptsTest {
    private final static String capiton =
        CLASS__ADF_CONF__CAPTION;
    private final static String description =
        CLASS__ADF_CONF__DESCRIPTION;

    public RClassesTest() {
        super(RCClasses.URI_FRAGMENT, RCClass.URI_FRAGMENT,
            CLASS__ADF_CONF__SLUG);
    }

    @Test
    public void getSimple() throws Exception {
        final String message =
            WebClient.create(getApplicationUrl() + "Classes")
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .get(String.class);
        log.debug("** messsage: " + message);
        assertThat(message,
            startsWith("{\n  \"meta\" : {\n    \"href\" : \"http:"));
    }

    // "Classes":{"items":[{"@xsi.type":"rcClass",
    // "id":"adf_configuration-1",
    // "meta":{"href":"http:\/\/localhost:20707\/dummyApp\/Class\/adf_configuration-1"
    // }
    // ,"mediaType":"application\/json;,application\/xml;,"},
    // "caption":"Adf Configuration","description":"Adf Configuration"}

    // <Classes><items xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    // xsi:type="rcClass"><id>adf_configuration-1</id><meta><href>http://localhost:1096
    // 7/dummyApp/Class/adf_configuration-1</href><mediaType>application/json;,application/xml;,</mediaType></meta><caption>Adf
    // Configuration</caption><description>Adf Configuration</desc
    // ription></items>

    // {"Class":{"id":"adf_configuration-1",
    // "meta":{"href":"http:\/\/localhost:27369\/dummyApp\/Class\/adf_configuration-1",
    // "mediaType":"application\/json;,application\/xml;,"},
    // "caption":"Adf Configuration","description":"Adf Configuration"}}

    protected void testGet(ValidatableResponse r) {
        r.body("caption", equalTo(capiton))
            .body("description", equalTo(description));
    }

    private static final String NEW_ITEM_CAPTION = "New Dummy Class";
    private static final String NEW_ITEM_JSON =
        "     {\n" +
        "        \"caption\": \"" + NEW_ITEM_CAPTION + "\",\n" +
        "        \"description\": \"New Dummy Class Description\"\n" +
            "    }\n";

    @Test
    public void testCreate() throws Exception {
        log.debug(NEW_ITEM_JSON);
        String idRegex =
            StringUtils.caption2VariableName(NEW_ITEM_CAPTION)
                .toLowerCase() + "-\\d+";
        String hrefRegex = ".*" + join(itemUriFragment, idRegex);
        givenApplicationUri()
            .body(NEW_ITEM_JSON)
            .contentType(ContentType.JSON)
            .post(itemUriFragment)
            .then().log().all()
            .statusCode(Response.Status.CREATED.getStatusCode())
            .contentType(ContentType.JSON)
            .header("Location", like(hrefRegex))
            .root(TestContentType.JSON.getSingleItemRootPath(this))
            .body("id", like(idRegex))
            .body("meta.href", like(hrefRegex))
            .body("caption", equalTo(NEW_ITEM_CAPTION))
            ;
        // TODO: check returned id and href

        // TODO: read it again an see if it was persisted

        // TODO: test creating duplicate

    }

    // TODO: test create with blank values,
    // TODO: test create with sending XML
}
