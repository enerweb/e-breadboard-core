package za.co.enerweb.ebr.adf.dummy.model;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import za.co.enerweb.ebr.adf.data_types.DtGisLocationArray;
import za.co.enerweb.ebr.adf.data_types.GisLocation;
import za.co.enerweb.ebr.adf.model.AdfInstance;
import za.co.enerweb.ebr.adf.model.AdfInstanceTypeDef;
import za.co.enerweb.ebr.adf.model.AdfParamDef;

@Data
@EqualsAndHashCode(callSuper = true, of = { })
@AdfInstanceTypeDef(entity = EDummy1.class)
public class TTypeGisDummy1 extends AdfInstance {
    public static final String KEY = deriveKey(TTypeGisDummy1.class);

    @AdfParamDef(type = DtGisLocationArray.class)
    public List<GisLocation> location;

}
