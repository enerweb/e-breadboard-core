package za.co.enerweb.ebr.adf.dbl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Date;
import java.util.List;

import org.junit.Test;

import za.co.enerweb.ebr.adf.entities.CmHierarchyDef;
import za.co.enerweb.ebr.adf.entities.IbInstance;

public class TestAdfDbLayerHierarchies
    extends AAdfDbLayerWithDummiesTest {

    private static final String DUMMY_HIERARCHY = "DUMMY_HIERARCHY1";
    private static Date effectiveDate = new Date();

    public void setUpAfterInjection() throws Exception {
        super.setUpAfterInjection();

        // add a hierarchy
        // data types

        dbl.getOrCreateHierarchy(DUMMY_HIERARCHY);

        // add root
        dbl.addHierarchyRootInstance(DUMMY_HIERARCHY, iDummy1InstanceA,
            null, null);

        // link some instances
        dbl.linkHierarchyInstances(DUMMY_HIERARCHY, iDummy1InstanceA,
            iDummy2InstanceC,
            null, null);

    }

    @Test
    public void testHierarchyRoot() {
        List<IbInstance> roots = dbl.findHierarchyRoots(DUMMY_HIERARCHY,
            new Date());
        assertEquals(iDummy1InstanceA, roots.get(0));
    }

    @Test
    public void testConvertOrphanHierarchyNodesToRoots() {
        // delete root indicator for iCoalsupply
        dummyAdfDbLayer.deleteHierarchyRoot(DUMMY_HIERARCHY, iDummy1InstanceA);

        List<IbInstance> roots = dbl.findHierarchyRoots(DUMMY_HIERARCHY,
            effectiveDate);
        assertEquals("make sure there are no roots", 0, roots.size());

        dbl.convertOrphanHierarchyNodesToRoots(
            DUMMY_HIERARCHY, effectiveDate);

        roots = dbl.findHierarchyRoots(DUMMY_HIERARCHY,
            effectiveDate);
        assertEquals(iDummy1InstanceA, roots.get(0));
    }

    @Test
    public void testHierarchyLink() {
        List<IbInstance> children = getChildren(iDummy1InstanceA);
        assertEquals(1, children.size());
        assertEquals(iDummy2InstanceC, children.get(0));

        assertEquals(0, getChildren(iDummy2InstanceC).size());
    }

    public List<IbInstance> getChildren(IbInstance parent) {
        return dbl.findHierarchyChildren(DUMMY_HIERARCHY,
            parent, effectiveDate);
    }

    @Test
    public void testInvertHierarchyLinkDirections() {
        dbl.invertHierarchyLinkDirections(DUMMY_HIERARCHY);

        List<IbInstance> children = getChildren(iDummy2InstanceC);
        assertEquals(1, children.size());
        assertEquals(iDummy1InstanceA, children.get(0));

        assertEquals(0, getChildren(iDummy1InstanceA).size());
    }

    @Test
    public void testDeleteParentInstance() {
        dbl.deleteInstance(iDummy1InstanceA);
        assertEquals(0, getChildren(iDummy1InstanceA).size());
    }

    @Test
    public void testDeleteChildInstance() {
        dbl.deleteInstance(iDummy2InstanceC);
        assertEquals(0, getChildren(iDummy1InstanceA).size());
    }

    @Test
    public void testDeleteHierarchy() {
        dbl.deleteHierarchy(DUMMY_HIERARCHY);
        CmHierarchyDef cmHierarchyDef = dbl.findHierarchy(DUMMY_HIERARCHY);
        assertNull(cmHierarchyDef);

        // see that trying to deleting a nonexisting hierarchy does not give
        // an NPE
        dbl.deleteHierarchy(DUMMY_HIERARCHY);
    }
}
