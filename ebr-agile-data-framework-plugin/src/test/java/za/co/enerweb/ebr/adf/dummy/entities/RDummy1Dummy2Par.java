package za.co.enerweb.ebr.adf.dummy.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import za.co.enerweb.ebr.adf.entities.CmRelparamdef;
import za.co.enerweb.ebr.adf.entities.CmReltypedef;

@Data
@EqualsAndHashCode(of="id")
@Entity
@Table(name = "R_DUMMY1_DUMMY2_PAR")
public class RDummy1Dummy2Par implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "DUMMY_APPLDATETIME", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date appldatetime;
    @Basic(optional = false)
    @Column(name = "VAL", nullable = false, length = 1000)
    private String val;
    @Column(name = "INDX", length = 25)
    private String indx;
    @JoinColumn(name = "DUMMY1_DUMMY2_ID",
        referencedColumnName = "ID")
    @ManyToOne
    private RDummy1Dummy2 rDummy1Dummy2;
    @JoinColumn(name = "RELTYPEDEF_ID", referencedColumnName = "ID",
        nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CmReltypedef cmReltypedef;
    @JoinColumns({
        @JoinColumn(name = "RELTYPEDEF_ID",
            referencedColumnName = "RELTYPEDEF_ID",
            nullable = false),
        @JoinColumn(name = "KEY", referencedColumnName = "KEY",
        nullable = false)
    })
    @ManyToOne(optional = false)
    private CmRelparamdef cmRelparamdef;

    public RDummy1Dummy2Par() {
    }

    public RDummy1Dummy2Par(Integer id) {
        this.id = id;
    }

    public RDummy1Dummy2Par(Integer id, Date appldatetime,
        String val) {
        this.id = id;
        this.appldatetime = appldatetime;
        this.val = val;
    }

}
