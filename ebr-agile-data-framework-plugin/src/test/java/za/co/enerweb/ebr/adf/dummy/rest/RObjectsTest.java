package za.co.enerweb.ebr.adf.dummy.rest;

import static org.hamcrest.Matchers.equalTo;
import lombok.extern.slf4j.Slf4j;

import org.apache.openejb.api.LocalClient;

import za.co.enerweb.ebr.adf.rest.model.RCObject;
import za.co.enerweb.ebr.adf.rest.model.RCObjects;

import com.jayway.restassured.response.ValidatableResponse;

@Slf4j
@LocalClient
public class RObjectsTest extends AConceptsTest {

    private static final String CAPTION = "Dummy 1 Instance A";

    public RObjectsTest() {
        super(RCObjects.URI_FRAGMENT, RCObject.URI_FRAGMENT,
            INSTANCE__DUMMY1_INSTANCE_A__SLUG);
    }

    // This XML file does not appear to have any style information associated
    // with it. The document tree is shown below.
    // <Instances>
    // <items xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    // xsi:type="rcInstance">
    // <id>type_dummy1-4.dummy_1_instance_a-1</id>
    // <meta>
    // <href>
    // http://127.0.0.1:8080/dummyApp/Instance/type_dummy1-4.dummy_1_instance_a-1
    // </href>
    // <mediaType>application/json;,application/xml;,</mediaType>
    // </meta>
    // <caption>Dummy 1 Instance A</caption>
    // </items>

    protected void testGet(ValidatableResponse r) {
        r.body("caption", equalTo(CAPTION));
    }

}
