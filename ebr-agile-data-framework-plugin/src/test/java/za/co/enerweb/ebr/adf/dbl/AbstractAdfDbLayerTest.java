package za.co.enerweb.ebr.adf.dbl;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.adf.AdfFactory;
import za.co.enerweb.ebr.adf.AdfTestUtils;
import za.co.enerweb.ebr.server.AEbrTest;

/*
 * Don't rely on adf test specific entities, because this test class
 * is used by ADF users.
 */
@Slf4j
@Data
public abstract class AbstractAdfDbLayerTest
    extends AEbrTest {

    protected AdfDbLayer dbl;
    protected AdfFactory adfFactory;

    public void setUpAfterInjection() throws Exception {
        super.setUpAfterInjection();
        adfFactory = getAdfFactory();
        dbl = createAdfDbLayer();
        // dbl.setDbSpec(AdfTestUtils.getDbSpec());
    }

    protected AdfDbLayer createAdfDbLayer() {
        // AdfFactory.resetCheckedDbs();
        return adfFactory.getAdfDbLayer();
    }

    public AdfFactory getAdfFactory() {
        return AdfTestUtils.getAdfFactory();
    }

}
