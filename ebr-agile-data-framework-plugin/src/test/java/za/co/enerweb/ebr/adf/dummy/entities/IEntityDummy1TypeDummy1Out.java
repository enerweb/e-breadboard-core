package za.co.enerweb.ebr.adf.dummy.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import za.co.enerweb.ebr.adf.entities.IbInstanceParameter;

/**
 * denormalized table
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true, of={})
@ToString(callSuper=true, of={})
@Entity
@Table(name = "I_DUMMY1_DUMMY1_OUT")
public class IEntityDummy1TypeDummy1Out extends IbInstanceParameter {
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "COST_OF_SUPPLY", nullable = false, length = 1000)
    private double costOfSupply;

    @Basic(optional = false)
    @Column(name = "AVERAGE_CV", nullable = false, length = 1000)
    private double averageCv;

    @Basic(optional = false)
    @Column(name = "AVERAGE_MOISTURE", nullable = false, length = 1000)
    private double averageMoisture;

    @Basic(optional = false)
    @Column(name = "AVERAGE_ABRASIVENESS", nullable = false, length = 1000)
    private double averageAbrasiveness;

    @Basic(optional = false)
    @Column(name = "AVERAGE_ASH", nullable = false, length = 1000)
    private double averageAsh;

    @Basic(optional = false)
    @Column(name = "NET_COAL_DELIVERY", nullable = false, length = 1000)
    private double netCoalDelivery;

    @JoinColumn(name = "COALSUPPLY_ID", referencedColumnName = "ID")
    @ManyToOne
    private IDummy1 iDummy1;

}
