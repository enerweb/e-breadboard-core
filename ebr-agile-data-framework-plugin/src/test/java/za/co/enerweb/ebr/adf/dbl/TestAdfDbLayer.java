package za.co.enerweb.ebr.adf.dbl;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import za.co.enerweb.ebr.adf.dummy.model.EDummy1;
import za.co.enerweb.ebr.adf.dummy.model.EDummy2;
import za.co.enerweb.ebr.adf.dummy.model.TTypeDummy1;
import za.co.enerweb.ebr.adf.dummy.model.TTypeDummy2;
import za.co.enerweb.ebr.adf.entities.CmReltypedef;
import za.co.enerweb.ebr.adf.entities.CmTypedef;
import za.co.enerweb.ebr.adf.entities.IbInstance;
import za.co.enerweb.ebr.adf.entities.IbInstanceParameter;
import za.co.enerweb.ebr.adf.entities.Relationship;
import za.co.enerweb.ebr.adf.model.InstanceRelationship;
import za.co.enerweb.toolbox.vaadin.lazytable.SortSpec;

public class TestAdfDbLayer extends AAdfDbLayerWithDummiesTest {
    // public void setUpAfterInjection() throws Exception {
    // super.setUpAfterInjection();
    // }

    @Test
    public void testFindAllTypeDefs() {
        List<CmTypedef> cmTypedefs = dbl.findAll(CmTypedef.class);
        assertGotTypes(cmTypedefs);
    }

    @Test
    public void testFindTypeDefs() {
        int typeCount = allTypes.size();
        List<CmTypedef> cmTypedefs = dbl.find(CmTypedef.class, 0, typeCount,
            new SortSpec(), null);
        assertGotTypes(cmTypedefs);

        cmTypedefs = dbl.find(CmTypedef.class, typeCount - 1, typeCount,
            new SortSpec(), null);
        assertEquals(Arrays.asList(typeDummy2), cmTypedefs);

        cmTypedefs = dbl.find(CmTypedef.class, 1, typeCount - 2,
            new SortSpec().addProperty("caption", false), null);
        assertEquals(Arrays.asList(getDynamicDummy(), typeDummy2, typeDummy1),
            cmTypedefs);
    }

    @Test
    public void testSaveAndLoadAdfInstanceArrayParam() {
        List<Double> value = Arrays.asList(0.1, 0.2);
        dummy1InstanceA.setDummyNumberArray(value);
        dbl.saveAdfInstance(dummy1InstanceA);
        dummy1InstanceA = dbl.findAdfInstance(dummy1InstanceA, true);
        assertEquals(value, dummy1InstanceA.getDummyNumberArray());
    }

    @Test
    public void testGetArray() {
        List<IbInstanceParameter> res = dbl.getSingleArrayParamValue(
            iDummy1InstanceA, PARAM_DUMMY_NUMBER_ARRAY, END
            .toDate());
        assertEquals(ARRAY_SIZE, res.size());
        assertEquals(VALUE_END + 0.1,
            dbl.getParamValue(res.get(0), cmParamdefNumberArray));
        assertEquals(VALUE_END + ARRAY_SIZE - 1 + 0.1,
            dbl.getParamValue(res.get(ARRAY_SIZE - 1), cmParamdefNumberArray));
    }

    @Test
    public void testHierarchies() {
        assertEquals(dummy1InstanceA, dummy2InstanceC.getTypeDummy1hier());
    }

    @Test
    public void testRelationship() {
        CmReltypedef relTypedef = dbl.findRelTypedef(
            REL_DUMMY1__DUMMY2);

        List<IbInstance> relatedInstances
            = dbl.findRelatedInstances(iDummy1InstanceA, relTypedef);
        assertEquals(1, relatedInstances.size());
        assertEquals(iDummy2InstanceC, relatedInstances.get(0));
        assertEquals(iDummy2InstanceC.getCaption(),
            relatedInstances.get(0).getCaption());

        relatedInstances
            = dbl.findRelatedInstancesTo(iDummy2InstanceC, relTypedef);
        assertEquals(Arrays.asList(iDummy1InstanceA, iDummy1InstanceB),
            relatedInstances);

        Relationship relationship = dbl.findRelationship(iDummy1InstanceA,
            iDummy2InstanceC, relTypedef);
        assertEquals(dummy1ADummy2CRel1, relationship);

        assertEquals(Arrays.asList(
            new InstanceRelationship(iDummy1InstanceA, iDummy2InstanceC,
                relTypedef),
            new InstanceRelationship(iDummy1InstanceB, iDummy2InstanceC,
                relTypedef)),
            dbl.findRelationships(relTypedef));

        TTypeDummy1 d1A = dbl.findAdfInstance(EDummy1.class,
            iDummy1InstanceA.getId());
        TTypeDummy2 d2C = dbl.findAdfInstance(EDummy2.class,
            iDummy2InstanceC.getId());
        assertEquals(d2C, d1A.getTypeDummy2());
        assertEquals(d1A, d2C.getTypeDummy1().get(0));

        assertEquals(1, dbl.deleteRelations(relTypedef, iDummy1InstanceA));
        assertEquals(1, dbl.deleteRelations(relTypedef, iDummy2InstanceC));
    }

    @Test
    public void testDeleteInstance() {
        dbl.removeInstances(iDummy1InstanceA);
        assertNull(dbl.findInstance(iDummy1InstanceA.getCmTypedef(),
            iDummy1InstanceA.getId()));
    }


    // FIXME: in order to do these tests we need a proper db manager
    // that can populate us the common metadata and some test data..
    // @Test
    // public void testBuildEntityTree() {
    // List<TreeNode> nodes = dbl.buildEntityTree(new TreeSpec(
    // "LOAD", "DSM").relationshipsWanted());
    // assertTrue(nodes.size() > 0);
    //
    // TreeNode loadNode = assertNode(nodes, null,
    // "LOAD", null, true);
    //
    // TreeNode asLoadNode = assertNode(loadNode.getChildren(), null,
    // "AREA_SECTOR_LOAD", null, true);
    // assertNode(asLoadNode.getChildren(), 1, null,
    // "Cape - Households", true);
    //
    // TreeNode dsmNode = assertNode(nodes, null,
    // "DSM", null, true);
    // TreeNode rDSM = assertNode(dsmNode.getChildren(), null,
    // "RESIDENTIAL", null, true);
    //
    // // Simon didn't say we wan't the weather entities
    // assertNode(nodes, null, "WEATHER", null, false);
    //
    // // see if we can read parameters
    // IbInstance asLoadInstance = (IbInstance) asLoadNode
    // .getChildren().iterator().next().getData();
    // readFirstParameter(asLoadInstance);
    //
    // IbInstance rDSMInstance = (IbInstance) rDSM
    // .getChildren().iterator().next().getData();
    // readFirstParameter(rDSMInstance);
    // }
    //
    // private TreeNode assertNode(Collection<TreeNode> nodes,
    // Integer id, String key, String caption, boolean mustExist) {
    // boolean found = false;
    // TreeNode ret = null;
    // for (TreeNode node : nodes) {
    // // log.debug("tree node" + node.toString());
    // if (equalProperty(id, node, "id")
    // || equalProperty(key, node, "key")
    // || equalProperty(key, node, "entity")) {
    // log.debug("matched tree node" + node.toString());
    // if (caption != null) {
    // assertEquals(caption, node.getCaption());
    // }
    // found = true;
    // ret = node;
    // }
    // }
    // assertEquals("Could not find node id=" + id + " or key=" + key,
    // mustExist, found);
    // return ret;
    // }
    //
    // private boolean equalProperty(Object expected, TreeNode node,
    // String name) {
    // return expected != null && expected.equals(getProperty(node, name));
    // }
    //
    // @SneakyThrows
    // private Object getProperty(TreeNode node, String name) {
    // if (PropertyUtils.isReadable(node.getData(), name)) {
    // return PropertyUtils.getSimpleProperty(
    // node.getData(), name);
    // }
    // return null;
    // }
    //
    // // @Test
    // // public void testGetInstanceParameterValues() {
    // // ILoad load = dbl.find(ILoad.class, 1);
    // // readFirstParameter(load);
    // // }
    //
    // private List<IbInstanceParameterNorm> readFirstParameter(
    // IbInstance instance) {
    // Collection<CmParamdef> paramDefs = dbl.get(
    // instance.getCmTypedef(), "cmParamdefs");
    // CmParamdef cmParamdef = paramDefs.iterator().next();
    // // log.debug(cmParamdef.toString());
    // List<IbInstanceParameterNorm> params =
    // dbl.getParamValues(
    // instance, cmParamdef);
    // // for (IbInstanceParameter parameter : parms) {
    // // log.debug(parameter.toString());
    // // }
    // assertFalse(params.isEmpty());
    // return params;
    // }
    //
    // @Test
    // public void testGetInputNumberTimeSeries() {
    // // 3 LOAD AREA_SECTOR_LOAD Load Load in an area
    // // 1 Cape - Households 3
    // assertNumberTimeSeries("AREA_SECTOR_LOAD", 1, "LOAD");
    // }
    //
    // @Test
    // public void testGetResultNumberTimeSeries() {
    // // 5 POWERSTATION COAL_PS Coal Powerstation
    // // Coal Powerstation with coal specific parameters
    // // 10 Arnot 5
    // // 5 COSTOFSUPPLY_4 Cost of Supply [R/MW] (4) Coal Station -
    // // Cost of Supply [R/MW] (4) NUMBER PU null RESULT null
    // assertNumberTimeSeries("COAL_PS", 10, "COSTOFSUPPLY_4");
    // }
    //
    // // skip this for now
    // // @Test
    // // public void testGetDenormNumberTimeSeries() {
    // // fail("I have no data to test this");
    // // // assertNumberTimeSeries("COAL_PS", 10, "COSTOFSUPPLY_4");
    // // }
    //
    // private void assertNumberTimeSeries(String typedefKey, int instanceId,
    // String paramdefKey) {
    // IbInstance instance = dbl.findInstance(typedefKey, instanceId);
    // log.debug(instance.toString());
    // CmParamdef loadParameterDef = dbl.findParameterDef(
    // instance.getCmTypedef(), paramdefKey);
    // log.debug(loadParameterDef.toString());
    // ITimeSeries<Double> ts = dbl.getNumberTimeSeries(
    // instance, loadParameterDef);
    // for (ITimeSeriesItem<Double> item : ts) {
    // log.debug(item.toString());
    // }
    // assertTrue("get some data", ts.size() > 0);
    // }
    //
    // @Test
    // public void testGetResultNumberArraySummaryTimeSeries() {
    // // 7 LOAD SYSTEM_LOAD System Load Load Aggregated to System Level
    // // 31 LOAD-SYSTEM_LOAD Thu Mar 17 22:52:32 2011 7
    // assertNumArraySummaryTimeSeries("SYSTEM_LOAD", 31, "LOAD");
    // }
    //
    // @Test
    // public void testGetDenormNumberArraySummaryTimeSeries() {
    // // TYPEDEF_ID KEY CAPTION DESCRIPTION DATATYPE_KEY
    // // UNIT_OF_MEASURE_KEY INDX_DATATYPE_KEY PARAMKIND_KEY PARAM_TABLE
    // // 1 RAINFALL_O Rainfall Rainfall from SAWS NUM_ARRAY MM
    // // NUMBER DENORM I_WEATHER_WEATHERSA_OUT
    // // ID ENTITY KEY CAPTION DESCRIPTION
    // // 1 WEATHER WEATHERSA Weather Weather data from weather bureau
    // assertNumArraySummaryTimeSeries("WEATHERSA", 1, "RAINFALL_O");
    // }
    //
    // private void assertNumArraySummaryTimeSeries(String typedefKey,
    // int instanceId, String paramdefKey) {
    // IbInstance instance = dbl.findInstance(typedefKey, instanceId);
    // log.debug(instance.toString());
    // CmParamdef cmParamDef = dbl.findParameterDef(
    // instance.getCmTypedef(), paramdefKey);
    // log.debug(cmParamDef.toString());
    // ITimeSeries<NumArraySummary> ts = dbl.getNumArraySummaryTimeSeries(
    // instance, cmParamDef);
    // for (ITimeSeriesItem<NumArraySummary> item : ts) {
    // log.debug(item.toString());
    // }
    // assertTrue("get some data", ts.size() > 0);
    // }
    //
    // @Test
    // public void testGetDenormNumberArrayProbability() {
    // // TYPEDEF_ID KEY CAPTION DESCRIPTION DATATYPE_KEY
    // // UNIT_OF_MEASURE_KEY INDX_DATATYPE_KEY PARAMKIND_KEY PARAM_TABLE
    // // 1 RAINFALL_O Rainfall Rainfall from SAWS NUM_ARRAY MM
    // // NUMBER DENORM I_WEATHER_WEATHERSA_OUT
    // // ID ENTITY KEY CAPTION DESCRIPTION
    // // 1 WEATHER WEATHERSA Weather Weather data from weather bureau
    // assertNumArrayProbability("WEATHERSA", 1, "RAINFALL_O");
    // }
    //
    // private void assertNumArrayProbability(String typedefKey,
    // int instanceId, String paramdefKey) {
    // IbInstance instance = dbl.findInstance(typedefKey, instanceId);
    // log.debug(instance.toString());
    // CmParamdef cmParamDef = dbl.findParameterDef(
    // instance.getCmTypedef(), paramdefKey);
    // log.debug(cmParamDef.toString());
    // ProbabilityInfo prob = dbl.getNumArrayProbability(
    // instance, cmParamDef);
    // log.debug(Arrays.toString(prob.getMinValues()));
    // assertTrue("get some data", prob.getMinValues().length > 0);
    // log.debug(Arrays.toString(prob.getMaxValues()));
    // assertTrue("get some data", prob.getMaxValues().length > 0);
    // }
}
