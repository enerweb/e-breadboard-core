package za.co.enerweb.ebr.adf.dummy.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import za.co.enerweb.ebr.adf.entities.CmParamdef;
import za.co.enerweb.ebr.adf.entities.IbInstanceParameterNorm;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, of = { })
@ToString(callSuper = true, of = { })
@Entity
@Table(name = "I_DUMMY1_PAR",
    uniqueConstraints = {
        @UniqueConstraint(columnNames = {"TYPEDEF_ID", "DUMMY1_ID",
            "KEY", "APPLDATETIME",
            "INDX"})})
public class IDummy1Par extends IbInstanceParameterNorm {
    private static final long serialVersionUID = 1L;

    @JoinColumn(name = "DUMMY1_ID", referencedColumnName = "ID")
    @ManyToOne
    private IDummy1 iDummy1;

    public IDummy1Par(Integer id) {
        super(id);
    }

    public IDummy1Par(Integer id, Date appldatetime, String val) {
        super(id, appldatetime, val);
    }

    public IDummy1Par(CmParamdef cmParamdef, Date appldatetime,
        String val, IDummy1 iCoalsupply) {
        super(cmParamdef, appldatetime, val);
        this.iDummy1 = iCoalsupply;
    }

}
