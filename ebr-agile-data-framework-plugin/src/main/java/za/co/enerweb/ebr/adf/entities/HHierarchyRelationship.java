package za.co.enerweb.ebr.adf.entities;

import static za.co.enerweb.ebr.adf.dbl.AdfLowlevelDbLayer.H_;
import static za.co.enerweb.ebr.adf.entities.HHierarchyRelationship.ENTITY_HIERARCHY_RELATIONSHIP;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * stores links between instance
 */
@Data
@EqualsAndHashCode(callSuper = true, of = { })
@ToString(callSuper = true, of = { })
@NoArgsConstructor
@Entity
@Table(name = H_ + ENTITY_HIERARCHY_RELATIONSHIP,
    uniqueConstraints = {
        @UniqueConstraint(columnNames = {"HIERARCHY_ID",
            "PARENT_TYPE_ID", "PARENT_ID", "CHILD_TYPE_ID", "CHILD_ID",
            "START_DATETIME", "END_DATETIME"})})
public class HHierarchyRelationship extends HierarchyRelationship {
    private static final long serialVersionUID = 1L;

    public static final String ENTITY_HIERARCHY_RELATIONSHIP
        = "HIERARCHY_RELATIONSHIP";

}
