package za.co.enerweb.ebr.adf.data_types;

import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.val;
import za.co.enerweb.ebr.adf.dbl.AdfDbLayer;
import za.co.enerweb.ebr.adf.entities.IbInstance;

/**
 * A geographical point
 * Decimal degrees (DD) express latitude and longitude geographic
 * coordinates as decimal fractions and are used in many Geographic
 * Information Systems (GIS), web mapping applications such as
 * Google Maps, and GPS devices. Decimal degrees are an alternative to
 * using degrees, minutes, and seconds (DMS). As with latitude and
 * longitude, the values are bounded by ±90° and ±180° respectively.
 * Positive latitudes are north of the equator, negative latitudes are
 * south of the equator. Positive longitudes are east of Prime Meridian,
 * negative longitudes are west of the Prime Meridian. Latitude and
 * longitude are usually expressed in that sequence, latitude before longitude.
 * http://en.wikipedia.org/wiki/Decimal_degrees
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class GisLocation {

    /*
     * Latitude and
     * longitude are usually expressed in that sequence,
     * latitude before longitude.
     * http://en.wikipedia.org/wiki/Decimal_degrees
     */
    private double latitude;
    private double longitude;

    @Override
    public String toString() {
        return latitude + "," + longitude;
    }

    /**
     * @return a list of lists of locations because an instance may have
     *         multiple location parameters each of which can be a point, line
     *         or polygon and I don't thing we wanna mix them.
     * @deprecated use AdfFactory.getGisDbLayer().getGisLocations()
     */
    @Deprecated
    public static List<List<GisLocation>> getGisLocations(
        AdfDbLayer dbl, IbInstance instance, Date effectiveDate) {
        return dbl.getAdfFactory().getGisDbLayer().getGisLocations(instance,
            effectiveDate);
    }

    private static Pattern POINT_PATTERN = Pattern.compile(
        "POINT \\(([\\d\\.-]+) ([\\d\\.-]+)\\)");

    public static GisLocation parseWellKnownTextPoint(String wkt) {
        if (wkt != null) {
            // POINT (20.06 -34.8)
            val m = POINT_PATTERN.matcher(wkt);
            if (m.matches()) {
                return new GisLocation(Double.parseDouble(m.group(2)),
                    Double.parseDouble(m.group(1)));
            }
        }
        return null;
    }
}
