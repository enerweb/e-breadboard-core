/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package za.co.enerweb.ebr.adf.rest.model;

import java.util.Collection;

import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import lombok.NoArgsConstructor;
import za.co.enerweb.toolbox.rest.Items;
import za.co.enerweb.toolbox.rest.Meta;

@XmlRootElement(name = RCProperties.URI_FRAGMENT)
@XmlSeeAlso({RCProperty.class, Items.class})
@NoArgsConstructor
public class RCProperties extends Items<RCProperty> {

    public final static String URI_FRAGMENT = "Properties";

    public RCProperties(Meta meta, Collection<RCProperty> items) {
        super(meta, items);
    }

    public RCProperties(UriInfo uriInfo, Collection<RCProperty> items) {
        this(
            new AdfMetaBuilder(uriInfo).add(URI_FRAGMENT).build(),
            items);
    }
}
