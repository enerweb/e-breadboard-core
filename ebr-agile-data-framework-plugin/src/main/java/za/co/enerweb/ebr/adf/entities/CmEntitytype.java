package za.co.enerweb.ebr.adf.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import za.co.enerweb.ebr.adf.model.EntityType;

@Data
@NoArgsConstructor
@EqualsAndHashCode(of="key")
@ToString(of={"key", "description"})
@Entity
@Table(name = "CM_ENTITYTYPE")
@NamedQueries({
    @NamedQuery(name = "CmEntitytype.findAll", query = "SELECT c FROM CmEntitytype c"),
    @NamedQuery(name = "CmEntitytype.findByKey", query = "SELECT c FROM CmEntitytype c WHERE c.key = :key"),
    @NamedQuery(name = "CmEntitytype.findByDescription", query = "SELECT c FROM CmEntitytype c WHERE c.description = :description")})
public class CmEntitytype implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "KEY", nullable = false, length = 25)
    private String key;
    @Basic(optional = false)
    @Column(name = "DESCRIPTION", nullable = false, length = 100)
    private String description;
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "cmEntitytype")
    private Collection<CmEntity> cmEntityCollection;

    public CmEntitytype(String key) {
        this.key = key;
    }

    public CmEntitytype(String key, String description) {
        this.key = key;
        this.description = description;
    }

    public EntityType getEnityType(){
        return EntityType.valueOf(key);
    }

}
