package za.co.enerweb.ebr.adf.data_types;

import za.co.enerweb.ebr.adf.model.AdfDataType;
import za.co.enerweb.ebr.adf.model.AdfDataTypeClass;

public class DtAdfResourceUrlArray extends DtAdfResourceUrl {

    public AdfDataTypeClass getAdfDataTypeClass() {
        return AdfDataTypeClass.ARRAY;
    }

    public Class<? extends AdfDataType<?>> getItemDataType() {
        return DtNumber.class;
    }

}
