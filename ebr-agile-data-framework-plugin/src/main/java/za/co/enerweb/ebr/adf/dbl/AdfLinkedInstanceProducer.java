package za.co.enerweb.ebr.adf.dbl;

import java.util.List;

import lombok.val;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.adf.AdfFactory;
import za.co.enerweb.ebr.adf.model.AdfInstance;
import za.co.enerweb.toolbox.vaadin.lazytable.SortSpec;
import za.co.enerweb.toolbox.vaadin.lazytable.filter.AFilter;

/**
 * The row objects/ids are of type AdfInstance.
 * At the moment linkedInstance is assumed to be the parent
 */
@Slf4j
public class AdfLinkedInstanceProducer<T extends AdfInstance>
    extends AdfInstanceProducer<T> {

    private final AdfFactory adfFactory;
    private final AdfInstance linkedInstance;
    private final String hierarchyKey;
    private List<T> validInstances;

    /**
     * @param propertyInfosTemplate may contain Strings which will imply
     *        propertyIds or PropertyInfo objects (they may be partial too)
     *        If specified, only properties in this list will be used in the
     *        order
     *        given.
     */
    public AdfLinkedInstanceProducer(Class<T> dataClass,
        List<?> propertyInfosTemplate,
        AdfFactory adfFactory,
        AdfInstance linkedInstance, String hierarchyKey) {
        super(dataClass, propertyInfosTemplate, adfFactory);
        this.adfFactory = adfFactory;
        this.linkedInstance = linkedInstance;
        this.hierarchyKey = hierarchyKey;
        reload();
    }


    @Override
    public void reload() {
        super.reload();
        AdfDbLayer adfDbLayer = adfFactory.getAdfDbLayer();
        validInstances = adfDbLayer.findHierarchyNeighbourAdfInstances(
            hierarchyKey,
            linkedInstance, null, true);
        log.debug("found " + validInstances.size() + " validInstances " +
            hierarchyKey + " " + linkedInstance);
    }

    @Override
    public int count(AFilter aFilter) {
        // FIXME: support filtering, but do we want to?
        return validInstances.size();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<T> find(int startIndex, int count, SortSpec sortSpec,
        AFilter aFilter) {
        return validInstances.subList(startIndex, startIndex + count);
    }

    @Override
    public void saveItems(final List<T> addedItems,
        final List<T> modifiedItems,
        final List<T> removedItems) {
        super.saveItems(addedItems, modifiedItems, removedItems);
        val dbl = adfFactory.getAdfDbLayer();

        // link new items to this hierarchy
        for (T t : addedItems) {
            dbl.linkHierarchyInstances(hierarchyKey,
                dbl.findInstance(linkedInstance),
                dbl.findInstance(t), null, null);
            validInstances.add(t);
        }
    }

    public T newInstance() {
        val ret = super.newInstance();
        return ret;
    }

}
