package za.co.enerweb.ebr.adf.data_types;

import za.co.enerweb.ebr.adf.model.AdfScalarDataType;

@SuppressWarnings("rawtypes")
public abstract class DtEnum<T extends Enum> extends
    AdfScalarDataType<T> {

    public final String toPersistedValue(T value) {
        return ((T) value).name();
    }

    @SuppressWarnings("unchecked")
    public final T fromPersistedValue(String persistedValue) {
        return (T) Enum.valueOf(getJavaType(), persistedValue);
    }

}
