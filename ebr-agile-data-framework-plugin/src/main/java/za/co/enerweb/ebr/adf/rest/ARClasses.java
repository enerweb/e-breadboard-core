package za.co.enerweb.ebr.adf.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.adf.rest.model.RCClass;
import za.co.enerweb.ebr.adf.rest.model.RCClasses;
import za.co.enerweb.toolbox.rest.ExpandMembers;
import za.co.enerweb.toolbox.rest.ResponsTypeUtil;


@Path("/" + RCClasses.URI_FRAGMENT + ResponsTypeUtil.PATH)
@Slf4j
public abstract class ARClasses extends ARConcept {

    @GET
    public Response getAll(@Context UriInfo uriInfo,
        @PathParam(ResponsTypeUtil.PARAM) String responseType,
        @QueryParam(ExpandMembers.PARAM) String expand) {

        return ResponsTypeUtil.parse(
            Response.ok(getRDbl().getAllRCClasses(uriInfo,
                    new ExpandMembers(RCClass.URI_FRAGMENT, expand))),
            responseType).build();
    }

}
