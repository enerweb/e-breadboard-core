package za.co.enerweb.ebr.datatype.dataresource;


/**
 * @deprecated use {@link za.co.enerweb.ebr.adf.data_types.AdfResourceUrl}
 */
public abstract class AdfResourceUrl {
    private AdfResourceUrl() {
    }
}
