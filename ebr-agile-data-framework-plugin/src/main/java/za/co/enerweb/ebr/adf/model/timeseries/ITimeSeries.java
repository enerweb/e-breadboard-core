package za.co.enerweb.ebr.adf.model.timeseries;

import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.SortedMap;

/*
 * This is not iterable any more because in future we may want to implement
 * different types of iterators. So rather use .sparseIterable() if you
 * want a nice iterable that you can use in foreach loops.
 */
public interface ITimeSeries<V>
    extends SortedMap<Date, V>,
    Serializable {

    /**
     * Returns an iterator backed by the collection.
     * This iterator iterates over the items actually stored
     * in the series, so it may be sparse.
     * @deprecated use sparseIterator()
     *             we don't want to use a iterator method otherwise
     *             CompositeTimeSeries
     *             can not implement this and some collection simultaneously.
     *             Also iterator was a bit ambiguous
     */
    Iterator<ITimeSeriesItem<V>> iterator();

    /**
     * Returns an iterator backed by the collection.
     * This iterator iterates over the items actually stored
     * in the series, so it may be sparse.
     * FIXME: change to :
     * Iterable<Map.Entry<Date, V>> sparseIterator();
     */
    Iterable<ITimeSeriesItem<V>> sparseIterable();

    // TODO: add a dense iterator that fills in the gaps
    // Iterator<ITimeSeriesItem<V>> iterator(granularity, start, end);

    void put(TimeSeriesItem<V> timeSeriesItem);

    Date getFirstDate();

    Date getLastDate();

    ITimeSeriesItem<V> floorEntry(Date date);
}
