package za.co.enerweb.ebr.adf.dbl;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import za.co.enerweb.ebr.adf.model.AdfDataType;

@NoArgsConstructor
@AllArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Accessors(fluent = true, chain = true)
public class ParameterFilter<T> {
    String parameterKey;
    T value;
    AdfDataType<T> dataType;
    boolean like; // only useful for text values

    public ParameterFilter(
        String parameterKey, AdfDataType<T> dataType, T value) {
        super();
        this.parameterKey = parameterKey;
        this.dataType = dataType;
        this.value = value;
    }
}
