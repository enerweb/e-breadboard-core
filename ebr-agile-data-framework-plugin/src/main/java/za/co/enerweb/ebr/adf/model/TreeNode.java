package za.co.enerweb.ebr.adf.model;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
// this needs to be unique in the whole tree:
@EqualsAndHashCode(of = {"data"})
@ToString(exclude = {"data"})
public class TreeNode implements Serializable, Comparable<TreeNode> {
    private static final long serialVersionUID = 1L;

    private String caption;
    private String description;
    private Serializable data;
    private String dataClassName;
    private Set<TreeNode> children = new TreeSet<TreeNode>();
    private boolean expandByDefault = false;

    /**
     * @param key may be null if id is specified
     * @param caption
     * @param description
     * @param data
     */
    public TreeNode(String caption, String description, Serializable data) {
        this.caption = caption;
        this.description = description;
        this.data = data;
        if (data != null) {
            dataClassName = data.getClass().getSimpleName();
        }
    }

    @Deprecated
    public TreeNode(Integer id, String key, String typedefKey,
        String caption, String description, Serializable jpaEntity) {
        this(caption, description, jpaEntity);
    }

    public boolean hasChildren() {
        return !children.isEmpty();
    }

    /**
     * Sort by captions
     */
    @Override
    public int compareTo(TreeNode o) {
        return caption.compareTo(o.caption);
    }

    /**
     * @param node
     */
    public void addChild(TreeNode node) {
        children.add(node);
    }

    /**
     * @return
     * @deprecated use getData
     */
    @Deprecated
    public Object getJpaEntity() {
        return data;
    }

}
