package za.co.enerweb.ebr.adf;

import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EntityManager;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.SneakyThrows;
import lombok.val;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;
import org.reflections.Reflections;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import za.co.enerweb.ebr.adf.config.json.MetadataIO;
import za.co.enerweb.ebr.adf.dbl.AdfDbLayer;
import za.co.enerweb.ebr.adf.dbl.AdfRegistry;
import za.co.enerweb.ebr.adf.dbl.AdfRestDbLayer;
import za.co.enerweb.ebr.adf.dbl.GisDbLayer;
import za.co.enerweb.ebr.adf.entities.IbInstance;
import za.co.enerweb.ebr.adf.model.AdfInstance;
import za.co.enerweb.ebr.annotations.OnEbrReload;
import za.co.enerweb.ebr.edbm.DBManagerFactory;
import za.co.enerweb.ebr.edbm.DbSpec;
import za.co.enerweb.ebr.edbm.EmbeddedDbSpec;
import za.co.enerweb.ebr.edbm.dbl.EdbmDbLayer;
import za.co.enerweb.ebr.gui.vaadin.menu.ActivateScreenMenuItem;
import za.co.enerweb.ebr.server.Registrar;

import com.google.common.base.Joiner;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.vaadin.ui.Component;

/**
 * Factory for everything you need in a ADF!
 * This more or less follows the Abstract Factory pattern.
 * It basically allows apps to return a specialised Dblayer
 * and TreeIconProducer etc. .
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Slf4j
public abstract class AdfFactory extends DBManagerFactory
implements Serializable {
    public static final String MODULE_NAME = "ebr-agile-data-framework-plugin";
    private static final long serialVersionUID = 1L;

    private List<String> entityPackages = findEntityPackages();
    // private static Set<String> checkedDbs = new HashSet<String>();
    private static Map<String, Cache<IbInstance, AdfInstance>>
    adfInstanceCache =
        new HashMap<String, Cache<IbInstance, AdfInstance>>();
    // XXX: we should uncache anything modified if a transaction is rolled back
    // also uncache when saving any ib instance or any property of it?!


    public AdfRegistry getAdfRegistry() {
        return AdfRegistry.getRegistry(getClass());
    }

    // We can make this protected again if we really need to use non-embedded
    // database.
    // private AdfFactory(DbSpec dbSpec) {
    // this.dbSpec = dbSpec;
    // getDbManager().warmUp(dbSpec);
    // }

    public final EntityManager getEntityManager(String persistenceUnitName) {
        return getDbManager().getEntityManager(getDbSpec(persistenceUnitName));
    }

    /**
     * This can instantiate any JPA enity for you.
     * (Provided we can find it of coarse)
     */
    @SneakyThrows
    // if you give an invalid entity it is your own problem.
    @SuppressWarnings("unchecked")
    public <I> I instantiateJpaEntity(String jpaEntityName) {
        return (I) getJpaEntityClass(jpaEntityName).newInstance();
    }

    /**
     * This can instantiate any JPA enity for you.
     * (Provided we can find it of coarse)
     */
    @SneakyThrows
    // if you give an invalid entity it is your own problem.
    @SuppressWarnings("unchecked")
    public <I> Class<I> getJpaEntityClass(String jpaEntityName) {
        for (String entityPackage : entityPackages) {
            try {
                return (Class<I>) Class.forName(
                    entityPackage + jpaEntityName);
            } catch (ClassNotFoundException e) {
                // not here hey, lets try the next one..
            }
        }
        throw new ClassNotFoundException("Class '" + jpaEntityName
            + "' was not found in any of the following packages:\n"
            + Joiner.on("\n").join(entityPackages));
    }

    public Collection<Class<?>> findAllEntities() {
        Set<URL> urlsForCurrentClasspath = new HashSet<URL>();
        for (String entityPackage : entityPackages) {
            urlsForCurrentClasspath.addAll(ClasspathHelper
                .forPackage(entityPackage));
        }
        Reflections reflections = new Reflections(new ConfigurationBuilder()
            .setUrls(urlsForCurrentClasspath).setScanners(
            new TypeAnnotationsScanner()));
        return reflections.getTypesAnnotatedWith(Entity.class);
    }

    public static Set<Class<? extends AdfFactory>> findAllAdfFactories() {
        return Registrar.getReflections().getSubTypesOf(AdfFactory.class);
    }

    /**
     * Override this for your plugin eg.
     * <pre>
     * &#064;SuppressWarnings(&quot;unchecked&quot;)
     * public DummyAdfDbLayer getDbLayer() {
     *     return (DummyAdfDbLayer) super.getDbLayer();
     * }
     * </pre>
     */
    // after a while I don't think we should cast it for the guy no more
// public AdfDbLayer getDbLayer() {
// AdfDbLayer dbLayer = getDbLayer(
// (Class<? extends AdfDbLayer>) getDbLayerClass());
// try {
// return dbLayer;
// } catch (ClassCastException e) {
// throw new IllegalStateException(e.getMessage()
// + " (The Dblayer type gets determined by getDblayerClass)", e);
// }
// }


    public <D extends EdbmDbLayer> D getDbLayer() {
        return (D) getAdfDbLayer();
    }

    public GisDbLayer getGisDbLayer() {
        return getAdfDbLayer(GisDbLayer.class);
    }

    public AdfRestDbLayer getAdfRestDbLayer() {
        return getAdfDbLayer(AdfRestDbLayer.class);
    }

    public MetadataIO getMetadataIO() {
        return getAdfDbLayer(MetadataIO.class);
    }

    @SuppressWarnings("unchecked")
    public <D extends AdfDbLayer> D getAdfDbLayer() {
        Class<AdfDbLayer> defualtDbLayerClass =
            (Class<AdfDbLayer>) (Class<?>) getDefaultDbLayerClass();
        return (D) getAdfDbLayer(getDefaultPersistencUnitName(),
            defualtDbLayerClass);
    }

    public <D extends AdfDbLayer> D getAdfDbLayer(Class<D> dblClass) {
        return (D) getAdfDbLayer(getDefaultPersistencUnitName(), dblClass);
    }

    @SuppressWarnings("unchecked")
    protected synchronized final <D extends AdfDbLayer> D getAdfDbLayer(
        String persistenceUnitName,
        Class<D> dblClass) {
        EdbmDbLayer dbl =
            super.getDbLayer(persistenceUnitName, (Class<D>) dblClass);
        if (dbl instanceof AdfDbLayer) {
            AdfDbLayer ret = (AdfDbLayer) dbl;

            ret.setAdfFactory(this);
            // if this db has not been checked, then check it
            // but only do it once per vmstart per unique dbspec..
            String dbSpecId = ret.getDbSpec().getId();
            Cache<IbInstance, AdfInstance> cache =
                adfInstanceCache.get(dbSpecId);
            if (cache == null) {
                synchronized (adfInstanceCache) {
                    cache = adfInstanceCache.get(dbSpecId);
                    if (cache == null) {
                        // log.debug("##### checking: " + dbSpecId);
                        // only check once even if it fails, so we can avoid
                        // recursive checks
                        cache = CacheBuilder.newBuilder()
                            .maximumSize(20000)
                            // .softValues() - cant use this because then it
                            // doesn't use .equals
                                .build();
                        adfInstanceCache.put(dbSpecId, cache);
                        ret.setAdfInstanceCache(cache);
                        ret.checkMetadata();
                    }
                }
            } else {
                ret.setAdfInstanceCache(cache);
            }
            return (D)ret;
        }
        throw new IllegalArgumentException(
            dblClass + " does not extend AdfDbLayer.");
    }

    public <DBSPEC extends DbSpec> DBSPEC getDbSpec() {
        return getDbSpec(getDefaultPersistencUnitName());
    }

    protected DbSpec createDbSpec(String persistenceUnitName, String id) {
        val ret = super.createDbSpec(persistenceUnitName, id);
        setProperty(ret, EmbeddedDbSpec.QUALIFIED_PACKAGE_NAME,
            getVarPackageName());
        return ret;
    }

    // vvv You have to implement this
    /**
     * @return then name of EBR package which includes the static tasks
     *         and resources eg.za.co.enerweb_adf-example
     */
    public abstract String getPackageName();


    // vvv You should override this
    /**
     * this will be used for filenames etc. so don't include spaces and
     * funny characters
     */
    public String getAppName() {
        return StringUtils.replace(getClass().getSimpleName(), "Factory", "");
    }

    // vvv Override these if you need to

    public String getDefaultPersistencUnitName() {
        // Use the module name, because the PU
        // tends to be module specific.
        // And one less different thing to configure.
        return getModuleName();
    }

    /**
     * @return then name of EBR package which includes the database
     *         and variable resources
     */
    public String getVarPackageName() {
        return getPackageName() + "-var";
    }

    // public IResourceProducer<Object> getTreeIconProducer(Component parent) {
    // return new AdfTreeIconProducer(parent);
    // }

    /**
     * By convention this will inject this factory into any fields called
     * adfFactory (by calling setAdfFactory)
     * This is needed by the generic screens so they work on the correct db.
     * It also automatically injects the more specific instances too
     * based on the actual factory class name eg. efsFactory
     * @param rank
     * @param caption
     * @param componentClass
     * @return
     */
    public ActivateScreenMenuItem getActivateScreenMenuItem(final int rank,
        final String caption,
        final Class<? extends Component> componentClass) {
        return new ActivateScreenMenuItem(
            componentClass.getName() + "?factory=" + getClass().getName(),
            rank, caption, componentClass)
            .set("adfFactory", this)
            .set(StringUtils.uncapitalize(getClass().getSimpleName()),
                this);
    }

    /**
     * Only override this if you want you always want customised trees.
     * @deprecated you should specify a TreeSpec
     */
    // public AdfTree getAdfTree(String caption) {
    // return new AdfTree(this, caption);
    // }

    /*
     * Only override this if you always want customised trees.
     */
    // public AdfTree getAdfTree(String caption,
    // TreeSpec treeSpec) {
    // return new AdfTree(this, caption, treeSpec);
    // }

    /**
     * This is a sneaky bit of magic to avoid manual/errorprone configuration.
     * We will try to load entities from your factory and its superclasses,
     * in each case assuming your module has a .entities package.
     * Eg. if you factory is in com.example.MyAdfFactory, then we will look in
     * com.example.entities and za.co.enerweb.ebr.adf.entities for entity
     * classes.
     * This should work for everybody so long as they follow the convention.
     * You can always override this if it doesn't work for you (call super,
     * and add whatever packages you like).
     */
    public List<String> findEntityPackages() {
        Class<?> klass = getClass();
        return findEntityPackages(klass);
    }

    /**
     * Add entity packages relative to this class and it's parent classes
     * (as long as they extend AdfFactory).
     */
    protected List<String> findEntityPackages(Class<?> klass) {
        List<String> ret = new ArrayList<String>(2); // 99% of cases
        while (AdfFactory.class.isAssignableFrom(klass)) {
            String e = klass.getPackage().getName() + ".entities.";
            //log.debug("Adding entities package: " + e);
            ret.add(e);
            klass = klass.getSuperclass();
        }
        return ret;
    }

    // vvv Internal
    /**
     * Used for testing - they have to be reset for every test..
     */
    @OnEbrReload
    public static void resetCheckedDbs() {
        adfInstanceCache.clear();
    }
}
