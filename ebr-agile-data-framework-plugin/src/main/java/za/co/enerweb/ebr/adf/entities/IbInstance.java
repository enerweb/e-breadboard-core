package za.co.enerweb.ebr.adf.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

// XXX: rename to Instance (deprecate and extend first)
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = {"cmTypedef", "id"})
@ToString(of = {"id", "caption", "cmTypedef"})
@MappedSuperclass
@Table(uniqueConstraints = {
    @UniqueConstraint(columnNames = {"TYPEDEF_ID", "ID"})})
public abstract class IbInstance implements Serializable,
    Comparable<IbInstance> {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id; // FIXME: should be long

    @Basic(optional = false)
    @Column(name = "CAPTION", nullable = false, length = 100)
    private String caption;

    @JoinColumn(name = "TYPEDEF_ID", referencedColumnName = "ID",
        nullable = false)
    @ManyToOne(optional = false)
    private CmTypedef cmTypedef;

    public IbInstance(Integer id, String caption) {
        this.id = id;
        this.caption = caption;
    }

    public IbInstance(CmTypedef cmTypedef, String caption) {
        this.cmTypedef = cmTypedef;
        this.caption = caption;
    }

    public int compareTo(IbInstance i) {
        return caption.compareTo(i.getCaption());
    }
}
