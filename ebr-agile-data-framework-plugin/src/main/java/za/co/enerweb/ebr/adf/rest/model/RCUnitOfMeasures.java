package za.co.enerweb.ebr.adf.rest.model;

import java.util.Collection;

import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import lombok.NoArgsConstructor;
import za.co.enerweb.toolbox.rest.Items;
import za.co.enerweb.toolbox.rest.Meta;

@XmlRootElement(name = RCUnitOfMeasures.URI_FRAGMENT)
@XmlSeeAlso({RCUnitOfMeasure.class, Items.class})
@NoArgsConstructor
public class RCUnitOfMeasures extends Items<RCUnitOfMeasure> {
    public final static String URI_FRAGMENT = "UnitOfMeasures";

    public RCUnitOfMeasures(Meta meta, Collection<RCUnitOfMeasure> items) {
        super(meta, items);
    }

    public RCUnitOfMeasures(UriInfo uriInfo, Collection<RCUnitOfMeasure> items) {
        this(
            new AdfMetaBuilder(uriInfo).add(URI_FRAGMENT).build(),
            items);
    }
}
