package za.co.enerweb.ebr.adf.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import za.co.enerweb.ebr.adf.dbl.AdfDbLayerUtils;
import za.co.enerweb.ebr.adf.model.AdfDataTypeClass;
import za.co.enerweb.ebr.adf.model.ParameterKind;
import za.co.enerweb.toolbox.string.StringUtils;

/**
 * @return
 */
@Data
@EqualsAndHashCode(of = {"cmParamdefPK", "caption"})
@ToString(of = {"cmParamdefPK", "caption"})
@NoArgsConstructor
@Entity
@Table(name = "CM_PARAMDEF")
public class CmParamdef implements Serializable, Comparable<CmParamdef> {
    private static final long serialVersionUID = 1L;
    // @Delegate
    @EmbeddedId
    private CmParamdefPK cmParamdefPK = new CmParamdefPK();
    @Basic(optional = false)
    @Column(name = "CAPTION", nullable = false, length = 100)
    private String caption;
    @Column(name = "DESCRIPTION", length = 400)
    private String description;
    @JoinColumn(name = "UNIT_OF_MEASURE_KEY", referencedColumnName = "KEY",
        nullable = true)
    @ManyToOne(optional = true)
    private CmUnitofmeasure cmUnitofmeasure;

    @JoinColumn(name = "PARAMKIND_KEY", referencedColumnName = "KEY",
        nullable = false)
    @ManyToOne(optional = false)
    private CmParamkind cmParamkind;
    @JoinColumn(name = "DATATYPE_KEY", referencedColumnName = "KEY")
    @ManyToOne(optional = false)
    private CmDatatype cmDatatype;
    @JoinColumn(name = "INDX_DATATYPE_KEY", referencedColumnName = "KEY")
    @ManyToOne
    private CmDatatype cmIndexDatatype;

    @Column(name = "PARAMKIND_KEY", insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private ParameterKind parameterKind;

    @Column(name = "PARAM_TABLE", length = 25)
    private String paramTable;

    public CmParamdef(CmTypedef cmTypedef, String key,
        String caption,
        String description,
        CmParamkind cmParamkind,
        CmDatatype cmDatatype,
        CmUnitofmeasure cmUnitofmeasure,
        CmDatatype cmIndexDatatype) {
        cmParamdefPK = new CmParamdefPK(cmTypedef, key);
        setCmTypedef(cmTypedef);
        if (caption == null) {
            caption = StringUtils.variableName2Caption(key);
        }
        this.caption = caption;
        this.description = description;
        setCmParamkind(cmParamkind);
        this.cmDatatype = cmDatatype;
        this.cmUnitofmeasure = cmUnitofmeasure;
        this.cmIndexDatatype = cmIndexDatatype;
    }

    public CmParamdefPK getCmParamdefPK() {
        return cmParamdefPK;
    }

    public CmDatatypeclass getCmDatatypeclass() {
        return cmDatatype.getCmDatatypeclass();
    }

    public AdfDataTypeClass getAdfDataTypeClass() {
        return cmDatatype.getAdfDataTypeClass();
    }

    public String getParamTableName() {
        switch (getParameterKind()) {
        case INPUT:
            return getCmTypedef().getParameterTableName();
        case RESULT:
            return getCmTypedef().getOutputTableName();
        case DENORM:
            return paramTable;
        default:
            throw new IllegalArgumentException("This method does not support "
                + "parameters of kind " + getParameterKind());
        }
    }

    public String getParamJpaEntity() {
        switch (getParameterKind()) {
        case INPUT:
            return getCmTypedef().getJpaParameterEntity();
        case RESULT:
            return getCmTypedef().getJpaOutputEntity();
        case DENORM:
            return AdfDbLayerUtils.paramTableToJpaEntity(paramTable);
        default:
            throw new IllegalArgumentException("This method does not support "
                + "parameters of kind " + getParameterKind());
        }
    }

    public String getValueProperty() {
        if (getParameterKind().equals(ParameterKind.DENORM)) {
            return AdfDbLayerUtils.paramKeyToJpaProperty(cmParamdefPK.getKey());
        }
        return "val";
    }

    public String getInstanceProperty() {
        return getCmTypedef().getParamInstanceProperty();
    }

    public String getInstanceColumn() {
        return getCmTypedef().getParamInstanceColumn();
    }

    public String getUnitOfMeasureCaption() {
        if (cmUnitofmeasure != null) {
            return cmUnitofmeasure.getCaption();
        }
        return "";
    }

    /*
     * sorts alphabetically by caption
     */
    @Override
    public int compareTo(CmParamdef o) {
        return caption.compareTo(o.caption);
    }

    public String getKey() {
        return cmParamdefPK.getKey();
    }

    public void setKey(String key) {
        cmParamdefPK.setKey(key);
    }

    public CmTypedef getCmTypedef() {
        return cmParamdefPK.getCmTypedef();
    }

    public void setCmTypedef(CmTypedef cmTypedef) {
        cmParamdefPK.setCmTypedef(cmTypedef);
        cmTypedef.add(this);
    }

    public void setCmParamkind(CmParamkind cmParamkind) {
        this.cmParamkind = cmParamkind;
        parameterKind = ParameterKind.valueOf(cmParamkind.getKey());
    }
}
