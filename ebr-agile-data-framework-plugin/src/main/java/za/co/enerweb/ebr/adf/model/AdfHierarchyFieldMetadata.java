package za.co.enerweb.ebr.adf.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(of = {"fieldName", "hierTypeDef", "childReference"})
public final class AdfHierarchyFieldMetadata {

    private String fieldName;
    private boolean childReference;
    private AdfHierarchyTypeDefMetadata hierTypeDef;
    private boolean eager = false;
    private String uniqueFieldPerParent = null;
    /**
     * if true this field is a list of values
     */
    private boolean list = false;

    public String getKey() {
        return hierTypeDef.getKey();
    }

    public boolean isParentReference() {
        return !childReference;
    }
}
