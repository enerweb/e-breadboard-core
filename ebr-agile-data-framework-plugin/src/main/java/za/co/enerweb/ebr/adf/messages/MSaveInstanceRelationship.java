package za.co.enerweb.ebr.adf.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import za.co.enerweb.ebr.adf.entities.CmReltypedef;
import za.co.enerweb.ebr.adf.entities.IbInstance;
import za.co.enerweb.ebr.adf.model.InstanceRelationship;
import za.co.enerweb.ebr.gui.vaadin.messaging.Message;

@Data
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
public class MSaveInstanceRelationship
extends Message {
//    @Delegate
    private InstanceRelationship instanceRelationship;
    public IbInstance getFromInstance() {
        return instanceRelationship.getFromInstance();
    }
    public CmReltypedef getRelationshipType() {
        return instanceRelationship.getRelationshipType();
    }
    public void setFromInstance(IbInstance fromInstance) {
        instanceRelationship.setFromInstance(fromInstance);
    }
    public void setRelationshipType(CmReltypedef relationshipType) {
        instanceRelationship.setRelationshipType(relationshipType);
    }
    public IbInstance getToInstance() {
        return instanceRelationship.getToInstance();
    }
    public void setToInstance(IbInstance toInstance) {
        instanceRelationship.setToInstance(toInstance);
    }
}
