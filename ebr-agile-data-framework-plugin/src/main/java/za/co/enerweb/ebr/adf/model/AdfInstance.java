package za.co.enerweb.ebr.adf.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.SneakyThrows;
import lombok.val;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.adf.AdfFactory;
import za.co.enerweb.ebr.adf.dbl.AdfDbLayerUtils;
import za.co.enerweb.ebr.adf.dbl.AdfRegistry;
import za.co.enerweb.toolbox.reflection.PropertyUtils;
import za.co.enerweb.toolbox.string.StringUtils;

/**
 * All instances should extend this.
 * Equals only unique per entity.
 */
@Slf4j
@Data
@EqualsAndHashCode(of = "id")
public abstract class AdfInstance implements Serializable, Cloneable
// ,Comparable<AdfInstance> allow subclasses to decide for themselves
// how they want to sort, if we implement it here, they can't override it..
{
    private static final Pattern prefixPattern = Pattern.compile(
        "^(T)([A-Z].*)");
    private boolean loadedEager = false;
    private Integer id; // FIXME: should be Long
    private String caption;
    private String typeDefKey = deriveKey();

    private HashMap<String, Object> dynamicParameters = null;

    protected String deriveKey() {
        return deriveKey(getClass());
    }

    protected static String deriveKey(Class<? extends AdfInstance> klass) {
        String ret = klass.getSimpleName();
        Matcher matcher = prefixPattern.matcher(ret);
        if (matcher.matches()) {
            ret = matcher.group(2);
        } else {
            throw new IllegalStateException("Class name does not " +
                "follow the entity name convention " +
                "(prefix it with T eg. TMyInstance) : " + ret);
        }
        return StringUtils.camelCaseToUnderscoreSeparated(ret).toUpperCase();
    }

    public AdfTypeDefMetadata getTypeDef(AdfFactory adfFactory) {
        return getTypeDef(adfFactory.getAdfRegistry());
    }

    public AdfTypeDefMetadata getTypeDef(AdfRegistry registry) {
        String typeDefKey = getTypeDefKey();
        if (typeDefKey == null) {
            return null;
        }
        return registry.getAdfTypeDef(typeDefKey);
    }

    public List<AdfParameterDefMetadata> getParameters(
        AdfFactory adfFactory) {
        val ret =
            new ArrayList<AdfParameterDefMetadata>(getTypeDef(adfFactory)
                .getParameters().values());
        Collections.sort(ret);
        return ret;
    }

    public AdfEntity getAdfEntity(AdfRegistry registry) {
        return registry.getAdfEntity(
            getTypeDef(registry).getEntityClass());
    }

    public boolean equals(AdfInstance other, AdfRegistry registry) {
        if (!this.equals(other)) {
            return false;
        }
        AdfEntity adfEntity = getAdfEntity(registry);
        AdfEntity otherAdfEntity = other.getAdfEntity(registry);
        return adfEntity.equals(otherAdfEntity);
    }

    public Class<?> getJpaEntityClass(AdfFactory adfFactory) {
        AdfRegistry adfRegistry = adfFactory.getAdfRegistry();
        AdfTypeDefMetadata adfTypeDef = getTypeDef(adfRegistry);
        return adfFactory.getJpaEntityClass(
            AdfDbLayerUtils.efsEntityToJpaInstanceEntity(
                adfRegistry.getAdfEntity(adfTypeDef.getEntityClass())
                    .getKey()));
    }

    // protected static String deriveRelationhsipKey(
    // Class<? extends AdfInstance> fromClass,
    // Class<? extends AdfInstance> toClass) {
    // return deriveKey(fromClass) + "_" + deriveKey(toClass);
    // }

    public static boolean hasInstance(
        List<? extends AdfInstance> adfInstances,
        Integer id) {
        for (AdfInstance adfInstance : adfInstances) {
            if (adfInstance.getId().equals(id)) {
                return true;
            }
        }
        return false;
    }

    public AdfParameterDefMetadata getParameter(AdfFactory adfFactory,
        String paramKey) {
        AdfParameterDefMetadata ret =
            getTypeDef(adfFactory).findParameterByParameterKey(paramKey);
        if (ret == null) {
            throw new IllegalArgumentException("Could not find parameter " +
                "by key " + typeDefKey + "." + paramKey + ".");
        }
        return ret;
    }

    public AdfParameterDefMetadata getParameterByFieldName(
        AdfFactory adfFactory,
        String fieldName) {
        AdfParameterDefMetadata ret =
            getTypeDef(adfFactory).findParameterByFieldName(fieldName);
        if (ret == null) {
            throw new IllegalArgumentException("Could not find parameter " +
                "by fieldname" + typeDefKey + "." + fieldName + ".");
        }
        return ret;
    }

    public String toString() {
        return caption;
    }

    public void set(AdfFactory adfFactory, String paramKey, Object value) {
        set(getParameter(adfFactory, paramKey),
            value);
    }

    public void set(AdfParameterDefMetadata paramDef, Object value) {
        String fieldName = paramDef.getFieldName();
        if (fieldName != null) {
            set(fieldName, value);
        } else {
            setDynamic(paramDef.getKey(), value);
        }
    }

    public void set(String fieldName, Object value) {
        try {
            PropertyUtils
                .setProperty(this, fieldName, value);
        } catch (Exception e) {
            log.warn("Could not set property "
                + this.getClass().getName() + "."
                + fieldName + " = " + value, e);
        }
    }

    public void setDynamic(String paramName, Object value) {
        getDynamicParameters().put(paramName, value);
    }

    public <RT> RT get(AdfFactory adfFactory, String paramKey) {
        return get(getParameter(adfFactory, paramKey));
    }

    public <RT> RT get(AdfParameterDefMetadata paramDef) {
        String fieldName = paramDef.getFieldName();
        if (fieldName == null) {
            return (RT) getDynamic(paramDef.getKey());
        } else {
            return get(fieldName);
        }
    }

    /**
     * @param fieldName
     * @return only returns non-dynamic values
     */
    public <RT> RT get(String fieldName) {
        return PropertyUtils
            .getProperty(this, fieldName);
    }

    public Object getDynamic(String paramName) {
        return getDynamicParameters().get(paramName);
    }

    public Map<String, Object> getDynamicParameters() {
        if (dynamicParameters == null) {
            dynamicParameters = new HashMap<String, Object>();
        }
        return dynamicParameters;
    }

    @SneakyThrows
    public AdfInstance clone() {
        val dolly = (AdfInstance) super.clone();
        if (dynamicParameters != null) {
            dolly.dynamicParameters = (HashMap<String, Object>)
                dynamicParameters.clone();
        }
        return dolly;
    }
}
