package za.co.enerweb.ebr.adf.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import org.apache.commons.lang.StringUtils;

@Data
@EqualsAndHashCode(of="key")
@ToString(of="key")
@NoArgsConstructor
@Entity
@Table(name = "CM_UNITOFMEASURE")
@NamedQueries({
    @NamedQuery(name = "CmUnitofmeasure.findAll", query = "SELECT c FROM CmUnitofmeasure c"),
    @NamedQuery(name = "CmUnitofmeasure.findByKey", query = "SELECT c FROM CmUnitofmeasure c WHERE c.key = :key"),
    @NamedQuery(name = "CmUnitofmeasure.findByCaption", query = "SELECT c FROM CmUnitofmeasure c WHERE c.caption = :caption")})
public class CmUnitofmeasure implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "KEY", nullable = false, length = 25)
    private String key;
    @Basic(optional = false)
    @Column(name = "CAPTION", nullable = false, length = 100)
    private String caption;

    @Column(name = "DESCRIPTION", length = 200)
    private String description;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "cmUnitofmeasure")
    private Collection<CmRelparamdef> cmRelparamdefCollection;
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "cmUnitofmeasure")
    private Collection<CmParamdef> cmParamdefCollection;

    public CmUnitofmeasure(String key, String caption) {
        this.key = key;
        this.caption = caption;
    }

    public String getCaptionAndDescription() {
        String ret = caption;
        if (description != null) {
            ret += " (" + description + ")";
        }
        return ret;
    }

    public String getCaptionAndDescription(int descriptionWidth) {
        String ret = caption;
        if (description != null && !description.isEmpty()) {
            ret += " (" + StringUtils.abbreviate(description, descriptionWidth)
                + ")";
        }
        return ret;
    }

}
