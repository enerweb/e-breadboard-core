package za.co.enerweb.ebr.datatype.gis;


/**
 * @deprecated use {@link za.co.enerweb.ebr.adf.data_types.GisLocation}
 */
@Deprecated
public abstract class GisLocation {
    private GisLocation() {
    };
}
