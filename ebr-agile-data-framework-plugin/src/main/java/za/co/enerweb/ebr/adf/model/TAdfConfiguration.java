package za.co.enerweb.ebr.adf.model;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@AdfInstanceTypeDef(entity = EGeneral.class,
    description = "Adf Configuration")
@FieldDefaults(level = AccessLevel.PUBLIC)
public class TAdfConfiguration extends AdfInstance {
    public static final String KEY = "ADF_CONFIGURATION";
    // deriveKey(TAdfConfiguration.class);
    // public static final String CAPTION = deriveCaption(KEY);

    // @Override
    // public final Class<? extends AdfInstanceEntity> getEntityClass() {
    // return EGeneral.class;
    // }
    //
    // @Override
    // public String getDescription() {
    // return "Adf Configuration";
    // }
    //
    // @AdfParmdef(description = "Adf Db Version", type =
    // DtInteger.class)
    // static String ADF_VERSION = "ADF_VERSION";
}
