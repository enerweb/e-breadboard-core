package za.co.enerweb.ebr.adf.model;

import lombok.Data;

@Data
public class ProbabilityInfo {
    // array containing 0 to 100 values
    // minValues[x] has a probability of x/100
    double[] minValues;
    double[] maxValues;
}
