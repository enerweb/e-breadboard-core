package za.co.enerweb.ebr.adf.entities;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true, of={})
@ToString(callSuper=true, of={"cmParamdef", "key", "val"})
@MappedSuperclass
@Table(uniqueConstraints = {
    @UniqueConstraint(columnNames = {"TYPEDEF_ID", // "INSTANCE_ID",
        "KEY", "APPLDATETIME", "INDX"})})
public class IbInstanceParameterNorm extends IbInstanceParameter {
    private static final long serialVersionUID = 1L;

    @Column(name = "KEY")
    private  String key;
    @Basic(optional = false)
    @Column(name = "VAL", nullable = false, length = 1000)
    private String val;

    @JoinColumns({
        @JoinColumn(name = "TYPEDEF_ID", referencedColumnName = "TYPEDEF_ID",
            nullable = false, insertable = false, updatable = false),
        @JoinColumn(name = "KEY", referencedColumnName = "KEY",
            nullable = false, insertable = false, updatable = false)
    })
    @ManyToOne(optional = false)
    private CmParamdef cmParamdef;

    public IbInstanceParameterNorm(Integer id) {
        super(id);
    }

    public IbInstanceParameterNorm(Integer id, Date appldatetime, String val) {
        super(id, appldatetime);
        this.val = val;
    }

    public IbInstanceParameterNorm(CmParamdef cmParamdef, Date appldatetime,
        String val) {
        super(cmParamdef.getCmTypedef(), appldatetime);
        setCmParamdef(cmParamdef);
        this.val = val;
    }

    @Transient
    @Column
    @Access(AccessType.PROPERTY)
    public Double getDoubleValue() {
        return new Double(val);
    }

    public void setDoubleValue(Double doubleValue) {
        // maybe this value is in scientific notation!
        val = doubleValue.toString();
    }

    public void setCmParamdef(CmParamdef cmParamdef) {
        this.cmParamdef = cmParamdef;
        this.key = cmParamdef.getKey();
    }
}
