package za.co.enerweb.ebr.adf.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import za.co.enerweb.ebr.adf.entities.CmReltypedef;
import za.co.enerweb.ebr.adf.entities.IbInstance;
import za.co.enerweb.ebr.adf.model.InstanceRelationshipTypeDef;
import za.co.enerweb.ebr.gui.vaadin.messaging.Message;

@Data
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
public class MSelectInstanceRelationshipTypeDef
extends Message {
    //@Delegate
    private InstanceRelationshipTypeDef instanceRelationshipTypeDef;

    public IbInstance getFromInstance() {
        return instanceRelationshipTypeDef.getFromInstance();
    }
    public CmReltypedef getRelationshipType() {
        return instanceRelationshipTypeDef.getRelationshipType();
    }
    public void setFromInstance(IbInstance fromInstance) {
        instanceRelationshipTypeDef.setFromInstance(fromInstance);
    }
    public void setRelationshipType(CmReltypedef relationshipType) {
        instanceRelationshipTypeDef.setRelationshipType(relationshipType);
    }
}
