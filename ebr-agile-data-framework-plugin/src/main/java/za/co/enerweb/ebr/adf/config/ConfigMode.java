package za.co.enerweb.ebr.adf.config;

import java.util.EnumSet;

import za.co.enerweb.ebr.adf.model.ParameterKind;

public enum ConfigMode {
    CONFIG(EnumSet.of(ParameterKind.INPUT), "Edit"),
    DETAILS(EnumSet.allOf(ParameterKind.class), "View"),
    SETUP(EnumSet.allOf(ParameterKind.class), "SETUP");

    private final EnumSet<ParameterKind> parameterKinds;
    private final String actionCaption;

    private ConfigMode(EnumSet<ParameterKind> parameterKinds,
        String actionCaption) {
        this.parameterKinds = parameterKinds;
        this.actionCaption = actionCaption;
    }

    public EnumSet<ParameterKind> getParameterKinds() {
        return parameterKinds;
    }

    public String getActionCaption() {
        return actionCaption;
    }
}
