package za.co.enerweb.ebr.adf.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(of="key")
@ToString(of="key")
@Entity
@Table(name = "CM_ENUM")
@NamedQueries({
    @NamedQuery(name = "CmEnum.findAll", query = "SELECT c FROM CmEnum c"),
    @NamedQuery(name = "CmEnum.findByKey", query = "SELECT c FROM CmEnum c WHERE c.key = :key"),
    @NamedQuery(name = "CmEnum.findByCaption", query = "SELECT c FROM CmEnum c WHERE c.caption = :caption")})
public class CmEnum implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "KEY", nullable = false, length = 25)
    private String key;
    @Basic(optional = false)
    @Column(name = "CAPTION", nullable = false, length = 100)
    private String caption;
    @JoinColumn(name = "DATATYPE_KEY", referencedColumnName = "KEY", nullable = false)
    @ManyToOne(optional = false)
    private CmDatatype cmDatatype;

    public CmEnum() {
    }

    public CmEnum(String key) {
        this.key = key;
    }

    public CmEnum(String key, String caption) {
        this.key = key;
        this.caption = caption;
    }

}
