package za.co.enerweb.ebr.adf.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import za.co.enerweb.ebr.adf.entities.IbInstance;
import za.co.enerweb.toolbox.vaadin.messaging.Message;

@Data
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
public class MSaveAdfInstance extends Message {
    private IbInstance instance;
}
