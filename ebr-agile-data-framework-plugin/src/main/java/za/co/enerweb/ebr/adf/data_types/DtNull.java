package za.co.enerweb.ebr.adf.data_types;

import za.co.enerweb.ebr.adf.model.AdfScalarDataType;

public class DtNull extends AdfScalarDataType<Object> {
    public static final String KEY = deriveKey(DtNull.class);

    public String getDescription() {
        return "Null/Void type";
    }

    public Class<Object> getJavaType() {
        return Object.class;
    }

    public String toPersistedValue(Object value) {
        return null;
    }

    public Object fromPersistedValue(String persistedValue) {
        return null;
    }

}
