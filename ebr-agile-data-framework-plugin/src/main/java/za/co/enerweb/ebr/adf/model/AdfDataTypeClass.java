package za.co.enerweb.ebr.adf.model;

/**
 * This needs to be manually kept in sync with the db.
 * I suppose we can check compliance during unit tests.
 */
public enum AdfDataTypeClass {
    SCALAR("Scalar"),
    ARRAY("Array has an item data type Id, and elements' INDX are the "
        + "array indexes"),
    ENUM("Enumeration constant"),
    TIMESERIES("Timeseries has an item data type Id, and elements' INDX are "
        + "the interval index relative to the applicable start date time."),
    SET("Set has an Item data type Id"),
    INSTANCE("An instance of a typedef, it simply stores the id of the "
        + "referenced instance and the typedef should be defined in the "
        + "paramdef definition.");

    private String description;

    private AdfDataTypeClass(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }

}
