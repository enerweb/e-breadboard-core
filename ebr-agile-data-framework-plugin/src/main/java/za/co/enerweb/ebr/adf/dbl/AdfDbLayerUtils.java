package za.co.enerweb.ebr.adf.dbl;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;

import za.co.enerweb.toolbox.reflection.PropertyUtils;

/**
 * In general we should hide the things below by adding useful methods
 * to the objects that need to use this.
 */
public class AdfDbLayerUtils {

    /**
     * ILoad -> LOAD
     * Done with a simple text conversion,
     * but can be done with an Enum in future if needed.
     * @param efsEntity
     * @return
     */
    public static String jpaEntityToEfsEntity(final String jpaEntity) {
        if (!jpaEntity.startsWith("I")) {
            throw new IllegalArgumentException("jpa entities are expected to "
                + "start with I");
        }
        return jpaEntity.substring(1).toUpperCase();
    }

    /**
     * LOAD -> ILoad
     * POWERSTATION -> IPowerstation
     * COAL_POWERSTATION -> ICoalPowerstation
     * Done with a simple text conversion,
     * but can be done with an Enum in future if needed.
     * @param efsEntity
     * @return
     */
    public static String efsEntityToJpaInstanceEntity(final String efsEntity) {
        return "I" + toCamelCase(efsEntity.toLowerCase());
    }

    /**
     * LOAD_WEATHER -> RLoadWeather
     * @param efsEntity
     * @return
     */
    public static String efsEntityToJpaRelationshipEntity(
        final String efsEntity) {
        return "R" + toCamelCase(efsEntity.toLowerCase());
    }

    /**
     * LOAD_WEATHER -> RLoadWeather
     * @param efsEntity
     * @return
     */
    public static String efsEntityToJpaHierarchyEntity(
        final String efsEntity) {
        return "H" + toCamelCase(efsEntity.toLowerCase());
    }

    /**
     * LOAD -> ILoadPar
     * @param entity
     * @return
     */
    public static String efsEntityToJpaParmameterEntity(
        final String efsEntity) {
        return efsEntityToJpaInstanceEntity(efsEntity) + "Par";
    }

    /**
     * LOAD -> ILoadOut
     * @param entity
     * @return
     */
    public static String efsEntityToJpaOutputEntity(final String efsEntity) {
        return efsEntityToJpaInstanceEntity(efsEntity) + "Out";
    }

    /**
     * LOAD -> I_LOAD_PAR
     * @param entity
     * @return
     */
    public static String efsEntityToParmameterTableName(
        final String efsEntity) {
        return "I_" + efsEntity + "_PAR";
    }

    /**
     * LOAD -> I_LOAD_OUT
     * @param entity
     * @return
     */
    public static String efsEntityToOutputTableName(final String efsEntity) {
        return "I_" + efsEntity + "_OUT";
    }

    /**
     * LOAD -> iLoad
     * @param entity
     * @return
     */
    public static String efsEntityToJpaParmameterInstanceId(
        final String efsEntity) {
        return "i" + toCamelCase(efsEntity.toLowerCase());
    }

    /**
     * LOAD -> LOAD_ID
     * @param entity
     * @return
     */
    public static String efsEntityToParmameterInstanceId(
        final String efsEntity) {
        return efsEntity + "_ID";
    }

    /**
     * LOAD -> I_LOAD
     * @param entity
     * @return
     */
    public static String efsEntityToInstanceTableName(final String entity) {
        return "I_" + entity;
    }

    /**
     * I_WEATHER_WEATHERSA_OUT -> IWeatherWeathersaOut
     * @param paramTable
     * @return
     */
    public static String paramTableToJpaEntity(final String paramTable) {
        return toCamelCase(paramTable);
    }

    /**
     * I_WEATHER_WEATHERSA_OUT -> IWeatherWeathersaOut
     * @param underscoreSeparated
     * @return
     */
    public static String toCamelCase(final String underscoreSeparated) {
        return StringUtils.remove(
            WordUtils.capitalizeFully(underscoreSeparated, new char[] {'_'}),
            '_');
    }

    /**
     * COST_OF_SUPPLY -> costOfSupply
     * @param key
     * @return
     */
    public static String paramKeyToJpaProperty(final String paramKey) {
        return StringUtils.uncapitalize(toCamelCase(paramKey));
    }

    /**
     * Generic way to set a property on a object
     */
    public static void setProperty(final Object object, final String fieldName,
            final Object value) {
        PropertyUtils.setProperty(object, fieldName, value);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getProperty(final Object object,
        final String fieldName) {
        return (T) PropertyUtils.getProperty(object, fieldName);
    }
}
