package za.co.enerweb.ebr.adf.data_types;

import za.co.enerweb.ebr.adf.model.AdfDataType;
import za.co.enerweb.ebr.adf.model.AdfDataTypeClass;

/*
 * TODO: maybe we can make an anotation that can go onto DtInteger
 * that can contain all this metadata..
 */
public class DtIntegerArray extends DtInteger {

    // protected String deriveKey() {
    // return "INTEGER_ARRAY";
    // }

    public String getDescription() {
        return "Integer Array";
    }

    public AdfDataTypeClass getAdfDataTypeClass() {
        return AdfDataTypeClass.ARRAY;
    }

    public Class<? extends AdfDataType<?>> getItemDataType() {
        return DtInteger.class;
    }
}
