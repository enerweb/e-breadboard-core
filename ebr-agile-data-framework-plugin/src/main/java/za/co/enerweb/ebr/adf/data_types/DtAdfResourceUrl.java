package za.co.enerweb.ebr.adf.data_types;

import za.co.enerweb.ebr.adf.model.AdfScalarDataType;

public class DtAdfResourceUrl extends AdfScalarDataType<AdfResourceUrl> {

    public String getDescription() {
        return "Stores a url or a file in the resources dir";
    }

    public Class<AdfResourceUrl> getJavaType() {
        return AdfResourceUrl.class;
    }

    public String toPersistedValue(AdfResourceUrl value) {
        return value.getUrl();
    }

    public AdfResourceUrl fromPersistedValue(String persistedValue) {
        return new AdfResourceUrl(getInstance(), getCmParamdef(),
            persistedValue);
    }

}
