package za.co.enerweb.ebr.adf.model.timeseries;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

public interface ITimeSeriesItem<V> extends Serializable,
Map.Entry<Date,V> {

    Date getDateTime();

    V getValue();
}
