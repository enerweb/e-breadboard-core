package za.co.enerweb.ebr.adf.data_types;

import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.ISODateTimeFormat;

import za.co.enerweb.ebr.adf.model.AdfScalarDataType;

public class DtDateTime extends AdfScalarDataType<Date> {

    public static final String KEY = "DATETIME";

    private static final DateTimeFormatter formatter;
    static {
        formatter = new DateTimeFormatterBuilder()
            .append(ISODateTimeFormat.date())
            .appendLiteral(' ')
            .append(ISODateTimeFormat.hourMinuteSecond())
            .toFormatter();
    }

    protected String deriveKey() {
        return KEY;
    }

    public String getDescription() {
        return "Date/Time (YYYY-MM-DD HH24:MI:SS)";
    }

    public Class<Date> getJavaType() {
        return Date.class;
    }

    public String toPersistedValue(Date value) {
        return formatter.print(new DateTime(value));
    }

    public Date fromPersistedValue(String persistedValue) {
        return formatter.parseDateTime(persistedValue).toDate();
    }

}
