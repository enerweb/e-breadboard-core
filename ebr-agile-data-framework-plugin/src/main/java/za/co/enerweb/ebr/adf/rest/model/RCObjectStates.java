/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package za.co.enerweb.ebr.adf.rest.model;

import java.util.Collection;

import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import lombok.NoArgsConstructor;
import za.co.enerweb.toolbox.rest.Items;
import za.co.enerweb.toolbox.rest.Meta;

@XmlRootElement(name = RCObjectStates.URI_FRAGMENT)
@XmlSeeAlso({RCObjectState.class, Items.class, RCPropertyValues.class,
    String.class})
@NoArgsConstructor
public class RCObjectStates extends Items<RCObjectState> {

    public final static String URI_FRAGMENT = "ObjectStates";

    public RCObjectStates(Meta meta, Collection<RCObjectState> items) {
        super(meta, items);
    }

    public RCObjectStates(UriInfo uriInfo, RCObject object,
        Collection<RCObjectState> items) {
        this(
            new AdfMetaBuilder(uriInfo).slash(RCObject.URI_FRAGMENT)
                .slash(object.getId()).slash(URI_FRAGMENT).build(),
            items);
    }
}
