package za.co.enerweb.ebr.adf.dbl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.val;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.adf.AdfFactory;
import za.co.enerweb.ebr.adf.entities.IbInstance;
import za.co.enerweb.ebr.adf.model.AdfInstance;
import za.co.enerweb.ebr.adf.model.AdfInstanceEntity;
import za.co.enerweb.ebr.adf.model.AdfTypeDefMetadata;
import za.co.enerweb.toolbox.vaadin.lazytable.ABeanProducer;
import za.co.enerweb.toolbox.vaadin.lazytable.SortSpec;
import za.co.enerweb.toolbox.vaadin.lazytable.filter.AFilter;

/**
 * The row objects/ids are of type AdfInstance
 */
@Slf4j
@Getter(value = AccessLevel.PROTECTED)
public class AdfInstanceProducer<T extends AdfInstance>
    extends ABeanProducer<T> {

    public static final String TYPE_DEF = "typeDef";

    private final AdfFactory adfFactory;
    private Class<Object> jpaEntityClass;
    private Class<? extends AdfInstanceEntity> adfEntityClass;

    // private AdfTypeDefMetadata adfTypeDef;

    public AdfInstanceProducer(Class<T> dataClass,
        AdfFactory adfFactory) {
        this(dataClass, null, adfFactory);
    }

    /**
     * @param propertyInfosTemplate may contain Strings which will imply
     *        propertyIds or PropertyInfo objects (they may be partial too)
     *        If specified, only properties in this list will be used in the
     *        order
     *        given.
     */
    public AdfInstanceProducer(Class<T> dataClass,
        List<?> propertyInfosTemplate,
        AdfFactory adfFactory) {
        super(dataClass, propertyInfosTemplate);
        this.adfFactory = adfFactory;

        AdfRegistry adfRegistry = adfFactory.getAdfRegistry();
        AdfTypeDefMetadata adfTypeDef = adfRegistry.getAdfTypeDef(
            getDataObjectClass());

        setJpaEntityClass(adfFactory, adfRegistry, adfTypeDef.getEntityClass());
    }

    public AdfInstanceProducer(Class<T> dataClass,
        AdfFactory adfFactory,
        Class<? extends AdfInstanceEntity> adfEntityClass) {
        this(dataClass, null, adfFactory, adfEntityClass);
    }

    /**
     * @param propertyInfosTemplate may contain Strings which will imply
     *        propertyIds or PropertyInfo objects (they may be partial too)
     *        If specified, only properties in this list will be used in the
     *        order
     *        given.
     */
    public AdfInstanceProducer(Class<T> dataClass,
        List<?> propertyInfosTemplate,
        AdfFactory adfFactory,
        Class<? extends AdfInstanceEntity> adfEntityClass) {
        super(dataClass, propertyInfosTemplate);
        this.adfFactory = adfFactory;
        setJpaEntityClass(adfFactory, adfFactory.getAdfRegistry(),
            adfEntityClass);
    }

    private void setJpaEntityClass(AdfFactory adfFactory,
        AdfRegistry adfRegistry,
        Class<? extends AdfInstanceEntity> adfEntityClass) {
        this.adfEntityClass = adfEntityClass;
        jpaEntityClass = adfFactory.getJpaEntityClass(
            AdfDbLayerUtils.efsEntityToJpaInstanceEntity(
                adfRegistry.getAdfEntity(adfEntityClass)
                    .getKey()));
    }

    @Override
    public int count(AFilter aFilter) {
        AdfDbLayer adfDbLayer = adfFactory.getAdfDbLayer();
        return adfDbLayer.count(jpaEntityClass, aFilter);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<T> find(int startIndex, int count, SortSpec sortSpec,
        AFilter aFilter) {
        // FIXME: support sorting
        // for now always sort by caption. later it should just be the default
        sortSpec = new SortSpec().addProperty("caption", true);
        AdfDbLayer adfDbLayer = adfFactory.getAdfDbLayer();
        List<Object> instances =
            adfDbLayer.find(
            jpaEntityClass, startIndex, count, sortSpec, aFilter);

        val ret = new ArrayList<T>(instances.size());
        for (Object object : instances) {
            if (object instanceof IbInstance) {
                IbInstance inst = (IbInstance) object;
                try {
                    ret.add(
                        (T) adfDbLayer.findAdfInstance(
                            adfEntityClass,
                            inst.getId(), false)
                        );
                } catch (Exception e) {
                    log.warn("Could not load instance: " + object, e);
                }
            }
        }
        return ret;
    }

    @Override
    public void saveItems(final List<T> addedItems,
        final List<T> modifiedItems,
        final List<T> removedItems) {
        val dbl = adfFactory.getAdfDbLayer();
        dbl.saveAdfInstances(addedItems);
        dbl.saveAdfInstances(modifiedItems);
        dbl.removeAdfInstances(removedItems);
    }

    public Object getPropertyValue(T dataObject,
        Serializable propertyId) {
        if (propertyId.equals(TYPE_DEF)) {
            AdfTypeDefMetadata typeDef = dataObject.getTypeDef(adfFactory);
            if (typeDef == null) {
                return "";
            }
            return typeDef.getCaption();
        }
        return super.getPropertyValue(dataObject, propertyId);
    }
}
