package za.co.enerweb.ebr.adf.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import za.co.enerweb.ebr.adf.model.AdfDataTypeClass;

@Data
@EqualsAndHashCode(of="key")
@ToString(of="key")
@Entity
@NoArgsConstructor
@Table(name = "CM_DATATYPECLASS")
@NamedQueries({
    @NamedQuery(name = "CmDatatypeclass.findAll", query = "SELECT c FROM CmDatatypeclass c"),
    @NamedQuery(name = "CmDatatypeclass.findByKey", query = "SELECT c FROM CmDatatypeclass c WHERE c.key = :key"),
    @NamedQuery(name = "CmDatatypeclass.findByDescription", query = "SELECT c FROM CmDatatypeclass c WHERE c.description = :description")})
public class CmDatatypeclass implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "KEY", nullable = false, length = 25)
    private String key;
    @Column(name = "DESCRIPTION", length = 400)
    private String description;

    @Column(name = "KEY", insertable=false, updatable=false)
    @Enumerated(EnumType.STRING)
    private AdfDataTypeClass adfDataTypeClass;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "cmDatatypeclass")
    private Collection<CmDatatype> cmDatatypeCollection;

    public CmDatatypeclass(AdfDataTypeClass adfDataTypeClass) {
        this.adfDataTypeClass = adfDataTypeClass;
        this.key = adfDataTypeClass.name();
        this.description = adfDataTypeClass.toString();
    }

}
