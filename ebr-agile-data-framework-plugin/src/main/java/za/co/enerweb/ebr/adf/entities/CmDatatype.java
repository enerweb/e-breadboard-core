package za.co.enerweb.ebr.adf.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import za.co.enerweb.ebr.adf.model.AdfDataTypeClass;

@NoArgsConstructor
@Data
@EqualsAndHashCode(of="key")
@ToString(of="key")
@Entity
@Table(name = "CM_DATATYPE")
public class CmDatatype implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "KEY", nullable = false, length = 25)
    private String key;
    @Column(name = "DESCRIPTION", length = 400)
    private String description;

    /**
     * this does not seem to get used. the length should be 25?!
     */
    // @Column(name = "ITEMDATATYPE_KEY", length = 10)
    // private String itemdatatypeKey;

    @JoinColumn(name = "ITEMDATATYPE_KEY", referencedColumnName = "KEY",
        nullable = true)
    @ManyToOne(optional = true,
        fetch = FetchType.EAGER
    // "javax.persistence.EntityNotFoundException: Unable to find za.co.enerweb.ebr.adf.entities.CmDatatype with id "
    // http://stackoverflow.com/questions/13539050/entitynotfoundexception-in-hibernate-many-to-one-mapping-however-data-exist

    // , fetch = FetchType.EAGER
    // Causes: org.hibernate.loader.MultipleBagFetchException: cannot
    // simultaneously fetch multiple bags
    )
    private CmDatatype itemdatatype;

//    @OneToMany(mappedBy = "cmIndexDatatype")
//    private Collection<CmRelparamdef> cmIndexRelparamdefCollection;
//    @OneToMany(mappedBy = "cmDatatype")
//    private Collection<CmRelparamdef> cmRelparamdefCollection;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "cmDatatype")
    // , fetch = FetchType.EAGER)
    private Set<CmEnum> cmEnumCollection;

//    @OneToMany(mappedBy = "cmIndexDatatype")
//    private Collection<CmParamdef> cmIndexParamdefCollection;
//        @OneToMany(mappedBy = "cmIndexDatatype")
//    private Collection<CmParamdef> cmParamdefCollection;

    @JoinColumn(name = "DATATYPECLASS_KEY", referencedColumnName = "KEY",
        nullable = false)
    @ManyToOne(optional = false,
        //        fetch = FetchType.LAZY // else I get
    // "javax.persistence.EntityNotFoundException: Unable to find za.co.enerweb.ebr.adf.entities.CmDatatype with id "
    // http://stackoverflow.com/questions/13539050/entitynotfoundexception-in-hibernate-many-to-one-mapping-however-data-exist

      fetch = FetchType.EAGER
    // Causes: org.hibernate.loader.MultipleBagFetchException: cannot
    // simultaneously fetch multiple bags
    )
    private CmDatatypeclass cmDatatypeclass;

    public AdfDataTypeClass getAdfDataTypeClass() {
        return AdfDataTypeClass.valueOf(cmDatatypeclass.getKey());
    }

    public CmDatatype(String key, String description,
        CmDatatypeclass cmDatatypeclass) {
        this.key = key;
        this.description = description;
        this.cmDatatypeclass = cmDatatypeclass;
    }
}
