package za.co.enerweb.ebr.adf.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import za.co.enerweb.ebr.adf.entities.IbInstance;
import za.co.enerweb.toolbox.vaadin.messaging.Message;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class MSelectAdfEntityInstance extends Message {

    private IbInstance instance;

}
