package za.co.enerweb.ebr.adf.rest;

import javax.ejb.EJB;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.adf.AdfFactoryRegistry;
import za.co.enerweb.ebr.adf.dbl.AdfRestDbLayer;

@Produces({MediaType.APPLICATION_JSON
    , MediaType.APPLICATION_XML
})
@Slf4j
public class ARConcept {

    @EJB
    private AdfFactoryRegistry factoryRegistry;

    protected final AdfRestDbLayer getRDbl() {
        return factoryRegistry.getAdfRestDbLayer(getClass());
    }

}
