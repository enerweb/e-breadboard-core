package za.co.enerweb.ebr.adf.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(of="cmRelparamdefPK")
@ToString(of="cmRelparamdefPK")
@Entity
@Table(name = "CM_RELPARAMDEF", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"KEY"})})
@NamedQueries({
    @NamedQuery(name = "CmRelparamdef.findAll", query = "SELECT c FROM CmRelparamdef c"),
    @NamedQuery(name = "CmRelparamdef.findByReltypedefId", query = "SELECT c FROM CmRelparamdef c WHERE c.cmRelparamdefPK.reltypedefId = :reltypedefId"),
    @NamedQuery(name = "CmRelparamdef.findByKey", query = "SELECT c FROM CmRelparamdef c WHERE c.cmRelparamdefPK.key = :key"),
    @NamedQuery(name = "CmRelparamdef.findByCaption", query = "SELECT c FROM CmRelparamdef c WHERE c.caption = :caption"),
    @NamedQuery(name = "CmRelparamdef.findByDescription", query = "SELECT c FROM CmRelparamdef c WHERE c.description = :description")})
public class CmRelparamdef implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CmRelparamdefPK cmRelparamdefPK;
    @Basic(optional = false)
    @Column(name = "CAPTION", nullable = false, length = 100)
    private String caption;
    @Column(name = "DESCRIPTION", length = 400)
    private String description;
    @JoinColumn(name = "UNIT_OF_MEASURE_KEY", referencedColumnName = "KEY",
        nullable = true)
    @ManyToOne(optional = true)
    private CmUnitofmeasure cmUnitofmeasure;
    @JoinColumn(name = "RELTYPEDEF_ID", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CmReltypedef cmReltypedef;
    @JoinColumn(name = "PARAMKIND_KEY", referencedColumnName = "KEY", nullable = false)
    @ManyToOne(optional = false)
    private CmParamkind cmParamkind;

    @JoinColumn(name = "INDX_DATATYPE_KEY", referencedColumnName = "KEY")
    @ManyToOne
    private CmDatatype cmIndexDatatype;

    @JoinColumn(name = "DATATYPE_KEY", referencedColumnName = "KEY")
    @ManyToOne
    private CmDatatype cmDatatype;

    public CmRelparamdef() {
    }

    public CmRelparamdef(CmRelparamdefPK cmRelparamdefPK) {
        this.cmRelparamdefPK = cmRelparamdefPK;
    }

    public CmRelparamdef(CmRelparamdefPK cmRelparamdefPK, String caption) {
        this.cmRelparamdefPK = cmRelparamdefPK;
        this.caption = caption;
    }

    public CmRelparamdef(int reltypedefId, String key) {
        this.cmRelparamdefPK = new CmRelparamdefPK(reltypedefId, key);
    }

    public CmRelparamdefPK getCmRelparamdefPK() {
        return cmRelparamdefPK;
    }

}
