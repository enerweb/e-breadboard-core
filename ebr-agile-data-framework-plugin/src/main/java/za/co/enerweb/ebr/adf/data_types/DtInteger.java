package za.co.enerweb.ebr.adf.data_types;

import za.co.enerweb.ebr.adf.model.AdfScalarDataType;

public class DtInteger extends AdfScalarDataType<Integer> {

    public String getDescription() {
        return "Positive or negative Integer value.";
    }

    public Class<Integer> getJavaType() {
        return Integer.class;
    }

    public String toPersistedValue(Integer value) {
        return ((Integer) value).toString();
    }

    public Integer fromPersistedValue(String persistedValue) {
        return new Integer(persistedValue);
    }

}
