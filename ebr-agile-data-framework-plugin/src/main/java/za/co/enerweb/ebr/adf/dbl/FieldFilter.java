package za.co.enerweb.ebr.adf.dbl;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor
@AllArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Accessors(fluent = true, chain = true)
public class FieldFilter {
    String fieldName;
    Object value;
    boolean like; // only useful for text values

    public FieldFilter(
        String fieldName, Object value) {
        super();
        this.fieldName = fieldName;
        this.value = value;
    }
}
