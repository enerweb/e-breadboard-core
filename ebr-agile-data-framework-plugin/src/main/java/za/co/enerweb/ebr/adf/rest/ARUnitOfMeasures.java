package za.co.enerweb.ebr.adf.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.adf.rest.model.RCUnitOfMeasure;
import za.co.enerweb.ebr.adf.rest.model.RCUnitOfMeasures;
import za.co.enerweb.toolbox.rest.ExpandMembers;
import za.co.enerweb.toolbox.rest.ResponsTypeUtil;

// specified in Application:
// @Produces({MediaType.APPLICATION_JSON
// //, MediaType.APPLICATION_XML
// })

@Path("/" + RCUnitOfMeasures.URI_FRAGMENT + ResponsTypeUtil.PATH)
@Slf4j
public abstract class ARUnitOfMeasures extends ARConcept {

    @GET
    public Response getAll(@Context UriInfo uriInfo,
        @PathParam(ResponsTypeUtil.PARAM) String responseType,
        @QueryParam(ExpandMembers.PARAM) String expand) {
        final RCUnitOfMeasures ret = getRDbl().getAllRCUnitOfMeasures(uriInfo,
            new ExpandMembers(RCUnitOfMeasure.URI_FRAGMENT, expand));
        return ResponsTypeUtil.parse(
            Response.ok(ret), responseType).build();
    }

}
