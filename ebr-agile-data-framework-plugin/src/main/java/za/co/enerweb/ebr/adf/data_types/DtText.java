package za.co.enerweb.ebr.adf.data_types;

import za.co.enerweb.ebr.adf.model.AdfScalarDataType;

public class DtText extends AdfScalarDataType<String> {
    public static final String KEY = deriveKey(DtText.class);

    public String getDescription() {
        return "Text";
    }

    public Class<String> getJavaType() {
        return String.class;
    }

    public String toPersistedValue(String value) {
        return (String) value;
    }

    public String fromPersistedValue(String persistedValue) {
        return (String) persistedValue;
    }

}
