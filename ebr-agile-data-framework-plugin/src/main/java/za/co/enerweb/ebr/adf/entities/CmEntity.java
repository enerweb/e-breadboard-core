package za.co.enerweb.ebr.adf.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import za.co.enerweb.ebr.adf.dbl.AdfDbLayerUtils;
import za.co.enerweb.ebr.adf.model.EntityType;

@Data
@NoArgsConstructor
@EqualsAndHashCode(of={"entity", "cmEntitytype"})
@ToString(of={"entity", "caption", "cmEntitytype"})
@Entity
@Table(name = "CM_ENTITY")
public class CmEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ENTITY", nullable = false, length = 25)
    private String entity;
    @Basic(optional = false)
    @Column(name = "CAPTION", nullable = false, length = 100)
    private String caption;
    @JoinColumn(name = "ENTITYTYPE_KEY", referencedColumnName = "KEY",
        nullable = false)
    @ManyToOne(optional = false)
    private CmEntitytype cmEntitytype;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "cmEntity")
    private Collection<CmTypedef> cmTypedefs = new ArrayList<CmTypedef>(0);

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "cmEntity")
    private Collection<CmReltypedef> cmReltypedefs;

    public CmEntity(String entity) {
        this.entity = entity;
    }

    public CmEntity(String entity, String caption) {
        this.entity = entity;
        this.caption = caption;
    }

    public CmEntity(String entity, String caption, CmEntitytype cmEntitytype) {
        super();
        this.entity = entity;
        this.caption = caption;
        this.cmEntitytype = cmEntitytype;
    }

    public EntityType getEntityType() {
        return getCmEntitytype().getEnityType();
    }

    public String getJpaEntity() {
        switch (getEntityType()) {
        case INSTANCE:
            return AdfDbLayerUtils.efsEntityToJpaInstanceEntity(entity);
        case RELATIONSHIP:
            return AdfDbLayerUtils.efsEntityToJpaRelationshipEntity(entity);
        case HIERARCHY:
            return AdfDbLayerUtils.efsEntityToJpaHierarchyEntity(entity);
        default:
            throw new IllegalStateException("Unrecognised entity type: "
                + getEntityType());
        }
    }

    public String getKey() {
        return entity;
    }
}
