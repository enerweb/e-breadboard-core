/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package za.co.enerweb.ebr.adf.rest.model;

import java.util.List;

import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import lombok.NoArgsConstructor;
import za.co.enerweb.toolbox.rest.Items;
import za.co.enerweb.toolbox.rest.Meta;

@XmlRootElement(name = RCClasses.URI_FRAGMENT)
@XmlSeeAlso({RCClass.class, Items.class})
@NoArgsConstructor
public class RCClasses extends Items<RCClass> {

    public final static String URI_FRAGMENT = "Classes";

    public RCClasses(Meta meta, List<RCClass> items) {
        super(meta, items);
    }

    public RCClasses(UriInfo uriInfo, List<RCClass> items) {
        this(new AdfMetaBuilder(uriInfo).add(URI_FRAGMENT).build(),
            items);
    }
}
