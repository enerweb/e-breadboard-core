package za.co.enerweb.ebr.adf.data_types;

import za.co.enerweb.ebr.adf.model.AdfDataType;
import za.co.enerweb.ebr.adf.model.AdfDataTypeClass;

public class DtGisLocationArray extends DtGisLocation {
    public static final String KEY = deriveKey(DtGisLocationArray.class);

    public String getDescription() {
        return "Array of geographical points. " + DECIMAL_DEGREES;
    }

    public AdfDataTypeClass getAdfDataTypeClass() {
        return AdfDataTypeClass.ARRAY;
    }

    public Class<? extends AdfDataType<?>> getItemDataType() {
        return DtGisLocation.class;
    }
}
