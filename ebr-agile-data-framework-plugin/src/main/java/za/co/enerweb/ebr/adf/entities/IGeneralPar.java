package za.co.enerweb.ebr.adf.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, of = { })
@ToString(callSuper = true, of = { })
@Entity
@Table(name = "I_GENERAL_PAR")
public class IGeneralPar extends IbInstanceParameterNorm {

    @JoinColumn(name = "GENERAL_ID", referencedColumnName = "ID")
    @ManyToOne
    private IGeneral iGeneral;

    public IGeneralPar(Integer id) {
        super(id);
    }

    public IGeneralPar(Integer id, Date appldatetime, String val) {
        super(id, appldatetime, val);
    }
}
