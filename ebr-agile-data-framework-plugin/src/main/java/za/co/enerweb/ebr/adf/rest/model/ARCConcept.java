package za.co.enerweb.ebr.adf.rest.model;

import static org.apache.commons.lang.StringUtils.isBlank;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

import org.apache.commons.beanutils.PropertyUtils;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

@Data
@XmlRootElement
public abstract class ARCConcept {

    protected static final DateTimeFormatter DATE_TIME_FORMATTER =
        ISODateTimeFormat.dateTimeNoMillis();

    public static String keyFromSlug(String slug) {
        return splitOnDashes(slug)[0].toUpperCase();
    }

    /**
     * this implementation assumes the id is at the end of the id
     * @param idFieldValue
     * @return
     */
    public static Integer idFromSlug(String slug) {
        return Integer.valueOf(splitOnDashes(slug)[1]);
    }

    protected static String parentSlug(String slug) {
        return splitOnDots(slug)[0];
    }

    protected static String childSlug(String slug) {
        return splitOnDots(slug)[1];
    }

    protected static String[] splitOnDashes(String slug)
        throws IllegalArgumentException {
        String[] split = slug.split("-");
        if (split.length < 2) {
            throw new IllegalArgumentException("Invalid object reference: "
                + slug);
        }
        return split;
    }

    protected static String[] splitOnDots(String slug)
        throws IllegalArgumentException {
        String[] split = slug.split("\\.");
        if (split.length < 2) {
            throw new IllegalArgumentException("Invalid object reference: "
                + slug);
        }
        return split;
    }

    /**
     * Subclasses should override this
     */
    public void validateNew() {

    }

    public void validateFields(String... fieldNames) {
        for (String fieldName : fieldNames) {
            Object value;
            try {
                value = PropertyUtils.getProperty(this, fieldName);
            } catch (Exception e) {
                throw new RuntimeException("Invalid field '" + fieldName
                    + "' of " + getClass().getSimpleName(), e);
            }
            validate(value, fieldName);
        }
    }

    private <T> T validate(T value, String fieldName) {
        if (value == null ||
            (value instanceof String && isBlank((String) value))) {
            throw new IllegalArgumentException(fieldName
                + " must be specified");
        }
        return value;
    }
}
