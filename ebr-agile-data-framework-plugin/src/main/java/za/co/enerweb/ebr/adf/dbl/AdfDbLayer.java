
package za.co.enerweb.ebr.adf.dbl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.val;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.WordUtils;

import za.co.enerweb.ebr.adf.data_types.DtNull;
import za.co.enerweb.ebr.adf.data_types.DtNumberArray;
import za.co.enerweb.ebr.adf.entities.CmDatatype;
import za.co.enerweb.ebr.adf.entities.CmDatatypeclass;
import za.co.enerweb.ebr.adf.entities.CmEntity;
import za.co.enerweb.ebr.adf.entities.CmHierarchyDef;
import za.co.enerweb.ebr.adf.entities.CmParamdef;
import za.co.enerweb.ebr.adf.entities.CmReltypedef;
import za.co.enerweb.ebr.adf.entities.CmTypedef;
import za.co.enerweb.ebr.adf.entities.HierarchyRelationship;
import za.co.enerweb.ebr.adf.entities.IbInstance;
import za.co.enerweb.ebr.adf.entities.IbInstanceParameter;
import za.co.enerweb.ebr.adf.model.AdfDataType;
import za.co.enerweb.ebr.adf.model.AdfDataTypeClass;
import za.co.enerweb.ebr.adf.model.AdfEntity;
import za.co.enerweb.ebr.adf.model.AdfHierarchyFieldMetadata;
import za.co.enerweb.ebr.adf.model.AdfHierarchyTypeDefMetadata;
import za.co.enerweb.ebr.adf.model.AdfInstance;
import za.co.enerweb.ebr.adf.model.AdfParameterDefMetadata;
import za.co.enerweb.ebr.adf.model.AdfRelationshipFieldMetadata;
import za.co.enerweb.ebr.adf.model.AdfRelationshipTypeDefMetadata;
import za.co.enerweb.ebr.adf.model.AdfTypeDefMetadata;
import za.co.enerweb.ebr.adf.model.EntityType;
import za.co.enerweb.ebr.adf.model.ProbabilityInfo;
import za.co.enerweb.toolbox.reflection.PropertyUtils;

/**
 * Data Access Object: separates persistence from the application logic.
 * Find methods may return null - you should check for it, but
 * get methods should throw a proper exception if it can't be found..
 */
@Stateful
@LocalBean
@Slf4j
public class AdfDbLayer extends AdfLowlevelDbLayer {

    public static final String UNIT_OF_MEASURE_PER_UNIT = "PU";

    public static final Integer ADF_VERSION = 1;


    /**
     * If you override this, you should call super too.
     */
    public synchronized void checkMetadata() {
        // find version
        // IbInstance configInstance = null;
        // try {
        // configInstance = findInstanceByCaption(
        // TAdfConfiguration.KEY,
        // TAdfConfiguration.CAPTION);
        // } catch (Exception e) {
        // log.debug("Can't find type: " + TAdfConfiguration.KEY);
        // }
        // if (configInstance == null) {
            fixAllMetadata(0);
        // } else {
        // Integer adfVersion = getParamValue(configInstance,
        // TAdfConfiguration.ADF_VERSION);
        // if (adfVersion == null) {
        // fixAllMetadata(0);
        // } else if (adfVersion.intValue() < ADF_VERSION) {
        // fixAllMetadata(adfVersion.intValue());
        // }
        // }
        em.flush();

        checkAllEntities();
        checkAllDataTypeClasses();
        checkAllDataTypes();
        checkAllTypeDefs();
        checkAllRelTypeDefs();
        checkAllHierarchyTypeDefs();

        // check some data integrity
        try {
            findAll(CmTypedef.class);
            findAll(CmReltypedef.class);
            findAll(CmHierarchyDef.class);
        } catch (Exception e) {
            log.error("Your metadata is screwed up. Fix it!\n"
                + "Maybe you have a CM_TYPEDEF, CM_RELTYPEDEF or "
                + "CM_HIERARCHY_DEF referencing a non-existing CM_ENTITY");
        }
        em.flush();
    }

    // does not seem to work :(
    // @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    private void checkAllEntities() {
        // select current types
        List<CmEntity> existingTypes = findAll(CmEntity.class);
        Set<String> existingKeys = new HashSet<String>();

        // extract keys
        for (CmEntity cmEntity : existingTypes) {
            existingKeys.add(cmEntity.getEntity());
        }

        // iterate through all the registered types
        val allEntities = adfRegistry.getAllAdfEntities();
        for (AdfEntity adfEntity : allEntities) {
            // each unknown one should now be inserted.
            if (!existingKeys.contains(adfEntity.getKey())) {
                log.info("Adding CmEntity to db: " + adfEntity.getKey());
                save(new CmEntity(adfEntity.getKey(),
                    adfEntity.getCaption(),
                    getOrCreateCmEntitytype(adfEntity.getEntityType())));
            }
        }
    }

    private void checkAllDataTypeClasses() {
        for (AdfDataTypeClass dtc : AdfDataTypeClass.values()) {
            getOrCreateAdfDataTypeClass(dtc);
        }
    }

    // @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    private void checkAllDataTypes() {
        // select current types
        List<CmDatatype> existingTypes = findAll(CmDatatype.class);
        Set<String> existingKeys = new HashSet<String>();

        // extract keys
        for (CmDatatype cmDatatype : existingTypes) {
            existingKeys.add(cmDatatype.getKey());
        }

        // iterate through all the registered types
        val allDataTypes = adfRegistry.getAllDataTypes();
        val newDataTypesWithItemDataType =
            new HashMap<AdfDataType<?>, CmDatatype>();
        for (AdfDataType<?> adfDataType : allDataTypes) {
            // each unknown one should now be inserted.
            if (!adfDataType.getClass().equals(DtNull.class)
                && !existingKeys.contains(adfDataType.getKey())) {
                log.info("Adding CmDatatype to db: " + adfDataType.getKey());
                CmDatatype cmDatatype = new CmDatatype(adfDataType.getKey(),
                    adfDataType.getDescription(),
                    getOrCreateAdfDataTypeClass(
                    adfDataType.getAdfDataTypeClass()));
                if (adfDataType.getItemDataType() != null) {
                    newDataTypesWithItemDataType.put(adfDataType, cmDatatype);
                } else {
                    save(cmDatatype);
                }
            }
        }

        // make sure the item data type is inserted first
        for (val entry : newDataTypesWithItemDataType.entrySet()) {
            AdfDataType<?> adfDataType = entry.getKey();
            CmDatatype cmDatatype = entry.getValue();
            cmDatatype.setItemdatatype(
                getCmDataType(adfDataType.getItemDataType()));
            save(cmDatatype);
        }
    }

    // @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    private void checkAllTypeDefs() {
        // iterate through db types and load captions descriptions from db
        for (CmTypedef tf : new ArrayList<CmTypedef>(this.findAllCmTypedefs())) {
            AdfTypeDefMetadata adfTypeDef = adfRegistry.getAdfTypeDefOrNull(
                tf.getKey());
            if (adfTypeDef != null) {
                // set caption and description from the db..
                adfTypeDef.setCaption(tf.getCaption());
                adfTypeDef.setDescription(tf.getDescription());
            }
        }

        // iterate through all the registered types and make sure they
        // are in the db
        val allDataTypes = adfRegistry.getAllTypeDefs();
        for (AdfTypeDefMetadata adfTypeDefMetadata : allDataTypes) {
            if (adfTypeDefMetadata.isAbstractSupertype()) {
                continue;
            }
            // each unknown one should now be inserted.
            String key = adfTypeDefMetadata.getKey();
            log.debug("Checking CmTypedef : " + key
                + " " + adfTypeDefMetadata.getCaption());
            CmTypedef cmTypeDef = getOrCreateCmTypedef(
                getCmEntity(adfTypeDefMetadata.getEntityClass()),
                key);
            cmTypeDef.setCaption(adfTypeDefMetadata.getCaption());
            cmTypeDef.setDescription(
                adfTypeDefMetadata.getDescription());
            em.persist(cmTypeDef);

            Map<String, AdfParameterDefMetadata> registeredParameters =
                adfTypeDefMetadata.getParameters();
            for (AdfParameterDefMetadata parameter :
                registeredParameters.values()) {

                log.info("Adding CmParamdef to db: " +
                    parameter.getKey()
                    + " " + parameter.getCaption());

                CmParamdef p = getOrCreateCmParamdef(cmTypeDef,
                    parameter.getKey(),
                    parameter.getCaption(),
                    parameter.getDescription(),
                    parameter.getParameterKind(),
                    parameter.getTypeClass(),
                    parameter.getCmUnitofmeasure(),
                    parameter.getIndexTypeClass());

                em.persist(p);
            }

            for(CmParamdef p:cmTypeDef.getCmParamdefs()) {
                val pm = registeredParameters.get(p.getKey());
                if (pm == null) {
                    AdfDataType<?> adfDataType = adfRegistry.getAdfDataType(
                        p.getCmDatatype());
                    adfRegistry.registerParamDef(adfTypeDefMetadata,
                        new AdfParameterDefMetadata(
                            null, p.getKey(), p.getCaption(),
                            p.getDescription(),
                            p.getCmUnitofmeasure(),
                            p.getParameterKind(),
                            adfDataType.getDataTypeClass(),
                            adfRegistry.getDataTypeDtClass(
                                p.getCmIndexDatatype()),
                            p.getParamTable(), adfDataType.getAdfDataTypeClass()
                                .equals(AdfDataTypeClass.ARRAY)
                            ));
                    log.debug("registered parameter from db: "
                        + p.getCaption());
                }
            }
            // em.flush(); // FIXME: not sure why it is not persisting
        }
    }

    // @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    private void checkAllRelTypeDefs() {
        // select current types
        List<CmReltypedef> existingTypes = findAll(CmReltypedef.class);
        Set<String> existingKeys = new HashSet<String>();

        // extract keys
        for (CmReltypedef cmDatatype : existingTypes) {
            existingKeys.add(cmDatatype.getKey());
        }

        // iterate through all the registered types
        val allDataTypes = adfRegistry.getAllRelationshipTypeDefs();
        for (AdfRelationshipTypeDefMetadata adfTypeDefMetadata : allDataTypes) {
            // each unknown one should now be inserted.
            String key = adfTypeDefMetadata.getKey();
            if (!existingKeys.contains(key)) {
                log.info("Adding CmReltypedef to db: " + key
                    + " " + adfTypeDefMetadata.getCaption());
                CmReltypedef cmTypeDef = new CmReltypedef(
                    getCmEntity(adfTypeDefMetadata.getEntityClass()),
                    key,
                    adfTypeDefMetadata.getCaption(),
                    adfTypeDefMetadata.getDescription(),
                    getCmTypedef(adfTypeDefMetadata.getFromTypeDef()
                        .getKey()),
                    getCmTypedef(adfTypeDefMetadata.getToTypeDef()
                        .getKey()));
                em.persist(cmTypeDef);

//                for (AdfParameterDefMetadata parameter : adfTypeDefMetadata
//                    .getParameters().values()) {
//
//                    log.info("Adding CmParamdef to db: " +
//                        parameter.getKey()
//                        + " " + parameter.getCaption());
//
//                    CmParamdef p = new CmParamdef(cmTypeDef,
//                        parameter.getKey(),
//                        parameter.getCaption(),
//                        parameter.getDescription(),
//                        getOrCreateParamKind(parameter.getParameterKind()),
//                        getCmDataType(parameter.getType()),
//                        null, // XXX: cmUnitofmeasure,
//                        getCmDataType(parameter.getIndexType()));
//
//                    em.persist(p);
//                }
            }
        }
    }

    // @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    private void checkAllHierarchyTypeDefs() {
        // select current types
        List<CmHierarchyDef> existingTypes = findAll(CmHierarchyDef.class);
        Set<String> existingKeys = new HashSet<String>();

        // extract keys
        for (CmHierarchyDef cmDatatype : existingTypes) {
            existingKeys.add(cmDatatype.getKey());
        }

        // iterate through all the registered types
        val allDataTypes = adfRegistry.getAllHierarchyTypeDefs();
        for (AdfHierarchyTypeDefMetadata adfHierTypeDef : allDataTypes) {
            // each unknown one should now be inserted.
            String key = adfHierTypeDef.getKey();
            if (!existingKeys.contains(key)) {
                log.info("Adding CmHierarchyDef to db: " + key
                    + " " + adfHierTypeDef.getCaption());
                CmHierarchyDef cmTypeDef = new CmHierarchyDef(
                    getCmEntity(adfHierTypeDef.getEntityClass()),
                    key,
                    adfHierTypeDef.getCaption(),
                    adfHierTypeDef.getDescription());
                em.persist(cmTypeDef);
                existingKeys.add(key);
            }
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    private void fixAllMetadata(int adfVersion) {
        log.info("Upgrading ADF db from version " + adfVersion + " to "
            + ADF_VERSION);
        if (adfVersion < 1) {
            // old db's may still have an invalid empty itemdatatype_key in
            // stead
            // of null. just fix it for now..
            try {
                em.createQuery(
                    "update " + CmDatatype.class.getSimpleName() +
                        " set itemdatatype = null "
                        + "where itemdatatype.key = ''").executeUpdate();
            } catch (Exception e) {
                log.warn(
                    "Could not update legacy CM_DATATYPE.itemdatatype_key "
                        + "from '' to null", e);
            }

        }

    }


    public ProbabilityInfo getNumArrayProbability(
        final IbInstance instance, final CmParamdef cmParamdef) {
        assertParameterDataType(cmParamdef, DtNumberArray.class);

        boolean normalised = !cmParamdef.getParameterKind().isDenorm();

        Object[] ranges = getRanges(instance, cmParamdef, normalised);

        ProbabilityInfo ret = new ProbabilityInfo();
        ret.setMaxValues(getProbabilities(instance, cmParamdef, normalised,
            (Date) ranges[0]));
        ret.setMinValues(getProbabilities(instance, cmParamdef, normalised,
            (Date) ranges[2]));
        return ret;
    }

    private double[] getProbabilities(final IbInstance instance,
        final CmParamdef cmParamdef, final boolean normalised,
        final Date applDate) {
        String checkParamKey = "";
        String valueProperty = cmParamdef.getValueProperty();
        if (normalised) {
            checkParamKey = "AND x.cmParamdef = :cmParamdef ";
        }
        Query q = em.createQuery(
            "SELECT x." + valueProperty + " FROM "
                + cmParamdef.getParamJpaEntity() + " x "
                + "WHERE x." + cmParamdef.getInstanceProperty()
                + " = :instance "
                + checkParamKey
                + "AND x.appldatetime = :applDate "
                + "ORDER BY x."
                + valueProperty // the algorithms want the values sorted
        ).setParameter("instance", instance)
            .setParameter("applDate", applDate);
        if (normalised) {
            q.setParameter("cmParamdef", cmParamdef);
        }
        @SuppressWarnings("unchecked") List<Double> valueList =
            q.getResultList();
        int dataSize = valueList.size();
        // this returns a probability for each value
        // double[][] ret = new double[size][];
        // int i = 0;
        // for (Double value : valueList) {
        // ret[i] = new double[]{value, 100 - ((i+1.0)/ size) * 100};
        // i++;
        // }
        int probabilitiesLength = 101;
        double[] ret = new double[probabilitiesLength];
        double ratio = ((double) dataSize) / probabilitiesLength;
        for (int i = 0; i < probabilitiesLength; i++) {
            int index = (int) Math.round(i * ratio);
            ret[i] = valueList.get(index);
        }
        return ret;
    }

    /**
     * @return max_date, max_range, min_date, min_range
     */
    private Object[] getRanges(final IbInstance instance,
        final CmParamdef cmParamdef, final boolean normalised) {
        String checkParamKey = "";
        String valueColumn = cmParamdef.getCmParamdefPK().getKey();
        if (normalised) {
            checkParamKey = "AND x.KEY = :cmParamdefKey ";
            valueColumn = "cast(" + valueColumn + " as DOUBLE)";
        }
        // using native query for the h2 function
        String rangeQuery =
            "SELECT x.appldatetime, "
                + "max(x." + valueColumn + ") - min(x." + valueColumn
                + ") range FROM "
                + cmParamdef.getParamTableName() + " x "
                + "WHERE x." + cmParamdef.getInstanceColumn()
                + " = :instanceId "
                + checkParamKey
                + "GROUP BY x.appldatetime";
        Query q =
            em.createNativeQuery(
                "SELECT max(rmax.appldatetime) max_date, mm.max_range, "
                    + "min(rmin.appldatetime) min_date, mm.min_range FROM ("
                    + "  SELECT max(r.range) max_range, "
                    + "min(r.range) min_range FROM ("
                    + rangeQuery
                    + ") r ) mm, ("
                    + rangeQuery
                    + ") rmax, ("
                    + rangeQuery
                    + ") rmin "
                    + "where mm.max_range = rmax.range "
                    + "and mm.min_range = rmin.range"
                ).setParameter("instanceId", instance.getId());
        if (normalised) {
            q.setParameter("cmParamdefKey",
                cmParamdef.getCmParamdefPK().getKey());
        }
        return (Object[]) q.getSingleResult();
    }


    public IbInstance findInstance(AdfInstance adfInstance) {
        AdfEntity adfEntity = adfInstance.getAdfEntity(getAdfRegistry());

        if (adfInstance == null
            || adfEntity == null
            || adfInstance.getId() == null) {
            return null;
        }
        return findInstanceByEntity(
            adfEntity.getKey(),
            adfInstance.getId());
    }


    // // vvv adf-logic
    //
    // public List<TreeNode> buildEntityTree(final TreeSpec treespec) {
    // List<TreeNode> ret = new ArrayList<TreeNode>();
    // boolean allEntities = treespec.getEntities().isEmpty();
    // String whereEntities = "";
    // if (!allEntities) {
    // whereEntities = " AND c.entity in (:entities) ";
    // }
    // Query q = em.createQuery(
    // "SELECT c FROM CmEntity c "
    // + "WHERE c.cmEntitytype.key = 'INSTANCE'"
    // + whereEntities +
    // " ORDER BY c.caption");
    // if (!allEntities) {
    // q.setParameter("entities", treespec.getEntities());
    // }
    // @SuppressWarnings("unchecked") Collection<CmEntity> cmEntities =
    // q.getResultList();
    //
    // for (CmEntity cmEntity : cmEntities) {
    // TreeNode node = new TreeNode(
    // cmEntity.getCaption(), null,
    // cmEntity);
    // addTypes(treespec, node, cmEntity);
    // ret.add(node);
    // }
    // return ret;
    // }
    //
    // protected void addTypes(final TreeSpec treespec, final TreeNode
    // parentNode,
    // final CmEntity cmEntity) {
    // Collection<CmTypedef> cmTypedefs = cmEntity.getCmTypedefs();
    // Collection<String> includeTypeDefs = treespec.getIncludeTypeDefs();
    // Collection<String> excludeTypeDefs = treespec.getExcludeTypeDefs();
    // for (CmTypedef cmTypedef : cmTypedefs) {
    // if (includeTypeDefs != null) {
    // if (!includeTypeDefs.contains(cmTypedef.getKey())) {
    // continue;
    // }
    // } else if (excludeTypeDefs != null && excludeTypeDefs.contains(
    // cmTypedef.getKey())) {
    // continue;
    // }
    //
    // TreeNode node = new TreeNode(
    // cmTypedef.getCaption(), cmTypedef.getDescription(),
    // cmTypedef);
    // addInstances(treespec, node, cmTypedef);
    // parentNode.addChild(node);
    // }
    // }
    //
    // private void addInstances(final TreeSpec treespec,
    // final TreeNode parentNode,
    // final CmTypedef cmTypedef) {
    // try {
    // List<IbInstance> instances = findInstancesByType(cmTypedef);
    // if (!instances.isEmpty()) {
    // new InstanceGrouper() {
    // public void addInstance(TreeSpec treespec,
    // TreeNode parentNode, IbInstance instance) {
    // TreeNode node = new TreeNode(
    // instance.getCaption(), null, instance);
    // parentNode.addChild(node);
    // addRelationshipsTypes(treespec, node, instance);
    // }
    // }.addInsances(treespec, parentNode,
    // cmTypedef.getKey() + " instance group ", instances);
    // }
    // } catch (Exception e) {
    // log.warn("Could not add instances of type "
    // + cmTypedef.getCaption(), e);
    // }
    // }
    //
    // private void addRelationshipsTypes(final TreeSpec treespec,
    // final TreeNode parentNode,
    // final IbInstance instance) {
    // if (treespec.isRelationshipsWanted()) {
    // for (CmReltypedef rel : instance.getCmTypedef().getRelatedFrom()) {
    // TreeNode node =
    // new TreeNode(
    // // cmTypedef.getKey(),
    // rel.getCaption() + " => "
    // + rel.getToCmTypedef().getCaption(),
    // rel.getDescription(),
    // new InstanceRelationshipTypeDef(instance, rel)
    // );
    // addRelationships(treespec, node, instance, rel);
    // parentNode.addChild(node);
    // }
    // }
    // }
    //
    // private void addRelationships(final TreeSpec treespec,
    // final TreeNode parentNode,
    // final IbInstance fromInstance, final CmReltypedef rel) {
    // try {
    // List<IbInstance> instances = findRelatedInstances(
    // fromInstance, rel);
    // if (!instances.isEmpty()) {
    // new InstanceGrouper() {
    // public void addInstance(TreeSpec treespec,
    // TreeNode parentNode, IbInstance instance) {
    // TreeNode node =
    // new TreeNode(
    // instance.getCaption(),
    // rel.getDescription(),
    // new InstanceRelationship(fromInstance,
    // instance, rel)
    // );
    // parentNode.addChild(node);
    // }
    // }.addInsances(treespec, parentNode,
    // fromInstance.getCmTypedef().getKey() + fromInstance.getId()
    // + " " + rel.getKey()
    // + " relationship instance group ", instances);
    // }
    // } catch (Exception e) {
    // log.error("Could not add relationship.", e);
    // }
    // }

    /**
     * If id is not null, it will only look for a matching id
     * else it will look for a matching caption
     * else this will create a new instance
     * @param instance
     * @return
     */
    public void saveAdfInstance(
        AdfInstance instance) {
        saveAdfInstance(instance, true, false);
    }

    public void saveAdfInstances(
        Iterable<? extends AdfInstance> instances) {
        for (AdfInstance adfInstance : instances) {
            saveAdfInstance(adfInstance);
        }
    }

    // XXX: make a version that ensures uniqueness on some field
    // on some relationship?!
    // maybe that should go into the metadata?!
    public void saveAdfInstance(
        AdfInstance instance, boolean uniqueCaptionsPerTypeDef,
        boolean eager // means save all fields even lists of related stuff
        ) {
        if (instance == null) {
            return;
        }
        AdfTypeDefMetadata typeDefMetadata = adfRegistry.getTypeDefs().get(
            instance.getClass());
        IbInstance ibInstance =
            determineIbInstance(instance, uniqueCaptionsPerTypeDef,
                typeDefMetadata);

        instance.setId(ibInstance.getId());
        instance.setTypeDefKey(typeDefMetadata.getKey());
        // log.debug("***** SAVED instance: " + ibInstance.getId() + " "
        // + ibInstance.getCaption());


        saveAllParameters(instance, typeDefMetadata, ibInstance);
        // FIXME: this should actually save and delete values too!
        saveRelationships(instance, typeDefMetadata, ibInstance);
        saveHierarchyRelationships(instance, typeDefMetadata, ibInstance,
            eager);

        adfInstanceCache.put(ibInstance, instance);
    }

    public IbInstance determineIbInstance(AdfInstance instance,
        boolean uniqueCaptionsPerTypeDef, AdfTypeDefMetadata typeDefMetadata) {
        String typeDefKey = typeDefMetadata.getKey();

        Integer id = instance.getId();
        IbInstance ibInstance;
        if (id != null) {
            ibInstance = findInstance(typeDefKey, id);
            // val adfInstanceCache = new HashMap<IbInstance, AdfInstance>();
            // then how do we delete relationshis?!!!
            // populateRelationships(ibInstance, typeDefMetadata, instance,
            // adfInstanceCache);
            // populateHierarchies(ibInstance, typeDefMetadata, instance,
            // adfInstanceCache);
        } else {
            ibInstance = findExistingChild(instance, typeDefMetadata);
        }
        if (ibInstance != null) {
            ibInstance.setCaption(instance.getCaption());
        } else {
            if (uniqueCaptionsPerTypeDef) {
                ibInstance = getOrCreateInstance(getCmTypedef(
                    typeDefKey), instance.getCaption());
                em.persist(ibInstance);
            } else {
                // ibInstance = getOrCreateInstance(getCmTypedef(
                // typeDefKey), instance.getCaption());
                ibInstance = createInstance(getCmTypedef(
                    typeDefKey), instance.getCaption());
                em.persist(ibInstance);
            }
            // em.flush(); // FIXME: debuggin
        }
        return ibInstance;
    }

    public void saveAllParameters(AdfInstance instance,
        AdfTypeDefMetadata typeDefMetadata, IbInstance ibInstance) {
        // now save all the parameters
        for (AdfParameterDefMetadata paramDef : typeDefMetadata.getParameters()
            .values()) {
            Object value = instance.get(paramDef);

            // if it is an array type then we should just replace all the
            // actual values..
            AdfDataTypeClass adfDataTypeClass = paramDef.getType(adfRegistry)
                .getAdfDataTypeClass();
            if (adfDataTypeClass.equals(AdfDataTypeClass.SCALAR)) {
                saveParamValue(ibInstance, paramDef.getKey(), value,
                    null, null);
            } else if (adfDataTypeClass.equals(AdfDataTypeClass.ARRAY)) {
                // XXX: only do this if we are eager or the
                // parameter is specified as eager..
                List valueList = (List) value;
                if (valueList != null) {

                    // delete the old values and add the new ones..
                    deleteParamValues(ibInstance, paramDef.getKey());
                    @SuppressWarnings("rawtypes") int i = 0;
                    for (Object item : valueList) {
                        addParamValue(ibInstance, paramDef.getKey(), item,
                            null, "" + i);
                        i++;
                    }
                }
            }
        }
    }

    private IbInstance findExistingChild(AdfInstance instance,
        AdfTypeDefMetadata typeDefMetadata) {
        // see if we have unique fields per parent,
        // if we do, see if the the entity already exists..
        for (AdfHierarchyFieldMetadata h : typeDefMetadata.getHierarchies()) {
            if (h.isParentReference()
                && h.getUniqueFieldPerParent() != null) {
                String uniqueFieldPerParent = h.getUniqueFieldPerParent();
                Object filterValue = PropertyUtils.getProperty(instance,
                    uniqueFieldPerParent);
                Object parentValue = PropertyUtils.getProperty(instance,
                    h.getFieldName());
                if (filterValue != null
                    && parentValue instanceof AdfInstance) {
                    // find parent's children with filter
                    IbInstance inst = findInstance(
                        (AdfInstance) parentValue);
                    if (inst != null) {
                        List<IbInstance> children =
                            findHierarchyNeighbours(h.getKey(), inst, null,
                                true, new InstanceFilter()
                                    .instanceClass(instance.getClass())
                                    .fieldName(uniqueFieldPerParent)
                                    .value(filterValue)
                            );
                        if (!children.isEmpty()) {
                            return children.get(0);
                        }
                    }
                }
            }
        }
        return null;
    }

    private void saveRelationships(AdfInstance instance,
        AdfTypeDefMetadata typeDefMetadata, IbInstance ibInstance) {
        // save relationships
        for (AdfRelationshipFieldMetadata relMd : typeDefMetadata
            .getRelationships()) {
            CmReltypedef cmReltypedef = getCmReltypedef(relMd.getRelTypeDef());
            Object val = PropertyUtils
                .getProperty(instance, relMd.getFieldName());
            if (val == null) {
                continue;
            }
            List<IbInstance> newRelatedInstances;
            if (val instanceof List) {
                @SuppressWarnings("rawtypes") List valList = (List) val;
                newRelatedInstances = new ArrayList<IbInstance>(valList.size());
                for (Object valItem : valList) {
                    Integer id = ((AdfInstance) valItem).getId();
                    if (id == null) {
                        throw new IllegalArgumentException("Could not save" +
                            " relationship of instance " + instance
                            + " because it has not been saved yet: "
                            + valItem);
                    }
                    newRelatedInstances.add(
                        findInstance(relMd.getTypeDef().getKey(),
                            id));
                }
            } else {
                newRelatedInstances = Collections.singletonList(
                    findInstance(relMd.getTypeDef().getKey(),
                        ((AdfInstance) val).getId()));
            }
            saveRelatedInstances(ibInstance,
                newRelatedInstances, cmReltypedef,
                relMd.isToReference() // opposite direction
            );
        }
    }

    private void saveHierarchyRelationships(AdfInstance instance,
        AdfTypeDefMetadata typeDefMetadata, IbInstance ibInstance,
        boolean eager) {
        for (AdfHierarchyFieldMetadata hierMd : typeDefMetadata
            .getHierarchies()) {
            CmHierarchyDef cmHierarchyDef =
                getCmHierarchyDef(hierMd.getHierTypeDef());
            Object val = PropertyUtils
                .getProperty(instance, hierMd.getFieldName());
            if (val == null) {
                continue;
            }
            if (!(hierMd.isEager() || eager)) {
                continue;
            }
            // delete old links!
            deleteHierarchyNeighbours(cmHierarchyDef.getKey(),
                ibInstance,
                null,
                hierMd.isChildReference());

            if (val instanceof List) {
                // XXX: this still saves duplicates if the list contains
                // multiple items with the same id..
                @SuppressWarnings("rawtypes") List valList = (List) val;
                for (Object valItem : valList) {
                    Integer id = ((AdfInstance) valItem).getId();
                    if (id == null) {
                        throw new IllegalArgumentException("Could not save" +
                            " relationship of instance " + instance
                            + " because it has not been saved yet: "
                            + valItem);
                    }
                    AdfInstance adfInstance = (AdfInstance) valItem;
                    if (adfInstance != null) {
                        IbInstance foundInstance = findInstance(adfInstance);
                        if (foundInstance != null) {
                            linkHierarchyInstancesIfNeeded(cmHierarchyDef,
                                ibInstance, foundInstance,
                                null, null, null,
                                hierMd.isChildReference());
                        }
                    }
                }
            } else {
                AdfInstance adfInstance = (AdfInstance) val;
                linkHierarchyInstancesIfNeeded(cmHierarchyDef,
                    ibInstance, findInstance(adfInstance),
                    null, null, null,
                    hierMd.isChildReference()
                    );
            }
        }
    }

    public <T extends AdfInstance> T findAdfInstance(T adfInstance) {
        return findAdfInstance(adfInstance, false);
    }

    /**
     * Load the instance from scratch
     */
    public <T extends AdfInstance> T findAdfInstance(T adfInstance,
        boolean eager) {
        if (adfInstance == null) {
            return null;
        }
        return findAdfInstance(
            adfInstance.getTypeDef(getAdfRegistry()).getEntityClass(),
            adfInstance.getId(), eager);
    }

    public <T extends AdfInstance> T findAdfInstance(
        Class<? extends AdfEntity> entityClass,
        Integer id) {
        return findAdfInstance(entityClass, id, false);
    }

    /*
     * to support only loading a subset of properties,
     * all specifying the ones you want to load: String... properties
     * or alternative ly specifying a class which' overlapping fields
     * should be populated.. in stead of the main typedef class
     */
    @SuppressWarnings("unchecked")
    public <T extends AdfInstance> T findAdfInstance(
        Class<? extends AdfEntity> entityClass,
        Integer id, boolean eager) {
        // AdfTypeDefMetadata typeDefMetadata = adfRegistry.getTypeDefs().get(
        // instanceClass);
        // String typeDefKey = typeDefMetadata.getKey();
        // IbInstance ibInstance = findInstance(typeDefKey, id);
        CmEntity cmEntity = getCmEntity(entityClass);
        if (!cmEntity.getEntityType().equals(EntityType.INSTANCE)) {
            throw new IllegalArgumentException(
                "Only instances are supported by this method.");
        }
        IbInstance ibInstance = findInstanceByEntity(cmEntity, id);
        return (T) populateAdfInstance(ibInstance, eager);
    }

    public void removeAdfInstances(
        AdfInstance... instances) {
        removeAdfInstances(Arrays.asList(instances));
    }

    public void removeAdfInstances(
        Iterable<? extends AdfInstance> instances) {
        for (AdfInstance adfInstance : instances) {
            removeAdfInstance(adfInstance);
        }
    }

    public void removeAdfInstance(AdfInstance adfInstance) {
        if (adfInstance != null) {
            IbInstance ibInstance = findInstance(adfInstance);
            invalidateCachedAdfInstances(ibInstance);
            removeInstances(ibInstance);
        }
    }

    public void transferHierarchyLinks(
        AdfInstance sourceInstance, AdfInstance targetInstance) {
        transferHierarchyLinks(
            findInstance(sourceInstance), findInstance(targetInstance));
    }

    public void linkHierarchyInstances(String hierarchyKey,
        AdfInstance parentInstance, AdfInstance childInstance,
        Date startDatetime, Date endDatetime) {
        linkHierarchyInstances(hierarchyKey,
            findInstance( parentInstance),  findInstance(childInstance),
            startDatetime, endDatetime);
    }

    public <T extends AdfInstance> List<T> findAdfInstances(
        Class<? extends AdfEntity> entityClass,
        List<IbInstance> ibInstances) {
        val ret = new ArrayList<T>();
        for (IbInstance ibInstance : ibInstances) {
            ret.add((T) findAdfInstance(entityClass, ibInstance.getId()));
        }
        return ret;
    }

    public <T extends AdfInstance> List<T> findAdfInstancesByEntity(
        Class<? extends AdfEntity> entityClass) {
        return findAdfInstancesByEntity(entityClass, false);
    }

    public <T extends AdfInstance> List<T> findAdfInstancesByEntity(
        Class<? extends AdfEntity> entityClass, boolean eager) {
        List<IbInstance> instances =
            findInstancesByEntity(adfRegistry.getAdfEntity(entityClass)
                .getKey());
        return populateAdfInstances(instances, eager);
    }

    @SuppressWarnings("unchecked")
    private <T> List<T> populateAdfInstances(List<IbInstance> instances,
        boolean eager) {
        List<T> ret = new ArrayList<T>(instances.size());
        for (IbInstance ibInstance : instances) {
            ret.add((T) populateAdfInstance(ibInstance, eager));
        }
        return ret;
    }

    public <T extends AdfInstance> List<T> findAdfInstancesByType(
        Class<T> instanceClass) {
        return populateAdfInstances(findInstancesByType(
            getCmTypedef(instanceClass)), false);
    }

    public List<AdfInstance> findAdfInstancesByType(
        List<Class<? extends AdfInstance>> instanceClasses) {
        return populateAdfInstances(findInstancesByType(
            getCmTypedefsArray(instanceClasses)), false);
    }

    public <T extends AdfInstance> List<T> findAdfInstancesByType(
        final String typedefKey) {
        return populateAdfInstances(findInstancesByType(
            getCmTypedef(typedefKey)), false);
    }

    @SuppressWarnings("unchecked")
    public <T extends AdfInstance> List<T> findHierarchyNeighbourAdfInstances(
        String hierarchyKey, AdfInstance instance,
        Date effectiveDate, boolean findChildren,
        InstanceFilter... filters) {

        List<IbInstance> ibInstances = findHierarchyNeighbours(
            hierarchyKey, findInstance(instance),
            effectiveDate,
            findChildren, filters);

        List<T> ret = new ArrayList<T>(ibInstances.size());
        for (IbInstance ibInstance : ibInstances) {
            ret.add((T) populateAdfInstance(ibInstance, false));
        }
        return ret;
    }

    @SuppressWarnings("unchecked")
    private <T extends AdfInstance> T populateAdfInstance(
        IbInstance ibInstance
        , boolean eager) {
        if (ibInstance == null) {
            return null;
        }
        T ret = (T) adfInstanceCache.getIfPresent(ibInstance);
        if (ret == null || (eager && !ret.isLoadedEager())) {
            String typeDefKey = ibInstance.getCmTypedef().getKey();
            AdfTypeDefMetadata typeDefMetadata =
                adfRegistry.getAdfTypeDef(
                    typeDefKey);
            ret = (T) typeDefMetadata.newAdfInstance();
            ret.setTypeDefKey(typeDefKey);
            ret.setLoadedEager(eager);
            ret.setId(ibInstance.getId());
            adfInstanceCache.put(ibInstance, ret);
            ret.setCaption(ibInstance.getCaption());

            loadParameters(ibInstance, ret, typeDefMetadata);

            populateRelationships(ibInstance, typeDefMetadata, ret, eager);
            populateHierarchies(ibInstance, typeDefMetadata, ret, eager);
        }
        return ret;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public <T extends AdfInstance> void loadParameters(IbInstance ibInstance,
        T ret,
        AdfTypeDefMetadata typeDefMetadata) {
        // now load all the parameters
        for (AdfParameterDefMetadata paramDef : typeDefMetadata
            .getParameters()
            .values()) {

            Object value = null;
            AdfDataTypeClass adfDataTypeClass = paramDef.getType(adfRegistry)
                .getAdfDataTypeClass();

            String key = paramDef.getKey();
            if (adfDataTypeClass.equals(AdfDataTypeClass.SCALAR)) {
                value =
                    getParamValue(ibInstance, key, null, null);
            } else if (adfDataTypeClass.equals(AdfDataTypeClass.ARRAY)) {
                val paramList =
                    getSingleArrayParamValue(ibInstance, key,
                        null);
                val valList = new ArrayList(paramList.size());
                for (IbInstanceParameter param :
                    paramList) {
                    valList.add(getParamValue(param, key));
                }
                value = valList;
            }

            ret.set(paramDef, value);
        }
    }

    private void populateRelationships(IbInstance ibInstance,
        AdfTypeDefMetadata typeDefMetadata, AdfInstance ret, boolean eager) {
        // now load all the relationships
        for (AdfRelationshipFieldMetadata relMd : typeDefMetadata
            .getRelationships()) {
            // AdfRelationshipTypeDefMetadata relmd =
            // adfRegistry.getAdfRelationshipTypeDef(rel.getRelationship());
            CmReltypedef cmReltypedef = getCmReltypedef(
                relMd.getRelTypeDef());
            List<IbInstance> relatedInstances;
            relatedInstances = findRelatedInstances(
                ibInstance, cmReltypedef, relMd.isToReference());

            setRelationshipProperty(typeDefMetadata, ret,
                relMd, relatedInstances);
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private void setRelationshipProperty(
        AdfTypeDefMetadata typeDefMetadata, AdfInstance ret,
        AdfRelationshipFieldMetadata rel,
        List<IbInstance> relatedInstances) {
        String fieldName = rel.getFieldName();
        Class<?> propertyType;
        try {
            propertyType = PropertyUtils
                .getPropertyDescriptor(ret, fieldName).getPropertyType();
            // propertyType = ret.getClass()
            // .getField(fieldName).getType();
        } catch (Exception e) {
            log.warn("Instance does not contain expected field: "
                + ret.getClass().getSimpleName() + "."
                + fieldName + " "
                + typeDefMetadata + " " + rel, e);
            return;
        }
        if (propertyType.equals(rel.getTypeDef().getInstanceClass())) {
            if (!relatedInstances.isEmpty()) {
                if (relatedInstances.size() > 1) {
                    log.warn("Instance is related to more than one instance," +
                        " although a single relationship is expected" +
                        " by the typedef.\n  " + typeDefMetadata + "\n  "
                        + rel);
                }
                PropertyUtils.setProperty(ret, fieldName,
                    populateAdfInstance(relatedInstances.get(0), false));
            }
        } else if (propertyType.equals(List.class)) {
             List relatedAdfInstances =
                new ArrayList(
                    relatedInstances.size());
            for (IbInstance relIbInstance : relatedInstances) {
                relatedAdfInstances.add(
                    populateAdfInstance(relIbInstance, false));
            }
            PropertyUtils
                .setProperty(ret, fieldName, relatedAdfInstances);
        } else {
            log.warn("Unsupported type for Instance relationship," +
                "at the moment only Lists are " +
                "supported. " + typeDefMetadata + " " + rel);
        }
    }

    private void populateHierarchies(IbInstance ibInstance,
        AdfTypeDefMetadata typeDefMetadata, AdfInstance ret, boolean eager) {
        for (AdfHierarchyFieldMetadata hierMd : typeDefMetadata
            .getHierarchies()) {
            if (eager || hierMd.isEager()) {
                // CmHierarchyDef cmHierarchyDef = getCmHierarchyDef(
                // hierMd.getHierTypeDef());
                // ^^^ maybe we should look this up and cache it ?!
                List<IbInstance> relatedInstances;
                relatedInstances = findHierarchyNeighbours(
                    hierMd.getHierTypeDef().getKey(), ibInstance, null,
                    hierMd.isChildReference());

                setHierarchyProperty(typeDefMetadata, ret,
                    hierMd, relatedInstances);
            }
        }
    }

    /**
     * This keeps any values already present
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    private void setHierarchyProperty(
        AdfTypeDefMetadata typeDefMetadata, AdfInstance ret,
        AdfHierarchyFieldMetadata rel,
        List<IbInstance> relatedInstances) {
        String fieldName = rel.getFieldName();
        Class<?> propertyType;
        try {
            propertyType = PropertyUtils
                .getPropertyDescriptor(ret, fieldName).getPropertyType();
        } catch (Exception e) {
            log.warn("Instance does not contain expected field: "
                + ret.getClass().getSimpleName() + "."
                + fieldName + " "
                + typeDefMetadata + " " + rel, e);
            return;
        }

        Object origVal = PropertyUtils.getProperty(ret, fieldName);

        if (propertyType.equals(List.class)) {
            List relatedAdfInstances =
                new ArrayList(
                    relatedInstances.size());
            // List<? extends AdfInstance> origAdfInstances;
            // if (origVal != null) {
            // origAdfInstances = (List<? extends AdfInstance>) origVal;
            // } else {
            // origAdfInstances = Collections.emptyList();
            // }
            for (IbInstance relIbInstance : relatedInstances) {
                // if (!AdfInstance.hasInstance(origAdfInstances, relIbInstance
                // .getId())) {
                    relatedAdfInstances.add(
                    populateAdfInstance(relIbInstance, false));
                // }
            }
            PropertyUtils
                .setProperty(ret, fieldName, relatedAdfInstances);
        } else {
            if (!relatedInstances.isEmpty()) {
                if (relatedInstances.size() > 1) {
                    log.warn("Instance is related to more than one instance," +
                        " although a single relationship is expected" +
                        " by the typedef.\n  " + typeDefMetadata + "\n  "
                        + rel);
                }
                // if (origVal == null) {
                PropertyUtils.setProperty(ret, fieldName,
                    populateAdfInstance(relatedInstances.get(0), false));
                // }
            }
        }
        // else {
        // log.warn("Unsupported type for Instance relationship," +
        // "at the moment only Lists are " +
        // "supported. " + typeDefMetadata + " " + rel);
        // }
    }

    @AllArgsConstructor
    @Data
    private class InstanceDateRange {
        IbInstance instance;
        Date start;
        Date end;
    }

    /**
     * In freeform hierarchies roots are identified by having a relationships
     * with a null parent. This method adds such relationships for all nodes
     * that is not a child of any other node at the effective date.
     */
    public void convertOrphanHierarchyNodesToRoots(
        String hierarchyKey, Date effectiveDate) {
        CmHierarchyDef cmHierarchyDef = findHierarchy(hierarchyKey);
        String hierarchyEntity = cmHierarchyDef.getJpaHierarchyEntity();

        // find all orphans (nodes without parents)
        Query q = em.createQuery(
            "SELECT parent FROM "
                + hierarchyEntity + " parent"
                + " WHERE (SELECT count(*)  FROM  " + hierarchyEntity
                + " child WHERE child.cmHierarchyDef = :cmHierarchyDef"
                + " AND child.parentCmTypedef = parent.childCmTypedef "
                + " AND child.parentInstanceId = parent.childInstanceId"
                + " AND :effectiveDate >= child.startDatetime"
                + " AND :effectiveDate < child.endDatetime) = 0"
                + " AND parent.cmHierarchyDef = :cmHierarchyDef"
                + " AND :effectiveDate >= parent.startDatetime"
                + " AND :effectiveDate < parent.endDatetime"
            );
        q.setParameter("cmHierarchyDef", cmHierarchyDef);
        q.setParameter("effectiveDate", effectiveDate);

        @SuppressWarnings("unchecked") List<HierarchyRelationship> orphans =
            q.getResultList();
        // Find unique date ranges for each instance otherwise we could
        // try to add the same one twice.
        // This way each active orphan will be linked with equivalent root
        Set<InstanceDateRange> ranges = new HashSet<InstanceDateRange>(
            orphans.size());
        for (HierarchyRelationship orphan : orphans) {
            ranges.add(new InstanceDateRange(orphan.getParentInstance(this),
                orphan.getStartDatetime(), orphan.getEndDatetime()));
        }
        for (InstanceDateRange range : ranges) {
            log.debug("Converting orphan to root:\n  " + range);
            addHierarchyRootInstance(hierarchyKey,
                range.getInstance(), range.getStart(), range.getEnd());
        }
    }

    public static String toCaption(String key) {
        return WordUtils.capitalizeFully(key.replace('_', ' '));
    }


    public CmTypedef getCmTypedef(
        Class<? extends AdfInstance> instanceClass) {
        AdfTypeDefMetadata md = adfRegistry.getAdfTypeDef(instanceClass);
        CmTypedef ret = find(getCmTypedefQuery(
            getCmEntity(md.getEntityClass()), md.getKey()));
        return ret;
    }

    // public CmTypedef[] getCmTypedefsArray(
    // Class<? extends AdfInstance>... instanceClasses) {
    // val ret = new CmTypedef[instanceClasses.length];
    // for (int i = 0; i < instanceClasses.length; i++) {
    // Class<? extends AdfInstance> c = instanceClasses[i];
    // ret[i] = getCmTypedef(c);
    // }
    // return ret;
    // }

    @SuppressWarnings("unchecked")
    public CmTypedef[] getCmTypedefsArray(
        List<Class<? extends AdfInstance>> instanceClasses) {
        val ret = new CmTypedef[instanceClasses.size()];
        for (int i = 0; i < instanceClasses.size(); i++) {
            Class<? extends AdfInstance> c = instanceClasses.get(i);
            ret[i] = getCmTypedef(c);
        }
        return ret;
    }

    public CmHierarchyDef getCmHierarchyDef(
        AdfHierarchyTypeDefMetadata hierTypeDef) {
        return getCmHierarchyDef(getCmEntity(hierTypeDef.getEntityClass()),
            hierTypeDef.getKey());
    }

    public CmDatatypeclass getOrCreateAdfDataTypeClass(AdfDataTypeClass dtc) {
        CmDatatypeclass ret = find(CmDatatypeclass.class, dtc.name());
        if (ret == null) {
            ret = save(new CmDatatypeclass(dtc));
        }
        return ret;
    }

    public List<IbInstance> findHierarchyRoots(
        String hierarchyKey,
        Date effectiveDate) {
        return findHierarchyChildren(hierarchyKey, null, effectiveDate);
    }

    /**
     * @deprecated since 0.11.0
     */
    @Deprecated
    public List<IbInstance> getHierarchyRoots(
        String hierarchyKey,
        Date effectiveDate) {
        return findHierarchyRoots(hierarchyKey, effectiveDate);
    }

    /**
     * @param hierarchyKey
     * @param parent Iff null then return roots.
     * @param effectiveDate
     * @return
     */
    public List<IbInstance> findHierarchyChildren(
        String hierarchyKey, IbInstance parent,
        Date effectiveDate) {
        return findHierarchyNeighbours(
            hierarchyKey, parent,
            effectiveDate, true);
    }

    /**
     * @deprecated since 0.11.0
     */
    @Deprecated
    public List<IbInstance> getHierarchyChildren(
        String hierarchyKey, IbInstance parent,
        Date effectiveDate) {
        return findHierarchyChildren(
            hierarchyKey, parent, effectiveDate);
    }

    public List<IbInstance> findHierarchyNeighbours(
        String hierarchyKey, IbInstance instance,
        Date effectiveDate, boolean findChildren, InstanceFilter... filters) {
        Query q =
            createHierarchyQuery(hierarchyKey, instance, effectiveDate,
                findChildren, false, filters);
        List<IbInstance> ret = new ArrayList<IbInstance>();
        @SuppressWarnings("unchecked") List<HierarchyRelationship> resultList =
            q.getResultList();
        if (findChildren) {
            for (HierarchyRelationship rel : resultList) {
                ret.add(rel.getChildInstance(this));
            }
        } else {
            for (HierarchyRelationship rel : resultList) {
                ret.add(rel.getParentInstance(this));
            }
        }
        return ret;
    }

    public int deleteHierarchyNeighbours(
        String hierarchyKey, IbInstance instance,
        Date effectiveDate, boolean deleteChildren,
        InstanceFilter... filters) {
        Query q =
            createHierarchyQuery(hierarchyKey, instance, effectiveDate,
                deleteChildren, true, filters);
        return q.executeUpdate();
    }

    private Query createHierarchyQuery(String hierarchyKey,
        IbInstance instance, Date effectiveDate, boolean findChildren,
        boolean delete, InstanceFilter... filters) {
        if (effectiveDate == null) {
            effectiveDate = new Date();
        }
        CmHierarchyDef cmHierarchyDef = getHierarchy(hierarchyKey);
        String hierarchyEntity = cmHierarchyDef.getJpaHierarchyEntity();
        String neighbourWhereClause, sourceCmTypedef, targetCmTypedef;
        StringBuilder filtersFromClause = new StringBuilder();
        StringBuilder filtersWhereClause = new StringBuilder();
        String sourceInstanceId, targetInstanceId;
        if (findChildren) {
            sourceCmTypedef = "parentCmTypedef";
            targetCmTypedef = "childCmTypedef";
            sourceInstanceId = "parentInstanceId";
            targetInstanceId = "childInstanceId";
        } else {
            sourceCmTypedef = "childCmTypedef";
            targetCmTypedef = "parentCmTypedef";
            sourceInstanceId = "childInstanceId";
            targetInstanceId = "parentInstanceId";
        }
        if (instance == null) {
            // look for roots
            neighbourWhereClause = " AND j." + sourceCmTypedef + " is null";
        } else {
            neighbourWhereClause =
                " AND j." + sourceCmTypedef + " = :instanceCmTypedef"
                    + " AND j." + sourceInstanceId + " = :instanceId";
        }

        val jpaParams = new HashMap<String, Object>();
        applyFilters(filtersFromClause, filtersWhereClause, jpaParams,
            targetCmTypedef, targetInstanceId, filters);
        String query = (delete ? "DELETE" : "SELECT j") + " FROM "
            + hierarchyEntity + " j " + filtersFromClause.toString()
            + " WHERE j.cmHierarchyDef = :cmHierarchyDef"
            + neighbourWhereClause
            + " AND :effectiveDate >= j.startDatetime"
            + " AND :effectiveDate < j.endDatetime"
            + filtersWhereClause.toString();
        // log.debug(query);
        Query q = em.createQuery(query
            );
        q.setParameter("cmHierarchyDef", cmHierarchyDef);
        if (instance != null) {
            q.setParameter("instanceCmTypedef", instance.getCmTypedef());
            q.setParameter("instanceId", instance.getId());
        }
        q.setParameter("effectiveDate", effectiveDate);
        for (Entry<String, Object> entry : jpaParams.entrySet()) {
            q.setParameter(entry.getKey(), entry.getValue());
        }
        return q;
    }

    private void applyFilters(StringBuilder filtersFromClause,
        StringBuilder filtersWhereClause,
        final java.util.HashMap<java.lang.String, java.lang.Object> jpaParams,
        String targetCmTypedef, String targetInstanceId,
        InstanceFilter... filters) {
        int fi = 0;
        for (InstanceFilter filter : filters) {
            // do a nasty join :(

            // find paramdef
            AdfTypeDefMetadata adfTypeDef = adfRegistry.getAdfTypeDef(
                filter.instanceClass());
            val parameters = adfTypeDef.getParameters();
            AdfEntity adfEntity = adfRegistry.getAdfEntity(
                adfTypeDef.getEntityClass());
            AdfParameterDefMetadata adfParameterDef = null;
            String fieldName = filter.fieldName();
            for (AdfParameterDefMetadata param : parameters.values()) {
                if (param.getFieldName().equals(fieldName)) {
                    adfParameterDef = param;
                }
            }
            if (adfParameterDef == null) {
                log.warn("Could not find field '" + fieldName
                    + "' in class: " + filter.instanceClass().getName());
            } else {
                String parmTableAlias = "pt" + fi;
                filtersFromClause.append(", " +
                    adfEntity.getJpaParmameterEntity()
                    + " " + parmTableAlias);
                filtersWhereClause.append(
                    " AND " + parmTableAlias + "." +
                    adfEntity.getJpaParmameterInstanceId()
                        + " = j." + targetInstanceId
                        + " AND " + parmTableAlias
                        + ".cmTypedef = j." + targetCmTypedef
                        + " AND " + parmTableAlias
                        + ".key = :cmParamdef_key_" + parmTableAlias +
                        " AND " + parmTableAlias
                        + ".val = :cmParam_val_" + parmTableAlias +
                        " AND :effectiveDate >= "
                        + parmTableAlias + ".appldatetime "
                    // FIXME: a bit difficult to test the end values
                    // should I do a count of newer values?
                    );
                jpaParams.put("cmParamdef_key_" + parmTableAlias,
                    adfParameterDef.getKey());

                // only support simple types..
                @SuppressWarnings("rawtypes") AdfDataType dataType =
                    adfParameterDef.getType(adfRegistry);
                @SuppressWarnings("unchecked") String persistedValue = dataType
                    .toPersistedValue(filter.value());
                jpaParams.put("cmParam_val_" + parmTableAlias,
                    persistedValue);
            }
            fi++;
        }
    }

}
