package za.co.enerweb.ebr.adf.data_types;

import za.co.enerweb.ebr.adf.model.AdfDataType;
import za.co.enerweb.ebr.adf.model.AdfDataTypeClass;

/*
 * TODO: maybe we can make an anotation that can go onto DtText
 * that can contain all this metadata..
 */
public class DtTextArray extends DtText {

    // protected String deriveKey() {
    // return "TEXT_ARRAY";
    // }

    public String getDescription() {
        return "Text Array";
    }

    public AdfDataTypeClass getAdfDataTypeClass() {
        return AdfDataTypeClass.ARRAY;
    }

    public Class<? extends AdfDataType<?>> getItemDataType() {
        return DtText.class;
    }
}
