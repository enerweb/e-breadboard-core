package za.co.enerweb.ebr.adf.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(of = {"fieldName", "toReference", "relTypeDef"})
public final class AdfRelationshipFieldMetadata {

    private String fieldName;
    private boolean toReference;
    private AdfRelationshipTypeDefMetadata relTypeDef;
    private AdfTypeDefMetadata typeDef;

    public String getKey() {
        return relTypeDef.getKey();
    }
}
