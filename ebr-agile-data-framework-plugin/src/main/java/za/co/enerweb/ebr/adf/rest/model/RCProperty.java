package za.co.enerweb.ebr.adf.rest.model;

import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import za.co.enerweb.ebr.adf.entities.CmParamdef;
import za.co.enerweb.ebr.adf.entities.CmTypedef;
import za.co.enerweb.toolbox.rest.ExpandMembers;

@Data
@EqualsAndHashCode(callSuper = true)
@XmlRootElement(name = RCProperty.URI_FRAGMENT)
@NoArgsConstructor
@AllArgsConstructor
public class RCProperty extends ARCCmConcept<CmParamdef> {
    public final static String URI_FRAGMENT = "Property";

    private String caption;
    private String description;

    private RCUnitOfMeasure unitOfMeasure;

//TODO:    private CmParamkind cmParamkind;
//TODO:    private CmDatatype cmDatatype;
//TODO:    private CmDatatype cmIndexDatatype;

    public RCProperty(UriInfo uriInfo, ExpandMembers expand, CmParamdef cmObject) {
        super(uriInfo, expand, cmObject);
        caption = cmObject.getCaption();
        if (isExpand()) {
            description = cmObject.getDescription();
        }
    }

    public String getUriFragment() {
        return URI_FRAGMENT;
    }

    /**
     *
     * @param cmObject
     * @return eg. weathersa-1.meantemp
     */
    protected String deriveSlug(CmParamdef cmObject) {
        return deriveSlugFromCmParamdef(cmObject);
    }

    public static String deriveSlugFromCmParamdef(CmParamdef cmObject) {
        return deriveSlugFromCmParamdef(cmObject.getCmTypedef(),
            cmObject.getKey());
    }

    public static String deriveSlugFromCmParamdef(CmTypedef cmTypedef,
        String key) {
        return RCClass.deriveSlugFromCmTypedef(cmTypedef) + "."
            + key.toLowerCase();
    }

    public static String typeDefKeyFromSlug(String slug) {
        return keyFromSlug(parentSlug(slug)).toUpperCase();
    }

    public static String fieldKeyFromSlug(String slug) {
        return childSlug(slug).toUpperCase();
    }

}
