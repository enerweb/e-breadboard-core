package za.co.enerweb.ebr.adf.data_types;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FileUtils;

import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.adf.AdfFactory;
import za.co.enerweb.ebr.adf.entities.CmParamdef;
import za.co.enerweb.ebr.adf.entities.IbInstance;
import za.co.enerweb.ebr.adf.entities.IbInstanceParameter;
import za.co.enerweb.ebr.datatype.dataresource.ResourceUrl;
import za.co.enerweb.ebr.server.HomeFile;
import za.co.enerweb.ebr.server.HomeFileCategory;
import za.co.enerweb.ebr.server.HomeFileSpec;
import za.co.enerweb.toolbox.io.CopyOrMove;

@Slf4j
@Data
@EqualsAndHashCode(callSuper = true)
public class AdfResourceUrl extends ResourceUrl {
    private static final long serialVersionUID = 1L;

    public static final String TYPE_ALIAS = "resource-url";
    public static final String ADF_RESOURCE_URL_PREFIX = "adf://";
    private static final String RP_INSTANCE_RESOURCES = "instance-resources";

    private CmParamdef cmParamdef;

    private IbInstance instance;

    public AdfResourceUrl(IbInstance instance, CmParamdef cmParamdef,
        String url) {
        super(url);
        this.instance = instance;
        this.cmParamdef = cmParamdef;
    }

    public HomeFileAndRelPath getHomeFileAndRelPath(AdfFactory adfFactory) {
        String relPath = removePrefix(ADF_RESOURCE_URL_PREFIX);
        if (relPath != null) {
            return new HomeFileAndRelPath(EbrFactory.getHomeService().getFile(
                AdfResourceUrl.getResourceDir(adfFactory,
                    getInstance(),
                    getCmParamdef(), relPath)), relPath);
        }
        return super.getHomeFileAndRelPath();
    }

    /**
     * @return {resources}/{dbpackage}/managed-resources/
     *         {typedefkey}/{typedefid}/{paramkey}/{relpath}
     */
    public static HomeFileSpec getResourceDir(AdfFactory adfFactory,
        IbInstance instance, CmParamdef cmParamdef, String relpath) {
        return new HomeFileSpec(HomeFileCategory.RESOURCES,
            adfFactory.getVarPackageName(), RP_INSTANCE_RESOURCES,
            instance.getCmTypedef().getKey(),
            instance.getId().toString(),
            cmParamdef.getKey(),
            relpath);
    }

    /**
     * Saves the file into the correct AdfResource location
     */
    public static AdfResourceUrl saveFromLocalFile(AdfFactory adfFactory,
        IbInstance instance, CmParamdef docParamdef,
        File sourceFile, CopyOrMove copyOrMove
        ) throws IOException {

        HomeFile homeFile = EbrFactory.getHomeService().getFile(
            AdfResourceUrl.getResourceDir(adfFactory,
                instance,
                docParamdef, sourceFile.getName()));

        // move it into its final destination
        homeFile.makeParentDirs();
        if (copyOrMove.isMove()) {
            FileUtils.moveFile(sourceFile, homeFile.getFile());
        } else {
            FileUtils.copyFile(sourceFile, homeFile.getFile());
        }

        String url = ADF_RESOURCE_URL_PREFIX + homeFile.getName();

        return new AdfResourceUrl(instance, docParamdef, url);
    }

    public String toString() {
        return super.toString();
    }

    /**
     * Deletes the file if it is local.
     * @param adfFactory
     */
    public void delete(AdfFactory adfFactory) {
        HomeFileAndRelPath hnr = getHomeFileAndRelPath(adfFactory);
        if (hnr != null) {
            hnr.getHomeFile().getFile().delete();
        }
    }

    public static void deleteUnreferencedFiles(AdfFactory adfFactory,
        IbInstance instance, CmParamdef cmParamdef) {
        HomeFileSpec resourceDirSpec = getResourceDir(adfFactory, instance,
            cmParamdef, "");
        List<IbInstanceParameter> paramValues = adfFactory.getAdfDbLayer()
            .getParamValues(instance, cmParamdef);
        Set<String> urls = new HashSet<String>();
        for (IbInstanceParameter pv : paramValues) {
            AdfResourceUrl paramValue =
                (AdfResourceUrl) adfFactory.getAdfDbLayer()
                    .getParamValue(pv, cmParamdef);
            if (paramValue != null) {
                urls.add(paramValue.getUrl());
            }
        }
        for (HomeFile homeFile : EbrFactory.getHomeService().findFiles(
            resourceDirSpec)) {
            if (!urls.contains(ADF_RESOURCE_URL_PREFIX + homeFile.getName())) {
                log.debug("deleteing: " + homeFile);
                homeFile.getFile().delete();
            }
        }
    }
}
