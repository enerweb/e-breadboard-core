package za.co.enerweb.ebr.adf.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import za.co.enerweb.ebr.adf.entities.CmTypedef;
import za.co.enerweb.toolbox.vaadin.messaging.Message;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class MSelectAdfType extends Message {
    private CmTypedef cmTypedef;
}
