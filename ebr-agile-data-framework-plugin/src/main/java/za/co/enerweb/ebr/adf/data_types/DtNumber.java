package za.co.enerweb.ebr.adf.data_types;

import za.co.enerweb.ebr.adf.model.AdfScalarDataType;

public class DtNumber extends AdfScalarDataType<Double> {

    public String getDescription() {
        return "Number";
    }

    public Class<Double> getJavaType() {
        return Double.class;
    }

    public String toPersistedValue(Double value) {
        return ((Double) value).toString();
    }

    public Double fromPersistedValue(String persistedValue) {
        return new Double(persistedValue);
    }

}
