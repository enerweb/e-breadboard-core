package za.co.enerweb.ebr.adf.entities;

import static org.apache.commons.lang.StringUtils.capitalize;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import za.co.enerweb.ebr.adf.AdfFactory;
import za.co.enerweb.ebr.adf.dbl.AdfDbLayerUtils;
import za.co.enerweb.toolbox.reflection.ClassUtils;

@Data
@EqualsAndHashCode(of="id")
@ToString(of={"id", "entity", "key", "caption"})
@Entity
@Table(name = "CM_RELTYPEDEF", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"ENTITY", "KEY"})})
public class CmReltypedef implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;

    @JoinColumn(name = "ENTITY", referencedColumnName = "ENTITY",
        nullable = false)
    @ManyToOne(optional = false)
    private CmEntity cmEntity;

    @Basic(optional = false)
    @Column(name = "ENTITY", insertable = false, updatable = false)
    private String entity;

    @Basic(optional = false)
    @Column(name = "KEY", nullable = false, length = 25)
    private String key;

    @Basic(optional = false)
    @Column(name = "CAPTION", nullable = false, length = 100)
    private String caption;

    @Column(name = "DESCRIPTION", length = 400)
    private String description;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "cmReltypedef")
    private Collection<CmRelparamdef> cmRelparamdefCollection;

    @JoinColumn(name = "TO_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private CmTypedef toCmTypedef;

    @JoinColumn(name = "FROM_ID", referencedColumnName = "ID")
    @ManyToOne
    private CmTypedef fromCmTypedef;

    public CmReltypedef() {
    }

    public CmReltypedef(Integer id) {
        this.id = id;
    }

    public String getJpaRelationshipEntity() {
        return AdfDbLayerUtils.efsEntityToJpaRelationshipEntity(
            entity);
    }

    public String getRelationshipInstanceFromProperty(AdfFactory adfFactory) {
        return getRelationshipInstanceProperty(adfFactory, "from",
            getFromCmTypedef());
    }

    public String getRelationshipInstanceToProperty(AdfFactory adfFactory) {
        return getRelationshipInstanceProperty(adfFactory, "to" ,
            getToCmTypedef());
    }

    private String getRelationshipInstanceProperty(AdfFactory adfFactory,
        String direction, CmTypedef relatedTypedef) {
        Class<? extends Relationship> relationshipClass
            = adfFactory.getJpaEntityClass(getJpaRelationshipEntity());

        String ret = relatedTypedef.getRelationshipInstanceProperty();
        if (ClassUtils.hasGetter(relationshipClass, ret)) {
            return ret;
        }
        String ret1 = direction + capitalize(
            relatedTypedef.getRelationshipInstanceProperty());
        if (ClassUtils.hasGetter(relationshipClass, ret1)) {
            return ret1;
        }

        throw new RuntimeException("Could not determine " + direction
            + " property for " + relationshipClass + ".\nExpeced "
            + ret + " or " + ret1 + ".");
    }

    public CmReltypedef(CmEntity cmEntity, String key, String caption,
        CmTypedef fromCmTypedef, CmTypedef toCmTypedef) {
        super();
        setCmEntity(cmEntity);
        this.key = key;
        this.caption = caption;
        this.toCmTypedef = toCmTypedef;
        this.fromCmTypedef = fromCmTypedef;
    }

    public CmReltypedef(CmEntity cmEntity, String key, String caption,
        String description, CmTypedef fromCmTypedef, CmTypedef toCmTypedef) {
        super();
        setCmEntity(cmEntity);
        this.key = key;
        this.caption = caption;
        this.description = description;
        this.toCmTypedef = toCmTypedef;
        this.fromCmTypedef = fromCmTypedef;
    }

    public void setCmEntity(CmEntity cmEntity) {
        this.cmEntity = cmEntity;
        entity = cmEntity.getEntity();
    }
}
