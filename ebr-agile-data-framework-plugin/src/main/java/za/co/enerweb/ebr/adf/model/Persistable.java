package za.co.enerweb.ebr.adf.model;

public interface Persistable {
    public String toPersistedValue();
    public void fromPersistedValue(String persistedValue);
}
