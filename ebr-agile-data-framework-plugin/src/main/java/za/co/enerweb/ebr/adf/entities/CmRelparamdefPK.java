package za.co.enerweb.ebr.adf.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(of="key")
@ToString(of="key")
@Embeddable
public class CmRelparamdefPK implements Serializable {
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "RELTYPEDEF_ID", nullable = false)
    private int reltypedefId;
    @Basic(optional = false)
    @Column(name = "KEY", nullable = false, length = 25)
    private String key;

    public CmRelparamdefPK() {
    }

    public CmRelparamdefPK(int reltypedefId, String key) {
        this.reltypedefId = reltypedefId;
        this.key = key;
    }

}
