
package za.co.enerweb.ebr.adf.dbl;

import static java.lang.String.format;
import static za.co.enerweb.ebr.adf.dbl.AdfDbLayer.toCaption;
import static za.co.enerweb.ebr.adf.dbl.AdfDbLayerUtils.setProperty;
import static za.co.enerweb.ebr.adf.entities.HHierarchyRelationship.ENTITY_HIERARCHY_RELATIONSHIP;
import static za.co.enerweb.ebr.adf.model.EntityType.HIERARCHY;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import lombok.Getter;
import lombok.SneakyThrows;
import lombok.val;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.math.stat.descriptive.moment.Mean;
import org.apache.commons.math.stat.descriptive.rank.Percentile;
import org.apache.commons.math.util.ResizableDoubleArray;
import org.joda.time.DateTime;

import za.co.enerweb.ebr.adf.AdfFactory;
import za.co.enerweb.ebr.adf.data_types.DtNumberArray;
import za.co.enerweb.ebr.adf.entities.CmDatatype;
import za.co.enerweb.ebr.adf.entities.CmEntity;
import za.co.enerweb.ebr.adf.entities.CmEntitytype;
import za.co.enerweb.ebr.adf.entities.CmHierarchyDef;
import za.co.enerweb.ebr.adf.entities.CmParamdef;
import za.co.enerweb.ebr.adf.entities.CmParamdefPK;
import za.co.enerweb.ebr.adf.entities.CmParamkind;
import za.co.enerweb.ebr.adf.entities.CmReltypedef;
import za.co.enerweb.ebr.adf.entities.CmSimulation;
import za.co.enerweb.ebr.adf.entities.CmTypedef;
import za.co.enerweb.ebr.adf.entities.CmUnitofmeasure;
import za.co.enerweb.ebr.adf.entities.HierarchyRelationship;
import za.co.enerweb.ebr.adf.entities.IbInstance;
import za.co.enerweb.ebr.adf.entities.IbInstanceParameter;
import za.co.enerweb.ebr.adf.entities.IbInstanceParameterNorm;
import za.co.enerweb.ebr.adf.entities.Relationship;
import za.co.enerweb.ebr.adf.model.AdfDataType;
import za.co.enerweb.ebr.adf.model.AdfDataTypeClass;
import za.co.enerweb.ebr.adf.model.AdfEntity;
import za.co.enerweb.ebr.adf.model.AdfInstance;
import za.co.enerweb.ebr.adf.model.AdfInstanceEntity;
import za.co.enerweb.ebr.adf.model.AdfRelationshipTypeDefMetadata;
import za.co.enerweb.ebr.adf.model.AdfTypeDefMetadata;
import za.co.enerweb.ebr.adf.model.EntityType;
import za.co.enerweb.ebr.adf.model.InstanceRelationship;
import za.co.enerweb.ebr.adf.model.ParameterKind;
import za.co.enerweb.ebr.adf.model.timeseries.ITimeSeries;
import za.co.enerweb.ebr.adf.model.timeseries.NumArraySummary;
import za.co.enerweb.ebr.adf.model.timeseries.SparseTimeSeries;
import za.co.enerweb.ebr.edbm.dbl.EdbmDbLayer;
import za.co.enerweb.toolbox.reflection.InvalidPropertyException;
import za.co.enerweb.toolbox.reflection.PropertyUtils;
import za.co.enerweb.toolbox.string.StringUtils;

import com.google.common.cache.Cache;

/**
 * Data Access Object: separates persistence from the application logic find
 * methods may return null - you should check for it get methods should throw a
 * proper exception if it can't be found..
 * This should have all the lowlevel CM/jpr methods somewhere else, out of the
 * way.
 * so we can have a much cleaner Object based Adf api here..
 */
@Stateful
@LocalBean
@Slf4j
public class AdfLowlevelDbLayer extends EdbmDbLayer {

    public static final String CM_ = "CM_";
    public static final String I_ = "I_";
    public static final String R_ = "R_";
    public static final String H_ = "H_";

    public static final Integer ADF_VERSION = 1;

    /**
     * Default start date for everpresent stuff
     */
    public static Date EPOCH = new DateTime(1970, 1, 1, 0, 0, 0, 0).toDate();
    public static Date APOCALYPSE = new DateTime(3012, 12, 21, 0, 0, 0, 0)
        .toDate(); // didn't wanna be as pessimistic as the Mayans
    // so i added 1000 years

    /**
     * the adfFactory is supposed to set this..
     */
    protected AdfFactory adfFactory;
    @Getter
    protected transient AdfRegistry adfRegistry;
    @Getter
    protected Cache<IbInstance, AdfInstance> adfInstanceCache;


    // vvv internal
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void setAdfFactory(AdfFactory adfFactory) {
        this.adfFactory = adfFactory;
        this.adfRegistry =
            AdfRegistry.getRegistry(adfFactory.getClass());
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void setAdfInstanceCache(Cache<IbInstance, AdfInstance>
        adfInstanceCache) {
        this.adfInstanceCache = adfInstanceCache;
    }

    public void invalidateCachedAdfInstances(IbInstance... ibInstances) {
        for (IbInstance instance : ibInstances) {
            if (instance != null) {
                // log.debug("Invalidating cached instance: " + instance);
                // if (log.isDebugEnabled()) {
                // log.debug("Before eviction: "
                // + adfInstanceCache.getIfPresent(instance));
                // }
                adfInstanceCache.invalidate(instance);
                // if (log.isDebugEnabled()) {
                // log.debug("After eviction: "
                // + adfInstanceCache.getIfPresent(instance));
                // }
            }
        }
    }

    public AdfFactory getAdfFactory() {
        if (adfFactory == null) {
            throw new NullPointerException(
                "Implementations of AdfFactory.getDbLayer is supposed to "
                    +
                    "setAdfFactory before returning the DbLayer."
                    +
                    "You should probably just call super.getDbLayer(moduleName,"
                    +
                    "dblayerClass)");
        }
        return adfFactory;
    }

    // vvv adf-generic

    @SuppressWarnings("unchecked")
    public <I> I instantiateJpaEntity(String jpaEntityName) {
        return (I) adfFactory.instantiateJpaEntity(jpaEntityName);
    }

    @SuppressWarnings("unchecked")
    public <T> T getParamValue(
        final IbInstance instance, final String key) {
        return (T)getParamValue(instance, key, null);
    }

    public <T> T getParamValue(
        final IbInstance instance, final String key, T defaultValue) {
        return (T)getParamValue(instance, key, null, defaultValue);
    }

    @SuppressWarnings("unchecked")
    public <T> T getParamValue(
        final IbInstance instance, final String key, Date effectiveDate,
        T defaultValue) {
        IbInstanceParameter param = getSingleParamValue(instance, key,
            effectiveDate);
        if (param != null) {
            return (T) getParamValue(param, key);
        }
        return defaultValue;
    }

    /**
     * Probably needs to be called from within the dbl
     */
    @SuppressWarnings("unchecked")
    public <T> T getParamValue(IbInstanceParameter ibInstanceParameter,
        final String key) {
        CmParamdef cmParamdef =
            ibInstanceParameter.getCmTypedef().findParamDef(key);
        return (T) getParamValue(ibInstanceParameter, cmParamdef);
    }

    @SuppressWarnings("unchecked")
    public <T> T getParamValue(IbInstanceParameter ibInstanceParameter,
        CmParamdef cmParamdef) {
        String property = cmParamdef.getValueProperty();
        try {
            Object value =
                PropertyUtils.getProperty(ibInstanceParameter, property);
            if (value instanceof String) {
                return (T) getAdfDataType(ibInstanceParameter, cmParamdef)
                    .fromPersistedValue((String) value);
                // return (T) cmParamdef.getDataType()
                // .fromPersistedValue(
                // ibInstanceParameter.getInstance(cmParamdef),
                // cmParamdef,
                // (String) value);
            } else {
                // For denormalized values numbers are stored as Double.
                // XXX: if this cast fails, maybe we should just toString it
                // and then fromPersistedValue
                return (T) value;
            }
        } catch (InvalidPropertyException e) {
            throw new IllegalArgumentException(format(
                "Could not obtain the value " +
                    "for unknown parameter key=%s using %s.%s ",
                cmParamdef.getKey(),
                getClass().getSimpleName(), property), e);
        }
    }

    protected <T> AdfDataType<T> getAdfDataType(
        IbInstanceParameter ibInstanceParameter, CmParamdef cmParamdef) {
        return getAdfDataType(ibInstanceParameter.getInstance(cmParamdef),
            cmParamdef);
    }

    protected <T> AdfDataType<T> getAdfDataType(
        IbInstance instance, CmParamdef cmParamdef) {
        @SuppressWarnings("unchecked") AdfDataType<T> adfDataType =
            (AdfDataType<T>) adfRegistry.getAdfDataType(
                cmParamdef.getCmDatatype().getKey());
        adfDataType.setCmParamdef(cmParamdef);
        adfDataType.setInstance(instance);
        adfDataType.setDbLayer((AdfDbLayer) this); // cheeky I know
        return adfDataType;
    }

    public IbInstanceParameter getSingleParamValue(
        final IbInstance instance, final String key) {
        return getSingleParamValue(instance, key, null);
    }

    public IbInstanceParameter getSingleParamValue(
        final IbInstance instance, final String key, Date effectiveDate) {
        CmTypedef type = instance.getCmTypedef();
        CmParamdef cmParamdef = this.findParameterDef(type, key);
        if (cmParamdef == null) {
            // if we can return null, I suppose that we don't have to throw this
            throw new IllegalArgumentException("Could not find parameter '"
                + key + "' for type: " + type.getKey());
        }
        String entity = cmParamdef.getParamJpaEntity();
        String qry = "SELECT c FROM " + entity
            + " c WHERE " + "c." + type.getParamInstanceProperty()
            + "=:instance and c.key=:key";
        if (effectiveDate != null) {
            qry += " and appldatetime <= :appldatetime";
        }
        qry += " ORDER BY appldatetime desc";
        Query q = em.createQuery(qry);
        q.setParameter("instance", instance);
        q.setParameter("key", key);
        if (effectiveDate != null) {
            q.setParameter("appldatetime", effectiveDate);
        }
        q.setMaxResults(1);
        try {
            @SuppressWarnings("unchecked") List<IbInstanceParameterNorm> resultList =
                q.getResultList();
            if (resultList.isEmpty()) {
                return null;
            }
            return (IbInstanceParameterNorm) resultList.get(0);
        } catch (NoResultException e) {
            // log.debug("No instance parameter found for "
            // + entity + " with key " + key
            // + " and instance id " + instance.getId()
            // + ". I'm returning null!");
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<IbInstanceParameter> getSingleArrayParamValue(
        final IbInstance instance, final String key, Date effectiveDate) {

        // find the exact latest effective date
        IbInstanceParameter dateParam = getSingleParamValue(
            instance, key, effectiveDate);
        if (dateParam == null) {
            return Collections.emptyList();
        }
        effectiveDate = dateParam.getAppldatetime();

        CmTypedef type = instance.getCmTypedef();
        CmParamdef cmParamdef = this.findParameterDef(type, key);
        String entity = cmParamdef.getParamJpaEntity();
        String qry = "SELECT c FROM " + entity
            + " c WHERE " + "c." + type.getParamInstanceProperty()
            + "=:instance and c.key=:key";
            qry += " and appldatetime = :appldatetime";
        qry += " ORDER BY convert(c.indx, integer)";
        Query q = em.createQuery(qry);
        q.setParameter("instance", instance);
        q.setParameter("key", key);
            q.setParameter("appldatetime", effectiveDate);
        return q.getResultList();
    }

    /**
     * Sets the value in the specified jpa entity
     */
    public <T> void setParamValue(String key,
        IbInstanceParameter param,
        T value) {
        param = em.merge(param);
        if (value == null) {
            // we don't store null values at present..
            em.remove(param);
        } else {
            setParamValue(param, param.getCmTypedef().findParamDef(key), value);
            save(param);
        }
    }

    /**
     * @deprecated in 0.11.0 will be removed in 0.12.0
     */
    @Deprecated
    public <T> void setSingleParamValue(
        IbInstance instance, final String key, Date appldatetime,
        T value) {
        setSingleParamValue(instance, key, value, appldatetime, null);
    }

    /**
     * @deprecated in 0.11.0 will be removed in 0.12.0
     */
    @Deprecated
    public <T> void setSingleParamValue(
        IbInstance instance, final String key, Date appldatetime,
        T value, String indx) {
        setSingleParamValue(instance, key, value, appldatetime, indx);
    }

    /**
     * Will replace any previous single param value
     */
    public <T> void setSingleParamValue(
        IbInstance instance, final String key, T value,
        Date appldatetime, String indx) {
        deleteParamValues(instance, key);
        if (value == null) {
            // we don't store null values at present..
            return;
        }
        addParamValue(instance, key, value, appldatetime, indx);
    }

    /**
     * @deprecated in 0.11.0 will be removed in 0.12.0
     */
    @Deprecated
    public <T> void addParamValue(
        IbInstance instance, final String key, Date appldatetime,
        T value) {
        addParamValue(instance, key, value, appldatetime, null);
    }

    /**
     * @deprecated in 0.11.0 will be removed in 0.12.0
     */
    @Deprecated
    public <T> void addParamValue(IbInstance instance, final String key,
        Date appldatetime, T value, String indx) {
        addParamValue(instance, key, value, appldatetime, indx);
    }

    /**
     * Add this param value without replacing existing values
     */
    public <T> void addParamValue(IbInstance instance, final String key,
        T value, Date appldatetime, String index) {
        addParamValue(instance, key, value,
            appldatetime, index, false);
    }

    /**
     * Save parameter value, replacing a previous one with the same
     * date and index.
     */
    public <T> void saveParamValue(IbInstance instance, final String key,
        T value, Date appldatetime, String index) {
        addParamValue(instance, key, value,
            appldatetime, index, true);
    }

    private <T> void addParamValue(IbInstance instance, final String key,
        T value, Date appldatetime, String index, boolean merge) {
        if (value == null) {
            // we don't store null values at present..
            return;
        }
        if (appldatetime == null) {
            appldatetime = EPOCH;
        }
        // create a new instance
        instance = em.merge(instance);
        CmTypedef cmTypedef = instance.getCmTypedef();
        CmParamdef cmParamdef = cmTypedef.findParamDef(key);
        // XXX: make this check optional..
        IbInstanceParameter paramVal = null;
        if (merge) {
            paramVal = getParamValue(instance, cmParamdef,
                appldatetime, index);
        }
        if (paramVal == null) {
            paramVal = getAdfFactory().instantiateJpaEntity(
            cmParamdef.getParamJpaEntity());
        }
        setProperty(paramVal,
            cmParamdef.getInstanceProperty(), instance);
        paramVal.setCmTypedef(cmTypedef);
        paramVal.setAppldatetime(appldatetime);
        paramVal.setIndx(index);
        setParamValue(paramVal, cmParamdef, value);

        if (paramVal instanceof IbInstanceParameterNorm) {
            IbInstanceParameterNorm normValue =
                (IbInstanceParameterNorm) paramVal;
            normValue.setCmParamdef(cmParamdef);
        }
        invalidateCachedAdfInstances(instance);
        save(paramVal);
    }

    // maybe add a batch operation that can do many in one go with less
    // repeating work..
    public <T> void setParamValue(IbInstanceParameter instanceParameter,
        CmParamdef cmParamdef, T value) {
        String property = cmParamdef.getValueProperty();
        try {
            if (cmParamdef.getParameterKind().isDenorm()) {
                // currently we only support numeric denorm values
                PropertyUtils.setProperty(instanceParameter, property, value);
            } else {
                if (value == null) {
                    PropertyUtils
                        .setProperty(instanceParameter, property, null);
                } else {
                    // if it is already a string, assume it is
                    // already in persisted value. If this isn't always
                    // accurate, we may want to de-convert and reconvert it?!
                    String persistedValue;
                    if (value instanceof String) {
                        persistedValue = (String) value;
                    } else {
                        persistedValue =
                            getAdfDataType(instanceParameter, cmParamdef)
                        .toPersistedValue((T) value);
                    }
                    PropertyUtils.setProperty(instanceParameter, property,
                        persistedValue
                        // adfDataType.getJavaType().cast(value))
                        // cmParamdef.getDataType().toPersistedValue(
                        // instanceParameter.getInstance(cmParamdef),
                        // cmParamdef,
                        // value)

                        );
                }
            }
        } catch (ClassCastException e) {
            throw new IllegalArgumentException(format(
                "Could not set the value " +
                    "for parameter key=%s using %s.%s = %s",
                cmParamdef.getKey(),
                instanceParameter.getClass().getSimpleName(), property,
                "" + value), e);
        } catch (InvalidPropertyException e) {
            throw new IllegalArgumentException(format(
                "Could not set the value " +
                    "for unknown parameter key=%s using %s.%s ",
                cmParamdef.getKey(),
                instanceParameter.getClass().getSimpleName(), property), e);
        }
    }


    public int deleteParamValues(
        final IbInstance instance, String... cmParamdefKeys) {
        return deleteParamValues(instance, Arrays.asList(cmParamdefKeys));
    }

    public int deleteParamValues(
        final IbInstance instance, List<String> cmParamdefKeys) {
        List<CmParamdef> params =
            new ArrayList<CmParamdef>(cmParamdefKeys.size());
        for (String cmParamdefKey : cmParamdefKeys) {
            params.add(getCmParamdef(
                instance.getCmTypedef(), cmParamdefKey));
        }
        return deleteParamValues(instance,
            params);
    }

    public int getParamValueCount(
        final IbInstance instance, final CmParamdef cmParamdef) {
        CmTypedef type = instance.getCmTypedef();
        // may need to count distinct x.idx ?! for number arrays?!
        return ((Long) em.createQuery(
            "SELECT count(x) FROM " + cmParamdef.getParamJpaEntity() + " x "
                + "WHERE x." + type.getParamInstanceProperty()
                + ".id = :instance "
                + "AND x.cmParamdef = :cmParamdef "
            ).setParameter("instance", instance.getId())
            .setParameter("cmParamdef", cmParamdef)
            .getSingleResult()).intValue();
    }

    @SuppressWarnings("unchecked")
    public List<IbInstanceParameter> getParamValues(
        final IbInstance instance, final CmParamdef cmParamdef,
        final int startIndex, final int count, final String orderBy) {
        CmTypedef type = instance.getCmTypedef();
        Query query = em.createQuery(
            "SELECT x FROM " + cmParamdef.getParamJpaEntity() + " x "
                + "WHERE x." + type.getParamInstanceProperty()
                + ".id = :instance "
                + "AND x.cmParamdef.cmParamdefPK.key = :cmParamdef "
                + "ORDER BY " + orderBy
            );
        query.setParameter("instance", instance.getId());
        query.setParameter("cmParamdef", cmParamdef.getCmParamdefPK().getKey());
        query.setFirstResult(startIndex);
        query.setMaxResults(count);
        List<?> resultList = query.getResultList();
        validateIbInstanceParameter(resultList);
        return (List<IbInstanceParameter>) resultList;
    }

    public IbInstanceParameter createParamValue(IbInstance instance,
        CmParamdef cmParamdef) {
        IbInstanceParameter ret;
        String entityClass = cmParamdef.getParamJpaEntity();
        try {
            ret = getAdfFactory().instantiateJpaEntity(entityClass);
            // set instance
            String fn = cmParamdef.getInstanceProperty();
            AdfDbLayerUtils.setProperty(ret, fn, instance);
        } catch (Exception e) {
            throw new IllegalStateException(
                "Could not create new instance of "
                    + entityClass, e);
        }
        ret.setCmTypedef(cmParamdef.getCmTypedef());
        if (ret instanceof IbInstanceParameterNorm) {
            IbInstanceParameterNorm normParam =
                (IbInstanceParameterNorm) ret;
            normParam.setCmParamdef(cmParamdef);
        }
        return ret;
    }

    public List<IbInstanceParameter> getParamValues(
        final IbInstance instance, String cmParamdefKey) {
        return getParamValues(instance,
            getCmParamdef(instance.getCmTypedef(), cmParamdefKey));
    }

    public IbInstanceParameter getParamValue(
        final IbInstance instance, final CmParamdef cmParamdef,
        Date appldatetime, String index) {
        CmTypedef type = instance.getCmTypedef();
        String qry = "SELECT x FROM " + cmParamdef.getParamJpaEntity() + " x "
            + "WHERE x." + type.getParamInstanceProperty()
            + ".id = :instance "
            + "AND x.cmParamdef = :cmParamdef "
            + "AND x.appldatetime = :appldatetime ";
        if (index != null) {
            qry += " AND x.indx = :index ";
        }
        TypedQuery<IbInstanceParameter> query = em.createQuery(
            qry,
            IbInstanceParameter.class
            // I think the following sorts numbers as string!
            // + ", x.indx " // does this work for all index types?
            ).setParameter("instance", instance.getId())
            .setParameter("cmParamdef", cmParamdef)
            .setParameter("appldatetime", appldatetime)
            .setMaxResults(2);
        if (index != null) {
            query.setParameter("index", index);
        }
        List<IbInstanceParameter> resultList = query
            .getResultList();
        if (resultList.size() > 0) {
            if (resultList.size() > 1) {
                log.warn("Duplicate parameters found: "
                    + instance + " "
                    + cmParamdef + " "
                    + appldatetime + " "
                    + index);
            }
            return resultList.get(0);
        }
        return null;
    }

    public List<IbInstanceParameter> getParamValues(
        final IbInstance instance, final CmParamdef cmParamdef) {
        CmTypedef type = instance.getCmTypedef();
        List<?> resultList = em.createQuery(
            "SELECT x FROM " + cmParamdef.getParamJpaEntity() + " x "
                + "WHERE x." + type.getParamInstanceProperty()
                + ".id = :instance "
                + "AND x.cmParamdef = :cmParamdef "
                + "ORDER BY x.appldatetime "
            // I think the following sorts numbers as string!
            // + ", x.indx " // does this work for all index types?
            ).setParameter("instance", instance.getId())
            .setParameter("cmParamdef", cmParamdef)
            .getResultList();
        validateIbInstanceParameter(resultList);
        return (List<IbInstanceParameter>) resultList;
    }

    public <T> ITimeSeries<T> getScalarTimeSeries(
        final IbInstance instance, final CmParamdef cmParamdef)
        throws AdfDbLayerException {
        return getScalarTimeSeries(instance, cmParamdef, null, null);
    }

    public <T> ITimeSeries<T> getScalarTimeSeries(
        final IbInstance instance, final CmParamdef cmParamdef,
        Date inclusiveStartDate, Date exclusiveEndDate)
        throws AdfDbLayerException {
        try {
            assertParameterAdfDataTypeClass(cmParamdef,
                AdfDataTypeClass.SCALAR);

            CmTypedef type = instance.getCmTypedef();

            return getScalarTimeSeries(instance, cmParamdef,
                cmParamdef.getParamJpaEntity(),
                type.getParamInstanceProperty(),
                inclusiveStartDate, exclusiveEndDate);
        } catch (IllegalStateException e) {
            throw new AdfDbLayerException(e);
        } catch (IllegalArgumentException e) {
            throw new AdfDbLayerException(e);
        }
    }

    /*
     * actually read the data from the table
     */
    @SuppressWarnings("unchecked")
    private <T> SparseTimeSeries<T> getScalarTimeSeries(
        final IbInstance parent, final CmParamdef cmParamdef,
        final String entity, final String parentIdProperty,
        Date inclusiveStartDate, Date exclusiveEndDate)
        throws IllegalStateException, IllegalArgumentException {
        String startDateFilter = "";
        if (inclusiveStartDate != null) {
            startDateFilter = " AND x.appldatetime >= :inclusiveStartDate";
        }
        String endDateFilter = "";
        if (exclusiveEndDate != null) {
            endDateFilter = " AND x.appldatetime < :exclusiveEndDate";
        }

        Query q = em.createQuery(
            "SELECT x.appldatetime, x.val FROM "
                + entity + " x "
                + "WHERE x." + parentIdProperty
                + " = :parent "
                + "AND x.cmParamdef = :cmParamdef "
                + startDateFilter
                + endDateFilter
                + " ORDER BY x.appldatetime "
            );
        q.setParameter("parent", parent);
        q.setParameter("cmParamdef", cmParamdef);
        if (inclusiveStartDate != null) {
            q.setParameter("inclusiveStartDate", inclusiveStartDate);
        }
        if (exclusiveEndDate != null) {
            q.setParameter("exclusiveEndDate", exclusiveEndDate);
        }

        List<Object[]> resultList = q.getResultList();
        SparseTimeSeries<T> ret = new SparseTimeSeries<T>();
        AdfDataType<T> adfDataType = getAdfDataType(parent, cmParamdef);
        for (Object[] row : resultList) {
            ret.put((Date) row[0],
                adfDataType.fromPersistedValue((String) row[1]));
        }
        return ret;
    }

    public <T> ITimeSeries<List<T>> getArrayTimeSeries(
        final IbInstance instance, String paramdefKey)
        throws AdfDbLayerException {

        return getArrayTimeSeries(instance, findParameterDef(
            instance.getCmTypedef(), paramdefKey));
    }

    public <T> ITimeSeries<List<T>> getArrayTimeSeries(
        final IbInstance instance, final CmParamdef cmParamdef)
        throws AdfDbLayerException {
        try {
            assertParameterAdfDataTypeClass(cmParamdef, AdfDataTypeClass.ARRAY);

            CmTypedef type = instance.getCmTypedef();

            return getArrayTimeSeries(instance, cmParamdef,
                cmParamdef.getParamJpaEntity(),
                type.getParamInstanceProperty());
        } catch (IllegalStateException e) {
            throw new AdfDbLayerException(e);
        } catch (IllegalArgumentException e) {
            throw new AdfDbLayerException(e);
        }
    }

    /*
     * actually read the data from the table
     */
    @SuppressWarnings("unchecked")
    private <T> SparseTimeSeries<List<T>> getArrayTimeSeries(
        final IbInstance parent, final CmParamdef cmParamdef,
        final String entity, final String parentIdProperty)
        throws IllegalStateException, IllegalArgumentException {
        List<Object[]> resultList =
            em.createQuery(
                "SELECT x.appldatetime, x.indx, x.val FROM "
                    + entity + " x "
                    + "WHERE x." + parentIdProperty
                    + " = :parent "
                    + "AND x.cmParamdef = :cmParamdef "
                    + "ORDER BY x.appldatetime, convert(x.indx, integer) "
                ).setParameter("parent", parent)
                .setParameter("cmParamdef", cmParamdef)
                .getResultList();
        SparseTimeSeries<List<T>> ret = new SparseTimeSeries<List<T>>();
        AdfDataType<T> adfDataType = getAdfDataType(parent, cmParamdef);
        for (Object[] row : resultList) {
            Date date = (Date) row[0];
            List<T> list = ret.get(date);
            if (list == null) {
                list = new ArrayList<T>();
                ret.put(date, list);
            }
            // XXX: assumes zerobased with no gaps
            list.add((T) adfDataType.fromPersistedValue((String) row[2]));
        }
        return ret;
    }

    protected void assertParameterDataType(final CmParamdef cmParamdef,
        final Class<? extends AdfDataType<?>> supported) {
        AdfDataType<?> adfDataType = adfRegistry.getAdfDataType(
            cmParamdef.getKey());
        if (!supported.equals(adfDataType.getAdfDataTypeClass())) {
            throw new IllegalArgumentException("This method only supports "
                + "parameters of type " + supported
                + " (" + adfDataType + " given).");
        }
    }

    private void assertParameterAdfDataTypeClass(final CmParamdef cmParamdef,
        final AdfDataTypeClass supported) {
        if (!supported.equals(cmParamdef.getAdfDataTypeClass())) {
            throw new IllegalArgumentException("This method only supports "
                + "parameters of with AdfDataTypeClass=" + supported
                + " (" + cmParamdef.getAdfDataTypeClass() + " given).");
        }
    }

    public ITimeSeries<NumArraySummary> getNumArraySummaryTimeSeries(
        final IbInstance instance, final CmParamdef cmParamdef) {
        assertParameterDataType(cmParamdef, DtNumberArray.class);
        boolean normalised = !cmParamdef.getParameterKind().isDenorm();
        List<Object[]> resultList =
            getAllValues(instance, cmParamdef, normalised);
        SparseTimeSeries<NumArraySummary> ret =
            new SparseTimeSeries<NumArraySummary>();
        if (resultList.isEmpty()) {
            return ret;
        }
        Date curDate = (Date) resultList.get(0)[0];
        ResizableDoubleArray rda = new ResizableDoubleArray();
        for (Object[] row : resultList) {
            Date newDate = (Date) row[0];
            if (newDate.after(curDate)) {
                // put data and get set for the next lot
                ret.put(curDate, calcSummary(rda));

                // reset
                curDate = newDate;
                rda = new ResizableDoubleArray();
            }
            if (normalised) {
                rda.addElement(Double.parseDouble((String) row[1]));
            } else {
                rda.addElement((Double) row[1]);
            }
        }
        if (rda.getNumElements() != 0) {
            ret.put(curDate, calcSummary(rda));
        }
        return ret;
    }

    private List<Object[]> getAllValues(final IbInstance instance,
        final CmParamdef cmParamdef, final boolean normalised) {
        String checkParamKey = "";
        String valueProperty = cmParamdef.getValueProperty();
        if (normalised) {
            checkParamKey = "AND x.cmParamdef = :cmParamdef ";
        }
        Query q = em.createQuery(
            "SELECT x.appldatetime, x." + valueProperty + " FROM "
                + cmParamdef.getParamJpaEntity() + " x "
                + "WHERE x." + cmParamdef.getInstanceProperty()
                + " = :instance "
                + checkParamKey
                + "ORDER BY x.appldatetime, x."
                + valueProperty // the algorithms want the values sorted
        ).setParameter("instance", instance);
        if (normalised) {
            q.setParameter("cmParamdef", cmParamdef);
        }
        @SuppressWarnings("unchecked") List<Object[]> resultList =
            q.getResultList();
        return resultList;
    }

    private NumArraySummary calcSummary(final ResizableDoubleArray rda) {
        NumArraySummary ret = new NumArraySummary();
        Mean m = new Mean();
        ret.setMean(m.evaluate(rda.getInternalValues(), rda.start(),
            rda.getNumElements()));
        Percentile p = new Percentile();
        p.setData(rda.getInternalValues(), rda.start(), rda.getNumElements());
        ret.setPercentile5(p.evaluate(5));
        ret.setPercentile95(p.evaluate(95));
        return ret;
    }


    private void validateIbInstanceParameter(final List<?> resultList) {
        if (!resultList.isEmpty() &&
            !(resultList.get(0) instanceof IbInstanceParameter)) {
            log.error(resultList.get(0).getClass()
                + " must extend " + IbInstanceParameter.class + ".");
        }
    }

    public Collection<String> findAllEntities() {
        Query q = em.createQuery(
            "SELECT distinct c.entity FROM CmTypedef c");
        @SuppressWarnings("unchecked") Collection<String> ret =
            q.getResultList();
        return ret;
    }

    public List<CmTypedef> findAllCmTypedefs() {
        return findAll(CmTypedef.class);
    }

    public CmTypedef findTypedefByKey(final String typedefKey) {
        Query q =
            em.createQuery("SELECT c FROM CmTypedef c WHERE c.key = :key");
        q.setParameter("key", typedefKey);
        return find(q);
    }

    public CmParamdef findParameterDef(final String typedefKey,
        final String paramdefKey) {
        return findParameterDef(findTypedefByKey(typedefKey), paramdefKey);
    }

    public CmParamdef findParameterDef(final CmTypedef cmTypedef,
        final String paramdefKey) {
        return find(CmParamdef.class, new CmParamdefPK(cmTypedef,
            paramdefKey));
    }


    public IbInstance findInstance(final String typedefKey,
        final int instanceId) {
        CmTypedef cmTypedef = findTypedefByKey(typedefKey);
        return findInstance(cmTypedef, instanceId);
    }

    @SuppressWarnings("unchecked")
    public <T extends IbInstance> T findInstance(
        final CmTypedef cmTypedef, final int instanceId) {
        em.merge(cmTypedef);
        String qry = "SELECT c FROM " + cmTypedef.getJpaInstanceEntity()
            + " c WHERE c.id = :id and c.cmTypedef=:typedef";
        Query q = em.createQuery(qry);
        q.setParameter("id", instanceId);
        q.setParameter("typedef", cmTypedef);
        return (T) find(q);
    }

    public <T extends IbInstance> T findInstanceByEntity(final String entity,
        final int instanceId) {
        return findInstanceByEntity(getCmEntity(entity), instanceId);
    }


    public <T> T findInstanceByEntity(final CmEntity cmEntity,
        final int instanceId) {
        // em.merge(cmEntity);
        String qry = "SELECT c FROM " + cmEntity.getJpaEntity()
            + " c WHERE c.id = :id";
        Query q = em.createQuery(qry);
        q.setParameter("id", instanceId);
        return find(q);
    }

    /**
     * @deprecated in 0.11.0, use findInstanceByEntity
     */
    @Deprecated
    public <T> T find(final CmEntity cmEntity,
        final int instanceId) {
        return findInstanceByEntity(cmEntity, instanceId);
    }

    public List<IbInstance> findInstancesByType(final String typedefKey) {
        return findInstancesByType(getCmTypedef(typedefKey));
    }

    public List<IbInstance> findInstancesByType(CmTypedef... cmTypedefs) {
        return findInstancesByType(Arrays.asList(cmTypedefs));
    }

    @SuppressWarnings("unchecked")
    public List<IbInstance> findInstancesByType(List<CmTypedef> cmTypedefs) {
        String jpaInstanceEntity = null;
        for (CmTypedef cmTypedef : cmTypedefs) {
            if (jpaInstanceEntity == null) {
                jpaInstanceEntity = cmTypedef.getJpaInstanceEntity();
            } else if (!jpaInstanceEntity.equals(
                cmTypedef.getJpaInstanceEntity())) {
                throw new IllegalArgumentException(
                    "Only supporst typedefs from " +
                        "the same entity at the moment: " +
                        jpaInstanceEntity + " != " +
                        cmTypedef.getJpaInstanceEntity());
            }
        }

        String qry = "SELECT c FROM " + jpaInstanceEntity
            + " c WHERE c.cmTypedef in :typedefs order by c.caption";
        Query q = em.createQuery(qry);
        q.setParameter("typedefs", cmTypedefs);
        return q.getResultList();
    }

    public IbInstance findInstanceByCaption(final String typedefKey,
        final String caption) {
        return findInstanceByCaption(getCmTypedef(typedefKey), caption);
    }

    public IbInstance findInstanceByCaption(final CmTypedef cmTypedef,
        final String caption) {
        String qry = "SELECT c FROM " + cmTypedef.getJpaInstanceEntity()
            + " c WHERE c.caption = :caption and c.cmTypedef=:typedef";
        Query q = em.createQuery(qry);
        q.setParameter("caption", caption);
        q.setParameter("typedef", cmTypedef);
        return find(q);
    }


    /**
     * Essentially finds all the instances stored in the same table.
     */
    @SuppressWarnings("unchecked")
    public List<IbInstance> findInstancesByEntity(final String entity) {
        String iEntity = AdfDbLayerUtils.efsEntityToJpaInstanceEntity(entity);
        Query q = em.createQuery("SELECT x FROM " + iEntity
            + " x order by caption");
        return q.getResultList();
    }

    /*
     * XXX: UNTESTED and UNUSED
     */
    public List<IbInstance> findInstancesByEntity(final String entity,
        Date effectiveDate, ParameterFilter<?>... filters) {
        String iEntity = AdfDbLayerUtils.efsEntityToJpaInstanceEntity(entity);
        String iEntityPar = AdfDbLayerUtils.efsEntityToJpaParmameterEntity(
            entity);
        if (effectiveDate == null) {
            effectiveDate = new Date();
        }
        // build query with nasty joins
        val jpaParams = new HashMap<String, Object>();
        StringBuilder filtersFromClause = new StringBuilder();
        StringBuilder filtersWhereClause = new StringBuilder();
        int fi = 0;
        for (ParameterFilter<?> filter : filters) {
            String parmTableAlias = "pt" + fi;
            filtersFromClause.append(", " + iEntityPar
                + " " + parmTableAlias);
            if (fi > 0) {
                filtersWhereClause.append(" AND ");
            }
            filtersWhereClause.append(
                parmTableAlias + ".id = x.id " +
                    " AND " + parmTableAlias
                    + ".cmTypedef = x.cmTypedef" +
                    " AND " + parmTableAlias
                    + ".key = :cmParamdef_key_" + parmTableAlias +
                    " AND " + parmTableAlias
                    + ".val = :cmParam_val_" + parmTableAlias +
                    " AND :effectiveDate >= "
                    + parmTableAlias + ".appldatetime "
                // FIXME: a bit difficult to test the end values
                // should I do a count of newer values?
                );
            jpaParams.put("cmParamdef_key_" + parmTableAlias,
                filter.parameterKey());

            jpaParams.put("effectiveDate",
                effectiveDate);

            // only support simple types..
            @SuppressWarnings("rawtypes") AdfDataType dataType =
                filter.dataType();
            @SuppressWarnings("unchecked")
            String persistedValue = dataType.toPersistedValue(filter.value());
            jpaParams.put("cmParam_val_" + parmTableAlias,
                persistedValue);
            fi++;
        }
        if (filtersWhereClause.length() > 0) {
            filtersWhereClause.insert(0, " WHERE ");
        }
        Query q = em.createQuery("SELECT x FROM " + iEntity
            + " x" + filtersFromClause.toString()
            + filtersWhereClause.toString()
            + " order by caption");
        // q.setParameter("effectiveDate", effectiveDate);
        for (Entry<String, Object> entry : jpaParams.entrySet()) {
            q.setParameter(entry.getKey(), entry.getValue());
        }
        return q.getResultList();
    }

    /**
     * @deprecated since 0.11.0
     */
    @Deprecated
    public List<IbInstance> findInstancesInEntity(final String entity) {
        return findInstancesByEntity(entity);
    }

    /**
     * This method can select all the instances from multiple typedefs
     */
    // XXX: rename to findInstances after we deleted
    // findInstances(String entity)
    // (Renaming because we are only returning some instances...)
    public List<IbInstance> findAllInstances(final String... typedefKeys) {
        Map<String, Set<CmTypedef>> entityToTypes =
            new HashMap<String, Set<CmTypedef>>();
        for (String typedefKey : typedefKeys) {
            CmTypedef cmTypedef = getCmTypedef(typedefKey);
            String iEntity = cmTypedef.getJpaInstanceEntity();
            Set<CmTypedef> types = entityToTypes.get(iEntity);
            if (types == null) {
                types = new HashSet<CmTypedef>();
                entityToTypes.put(iEntity, types);
            }
            types.add(cmTypedef);
        }
        if (entityToTypes.size() == 1) {
            Entry<String, Set<CmTypedef>> e = entityToTypes.entrySet()
                .iterator().next();
            return findInstances(e.getKey(), e.getValue());
        }
        List<IbInstance> ret = new ArrayList<IbInstance>();
        for (Entry<String, Set<CmTypedef>> e : entityToTypes.entrySet()) {
            ret.addAll(findInstances(e.getKey(), e.getValue()));
        }
        return ret;
    }

    @SuppressWarnings("unchecked")
    private List<IbInstance> findInstances(final String iEntity,
        final Set<CmTypedef> types) {
        String qry = "SELECT c FROM " + iEntity
            + " c WHERE c.cmTypedef in (:types)"
            + " ORDER By c.caption";
        Query q = em.createQuery(qry);
        q.setParameter("types", types);
        return q.getResultList();
    }

    /**
     * @deprecated in 0.11.0 will be removed in 0.12.0
     */
    @Deprecated
    public List<IbInstance> findRelatedInstances(
        final IbInstance fromInstance,
        final CmReltypedef relTypeDef) {
        return findRelatedInstances(
            fromInstance, relTypeDef, true);
    }

    /**
     * @deprecated in 0.11.0 will be removed in 0.12.0
     */
    @Deprecated
    public List<IbInstance> findRelatedInstancesTo(
        final IbInstance toInstance,
        final CmReltypedef relTypeDef) {
        return findRelatedInstances(
            toInstance, relTypeDef, false);
    }

    /**
     * if from is true then ibInstance is considered the from field in the
     * relationship
     */
    @SuppressWarnings("unchecked")
    public List<IbInstance> findRelatedInstances(
        final IbInstance ibInstance,
        final CmReltypedef relTypeDef, boolean instanceIsFrom) {
        String toProp = relTypeDef.getRelationshipInstanceToProperty(
            adfFactory);
        String fromProp = relTypeDef.getRelationshipInstanceFromProperty(
            adfFactory);
        String origProp, targetProp;
        if (instanceIsFrom) {
            origProp = fromProp;
            targetProp = toProp;
        } else {
            origProp = toProp;
            targetProp = fromProp;
        }
        String qry = "SELECT c." + targetProp
            + " FROM " + relTypeDef.getJpaRelationshipEntity()
            + " c WHERE c.cmReltypedef = :cmReltypedef"
            + " AND c." + origProp + " = :ibInstance"
            + " ORDER By c." + targetProp + ".caption";
        Query q = em.createQuery(qry);
        q.setParameter("cmReltypedef", relTypeDef);
        q.setParameter("ibInstance", ibInstance);
        return q.getResultList();
    }


    public Relationship findRelationship(
        final IbInstance fromInstance,
        final IbInstance toInstance,
        final CmReltypedef relTypeDef) {
        String qry = "SELECT c FROM " + relTypeDef.getJpaRelationshipEntity()
            + " c WHERE c.cmReltypedef = :cmReltypedef"
            + " AND c."
            + relTypeDef.getRelationshipInstanceFromProperty(adfFactory)
            + " = :fromInstance"
            + " AND c."
            + relTypeDef.getRelationshipInstanceToProperty(adfFactory)
            + " = :toInstance";
        Query q = em.createQuery(qry);
        q.setParameter("cmReltypedef", relTypeDef);
        q.setParameter("fromInstance", fromInstance);
        q.setParameter("toInstance", toInstance);
        return find(q);
    }

    @SuppressWarnings("unchecked")
    public List<InstanceRelationship> findRelationships(
        final CmReltypedef relTypeDef) {
        List<InstanceRelationship> list = new ArrayList<InstanceRelationship>();
        CmTypedef from = relTypeDef.getFromCmTypedef();
        CmTypedef to = relTypeDef.getToCmTypedef();

        String relTable = "R"
            + StringUtils.tableName2CamelCase(relTypeDef.getEntity());

        String qry = "select c." + from.getParamInstanceProperty() + ", c." +
            to.getParamInstanceProperty() + " from " + relTable + " as c "
            + "where c.cmReltypedef = :relTypeDef";
        Query q = em.createQuery(qry);
        q.setParameter("relTypeDef", relTypeDef);
        List<Object[]> resultList = q.getResultList();
        for (Object[] instances : resultList) {
            IbInstance f = (IbInstance) instances[0];
            IbInstance t = (IbInstance) instances[1];
            list.add(new InstanceRelationship(f, t, relTypeDef));
        }
        return list;
    }

    public void saveRelatedInstances(
        final IbInstance instance,
        List<IbInstance> newRelatedInstances,
        final CmReltypedef relTypeDef, boolean from) {
        String prop;
        if (from) {
            prop = relTypeDef.getRelationshipInstanceFromProperty(
                adfFactory);
        } else {
            prop = relTypeDef.getRelationshipInstanceToProperty(
                adfFactory);
        }
        String qry = "DELETE "
            + " FROM " + relTypeDef.getJpaRelationshipEntity()
            + " c WHERE c.cmReltypedef = :cmReltypedef"
            + " AND c." + prop + " = :instance";
        Query q = em.createQuery(qry);
        q.setParameter("cmReltypedef", relTypeDef);
        q.setParameter("instance", instance);
        q.executeUpdate();
        if (from) {
            for (IbInstance toIbInstance : newRelatedInstances) {
                createRelationship(relTypeDef,
                    instance, toIbInstance);
            }
        } else {
            for (IbInstance fromIbInstance : newRelatedInstances) {
                createRelationship(relTypeDef,
                    fromIbInstance, instance);
            }
        }
    }

    // vvv specific finds

    public CmSimulation findCmSimulation(final String module) {
        Query q = em.createNamedQuery("CmSimulation.findByModule");
        q.setParameter("module", module);
        return find(q);
    }

    // vvv adf-logic
    public CmDatatype findDataType(final String name) {
        Query q = em
            .createQuery("SELECT c FROM CmDatatype c WHERE c.key = :key");
        q.setParameter("key", name);
        return find(q);
    }

    public CmParamkind findParamKind(final String name) {
        Query q = em.createNamedQuery("CmParamkind.findByKey");
        q.setParameter("key", name);
        return find(q);
    }

    public CmUnitofmeasure findUnitOfMeasure(final String key) {
        Query q = em.createNamedQuery("CmUnitofmeasure.findByKey");
        q.setParameter("key", key);
        return find(q);
    }

    /**
     * @deprecated in 0.11.0
     */
    @Deprecated
    public void deleteInstance(IbInstance instance) {
        removeInstances(instance);
    }

    public <T> void remove(Collection<T> entities) {
        for (T entity : entities) {
            if (entity instanceof IbInstance) {
                removeInstances(Collections.singletonList((IbInstance) entity));
                continue;
            }
            // if (entity instanceof IbInstanceParameter) {
            // invalidateCachedAdfInstances(((IbInstanceParameter)entity)
            // .getInstance(cmParamdef));
            // }
                entity = em.merge(entity);
                em.remove(entity);
        }
    }

    /**
     * Deletes the instance and all relationships linked to it
     * @param instance
     */
    public void removeInstances(IbInstance... instances) {
        removeInstances(Arrays.asList(instances));
    }

    public void removeInstances(Iterable<IbInstance> instances) {
        for (IbInstance instance : instances) {
            if (instance == null) {
                continue;
            }
            instance = em.merge(instance);
            CmTypedef type = instance.getCmTypedef();
            for (CmReltypedef relType : findRelTypedefs(type)) {
                deleteRelations(relType, instance);
            }

            // XXX: only need to do it once per hierarchy entity!
            List<CmHierarchyDef> hierarchies = findAll(CmHierarchyDef.class);
            for (CmHierarchyDef cmHierarchyDef : hierarchies) {
                deleteHierarchyLinks(cmHierarchyDef, instance);
            }

            deleteParamValues(instance);
            em.remove(instance);
            invalidateCachedAdfInstances(instance);
        }
    }

    public int deleteParamValues(final IbInstance instance) {
        CmTypedef typedef = instance.getCmTypedef();
        String property = typedef.getParamInstanceProperty();
        int ret = 0;
        Set<CmParamdef> parmdefs = typedef.getCmParamdefs();
        Set<String> tables = new HashSet<String>(parmdefs.size());
        for (CmParamdef paramdef : parmdefs) {
            tables.add(paramdef.getParamJpaEntity());
        }
        for (String table : tables) {
            Query q = em.createQuery("delete from " + table + " a where a."
                + property + " = :instance and "
                + "a.cmTypedef = :typedef");
            q.setParameter("instance", instance);
            q.setParameter("typedef", typedef);
            ret += q.executeUpdate();
        }
        return ret;
    }

    public int deleteParamValues(final IbInstance instance,
        final CmParamdef... parmdefs) {
        return deleteParamValues(instance, Arrays.asList(parmdefs));
    }

    /**
     * FIXME: this does not support non-normal parameter tables. Should
     * determine which params are in which tables and if all the params are
     * present delete the row, otherwise set the applicable values to null ?!
     */
    public int deleteParamValues(final IbInstance instance,
        final Iterable<CmParamdef> parmdefs) {
        CmTypedef typedef = instance.getCmTypedef();
        String property = typedef.getParamInstanceProperty();
        int ret = 0;
        for (CmParamdef paramdef : parmdefs) {
            String table = paramdef.getParamJpaEntity();
            Query q = em.createQuery("delete from " + table + " a where a."
                + property + " = :instance and "
                + "a.cmTypedef = :typedef and a.cmParamdef = :parmdef");
            q.setParameter("instance", instance);
            q.setParameter("typedef", typedef);
            q.setParameter("parmdef", paramdef);
            ret += q.executeUpdate();
        }
        return ret;
    }

    public CmReltypedef findRelTypedef(
        final String relTypeDefKey) {
        Query q =
            em.createQuery("SELECT c FROM CmReltypedef c " +
                "WHERE c.key = :key");
        q.setParameter("key", relTypeDefKey);
        return find(q);
    }

    public CmReltypedef findRelTypedef(
        final CmTypedef fromCmTypedef,
        final CmTypedef toCmTypedef
        ) {
        CriteriaQuery<CmReltypedef> cq = cb.createQuery(CmReltypedef.class);
        Root<CmReltypedef> from = cq.from(CmReltypedef.class);
        cq.select(from).where(cb.and(
            cb.equal(from.get("fromCmTypedef"), fromCmTypedef),
            cb.equal(from.get("toCmTypedef"), toCmTypedef)));
        return find(cq);
    }

    public Collection<CmReltypedef> findRelTypedefs(
        final CmTypedef fromOrToCmTypedef
        ) {
        CriteriaQuery<CmReltypedef> cq = cb.createQuery(CmReltypedef.class);
        Root<CmReltypedef> from = cq.from(CmReltypedef.class);
        cq.select(from).where(cb.or(
            cb.equal(from.get("fromCmTypedef"), fromOrToCmTypedef),
            cb.equal(from.get("toCmTypedef"), fromOrToCmTypedef)));
        return findAll(cq);
    }

    public int deleteRelations(
        final CmReltypedef relTypeDef, final IbInstance fromOrToinstance) {
        List<InstanceRelationship> list = new ArrayList<InstanceRelationship>();
        CmTypedef fromType = relTypeDef.getFromCmTypedef();
        CmTypedef toType = relTypeDef.getToCmTypedef();
        boolean from = fromType.equals(fromOrToinstance.getCmTypedef());
        boolean to = toType.equals(fromOrToinstance.getCmTypedef());
        if (!from && !to) {
            log.warn("Instance is not part of this relationship:\n"
                + fromOrToinstance + "\n-> " + relTypeDef);
            return 0;
        }

        String relTable = "R"
            + StringUtils.tableName2CamelCase(relTypeDef.getEntity());

        val clauses = new ArrayList<String>();
        if (from) {
            clauses.add("c." + fromType.getParamInstanceProperty()
                + " = :from");
        }
        if (to) {
            clauses.add("c." + toType.getParamInstanceProperty() + " = :to");
        }
        String qry = "delete " + relTable + " as c "
            + "where c.cmReltypedef = :relTypeDef AND ("
            + StringUtils.join(clauses, " OR ")
            + ")";

        Query q = em.createQuery(qry);
        q.setParameter("relTypeDef", relTypeDef);
        if (from) {
            q.setParameter("from", fromOrToinstance);
        }
        if (to) {
            q.setParameter("to", fromOrToinstance);
        }
        return q.executeUpdate();
    }

    // vvv Metadata helpers
    public CmEntitytype getOrCreateCmEntitytype(final EntityType type) {
        return find(CmEntitytype.class, type.name(),
            new EntityCreator<CmEntitytype>() {
                public CmEntitytype createEntity() {
                    return new CmEntitytype(type.name(), type.toString());
                }
            });
    }

    public CmEntity getCmEntity(
        final Class<? extends AdfEntity> entityClass) {
        return getCmEntity(adfRegistry.getAdfEntity(entityClass)
            .getKey());
    }

    public CmEntity getCmEntity(final String entity) {
        CmEntity ret = find(CmEntity.class, entity);
        if (ret == null) {
            throw new AdfDbLayerException("Could not find entity: " + entity);
        }
        return ret;
    }

    /**
     * Preferably use *.model.<? extends AdfInstanceEntity>
     * to declare entities
     */
    public CmEntity getOrCreateCmEntity(final String entity,
        final EntityType type) {
        return getOrCreateCmEntity(entity, toCaption(entity), type);
    }

    /**
     * Preferably use *.model.<? extends AdfInstanceEntity>
     * to declare entities
     */
    public CmEntity getOrCreateCmEntity(final String entity,
        final String caption,
        final EntityType type) {
        return find(CmEntity.class, entity, new EntityCreator<CmEntity>() {
            public CmEntity createEntity() {
                return new CmEntity(entity,
                    caption,
                    getOrCreateCmEntitytype(type));
            }
        });
    }

    public AdfDataType<?> getAdfDataType(
        Class<? extends AdfDataType<?>> dtClass) {
        return adfRegistry.getAdfDataType(dtClass);
    }

    public AdfDataType<?> getAdfDataType(String dtKey) {
        return adfRegistry.getAdfDataType(dtKey);
    }

    public CmDatatype getCmDataType(AdfDataType<?> adfDataType) {
        return getCmDataType(adfDataType.getDataTypeClass());
    }

    public CmDatatype getCmDataType(Class<? extends AdfDataType<?>> dtClass) {
        if (dtClass == null) {
            return null;
        }
        AdfDataType<?> adfDataType = getAdfDataType(dtClass);
        if (adfDataType == null) {
            return null;
        }
        return find(CmDatatype.class, adfDataType.getKey());
    }

    public CmParamdef getOrCreateCmParamdef(final CmTypedef cmTypedef,
        final String key,
        final String caption,
        final String description,
        final ParameterKind paramkind,
        Class<? extends AdfDataType<?>> dtClass,
        final CmUnitofmeasure cmUnitofmeasure,
        Class<? extends AdfDataType<?>> indexDtClass) {
        return getOrCreateCmParamdef(cmTypedef, key, caption, description,
            getOrCreateParamKind(paramkind),
            getCmDataType(dtClass),
            cmUnitofmeasure, getCmDataType(indexDtClass));

    }

    public CmParamdef getOrCreateCmParamdef(final CmTypedef cmTypedef,
        final String key,
        final String caption,
        final String description,
        final CmParamkind cmParamkind,
        final CmDatatype cmDatatype,
        final CmUnitofmeasure cmUnitofmeasure,
        final CmDatatype cmIndexDatatype) {
        //cmTypedef.getCmParamdefs(); //load it
        return find(CmParamdef.class, new CmParamdefPK(cmTypedef, key),
            new EntityCreator<CmParamdef>() {
                public CmParamdef createEntity() {
                    return new CmParamdef(cmTypedef, key,
                        caption,
                        description,
                        cmParamkind,
                        cmDatatype,
                        cmUnitofmeasure,
                        cmIndexDatatype);
                }
            });
    }

    public CmParamdef getCmParamdef(final CmTypedef cmTypedef,
        final String key) {
        return find(CmParamdef.class, new CmParamdefPK(cmTypedef, key));
    }

    public <T extends IbInstance> T getOrCreateInstance(
        final CmTypedef typedef,
        final String instanceCaption) {
        Class<T> klass = adfFactory.getJpaEntityClass(
            typedef.getJpaInstanceEntity());
        CriteriaQuery<T> cq = cb.createQuery(klass);
        Root<T> from = cq.from(klass);
        cq.select(from).where(cb.and(
            cb.equal(from.get("cmTypedef"), typedef),
            cb.equal(from.get("caption"), instanceCaption)));

        T instance = find(cq, new EntityCreator<T>() {
            @SneakyThrows
            public T createEntity() {
                return (T)createInstance(typedef, instanceCaption);
            }
        });
        return instance;
    }

    public void linkHierarchyInstancesIfNeeded(CmHierarchyDef cmHierarchyDef,
        IbInstance sourceInstance, IbInstance targetInstance,
        Date effectiveDate,
        Date startDatetime, Date endDatetime,
        boolean isSourceTheParent
        ) {
        // log.debug("adding hierarchy relationship " + cmHierarchyDef.getKey()
        // + " from: "
        // + (sourceInstance != null ? sourceInstance.getCaption() : null)
        // + " to: "
        // + (targetInstance != null ? targetInstance.getCaption() : null));
        if (!isSourceTheParent) {
            IbInstance tmp = sourceInstance;
            sourceInstance = targetInstance;
            targetInstance = tmp;
        }
        linkHierarchyInstancesIfNeeded(cmHierarchyDef,
            sourceInstance, targetInstance, effectiveDate,
            startDatetime, endDatetime);
    }

    public void linkHierarchyInstancesIfNeeded(CmHierarchyDef cmHierarchyDef,
        IbInstance parentInstance, IbInstance childInstance,
        Date effectiveDate,
        Date startDatetime, Date endDatetime
        ) {
        if (!isHierarchyLinked(cmHierarchyDef.getKey(), parentInstance,
            childInstance, effectiveDate)) {
            linkHierarchyInstances(cmHierarchyDef,
                parentInstance, childInstance,
                startDatetime, endDatetime);
        }
    }

    @SuppressWarnings("unchecked")
    @SneakyThrows
    public <T extends IbInstance> T createInstance(
        final CmTypedef typedef, final String instanceCaption) {
        IbInstance ret = adfFactory.instantiateJpaEntity(
            typedef.getJpaInstanceEntity());
        ret.setCmTypedef(typedef);
        ret.setCaption(instanceCaption);
        return (T) ret;
    }

    public <T extends Relationship> T createRelationship(
        IbInstance fromInstance, IbInstance toInstance) {
        return (T)createRelationship(findRelTypedef(fromInstance.getCmTypedef(),
            toInstance.getCmTypedef()),
            fromInstance, toInstance);
    }

    @SuppressWarnings("unchecked")
    @SneakyThrows
    public <T extends Relationship> T createRelationship(
        final CmReltypedef relTypeDef,
        IbInstance fromInstance, IbInstance toInstance) {
        log.debug("adding relationship from: "
            + fromInstance.getCaption()
            + " to: " + toInstance.getCaption());

        Relationship ret = adfFactory.instantiateJpaEntity(
            relTypeDef.getJpaRelationshipEntity());
        ret.setCmReltypedef(relTypeDef);

        AdfDbLayerUtils.setProperty(ret,
            relTypeDef.getRelationshipInstanceFromProperty(adfFactory),
            fromInstance);
        AdfDbLayerUtils.setProperty(ret,
            relTypeDef.getRelationshipInstanceToProperty(adfFactory),
            toInstance);
        return (T) save(ret);
    }

    /**
     * This validates that the type exists. If you want to manually check if it
     * exists, rather use findTypedefByKey directly.
     */
    public CmTypedef getCmTypedef(final String typedefKey)
        throws IllegalArgumentException {
        CriteriaQuery<CmTypedef> cq = cb.createQuery(CmTypedef.class);
        Root<CmTypedef> from = cq.from(CmTypedef.class);
        cq.select(from).where(cb.and(
            cb.equal(from.get("key"), typedefKey)));
        CmTypedef cmTypedef = find(cq);
        if (cmTypedef == null) {
            throw new IllegalArgumentException("Unknown type: " + typedefKey);
        }
        return cmTypedef;
    }

    public CmTypedef getCmTypedef(
        Class<? extends AdfInstance> instanceClass) {
        AdfTypeDefMetadata md = adfRegistry.getAdfTypeDef(instanceClass);
        CmTypedef ret = find(getCmTypedefQuery(
            getCmEntity(md.getEntityClass()), md.getKey()));
        return ret;
    }

    public CmTypedef getCmTypedef(
        final CmEntity cmEntity, final String key) {
        CmTypedef ret = find(getCmTypedefQuery(cmEntity, key));
        return ret;
    }

    protected CriteriaQuery<CmTypedef> getCmTypedefQuery(
        final CmEntity cmEntity,
        final String key) {
        CriteriaQuery<CmTypedef> cq = cb.createQuery(CmTypedef.class);
        Root<CmTypedef> from = cq.from(CmTypedef.class);
        cq.select(from).where(cb.and(
            cb.equal(from.get("cmEntity"), cmEntity),
            cb.equal(from.get("key"), key)));
        return cq;
    }

    @Deprecated
    public CmTypedef getOrCreateCmTypedef(
        final CmEntity cmEntity, final String key) {
        return getOrCreateCmTypedef(cmEntity, key, toCaption(key));
    }

    @Deprecated
    public CmTypedef getOrCreateCmTypedef(
        final CmEntity cmEntity, final String key, final String caption) {
        return find(getCmTypedefQuery(cmEntity, key),
            new EntityCreator<CmTypedef>() {
                public CmTypedef createEntity() {
                    return new CmTypedef(cmEntity, key, caption);
                }
            });
    }

    public CmTypedef getOrCreateCmTypedef(
        Class<? extends AdfInstanceEntity> entityClass,
        Class<? extends AdfInstance> instanceClass,
        final String key, final String caption, final String description) {
        final CmEntity cmEntity = getCmEntity(entityClass);
        // adfRegistry.registerTypeDef(
        // new AdfTypeDefMetadata().init(
        // key, caption, description, entityClass, instanceClass));
        CmTypedef ret = find(getCmTypedefQuery(cmEntity, key),
            new EntityCreator<CmTypedef>() {
                public CmTypedef createEntity() {
                    return new CmTypedef(cmEntity, key, caption);
                }
            });
        ret.setDescription(description);
        return ret;
    }

    @Deprecated
    public CmReltypedef getOrCreateCmReltypedef(
        final CmEntity cmEntity, final String key,
        final CmTypedef fromCmTypedef,
        final CmTypedef toCmTypedef
        ) {
        return getOrCreateCmReltypedef(cmEntity, key, toCaption(key),
            fromCmTypedef, toCmTypedef);
    }

    public CmReltypedef getCmReltypedef(
        AdfRelationshipTypeDefMetadata relTypeDef) {
        return getCmReltypedef(getCmEntity(relTypeDef.getEntityClass()),
            relTypeDef.getKey());
    }

    public CmReltypedef getCmReltypedef(
        final CmEntity cmEntity, final String key
        ) {
        CriteriaQuery<CmReltypedef> cq = cb.createQuery(CmReltypedef.class);
        Root<CmReltypedef> from = cq.from(CmReltypedef.class);
        cq.select(from).where(cb.and(
            cb.equal(from.get("cmEntity"), cmEntity),
            cb.equal(from.get("key"), key)));
        return find(cq);
    }

    @Deprecated
    public CmReltypedef getOrCreateCmReltypedef(
        final CmEntity cmEntity, final String key, final String caption,
        final CmTypedef fromCmTypedef,
        final CmTypedef toCmTypedef
        ) {
        CriteriaQuery<CmReltypedef> cq = cb.createQuery(CmReltypedef.class);
        Root<CmReltypedef> from = cq.from(CmReltypedef.class);
        cq.select(from).where(cb.and(
            cb.equal(from.get("cmEntffity"), cmEntity),
            cb.equal(from.get("key"), key)));
        return find(cq,
            new EntityCreator<CmReltypedef>() {
                public CmReltypedef createEntity() {
                    return new CmReltypedef(cmEntity, key, caption,
                        fromCmTypedef, toCmTypedef);
                }
            });
    }

    public CmHierarchyDef getCmHierarchyDef(
        final CmEntity cmEntity, final String key
        ) {
        // XXX: convert to a named query
        CriteriaQuery<CmHierarchyDef> cq = cb.createQuery(CmHierarchyDef.class);
        Root<CmHierarchyDef> from = cq.from(CmHierarchyDef.class);
        cq.select(from).where(cb.and(
            cb.equal(from.get("cmEntity"), cmEntity),
            cb.equal(from.get("key"), key)));
        return find(cq);
    }

    public CmParamkind getOrCreateParamKind(ParameterKind pk) {
        CmParamkind ret = find(CmParamkind.class, pk.name());
        if (ret == null) {
            ret = save(new CmParamkind(pk));
        }
        return ret;
    }

    public CmUnitofmeasure getOrCreateUnitOfmeasure(String key) {
        CmUnitofmeasure ret = find(CmUnitofmeasure.class, key);
        if (ret == null) {
            ret = save(new CmUnitofmeasure(key, key));
        }
        return ret;
    }

    public void cloneInstance(IbInstance origIbInstance, String newCaption) {
        IbInstance newInstance = getOrCreateInstance(
            origIbInstance.getCmTypedef(),
            newCaption);
        for (CmParamdef paramDef : origIbInstance.getCmTypedef()
            .getCmParamdefs()) {
            List<IbInstanceParameter> paramValues =
                this.getParamValues(origIbInstance, paramDef);
            for (IbInstanceParameter paramValue : paramValues) {
                setSingleParamValue(newInstance, paramDef.getKey(),
                    getParamValue(paramValue, paramDef),
                    paramValue.getAppldatetime(),
                    paramValue.getIndx());
            }
        }
        log.debug("added: " + newInstance);
    }

// /**
// * @param moveFile if true, move the file, otherwise just copy it
// */
// public AdfResourceUrl addResourceFromLocalFile(
// IbInstance instance, String paramdefKey, Date appldatetime,
// File sourceFile, boolean moveFile
// ) throws IOException {
//
// CmParamdef docParamdef = getCmParamdef(
// instance.getCmTypedef(), paramdefKey);
//
// HomeFile homeFile = EbrFactory.getHomeService().getFile(
// AdfResourceUrl.getResourceDir(adfFactory,
// instance,
// docParamdef, sourceFile.getName()));
//
// // move it into its final destination
// homeFile.makeParentDirs();
// if (moveFile) {
// FileUtils.moveFile(sourceFile, homeFile.getFile());
// } else {
// FileUtils.copyFile(sourceFile, homeFile.getFile());
// }
//
// String url = ADF_RESOURCE_URL_PREFIX + homeFile.getName();
//
// AdfResourceUrl aru = new AdfResourceUrl(instance, docParamdef, url);
//
// addParamValue(instance, paramdefKey, appldatetime, aru);
//
// return aru;
// }

    // vvv Hierarchy methods

    public CmHierarchyDef findHierarchy(
        final String hierarchyDefKey) {
        Query q =
            em.createQuery("SELECT c FROM "
                + CmHierarchyDef.class.getSimpleName() + " c " +
                "WHERE c.key = :key");
        q.setParameter("key", hierarchyDefKey);
        return find(q);
    }

    public CmHierarchyDef getHierarchy(
        final String hierarchyDefKey) {
        val ret = findHierarchy(hierarchyDefKey);
        if (ret == null) {
            throw new IllegalArgumentException(
                "Could not find hierachy with key: " + hierarchyDefKey);
        }
        return ret;
    }

    public CmHierarchyDef getOrCreateHierarchy(
        final String hierarchyDefKey) {
        CmHierarchyDef ret = findHierarchy(hierarchyDefKey);
        if (ret == null) {
            ret = save(new CmHierarchyDef(
                getOrCreateCmEntity(ENTITY_HIERARCHY_RELATIONSHIP, HIERARCHY),
                hierarchyDefKey));
        }
        return ret;
    }

    public void addHierarchyRootInstance(String hierarchyKey,
        IbInstance instance,
        Date startDatetime, Date endDatetime) {
        linkHierarchyInstances(hierarchyKey, (IbInstance) null, instance,
            startDatetime, endDatetime);
    }

    public void addHierarchyRootInstance(CmHierarchyDef cmHierarchyDef,
        IbInstance instance,
        Date startDatetime, Date endDatetime) {
        linkHierarchyInstances(cmHierarchyDef, (IbInstance) null, instance,
            startDatetime, endDatetime);
    }

    public void linkHierarchyInstances(String hierarchyKey,
        IbInstance parentInstance, IbInstance childInstance,
        Date startDatetime, Date endDatetime) {
        CmHierarchyDef cmHierarchyDef = findHierarchy(hierarchyKey);
        linkHierarchyInstances(cmHierarchyDef, parentInstance, childInstance,
            startDatetime, endDatetime);
    }

    public void linkHierarchyInstances(CmHierarchyDef cmHierarchyDef,
        IbInstance sourceInstance, IbInstance targetInstance,
        Date startDatetime, Date endDatetime, boolean isSourceTheParent) {
        if (!isSourceTheParent) {
            IbInstance tmp = sourceInstance;
            sourceInstance = targetInstance;
            targetInstance = tmp;
        }
        linkHierarchyInstances(cmHierarchyDef,
            sourceInstance, targetInstance,
            startDatetime, endDatetime);
    }

    public void linkHierarchyInstances(CmHierarchyDef cmHierarchyDef,
        IbInstance parentInstance, IbInstance childInstance,
        Date startDatetime, Date endDatetime) {
        // log.debug("adding hierarchy relationship " + cmHierarchyDef.getKey()
        // + " from: "
        // + (parentInstance != null ? parentInstance.getCaption() : null)
        // + " to: "
        // + (childInstance != null ? childInstance.getCaption() : null));
        HierarchyRelationship rel = instantiateJpaEntity(
            cmHierarchyDef.getJpaHierarchyEntity());
        rel.init(cmHierarchyDef, parentInstance, childInstance,
            startDatetime, endDatetime);
        invalidateCachedAdfInstances(parentInstance, childInstance);
        em.persist(rel);
        // em.flush();
    }

    public void linkHierarchyInstances(String hierarchyKey,
        IbInstance childInstance, List<? extends IbInstance> parentInstances,
        Date startDatetime, Date endDatetime) {
        CmHierarchyDef cmHierarchyDef = findHierarchy(hierarchyKey);
        for (IbInstance parentInstance : parentInstances) {
            linkHierarchyInstances(cmHierarchyDef, parentInstance,
                childInstance, startDatetime, endDatetime);
        }
    }

    public void linkHierarchyInstances(String hierarchyKey,
        List<? extends IbInstance> childInstances, IbInstance parentInstance,
        Date startDatetime, Date endDatetime) {
        CmHierarchyDef cmHierarchyDef = findHierarchy(hierarchyKey);
        for (IbInstance childInstance : childInstances) {
            linkHierarchyInstances(cmHierarchyDef, parentInstance,
                childInstance, startDatetime, endDatetime);
        }
    }




    public boolean isHierarchyLinked(
        String hierarchyKey, IbInstance parent,
        IbInstance child,
        Date effectiveDate) {
        if (effectiveDate == null) {
            effectiveDate = new Date();
        }
        CmHierarchyDef cmHierarchyDef = findHierarchy(hierarchyKey);
        String hierarchyEntity = cmHierarchyDef.getJpaHierarchyEntity();
        String parentWhereClause, childWhereClause;
        if (parent == null) {
            // look for roots
            parentWhereClause = " AND j.parentCmTypedef is null";
        } else {
            parentWhereClause =
                " AND j.parentCmTypedef = :parentCmTypedef"
                    + " AND j.parentInstanceId = :parentId";
        }
        if (child == null) {
            // look for roots
            childWhereClause = " AND j.childCmTypedef is null";
        } else {
            childWhereClause =
                " AND j.childCmTypedef = :childCmTypedef"
                    + " AND j.childInstanceId = :childId";
        }
        String query = "SELECT j FROM "
            + hierarchyEntity + " j"
            + " WHERE j.cmHierarchyDef = :cmHierarchyDef"
            + parentWhereClause
            + childWhereClause
            + " AND :effectiveDate >= j.startDatetime"
            + " AND :effectiveDate < j.endDatetime";
        // log.debug(query);
        Query q = em.createQuery(query);
        q.setParameter("cmHierarchyDef", cmHierarchyDef);
        if (parent != null) {
            q.setParameter("parentCmTypedef", parent.getCmTypedef());
            q.setParameter("parentId", parent.getId());
        }
        if (child != null) {
            q.setParameter("childCmTypedef", child.getCmTypedef());
            q.setParameter("childId", child.getId());
        }
        q.setParameter("effectiveDate", effectiveDate);
        List<IbInstance> ret = new ArrayList<IbInstance>();
        @SuppressWarnings("unchecked") List<HierarchyRelationship> resultList =
            q.setMaxResults(1).getResultList();
        return !ret.isEmpty();
    }


    /**
     * If your data was loaded with the parent child relationships swapped
     * around, then you can use this method to fix it..
     */
    public void invertHierarchyLinkDirections(
        String hierarchyKey) {
        CmHierarchyDef cmHierarchyDef = findHierarchy(hierarchyKey);
        String hierarchyEntity = cmHierarchyDef.getJpaHierarchyEntity();

        // find all orphans (nodes without parents)
        Query q = em.createQuery(
            "SELECT link FROM "
                + hierarchyEntity + " link"
                + " WHERE link.cmHierarchyDef = :cmHierarchyDef"
            );
        q.setParameter("cmHierarchyDef", cmHierarchyDef);

        @SuppressWarnings("unchecked") List<HierarchyRelationship> links =
            q.getResultList();
        for (HierarchyRelationship link : links) {
            // invert it
            CmTypedef parentCmTypedef = link.getParentCmTypedef();
            Integer parentInstanceId = link.getParentInstanceId();

            // but only if it is not a root
            if (parentCmTypedef != null && parentCmTypedef != null) {
                log.debug("Inverting hierarchy link direction:\n  "
                    + link);
                link.setParentCmTypedef(link.getChildCmTypedef());
                link.setParentInstanceId(link.getChildInstanceId());

                link.setChildCmTypedef(parentCmTypedef);
                link.setChildInstanceId(parentInstanceId);
            }
        }
    }

    // TODO: add method unrootIfNeeded(instance?),
    // can optionally be called on child when adding a new link
    // alternatively we can add a method to unroot all nodes with parents...

    public int deleteHierarchyLinks(CmHierarchyDef cmHierarchyDef,
        IbInstance instance) {
        String hierarchyEntity = cmHierarchyDef.getJpaHierarchyEntity();
        Query q = em.createQuery("delete FROM "
            + hierarchyEntity + " j"
            + " WHERE j.cmHierarchyDef = :cmHierarchyDef"
            + " AND ((j.parentCmTypedef = :instanceCmTypedef"
            + " AND j.parentInstanceId = :instanceId)"
            + " OR (j.childCmTypedef = :instanceCmTypedef"
            + " AND j.childInstanceId = :instanceId))"
            );
        q.setParameter("cmHierarchyDef", cmHierarchyDef);
        q.setParameter("instanceCmTypedef", instance.getCmTypedef());
        q.setParameter("instanceId", instance.getId());
        return q.executeUpdate();
    }

    public void transferHierarchyLinks(
        IbInstance sourceInstance, IbInstance targetInstance) {
        log.debug("Transfering all hierarchy links from {} to {}",
            sourceInstance, targetInstance);
        // XXX: only need to do it once per hierarchy entity!
        List<CmHierarchyDef> hierarchies = findAll(CmHierarchyDef.class);
        for (CmHierarchyDef cmHierarchyDef : hierarchies) {
            transferHierarchyLinks(cmHierarchyDef,
                sourceInstance, targetInstance);
        }
        this.invalidateCachedAdfInstances(sourceInstance, targetInstance);
    }

    private void transferHierarchyLinks(CmHierarchyDef cmHierarchyDef,
        IbInstance sourceInstance, IbInstance targetInstance) {
        String hierarchyEntity = cmHierarchyDef.getJpaHierarchyEntity();
        transferHierarchyLinks(cmHierarchyDef, sourceInstance, targetInstance,
            hierarchyEntity, false);
        transferHierarchyLinks(cmHierarchyDef, sourceInstance, targetInstance,
            hierarchyEntity, true);
    }

    private void transferHierarchyLinks(CmHierarchyDef cmHierarchyDef,
        IbInstance sourceInstance, IbInstance targetInstance,
        String hierarchyEntity, boolean child) {
        val q = em.createQuery(format("SELECT j FROM %1$s j"
            + " WHERE j.cmHierarchyDef = :cmHierarchyDef"
            + " AND j.%2$sCmTypedef = :sourceInstanceCmTypedef"
            + " AND j.%2$sInstanceId = :sourceInstanceId",
            hierarchyEntity, child ? "child" : "parent"),
            HierarchyRelationship.class);
        q.setParameter("cmHierarchyDef", cmHierarchyDef);
        q.setParameter("sourceInstanceCmTypedef",
            sourceInstance.getCmTypedef());
        q.setParameter("sourceInstanceId", sourceInstance.getId());
        for (HierarchyRelationship h : q.getResultList()) {
            if (child) {
                h.setChildInstanceId(targetInstance.getId());
                h.setChildCmTypedef(targetInstance.getCmTypedef());
            } else {
                h.setParentInstanceId(targetInstance.getId());
                h.setParentCmTypedef(targetInstance.getCmTypedef());
            }
            invalidateCachedAdfInstances(h.getParentInstance(this),
                h.getChildInstance(this));
        }

    }

    public void deleteHierarchy(String hierarchyKey) {
        CmHierarchyDef cmHierarchyDef = findHierarchy(hierarchyKey);
        if (cmHierarchyDef != null) {
            String hierarchyEntity = cmHierarchyDef.getJpaHierarchyEntity();
            Query q = em.createQuery("delete FROM "
                + hierarchyEntity + " j"
                + " WHERE j.cmHierarchyDef = :cmHierarchyDef"
                );
            q.setParameter("cmHierarchyDef", cmHierarchyDef);
            q.executeUpdate();
            this.remove(cmHierarchyDef);
        }
    }
    protected CmTypedef getOrCreateHierarchyNodetype(String entity,
        String nodeTypeDefKey) {
        CmEntity entityHierarchyNode = getCmEntity(entity);

        CmTypedef ret = getOrCreateCmTypedef(entityHierarchyNode,
            nodeTypeDefKey, toCaption(nodeTypeDefKey)
                    + " Type");
        ret.setDescription("All the properties of "
            + ret.getCaption());

        // add the basic param defs
        // getAdfFactory().getGisDbLayer().addGisParameter(ret);

        return ret;
    }

    // ^^^ Hierarchy methods

    // vvv Set up common metadata

    protected CmEntity getHierarchyNodeEntity(String entity) {
        return getOrCreateCmEntity(
            entity,
            "Hierarchy Node", EntityType.INSTANCE);
    }

    // ^^^ Set up common metadata

}
