package za.co.enerweb.ebr.adf.model.timeseries;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NumArraySummary implements Serializable {
    private static final long serialVersionUID = 1L;
    public static String PERCENTILE_5 = "5th Percentile";
    public static String MEAN = "Mean";
    public static String PERCENTILE_95 = "95th Percentile";
    private double percentile5, mean,
        percentile95;
}
