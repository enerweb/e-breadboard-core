package za.co.enerweb.ebr.adf.rest.model;

import static za.co.enerweb.toolbox.string.StringUtils.caption2VariableName;

import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import za.co.enerweb.ebr.adf.entities.IbInstance;
import za.co.enerweb.toolbox.rest.ExpandMembers;

@Data
@EqualsAndHashCode(callSuper = true)
@XmlRootElement(name = RCObject.URI_FRAGMENT)
@NoArgsConstructor
@AllArgsConstructor
public class RCObject extends ARCCmConcept<IbInstance> {
    public final static String URI_FRAGMENT = "Object";

    private String caption;

    public RCObject(UriInfo uriInfo, ExpandMembers expand,
        IbInstance cmObject) {
        super(uriInfo, expand, cmObject);
        caption = cmObject.getCaption();
        if (isExpand()) {
        }
    }

    public String getUriFragment() {
        return URI_FRAGMENT;
    }

    protected String deriveSlug(IbInstance cmObject) {
        return RCClass.deriveSlugFromCmTypedef(cmObject.getCmTypedef()) + "."
            + caption2VariableName(cmObject.getCaption()).toLowerCase() + "-"
            + cmObject.getId();
    }

    public static String typeDefKeyFromSlug(String slug) {
        return keyFromSlug(parentSlug(slug)).toUpperCase();
    }

    public static Integer instanceIdFromSlug(String slug) {
        return idFromSlug(childSlug(slug));
    }

}
