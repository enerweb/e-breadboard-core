package za.co.enerweb.ebr.adf.model;

import static za.co.enerweb.toolbox.string.StringUtils.variableName2Caption;

public enum EntityType {
    INSTANCE, RELATIONSHIP, HIERARCHY;

    public String toString() {
        return variableName2Caption(name());
    }
}
