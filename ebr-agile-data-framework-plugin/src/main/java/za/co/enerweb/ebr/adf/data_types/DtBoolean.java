package za.co.enerweb.ebr.adf.data_types;

import za.co.enerweb.ebr.adf.model.AdfScalarDataType;

public class DtBoolean extends AdfScalarDataType<Boolean> {

    public String getDescription() {
        return "Boolean Flag (Y/N)";
    }

    public Class<Boolean> getJavaType() {
        return Boolean.class;
    }

    public String toPersistedValue(Boolean value) {
        return ((Boolean) value) ? "Y" : "N";
    }

    public Boolean fromPersistedValue(String persistedValue) {
        if (persistedValue.equals("Y")) {
            return Boolean.TRUE;
        } else if (persistedValue.equals("N")) {
            return Boolean.FALSE;
        } else {
            throw new IllegalArgumentException(
                "Unsupported boolean value: "
                    + "'" + persistedValue
                    + "', expected Y or N. Fix your data.");
        }
    }

}
