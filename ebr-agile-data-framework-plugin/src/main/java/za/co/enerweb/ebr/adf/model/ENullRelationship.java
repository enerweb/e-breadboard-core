package za.co.enerweb.ebr.adf.model;

import za.co.enerweb.ebr.adf.model.AdfInstanceEntity;

public abstract class ENullRelationship extends AdfRelationshipEntity {
    public static final String KEY = deriveKey(ENullRelationship.class);
}
