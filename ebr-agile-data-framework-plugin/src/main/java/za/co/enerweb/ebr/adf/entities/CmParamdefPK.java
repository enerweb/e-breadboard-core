package za.co.enerweb.ebr.adf.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Embeddable
public class CmParamdefPK implements Serializable {
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @JoinColumn(name = "TYPEDEF_ID", referencedColumnName = "ID",
        nullable = false)
    @ManyToOne(optional = false, targetEntity = CmTypedef.class)
    private CmTypedef cmTypedef;

    @Basic(optional = false)
    @Column(name = "KEY", nullable = false, length = 50)
    private String key;

    public CmParamdefPK() {
    }

    public CmParamdefPK(CmTypedef cmTypedef, String key) {
        this.cmTypedef = cmTypedef;
        this.key = key;
    }
}
