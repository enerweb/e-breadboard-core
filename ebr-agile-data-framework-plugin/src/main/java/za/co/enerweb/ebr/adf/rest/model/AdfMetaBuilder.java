/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package za.co.enerweb.ebr.adf.rest.model;

import javax.ws.rs.core.UriInfo;

import za.co.enerweb.toolbox.rest.Meta;
import za.co.enerweb.toolbox.rest.MetaBuilder;

/**
 * Always supports json and xml;
 */
public class AdfMetaBuilder extends MetaBuilder {

    public AdfMetaBuilder(String href) {
        super(href);
    }

    public AdfMetaBuilder(UriInfo uriInfo) {
        super(uriInfo);
    }

    @Override
    public Meta build() {
        this.applicationJson().applicationXml();
        return super.build();
    }

}
