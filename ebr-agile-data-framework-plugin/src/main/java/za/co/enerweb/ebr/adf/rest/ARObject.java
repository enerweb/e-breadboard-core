package za.co.enerweb.ebr.adf.rest;

import java.util.Collection;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.adf.rest.model.RCObject;
import za.co.enerweb.ebr.adf.rest.model.RCObjectState;
import za.co.enerweb.ebr.adf.rest.model.RCObjectStates;
import za.co.enerweb.toolbox.rest.ExpandMembers;
import za.co.enerweb.toolbox.rest.ResponsTypeUtil;

// specified in Application:
//@Produces({MediaType.APPLICATION_JSON
// //, MediaType.APPLICATION_XML
//})
@Path("/" + RCObject.URI_FRAGMENT)
@Slf4j
public abstract class ARObject extends ARConcept {

//    @Path( ResponsTypeUtil.PATH)
//    @GET
//    public Response getAll(@Context UriInfo uriInfo,
//        @PathParam(ResponsTypeUtil.PARAM) String responseType,
//        @QueryParam(ExpandMembers.PARAM) String expand) {
//        final RProperties ret = getDbl().getAllRProperties(uriInfo,
//            new ExpandMembers(URI_FRAGMENT, expand));
//        return ResponsTypeUtil.parse(
//            Response.ok(ret), responseType).build();
//    }

    @Path("/{idFieldValue}" + ResponsTypeUtil.PATH)
    @GET
    public Response get(@Context UriInfo uriInfo,
        @PathParam(ResponsTypeUtil.PARAM) String responseType,
        @QueryParam(ExpandMembers.PARAM) String expand,
        @NotNull @Size(min = 1)
        @PathParam("idFieldValue") String slug) {

        final RCObject ret = getRCObject(uriInfo, expand, slug);

        return ResponsTypeUtil.parse(Response.ok(ret), responseType).build();
    }

    protected RCObject getRCObject(UriInfo uriInfo, String expand,
        String slug) {
        final RCObject ret = getRDbl().getRCObject(uriInfo,
            new ExpandMembers(RCObject.URI_FRAGMENT, expand),
            RCObject.typeDefKeyFromSlug(slug),
            RCObject.instanceIdFromSlug(slug));
        return ret;
    }

    @Path("/{idFieldValue}/" + RCObjectStates.URI_FRAGMENT + ResponsTypeUtil.PATH)
    @GET
    public Response getInstances(@Context UriInfo uriInfo,
        @PathParam(ResponsTypeUtil.PARAM) String responseType,
        @QueryParam(ExpandMembers.PARAM) String expand,
        @NotNull @Size(min = 1) @PathParam("idFieldValue") String slug) {

        final RCObject object = getRCObject(uriInfo, expand, slug);
        Collection<RCObjectState> items = getRDbl().getRCObjectValueList(
            uriInfo,
            new ExpandMembers(RCObject.URI_FRAGMENT, expand),
            object, null, null);
        // CmTypedef cmTypedef = getDbl().find(
        // CmTypedef.class, RCClass.idFromSlug(idFieldValue));

        RCObjectStates ret = new RCObjectStates(uriInfo,
            //expand,
            object, items);
        return ResponsTypeUtil.parse(Response.ok(ret), responseType).build();
    }
}
