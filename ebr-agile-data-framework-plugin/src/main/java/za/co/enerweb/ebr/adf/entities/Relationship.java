package za.co.enerweb.ebr.adf.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = {"cmReltypedef", "id"})
@ToString(of = {"id", "cmReltypedef"})
@MappedSuperclass
public abstract class Relationship implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
    @JoinColumn(name = "RELTYPEDEF_ID",
        referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private CmReltypedef cmReltypedef;

    public Relationship(Integer id) {
        this.id = id;
    }

}
