package za.co.enerweb.ebr.adf.model;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface AdfHierarchyChild {

    /**
     * The Typedef for this instance
     */
    Class<? extends AdfHierarchyEntity> entity();

    /**
     * Should be less than 50 characters
     * @return
     */
    String key();

    /**
     * By default this will be derived from the key class
     * that this is annotating.
     * Should be less than 100 characters
     */
    String caption() default "";

    /**
     * Should be less than 400 characters
     * @return
     */
    String description() default "";

    /**
     * Weather this field should be fetched by default.
     * Lists are by default not fetched but all the rest are.
     * @return
     */
    AdfFetchType fetch() default AdfFetchType.DEFAULT;
}
