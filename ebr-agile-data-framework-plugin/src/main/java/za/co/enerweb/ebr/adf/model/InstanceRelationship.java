package za.co.enerweb.ebr.adf.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import za.co.enerweb.ebr.adf.entities.CmReltypedef;
import za.co.enerweb.ebr.adf.entities.IbInstance;

@Data
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
@AllArgsConstructor
public class InstanceRelationship extends InstanceRelationshipTypeDef {
    private static final long serialVersionUID = 1L;
    private IbInstance toInstance;

    public InstanceRelationship(IbInstance fromInstance,
        IbInstance toInstance, CmReltypedef relationshipType) {
        super(fromInstance, relationshipType);
        this.toInstance = toInstance;
    }

}
