package za.co.enerweb.ebr.adf.rest;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import lombok.val;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.adf.entities.CmUnitofmeasure;
import za.co.enerweb.ebr.adf.rest.model.RCUnitOfMeasure;
import za.co.enerweb.toolbox.rest.ExpandMembers;
import za.co.enerweb.toolbox.rest.ResponsTypeUtil;

@Path("/" + RCUnitOfMeasure.URI_FRAGMENT)
@Slf4j
public abstract class ARUnitOfMeasure extends ARConcept {

    @Path("/{idFieldValue}" + ResponsTypeUtil.PATH)
    @GET
    public Response get(@Context UriInfo uriInfo,
        @PathParam(ResponsTypeUtil.PARAM) String responseType,
        @QueryParam(ExpandMembers.PARAM) String expand,
        @NotNull @Size(min = 1)
        @PathParam("idFieldValue") String slug) {
        final String uomKey = RCUnitOfMeasure.uomkeyFromSlug(slug);

        CmUnitofmeasure o = getRDbl().findUnitOfMeasure(uomKey);

        val ret = getRDbl().getRCUnitOfMeasure(uriInfo,
            new ExpandMembers(RCUnitOfMeasure.URI_FRAGMENT, expand), o);

        return ResponsTypeUtil.parse(Response.ok(
            ret), responseType).build();
    }

}
