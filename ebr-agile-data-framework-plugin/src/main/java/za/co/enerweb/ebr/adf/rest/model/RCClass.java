package za.co.enerweb.ebr.adf.rest.model;

import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import za.co.enerweb.ebr.adf.entities.CmTypedef;
import za.co.enerweb.toolbox.rest.ExpandMembers;
import za.co.enerweb.toolbox.string.StringUtils;

@Data
@EqualsAndHashCode(callSuper = true)
@XmlRootElement(name = RCClass.URI_FRAGMENT)
@NoArgsConstructor
@AllArgsConstructor
public class RCClass extends ARCCmConcept<CmTypedef> {
    public final static String URI_FRAGMENT = "Class";

    private String caption;
    private String description;
    private RCProperties properties;

    public RCClass(UriInfo uriInfo, ExpandMembers expand, CmTypedef cmObject) {
        super(uriInfo, expand, cmObject);
        caption = cmObject.getCaption();

        if (isExpand()) {
            description = cmObject.getDescription();
        }
    }

    public String getUriFragment() {
        return URI_FRAGMENT;
    }

    public void validateNew() {
        validateFields("caption", "description");
    }

    /**
     * Derive a key from caption
     */
    public String generateKey() {
        return StringUtils.caption2VariableName(caption)
            .toUpperCase();
    }

    /**
     *
     * @param cmObject
     * @return eg. weathersa-1
     */
    protected String deriveSlug(CmTypedef cmObject) {
        return deriveSlugFromCmTypedef(cmObject);
    }

    public static String deriveSlugFromCmTypedef(CmTypedef cmObject) {
        return deriveSlugFromCmTypedef(cmObject.getId(), cmObject.getKey());
    }

    public static String deriveSlugFromCmTypedef(Integer id, String key) {
        return key.toLowerCase() + "-" + id;
    }

    public static String typeDefKeyFromSlug(String slug) {
        return keyFromSlug(slug);
    }
}
