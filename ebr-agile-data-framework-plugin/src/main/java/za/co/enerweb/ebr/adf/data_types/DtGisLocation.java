package za.co.enerweb.ebr.adf.data_types;

import org.apache.commons.lang.StringUtils;

import za.co.enerweb.ebr.adf.model.AdfScalarDataType;

public class DtGisLocation extends AdfScalarDataType<GisLocation> {

    public static final String DECIMAL_DEGREES =
        "Decimal degrees (DD) express latitude " +
            "and longitude geographic" +
            " coordinates as decimal fractions " +
            "and are used in many Geographic" +
            " Information Systems (GIS)";

    public String getDescription() {
        return "A geographical point. " + DECIMAL_DEGREES;
    }

    public Class<GisLocation> getJavaType() {
        return GisLocation.class;
    }

    public String toPersistedValue(GisLocation value) {
        return value.getLatitude() + "," + value.getLongitude();
    }

    public GisLocation fromPersistedValue(String persistedValue) {
        String[] tokens = StringUtils.split(persistedValue, ',');
        if (tokens.length != 2) {
            throw new IllegalArgumentException("Expected 2 decimal values "
                + "seperated by a comma, got: '" + persistedValue + "'");
        }
        return new GisLocation(Double.parseDouble(tokens[0]),
            Double.parseDouble(tokens[1]));
    }

}
