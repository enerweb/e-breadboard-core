package za.co.enerweb.ebr.adf.dbl;

/**
 * Convenient RuntimeException that will not roll back your transaction
 */
@javax.ejb.ApplicationException
public class AdfDbLayerException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public AdfDbLayerException() {
    }

    /**
     * @param message
     */
    public AdfDbLayerException(String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public AdfDbLayerException(Throwable cause) {
        super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public AdfDbLayerException(String message, Throwable cause) {
        super(message, cause);
    }

}
