package za.co.enerweb.ebr.adf.model;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import za.co.enerweb.ebr.adf.dbl.AdfDbLayerUtils;
import za.co.enerweb.toolbox.string.StringUtils;

/**
 * Extend this to make a object definition of your Adf Entity.
 * It must have a no args constructor.
 * The convention is to prefix it with E eg. EMyEntity
 */
@Getter
// can make it @Getter(lazy=true) if needed
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public abstract class AdfEntity {
    private static final Pattern prefixPattern = Pattern.compile(
        "^(E)([A-Z].*)");

    String key = deriveKey();

    String caption = deriveCaption();

    protected String deriveKey() {
        return deriveKey(getClass());
    }

    protected static String deriveKey(Class<? extends AdfEntity> klass) {
        String ret = klass.getSimpleName();
        Matcher matcher = prefixPattern.matcher(ret);
        if (matcher.matches()) {
            ret = matcher.group(2);
        } else {
            throw new IllegalStateException("Class name does not " +
                "follow the entity name convention " +
                "(prefix it with E eg. EMyEntity) : " + ret);
        }
        return StringUtils.camelCaseToUnderscoreSeparated(ret).toUpperCase();
    }

    private String deriveCaption() {
        return StringUtils.variableName2Title(getKey());
    }

    public abstract EntityType getEntityType();

    public String getJpaInstanceEntity() {
        return AdfDbLayerUtils.efsEntityToJpaInstanceEntity(key);
    }

    public String getJpaParmameterEntity() {
        return AdfDbLayerUtils.efsEntityToJpaParmameterEntity(key);
    }

    public String getJpaParmameterInstanceId() {
        return AdfDbLayerUtils.efsEntityToJpaParmameterInstanceId(key);
    }

}
