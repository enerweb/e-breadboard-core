package za.co.enerweb.ebr.adf.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

// this sounds more like a type of a user visible generic config
// maybe we should write that and let subprojects use that..

@Data
@EqualsAndHashCode(of="module")
@Entity
@Table(name = "CM_SIMULATION")
@NamedQueries({
    @NamedQuery(name = "CmSimulation.findAll", query = "SELECT c FROM CmSimulation c"),
    @NamedQuery(name = "CmSimulation.findByModule", query = "SELECT c FROM CmSimulation c WHERE c.module = :module"),
    @NamedQuery(name = "CmSimulation.findByStartDatetime", query = "SELECT c FROM CmSimulation c WHERE c.startDatetime = :startDatetime"),
    @NamedQuery(name = "CmSimulation.findByEndDatetime", query = "SELECT c FROM CmSimulation c WHERE c.endDatetime = :endDatetime"),
    @NamedQuery(name = "CmSimulation.findByIterations", query = "SELECT c FROM CmSimulation c WHERE c.iterations = :iterations"),
    @NamedQuery(name = "CmSimulation.findBySeed", query = "SELECT c FROM CmSimulation c WHERE c.seed = :seed"),
    @NamedQuery(name = "CmSimulation.findByTimestamp", query = "SELECT c FROM CmSimulation c WHERE c.timestamp = :timestamp")})
public class CmSimulation implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * the global simulation config
     */
    public static String GLOBAL = "GLOBAL";

    @Id
    @Basic(optional = false)
    @Column(name = "MODULE", nullable = false, length = 100)
    private String module;
    @Basic(optional = false)
    @Column(name = "START_DATETIME", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDatetime;
    @Basic(optional = false)
    @Column(name = "END_DATETIME", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDatetime;
    @Basic(optional = false)
    @Column(name = "ITERATIONS", nullable = false)
    private int iterations;
    @Basic(optional = false)
    @Column(name = "SEED", nullable = false)
    private int seed;

    // this should not be text, but anyways
    @Basic(optional = false)
    @Column(name = "TIMESTAMP", nullable = false, length = 25)
    private String timestamp;
    @Column(name = "interval", nullable = false, length = 25)
    private String interval;

    public CmSimulation() {
    }

    public CmSimulation(String module) {
        this.module = module;
    }

    public CmSimulation(String module, Date startDatetime, Date endDatetime,
        int iterations, int seed, String timestamp) {
        this.module = module;
        this.startDatetime = startDatetime;
        this.endDatetime = endDatetime;
        this.iterations = iterations;
        this.seed = seed;
        this.timestamp = timestamp;
    }

    @PrePersist
    public void updateTimeStamp() {
        timestamp = ISODateTimeFormat.basicDateTime().print(new DateTime());
    }

}
