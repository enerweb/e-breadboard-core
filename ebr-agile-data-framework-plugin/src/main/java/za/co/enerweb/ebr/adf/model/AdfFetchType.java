package za.co.enerweb.ebr.adf.model;

public enum AdfFetchType {
    EAGER, LAZY, DEFAULT;
    // maybe add a NEVER, for the cases where we need to define it,
    // but don't even wan't to load it when the caller is eager..

    public boolean isEager(boolean isList) {
        switch (this) {
        case EAGER:
            return true;
        case LAZY:
            return false;
        }
        return !isList;
    }
}
