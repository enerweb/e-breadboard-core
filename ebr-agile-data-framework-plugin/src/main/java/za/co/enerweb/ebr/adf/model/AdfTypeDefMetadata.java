package za.co.enerweb.ebr.adf.model;

import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.StringUtils.isNotBlank;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.ToString;
import lombok.val;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.toolbox.string.StringUtils;

/**
 * Extend this to make a object definition of your Adf Type.
 * It must have a no args constructor.
 * The convention is to prefix it with T eg. TMyType
 */
@Slf4j
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(of = {"key"})
@ToString(of = {"caption", "key"}, includeFieldNames = false)
// can make it @Getter(lazy=true) if needed
@FieldDefaults(level = AccessLevel.PRIVATE)
public final class AdfTypeDefMetadata {

    private static final Pattern prefixPattern = Pattern.compile(
        "^(T)([A-Z].*)");

    String key;
    String caption;
    String description = "";
    boolean abstractSupertype = false;
    Class<? extends AdfInstanceEntity> entityClass;
    Class<? extends AdfInstance> instanceClass;
    // String dynamicParamsFieldName = null;

    /**
     * Key => AdfParameterDefMetadata
     */
    Map<String, AdfParameterDefMetadata> parameters =
        new HashMap<String, AdfParameterDefMetadata>();

    List<AdfRelationshipFieldMetadata> relationships =
        new ArrayList<AdfRelationshipFieldMetadata>();

    // map to force unique key entries..
    Map<String, AdfHierarchyFieldMetadata> hierarchyMap =
        new HashMap<String, AdfHierarchyFieldMetadata>();

    public AdfTypeDefMetadata(String key, String caption, String description,
        boolean abstractSupertype) {
        this.key = key;
        this.caption = caption;
        this.description = description;
        this.abstractSupertype = abstractSupertype;
    }

    public AdfTypeDefMetadata(String key, String caption, String description,
        boolean abstractSupertype,
        Class<? extends AdfInstanceEntity> entityClass,
        Class<? extends AdfInstance> instanceClass) {
        this.key = key;
        this.caption = caption;
        this.description = description;
        this.abstractSupertype = abstractSupertype;
        this.entityClass = entityClass;
        this.instanceClass = instanceClass;
    }

    public AdfParameterDefMetadata findParameterByParameterKey(
        String paramKey) {
        return parameters.get(paramKey);
    }

    public AdfParameterDefMetadata findParameterByFieldName(String fieldName) {
        for (AdfParameterDefMetadata p : parameters.values()) {
            String fn = p.getFieldName();
            if (fn != null && fn.equals(fieldName)) {
                return p;
            }
        }
        return null;
    }

    public AdfHierarchyFieldMetadata findAdfHierarchyFieldByFieldName(
        String fieldName) {
        for (AdfHierarchyFieldMetadata h : hierarchyMap.values()) {
            if (h.getFieldName().equals(fieldName)) {
                return h;
            }
        }
        return null;
    }

    protected static String deriveKey(Class<? extends AdfInstance> klass) {
        String ret = klass.getSimpleName();
        Matcher matcher = prefixPattern.matcher(ret);
        if (matcher.matches()) {
            ret = matcher.group(2);
        } else {
            throw new IllegalStateException("Class name does not " +
                "follow the typedef name convention " +
                "(prefix it with T eg. TMyInstance) : " + ret);
        }
        return StringUtils.camelCaseToUnderscoreSeparated(ret).toUpperCase();
    }

    protected static String deriveCaption(String key) {
        return StringUtils.variableName2Title(key);
    }

    public AdfTypeDefMetadata init(AdfInstanceTypeDef adfInstanceTypeDef,
        Class<? extends AdfInstance> instanceClass) {
        init(adfInstanceTypeDef.key(), adfInstanceTypeDef.caption(),
            adfInstanceTypeDef.description(),
            adfInstanceTypeDef.entity(), instanceClass);
        return this;
    }

    public AdfTypeDefMetadata init(String key,
        String caption,
        String description,
        Class<? extends AdfInstanceEntity> entityClass,
        Class<? extends AdfInstance> instanceClass) {
        this.instanceClass = instanceClass;
        if (isBlank(key)) {
            key = deriveKey(instanceClass);
        }
        this.key = key;

        if (isBlank(caption)) {
            caption = deriveCaption(key);
        }
        this.caption = caption;

        this.description = description;
        this.entityClass = entityClass;
        return this;
    }

    public void addParamDef(Field field, AdfParamDef paramDef) {
        AdfParameterDefMetadata p = new AdfParameterDefMetadata();

        String paramKey = paramDef.key();

        if (parameters.containsKey(paramKey)) {
            log.warn("Parameter is being re-defined: " + paramKey
                + "\n " + this);
            return;
        }

        p.setFieldName(field.getName());

        p.setKey(paramKey);
        if (isBlank(paramKey)) {
            p.setKey(
                StringUtils.variableName2TableName(field.getName()));
        }
        p.setList(field.getType().equals(List.class));
        p.setCaption(paramDef.caption());
        if (isBlank(p.getCaption())) {
            p.setCaption(deriveCaption(p.getKey()));
        }
        p.setDescription(paramDef.description());
        // TODO: p.setcmUnitofmeasure(paramDef.cmUnitofmeasure());
        p.setParameterKind(paramDef.paramkind());
        p.setTypeClass(paramDef.type());
        p.setIndexTypeClass(paramDef.indexType());
        p.setParamTable(paramDef.paramTable());
        addParamDef(p);
    }

    public void addParamDef(AdfParameterDefMetadata p) {
        parameters.put(p.getKey(), p);
    }

    @SneakyThrows
    public AdfInstance newAdfInstance() {
        return getInstanceClass().newInstance();
    }


    public void addRelationship(Field field,
        AdfRelationshipTypeDefMetadata relTypeDef,
        AdfTypeDefMetadata typeDef, boolean isToReference) {
        AdfRelationshipFieldMetadata r = new AdfRelationshipFieldMetadata();
        r.setFieldName(field.getName());
        r.setRelTypeDef(relTypeDef);
        r.setTypeDef(typeDef);
        r.setToReference(isToReference);
        relationships.add(r);
    }

    public void setRelationshipTypeDef(
        AdfRelationshipTypeDefMetadata relTypeDef,
        AdfTypeDefMetadata typeDef, boolean isToReference) {
        // find relationship with correct key,set it's field metadata.

        for (val typeRelationship : relationships) {
            if (typeRelationship.getKey().equals(relTypeDef.getKey())) {
                if (typeRelationship.isToReference() == isToReference) {
                    typeRelationship.setTypeDef(typeDef);
                }
            }
        }
    }

    public void addHierarchy(Field field,
        AdfHierarchyTypeDefMetadata hierTypeDef,
        boolean isChildReference, AdfFetchType adfFetchType,
        String uniqueFieldPerParent) {
        AdfHierarchyFieldMetadata h = new AdfHierarchyFieldMetadata();
        h.setFieldName(field.getName());
        h.setHierTypeDef(hierTypeDef);
        h.setChildReference(isChildReference);
        boolean isList = field.getType().equals(List.class);
        h.setList(isList);
        h.setEager(adfFetchType.isEager(isList));
        if (isNotBlank(uniqueFieldPerParent)) {
            h.setUniqueFieldPerParent(uniqueFieldPerParent);
        }
        hierarchyMap.put(h.getKey(), h);
    }

    public Collection<AdfHierarchyFieldMetadata> getHierarchies() {
        return hierarchyMap.values();
    }
}
