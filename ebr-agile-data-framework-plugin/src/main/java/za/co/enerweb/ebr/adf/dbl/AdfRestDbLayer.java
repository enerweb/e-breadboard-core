
package za.co.enerweb.ebr.adf.dbl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.persistence.Query;
import javax.ws.rs.core.UriInfo;

import lombok.val;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.adf.entities.CmEntity;
import za.co.enerweb.ebr.adf.entities.CmParamdef;
import za.co.enerweb.ebr.adf.entities.CmTypedef;
import za.co.enerweb.ebr.adf.entities.CmUnitofmeasure;
import za.co.enerweb.ebr.adf.entities.IbInstance;
import za.co.enerweb.ebr.adf.model.EntityType;
import za.co.enerweb.ebr.adf.rest.model.RCClass;
import za.co.enerweb.ebr.adf.rest.model.RCClasses;
import za.co.enerweb.ebr.adf.rest.model.RCObject;
import za.co.enerweb.ebr.adf.rest.model.RCObjectState;
import za.co.enerweb.ebr.adf.rest.model.RCObjects;
import za.co.enerweb.ebr.adf.rest.model.RCProperties;
import za.co.enerweb.ebr.adf.rest.model.RCProperty;
import za.co.enerweb.ebr.adf.rest.model.RCPropertyValue;
import za.co.enerweb.ebr.adf.rest.model.RCUnitOfMeasure;
import za.co.enerweb.ebr.adf.rest.model.RCUnitOfMeasures;
import za.co.enerweb.toolbox.rest.ExpandMembers;

@Stateful
@LocalBean
@Slf4j
public class AdfRestDbLayer extends AdfDbLayer {

    public RCClass getRCClass(UriInfo uriInfo, ExpandMembers expand,
        Integer id) {
        CmTypedef cmTypedef = find(
            CmTypedef.class, id);
        return getRCClass(uriInfo, expand, cmTypedef);
    }

    public RCClass getRCClass(UriInfo uriInfo, ExpandMembers expand,
        CmTypedef cmTypedef) {
        expand.enter(RCClass.class);
        final RCClass ret = new RCClass(uriInfo, expand, cmTypedef);
        if (ret.isExpand()) {
            ret.setProperties(getRCProperties(uriInfo, expand,
                cmTypedef.getCmParamdefs()));
        }
        expand.exit(RCClass.class);
        return ret;
    }

    public RCClasses getAllRCClasses(UriInfo uriInfo, ExpandMembers expand) {
        List<RCClass> ret = new ArrayList<RCClass>();
        val all = findAll(CmTypedef.class);
        for (CmTypedef x : all) {
            ret.add(getRCClass(uriInfo, expand, x));
        }
        return new RCClasses(uriInfo, ret);
    }


    public RCClass createRCCLass(UriInfo uriInfo, RCClass newRC) {
        newRC.validateNew();
        String key = newRC.generateKey();
        String entityKey = key;
        for (int i = 1; find(CmEntity.class, entityKey) != null; i++) {
            entityKey = key + i;
        }
        CmEntity cmEntity = getOrCreateCmEntity(entityKey,
            newRC.getCaption(), EntityType.INSTANCE);

        CmTypedef cmObject =
            save(new CmTypedef(cmEntity, key, newRC.getCaption(),
                newRC.getDescription()));
        newRC.setCmObject(cmObject);
        newRC.initMeta(uriInfo);
        return newRC;
    }

    public RCObjects getRCObjectsOfClass(
        UriInfo uriInfo, ExpandMembers expand,
        String typeDefKey) {
        List<RCObject> ret = new ArrayList<RCObject>();
        for (IbInstance x : findInstancesByType(typeDefKey)) {
            ret.add(getRCObject(uriInfo, expand, x));
        }
        return new RCObjects(uriInfo, ret);
    }

    public RCObjects getAllRCObjects(UriInfo uriInfo,
        ExpandMembers expand) {
        List<RCObject> ret = new ArrayList<RCObject>();
        for (CmTypedef typDef : findAll(CmTypedef.class)) {
            for (IbInstance x : findInstancesByType(typDef)) {
                ret.add(getRCObject(uriInfo, expand, x));
            }
        }
        return new RCObjects(uriInfo, ret);
    }


    public RCObject getRCObject(UriInfo uriInfo, ExpandMembers expand,
        String typeDefKey, Integer instanceId) {
        return getRCObject(uriInfo, expand,
            findInstance(typeDefKey, instanceId));
    }

    public RCObject getRCObject(UriInfo uriInfo, ExpandMembers expand,
        IbInstance instance) {
        expand.enter(RCObject.class);
        final RCObject ret = new RCObject(uriInfo, expand, instance);
        expand.exit(RCObject.class);
        return ret;
    }

    public RCProperty getRCProperty(UriInfo uriInfo, ExpandMembers expand,
        CmParamdef cmParamdef) {
        expand.enter(RCProperty.class);
        RCProperty ret = new RCProperty(uriInfo, expand, cmParamdef);
        if (ret.isExpand()) {
            CmUnitofmeasure cmUnitofmeasure = cmParamdef.getCmUnitofmeasure();
            if (cmUnitofmeasure != null) {
                ret.setUnitOfMeasure(getRCUnitOfMeasure(uriInfo, expand,
                    cmUnitofmeasure));
            }
        }
        expand.exit(RCProperty.class);
        return ret;
    }

    public Collection<RCProperty> getRCPropertiesList(UriInfo uriInfo,
        ExpandMembers expand,
        final Collection<CmParamdef> all) {
        val ret = new HashSet<RCProperty>();
        for (CmParamdef x : all) {
            ret.add(new RCProperty(uriInfo, expand, x
        ));
        }
        return ret;
    }

    public RCProperties getRCProperties(UriInfo uriInfo, ExpandMembers expand,
        final Collection<CmParamdef> all) {
        return new RCProperties(uriInfo, getRCPropertiesList(uriInfo, expand, all));
    }

    public RCProperties getAllRCProperties(UriInfo uriInfo, ExpandMembers expand) {
        return getRCProperties(uriInfo, expand, findAll(CmParamdef.class));

//        val ret = new ArrayList<RProperty>();
//        for (CmParamdef x : findAll(CmParamdef.class)) {
//            ret.add(getRProperty(uriInfo, expand, x));
//        }
//        return new RCProperties(uriInfo, ret);
    }

    public RCUnitOfMeasure getRCUnitOfMeasure(UriInfo uriInfo,
        ExpandMembers expand,
        CmUnitofmeasure o) {
        expand.enter(RCUnitOfMeasure.class);
        final RCUnitOfMeasure ret = new RCUnitOfMeasure(uriInfo, expand, o);
        expand.exit(RCUnitOfMeasure.class);
        return ret;
    }

    public Collection<RCUnitOfMeasure> getRCUnitOfMeasuresList(UriInfo uriInfo,
        ExpandMembers expand,
        final Collection<CmUnitofmeasure> all) {
        val ret = new HashSet<RCUnitOfMeasure>();
        for (CmUnitofmeasure x : all) {
            ret.add(new RCUnitOfMeasure(uriInfo, expand, x
                ));
        }
        return ret;
    }

    public RCUnitOfMeasures getRCUnitOfMeasures(UriInfo uriInfo,
        ExpandMembers expand,
        final Collection<CmUnitofmeasure> all) {
        return new RCUnitOfMeasures(uriInfo, getRCUnitOfMeasuresList(uriInfo,
            expand, all));
    }

    public RCUnitOfMeasures getAllRCUnitOfMeasures(UriInfo uriInfo,
        ExpandMembers expand) {
        return getRCUnitOfMeasures(uriInfo, expand,
            findAll(CmUnitofmeasure.class));
    }

    public Collection<RCObjectState> getRCObjectValueList(UriInfo uriInfo,
        ExpandMembers expandMembers, RCObject object,
        Date inclusiveStartDate, Date exclusiveEndDate) {
        // for each applicable date in param table we create one RCObjectState
        String startDateFilter = "";
        IbInstance ibInstance = object.getCmObject();
        CmTypedef cmTypedef = ibInstance.getCmTypedef();

        String entity = cmTypedef.getJpaParameterEntity();
        String parentIdProperty = cmTypedef.getParamInstanceProperty();

        if (inclusiveStartDate != null) {
            startDateFilter = " AND x.appldatetime >= :inclusiveStartDate";
        }
        String endDateFilter = "";
        if (exclusiveEndDate != null) {
            endDateFilter = " AND x.appldatetime < :exclusiveEndDate";
        }
        Query q = em.createQuery(
            "SELECT x.appldatetime, x.key, x.indx, x.val FROM "
                + entity + " x "
                + "WHERE x." + parentIdProperty
                + " = :parent "
                // + "AND x.cmParamdef = :cmParamdef "
                + startDateFilter
                + endDateFilter
                + " ORDER BY x.appldatetime, x.key "
            );
        q.setParameter("parent", ibInstance);
        // q.setParameter("cmParamdef", cmParamdef);
        if (inclusiveStartDate != null) {
            q.setParameter("inclusiveStartDate", inclusiveStartDate);
        }
        if (exclusiveEndDate != null) {
            q.setParameter("exclusiveEndDate", exclusiveEndDate);
        }

        List<Object[]> resultList = q.getResultList();
        val dateMap = new LinkedHashMap<Date, RCObjectState>();
        // val ret = new ArrayList<RCObjectState>(resultList.size());
        // AdfDataType<T> adfDataType = getAdfDataType(parent, cmParamdef);
        for (Object[] row : resultList) {
            Date applDateTime = (Date) row[0];
            RCObjectState state = dateMap.get(applDateTime);
            if (state == null) {
                state = new RCObjectState();
                state.setApplDateTime(applDateTime);
                dateMap.put(applDateTime, state);
            }

            String key = (String) row[1];
            String index = (String) row[2];
            String value = (String) row[3]; // adfDataType.fromPersistedValue ??
            // add value
            RCPropertyValue pv = new RCPropertyValue(
                RCProperty.deriveSlugFromCmParamdef(cmTypedef, key),
                value, index);
            state.addPropertyValue(pv);
        }
        // TODO: maybe sort multiple values in each state according to index
        // type
        // if applicable
        return dateMap.values();
    }

}
