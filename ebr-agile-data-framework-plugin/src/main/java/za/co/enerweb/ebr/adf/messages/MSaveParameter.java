package za.co.enerweb.ebr.adf.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import za.co.enerweb.ebr.adf.entities.CmParamdef;
import za.co.enerweb.ebr.adf.entities.IbInstance;
import za.co.enerweb.ebr.gui.vaadin.messaging.Message;

@Data
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
public class MSaveParameter extends Message {
    private IbInstance ibInstance;
    private CmParamdef cmParamdef;
}
