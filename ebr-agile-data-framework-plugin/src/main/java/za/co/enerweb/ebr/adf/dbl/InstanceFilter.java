package za.co.enerweb.ebr.adf.dbl;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import za.co.enerweb.ebr.adf.model.AdfInstance;

@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@Accessors(fluent = true, chain = true)
public class InstanceFilter extends FieldFilter {
    Class<? extends AdfInstance> instanceClass;

    public InstanceFilter(Class<? extends AdfInstance> instanceClass,
        String fieldName, Object value) {
        super(fieldName, value);
        this.instanceClass = instanceClass;
    }

    public InstanceFilter fieldName(String fieldName) {
        super.fieldName(fieldName);
        return this;
    }

    public InstanceFilter value(Object value) {
        super.value(value);
        return this;
    }
}
