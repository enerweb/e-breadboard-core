package za.co.enerweb.ebr.adf.rest.model;

import java.util.List;

import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import lombok.NoArgsConstructor;
import za.co.enerweb.toolbox.rest.Items;
import za.co.enerweb.toolbox.rest.Meta;

@XmlRootElement(name = RCObjects.URI_FRAGMENT)
// @XmlType()
@XmlSeeAlso({RCObject.class, Items.class})
@NoArgsConstructor
public class RCObjects extends Items<RCObject> {
    public final static String URI_FRAGMENT = "Objects";

    public RCObjects(Meta meta, List<RCObject> items) {
        super(meta, items);
    }

    public RCObjects(UriInfo uriInfo, List<RCObject> items) {
        this(
            new AdfMetaBuilder(uriInfo).add(URI_FRAGMENT).build(),
            items);
    }
}
