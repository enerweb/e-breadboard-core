package za.co.enerweb.ebr.adf;

import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.adf.dbl.AdfDbLayer;
import za.co.enerweb.ebr.adf.dbl.AdfRestDbLayer;
import za.co.enerweb.ebr.server.Registrar;
import za.co.enerweb.toolbox.timing.Stopwatch;


/**
 * Factories are registered by their package
 */
@Singleton
@LocalBean
@Slf4j
public class AdfFactoryRegistry {

    private Map<String, AdfFactory> regsistry;

    @PostConstruct
    public void refreshRegistry() {
        regsistry = new HashMap<String, AdfFactory>();
        Stopwatch sw = new Stopwatch();

        Set<Class<? extends AdfFactory>> factories =
            Registrar.getReflections().getSubTypesOf(AdfFactory.class);

        // if (converters.size() == 0) {
        // log.debug("Could not find any ITypeConverters.");
        // } else {
        // log.debug("Found " + converters.size() + " ITypeConverters");
        // }
        for (Class<? extends AdfFactory> factoryClass : factories) {
            // only consider non-abstract subclasses
            if (!Modifier.isAbstract(factoryClass.getModifiers())) {
                try {
                    // log.debug("registering: " + converterClass);
                    AdfFactory factory = factoryClass.newInstance();
                    regsistry.put(factory.getClass().getPackage().getName(),
                        factory);
                } catch (IllegalAccessException e) {
                    log.warn("Not registering AdfFactory {} " +
                        "(because we don't have access)",
                        factoryClass.getName());
                } catch (Exception e) {
                    log.error("Could not register a AdfFactory from: "
                        + factoryClass.getName()
                        + " (Maybe it needs a no-parameter constructor)", e);
                }
            } else {
                log.debug(
                    "Ignoring abstract " + factoryClass.getName());
            }
        }
        log.debug("Registering AdfFactories took {} ms", sw
            .getTotalMilliSeconds());
    }

    /**
     * will find the first factory that is in a parent package of someClass eg.
     * com.example.MyFactory will be returned for
     * com.example.rest.whatever.SomeClass
     * @param someClass
     * @return
     */
    public AdfFactory getAdfFactory(Class<?> someClass) {
        String somePackage = someClass.getPackage().getName();
        for (Entry<String, AdfFactory> entry : regsistry.entrySet()) {
            if (somePackage.startsWith(entry.getKey())) {
                // log.debug("For someClass: " + someClass);
                // log.debug("Using factory: " + entry.getValue().getClass());
                return entry.getValue();
            }
        }
        for (Entry<String, AdfFactory> entry : regsistry.entrySet()) {
            log.debug(entry.getKey());
        }
        throw new NoSuchElementException(
            "Could not find a suitable factory for " + someClass);
    }

    public AdfDbLayer getAdfDbLayer(Class<?> someClass) {
        return getAdfFactory(someClass).getAdfDbLayer();
    }

    public AdfRestDbLayer getAdfRestDbLayer(Class<?> someClass) {
        return getAdfFactory(someClass).getAdfRestDbLayer();
    }
}
