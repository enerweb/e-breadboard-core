package za.co.enerweb.ebr.adf.model;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import za.co.enerweb.ebr.adf.data_types.DtNull;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface AdfParamDef {
//    private static final Class<? extends AdfDataType<?>> defaultAdfDataType
//        = AdfDataType<Object>;

    /**
     * The Typedef for this instance.
     * TODO: Maybe we can auto detect this in most cases?!!
     */
    Class<? extends AdfDataType<?>> type();

    Class<? extends AdfDataType<?>> indexType() default DtNull.class;

    String key() default "";

    String caption() default "";

    String description() default "";

    /**
     * TODO: we should implement something proper for unit of measures,
     * maybe it should work like data Types
     */
    // @Deprecated
    // String unitOfMeasureKey() default "";

    ParameterKind paramkind() default ParameterKind.INPUT;

    String paramTable() default "";
}
