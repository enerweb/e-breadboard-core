package za.co.enerweb.ebr.adf.model;

import static org.apache.commons.lang.StringUtils.isBlank;

import java.util.regex.Pattern;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.toolbox.string.StringUtils;

/**
 * Extend this to make a object definition of your Adf Relationship Type.
 * It must have a no args constructor.
 * The convention is to prefix it with T eg. TMyType
 */
@Slf4j
@Data
// can make it @Getter(lazy=true) if needed
@FieldDefaults(level = AccessLevel.PRIVATE)
public final class AdfHierarchyTypeDefMetadata {

    private static final Pattern prefixPattern = Pattern.compile(
        "^(H)([A-Z].*)");

    String key;
    String caption;
    String description = "";
    Class<? extends AdfHierarchyEntity> entityClass;

    protected static String deriveCaption(String key) {
        return StringUtils.variableName2Title(key);
    }

    public AdfHierarchyTypeDefMetadata(String key) {
        this.key = key;
    }

    public void init(String caption, String description,
        Class<? extends AdfHierarchyEntity> entityClass) {
        if (this.entityClass == null) {
            this.entityClass = entityClass;
        } else if (!this.entityClass.equals(entityClass)) {
            throw new IllegalStateException("Relationship with key='" +
                key
                + "' is being more than once with different entity classes. " +
                "\n  " + entityClass
                + "\n  " + this.entityClass);
        }
        if (isBlank(caption)) {
            caption = deriveCaption(key);
        }
        this.caption = caption;

        if (!isBlank(description)) {
            this.description = description;
        }
    }

    public String toString() {
        return key;
    }
}
