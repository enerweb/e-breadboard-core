package za.co.enerweb.ebr.adf.rest.model;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@XmlRootElement(name = RCPropertyValue.URI_FRAGMENT)
@NoArgsConstructor
@AllArgsConstructor
public class RCPropertyValue extends ARCConcept {
    public final static String URI_FRAGMENT = "PropertyValue";

    // ID APPLDATETIME TYPEDEF_ID KEY VAL INDX COALSUPPLY_ID
    // 178302 2011-04-01 00:00:00.0 12 DELIVERY_14 0.0 null 4068

    private String propertyId;

    // TODO: figure out if we want to to distinguish value types and only quote
    // them if applicable. can one do raw json values?
    private String value;

    // TODO: figure out if we want to to distinguish value types and only quote
    // them if applicable. can one do raw json values?
    private String index;

}
