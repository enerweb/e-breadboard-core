package za.co.enerweb.ebr.adf.ui;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import za.co.enerweb.toolbox.vaadin.messaging.Message;

@Data
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
public class MFactoryAndEntitySelected extends Message {
    private Class<?> factoryClass;
    private Class<?> entityClass;
}
