package za.co.enerweb.ebr.adf.rest.model;

import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import za.co.enerweb.ebr.adf.entities.CmUnitofmeasure;
import za.co.enerweb.toolbox.rest.ExpandMembers;

@Data
@EqualsAndHashCode(callSuper = true)
@XmlRootElement(name = RCUnitOfMeasure.URI_FRAGMENT)
@NoArgsConstructor
@AllArgsConstructor
public class RCUnitOfMeasure extends ARCCmConcept<CmUnitofmeasure> {

    public final static String URI_FRAGMENT = "UnitOfMeasure";

    private String caption;
    private String description;

    public RCUnitOfMeasure(UriInfo uriInfo, ExpandMembers expand,
        CmUnitofmeasure cmObject) {
        super(uriInfo, expand, cmObject);
        if (cmObject != null) {
            caption = cmObject.getCaption();
            if (isExpand()) {
                description = cmObject.getDescription();
            }
        }
    }

    public String getUriFragment() {
        return URI_FRAGMENT;
    }

    protected String deriveSlug(CmUnitofmeasure cmObject) {
        // if (cmObject == null) {
        // return "null";
        // }
        return // caption2VariableName(cmObject.getCaption()).toLowerCase() +
               // "-"
        // +
        cmObject.getKey().toLowerCase();
    }

    public static String uomkeyFromSlug(String slug) {
        return // splitOnDashes(
        slug // )[1]
        .toUpperCase();
    }
}
