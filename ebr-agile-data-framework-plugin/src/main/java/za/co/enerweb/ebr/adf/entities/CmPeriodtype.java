package za.co.enerweb.ebr.adf.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(of="key")
@ToString(of="key")
@Entity
@Table(name = "CM_PERIODTYPE")
@NamedQueries({
    @NamedQuery(name = "CmPeriodtype.findAll", query = "SELECT c FROM CmPeriodtype c"),
    @NamedQuery(name = "CmPeriodtype.findByKey", query = "SELECT c FROM CmPeriodtype c WHERE c.key = :key"),
    @NamedQuery(name = "CmPeriodtype.findByCaption", query = "SELECT c FROM CmPeriodtype c WHERE c.caption = :caption"),
    @NamedQuery(name = "CmPeriodtype.findByDefinition", query = "SELECT c FROM CmPeriodtype c WHERE c.definition = :definition")})
public class CmPeriodtype implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "KEY", nullable = false, length = 25)
    private String key;
    @Basic(optional = false)
    @Column(name = "CAPTION", nullable = false, length = 100)
    private String caption;
    @Basic(optional = false)
    @Column(name = "DEFINITION", nullable = false, length = 1000)
    private String definition;

    public CmPeriodtype() {
    }

    public CmPeriodtype(String key) {
        this.key = key;
    }

    public CmPeriodtype(String key, String caption, String definition) {
        this.key = key;
        this.caption = caption;
        this.definition = definition;
    }

}
