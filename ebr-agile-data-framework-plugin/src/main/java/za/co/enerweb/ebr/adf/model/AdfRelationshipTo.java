package za.co.enerweb.ebr.adf.model;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface AdfRelationshipTo {

    /**
     * The Typedef for this instance
     */
    Class<? extends AdfRelationshipEntity> entity();

    String key();

    /**
     * By default this will be derived from the key class
     * that this is annotating.
     */
    String caption() default "";

    String description() default "";
}
