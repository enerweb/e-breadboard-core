package za.co.enerweb.ebr.adf.model;

import lombok.Getter;

/**
 * This needs to be manually kept in sync with the db.
 * I suppose we can check compliance during unit tests.
 */
public enum ParameterKind {
    INPUT(false, true),
    INPUT_DENORM(true, true),
    RESULT(false, false),
    RESULT_DENORM(true, false),
    /**
     * @deprecated We should now use RESULT_DENORM
     *  Use isDenorm to easily figure out if it is denorm or not.
     *  We should add a migration script to change it over.
     */
    @Deprecated
    DENORM(true, false)
    ;

    @Getter
    boolean denorm;
    @Getter
    boolean input;

    private ParameterKind(boolean isDenorm, boolean input) {
        this.denorm = isDenorm;
        this.input = input;
    }

    public boolean isResult() {
        return !input;
    }
}
