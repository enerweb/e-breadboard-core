package za.co.enerweb.ebr.adf.model.timeseries;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import com.google.common.collect.Iterators;

/**
 * XXX: This class is not thread safe,
 * don't change stuff while we are iterating..
 * we can set a flag to check for this
 */
public class CompositeTimeSeries
// partially implements List<ITimeSeries<?>>
// partially implements ITimeSeries<Object[]>
{

    private List<ITimeSeries<?>> timeSerii = new ArrayList<ITimeSeries<?>>();
    private Date firstDate = null;
    private Date lastDate = null;

    private void updateDateRange() {
        for (ITimeSeries<?> timeSeries : timeSerii) {
            if (timeSeries.isEmpty()) {
                continue;
            }
            Date first = timeSeries.getFirstDate();
            if (firstDate == null || first.before(firstDate)) {
                firstDate = first;
            }
            Date last = timeSeries.getLastDate();
            if (lastDate == null || last.after(lastDate)) {
                lastDate = last;
            }
            // System.out.println("firstDate: " + firstDate);
            // System.out.println("lastDate: " + lastDate);
        }
    }

    /**
     * Backed by the collection.
     * XXX: Maybe this should give out Map.Entity<Date,?> values
     */
    public Iterable<ITimeSeriesItem<Object[]>> sparseIterable() {
        return new Iterable<ITimeSeriesItem<Object[]>>() {
            @Override
            public Iterator<ITimeSeriesItem<Object[]>> iterator() {
                return sparseIterator();
            }
        };
    }

    /**
     * Backed by the collection.
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    protected Iterator<ITimeSeriesItem<Object[]>> sparseIterator() {
        updateDateRange();
        if (firstDate == null) {
            // no contained timeseries has any values
            return Iterators.emptyIterator();
        }

        return new Iterator<ITimeSeriesItem<Object[]>>() {
            // keep track weather we iterated the last date or not, whihc
            // is very tricky when there is only one value in the series..
            private boolean iteratedLastDate = false;

            private Map.Entry<Date, ?>[] tsCurEntity = new Map.Entry[timeSerii
                .size()];
            private Iterator<Map.Entry<Date, ?>>[] tsIterators =
                new Iterator[timeSerii.size()];
            private Date curDate = firstDate;
            {
                int i = 0;
                // init iterators
                for (ITimeSeries<?> timeSeries : timeSerii) {
                    Iterator<Map.Entry<Date, ?>> tsi = tsIterators[i] =
                        (Iterator) timeSeries.entrySet().iterator();

                    // next all
                    // to get curr positions
                    nextEntry(i);
                    i++;
                }
            }

            /**
             * Move the iterator forward
             */
            private void nextEntry(int i) {
                if (tsIterators[i].hasNext()) {
                    tsCurEntity[i] = tsIterators[i].next();
                } else {
                    tsCurEntity[i] = null;
                    tsIterators[i] = null;
                }
            }


            @Override
            public boolean hasNext() {
                return curDate.before(lastDate)
                    || (curDate.equals(lastDate) && !iteratedLastDate)
                    ;
            }

            @Override
            public ITimeSeriesItem<Object[]> next() {
                if (!hasNext()) {
                    throw new IllegalStateException(
                        "There are no more items to get.");
                }
                curDate = findMinDate();

                // move all the iterators forward that are at the min date
                // and collect result
                Object[] values = new Object[tsIterators.length];
                for (int i = 0; i < tsIterators.length; i++) {
                    Iterator<Map.Entry<Date, ?>> tsi = tsIterators[i];
                    if (tsi == null) {
                        continue;
                    }
                    if (tsCurEntity[i].getKey().equals(curDate)) {
                        values[i] = tsCurEntity[i].getValue();
                        nextEntry(i);
                    }
                }

                if (curDate.equals(lastDate)) {
                    iteratedLastDate = true;
                }
                return new TimeSeriesItem<Object[]>(curDate, values);
            }

            /**
             * find the next minimum date of all the timeseries
             * for the current iteration
             */
            private Date findMinDate() {
                Date minDate = lastDate;
                for (int i = 0; i < tsIterators.length; i++) {
                    if (tsCurEntity[i] != null) {
                        Date curTsDate = tsCurEntity[i].getKey();
                        if (curTsDate.before(minDate)) {
                            minDate = curTsDate;
                        }
                    }
                }
                return minDate;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }

        };
    }

    // vvv Delegated methods

    public void add(ITimeSeries<?> series) {
        timeSerii.add(series);
    }

    public int size() {
        return timeSerii.size();
    }

    public boolean contains(Object o) {
        return timeSerii.contains(o);
    }

    public Iterator<ITimeSeries<?>> iterator() {
        return timeSerii.iterator();
    }

    public boolean remove(Object o) {
        return timeSerii.remove(o);
    }

    public boolean containsAll(Collection<?> c) {
        return timeSerii.containsAll(c);
    }

    public boolean addAll(Collection<? extends ITimeSeries<?>> c) {
        return timeSerii.addAll(c);
    }

    public void clear() {
        timeSerii.clear();
    }

    public ITimeSeries<?> set(int index, ITimeSeries<?> element) {
        return timeSerii.set(index, element);
    }

    public void add(int index, ITimeSeries<?> element) {
        timeSerii.add(index, element);
    }

    public ITimeSeries<?> remove(int index) {
        return timeSerii.remove(index);
    }

    public int indexOf(Object o) {
        return timeSerii.indexOf(o);
    }

    public ListIterator<ITimeSeries<?>> listIterator() {
        return timeSerii.listIterator();
    }

    public boolean isEmpty() {
        return timeSerii.isEmpty();
    }

    public boolean addAll(int index, Collection<? extends ITimeSeries<?>> c) {
        return timeSerii.addAll(index, c);
    }

    public boolean removeAll(Collection<?> c) {
        return timeSerii.removeAll(c);
    }

    public ITimeSeries<?> get(int index) {
        return timeSerii.get(index);
    }

}
