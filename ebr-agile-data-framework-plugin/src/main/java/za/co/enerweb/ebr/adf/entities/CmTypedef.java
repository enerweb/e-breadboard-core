package za.co.enerweb.ebr.adf.entities;

import static java.lang.String.format;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import za.co.enerweb.ebr.adf.dbl.AdfDbLayerUtils;

// why does this have an id and a key?!
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@ToString(of = {"id", "entity", "key", "caption"})
@Entity
@Table(name = "CM_TYPEDEF", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"ENTITY", "KEY"})})
public class CmTypedef implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;

    @JoinColumn(name = "ENTITY", referencedColumnName = "ENTITY",
        nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private CmEntity cmEntity;

    @Basic(optional = false)
    @Column(name = "ENTITY", insertable = false, updatable = false)
    private String entity;

    @Basic(optional = false)
    @Column(name = "KEY", nullable = false, length = 25)
    private String key;
    @Basic(optional = false)
    @Column(name = "CAPTION", nullable = false, length = 100)
    private String caption;
    @Column(name = "DESCRIPTION", length = 400)
    private String description;

    // sorted by caption
    @OrderBy("caption")
    @OneToMany(
        cascade = CascadeType.REMOVE,
        mappedBy = "cmParamdefPK.cmTypedef"
     ,
     fetch = FetchType.EAGER
    // Causes: org.hibernate.loader.MultipleBagFetchException: cannot
    // simultaneously fetch multiple bags
    // may need to upgrade hibernate or change this to a set
    //http://stackoverflow.com/questions/4334970/hibernate-cannot-simultaneously-fetch-multiple-bags
    )
    private Set<CmParamdef> cmParamdefs = new HashSet<CmParamdef>();

    /**
     * All relationships where this is the target
     */
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "toCmTypedef")
    private Collection<CmReltypedef> relatedTo;

    /**
     * All relationships where this is the source
     */
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "fromCmTypedef")
    private Collection<CmReltypedef> relatedFrom;

    public CmTypedef(Integer id) {
        this.id = id;
    }

    public CmTypedef(CmEntity cmEntity, String key,
        String caption) {
        setCmEntity(cmEntity);
        this.key = key;
        this.caption = caption;
    }

    public CmTypedef(Integer id, CmEntity cmEntity, String key,
        String caption) {
        this.id = id;
        setCmEntity(cmEntity);
        this.key = key;
        this.caption = caption;
    }

    public CmTypedef(CmEntity cmEntity, String key, String caption,
        String description) {
        setCmEntity(cmEntity);
        this.key = key;
        this.caption = caption;
        this.description = description;
    }

    public void add(final CmParamdef x) {
        if (!cmParamdefs.contains(x)) {
            cmParamdefs.add(x);
        }
        if (!x.getCmTypedef().equals(this)) {
            x.setCmTypedef(this);
        }
    }

    public String getInstanceTableName() {
        return AdfDbLayerUtils.efsEntityToInstanceTableName(entity);
    }

    public String getJpaInstanceEntity() {
        return AdfDbLayerUtils.efsEntityToJpaInstanceEntity(entity);
    }

    public String getJpaParameterEntity() {
        return AdfDbLayerUtils.efsEntityToJpaParmameterEntity(entity);
    }

    public String getJpaOutputEntity() {
        return AdfDbLayerUtils.efsEntityToJpaOutputEntity(entity);
    }

    public String getParameterTableName() {
        return AdfDbLayerUtils.efsEntityToParmameterTableName(entity);
    }

    public String getOutputTableName() {
        return AdfDbLayerUtils.efsEntityToOutputTableName(entity);
    }

    /**
     * The parameter's property linking back to its parent
     * @return
     */
    public String getParamInstanceProperty() {
        return AdfDbLayerUtils.efsEntityToJpaParmameterInstanceId(entity);
    }

    public String getParamInstanceColumn() {
        return AdfDbLayerUtils.efsEntityToParmameterInstanceId(entity);
    }

    /**
     * NB. This only support asymetrical relationships
     * (i.e. can't link to itself)
     */
    public String getRelationshipInstanceProperty() {
        return AdfDbLayerUtils.efsEntityToJpaParmameterInstanceId(entity);
    }

    public void setCmEntity(CmEntity cmEntity) {
        this.cmEntity = cmEntity;
        entity = cmEntity.getEntity();
    }

    public CmParamdef findParamDef(String key) {
        // XXX: if they were stored in a map we could probably
        // get this quicker..
        for (CmParamdef paramDef : getCmParamdefs()) {
            if(paramDef.getKey().equals(key)) {
                return paramDef;
            }
        }
        throw new IllegalArgumentException(format(
                "Unknown parameter key='%s'" +
                " for type: %s", key, this));
    }

}
