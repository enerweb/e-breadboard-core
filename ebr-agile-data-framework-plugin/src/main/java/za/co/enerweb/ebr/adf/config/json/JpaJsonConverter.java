package za.co.enerweb.ebr.adf.config.json;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import za.co.enerweb.ebr.adf.dbl.AdfDbLayer;
import za.co.enerweb.ebr.adf.dbl.AdfDbLayerUtils;

import static za.co.enerweb.ebr.adf.dbl.AdfDbLayerUtils.*;

@Slf4j
@NoArgsConstructor
@Data
public abstract class JpaJsonConverter<T> {
    private static final String[] DISPLAY_COLUMN_NAMES = new String[] {
        "CAPTION", "DESCRIPTION"};

    public abstract Class<T> getJpaClass();

    public List<String> getSimpleColumnNames() {
        return Collections.emptyList();
    }

    public List<? extends FieldConverter> getFieldConverters() {
        return Collections.emptyList();
    }

    /**
     * By default return the first column that corresponds to one of
     * the DISPLAY_COLUMN_NAMES, otherwise the second column
     * otherwise the first column.
     * @return
     */
    public String getDisplayColumnName() {
        List<String> simpleColumnNames = getSimpleColumnNames();
        String ret = null;
        for (String simpleColumnName : simpleColumnNames) {
            if (ret != null) {
                break;
            }
            ret = getDisplayColumnName(simpleColumnName);
        }
        if (ret != null) {
            return ret;
        }
        if (simpleColumnNames.size() > 1) {
            return simpleColumnNames.get(1);
        }
        return simpleColumnNames.get(0);
    }

    protected String getDisplayColumnName(String possibleDispCol) {
        for (String dcn : DISPLAY_COLUMN_NAMES) {
            if (dcn.equals(possibleDispCol)) {
                return dcn;
            }
        }
        return null;
    }

    /*
     * Override this if there is some custom export actions
     */
    public void jpaToJson(T source, JSONObject target) {

    }

    public void jsonToJpa(JSONObject source, T target) {

    }

    public JSONArray exportToJson(AdfDbLayer dbl)
        throws JSONException {
        JSONArray jsonObjects = new JSONArray();
        List<T> jpaObjects = dbl.findAll(getJpaClass(), getDisplayColumnName());
        int i = 0;
        for (T jpaObject : jpaObjects) {
            try {
                JSONObject jsonObject = new JSONObject();
                for (String simpleColumnName : getSimpleColumnNames()) {
                    String jpaProperty =
                        paramKeyToJpaProperty(simpleColumnName);
                    Object value = getProperty(jpaObject, jpaProperty);
                    if (value != null) {
                        jsonObject.put(simpleColumnName, value);
                    }
                }

                for (FieldConverter fieldConverter : getFieldConverters()) {
                    fieldConverter.jpaToJson(dbl, jpaObject, jsonObject);
                }

                jpaToJson(jpaObject, jsonObject);
                jsonObjects.put(i++, jsonObject);
            } catch (Exception e) {
                log.warn("Could not export to json object.", e);
            }
        }
        return jsonObjects;
    }

    public void importFromJson(AdfDbLayer dbl, String tableName,
        JSONArray jsonObjects)
        throws JSONException {
        val unsavedObjects = new ArrayList<JSONObject>(jsonObjects.length());
        for (int i = 0; i < jsonObjects.length(); i++) {
            JSONObject jsonObject = jsonObjects.getJSONObject(i);
            if (!insertObject(dbl, jsonObject, tableName)) {
                unsavedObjects.add(jsonObject);
            }
        }
        // if at first we don't succeed, try again.
        if (!unsavedObjects.isEmpty()) {
            // when no more inserts are going through we need to stop
            boolean gotSmaller = true;
            while (gotSmaller && !unsavedObjects.isEmpty()) {
                gotSmaller = false;
                for (Iterator<JSONObject> iterator =
                    unsavedObjects.iterator(); iterator.hasNext();) {
                    val jsonObject = (JSONObject) iterator.next();
                    if (insertObject(dbl, jsonObject, tableName)) {
                        iterator.remove();
                        gotSmaller = true;
                    }
                }
            }
            if (!gotSmaller && !unsavedObjects.isEmpty()) {
                log.error("Could not persist all values:");
                for (JSONObject jsonObject : unsavedObjects) {
                    log.error(" * " + jsonObject);
                }
            }
        }
    }

    private boolean insertObject(AdfDbLayer dbl, JSONObject jsonObject,
        String tableName) {
        try {
            T jpaEntity = (T)dbl.instantiateJpaEntity(
                AdfDbLayerUtils.toCamelCase(tableName));
            for (String simpleColumnName : getSimpleColumnNames()) {
                if (jsonObject.has(simpleColumnName)) {
                    String jpaProperty =
                        paramKeyToJpaProperty(simpleColumnName);

                    setProperty(jpaEntity, jpaProperty,
                        jsonObject.get(simpleColumnName));
                }
            }

            for (FieldConverter fieldConverter : getFieldConverters()) {
                if (!fieldConverter.jsonToJpa(dbl, jsonObject, jpaEntity)) {
                    log.debug("Could not insert object:'\n" + jsonObject
                        + "\n" + jpaEntity);
                    return false;
                }
            }

            jsonToJpa(jsonObject, jpaEntity);
            jpaEntity = dbl.save(jpaEntity);
            return true;
        } catch (Exception e) {
            log.warn("Could not import json object.", e);
        }
        return false;
    }

    public interface FieldConverter {
        void jpaToJson(AdfDbLayer dbl, Object source, JSONObject target)
            throws Exception;

        /**
         * return true if successful
         */
        boolean jsonToJpa(AdfDbLayer dbl, JSONObject source, Object target)
            throws Exception;
    }

    /**
     * jsonObject.put("ENTITYTYPE_KEY",
     * jpaObject.getCmEntitytype().getKey());
     */
    @Data
    @AllArgsConstructor
    public class SimpleForeignKey implements FieldConverter {
        private String columnName;
        private String propertyName;
        private Class<?> referencedClass;
        private String referencedPrimaryKey;

        public void jpaToJson(AdfDbLayer dbl, Object source, JSONObject target)
            throws JSONException {
            Object referencedEntity = getProperty(source, propertyName);
            if (referencedEntity != null) {
                target.put(columnName,
                    getProperty(referencedEntity,
                        referencedPrimaryKey));
            }
        }

        public boolean jsonToJpa(AdfDbLayer dbl, JSONObject source,
            Object target) throws JSONException {
            if (source.has(columnName)) {
                Object referencedValue = source.get(columnName);
                Object referencedEntity = dbl.find(referencedClass,
                    referencedValue);
                if (referencedEntity == null) {
                    List<?> all = dbl.findAll(referencedClass);
                    log.info("Could not find referenced value " + columnName
                        + " = " + referencedValue
                        + "\n This are the only available options:");
                    for (Object object : all) {
                        log.info(" * " + object);
                    }
                    return false;
                }
                setProperty(target, propertyName, referencedEntity);
            }
            return true;
        }
    }
}
