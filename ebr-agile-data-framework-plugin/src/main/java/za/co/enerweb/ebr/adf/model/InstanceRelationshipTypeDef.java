package za.co.enerweb.ebr.adf.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import za.co.enerweb.ebr.adf.entities.CmReltypedef;
import za.co.enerweb.ebr.adf.entities.IbInstance;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InstanceRelationshipTypeDef implements Serializable {
    private static final long serialVersionUID = 1L;
    private IbInstance fromInstance;
    private CmReltypedef relationshipType;
}
