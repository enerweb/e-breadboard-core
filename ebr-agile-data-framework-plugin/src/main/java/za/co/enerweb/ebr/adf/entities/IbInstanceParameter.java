package za.co.enerweb.ebr.adf.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import za.co.enerweb.toolbox.reflection.PropertyUtils;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of={"cmTypedef", "id"})
@ToString(of={"id", "appldatetime", "indx"})
@MappedSuperclass
public class IbInstanceParameter implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "APPLDATETIME", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date appldatetime;

    @Column(name = "INDX", length = 25)
    private String indx;

    @JoinColumn(name = "TYPEDEF_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private CmTypedef cmTypedef;

    public IbInstanceParameter(Integer id) {
        this.id = id;
    }

    public IbInstanceParameter(Integer id, Date appldatetime) {
        this.id = id;
        this.appldatetime = appldatetime;
    }

    public IbInstanceParameter(CmTypedef cmTypedef, Date appldatetime) {
        this.cmTypedef = cmTypedef;
        this.appldatetime = appldatetime;
    }

    public <T extends IbInstance> T getInstance(CmParamdef cmParamdef) {
        String instanceProperty = cmParamdef.getInstanceProperty();
        Object ret = PropertyUtils.getProperty(this,
            instanceProperty);
        return (T) ret;
    }

//    @PostRemove
//    public void postRemove() {
//        // remove all unreferenced adf parameters from the file system
//        CmParamdef cmParamdef = getCmTypedef().findParamDef(key);
//
//    }
}
