package za.co.enerweb.ebr.adf.rest.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import za.co.enerweb.ebr.adf.entities.CmParamdef;

@Data
@EqualsAndHashCode(callSuper = true)
@XmlRootElement(name = RCObjectState.URI_FRAGMENT)
@NoArgsConstructor
@AllArgsConstructor
public class RCObjectState extends ARCConcept {
    public final static String URI_FRAGMENT = "ObjectState";

    // @XmlElement
    // ^^^ needed if no setter beause of other sestters...
    private Date applDateTime;
    private RCPropertyValues propertyValues = new RCPropertyValues();

    // public RCObjectState(UriInfo uriInfo, ExpandMembers expand,
    // DateTime dateTime) {
    // super(uriInfo, expand, cmObject, URI_FRAGMENT);
    // }

    /**
     * @param cmObject
     * @return eg. weathersa-1.meantemp
     */
    protected String deriveSlug(CmParamdef cmObject) {
        return RCClass.deriveSlugFromCmTypedef(cmObject.getCmTypedef()) + "."
            + cmObject.getKey().toLowerCase();
    }

    public static String typeDefKeyFromSlug(String slug) {
        return keyFromSlug(parentSlug(slug)).toUpperCase();
    }

    public static String fieldKeyFromSlug(String slug) {
        return childSlug(slug).toUpperCase();
    }

    // public void setApplDateTime(DateTime applDateTime) {
    // this.applDateTime = DATE_TIME_FORMATTER.print(applDateTime);
    // }

    // public void setApplDateTime(Date applDateTime) {
    // this.applDateTime =
    // DATE_TIME_FORMATTER.print(new DateTime(applDateTime));
    // }

    public void addPropertyValue(RCPropertyValue pv) {
        propertyValues.add(pv);
    }

}
