package za.co.enerweb.ebr.adf.model;


public abstract class AdfRelationshipEntity extends AdfEntity {

    @Override
    public final EntityType getEntityType() {
        return EntityType.RELATIONSHIP;
    }

    public abstract Class<? extends AdfInstanceEntity>
        getFromInstanceEntity();

    public abstract Class<? extends AdfInstanceEntity>
        getToInstanceEntity();

}
