package za.co.enerweb.ebr.adf.model;

import static org.apache.commons.lang.StringUtils.isBlank;

import java.util.regex.Pattern;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.toolbox.string.StringUtils;

/**
 * Extend this to make a object definition of your Adf Relationship Type.
 * It must have a no args constructor.
 * The convention is to prefix it with T eg. TMyType
 */
@Slf4j
@Data
// can make it @Getter(lazy=true) if needed
@FieldDefaults(level = AccessLevel.PRIVATE)
public final class AdfRelationshipTypeDefMetadata {

    private static final Pattern prefixPattern = Pattern.compile(
        "^(R)([A-Z].*)");

    String key;
    String caption;
    String description = "";
    Class<? extends AdfRelationshipEntity> entityClass;
    // AdfRelationshipFieldMetadata from;
    // AdfRelationshipFieldMetadata to;
    AdfTypeDefMetadata fromTypeDef;
    AdfTypeDefMetadata toTypeDef;

    // protected static String deriveKey(
    // Class<? extends AdfRelationship> klass) {
    // String ret = klass.getSimpleName();
    // Matcher matcher = prefixPattern.matcher(ret);
    // if (matcher.matches()) {
    // ret = matcher.group(2);
    // } else {
    // throw new IllegalStateException("Class name does not " +
    // "follow the typedef name convention " +
    // "(prefix it with R eg. RMyRelationship) : " + ret);
    // }
    // return StringUtils.camelCaseToUnderscoreSeparated(ret).toUpperCase();
    // }

    protected static String deriveCaption(String key) {
        return StringUtils.variableName2Title(key);
    }

    public AdfRelationshipTypeDefMetadata(String key) {
        this.key = key;
    }

    public void init(AdfTypeDefMetadata fromTypeDef,
        AdfRelationshipTo relationshipTo) {
        if (entityClass != null) {
            throw new IllegalStateException("Relationship with key='" +
                key
                + "' is being more than once. Make sure a parent class isn't " +
                "already registering it.\n  " + fromTypeDef
                + "\n  " + this.fromTypeDef);
        }
        this.fromTypeDef = fromTypeDef;
        caption = relationshipTo.caption();
        if (isBlank(caption)) {
            caption = deriveCaption(key);
        }
        description = relationshipTo.description();
        entityClass = relationshipTo.entity();
    }

    // public void addFrom(Field field, AdfRelationshipFrom from,
    // AdfTypeDefMetadata typeDef) {
    // this.from = addRelationship(field,
    // typeDef, true);
    // }
    //
    // public void addTo(Field field, AdfRelationshipTo to,
    // AdfTypeDefMetadata typeDef) {
    // this.to = addRelationship(field,
    // typeDef, false);
    // }
    //
    // private AdfRelationshipFieldMetadata addRelationship(Field field,
    // AdfTypeDefMetadata typeDef, boolean isFrom) {
    // AdfRelationshipFieldMetadata r = new AdfRelationshipFieldMetadata();
    // r.setFieldName(field.getName());
    // if (typeDef == null) {
    // throw new IllegalArgumentException("Could not find a TypeDef" +
    // " for field " + field + " make sure it extends " +
    // "AdfInstance and it has a @AdfInstanceTypeDef " +
    // "annotation");
    // }
    // r.setTypeDef(typeDef);
    // r.setFrom(isFrom);
    // r.setRelationshipClass(getRelationshipClass());
    // return r;
    // }

}
