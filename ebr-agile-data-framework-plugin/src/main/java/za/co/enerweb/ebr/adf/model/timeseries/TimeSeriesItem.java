package za.co.enerweb.ebr.adf.model.timeseries;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of="dateTime")
public class TimeSeriesItem<V> implements ITimeSeriesItem<V>
//, Comparable<TimeSeriesItem<V>>
{
    private static final long serialVersionUID = 1L;
    private Date dateTime;
    protected V value;

    public TimeSeriesItem(java.util.Map.Entry<Date, V> entry) {
        this(entry.getKey(), entry.getValue());
    }

    @Override
    public Date getKey() {
        return dateTime;
    }

    public V setValue(V value) {
        V oldValue = this.value;
        this.value = value;
        return oldValue;
    }

//    /**
//     * By default sort by date
//     * @param o
//     * @return
//     */
//    @Override
//    public int compareTo(TimeSeriesItem<V> o) {
//        return dateTime.compareTo(o.dateTime);
//    }
}
