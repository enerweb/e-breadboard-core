package za.co.enerweb.ebr.adf.model;


public abstract class AdfHierarchyEntity extends AdfEntity {

    @Override
    public final EntityType getEntityType() {
        return EntityType.HIERARCHY;
    }

}
