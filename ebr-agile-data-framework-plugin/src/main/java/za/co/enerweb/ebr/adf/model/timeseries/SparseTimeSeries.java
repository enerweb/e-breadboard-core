package za.co.enerweb.ebr.adf.model.timeseries;

import java.util.Date;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.joda.time.DateTime;

/**
 * V is the type of values this timeseries contains
 */
public class SparseTimeSeries<V> extends TreeMap<Date, V>
implements ITimeSeries<V> {

    private static final long serialVersionUID = 1L;

    // /**
    // * returns null if empty
    // */
    public Date getFirstDate() {
        // if (isEmpty()) {
        // return null;
        // }
        return this.firstKey();
    }

    // /**
    // * returns null if empty
    // */
    public Date getLastDate() {
        // if (isEmpty()) {
        // return null;
        // }
        return this.lastKey();
    }

    /**
     * Backed by the collection.
     */
    @Override
    public Iterator<ITimeSeriesItem<V>> iterator() {
        return sparseIterator();
    }

    /**
     * Backed by the collection.
     */
    @Override
    public Iterable<ITimeSeriesItem<V>> sparseIterable() {
        return new Iterable<ITimeSeriesItem<V>>() {

            @Override
            public Iterator<ITimeSeriesItem<V>> iterator() {
                return sparseIterator();
            }
        };
    }

    protected Iterator<ITimeSeriesItem<V>> sparseIterator() {
        return new Iterator<ITimeSeriesItem<V>>(){
            Iterator<Entry<Date, V>> i = entrySet().iterator();

            @Override
            public boolean hasNext() {
                return i.hasNext();
            }

            @Override
            public ITimeSeriesItem<V> next() {
                java.util.Map.Entry<Date, V> next = i.next();
                return new TimeSeriesItem<V>(next);
            }

            @Override
            public void remove() {
                i.remove();
            }

        };
    }

    @Override
    public void put(TimeSeriesItem<V> timeSeriesItem) {
        put(timeSeriesItem.getDateTime(), timeSeriesItem.getValue());
    }

    /**
     * @param dateTime
     * @param value
     */
    public void put(DateTime dateTime, V value) {
        put(dateTime.toDate(), value);
    }

    public ITimeSeriesItem<V> floorEntry(Date dateTime) {
        return new TimeSeriesItem<V>(super.floorEntry(dateTime));
    }

    public String toString() {
        if (isEmpty()) {
            return "empty";
        }
        return "size:" + size() + " first:" + getFirstDate() + " last:"
            + getLastDate();
    }

    public String toStringVerbose() {
        StringBuilder sb = new StringBuilder();
        for (ITimeSeriesItem<V> item : sparseIterable()) {
            sb.append(item.getDateTime());
            sb.append("\t");
            sb.append(item.getValue());
            sb.append("\n");
        }
        return sb.toString();
    }
}
