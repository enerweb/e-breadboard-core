package za.co.enerweb.ebr.adf.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import za.co.enerweb.ebr.adf.entities.CmTypedef;
import za.co.enerweb.ebr.gui.vaadin.messaging.Message;

@Data
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
public class MSaveAdfType extends Message {
    private CmTypedef cmTypedef;
}
