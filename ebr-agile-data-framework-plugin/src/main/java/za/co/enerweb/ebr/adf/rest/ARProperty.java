package za.co.enerweb.ebr.adf.rest;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.adf.dbl.AdfDbLayer;
import za.co.enerweb.ebr.adf.entities.CmParamdef;
import za.co.enerweb.ebr.adf.rest.model.RCProperty;
import za.co.enerweb.toolbox.rest.ExpandMembers;
import za.co.enerweb.toolbox.rest.ResponsTypeUtil;

// specified in Application:
//@Produces({MediaType.APPLICATION_JSON
// //, MediaType.APPLICATION_XML
//})

@Path("/" + RCProperty.URI_FRAGMENT)
@Slf4j
public abstract class ARProperty extends ARConcept {

    @Path("/{idFieldValue}" + ResponsTypeUtil.PATH)
    @GET
    public Response get(@Context UriInfo uriInfo,
        @PathParam(ResponsTypeUtil.PARAM) String responseType,
        @QueryParam(ExpandMembers.PARAM) String expand,
        @NotNull @Size(min = 1)
        @PathParam("idFieldValue") String slug) {

        final String typeDefKey = RCProperty.typeDefKeyFromSlug(slug);
        final String paramDefKey = RCProperty.fieldKeyFromSlug(slug);

        CmParamdef cmParamdef = getRDbl().findParameterDef(
            typeDefKey, paramDefKey);
        final RCProperty ret = getRDbl().getRCProperty(uriInfo,
            new ExpandMembers(RCProperty.URI_FRAGMENT, expand), cmParamdef);

        return ResponsTypeUtil.parse(Response.ok(
            ret), responseType).build();
    }

}
