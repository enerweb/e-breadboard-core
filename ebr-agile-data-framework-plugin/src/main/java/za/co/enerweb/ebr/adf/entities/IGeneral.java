package za.co.enerweb.ebr.adf.entities;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, of = { })
@ToString(callSuper = true, of = { })
@Entity
@Table(name = "I_GENERAL")
public class IGeneral extends IbInstance {

    public IGeneral(CmTypedef cmTypedef, String caption) {
        super(cmTypedef, caption);
    }

}
