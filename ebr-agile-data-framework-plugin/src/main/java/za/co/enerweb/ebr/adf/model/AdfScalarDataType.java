package za.co.enerweb.ebr.adf.model;


/**
 * Extend this to make a object definition of your Adf Data Type.
 * It must have a no args constructor.
 * The convention is to prefix it with Dt eg. DtMyDataType
 * and it must be in the package called data_types relative to your AdfFactory
 */
public abstract class AdfScalarDataType<T> extends AdfDataType<T> {

    public AdfDataTypeClass getAdfDataTypeClass() {
        return AdfDataTypeClass.SCALAR;
    }
}
