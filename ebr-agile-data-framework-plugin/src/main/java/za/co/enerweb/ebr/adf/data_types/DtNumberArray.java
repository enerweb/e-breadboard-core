package za.co.enerweb.ebr.adf.data_types;

import za.co.enerweb.ebr.adf.model.AdfDataType;
import za.co.enerweb.ebr.adf.model.AdfDataTypeClass;

/*
 * TODO: maybe we can make an anotation that can go onto DtNumber
 * that can contain all this metadata..
 */
public class DtNumberArray extends DtNumber {

    protected String deriveKey() {
        return "NUM_ARRAY";
    }

    public String getDescription() {
        return "Number Array";
    }

    public AdfDataTypeClass getAdfDataTypeClass() {
        return AdfDataTypeClass.ARRAY;
    }

    public Class<? extends AdfDataType<?>> getItemDataType() {
        return DtNumber.class;
    }
}
