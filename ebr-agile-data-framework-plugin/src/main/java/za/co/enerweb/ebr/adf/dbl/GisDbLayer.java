package za.co.enerweb.ebr.adf.dbl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;

import lombok.val;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.adf.data_types.DtGisLocation;
import za.co.enerweb.ebr.adf.data_types.DtGisLocationArray;
import za.co.enerweb.ebr.adf.data_types.DtNumber;
import za.co.enerweb.ebr.adf.data_types.GisLocation;
import za.co.enerweb.ebr.adf.entities.CmParamdef;
import za.co.enerweb.ebr.adf.entities.CmTypedef;
import za.co.enerweb.ebr.adf.entities.IbInstance;
import za.co.enerweb.ebr.adf.entities.IbInstanceParameter;
import za.co.enerweb.ebr.adf.model.ParameterKind;

@Slf4j
@Stateful
@LocalBean
public class GisDbLayer extends AdfDbLayer {
    public static final String PARAM_LOCATION = "LOCATION";

    /**
     * adds a location parameter to the type
     * @param dbl
     * @param cmTypedef
     */
    public void addGisParameter(CmTypedef cmTypedef) {
        try {
            cmTypedef = em.merge(cmTypedef);
            CmParamdef paramLocation = getOrCreateCmParamdef(cmTypedef,
                PARAM_LOCATION, null, null,
                ParameterKind.INPUT, DtGisLocationArray.class,
                getOrCreateUnitOfmeasure("DD"),
                null);
            paramLocation.setCmIndexDatatype(getCmDataType(DtNumber.class));
            paramLocation.setDescription("Geographical area of node.");
        } catch (Exception e) {
            log.warn("Could not add gis parameter to " + cmTypedef);
        }
    }

    /**
     * @return a list of lists of locations because an instance may have
     *         multiple location parameters each of which can be a point, line
     *         or polygon and I don't think we wanna mix them.
     */
    public List<List<GisLocation>> getGisLocations(
        IbInstance instance, Date effectiveDate) {
        instance = em.merge(instance);
        List<List<GisLocation>> ret = new ArrayList<List<GisLocation>>();
        AdfRegistry adfRegistry = getAdfRegistry();
        for (CmParamdef paramDef : instance.getCmTypedef().getCmParamdefs()) {
            val adfDataTypeClass = adfRegistry.getDataTypeDtClass(paramDef);
            if (adfDataTypeClass.equals(DtGisLocation.class)) {
                GisLocation l = getParamValue(instance, paramDef.getKey(),
                    effectiveDate, null);
                if (l != null) {
                    ret.add(Collections.singletonList(l));
                }
            } else if (adfDataTypeClass.equals(DtGisLocationArray.class)) {
                List<IbInstanceParameter> params =
                    getSingleArrayParamValue(instance, paramDef.getKey(),
                        effectiveDate);
                List<GisLocation> ll = new ArrayList<GisLocation>();
                for (IbInstanceParameter param : params) {
                    GisLocation l = getParamValue(param, paramDef);
                    if (l != null) {
                        ll.add(l);
                    }
                }
                if (!ll.isEmpty()) {
                    ret.add(ll);
                }
            }
        }

        return ret;
    }

    /**
     * Sets all gis location parameters!
     */
    public void setGisLocations(
        IbInstance instance, Date appldatetime, List<GisLocation> locations) {
        instance = em.merge(instance);
        AdfRegistry adfRegistry = getAdfRegistry();
        for (CmParamdef paramDef : instance.getCmTypedef().getCmParamdefs()) {
            val adfDataTypeClass = adfRegistry.getDataTypeDtClass(paramDef);
            if (adfDataTypeClass.equals(DtGisLocationArray.class)) {
                deleteParamValues(instance, paramDef);
                int i = 0;
                for (GisLocation gisLocation : locations) {
                    addParamValue(instance, paramDef.getKey(), gisLocation,
                        appldatetime, "" + i);
                    i++;
                }
            }
        }

    }
}
