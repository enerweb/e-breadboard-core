package za.co.enerweb.ebr.adf.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import za.co.enerweb.ebr.adf.model.ParameterKind;

@Data
@EqualsAndHashCode(of="key")
@ToString(of="key")
@NoArgsConstructor
@Entity
@Table(name = "CM_PARAMKIND")
@NamedQueries({
    @NamedQuery(name = "CmParamkind.findAll", query = "SELECT c FROM CmParamkind c"),
    @NamedQuery(name = "CmParamkind.findByKey", query = "SELECT c FROM CmParamkind c WHERE c.key = :key"),
    @NamedQuery(name = "CmParamkind.findByDescription", query = "SELECT c FROM CmParamkind c WHERE c.description = :description")})
public class CmParamkind implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "KEY", nullable = false, length = 25)
    private String key;
    @Column(name = "DESCRIPTION", length = 400)
    private String description;

    @Column(name = "KEY", insertable=false, updatable=false)
    @Enumerated(EnumType.STRING)
    private ParameterKind parameterKind;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "cmParamkind")
    private Collection<CmRelparamdef> cmRelparamdefCollection;
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "cmParamkind")
    private Collection<CmParamdef> cmParamdefCollection;

    public CmParamkind(ParameterKind parameterKind) {
        this.parameterKind = parameterKind;
        this.key = parameterKind.name();
        this.description = parameterKind.toString();
    }
}
