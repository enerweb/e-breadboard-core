package za.co.enerweb.ebr.adf.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import za.co.enerweb.ebr.adf.dbl.AdfRegistry;
import za.co.enerweb.ebr.adf.entities.CmUnitofmeasure;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class AdfParameterDefMetadata
    implements Comparable<AdfParameterDefMetadata> {

    /**
     * If this is null, then there is no field for this..
     */
    private String fieldName;
    private String key;
    private String caption;
    private String description;
    private CmUnitofmeasure cmUnitofmeasure;
    private ParameterKind parameterKind;
    private Class<? extends AdfDataType<?>> typeClass;
    private Class<? extends AdfDataType<?>> indexTypeClass;
    private String paramTable;
    /**
     * if true this field is a list of values
     */
    private boolean list = false;

    public AdfDataType<?> getType(AdfRegistry adfRegistry) {
        return adfRegistry.getAdfDataType(typeClass);
    }

    @Override
    public int compareTo(AdfParameterDefMetadata o) {
        int ret = caption.compareTo(o.caption);
        if (ret == 0) {
            ret = getUnitOfMeasureCaption().compareTo(
                o.getUnitOfMeasureCaption());
        }
        return ret;
    }

    public String getUnitOfMeasureCaption() {
        if (cmUnitofmeasure != null) {
            return cmUnitofmeasure.getCaption();
        }
        return "";
    }

    public String getCaptionWithUnitOfMeasure() {
        if (cmUnitofmeasure != null) {
            return caption + " (" + cmUnitofmeasure.getCaption() + ")";
        }
        return caption;
    }
}
