package za.co.enerweb.ebr.adf.model;


public abstract class AdfInstanceEntity extends AdfEntity {

    @Override
    public final EntityType getEntityType() {
        return EntityType.INSTANCE;
    }

}
