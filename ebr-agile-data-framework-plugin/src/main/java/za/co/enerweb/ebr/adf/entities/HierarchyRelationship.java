package za.co.enerweb.ebr.adf.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Data;
import za.co.enerweb.ebr.adf.dbl.AdfDbLayer;
import za.co.enerweb.ebr.adf.dbl.AdfLowlevelDbLayer;

/**
 * stores links between instances
 */
@Data
@AllArgsConstructor
@MappedSuperclass
@Table(
    uniqueConstraints = {
        @UniqueConstraint(columnNames = {"HIERARCHY_ID",
            "PARENT_TYPE_ID", "PARENT_ID", "CHILD_TYPE_ID", "CHILD_ID",
            "START_DATETIME", "END_DATETIME"})}
)
// @org.hibernate.annotations.Table(
// appliesTo = "table_name",
// indexes = {
// @Index(name="multi_column_index", columnNames = {"col1", "col2"}),
// }
// )
public abstract class HierarchyRelationship implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;

    @JoinColumn(name = "HIERARCHY_ID",
        referencedColumnName = "ID",
        nullable = false)
    @ManyToOne(optional = false)
    private CmHierarchyDef cmHierarchyDef;

    /**
     * null if root
     */
    @JoinColumn(name = "PARENT_TYPE_ID",
        referencedColumnName = "ID", nullable = true)
    @ManyToOne(optional = true)
    private CmTypedef parentCmTypedef;

    /**
     * null if root
     */
    @Basic(optional = true)
    @Column(name = "PARENT_ID", nullable = true)
    private Integer parentInstanceId;

    @JoinColumn(name = "CHILD_TYPE_ID", referencedColumnName = "ID")
    @ManyToOne
    private CmTypedef childCmTypedef;

    @Basic(optional = false)
    @Column(name = "CHILD_ID", nullable = false)
    private Integer childInstanceId;

    /**
     * Inclusive start date, i.e. it's the closed lower bound.
     */
    @Basic(optional = false)
    @Column(name = "START_DATETIME", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDatetime;

    /**
     * Exclusive end date, i.e. it's the open upper bound.
     */
    @Basic(optional = false)
    @Column(name = "END_DATETIME", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDatetime;

    public HierarchyRelationship() {
        startDatetime = AdfDbLayer.EPOCH;
        endDatetime = AdfDbLayer.APOCALYPSE;
    }

    public void init(CmHierarchyDef cmHierarchyDef,
        IbInstance parentInstance,
        IbInstance childInstance,
        Date startDatetime,
        Date endDatetime) {
        this.cmHierarchyDef = cmHierarchyDef;
        if (parentInstance == null) {
            parentCmTypedef = null;
            parentInstanceId = null;
        } else {
            parentCmTypedef = parentInstance.getCmTypedef();
            parentInstanceId = parentInstance.getId();
        }

        if (childInstance == null) {
            throw new NullPointerException("childInstance");
        }
        childCmTypedef = childInstance.getCmTypedef();
        childInstanceId = childInstance.getId();

        if (startDatetime == null) {
            this.startDatetime = AdfDbLayer.EPOCH;
        } else {
            this.startDatetime = startDatetime;
        }
        if (endDatetime == null) {
            this.endDatetime = AdfDbLayer.APOCALYPSE;
        } else {
            this.endDatetime = endDatetime;
        }
    }

    /**
     * null if root
     */
    public IbInstance getParentInstance(AdfLowlevelDbLayer dbl) {
        if (!isRoot()) {
            return dbl.findInstance(parentCmTypedef, parentInstanceId);
        }
        return null;
    }

    public boolean isRoot() {
        return parentInstanceId == null;
    }

    public IbInstance getChildInstance(AdfLowlevelDbLayer dbl) {
        return dbl.findInstance(childCmTypedef, childInstanceId);
    }
}
