package za.co.enerweb.ebr.adf.dbl;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import za.co.enerweb.ebr.adf.AdfFactory;
import za.co.enerweb.ebr.adf.entities.CmDatatype;
import za.co.enerweb.ebr.adf.entities.CmParamdef;
import za.co.enerweb.ebr.adf.model.AdfDataType;
import za.co.enerweb.ebr.adf.model.AdfEntity;
import za.co.enerweb.ebr.adf.model.AdfHierarchyChild;
import za.co.enerweb.ebr.adf.model.AdfHierarchyEntity;
import za.co.enerweb.ebr.adf.model.AdfHierarchyParent;
import za.co.enerweb.ebr.adf.model.AdfHierarchyTypeDefMetadata;
import za.co.enerweb.ebr.adf.model.AdfInstance;
import za.co.enerweb.ebr.adf.model.AdfInstanceTypeDef;
import za.co.enerweb.ebr.adf.model.AdfParamDef;
import za.co.enerweb.ebr.adf.model.AdfParameterDefMetadata;
import za.co.enerweb.ebr.adf.model.AdfRelationshipFrom;
import za.co.enerweb.ebr.adf.model.AdfRelationshipTo;
import za.co.enerweb.ebr.adf.model.AdfRelationshipTypeDefMetadata;
import za.co.enerweb.ebr.adf.model.AdfTypeDefMetadata;
import za.co.enerweb.ebr.annotations.OnEbrReload;
import za.co.enerweb.ebr.server.Registrar;

/**
 * This keeps track of all the datatypes that is associated with
 * each AdfFactory.
 * If you define custom types they have to be in the .data_types package.
 */
@Slf4j
public class AdfRegistry {

    private static LoadingCache<Class<? extends AdfFactory>, AdfRegistry>
    adfRegistries =
        CacheBuilder.newBuilder().build(
            // this syntax will be sweeter with lambdas I hope..
            new CacheLoader<Class<? extends AdfFactory>,
            AdfRegistry>() {
                public AdfRegistry load(
                    Class<? extends AdfFactory> key) {
                    AdfRegistry ret = new AdfRegistry();
                    ret.registerAll(key);
                    return ret;
                }
            });

    @Data
    @AllArgsConstructor
    private class TypeField {
        Class<? extends AdfInstance> instanceClass;
        // Class<? extends AdfInstance> fieldActualClass;
        Field field;
        AdfTypeDefMetadata typeDef;
    }

    @SneakyThrows
    public static AdfRegistry getRegistry(
        Class<? extends AdfFactory> factoryClass) {
        return adfRegistries.get(factoryClass);
    }

    @OnEbrReload
    public static void reset() {
        adfRegistries.invalidateAll();
    }

    // vvv entity
    private final Map<Class<? extends AdfEntity>, AdfEntity>
    _entityClasses =
        new HashMap<Class<? extends AdfEntity>, AdfEntity>();
    private final Map<String, AdfEntity> _entityKeys =
        new HashMap<String, AdfEntity>();
    @Getter//(lazy = true)
    private final Map<Class<? extends AdfEntity>, AdfEntity>
    entityClasses =
 Collections.unmodifiableMap(_entityClasses);

    @Getter//(lazy = true)
    private final Map<String, AdfEntity> entityKeys =
        Collections.unmodifiableMap(_entityKeys);

    public Collection<AdfEntity> getAllAdfEntities() {
        return getEntityClasses().values();
    }

    public AdfEntity getAdfEntity(Class<? extends AdfEntity> entityClass) {
        return _entityClasses.get(entityClass);
    }

    // vvv dataTypes
    private final Map<Class<? extends AdfDataType<?>>, AdfDataType<?>>
    _dataTypeClasses =
        new HashMap<Class<? extends AdfDataType<?>>, AdfDataType<?>>();
    private final Map<String, AdfDataType<?>> _dataTypeKeys =
        new HashMap<String, AdfDataType<?>>();
    @Getter//(lazy = true)
    private final Map<Class<? extends AdfDataType<?>>, AdfDataType<?>>
    dataTypeClasses =
        Collections.unmodifiableMap(_dataTypeClasses);
    @Getter//(lazy = true)
    private final Map<String, AdfDataType<?>> dataTypeKeys =
        Collections.unmodifiableMap(_dataTypeKeys);

    public AdfDataType<?> getAdfDataType(
        @SuppressWarnings("rawtypes") Class<? extends AdfDataType> klass) {
        AdfDataType<?> ret = _dataTypeClasses.get(klass);
        // log.debug("getAdfDataType(" + klass.getSimpleName() + ") " + ret);
        checkRet("AdfDataType", ret, klass);
        return ret;
    }

    public AdfDataType<?> getAdfDataType(String key) {
        AdfDataType<?> ret = _dataTypeKeys.get(key);
        checkRet("AdfDataType", ret, key);
        return ret;
    }

    public AdfDataType<?> getAdfDataType(CmDatatype cmDatatype) {
        return getAdfDataType(cmDatatype.getKey());
    }

    public AdfDataType<?> getAdfDataType(CmParamdef paramDef) {
        return getAdfDataType(paramDef.getCmDatatype());
    }

    @SuppressWarnings("unchecked")
    public Class<? extends AdfDataType<?>> getDataTypeDtClass(
        CmDatatype cmDatatype) {
        if (cmDatatype == null) {
            return null;
        }
        return (Class<? extends AdfDataType<?>>) getAdfDataType(
            cmDatatype)
            .getClass();
    }

    public Class<? extends AdfDataType<?>> getDataTypeDtClass(
        CmParamdef paramDef) {
        return (Class<? extends AdfDataType<?>>) getDataTypeDtClass(paramDef
            .getCmDatatype());
    }

    public Collection<AdfDataType<?>> getAllDataTypes() {
        return getDataTypeKeys().values();
    }


    // vvv typedef
    private final Map<Class<? extends AdfInstance>, AdfTypeDefMetadata>
    _typeDefs =
        new HashMap<Class<? extends AdfInstance>, AdfTypeDefMetadata>();
    private final Map<String, AdfTypeDefMetadata> _typeDefKeys =
        new HashMap<String, AdfTypeDefMetadata>();
    @Getter//(lazy = true)
    private final Map<Class<? extends AdfInstance>, AdfTypeDefMetadata>
    typeDefs =
        Collections.unmodifiableMap(_typeDefs);
    @Getter//(lazy = true)
    private final Map<String, AdfTypeDefMetadata> typeDefKeys =
        Collections.unmodifiableMap(_typeDefKeys);

    public AdfTypeDefMetadata getAdfTypeDef(
        Class<? extends AdfInstance> klass) {
        AdfTypeDefMetadata ret = _typeDefs.get(klass);
        checkRet("AdfTypeDef", ret, klass);
        return ret;
    }

    public AdfTypeDefMetadata getAdfTypeDefOrNull(String key) {
        AdfTypeDefMetadata ret = _typeDefKeys.get(key);
        return ret;
    }

    public AdfTypeDefMetadata getAdfTypeDef(String key) {
        AdfTypeDefMetadata ret = getAdfTypeDefOrNull(key);
        checkRet("AdfTypeDef", ret, key);
        return ret;
    }

    public Collection<AdfTypeDefMetadata> getAllTypeDefs() {
        return getTypeDefKeys().values();
    }

    public void registerTypeDef(AdfTypeDefMetadata tdmd) {
        if (!_typeDefKeys.containsKey(tdmd.getKey())) {
            _typeDefs.put(tdmd.getInstanceClass(), tdmd); // overrides sharing:(
            _typeDefKeys.put(tdmd.getKey(), tdmd);
        }
    }

    public void registerParamDef(AdfTypeDefMetadata tdmd,
        AdfParameterDefMetadata paramd) {
        log.debug("Adding paramdef: " + tdmd.getKey() + "." + paramd.getKey());
        _typeDefKeys.get(tdmd.getKey()).addParamDef(paramd);
    }

    // vvv relTypeDef
    private final Map<String, AdfRelationshipTypeDefMetadata>
    _relTypeDefKeys =
        new HashMap<String, AdfRelationshipTypeDefMetadata>();
    @Getter//(lazy = true)
    private final Map<String, AdfRelationshipTypeDefMetadata>
    relTypeDefKeys =
        Collections.unmodifiableMap(_relTypeDefKeys);

    public AdfRelationshipTypeDefMetadata getAdfRelationshipTypeDef(
        String key) {
        AdfRelationshipTypeDefMetadata ret = _relTypeDefKeys.get(key);
        checkRet("AdfRelationshipTypeDef", ret, key);
        return ret;
    }

    public Collection<AdfRelationshipTypeDefMetadata>
        getAllRelationshipTypeDefs() {
        return getRelTypeDefKeys().values();
    }


    // vvv hierarchyTypeDef
    private final Map<String, AdfHierarchyTypeDefMetadata>
    _hierarchyTypeDefKeys =
        new HashMap<String, AdfHierarchyTypeDefMetadata>();
    @Getter//(lazy = true)
    private final Map<String, AdfHierarchyTypeDefMetadata>
    hierarchyTypeDefKeys =
        Collections.unmodifiableMap(_hierarchyTypeDefKeys);

    public AdfHierarchyTypeDefMetadata getAdfHierarchyTypeDef(
        String key) {
        AdfHierarchyTypeDefMetadata ret = _hierarchyTypeDefKeys.get(key);
        checkRet("AdfHierarchyTypeDefMetadata", ret, key);
        return ret;
    }

    public Collection<AdfHierarchyTypeDefMetadata>
        getAllHierarchyTypeDefs() {
        return getHierarchyTypeDefKeys().values();
    }

    private final void registerAll(Class<? extends AdfFactory>
        factoryClass) {

        Class<?> curClass = factoryClass;
        val factoryPackageNames = new ArrayList<String>();

        while (AdfFactory.class.isAssignableFrom(curClass)) {
            factoryPackageNames.add(curClass.getPackage().getName());
            curClass = curClass.getSuperclass();
        }
        registerAll(factoryPackageNames);
    }

    private void registerAll(List<String> factoryPackageNames) {
        FilterBuilder filterBuilder = new FilterBuilder()
            .include(
            // must include all super classes..
            FilterBuilder.prefix("za.co.enerweb.ebr.adf.model"));
        for (String factoryPackageName : factoryPackageNames) {
            filterBuilder.include(
                FilterBuilder.prefix(factoryPackageName + ".data_types"));
            filterBuilder.include(
                FilterBuilder.prefix(factoryPackageName + ".model"));
        }
        Reflections reflections = new Reflections(new ConfigurationBuilder()
            .filterInputsBy(filterBuilder)
            .setUrls(Registrar.getPluginFilteredClasspath())
            .setScanners(new SubTypesScanner()));

        registerAllEntities(reflections);
        registerAllDataTypes(reflections);
        List<TypeField> typeFields = new ArrayList<AdfRegistry.TypeField>(
            _entityKeys.size() * 5);
        registerAllTypeDefs(reflections, typeFields);

        for (TypeField typeField : typeFields) {
            addRelationships(typeField.instanceClass, typeField.typeDef,
                typeField.field);
            addHierarchies(typeField.instanceClass, typeField.typeDef,
                typeField.field);
        }
    }

    private void registerAllEntities(Reflections reflections) {
        Set<Class<? extends AdfEntity>> classes =
            reflections.getSubTypesOf(AdfEntity.class);
        for (Class<? extends AdfEntity> entityClass : classes) {
            if (!Modifier.isAbstract(entityClass.getModifiers())) {
                AdfEntity entity;
                try {
                    entity = entityClass.newInstance();
                } catch (Exception e) {
                    log.warn("Could not register ADF entity: "
                        + entityClass.getName()
                        + " (check if it has a public no-args constructor)", e);
                    continue;
                }
                AdfEntity put1 =
                    _entityClasses.put(
                        (Class<? extends AdfEntity>) entityClass, entity);
                AdfEntity put2 =
                    _entityKeys.put(entity.getKey(), entity);
                if (put1 != null || put2 != null) {
                    log.warn("Entity is being registered more than once," +
                        "this propably points to registerAllEntities() or" +
                        "checkMetadata() being called at an " +
                        "inappropriate time.");
                }
            }
        }
    }

    @SuppressWarnings("rawtypes")
    private void registerAllDataTypes(Reflections reflections) {
        Set<Class<? extends AdfDataType>> classes =
            reflections.getSubTypesOf(AdfDataType.class);
        for (Class<? extends AdfDataType> dtClass : classes) {
            if (!Modifier.isAbstract(dtClass.getModifiers())) {
                AdfDataType dt;
                try {
                    dt = dtClass.newInstance();
                } catch (Exception e) {
                    log.warn("Could not register ADF data type: "
                        + dtClass.getName()
                        + " (check if it has a public no-args constructor)", e);
                    continue;
                }
                @SuppressWarnings("unchecked") AdfDataType<?> put1 =
                    _dataTypeClasses.put(
                        (Class<? extends AdfDataType<?>>) dtClass, dt);
                AdfDataType<?> put2 = _dataTypeKeys.put(dt.getKey(), dt);
                if (put1 != null || put2 != null) {
                    log.warn("Datatype is being registered more than once," +
                        "this propably points to registerAllDataTypes() or" +
                        "checkMetadata() being called at an " +
                        "inappropriate time.");
                }
            }
        }
    }

    private void registerAllTypeDefs(Reflections reflections,
        List<TypeField> typeFields) {
        Set<Class<? extends AdfInstance>> classes =
            reflections.getSubTypesOf(AdfInstance.class);
        for (Class<? extends AdfInstance> instanceClass : classes) {
            // if (!Modifier.isAbstract(instanceClass.getModifiers())) {
                AdfInstanceTypeDef adfInstanceTypeDef =
                    instanceClass.getAnnotation(AdfInstanceTypeDef.class);
                if (adfInstanceTypeDef != null) {
                    addTypeDef(instanceClass, adfInstanceTypeDef, typeFields);
                } else {
                    log.error("AdfInstance type is not annotated with " +
                    "@AdfInstanceTypeDef. It will be ignored: "
                    + instanceClass.getName());
                }
            // }
        }
    }

    private void addTypeDef(Class<? extends AdfInstance> instanceClass,
        AdfInstanceTypeDef adfInstanceTypeDef, List<TypeField> typeFields) {
        AdfTypeDefMetadata typeDef = new AdfTypeDefMetadata();
        typeDef.setAbstractSupertype(
            Modifier.isAbstract(instanceClass.getModifiers()));
        typeDef.init(adfInstanceTypeDef, instanceClass);

        Class<?> curClass = instanceClass;
        while (AdfInstance.class.isAssignableFrom(curClass)) {
            Field[] fields = curClass.getDeclaredFields();
            for (Field field : fields) {
                if (!Modifier.isStatic(field.getModifiers())) {
                    AdfParamDef paramDef =
                        field.getAnnotation(AdfParamDef.class);
                    if (paramDef != null) {
                        typeDef.addParamDef(field, paramDef);
                    }
                    typeFields.add(new TypeField(instanceClass,
                        // (Class<? extends AdfInstance>)curClass,
                        field, typeDef));
//                    either just use the saved classes
//                    or store a complex object with non-static fields
//                    per classdef that have to be processed in second pass..
//                    addRelationships(instanceClass, typeDef, field);
//                    addHierarchies(instanceClass, typeDef, field);
                }
            }
            curClass = curClass.getSuperclass();
        }

        AdfTypeDefMetadata put1 =
            _typeDefs.put(
                (Class<? extends AdfInstance>) instanceClass,
                typeDef);
        AdfTypeDefMetadata put2 =
            _typeDefKeys.put(typeDef.getKey(), typeDef);
        if (put1 != null || put2 != null) {
            log.warn("AdfInstanceTypeDef is being registered " +
                "more than once, this propably points to " +
                "registerAllTypeDefs() or" +
                "checkMetadata() being called at an " +
                "inappropriate time.");
        }
    }

    private void addRelationships(Class<? extends AdfInstance> instanceClass,
        AdfTypeDefMetadata typeDef, Field field) {
        AdfRelationshipFrom relationshipFrom = field
            .getAnnotation(AdfRelationshipFrom.class);
        if (relationshipFrom != null) {
            AdfRelationshipTypeDefMetadata relTypeDef =
                getOrCreateRelTypeDef(relationshipFrom.key());
            relTypeDef.setToTypeDef(typeDef); // field is from so this is to
            // if the inverse relationship exists
            // then this should populate itself..
            AdfTypeDefMetadata fromTypeDef = setRelationshipsTypeDef(
                relTypeDef, typeDef, field, true);
            typeDef.addRelationship(field, relTypeDef, fromTypeDef, false);
            validateInverse(instanceClass, field, typeDef);
        }

        AdfRelationshipTo relationshipTo = field
            .getAnnotation(AdfRelationshipTo.class);
        if (relationshipTo != null) {
            // register type from this to to
            AdfRelationshipTypeDefMetadata relTypeDef =
                getOrCreateRelTypeDef(relationshipTo.key());
            relTypeDef.init(typeDef, relationshipTo);
            relTypeDef.setFromTypeDef(typeDef); // field is to so this is from

            // typeDef.addRelationship(field, typeDef, false); // add from

            // find the inverse connection
            // look up related type
            // if it exist update it
            AdfTypeDefMetadata toTypeDef = setRelationshipsTypeDef(
                relTypeDef, typeDef, field, false);

            typeDef.addRelationship(field, relTypeDef, toTypeDef, true);
            validateInverse(instanceClass, field, typeDef);
        }
    }

    private void validateInverse(Class<? extends AdfInstance> instanceClass,
        Field field, AdfTypeDefMetadata otherTypeDef) {
        if (otherTypeDef != null
            && !otherTypeDef.getInstanceClass().equals(instanceClass)) {
            throw new IllegalStateException("Relationship field and it's" +
                " referece are of different types:\n  "
                + field + "\n  " + otherTypeDef.getInstanceClass());
        }
    }

    @SuppressWarnings("unchecked")
    private AdfTypeDefMetadata setRelationshipsTypeDef(
        AdfRelationshipTypeDefMetadata relTypeDef,
        AdfTypeDefMetadata typeDef,
        Field field, boolean isTo) {

        Class<?> type = getFieldType(field);
        if (type == null) {
            log.error("@AdfRelationshipFrom " +
                "on " + field
                + " may only be put on fields" +
                "of type AdfInstance or List<AdfInstance>. " +
                "It will be ignored.");
            return null;
        }

        AdfTypeDefMetadata adfTypeDefMetadata = _typeDefs
            .get((Class<AdfInstance>) type);

        if (adfTypeDefMetadata != null) {
            adfTypeDefMetadata.setRelationshipTypeDef(
                relTypeDef, typeDef, isTo);
        }
        // else it has not been defined.
        return adfTypeDefMetadata;
    }

    @SuppressWarnings("rawtypes")
    private Class<?> getFieldType(Field field) {
        Class<?> type = null;

        if (AdfInstance.class.isAssignableFrom(
            field.getType())) {
            type = field.getType();
        } else if (List.class.isAssignableFrom(
            field.getType())) {
            ParameterizedType ptype =
                (ParameterizedType) field.getGenericType();
            Type[] actualTypeArguments = ptype.getActualTypeArguments();
            if (actualTypeArguments.length > 0
                && actualTypeArguments[0] instanceof Class) {
                if (AdfInstance.class.isAssignableFrom(
                    (Class) actualTypeArguments[0])) {
                    type = (Class) actualTypeArguments[0];
                }
            }
        }
        return type;
    }

    private AdfRelationshipTypeDefMetadata getOrCreateRelTypeDef(
        String relationshipKey) {
        AdfRelationshipTypeDefMetadata relTypeDef =
            _relTypeDefKeys.get(relationshipKey);
        if (relTypeDef == null) {
            relTypeDef = new AdfRelationshipTypeDefMetadata(relationshipKey);
        }
        _relTypeDefKeys.put(relTypeDef.getKey(), relTypeDef);
        return relTypeDef;
    }

    private void addHierarchies(Class<? extends AdfInstance> instanceClass,
        AdfTypeDefMetadata typeDef, Field field) {
        {
            AdfHierarchyParent hierParent = field
                .getAnnotation(AdfHierarchyParent.class);
            if (hierParent != null) {
                AdfHierarchyTypeDefMetadata hierTypeDef =
                    getOrCreateHierTypeDef(hierParent.key(),
                        hierParent.caption(), hierParent.description(),
                        hierParent.entity());
                typeDef.addHierarchy(field, hierTypeDef, false,
                    hierParent.fetch(), hierParent.uniqueFieldPerParent());
            }
        }

        AdfHierarchyChild hierChild = field
            .getAnnotation(AdfHierarchyChild.class);
        if (hierChild != null) {
            AdfHierarchyTypeDefMetadata hierTypeDef =
                getOrCreateHierTypeDef(hierChild.key(),
                    hierChild.caption(), hierChild.description(),
                    hierChild.entity());
            typeDef.addHierarchy(field, hierTypeDef, true,
                hierChild.fetch(), "");
        }
    }

    private AdfHierarchyTypeDefMetadata getOrCreateHierTypeDef(
        String hierarchyKey, String caption, String description,
        Class<? extends AdfHierarchyEntity> entityClass) {
        AdfHierarchyTypeDefMetadata hierTypeDef =
            _hierarchyTypeDefKeys.get(hierarchyKey);
        if (hierTypeDef == null) {
            hierTypeDef = new AdfHierarchyTypeDefMetadata(hierarchyKey);
        }
        hierTypeDef.init(caption, description, entityClass);
        _hierarchyTypeDefKeys.put(hierTypeDef.getKey(), hierTypeDef);
        return hierTypeDef;
    }

    private void checkRet(String what, Object ret, String key) {
        if (ret == null) {
            throw new IllegalArgumentException("Could not find "
                + what + " with key " + key);
        }
    }

    private void checkRet(String what, Object ret, Class<?> key) {
        if (ret == null) {
            throw new IllegalArgumentException("Could not find "
                + what + " for class " + key);
        }
    }

}
