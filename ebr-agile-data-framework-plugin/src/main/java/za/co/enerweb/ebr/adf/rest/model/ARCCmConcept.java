package za.co.enerweb.ebr.adf.rest.model;

import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import za.co.enerweb.toolbox.rest.ExpandMembers;
import za.co.enerweb.toolbox.rest.Meta;

@Data
@EqualsAndHashCode(callSuper = true)
@XmlRootElement
@NoArgsConstructor
@AllArgsConstructor
public abstract class ARCCmConcept<T> extends ARCConcept {

    private Meta meta;

    /**
     * Slug (http://en.wikipedia.org/wiki/Clean_URL#Slug)
     */
    private String id;
    @Getter(onMethod = @_({@XmlTransient}))
    private boolean expand = true;
    @Getter(onMethod = @_({@XmlTransient}))
    protected T cmObject;

    public ARCCmConcept(UriInfo uriInfo, ExpandMembers expand, T cmObject) {
        this.expand = expand.shouldExpand(getUriFragment());
        setCmObject(cmObject);
        initMeta(uriInfo);
    }

    public void initMeta(UriInfo uriInfo) {
        validateFields("id"); // nicer err if id is null
        meta = new AdfMetaBuilder(uriInfo).add(getUriFragment())
            .slash(id).build();
    }

    @XmlTransient
    public abstract String getUriFragment();

    // cool but probably slow and unintuitive :)
//    {
//        return ((String) (getClass().getField("URI_FRAGMENT").get(null)));
//    }

    protected abstract String deriveSlug(T cmObject);

    public static String keyFromSlug(String slug) {
        return splitOnDashes(slug)[0].toUpperCase();
    }

    /**
     * this implementation assumes the id is at the end of the id
     *
     * @param idFieldValue
     * @return
     */
    public static Integer idFromSlug(String slug) {
        return Integer.valueOf(splitOnDashes(slug)[1]);
    }

    protected static String parentSlug(String slug) {
        return splitOnDots(slug)[0];
    }

    protected static String childSlug(String slug) {
        return splitOnDots(slug)[1];
    }


    protected static String[] splitOnDashes(String slug) throws IllegalArgumentException {
        String[] split = slug.split("-");
        if (split.length < 2) {
            throw new IllegalArgumentException("Invalid object reference: " + slug);
        }
        return split;
    }

    protected static String[] splitOnDots(String slug) throws IllegalArgumentException {
        String[] split = slug.split("\\.");
        if (split.length < 2) {
            throw new IllegalArgumentException("Invalid object reference: " + slug);
        }
        return split;
    }

    public void setCmObject(T cmObject) {
        this.cmObject = cmObject;
        this.id = deriveSlug(cmObject);
    }

}
