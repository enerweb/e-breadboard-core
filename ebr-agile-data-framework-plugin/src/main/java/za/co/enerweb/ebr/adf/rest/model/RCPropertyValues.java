/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package za.co.enerweb.ebr.adf.rest.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import za.co.enerweb.toolbox.rest.Items;
import za.co.enerweb.toolbox.rest.Meta;

@XmlRootElement(name = RCPropertyValues.URI_FRAGMENT)
@XmlSeeAlso({RCPropertyValue.class, Items.class})
public class RCPropertyValues extends Items<RCPropertyValue> {

    public final static String URI_FRAGMENT = "PropertyValues";

    public RCPropertyValues() {
        setItems(new ArrayList<RCPropertyValue>());
    }

    public RCPropertyValues(Meta meta, Collection<RCPropertyValue> items) {
        super(meta, items);
    }

    public RCPropertyValues(UriInfo uriInfo, Collection<RCPropertyValue> items) {
        this(
            new AdfMetaBuilder(uriInfo).add(URI_FRAGMENT).build(),
            items);
    }
}
