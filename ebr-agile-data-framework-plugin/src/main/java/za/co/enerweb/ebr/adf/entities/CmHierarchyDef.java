package za.co.enerweb.ebr.adf.entities;

import static za.co.enerweb.ebr.adf.dbl.AdfLowlevelDbLayer.CM_;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import za.co.enerweb.ebr.adf.dbl.AdfDbLayerUtils;
import za.co.enerweb.toolbox.string.StringUtils;

/**

 */

@Data
@NoArgsConstructor
@EqualsAndHashCode(of="id")
@ToString(of={"id", "entity", "key", "caption"})
@Entity
@Table(name = CM_ + CmHierarchyDef.ENTITY_HIERARCHY_DEF)
public class CmHierarchyDef implements Serializable {

    private static final long serialVersionUID = 1L;
    // FIXME: CM_HIERARCHY_DEF => CM_HIERARCHYDEF to look the same as other defs
    public static final String ENTITY_HIERARCHY_DEF = "HIERARCHY_DEF";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;

    @JoinColumn(name = "ENTITY", referencedColumnName = "ENTITY",
        nullable = false)
    @ManyToOne(optional = false)
    private CmEntity cmEntity;

    @Basic(optional = false)
    @Column(name = "ENTITY", insertable = false, updatable = false)
    private String entity;

    @Basic(optional = false)
    @Column(name = "KEY", nullable = false, length = 50)
    private String key;

    @Basic(optional = false)
    @Column(name = "CAPTION", nullable = false, length = 100)
    private String caption;

    @Column(name = "DESCRIPTION", length = 400)
    private String description;

    public CmHierarchyDef(CmEntity cmEntity, String key) {
        setCmEntity(cmEntity);
        this.key = key;
        this.caption = StringUtils.variableName2Caption(key);
    }

    public CmHierarchyDef(CmEntity cmEntity, String key, String caption,
        String description) {
        setCmEntity(cmEntity);
        this.key = key;
        this.caption = caption;
        this.description = description;
    }

    public void setCmEntity(CmEntity cmEntity) {
        this.cmEntity = cmEntity;
        entity = cmEntity.getEntity();
    }

    public String getJpaHierarchyEntity() {
        return AdfDbLayerUtils.efsEntityToJpaHierarchyEntity(
            entity);
    }
}
