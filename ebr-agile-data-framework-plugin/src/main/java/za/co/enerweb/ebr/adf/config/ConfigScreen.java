package za.co.enerweb.ebr.adf.config;

import java.util.Collection;
import java.util.Collections;

import lombok.Setter;
import za.co.enerweb.ebr.adf.AdfFactory;
import za.co.enerweb.ebr.gui.vaadin.components.IocTabSheet;

import com.vaadin.ui.Component;

/**
 * maybe make this more configurable,
 * with a builder api eg. .config(["caption"]).details()
 */
public class ConfigScreen extends IocTabSheet {
    private static final String ENTITY_MANAGER = "Entity Manager";

    private static final long serialVersionUID = 1L;

    private String[] entities;

    @Setter
    private AdfFactory adfFactory; // gets injected

    public void attach() {
        super.attach();

    }

    // @Tab(caption="Config", rank=1, selected=true)
    // public Component createConfig() {
    // return new ConfigTab(adfFactory, ConfigMode.CONFIG,
    // new TreeSpec(entities));
    // }

    // @Tab(caption="Import/Export", rank=10, selected=true)
    // public Component createImportExportTab() {
    // return new ImportExportTab(adfFactory);
    // }

    // @Tab(caption="Details", rank=20)
    // public Component createDetail() {
    // return new ConfigTab(adfFactory, ConfigMode.DETAILS,
    // new TreeSpec(entities));
    // }

    // @Tab(caption = "SETUP", rank = 100)
    // public Component createSetup() {
    // return new ConfigTab(adfFactory, ConfigMode.SETUP,
    // new TreeSpec(entities));
    // }
    public Collection<TabMetadata> getDynamicTabs() {
        // if (EbrFactory.getOrchestrationEngine().isInDevMode()) {
        // return Arrays.asList(new TabMetadata(ENTITY_MANAGER,
        // 1000, false));
        // // this.addTab(c)
        // // AdfEntityManager
        // }
        return Collections.emptyList();
    }

    public Component createDynamicTab(TabMetadata tab) {
        // if (tab.getCaption().equals(ENTITY_MANAGER)) {
        // AdfEntityManagerTab adfEntityManager = new AdfEntityManagerTab();
        // adfEntityManager.setAdfFactory(adfFactory);
        // return adfEntityManager;
        // }
        return super.createDynamicTab(tab);
    }
}
