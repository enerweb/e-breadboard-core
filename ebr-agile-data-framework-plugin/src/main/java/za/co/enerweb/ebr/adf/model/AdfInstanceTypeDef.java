package za.co.enerweb.ebr.adf.model;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface AdfInstanceTypeDef {

    /**
     * The Typedef for this instance
     */
    Class<? extends AdfInstanceEntity> entity();

    /**
     * By default this will be derived from the AdfInstance class
     * that this is annotating.
     */
    String key() default "";

    String caption() default "";

    String description() default "";

}
