package za.co.enerweb.ebr.adf.model;

import static lombok.AccessLevel.PROTECTED;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.Getter;
import lombok.Setter;
import za.co.enerweb.ebr.adf.dbl.AdfDbLayer;
import za.co.enerweb.ebr.adf.entities.CmParamdef;
import za.co.enerweb.ebr.adf.entities.IbInstance;
import za.co.enerweb.toolbox.string.StringUtils;

/**
 * Extend this to make a object definition of your Adf Data Type.
 * It must have a no args constructor.
 * The convention is to prefix it with Dt eg. DtMyDataType
 * and it must be in the package called data_types relative to your AdfFactory
 * T is the java type of this class.
 * TODO: Maybe rename this to DtAdfDataType so it conforms to our convention!
 */
public abstract class AdfDataType<T> {

    private static final Pattern prefixPattern = Pattern.compile(
        "^(Dt)([A-Z].*)");

    @Getter
    String key = deriveKey();

    // vvv these may only be used during conversion
    @Getter(PROTECTED)
    @Setter
    private AdfDbLayer dbLayer;

    @Getter(PROTECTED)
    @Setter
    private IbInstance instance;

    @Getter(PROTECTED)
    @Setter
    private CmParamdef cmParamdef;

    protected String deriveKey() {
        return deriveKey(getClass());
    }

    @SuppressWarnings("unchecked")
    protected <D extends AdfDbLayer> D getDbLayer() {
        return (D) dbLayer;
    }

    protected static String deriveKey(Class<? extends AdfDataType> klass) {
        String ret = klass.getSimpleName();
        Matcher matcher = prefixPattern.matcher(ret);
        if (matcher.matches()) {
            ret = matcher.group(2);
        } else {
            throw new IllegalStateException("Class name does not " +
                "follow the entity name convention " +
                "(prefix it with Dt eg. DtMyEntity) : " + ret);
        }
        return StringUtils.camelCaseToUnderscoreSeparated(ret).toUpperCase();
    }

    public abstract AdfDataTypeClass getAdfDataTypeClass();

    @SuppressWarnings("unchecked")
    public Class<? extends AdfDataType<T>> getDataTypeClass() {
        return (Class<? extends AdfDataType<T>>) getClass();
    }

    public abstract String getDescription();

    public abstract Class<T> getJavaType();

    public abstract String toPersistedValue(T value);

    public abstract T fromPersistedValue(String persistedValue);

    public Class<? extends AdfDataType<?>> getItemDataType() {
        return null;
    }

    public String toString() {
        return this.getKey() + " (" + getDescription() + ")";
    }
}
