package za.co.enerweb.ebr.adf.rest;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.adf.rest.model.RCClass;
import za.co.enerweb.ebr.adf.rest.model.RCObject;
import za.co.enerweb.ebr.adf.rest.model.RCObjects;
import za.co.enerweb.toolbox.rest.ExpandMembers;
import za.co.enerweb.toolbox.rest.ResponsTypeUtil;

@Path("/" + RCClass.URI_FRAGMENT)
@Slf4j
public abstract class ARClass extends ARConcept {

    @Path("/{idFieldValue}" + ResponsTypeUtil.PATH)
    @GET
    public Response get(@Context UriInfo uriInfo,
        @PathParam(ResponsTypeUtil.PARAM) String responseType,
        @QueryParam(ExpandMembers.PARAM) String expand,
        @NotNull @Size(min = 1)
        @PathParam("idFieldValue") String slug) {
        return ResponsTypeUtil.parse(Response.ok(
            getRDbl().getRCClass(uriInfo,
                new ExpandMembers(RCClass.URI_FRAGMENT, expand),
                RCClass.idFromSlug(slug))),
            responseType).build();
    }

    @Path("/{idFieldValue}/" + RCObjects.URI_FRAGMENT + ResponsTypeUtil.PATH)
    @GET
    public Response getObjects(@Context UriInfo uriInfo,
        @PathParam(ResponsTypeUtil.PARAM) String responseType,
        @QueryParam(ExpandMembers.PARAM) String expand,
        @NotNull @Size(min = 1)
        @PathParam("idFieldValue") String slug) {
        return ResponsTypeUtil.parse(Response.ok(
            getRDbl().getRCObjectsOfClass(uriInfo,
                new ExpandMembers(RCObject.URI_FRAGMENT, expand),
                RCClass.typeDefKeyFromSlug(slug))),
            responseType).build();
    }

    @Path("/" + ResponsTypeUtil.PATH)
    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response create(@Context UriInfo uriInfo,
        @PathParam(ResponsTypeUtil.PARAM) String responseType,
        RCClass newRC) {
        newRC = getRDbl().createRCCLass(uriInfo, newRC);
        return Response.created(
            newRC.getMeta().getUri()).entity(newRC).build();
    }
}
