package za.co.enerweb.ebr.adf.config.json;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;

import lombok.extern.slf4j.Slf4j;

import org.json.JSONException;
import org.json.JSONObject;

import za.co.enerweb.ebr.adf.dbl.AdfDbLayer;
import za.co.enerweb.ebr.adf.entities.CmDatatype;
import za.co.enerweb.ebr.adf.entities.CmDatatypeclass;
import za.co.enerweb.ebr.adf.entities.CmEntity;
import za.co.enerweb.ebr.adf.entities.CmEntitytype;
import za.co.enerweb.ebr.adf.entities.CmEnum;
import za.co.enerweb.ebr.adf.entities.CmHierarchyDef;
import za.co.enerweb.ebr.adf.entities.CmParamdef;
import za.co.enerweb.ebr.adf.entities.CmParamkind;
import za.co.enerweb.ebr.adf.entities.CmTypedef;
import za.co.enerweb.ebr.adf.entities.CmUnitofmeasure;
import za.co.enerweb.ebr.server.HomeFile;

@Slf4j
@Stateful
@LocalBean
public class MetadataIO extends AdfDbLayer {

    // table name => JpaJsonConverter class
    private Map<String, Class<? extends JpaJsonConverter<?>>> converters =
        new LinkedHashMap<String, Class<? extends JpaJsonConverter<?>>>();

    public MetadataIO() {
        converters.put("CM_DATATYPECLASS", CmDataTypeClassConverter.class);
        converters.put("CM_DATATYPE", CmDatatypeConverter.class);
        converters.put("CM_ENTITYTYPE", CmEntityTypesConverter.class);
        converters.put("CM_ENTITY", CmEntityConverter.class);
        converters.put("CM_ENUM", CmEnumConverter.class);
        converters.put("CM_HIERARCHY_DEF", CmHierarchyDefConverter.class);
        converters.put("CM_TYPEDEF", CmTypedefConverter.class);
        converters.put("CM_PARAMKIND", CmParamkindConverter.class);
        converters.put("CM_UNITOFMEASURE", CmUnitofmeasureConverter.class);
        converters.put("CM_PARAMDEF", CmParamdefConverter.class);
        // CM_PERIODTYPE

    }

    public static class CmDataTypeClassConverter
        extends JpaJsonConverter<CmDatatypeclass> {
        public Class<CmDatatypeclass> getJpaClass() {
            return CmDatatypeclass.class;
        }
        public List<String> getSimpleColumnNames() {
            return Arrays.asList("KEY", "DESCRIPTION");
        }
    }


    public static class CmDatatypeConverter
        extends JpaJsonConverter<CmDatatype> {
        public Class<CmDatatype> getJpaClass() {
            return CmDatatype.class;
        }
        public List<String> getSimpleColumnNames() {
            return Arrays.asList("KEY", "DESCRIPTION");
        }
        @SuppressWarnings("unchecked")
        public List<? extends FieldConverter> getFieldConverters() {
            return Arrays.asList(new SimpleForeignKey(
                "ITEMDATATYPE_KEY", "itemdatatype", CmDatatype.class,
                "key"),
                new SimpleForeignKey(
                "DATATYPECLASS_KEY", "cmDatatypeclass", CmDatatypeclass.class,
                "key"));
        }
    }

    public static class CmEntityTypesConverter
        extends JpaJsonConverter<CmEntitytype> {
        public Class<CmEntitytype> getJpaClass() {
            return CmEntitytype.class;
        }
        public List<String> getSimpleColumnNames() {
            return Arrays.asList("KEY", "DESCRIPTION");
        }
    }

    public static class CmEntityConverter
        extends JpaJsonConverter<CmEntity> {
        public Class<CmEntity> getJpaClass() {
            return CmEntity.class;
        }
        public List<String> getSimpleColumnNames() {
            return Arrays.asList("ENTITY", "CAPTION");
        }
        @SuppressWarnings("unchecked")
        public List<? extends FieldConverter> getFieldConverters() {
            return Arrays.asList(new SimpleForeignKey(
                "ENTITYTYPE_KEY", "cmEntitytype", CmEntitytype.class, "key"));
        }
    }

    public static class CmEnumConverter
        extends JpaJsonConverter<CmEnum> {
        public Class<CmEnum> getJpaClass() {
            return CmEnum.class;
        }
        public List<String> getSimpleColumnNames() {
            return Arrays.asList("KEY", "CAPTION");
        }
        @SuppressWarnings("unchecked")
        public List<? extends FieldConverter> getFieldConverters() {
            return Arrays.asList(new SimpleForeignKey(
                "DATATYPE_KEY", "cmDatatype", CmDatatype.class,
                "key"));
        }
    }

    public static class CmHierarchyDefConverter
        extends JpaJsonConverter<CmHierarchyDef> {
        public Class<CmHierarchyDef> getJpaClass() {
            return CmHierarchyDef.class;
        }
        public List<String> getSimpleColumnNames() {
            return Arrays.asList("ID", "KEY", "CAPTION", "DESCRIPTION");
        }

        @SuppressWarnings("unchecked")
        public List<? extends FieldConverter> getFieldConverters() {
            return Arrays.asList(new SimpleForeignKey(
                "ENTITY", "cmEntity", CmEntity.class,
                "entity"));
        }
    }

    // should consider identical keys equal too
    //
    public static class CmTypedefConverter
        extends JpaJsonConverter<CmTypedef> {
        public Class<CmTypedef> getJpaClass() {
            return CmTypedef.class;
        }

        public List<String> getSimpleColumnNames() {
            return Arrays.asList("ID", "KEY", "CAPTION", "DESCRIPTION");
        }

        @SuppressWarnings("unchecked")
        public List<? extends FieldConverter> getFieldConverters() {
            return Arrays.asList(new SimpleForeignKey(
                "ENTITY", "cmEntity", CmEntity.class,
                "entity"));
        }
    }

    public static class CmParamkindConverter
        extends JpaJsonConverter<CmParamkind> {
        public Class<CmParamkind> getJpaClass() {
            return CmParamkind.class;
        }

        public List<String> getSimpleColumnNames() {
            return Arrays.asList("KEY", "DESCRIPTION");
        }
    }


    public static class CmUnitofmeasureConverter
        extends JpaJsonConverter<CmUnitofmeasure> {
        public Class<CmUnitofmeasure> getJpaClass() {
            return CmUnitofmeasure.class;
        }

        public List<String> getSimpleColumnNames() {
            return Arrays.asList("KEY", "CAPTION");
        }
    }

    public static class CmParamdefConverter
        extends JpaJsonConverter<CmParamdef> {
        public Class<CmParamdef> getJpaClass() {
            return CmParamdef.class;
        }

        public List<String> getSimpleColumnNames() {
            return Arrays.asList("KEY", "CAPTION", "DESCRIPTION",
                "PARAM_TABLE");
        }

        @SuppressWarnings("unchecked")
        public List<? extends FieldConverter> getFieldConverters() {
            return Arrays.asList(
                new SimpleForeignKey(
                    "TYPEDEF_ID", "cmTypedef", CmTypedef.class,
                    "id"),
                new SimpleForeignKey(
                    "PARAMKIND_KEY", "cmParamkind", CmParamkind.class,
                    "key"),
                new SimpleForeignKey(
                    "DATATYPE_KEY", "cmDatatype", CmDatatype.class,
                    "key"),
                new SimpleForeignKey(
                    "INDX_DATATYPE_KEY", "cmIndexDatatype", CmDatatype.class,
                    "key"),
                new SimpleForeignKey(
                    "UNIT_OF_MEASURE_KEY", "cmUnitofmeasure",
                    CmUnitofmeasure.class,
                    "key")
                );
        }
    }

    public void exportToHomeFile(HomeFile hf)
        throws JSONException, IOException {
        hf.setContent(exportToText());
    }

    public String exportToText()
        throws JSONException, IOException {
        return exportToJson().toString(2) + "\n";
    }

    public void importFromHomeFile(HomeFile hf)
        throws JSONException, IOException {
        importFromJsonText(hf.getContent());
    }

    public void importFromJsonText(String json)
        throws JSONException, IOException {
        importFromJson(new JSONObject(json));
    }

    private JSONObject exportToJson() throws JSONException {
        JSONObject jsonMain = new JSONObject();
        for (Entry<String, Class<? extends JpaJsonConverter<?>>> entry
            : converters.entrySet()) {
            try {
                JpaJsonConverter<?> c = entry.getValue().newInstance();
                jsonMain.put(entry.getKey(), c.exportToJson(this));
            } catch (Exception e) {
                log.error(
                    "Could not export " + entry.getKey() + " "
                        + entry.getValue(), e);
            }
        }

        return jsonMain;
    }

    private void importFromJson(JSONObject jsonMain) throws JSONException {
        for (Entry<String, Class<? extends JpaJsonConverter<?>>> entry
            : converters.entrySet()) {
            String tableName = entry.getKey();
            try {
                if (jsonMain.has(tableName)) {
                    JpaJsonConverter<?> c = entry.getValue().newInstance();
                    c.importFromJson(this, tableName,
                        jsonMain.getJSONArray(tableName));
                }
            } catch (Exception e) {
                log.error(
                    "Could not export " + tableName + " "
                        + entry.getValue(), e);
            }
        }
    }

}

