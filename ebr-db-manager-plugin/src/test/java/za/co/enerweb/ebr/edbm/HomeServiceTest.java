package za.co.enerweb.ebr.edbm;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;

import javax.ejb.EJB;

import lombok.SneakyThrows;

import org.apache.openejb.api.LocalClient;
import org.junit.Test;

import za.co.enerweb.ebr.server.IHomeService;
import za.co.enerweb.toolbox.openejb.AOpenEjbTest;

/*
 * This should only be testing the thin HomeService layer.
 * This only runs with the latest home layout version.
 */
@LocalClient
public class HomeServiceTest extends AOpenEjbTest {
    @EJB
    private IHomeService homeService;
    private File ebrHomeDir;

    @Override
    public void setUpAfterInjection() throws Exception {
        super.setUpAfterInjection();
        // test factory works
        ebrHomeDir = homeService.getMainHomeDir();
    }

    @Test
    public void testTestHome() {
        assertTrue("Make sure we get a test home.",
            ebrHomeDir.getAbsolutePath().contains("ebr-test"));
    }

    @Override
    @SneakyThrows
    public void tearDownAfterClosingContext() {
        super.tearDownAfterClosingContext();
        assertFalse("Home service must clean up test homes",
            ebrHomeDir.exists());
    }
}
