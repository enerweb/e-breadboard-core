package za.co.enerweb.ebr.edbm.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"key", "entitytype"})
@ToString(of={"key", "caption", "entitytype"})
@Entity
@Table(name = "TEST_ENTITY")
public class DummyEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "KEY", nullable = false, length = 25)
    private String key;
    @Basic(optional = false)
    @Column(name = "CAPTION", nullable = false, length = 100)
    private String caption;
    @JoinColumn(name = "ENTITYTYPE_KEY", referencedColumnName = "KEY", nullable = false)
    @ManyToOne(optional = false)
    private DummyEntityType entitytype;

}
