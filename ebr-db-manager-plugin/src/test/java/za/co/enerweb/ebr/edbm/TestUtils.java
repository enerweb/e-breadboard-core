package za.co.enerweb.ebr.edbm;

import lombok.val;
import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.edbm.dbl.EdbmDbLayer;

public abstract class TestUtils {

    public static DbSpec getDbSpec() {
        val ret = new H2EmbeddedDbSpec();
        ret.setPersitenceUnitName("edbm-test-pu");
        ret.setProperty("qualifiedPackageName", "edbm-test-package");
        return ret;
    }

    public static EdbmDbLayer getEdbmDbLayer() {
        EdbmDbLayer ret = EbrFactory.lookupEjb(
            DBManagerFactory.MODULE_NAME, EdbmDbLayer.class);
        ret.setDbSpec(getDbSpec());
        return ret;
    }
}
