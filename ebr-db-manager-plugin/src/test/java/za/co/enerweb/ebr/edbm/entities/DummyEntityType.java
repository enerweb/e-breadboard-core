package za.co.enerweb.ebr.edbm.entities;

import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"key"})
@ToString(of = {"key", "caption"})
@Entity
@Table(name = "TEST_ENTITY_TYPE")
public class DummyEntityType {
    @Id
    @Basic(optional = false)
    @Column(name = "KEY", nullable = false, length = 50)
    private String key;
    @Basic(optional = false)
    @Column(name = "CAPTION", nullable = false, length = 100)
    private String caption;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "entitytype")
    private Collection<DummyEntity> entities;

    public DummyEntityType(String key, String caption) {
        this.key = key;
        this.caption = caption;
    }
}
