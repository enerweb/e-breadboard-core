package za.co.enerweb.ebr.edbm;

import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.File;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;

import lombok.extern.slf4j.Slf4j;

import org.apache.openejb.api.LocalClient;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import za.co.enerweb.ebr.edbm.dbl.EdbmDbLayer;
import za.co.enerweb.ebr.edbm.entities.DummyEntityType;
import za.co.enerweb.ebr.server.AEbrTest;
import za.co.enerweb.ebr.server.IHomeService;

@Slf4j
@LocalClient
public class TestDBManagerFactory extends AEbrTest {

    @EJB
    private IHomeService homeService;

    public static class DummyDBManagerFactory extends
        DBManagerFactory {

        @Override
        public String getModuleName() {
            return "dummy-module";
        }

        public DummyDbLayer getDbLayer() {
            return getDbLayer("edbm-test-pu", DummyDbLayer.class);
        }

        public DummyDbLayer getDbLayerByPersistanceUnit() {
            return getDbLayer("edbm-test-pu");
        }

        public DummyDbLayer getDbLayerById() {
            return getDbLayer("edbm-test-pu", "edbm-test-ID");
        }

        public DummyDbLayer getMemDbLayer() {
            return getDbLayer("edbm-test-pu", "edbm-test-MEM");
        }

        public DummyDbLayer getLocalFileDbLayer() {
            return getDbLayer("edbm-test-pu", "edbm-test-LOCAL_FILE");
        }

        public DummyDbLayer
            getDynamicLocalFileDbLayer(String absPath) {
            H2LocalFileDbSpec dbSpec = getDbSpec("edbm-test-pu",
                "edbm-test-DYNAMIC_LOCAL_FILE");
            dbSpec.setFile(absPath);
            return getDbLayer(dbSpec);
        }

        @SuppressWarnings("unchecked")
        public <D extends EdbmDbLayer> Class<D> getDefaultDbLayerClass() {
            return (Class<D>) DummyDbLayer.class;
        }
    }

    @Stateful
    @LocalBean
    public static class DummyDbLayer extends EdbmDbLayer {

    }

    private DummyDBManagerFactory factory;
    private DummyDbLayer dbLayer;
    private DummyEntityType testEntityType;
    @Rule
    public TestName testName = new TestName();

    @Override
    public void setUpAfterInjection() throws Exception {
        super.setUpAfterInjection();
        factory = new DummyDBManagerFactory();
    }

    private void initDbLayer() {
        dbLayer = factory.getDbLayer();
        addEntity();
    }

    private void addEntity() {
        String typeKey = testName.getMethodName();
        testEntityType = new DummyEntityType(typeKey,
            typeKey + " caption");
        dbLayer.save(testEntityType);
    }

    @Test
    public void getDbLayerByPersistanceUnit() {
        dbLayer = factory.getDbLayerByPersistanceUnit();
        assertEquals("mydbname", dbLayer.getDbSpec().getProperty("name"));
    }

    @Test
    public void getDbLayerById() {
        dbLayer = factory.getDbLayerById();
        assertEquals("mydbnameById", dbLayer.getDbSpec().getProperty("name"));
    }

    @Test
    public void getMemDbLayer() {
        dbLayer = factory.getMemDbLayer();
        assertEquals("myMEMdbname", dbLayer.getDbSpec().getProperty("name"));
        assertThat(dbLayer.getDbSpec().getJdbcUrl(),
            startsWith("jdbc:log4jdbc:h2:mem:myMEMdbname"));

        addEntity();
        assertFindEntity();
    }

    @Test
    public void getLocalFileDbLayer() {
        dbLayer = factory.getLocalFileDbLayer();
        assertThat(dbLayer.getDbSpec().getProperty("file"),
            endsWith("target/classes/myLOCAL_FILEdb"));
        assertThat(dbLayer.getDbSpec().getJdbcUrl(),
            startsWith("jdbc:log4jdbc:h2:file:"));

        addEntity();
        assertFindEntity();
    }

    @Test
    public void getDynamicLocalFileDbLayer() {
        File file = homeService.getTempFile("dummmyFile").getFile();
        String absPath = file.getAbsolutePath();
        dbLayer = factory.getDynamicLocalFileDbLayer(absPath);
        assertThat(((H2LocalFileDbSpec) dbLayer.getDbSpec()).getFile(),
            equalTo(absPath));
        assertThat(dbLayer.getDbSpec().getJdbcUrl(),
            startsWith("jdbc:log4jdbc:h2:file:" + absPath));

        addEntity();
        assertFindEntity();
    }

    @Test
    public void testQuery() {
        initDbLayer();
        assertFindEntity();
    }

    private void assertFindEntity() {
        DummyEntityType foundEt = dbLayer.find(DummyEntityType.class,
            testEntityType.getKey());
        assertEquals(testEntityType.getKey(), foundEt.getKey());
        assertEquals(testEntityType.getCaption(), foundEt.getCaption());
    }

}
