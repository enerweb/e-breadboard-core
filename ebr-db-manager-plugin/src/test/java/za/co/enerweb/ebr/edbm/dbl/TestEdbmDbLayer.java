package za.co.enerweb.ebr.edbm.dbl;


import org.apache.openejb.api.LocalClient;
import org.junit.Test;

import za.co.enerweb.ebr.edbm.entities.DummyEntityType;

@LocalClient
public class TestEdbmDbLayer extends AEdbmDbLayerTest {
    @Test
    public void testFindDummyEntityType() {
        DummyEntityType foundTestEntityType = dbl.find(DummyEntityType.class,
            testEntityType.getKey());
        assertEqualEntityTypes(testEntityType, foundTestEntityType);
    }

}
