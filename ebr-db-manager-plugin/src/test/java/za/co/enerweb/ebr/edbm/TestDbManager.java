package za.co.enerweb.ebr.edbm;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import lombok.extern.slf4j.Slf4j;

import org.apache.openejb.api.LocalClient;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import za.co.enerweb.ebr.edbm.entities.DummyEntityType;
import za.co.enerweb.ebr.server.AEbrTest;
import za.co.enerweb.ebr.server.IHomeService;

@Slf4j
@LocalClient
public class TestDbManager extends AEbrTest {
    @Rule
    public TestName testName = new TestName();

    @EJB
    private IDbManager dbManager;

    @EJB
    private IHomeService homeService;

    private DbSpec dbSpec;
    private EntityManager em;
    private DummyEntityType testEntityType;

    @Override
    public void setUpAfterInjection() throws Exception {
        super.setUpAfterInjection();

        // dbManager = EbrFactory.lookupEjb(IDbManager.class);

        Assert.assertNotNull(homeService);
        Assert.assertNotNull(dbManager);
        log.debug("dbManager: " + dbManager);
        dbSpec = TestUtils.getDbSpec();
        em = dbManager.getEntityManager(dbSpec);
        Assert.assertNotNull(em);

        String typeKey = testName.getMethodName();
        testEntityType = new DummyEntityType(typeKey,
            typeKey + " caption");
        em.getTransaction().begin();
        em.persist(testEntityType);
        em.getTransaction().commit();
    }

    // @After
    // public void cleanup() {
    // EbrFactory.close();
    // }

    @Test
    public void testQuery() {
        DummyEntityType foundEt = em.find(DummyEntityType.class,
            testEntityType.getKey());
        assertEquals(testEntityType.getKey(), foundEt.getKey());
        assertEquals(testEntityType.getCaption(), foundEt.getCaption());
    }

    @Test
    public void testNativeQuery() {
        Query q = em.createNativeQuery(
            "SELECT KEY, CAPTION FROM TEST_ENTITY_TYPE "
                + "WHERE KEY = :key "
                + "order by KEY");
        q.setParameter("key", testEntityType.getKey());
        Object[] foundEt = (Object[]) q.getSingleResult();
        log.debug(Arrays.toString(foundEt));
        assertEquals(testEntityType.getKey(), (String) foundEt[0]);
        assertEquals(testEntityType.getCaption(), (String) foundEt[1]);
    }

    // @Test
    // public void getSaveAndLoadDb() throws DbLayerException {
    // // make sure the db is open
    // dbManager.getWorkingEntityManager();
    // // save the working db
    // // open the save db
    // // load into the working copy
    // // open a connection
    // }
}
