package za.co.enerweb.ebr.edbm.dbl;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.rules.TestName;

import za.co.enerweb.ebr.edbm.TestUtils;
import za.co.enerweb.ebr.edbm.entities.DummyEntityType;
import za.co.enerweb.toolbox.openejb.AOpenEjbTest;

public abstract class AEdbmDbLayerTest
    extends AOpenEjbTest {

    @Rule
    public TestName testName = new TestName();

    protected EdbmDbLayer dbl;
    protected DummyEntityType testEntityType;

    protected EdbmDbLayer createDbLayer(){
        return TestUtils.getEdbmDbLayer();
    }

    public void setUpAfterInjection()  throws Exception {
        dbl = createDbLayer();
        dbl.setDbSpec(TestUtils.getDbSpec());
        Assert.assertNotNull(dbl);

        // add some test data
        String typeKey = testName.getMethodName();
        testEntityType = new DummyEntityType(typeKey,
            typeKey + " caption");
        dbl.save(testEntityType);
    }

    protected void assertEqualEntityTypes(
            DummyEntityType expectedTestEntityType,
            DummyEntityType foundTestEntityType) {
        assertEquals(expectedTestEntityType.getKey(),
            foundTestEntityType.getKey());
        assertEquals(expectedTestEntityType.getCaption(),
            foundTestEntityType.getCaption());
    }
}
