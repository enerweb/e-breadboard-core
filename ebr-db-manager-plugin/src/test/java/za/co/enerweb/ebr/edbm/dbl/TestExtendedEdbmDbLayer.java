package za.co.enerweb.ebr.edbm.dbl;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;

import org.apache.openejb.api.LocalClient;
import org.junit.Test;

import za.co.enerweb.ebr.edbm.entities.DummyEntityType;

@LocalClient
public class TestExtendedEdbmDbLayer extends AEdbmDbLayerTest {

    @Stateful
    @LocalBean
    public static class ExtendedEdbmDbLayer extends EdbmDbLayer {
        public <T> T extendedFind(Class<T> klass, Object primaryKey) {
            return em.find(klass, primaryKey);
        }
    }

    @EJB
    private ExtendedEdbmDbLayer dbl;

    protected EdbmDbLayer createDbLayer() {
        return (EdbmDbLayer) dbl;
    }

    /*
     * make sure inherited methods work
     */
    @Test
    public void testFindDummyEntityType() {
        DummyEntityType foundTestEntityType = dbl.find(DummyEntityType.class,
            testEntityType.getKey());
        assertEqualEntityTypes(testEntityType, foundTestEntityType);
    }

    /*
    * make sure child methods work
    */
    @Test
    public void testExtendedFindDummyEntityType() {
        DummyEntityType foundTestEntityType = dbl.extendedFind(
            DummyEntityType.class, testEntityType.getKey());
        assertEqualEntityTypes(testEntityType, foundTestEntityType);
    }
}
