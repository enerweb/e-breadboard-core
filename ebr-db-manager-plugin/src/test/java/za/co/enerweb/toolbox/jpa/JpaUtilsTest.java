package za.co.enerweb.toolbox.jpa;

import static org.junit.Assert.assertEquals;

import javax.persistence.Id;
import javax.persistence.Table;

import junit.framework.Assert;
import lombok.Data;

import org.junit.Test;

public class JpaUtilsTest {

    @Data
    public static class DummyEntityWithAtId {
        @Id
        private String id;
    }

    @Data
    public static class DummyEntityWithoutAtId {
        private String id;
    }

    @Test
    public void testDetectIdField() {
        assertEquals("id", JpaUtils.detectIdField(DummyEntityWithAtId.class));
        Assert.assertNull(JpaUtils.detectIdField(DummyEntityWithoutAtId.class)
            );
    }

    @Data
    @Table
    public static class DummyEntityWithAtTable {
        private String id;
    }

    @Data
    @Table(name = "WithAtTableName")
    public static class DummyEntityWithAtTableName {
        private String id;
    }

    @Data
    public static class DummyEntityWithoutAtTable {
        private String id;
    }

    @Test
    public void testDetectTableName() {
        assertEquals("DUMMY_ENTITY_WITH_AT_TABLE",
            JpaUtils.detectTableName(DummyEntityWithAtTable.class));
        assertEquals("WithAtTableName",
            JpaUtils.detectTableName(DummyEntityWithAtTableName.class));
        assertEquals("DUMMY_ENTITY_WITHOUT_AT_TABLE",
            JpaUtils.detectTableName(DummyEntityWithoutAtTable.class));
    }

}
