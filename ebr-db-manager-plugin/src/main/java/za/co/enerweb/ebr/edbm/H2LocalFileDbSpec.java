package za.co.enerweb.ebr.edbm;

import static za.co.enerweb.ebr.edbm.H2EmbeddedDbSpec.H2_DB_EXTENTION;

import java.io.File;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;

/**
 * Specifies an embedded H2 db, which gets stored in the resources dir.
 */
@Slf4j
public class H2LocalFileDbSpec extends DbSpec {
    private static final String EXTENTION = "." + H2_DB_EXTENTION;

    @Getter
    @Setter
    private String file;

    protected void initProperties(Map<String, String> section) {
        super.initProperties(section);
        file = getProperty("file");
        if (!file.endsWith(EXTENTION)) {
            file += EXTENTION;
        }
    }

    public String getDbAbsPathBaseName() {
        return StringUtils.replace(file, EXTENTION, "");
    }

    @Override
    protected void initBeforeConnecting() {
        H2EmbeddedDbSpec.updateH2Properties(this, getDbAbsPathBaseName());

        File dbFile = getDbFile();
        log.debug("dbFile:" + dbFile.getAbsolutePath());
        if (!dbFile.exists()) {
            // maybe we can generate a db if we have a script..
            log.warn("Db does not exist: "
                + dbFile.getAbsolutePath());
        }
    }

    public File getDbFile() {
        return new File(file);
    }
}
