package za.co.enerweb.ebr.edbm;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.annotation.PreDestroy;
import javax.ejb.Asynchronous;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.pool.KeyedObjectPool;

import za.co.enerweb.ebr.edbm.internal.EdbmEntityManager;
import za.co.enerweb.ebr.edbm.internal.EntityManagerPoolFactory;
import za.co.enerweb.ebr.server.IExecutor;
import za.co.enerweb.ebr.server.IHomeService;

/**
 * create working db
 * save working db
 * - can we just copy the file live?
 * - flush it first
 * - other components may have open connections
 * load - can we copy?
 * we may need to display content from saved databases later
 * so we should keep a map of db ems so that we can flush and close them
 * when we need to copy/save it.
 * or should we manually copy each table?
 * Doing a copy should lock the db wait for all entity managers to be closed,
 * copy it and then unlock it.
 * mmm from h2database docs:
 * "To backup data while the database is running,
 * the SQL command SCRIPT can be used."
 */
@Slf4j
@Singleton
@DependsOn({"HomeService", "Executor"})
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class DbManager implements IDbManager {

    private KeyedObjectPool entityManagerPool;
    private Map<DbSpec, EntityManagerFactory> entityManagerFactories;

    @EJB
    private IHomeService homeService;
    @EJB
    IExecutor executor;

    public DbManager() {
        init();
    }

    // @PostConstruct
    private void init() {
        if (entityManagerFactories == null) {
            entityManagerFactories =
                new HashMap<DbSpec, EntityManagerFactory>();
        }
        if (entityManagerPool == null) {
            entityManagerPool = EntityManagerPoolFactory
                .createKeyedObjectPool();
        }
    }

    @PreDestroy
    private void close() {
        // make sure we don't give away any more connections.
        KeyedObjectPool entityManagerPool2 = entityManagerPool;
        entityManagerPool = null;

        // close all the entity manager factories
        for (Iterator<Entry<DbSpec, EntityManagerFactory>> iterator =
            entityManagerFactories.entrySet().iterator(); iterator.hasNext();) {
            Entry<DbSpec, EntityManagerFactory> e = iterator.next();
            e.getValue().close();
            iterator.remove();
        }
        entityManagerFactories = null;

        if (entityManagerPool2 != null) {
            try {

                entityManagerPool2.close();

                entityManagerPool2.clear();

                // wait for entityManagerPool.getNumActive() to be zero
                // int sleepms = 250;
                // while (entityManagerPool2.getNumActive() > 0) {
                // log.warn("Waiting for entityManagerPool to close, " +
                // "some entity manager are not being released. " +
                // sleepms);
                // Thread.sleep(sleepms);
                // sleepms += sleepms;
                // }
            } catch (Exception e) {
                log.warn("Could not close the EntityManagerPool.", e);
            }
        }
    }

    @Override
    public void reset() {
        close();
        init();
    }

    @Override
    public EntityManager getEntityManager(DbSpec dbSpec) {
        dbSpec.setHomeService(homeService);
        try {
            // log.debug(">>>>>>>> borrowObject: " + dbSpec);
            return (EntityManager) entityManagerPool.borrowObject(dbSpec);
        } catch (EJBException e) {
            throw e;
        } catch (Exception e) {
            log.info("Failed to get db connection for: " + dbSpec);
            throw new DbManagerException("Could not obtain an entity manager "
                + "in a resonable amount of time.\n Make sure you close the "
                + "entity manager you obtain or increase the pool size.", e);
        }
    }

    @Asynchronous
    @Override
    public void warmUp(DbSpec dbSpec) {
        // log.debug("Warming up: " + dbSpec);
        // just open the working db and return it to the pool
        getEntityManager(dbSpec).close();
    }

    public EdbmEntityManager createEntityManager(final DbSpec dbSpec)
        throws Exception {
        Future<EdbmEntityManager> future =
            executor.submit(new Callable<EdbmEntityManager>() {

                @Override
                public EdbmEntityManager call() throws Exception {

                    try {
                        return new EdbmEntityManager(
                            getEntityManagerFactory(dbSpec)
                                .createEntityManager(), dbSpec);
                    } catch (Exception e) {
                        throw new Exception(
                            "Could not connect to database: " + dbSpec, e);
                    }
                }
            });
        int timeoutSeconds = dbSpec.getTimeoutSeconds();
        try {
            return future.get(timeoutSeconds, TimeUnit.SECONDS);
        } catch (Exception e) {
            throw new Exception("Could not connect to database in "
                + timeoutSeconds + "s: "
                + dbSpec, e);
        }
    }

    private EntityManagerFactory getEntityManagerFactory(DbSpec dbSpec) {
        EntityManagerFactory emf;
        emf = entityManagerFactories.get(dbSpec);
        if (emf == null) {
            synchronized (entityManagerFactories) {
                emf = entityManagerFactories.get(dbSpec);
                if (emf == null) {
                    emf = dbSpec.createEntityManagerFactory();
                    entityManagerFactories.put(dbSpec, emf);
                }
            }
        }
        return emf;
    }

    public void returnEntityManager(EntityManager em, DbSpec dbSpec) {
        // log.debug("<<<<<<<< returnEntityManager: " + dbSpec);
        try {
            entityManagerPool.returnObject(dbSpec, em);
        } catch (Exception e) {
            log.warn("Could not return entiy manager to the pool: " + dbSpec
                , e);
            em.close(); // close it then
        }
    }

    // @Override
    // public DbMetadata getDbMetadata(String name) {
    //
    // }
    //
    // @Override
    // public void setDbMetadata(DbMetadata dbMetadata) {
    // // maybe just allow setting subsections
    // // eg. has_load_results = true.
    // }
    //
    // @Override
    // public List<DbMetadata> getAvailableDbs() {
    // // scan dir or read metadb
    // }

    @Override
    public void copyDb(DbSpec from, DbSpec to) {
        // close applicable connections
        // validate name \w{3,40}
        // TODO: move existing one to trash
        // copy from to
        throw new UnsupportedOperationException();
    }

}
