package za.co.enerweb.ebr.edbm.dbl;

public class EnitityNotFoundException extends RuntimeException {

    public EnitityNotFoundException() {
    }

    public EnitityNotFoundException(String message) {
        super(message);
    }

    public EnitityNotFoundException(Throwable cause) {
        super(cause);
    }

    public EnitityNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
