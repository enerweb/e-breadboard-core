package za.co.enerweb.ebr.edbm.internal;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.pool.KeyedObjectPool;
import org.apache.commons.pool.KeyedPoolableObjectFactory;
import org.apache.commons.pool.impl.GenericKeyedObjectPool;

import za.co.enerweb.ebr.edbm.DBManagerFactory;
import za.co.enerweb.ebr.edbm.DbSpec;

/*
 * http://www.fasterj.com/cartoon/cartoon114.shtml
 */
@Slf4j
public class EntityManagerPoolFactory implements KeyedPoolableObjectFactory {

    // make sure our apps behave
    private static final int MAX_CONCURRENT_CONNECTIONS = 45;
    private static final int MILLISECONDS_TO_WAIT = 20000;

    public static KeyedObjectPool createKeyedObjectPool() {
        GenericKeyedObjectPool ret = new GenericKeyedObjectPool(
            new EntityManagerPoolFactory());
        ret.setMaxActive(MAX_CONCURRENT_CONNECTIONS);
        ret.setMaxIdle(10);
        ret.setMaxTotal(MAX_CONCURRENT_CONNECTIONS);
        ret.setMaxWait(MILLISECONDS_TO_WAIT);
        ret.setTestOnBorrow(true);
        ret.setTestOnReturn(true);
        return ret;
    }

    @Override
    public void activateObject(Object key, Object obj) throws Exception {
    }

    @Override
    public void destroyObject(Object key, Object obj) throws Exception {
        EdbmEntityManager em = (EdbmEntityManager)obj;
        if (em.isOpen()) {
            log.debug("Closing EntityManager: " + key);
            em.flush();
            em.reallyClose();
        }
    }

    @Override
    public Object makeObject(Object key) throws Exception {
        return DBManagerFactory.getDbManager().createEntityManager(
            (DbSpec) key);
    }

    @Override
    public void passivateObject(Object key, Object obj) throws Exception {
        EdbmEntityManager em = (EdbmEntityManager)obj;
        if (em.getTransaction().isActive()) {
            em.getTransaction().rollback();
        }
        em.clear();
    }

    @Override
    public boolean validateObject(Object key, Object obj) {
        EdbmEntityManager em = (EdbmEntityManager)obj;
        return em.isOpen() && !em.getTransaction().isActive();
    }


}
