package za.co.enerweb.ebr.edbm;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;

/*
 * The JDBC URL format for MySQL Connector/J is as follows,
 * with items in square brackets ([, ]) being optional:

jdbc:mysql://[host][,failoverhost...][:port]/[database] »
[?propertyName1][=propertyValue1][&propertyName2][=propertyValue2]...
If the host name is not specified, it defaults to 127.0.0.1.
If the port is not specified, it defaults to 3306, the default port number
for MySQL servers.

jdbc:mysql://[host:port],[host:port].../[database] »
[?propertyName1][=propertyValue1][&propertyName2][=propertyValue2]...
 */
@Slf4j
public class MySqlDbSpec extends DbSpec {

    @Override
    protected void initBeforeConnecting() {
        String url = "jdbc:log4jdbc:mysql://"
            + getProperty("host", "127.0.0.1")
            + ":" + getProperty("port", "3306")
            + "/" + getProperty("name");
        // when giving this url to
        String externalUrl = StringUtils.replace(url, ":log4jdbc:", ":");
        log.debug("connecting to db url: " + externalUrl);
        setProperty(JDBC_URL, url);
        // setProperty("hibernate.connection.username", username);
        // setProperty("hibernate.connection.password", password);
    }

}
