package za.co.enerweb.ebr.edbm;

import static java.lang.String.format;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.server.IConfigurationService;
import za.co.enerweb.ebr.server.IHomeService;

/**
 * Generic specification of how to find a database.
 * The only current implementation is H2EmbeddedDbSpec.
 */
@Slf4j
@EqualsAndHashCode(of = "id")
// @Data
public abstract class DbSpec implements Serializable {

    protected static String RP_DBS = "dbs";
    protected static final String JDBC_URL =
        "javax.persistence.jdbc.url";

    @Setter(value = AccessLevel.PACKAGE)
    @Getter
    private String id;

    @Setter(value = AccessLevel.PACKAGE)
    @Getter
    private String persitenceUnitName;

    @Setter(value = AccessLevel.PROTECTED)
    @Getter
    private boolean autoUpdateSchema = false;

    @Getter
    private int timeoutSeconds = 15;

    /*
     * is set before initBeforeConnecting(), createEntityManagerFactory(),
     * are called
     */
    @Getter(AccessLevel.PROTECTED)
    @Setter(AccessLevel.PROTECTED)
    private transient IHomeService homeService;

    private Map<String, String> properties = new HashMap<>();

    protected void setProperty(String key, String value) {
        setProperty(key, value, false);
    }

    protected void setProperty(String key, String value, boolean forceOverride) {
        if(forceOverride || !properties.containsKey(key)) {
            properties.put(key, value);
        }
    }

    public String getProperty(String key) {
        return properties.get(key);
    }

    public String getProperty(String key, String defaultValue) {
        String ret = properties.get(key);
        if (ret == null) {
            return defaultValue;
        }
        return ret;
    }

    public String getRequiredProperty(String key) {
        String ret = properties.get(key);
        if (ret == null) {
            throw new DbConfigurationException(format("Required property '%s' "
                + "is not defined for db '%s' in %s.",
                key, getPersitenceUnitName(), IConfigurationService.FILENAME));
        }
        return ret;
    }

    /**
     * Do final setup and checks before attempting connection
     * (eg. copy non-existant embedded db over from a template)
     */
    protected abstract void initBeforeConnecting();

    /**
     * call super if you override this!
     * @param section
     */
    protected void initProperties(Map<String, String> section) {
        for (Map.Entry<String, String> entry : section.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            properties.put(key, value);
            switch (key) {
            case "user":
            case "password":
                properties.put("javax.persistence.jdbc." + key, value);
                break;
            case "autoUpdateSchema":
                autoUpdateSchema = Boolean.valueOf(value);
                break;
            case "timeoutSeconds":
                timeoutSeconds = Integer.valueOf(value);
                break;

            }
        }
    }

    public final EntityManagerFactory createEntityManagerFactory() {
        // log.debug("********** createEntityManagerFactory: " + this);
        initBeforeConnecting();
        if (autoUpdateSchema) {
            properties.put("hibernate.hbm2ddl.auto", "update");
            properties.put("eclipselink.ddl-generation", "create-tables");
            properties
                .put("eclipselink.ddl-generation.output-mode", "database");
        }
        EntityManagerFactory ret = Persistence.createEntityManagerFactory(
            getPersitenceUnitName(),
            properties);
        return ret;
    }

    @Override
    public String toString() {
        return persitenceUnitName;
    }

    public Map<String, String> getProperties() {
        return Collections.unmodifiableMap(properties);
    }

    public String getJdbcUrl() {
        return getRequiredProperty(JDBC_URL);
    }
}
