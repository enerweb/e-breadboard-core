package za.co.enerweb.ebr.edbm.internal;

import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;
import javax.persistence.FlushModeType;
import javax.persistence.LockModeType;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.metamodel.Metamodel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import za.co.enerweb.ebr.edbm.DBManagerFactory;
import za.co.enerweb.ebr.edbm.DbSpec;
import za.co.enerweb.ebr.edbm.IDbManager;

@AllArgsConstructor
public class EdbmEntityManager implements EntityManager {
    private EntityManager em;

    @Getter
    private DbSpec dbSpec;

    public void clear() {
        em.clear();
    }

    public void close() {
        IDbManager dbm = DBManagerFactory.getDbManagerForClosing();
        if (dbm != null) {
            dbm.returnEntityManager(this, dbSpec);
        }
    }

    public void reallyClose() {
        em.close();
    }

    public boolean contains(Object arg0) {
        return em.contains(arg0);
    }

    public Query createNamedQuery(String arg0) {
        return em.createNamedQuery(arg0);
    }

    public Query createNativeQuery(String arg0,
        @SuppressWarnings("rawtypes") Class arg1) {
        return em.createNativeQuery(arg0, arg1);
    }

    public Query createNativeQuery(String arg0, String arg1) {
        return em.createNativeQuery(arg0, arg1);
    }

    public Query createNativeQuery(String arg0) {
        return em.createNativeQuery(arg0);
    }

    public Query createQuery(String arg0) {
        return em.createQuery(arg0);
    }

    public <T> T find(Class<T> entityClass, Object id) {
        try {
            return em.find(entityClass, id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Could not find entity "
                + entityClass.getName() + " with id = '" + id + "'");
        }
    }

    public void flush() {
        em.flush();
    }

    public Object getDelegate() {
        return em.getDelegate();
    }

    public FlushModeType getFlushMode() {
        return em.getFlushMode();
    }

    public <T> T getReference(Class<T> arg0, Object arg1) {
        return em.getReference(arg0, arg1);
    }

    public EntityTransaction getTransaction() {
        return em.getTransaction();
    }

    public boolean isOpen() {
        return em.isOpen();
    }

    public void joinTransaction() {
        em.joinTransaction();
    }

    public void lock(Object arg0, LockModeType arg1) {
        em.lock(arg0, arg1);
    }

    public <T> T merge(T arg0) {
        return em.merge(arg0);
    }

    public void persist(Object arg0) {
        em.persist(arg0);
    }

    public void refresh(Object arg0) {
        em.refresh(arg0);
    }

    public void remove(Object arg0) {
        em.remove(arg0);
    }

    public void setFlushMode(FlushModeType arg0) {
        em.setFlushMode(arg0);
    }

    public <T> TypedQuery<T> createNamedQuery(String arg0, Class<T> arg1) {
        return em.createNamedQuery(arg0, arg1);
    }

    public <T> TypedQuery<T> createQuery(CriteriaQuery<T> arg0) {
        return em.createQuery(arg0);
    }

    public <T> TypedQuery<T> createQuery(String arg0, Class<T> arg1) {
        return em.createQuery(arg0, arg1);
    }

    public void detach(Object arg0) {
        em.detach(arg0);
    }

    public <T> T find(Class<T> arg0, Object arg1, LockModeType arg2,
        Map<String, Object> arg3) {
        return em.find(arg0, arg1, arg2, arg3);
    }

    public <T> T find(Class<T> arg0, Object arg1, LockModeType arg2) {
        return em.find(arg0, arg1, arg2);
    }

    public <T> T find(Class<T> arg0, Object arg1, Map<String, Object> arg2) {
        return em.find(arg0, arg1, arg2);
    }

    public CriteriaBuilder getCriteriaBuilder() {
        return em.getCriteriaBuilder();
    }

    public EntityManagerFactory getEntityManagerFactory() {
        return em.getEntityManagerFactory();
    }

    public LockModeType getLockMode(Object arg0) {
        return em.getLockMode(arg0);
    }

    public Metamodel getMetamodel() {
        return em.getMetamodel();
    }

    public Map<String, Object> getProperties() {
        return em.getProperties();
    }

    public void lock(Object arg0, LockModeType arg1, Map<String, Object> arg2) {
        em.lock(arg0, arg1, arg2);
    }

    public void refresh(Object arg0, LockModeType arg1, Map<String,
            Object> arg2) {
        em.refresh(arg0, arg1, arg2);
    }

    public void refresh(Object arg0, LockModeType arg1) {
        em.refresh(arg0, arg1);
    }

    public void refresh(Object arg0, Map<String, Object> arg1) {
        em.refresh(arg0, arg1);
    }

    public void setProperty(String arg0, Object arg1) {
        em.setProperty(arg0, arg1);
    }

    public <T> T unwrap(Class<T> arg0) {
        return em.unwrap(arg0);
    }
}
