package za.co.enerweb.ebr.edbm;

import java.util.HashMap;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;

/**
 * Specifies an embedded H2 db, which gets stored in memory.
 * To access an in-memory database from another process or from another
 * computer, you need to start a TCP server in the same process as the in-memory
 * database was created. The other processes then need to access the database
 * over TCP/IP or SSL/TLS, using a database URL such as:
 * jdbc:h2:tcp://localhost/mem:db1.
 */
@Slf4j
public class H2MemoryDbSpec extends DbSpec {

    @Override
    protected void initBeforeConnecting() {
        setAutoUpdateSchema(true);
        String name = getProperty("name", "main");

        // jdbc:h2:
        Map<String, String> properties = new HashMap<String, String>();

        String url = "jdbc:log4jdbc:h2:mem:" + name +
            ";DB_CLOSE_DELAY=-1" // By default, closing the last connection to a
                                 // database closes the database. For an
                                 // in-memory database, this means the content
                                 // is lost. This is to keep the database open
                                 // + ";AUTO_SERVER=TRUE" // allow multiple
                                 // processes
            // + ";MVCC=TRUE" // allow higher concurrency
//            + ";LOCK_TIMEOUT=10000" // give more leeway for concurrent threads
            + ";TRACE_LEVEL_FILE=4" // get h2 to log to sl4fj
        ;
        // XXX: factor out common hibernate code..
        String externalUrl = StringUtils.replace(url, ":log4jdbc:", ":");
        log.info("Connecting to db jdbcurl: " + externalUrl);
        log.info(" connect from elsewhere using jdbcurl: "
            + "jdbc:h2:tcp://localhost/mem:" + name);
        setProperty(JDBC_URL, url);
        setProperty("hibernate.connection.username", "sa");
        setProperty("hibernate.connection.password", "");
        setProperty("hibernate.dialect",
            "org.hibernate.dialect.H2Dialect");
    }

}
