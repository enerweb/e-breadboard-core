package za.co.enerweb.ebr.edbm.dbl;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.PrePassivate;
import javax.ejb.SessionSynchronization;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.edbm.DbManagerException;
import za.co.enerweb.ebr.edbm.DbSpec;
import za.co.enerweb.ebr.edbm.IDbManager;
import za.co.enerweb.toolbox.reflection.PropertyUtils;
import za.co.enerweb.toolbox.vaadin.lazytable.SortSpec;
import za.co.enerweb.toolbox.vaadin.lazytable.filter.AFilter;

// XXX: I think we can switch off all transactions for readonly methods using
// @TransactionAttribute.SUPPORTS
// Maybe @AroundInvoke to inject the entity manager
// and then the transaction wrapper just needs to do the transaction stuff.
@Stateful
@LocalBean
@Slf4j
public class EdbmDbLayer implements SessionSynchronization
// ,
// IDataProducer
{

    // maybe we can use this with better syntax with Lambdas one day
    public static interface EntityCreator<T> {
        /**
         * Don't need to persist in here, because the find method will do that
         * @return
         */
        public T createEntity();
    }

    @EJB
    private IDbManager dbManager;

    @Getter
    private DbSpec dbSpec;

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void setDbSpec(DbSpec dbSpec) {
        this.dbSpec = dbSpec;
    }

    protected EntityManager em;
    protected CriteriaBuilder cb;

    // @AfterBegin
    @Override
    public void afterBegin() // throws EJBException
    {
        if (dbSpec == null) {
            log.debug(getClass().getSimpleName() + "dbSpec: " + dbSpec);
            throw new EJBException("dbSpec is null, please call setDbSpec "
                + " before anything else.");
        }
        if (em == null) {
            em = dbManager.getEntityManager(dbSpec);
            if (em == null) {
                log.error("Could not connect to db:" + dbSpec);
            }
            cb = em.getCriteriaBuilder();
        }
        // log.debug(dbName + " begin transaction");
        em.getTransaction().begin();
    }

    // @BeforeCompletion
    // @Override
    public void beforeCompletion() throws EJBException, RemoteException {
        // log.debug(dbName + " commit transaction");
        try {
            if (em != null) {
                em.getTransaction().commit();
            }
        } catch (Exception e) {
            log.error("Could not commit current transaction.", e);
        }
    }

    // @AfterCompletion
    // @Override
    public void afterCompletion(boolean committed) throws EJBException,
        RemoteException {
        if (!committed && em != null && em.getTransaction().isActive()) {
            em.getTransaction().rollback();
        }
        returnEntityManager();
    }

    @PrePassivate
    @PreDestroy
    public void returnEntityManager() {
        if (em != null) {
            // log.debug(dbName + " close em");
            // release em from dbmanager
            em.close();
            em = null;
        }
    }

    // vvv JPA-generic

    @SuppressWarnings("unchecked")
    public <T> List<T> findAll(Class<T> klass) {
        return em.createQuery("SELECT x FROM "
            + klass.getSimpleName() + " x")
            .getResultList();
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> findAll(Class<T> klass, String orderBy) {
        return em.createQuery("SELECT x FROM "
            + klass.getSimpleName() + " x order by " + orderBy)
            .getResultList();
    }

    public <T> List<T> findAll(Query query) {
        @SuppressWarnings("unchecked") List<T> resultList = query
            .getResultList();
        return resultList;
    }

    /**
     * @return null if not found
     */
    public <T> List<T> findAll(CriteriaQuery<T> criteriaQuery) {
        List<T> resultList = em.createQuery(criteriaQuery)
            .getResultList();
        return resultList;
    }

    public <T> int count(Class<T> jpaEntityClass) {
        Query query = em.createQuery(
            "SELECT count(t) from " + jpaEntityClass.getSimpleName()
                + " as t");
        return (int) ((Long) query.getSingleResult()).longValue();
    }

    public <T> List<T> find(
        Class<T> jpaEntityClass,
        final int startIndex,
        final int count, String orderBy) {

        Query query = em.createQuery(
            "SELECT t from " + jpaEntityClass.getSimpleName()
                + " as t "
                + orderBy);
        query.setFirstResult(startIndex);
        query.setMaxResults(count);

        @SuppressWarnings("unchecked") List<T> ret = query.getResultList();
        return ret;
    }

    public <T> int count(Class<T> entityClass, AFilter aFilter) {
        // FIXME: implement filtering!
        return count(entityClass);
    }

    public <T> List<T> find(Class<T> entityClass, int startIndex,
        int count,
        SortSpec sortSpec, AFilter aFilter) {
        // FIXME: implement filtering!
        return find(entityClass, startIndex, count,
            sortSpec.toQueryString("ORDER BY", ", ", "t.", " ASC", " DESC"));
    }

    /**
     * The same as find, but throws an EnitityNotFoundException if it was not
     * found
     */
    public <T> T findOrThrow(Class<T> klass, Object primaryKey) {
        T ret = em.find(klass, primaryKey);
        if (ret == null) {
            throw new EnitityNotFoundException("Could not find entity " +
                klass.getName() + " with primaryKey='" + primaryKey + "'");
        }
        return ret;
    }

    /**
     * Find by primary key.
     * Search for an entity of the specified class and primary key.
     * If the entity instance is contained in the persistence context,
     * it is returned from there.
     * @param entityClass entity class
     * @param primaryKey primary key
     * @return the found entity instance or null if the entity does
     *         not exist
     * @throws IllegalArgumentException if the first argument does
     *         not denote an entity type or the second argument is
     *         is not a valid type for that entity's primary key or
     *         is null
     */
    public <T> T find(Class<T> klass, Object primaryKey) {
        return em.find(klass, primaryKey);
    }

    /**
     * Look up an entity by any field, returns null if it was not found.
     */
    public <T> T findByField(Class<T> klass, String fieldName,
        Object fieldValue) {
        List<T> resultList =
            em.createQuery("SELECT x " +
                "      FROM " + klass.getSimpleName() + " x " +
                "      WHERE x." + fieldName + " = :" + fieldName, klass)
                .setParameter(
                    fieldName, fieldValue)
                .setMaxResults(1).getResultList();
        if (resultList.isEmpty()) {
            return null;
        }
        return resultList.get(0);
    }

    /**
     * Will create and persist a new entity if one could not be found
     * @param klass
     * @param primaryKey
     * @param entityCreator
     * @return
     */
    public <T> T find(Class<T> klass, Object primaryKey,
        EntityCreator<T> entityCreator) {
        T ret = em.find(klass, primaryKey);
        if (ret == null) {
            ret = entityCreator.createEntity();
            em.persist(ret);
            // em.flush();
            log.debug("created " + ret);
            return ret;
        }
        return ret;
    }

    /**
     * @return null if not found
     */
    public <T> T find(CriteriaQuery<T> criteriaQuery) {
        List<T> resultList = em.createQuery(criteriaQuery)
            .setMaxResults(1)
            .getResultList();
        if (!resultList.isEmpty()) {
            return resultList.get(0);
        } else {
            return null;
        }
    }

    public <T> T find(Query query) {
        @SuppressWarnings("unchecked") List<T> resultList = query
            .setMaxResults(1)
            .getResultList();
        if (!resultList.isEmpty()) {
            return resultList.get(0);
        } else {
            return null;
        }
    }

    /**
     * Will create and persist a new entity if one could not be found
     * @param criteriaQuery
     * @param entityCreator Used to create a new entity if one could not be
     *        found already
     * @return
     */
    public <T> T find(CriteriaQuery<T> criteriaQuery,
        EntityCreator<T> entityCreator) {
        List<T> resultList = em.createQuery(criteriaQuery)
            .setMaxResults(1)
            .getResultList();
        if (!resultList.isEmpty()) {
            return resultList.get(0);
        } else {
            T ret = entityCreator.createEntity();
            em.persist(ret);
            // log.debug("created " + ret);
            return ret;
        }
    }

    public <T> T find(Query query,
        EntityCreator<T> entityCreator) {
        @SuppressWarnings("unchecked") List<T> resultList = query
            .setMaxResults(1)
            .getResultList();
        if (!resultList.isEmpty()) {
            return resultList.get(0);
        } else {
            T ret = entityCreator.createEntity();
            em.persist(ret);
            // log.debug("created " + ret);
            return ret;
        }
    }

    /**
     * When you need a property/field of a JPA entity that is lazy loaded,
     * you can use this method to read it for you.
     * @param entity
     * @param field
     * @return
     */
    @SuppressWarnings("unchecked")
    public <E, F> F get(E entity, String field) {
        try {
            entity = em.merge(entity);
            F ret = (F) PropertyUtils.getProperty(entity, field);
            if (ret instanceof Collection<?>) {
                Collection<?> col = (Collection<?>) ret;
                col.size(); // force loading the collection
            }
            return ret;
        } catch (Exception e) {
            throw new DbManagerException(
                "Could not get field: '" + field + "'", e);
        }
    }

    public <T> void remove(T... entities) {
        remove(Arrays.asList(entities));
    }

    public <T> void remove(Collection<T> entities) {
        for (T entity : entities) {
            entity = em.merge(entity);
            em.remove(entity);
        }
    }

    public <T> int removeAll(Class<T> klass) {
        String entityName = klass.getSimpleName();
        int ret = em.createQuery("DELETE FROM " + entityName + " x")
            .executeUpdate();
        log.debug("Deleted {} rows of {}", ret, entityName);
        return ret;
    }

    public <T> void save(T... entities) {
        save(Arrays.asList(entities));
    }

    public <T> void save(Collection<T> entities) {
        for (T entity : entities) {
            em.merge(entity);
        }
    }

    public <T> T save(T entity) {
        T ret = em.merge(entity);

        // For some reason if I do not do the following then the returned object
        // does not contain uninstantiated collections.
        // em.flush();
        // em.refresh(ret);
        // Fixed that by manually initialising the collection by default..

        return ret;
    }

    /**
     * call super if you override this
     */
    public void init() {
    }

    // @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    // public <T> T saveInNewTransaction(T entity) throws RuntimeException {
    // T ret = em.merge(entity);
    //
    // // For some reason if I do not do the following then the returned object
    // // does not contain uninstantiated collections.
    // // em.flush();
    // // em.refresh(ret);
    // // Fixed that by manually initialising the collection by default..
    //
    // return ret;
    // }

}
