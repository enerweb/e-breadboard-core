package za.co.enerweb.ebr.edbm;

import javax.ejb.Local;
import javax.persistence.EntityManager;

import za.co.enerweb.ebr.edbm.internal.EdbmEntityManager;

@Local
public interface IDbManager {

    /**
     * You should not keep these entity managers for long
     * get them use them and then close them!
     * @param name
     * @return
     */
    EntityManager getEntityManager(DbSpec dbSpec);

    /**
     * Returns the entity manager to the pool.
     * @param em
     * @param dbSpec
     */
    void returnEntityManager(EntityManager em, DbSpec dbSpec);

    /**
     * @param name
     * @throws UnsupportedOperationException if we can't do it
     */
    void copyDb(DbSpec from, DbSpec to);

    /**
     * Cloase all connections and init this again.
     */
    void reset();

    /**
     * Create a non-pooled EntityManager, for internal use only!
     * @param name
     * @return
     * @throws Exception if there is a problem creating a new instance,
     *         this will be propagated to the code requesting an object.
     */
    EdbmEntityManager createEntityManager(DbSpec dbSpec) throws Exception;

    /**
     * @param dbSpec
     */
    void warmUp(DbSpec dbSpec);

}
