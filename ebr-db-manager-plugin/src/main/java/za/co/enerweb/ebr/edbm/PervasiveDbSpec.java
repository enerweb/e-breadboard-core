package za.co.enerweb.ebr.edbm;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;

@Slf4j
@Data
public class PervasiveDbSpec extends DbSpec {
    private static final long serialVersionUID = 1L;

    @Override
    protected void initBeforeConnecting() {
        String url = "jdbc:pervasive://" + getProperty("host") + ":" +
            getProperty("port", "1583") + "/"
            + getProperty("datasource");
        // "jdbc:log4jdbc:pervasive://"
        // String url = "jdbc:oracle:thin:@" + host + ":" + port + ":"
        // + schema;

        // when giving this url to
        String externalUrl = StringUtils.replace(url, ":log4jdbc:", ":");
        log.debug("connecting to db url: " + externalUrl);
        setProperty("eclipselink.target-database",
            "org.eclipse.persistence.platform.database.PervasivePlatform");
        setProperty(JDBC_URL, url);
    }

}
