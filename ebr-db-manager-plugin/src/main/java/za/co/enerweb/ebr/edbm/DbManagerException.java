package za.co.enerweb.ebr.edbm;

public class DbManagerException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public DbManagerException() {
    }

    /**
     * @param message
     */
    public DbManagerException(String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public DbManagerException(Throwable cause) {
        super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public DbManagerException(String message, Throwable cause) {
        super(message, cause);
    }

}
