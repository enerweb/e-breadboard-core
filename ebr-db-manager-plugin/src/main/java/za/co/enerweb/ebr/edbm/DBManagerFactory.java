package za.co.enerweb.ebr.edbm;

import static java.lang.String.format;

import java.util.concurrent.ConcurrentMap;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.EbrFactory;
import za.co.enerweb.ebr.annotations.OnEbrReload;
import za.co.enerweb.ebr.edbm.dbl.EdbmDbLayer;
import za.co.enerweb.ebr.server.ConfSection;
import za.co.enerweb.ebr.server.IConfigurationService;

import com.google.common.collect.MapMaker;

@Slf4j
public abstract class DBManagerFactory
// <DBL extends EdbmDbLayer>
{

    private static final String SECTION_PREFIX = "database/";

    public static final String MODULE_NAME =
        "ebr-db-manager-plugin";

    // private final Class<DBL> dbLayerClass;

    // private static WeakHashMap<DBManagerFactory, Void> dBManagerFactories =
    // new WeakHashMap<>();

    private static ConcurrentMap<String, DbSpec> dbSpecs = new MapMaker()
        .weakValues()
        .makeMap();

    // protected DBManagerFactory(Class<DBL> dbLayerClass) {
    // // dBManagerFactories.put(this, null);
    // this.dbLayerClass = dbLayerClass;
    // }

    /**
     * If you want multiple dbspecs for the same persistenceUnit, then specify
     * an id!
     * @param persistenceUnitName
     * @return
     */
    public <DBSPEC extends DbSpec> DBSPEC getDbSpec(String persistenceUnitName
        ) {
        return getDbSpec(persistenceUnitName, persistenceUnitName);
    }

    public <DBSPEC extends DbSpec> DBSPEC getDbSpec(String persistenceUnitName,
        String id) {
        DbSpec dbSpec = dbSpecs.get(id);
        if (dbSpec == null) {
            synchronized (this) {
                dbSpec = dbSpecs.get(id);
                if (dbSpec == null) {
                    dbSpec = createDbSpec(persistenceUnitName, id);
                    dbSpecs.put(id, dbSpec);
                }
            }
            getDbManager().warmUp(dbSpec);
        }
        return (DBSPEC) dbSpec;
    }

    protected DbSpec createDbSpec(String persistenceUnitName, String id) {
        if (persistenceUnitName == null) {
            throw new DbConfigurationException(
                "No persistenceUnitName specied, either specify it or override"
                    + "getDefaultPersistencUnitName(): "
                    + getClass().getSimpleName());
        }

        // look for a configuration, if none is found use an embedded db
        ConfSection section =
            EbrFactory.getConfigurationService().getSection(
                SECTION_PREFIX + id);
        if (section.isEmpty()) {
            section = EbrFactory.getConfigurationService().getSection(
                    SECTION_PREFIX + persistenceUnitName);
            if (section == null) {
                throw new DbConfigurationException(
                    format(
                        "No db configuration section found by id (%s%s) or by "
                            + "persistenceunit (%s%s) \n (in %s)"
                        , SECTION_PREFIX, id, SECTION_PREFIX,
                        persistenceUnitName, IConfigurationService.FILENAME));
            }
        }

        String typeInConf = section.get("type");
        String typeStr = "za.co.enerweb.ebr.edbm." + typeInConf + "DbSpec";
        Class<?> clazz = null;
        try {
            clazz = Class.forName(typeStr);
        } catch (ClassNotFoundException e) {
            try {
                clazz = Class.forName(typeInConf);
            } catch (ClassNotFoundException e1) {
                throw new DbConfigurationException(
                    "Could not determine db type: '"
                        + typeInConf
                        + "'"
                        + "\n valid types need a class to exist:"
                        + "za.co.enerweb.ebr.edbm.<type>DbSpec\n"
                        + "or the type must be a fully qualified "
                        + "class name.",
                    e);
            }
        }
        if (DbSpec.class.isAssignableFrom(clazz)) {
            try {
                DbSpec ret = (DbSpec) clazz.newInstance();
                // PropertyUtils.setProperty(bean, fieldName, value);
                ret.setId(id);
                ret.setPersitenceUnitName(persistenceUnitName);
                ret.initProperties(section);

                return ret;
            } catch (Exception e1) {
                throw new DbConfigurationException(
                    "Could not instantiate class, "
                        + "make sure it has a public no-args constructor: '"
                        + clazz + "'");
            }
        } else {
            throw new DbConfigurationException(
                "Db type must be a subclass of DbSpec: "
                    + clazz);
        }
    }

    protected static void setProperty(DbSpec dbspec, String key, String value) {
        dbspec.setProperty(key, value);
    }

    @SneakyThrows
    public static IDbManager getDbManager() {
        return EbrFactory.lookupEjb(MODULE_NAME, IDbManager.class);
    };

    public static IDbManager getDbManagerForClosing() {
        // return EbrFactory.lookupEjbIfOpen(MODULE_NAME,
        // IDbManager.class);
        try {
            return getDbManager();
        }
        catch (Exception e) {
            log.info("Could not look up db manager for closing.\n"
                    + e.getMessage());
            return null;
        }
    };

    public <D extends EdbmDbLayer> D getDbLayer() {
        return getDbLayer(getDefaultPersistencUnitName());
    }

    /**
     * If you want to have multiple different dbspecs for the same
     * persistenceUnit then you need to specify an id
     */
    @SuppressWarnings("unchecked")
    public <D extends EdbmDbLayer> D getDbLayer(String persistenceUnitName) {
        return getDbLayer(persistenceUnitName, persistenceUnitName);
    }

    @SuppressWarnings("unchecked")
    public <D extends EdbmDbLayer> D getDbLayer(String persistenceUnitName,
        String id) {
        EdbmDbLayer dbLayer = getDbLayer(persistenceUnitName, id,
            (Class<D>) getDefaultDbLayerClass());
        try {
            return (D) dbLayer;
        } catch (ClassCastException e) {
            throw new IllegalStateException(e.getMessage()
                + " (The Dblayer type gets determined by getDblayerClass)", e);
        }
    }

    /**
     * If you want to have multiple different dbspecs for the same
     * persistenceUnit then you need to specify an id
     */
    protected <D extends EdbmDbLayer> D getDbLayer(
        String persistenceUnitName,
        Class<D> dblClass) {
        return getDbLayer(getDbSpec(persistenceUnitName, persistenceUnitName),
            dblClass);
    }

    protected <D extends EdbmDbLayer> D getDbLayer(
        String persistenceUnitName, String id,
        Class<D> klass) {
        return getDbLayer(getDbSpec(persistenceUnitName, id), klass);
    }

    @SuppressWarnings("unchecked")
    public final <D extends EdbmDbLayer> D getDbLayer(DbSpec dbSpec) {
        return getDbLayer(dbSpec, (Class<D>) getDefaultDbLayerClass());
    }

    @SuppressWarnings("unchecked")
    private final <D extends EdbmDbLayer> D getDbLayer(DbSpec dbSpec,
        Class<D> dblClass) {
        if (dbSpec == null) {
            throw new NullPointerException(
                "Can't instantiate a db layer without a spec: "
                    + getClass().getSimpleName());
        }
        if (dblClass == null) {
            throw new DbConfigurationException(
                "No DbLayer class specied, either specify it or override"
                    + "getDefualtDbLayerClass(): "
                    + getClass().getSimpleName());
        }
        EdbmDbLayer ret = EbrFactory.lookupEjb(getModuleName(), dblClass);
        ret.setDbSpec(dbSpec);
        ret.init();
        return (D) ret;
    }

    public <D extends EdbmDbLayer> Class<D> getDefaultDbLayerClass() {
        return null;
    }

    public String getDefaultPersistencUnitName() {
        return null;
    }

    @OnEbrReload(rank = -10)
    public static void resetAll() {
        dbSpecs.clear();
        // for (DBManagerFactory f : dBManagerFactories.keySet()) {
        // if (f != null) {
        // f.reset();
        // }
        // }
    }

    // vvv You have to implement this

    /**
     * @return then name of the jar file containing this
     *         eg. "ebr-adf-example-plugin"
     */
    public abstract String getModuleName();

    /**
     * @return Custom AdfDbLayer class. This determines what type
     *         getDbLayer() will return.
     */

    // /**
    // * @return then name of EBR package which includes the static tasks
    // * and resources eg.za.co.enerweb_adf-example
    // */
    // public abstract String getPackageName();

    // vvv Override these if you need to
    // /**
    // * @return then name of EBR package which includes the database
    // * and variable resources
    // */
    // public String getVarPackageName() {
    // return getPackageName() + "-var";
    // }

}
