package za.co.enerweb.ebr.edbm.dbl;

import java.util.List;

import za.co.enerweb.ebr.edbm.DBManagerFactory;
import za.co.enerweb.toolbox.vaadin.lazytable.ABeanProducer;
import za.co.enerweb.toolbox.vaadin.lazytable.SortSpec;
import za.co.enerweb.toolbox.vaadin.lazytable.filter.AFilter;

public class JpaEntityProducer<T> extends ABeanProducer<T> {

    private final DBManagerFactory dbManagerFactory;
    private String persistenceUnitName;

    public JpaEntityProducer(DBManagerFactory dbManagerFactory,
        String persistenceUnitName,
        Class<T> dataClass) {
        super(dataClass);
        this.dbManagerFactory = dbManagerFactory;
        this.persistenceUnitName = persistenceUnitName;
    }

    @Override
    public int count(AFilter aFilter) {
        // FIXME: implement filtering
        return dbManagerFactory.getDbLayer(persistenceUnitName).count(
            getDataObjectClass(), aFilter);
    }

    @Override
    public List<T> find(int startIndex, int count, SortSpec sortSpec,
        AFilter aFilter) {
        // FIXME: implement filtering and sorting
        return dbManagerFactory.getDbLayer(persistenceUnitName).find(
            getDataObjectClass(), startIndex, count, sortSpec, aFilter);
    }

}
