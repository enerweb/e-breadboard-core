package za.co.enerweb.ebr.edbm;

import java.util.HashSet;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;

import za.co.enerweb.ebr.server.IHomeService;

/**
 * Specifies an embedded H2 db, which gets stored in the resources dir.
 */
@Slf4j
public class H2EmbeddedDbSpec extends EmbeddedDbSpec {

    public static String H2_DB_EXTENTION = "h2.db";
    // XXX: a dirty hack until this is readonly
    private static Set<String> printedJdbcurls = new HashSet<String>();

    public String getDbAbsPath() {
        return getDbAbsPathBaseName() + "." + H2_DB_EXTENTION;
    }

    private String dbAbsPathBaseName = null;

    public String getDbAbsPathBaseName() {
        if (dbAbsPathBaseName == null && getHomeService() != null) {
            // only populate this once
            dbAbsPathBaseName = getHomeService().getFile(
                getDbDirSpec().subDir(getName())).getAbsolutePath();
        }
        return dbAbsPathBaseName;
    }

    public void setHomeService(IHomeService homeService) {
        super.setHomeService(homeService);
        dbAbsPathBaseName = null;
    }

    @Override
    protected void initBeforeConnecting() {
        super.initBeforeConnecting();

        updateH2Properties(this, getDbAbsPathBaseName());
    }

    protected static void updateH2Properties(DbSpec dbspec,
        String dbAbsPathBaseName) {
        String url = "jdbc:log4jdbc:h2:file:" + dbAbsPathBaseName
            // + ";IFEXISTS=TRUE" // only allow existing db files
            + ";DB_CLOSE_DELAY=0" // close immediately
            // after the last connection was closed
            + ";AUTO_SERVER=TRUE" // allow multiple processes
            + ";MVCC=TRUE" // allow higher concurrency

            + ";LOCK_TIMEOUT=15000" // give more leeway for concurrent threads
            // + ";TRACE_LEVEL_FILE=4" // get h2 to log to sl4fj
        ;

        String cacheSizeKb = dbspec.getProperty("cacheSizeKb");
        if (cacheSizeKb != null) {
            url += ";CACHE_SIZE=" + Integer.valueOf(cacheSizeKb);
        }
        // XXX: factor out common hibernate code..
        String externalUrl = StringUtils.replace(url, ":log4jdbc:", ":");

        // XXX: use a dirty hack until this is readonly
        if (!printedJdbcurls.contains(externalUrl)) {
            // only do this once:
            log.info("Connecting to db jdbcurl:\n  " + externalUrl + "\n");
            printedJdbcurls.add(externalUrl);
        }

        dbspec.setProperty(JDBC_URL, url);
        dbspec.setProperty("javax.persistence.jdbc.user", "sa");
        dbspec.setProperty("javax.persistence.jdbc.password", "");
        dbspec.setProperty("hibernate.dialect",
            "org.hibernate.dialect.H2Dialect");

        // it looks like hibernate keeps the db open and
        // re-uses it between tests..
        dbspec.setProperty("hibernate.ejb.entitymanager_factory_name",
            url);
    }

}
