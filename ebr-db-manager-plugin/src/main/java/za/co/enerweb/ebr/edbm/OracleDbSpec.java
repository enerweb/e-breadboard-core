package za.co.enerweb.ebr.edbm;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;

@Slf4j
@Data
public class OracleDbSpec extends DbSpec {
    private static final long serialVersionUID = 1L;

    @Override
    protected void initBeforeConnecting() {
        String url = "jdbc:log4jdbc:oracle:thin:@" + getProperty("host")
            + ":" + getProperty("port", "1521") + ":"
            + getProperty("sid");

        // when giving this url to
        String externalUrl = StringUtils.replace(url, ":log4jdbc:", ":");
        log.debug("connecting to db url: " + externalUrl);
        setProperty(JDBC_URL, url);
        // setProperty("hibernate.connection.username", username);
        // setProperty("hibernate.connection.password", password);
        String spatial = getProperty("spatial", "false");
        if (!Boolean.parseBoolean(spatial)) {
            setProperty("hibernate.dialect",
            "org.hibernate.dialect.OracleDialect");
        } else {
            setProperty("hibernate.dialect",
                "org.hibernate.spatial.dialect.oracle.OracleSpatial10gDialect");
        }
    }

}
