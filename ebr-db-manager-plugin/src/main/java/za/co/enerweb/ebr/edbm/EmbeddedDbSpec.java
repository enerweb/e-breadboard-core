package za.co.enerweb.ebr.edbm;

import java.io.File;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.ebr.server.HomeFileCategory;
import za.co.enerweb.ebr.server.HomeFileSpec;

/**
 * Specifies an embedded db, which gets stored in the resources dir.
 */
@Slf4j
public abstract class EmbeddedDbSpec extends DbSpec {

    public static final String QUALIFIED_PACKAGE_NAME = "qualifiedPackageName";

    public static String WORKING_DB_NAME = "work";

    @Getter
    private String name = WORKING_DB_NAME;

    /**
     * vendor_packagename eg. za.co.enerweb_example-package
     */
    @Getter
    private String qualifiedPackageName;

    @Override
    protected void initBeforeConnecting() {
        qualifiedPackageName = getRequiredProperty(QUALIFIED_PACKAGE_NAME);
        name = getProperty("name");

        File dbFile = getDbFile();
        log.debug("dbFile:" + dbFile.getAbsolutePath());
        if (!dbFile.exists()) {
            // maybe we can generate a db if we have a script..
            log.warn("Db does not exist: "
                  + dbFile.getAbsolutePath());
            // create the dir so long
            dbFile.getParentFile().mkdirs();
        }
    }


    public HomeFileSpec getDbDirSpec() {
        return new HomeFileSpec(HomeFileCategory.RESOURCES,
            getQualifiedPackageName(),
            RP_DBS);
    }

    public abstract String getDbAbsPath();

    public File getDbFile() {
        return new File(getDbAbsPath());
    }

    @Override
    public String toString() {
        return qualifiedPackageName + ":" + name + ":"
            + getPersitenceUnitName();
    }
}
