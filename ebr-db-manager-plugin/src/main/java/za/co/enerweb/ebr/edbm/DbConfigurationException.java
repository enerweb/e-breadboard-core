package za.co.enerweb.ebr.edbm;

import za.co.enerweb.ebr.exceptions.ServiceException;

public class DbConfigurationException extends ServiceException {

    public DbConfigurationException() {
    }

    public DbConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }

    public DbConfigurationException(String message) {
        super(message);
    }

    public DbConfigurationException(Throwable cause) {
        super(cause);
    }

}
