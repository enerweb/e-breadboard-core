package za.co.enerweb.toolbox.jpa;

import java.lang.reflect.Field;

import javax.persistence.Id;
import javax.persistence.Table;

import za.co.enerweb.toolbox.string.StringUtils;

/*
 * maybe stick these in a softreference map
 */
public class JpaUtils {

    /**
     * Returns the name of the field annotated with @Id or null if none can
     * be found
     */
    public static String detectIdField(Class<?> entityClass) {
        Field[] fields = entityClass.getDeclaredFields();
        for (Field f : fields) {
            if (f.getAnnotation(Id.class) != null) {
                return f.getName();
            }
        }
        return null;
    }

    public static String detectTableName(Class<?> entityClass) {
        Table atTable = entityClass.getAnnotation(Table.class);
        if (atTable != null) {
            String customName = atTable.name();
            if (!customName.isEmpty()) {
                return customName;
            }
        }
        return StringUtils.camelCaseToUnderscoreSeparated(
            entityClass.getSimpleName()).toUpperCase();
    }
}
